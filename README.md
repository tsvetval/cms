# README #

Для запуска в linux как сервис надо: 
1) создать в $CATALINA_HOME/bin/setenv.sh
2) Указать в нем : 

```
#!bash
JAVA_OPTS="$JAVA_OPTS -Dfile.encoding=UTF8"
export JAVA_OPTS

```

3) создать в /etc/init.d/tomcat
4) указать в нем:
```
#!bash

#!/bin/bash
# description: Tomcat Start Stop Restart
# processname: tomcat
# chkconfig: 234 20 80
JAVA_HOME=/usr/java/jdk1.8.0_20
export JAVA_HOME
PATH=$JAVA_HOME/bin:$PATH
export PATH
CATALINA_HOME=/tomcat
LANG=ru_RU.UTF-8
export LANG

echo $LANG

case $1 in
start)
sh $CATALINA_HOME/bin/startup.sh
;;
stop)
sh $CATALINA_HOME/bin/shutdown.sh
sudo killall -9 java
sudo zip -r /tomcat/logs.zip /tomcat/logs
sudo rm -rf /tomcat/logs/*
;;
restart)
sh $CATALINA_HOME/bin/shutdown.sh
sh $CATALINA_HOME/bin/startup.sh
;;
esac
exit 0


```

5) запускать томкат можно: service tomcat start