package ru.peak.controllers;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlSecurityException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.misc.BinaryHelper;
import ru.peak.misc.UploadedFile;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.ml.prop.Property;
import ru.peak.ml.template.TemplateEngine;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.helper.DownloadHelper;
import ru.peak.ml.web.service.MlPageBlockService;
import ru.peak.ml.web.service.MlPageService;
import ru.peak.ml.web.service.MlUtilService;
import ru.peak.security.dao.MlUserDao;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.Map;

/**
 *
 */
@Singleton
public class MlCmsServlet extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(MlCmsServlet.class);

    @Inject
    TemplateEngine templateEngine;

    public static final String REDIRECT_URL = "/console/auth";
    public static final String CHANGE_PASSWORD_URL = "/console/changePassword";
    public static final String SECURITY_BLOCK_ID = "3";
    public static final String CHANGE_PASSWORD_BLOCK_ID = "22";
    public static final String REQUEST_PARAMETER_USER = "user";
    public static final String USER_KEY_SESSION = "user-id";
    public static final String URL_FOR_BACK_AFTER_AUTH = "need-redirect-url";


    private static final String ML_APPLICATION_ERROR = "application";
    private static final String ML_SERVER_ERROR = "server";
    private static final String ML_SECURITY_ERROR = "security";


    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            /*
            * Так как MlCmsServlet синглтон, то при переинициализации у него остается "старый" EntityManager.
            * Поэтому получаем сервисы из джуса каждый раз.
            * */
            MlPageService mlPageService = GuiceConfigSingleton.inject(MlPageService.class);
            MlUtilService mlUtilService = GuiceConfigSingleton.inject(MlUtilService.class);
            MlPageBlockService mlPageBlockService = GuiceConfigSingleton.inject(MlPageBlockService.class);
            //SecurityService securityService = GuiceConfigSingleton.inject(SecurityService.class);
            //-----------------------------------Security Check---------------------------------
            String url = req.getRequestURI();
            String pageBlockId = req.getParameter("pageBlockId");

            MlHttpServletRequest request = new MlHttpServletRequest(req);
            MlHttpServletResponse response = new MlHttpServletResponse(resp);
            //TODO Это дыра в безопасности мы можем передать 3 блок искусственно
/*
            if (!url.endsWith(REDIRECT_URL) && !SECURITY_BLOCK_ID.equals(pageBlockId)) {
                HttpSession session = req.getSession();
                if (session.isNew() || session.getAttribute(USER_KEY_SESSION) == null) {
                    if (session.getAttribute(URL_FOR_BACK_AFTER_AUTH) == null) {
                        session.setAttribute(URL_FOR_BACK_AFTER_AUTH, url);
                        log.info(String.format("IP : %s, after auth will redirect to: %s ", req.getRemoteAddr(), url));
                    }
                    if (req.getMethod().equals("GET")) {
                        resp.sendRedirect(req.getContextPath() + REDIRECT_URL);
                    } else {
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.add("needAuth", new JsonPrimitive(true));
                        jsonObject.add("url", new JsonPrimitive(req.getContextPath() + REDIRECT_URL));
                        response.setJsonData(jsonObject);
                        renderJson(jsonObject, resp);
                    }
                    return;
                } else {
                    Long id = (Long) session.getAttribute(USER_KEY_SESSION);
                    MlUser mlUser = securityService.getUserById(id);
                    securityService.detach(mlUser);
                    req.setAttribute(REQUEST_PARAMETER_USER, mlUser);
                    Boolean needChangePassword = (Boolean) (session.getAttribute(SecurityFilter.USER_NEED_CHANGE_PASSWORD));
                    if (!url.endsWith(CHANGE_PASSWORD_URL) && needChangePassword != null && needChangePassword == true && !CHANGE_PASSWORD_BLOCK_ID.equals(pageBlockId)) {
                        resp.sendRedirect(req.getContextPath() + CHANGE_PASSWORD_URL);
                        return;
                    }
                }
            }
*/
            //-----------------------------------Security Check---------------------------------

            //TODO check security for upload
            if ((req.getContextPath() + "/upload").equals(url)) {
            /*Происходит загрузка файла*/
                Long userId = null;
                if (req.getSession().getAttribute(USER_KEY_SESSION) != null) {
                    userId = (Long) req.getSession().getAttribute(USER_KEY_SESSION);
                }
                if(req.getParameter("userLogin")!=null){
                    MlUserDao mlUserDao = GuiceConfigSingleton.inject(MlUserDao.class);
                    MlUser mlUser = mlUserDao.getUserByLogin(req.getParameter("userLogin"));
                    if(mlUser!=null){
                        userId = mlUser.getId();
                    }
                }
                Enumeration<String> attributeList = req.getAttributeNames();
                while (attributeList.hasMoreElements()) {
                    String attribute = attributeList.nextElement();
                    if (req.getAttribute(attribute) instanceof UploadedFile) {
                        UploadedFile uploadedFile = (UploadedFile) req.getAttribute(attribute);
                        String tempPath = String.format("%s/user_%d/%s", Property.getTempDir(), userId, attribute);
                        log.debug(String.format("Upload file to TempDir %s", tempPath));
                        FileUtils.writeByteArrayToFile(new File(tempPath), uploadedFile.getContent());
                    }
                }
                return;
            }
            if ((req.getContextPath() + "/getTempFile").equals(url)) {
                Long userId = null;
                if (req.getSession().getAttribute(USER_KEY_SESSION) != null) {
                    userId = (Long) req.getSession().getAttribute(USER_KEY_SESSION);
                }
                String fileName = request.getString("fileName");
                String tempPath = String.format("%s/user_%d/%s", Property.getTempDir(), userId, fileName);
                response.setBinaryData(FileUtils.readFileToByteArray(new File(tempPath)));
                response.setDataType(MlHttpServletResponse.DataType.BINARY);
            } else {

                if (url.startsWith(req.getContextPath() + "/util/")) {
            /*Вызов утилиты*/
                    processUtilActions(mlUtilService, request, response);

                } else {
                    String pageUrl = req.getPathInfo();
                    pageUrl = URLDecoder.decode(pageUrl, "UTF-8");
                    if (pageUrl.endsWith("/page_block") || // page_block actions (ajax requests)
                            BinaryHelper.isShowLink(pageUrl)) { // binary request (img tag)
                        // Это звапрос к пейдж блоку
                        processPageBlockActions(mlPageBlockService, request, response);
                    } else if (DownloadHelper.isDownloadLink(pageUrl, getServletContext())) { // download link
                        // Это запрос на скачиванеи файла
                        DownloadHelper.feedDownloadFile(pageUrl, resp, getServletContext());
                        return;
                    } else {  // show site Page
                        // Запрос на отображение страницы
                        mlPageService.processRequest(request, response);
                    }
                }
            }
            switch (response.getDataType()) {
                case HTML: {
                    log.debug(String.format("Do render template [%s]", response.getHtmlTemplate()));
                    renderTemplate(response.getHtmlTemplate(), response.getHtmlTemplateData(), resp);
                    break;
                }

                case JSON: {
                    log.debug(String.format("Do render Json "));
                    renderJson(response.getJsonData(), resp);
                    break;
                }

                case DOWNLOAD: {
                    JsonObject jsonObject = new JsonObject();
                    String link = DownloadHelper.generateDownloadLink(response.getDownloadData(), response.getDownloadFileName(), req.getServletContext());
                    jsonObject.add("url", new JsonPrimitive(link));
                    jsonObject.add("downloadLink", new JsonPrimitive(true));
                    renderJson(jsonObject, resp);
                    break;
                }
                case BINARY: {
                    BinaryHelper.renderBinary(resp, response.getBinaryData());
                }

                case NONE:
                    break;
                case REDIRECT:
                    break;

                default: {
                    log.error("Response data type doesn't specified!");
                }
            }

        } catch (Throwable e) {
            log.error("Internal Ml Cms ERROR", e);
            throw new MlServerException(e.getMessage(), e);
        }
    }

    private void processUtilActions(MlUtilService mlUtilService, MlHttpServletRequest request, MlHttpServletResponse response) {
        try {
            mlUtilService.processRequest(request, response);
        } catch (MlSecurityException e) {
            JsonObject dataJson = new JsonObject();
            dataJson.add("error", new JsonPrimitive(ML_SECURITY_ERROR));
            dataJson.add("message", new JsonPrimitive(e.getMessage() != null ? e.getMessage() : "null"));
            response.setJsonData(dataJson);
        } catch (MlApplicationException e) {
            JsonObject dataJson = new JsonObject();
            dataJson.add("error", new JsonPrimitive(ML_APPLICATION_ERROR));
            dataJson.add("message", new JsonPrimitive(e.getMessage() != null ? e.getMessage() : "null"));
            response.setJsonData(dataJson);
        } catch (MlServerException e) {
            JsonObject dataJson = new JsonObject();
            dataJson.add("error", new JsonPrimitive(ML_SERVER_ERROR));
            dataJson.add("message", new JsonPrimitive(e.getMessage() != null ? e.getMessage() : "null"));

            StringBuilder sb = new StringBuilder();

            if (e.getCause() != null) {
                sb.append(e.getCause().toString());
                for (StackTraceElement element : e.getCause().getStackTrace()) {
                    sb.append(element.toString());
                    sb.append("\n");
                }
            }
            sb.append(e.toString());
            for (StackTraceElement element : e.getStackTrace()) {
                sb.append(element.toString());
                sb.append("\n");
            }

            dataJson.add("stacktrace", new JsonPrimitive(sb.toString()));
            response.setJsonData(dataJson);
        } catch (Throwable e) {
            log.error("", e);
            JsonObject dataJson = new JsonObject();
            dataJson.add("error", new JsonPrimitive(ML_SERVER_ERROR));
            dataJson.add("message", new JsonPrimitive(e.getClass().getName() + " : " + (e.getMessage() != null ? e.getMessage() : "null")));
            StringBuilder sb = new StringBuilder();

            sb.append(e.toString());
            for (StackTraceElement element : e.getStackTrace()) {
                sb.append(element.toString());
                sb.append("\n");
            }
            dataJson.add("stacktrace", new JsonPrimitive(sb.toString()));
            response.setJsonData(dataJson);
        }
    }


    private void processPageBlockActions(MlPageBlockService mlPageBlockService, MlHttpServletRequest request, MlHttpServletResponse response) {
        try {
            mlPageBlockService.processRequest(request, response);
        } catch (MlSecurityException e) {
            JsonObject dataJson = new JsonObject();
            dataJson.add("error", new JsonPrimitive(ML_SECURITY_ERROR));
            dataJson.add("message", new JsonPrimitive(e.getMessage() != null ? e.getMessage() : "null"));
            response.setJsonData(dataJson);
        } catch (MlApplicationException e) {
            JsonObject dataJson = new JsonObject();
            dataJson.add("error", new JsonPrimitive(ML_APPLICATION_ERROR));
            dataJson.add("message", new JsonPrimitive(e.getMessage() != null ? e.getMessage() : "null"));
            response.setJsonData(dataJson);
        } catch (MlServerException e) {
            JsonObject dataJson = new JsonObject();
            dataJson.add("error", new JsonPrimitive(ML_SERVER_ERROR));
            dataJson.add("message", new JsonPrimitive(e.getMessage() != null ? e.getMessage() : "null"));

            StringBuilder sb = new StringBuilder();

            if (e.getCause() != null) {
                sb.append(e.getCause().toString());
                for (StackTraceElement element : e.getCause().getStackTrace()) {
                    sb.append(element.toString());
                    sb.append("\n");
                }
            }
            sb.append(e.toString());
            for (StackTraceElement element : e.getStackTrace()) {
                sb.append(element.toString());
                sb.append("\n");
            }

            dataJson.add("stacktrace", new JsonPrimitive(sb.toString()));
            response.setJsonData(dataJson);
        } catch (Throwable e) {
            log.error("", e);
            JsonObject dataJson = new JsonObject();
            dataJson.add("error", new JsonPrimitive(ML_SERVER_ERROR));
            dataJson.add("message", new JsonPrimitive(e.getClass().getName() + " : " + (e.getMessage() != null ? e.getMessage() : "null")));
            StringBuilder sb = new StringBuilder();

            sb.append(e.toString());
            for (StackTraceElement element : e.getStackTrace()) {
                sb.append(element.toString());
                sb.append("\n");
            }
            dataJson.add("stacktrace", new JsonPrimitive(sb.toString()));
            response.setJsonData(dataJson);
        }
    }

    private void renderTemplate(String templateName, Map<String, Object> data, HttpServletResponse resp) throws IOException {
        String result = templateEngine.renderTemplate(templateName, data);
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html");
        resp.getWriter().print(result);
    }

    private void renderJson(JsonElement data, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        //JsonObject result = new JsonObject();
//        for (Map.Entry<String, JsonElement> entry : data.entrySet()) {
//            result.add(entry.getKey(), entry.getValue());
//        }
        resp.getWriter().print(new GsonBuilder().create().toJson(data));
    }
}
