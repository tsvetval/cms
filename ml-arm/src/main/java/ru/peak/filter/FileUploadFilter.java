package ru.peak.filter;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.peak.misc.UploadedFile;
import ru.peak.ml.prop.MlProperties;
import ru.peak.ml.prop.Property;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 */
@Singleton
public class FileUploadFilter implements Filter {
    private static final Logger log = LoggerFactory.getLogger(FileUploadFilter.class);

    ServletFileUpload uploader;

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        if (ServletFileUpload.isMultipartContent(request)) {
            //TODO только для авторизованых пользователей
            try {
                List<FileItem> fileItemsList = uploader.parseRequest(request);
                for (FileItem fileItem : fileItemsList) {
                    UploadedFile file = new UploadedFile();
                    file.setContent(fileItem.get());
                    file.setName(fileItem.getName());
                    request.setAttribute(fileItem.getFieldName(), file);
                }
            } catch (FileUploadException e) {
                log.error("", e);
            }
        }
        chain.doFilter(request, resp);
    }

    public void init(FilterConfig config) throws ServletException {
        DiskFileItemFactory factory = new DiskFileItemFactory();
        File file = new File(Property.getTempDir());
        factory.setRepository(file);
        uploader = new ServletFileUpload(factory);
    }

}
