package ru.peak.listener;

import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.persist.PersistService;
import com.google.inject.servlet.GuiceServletContextListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.modules.*;

import javax.servlet.ServletContextEvent;
import java.util.*;

public class GuiceListener extends GuiceServletContextListener {
    private static final Logger log = LoggerFactory.getLogger(GuiceListener.class);


    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        log.debug("Start ObjectModelGuiceInitContextListener contextInitialized");
        super.contextInitialized(servletContextEvent);
        // Стартуем перситенс сервис тут т.к. он нам понадобится при инициализации мета данных
        log.debug("Start PersistService");
        GuiceConfigSingleton.inject(PersistService.class).start();
        log.info("Guice Context Listener started");

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        //persistService.stop();
        super.contextDestroyed(servletContextEvent);
    }

    @Override
    protected Injector getInjector() {
        List<Module> modules = (new GuiceModules()).getGuiceModules();
        GuiceConfigSingleton gcs = GuiceConfigSingleton.getInstance(modules);
        return gcs.getInjector();
    }
}
