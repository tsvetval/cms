package ru.peak.listener;

import com.google.inject.persist.UnitOfWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.holders.SecurityHolder;
import ru.peak.ml.core.initializer.MlMetaDataInitializeService;
import ru.peak.ml.core.holders.impl.SecurityHolderImpl;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Листенер проводящий изначальную загрузку классов
 */
public class MlBootstrapListener implements ServletContextListener {
    private static final Logger log = LoggerFactory.getLogger(MlBootstrapListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        UnitOfWork unitOfWork = GuiceConfigSingleton.inject(UnitOfWork.class);
        log.debug("Try Start UnitOfWork");
        unitOfWork.begin();
        try {
            log.debug("Start MlBootstrapListener, Try to initialize MetaData");
            MlMetaDataInitializeService mlMetaDataInitializeService = GuiceConfigSingleton.inject(MlMetaDataInitializeService.class);
            mlMetaDataInitializeService.initializeAllMetaData();
            log.debug("Start MlBootstrapListener started. Meta Data ready to work.");
        } finally {
            log.debug("Try Stop UnitOfWork");
            unitOfWork.end();
        }
        GuiceConfigSingleton.inject(SecurityHolder.class).initAllRoles();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        log.debug("Destroy context of MlBootStrapListener");
    }
}
