package ru.peak.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.prop.MlProperties;
import ru.peak.ml.prop.Property;
import ru.peak.ml.web.service.MlSchedulerService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Лисенер для обновления стандартныйх свойств
 */
public class PropertiesListener implements ServletContextListener {

    private static final Logger log = LoggerFactory.getLogger(PropertiesListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        log.debug("Run PropertiesListener");
        MlProperties properties = GuiceConfigSingleton.inject(MlProperties.class);
        // Need to overwrite path properties, otherwise they will be pointed to hardcoded '/ml-arm' context path
        if(!"development".equals(properties.getProperty(Property.MODE))) {
            String contextPath = servletContextEvent.getServletContext().getContextPath();
            properties.setProperty(
                    Property.TEMPLATE_PATH,
                    System.getProperty("catalina.base") + "/webapps" + contextPath + "/WEB-INF/templates"
            );
            properties.setProperty(
                    Property.STATIC_PATH,
                    System.getProperty("catalina.base") + "/webapps/" + contextPath + "/public"
            );
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        log.debug("Begin shutdown MlSchedulerService...");
        GuiceConfigSingleton.inject(MlSchedulerService.class).shutdown();
        log.debug("End shutdown MlSchedulerService. All right.");
    }
}
