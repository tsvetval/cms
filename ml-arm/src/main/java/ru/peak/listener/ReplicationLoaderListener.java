package ru.peak.listener;

import com.google.inject.persist.UnitOfWork;
import org.apache.commons.compress.archivers.ArchiveException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.template.TemplateEngine;
import ru.peak.ml.web.model.MlImportReplicationFile;
import ru.peak.ml.web.replication.helper.ImportReplicationOption;
import ru.peak.ml.web.replication.helper.Replication;
import ru.peak.ml.web.service.MlReplicationService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by d_litovchenko on 12.03.15.
 */
public class ReplicationLoaderListener implements ServletContextListener {

    private static final String SHOW_RESULT = "blocks/utils/replication/import/showResultImport.hml";
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resLib = classLoader.getResource("");
        List<String> replicationFileNames = new ArrayList<>();
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        List<MlImportReplicationFile> replicationFiles = commonDao.getQueryWithoutSecurityCheck("select o from MlImportReplicationFile o").getResultList();
        for(MlImportReplicationFile replicationFile: replicationFiles){
            replicationFileNames.add((String) replicationFile.get("replicationFile_filename"));
        }
        try {
            File file = new File(resLib.toURI());
            if(file.isDirectory()){
                findReplicationFile(file,replicationFileNames);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ArchiveException e) {
            e.printStackTrace();
        }
    }

    public void findReplicationFile(File directory, List<String> replicationFileNames) throws IOException, JAXBException, ArchiveException {
        for(File zipFile: directory.listFiles()){
            if(zipFile.isDirectory()){
                findReplicationFile(zipFile, replicationFileNames);
            }else{
                if(zipFile.getName().endsWith("zip")){
                    if(!replicationFileNames.contains(zipFile.getName())){
                        UnitOfWork unitOfWork = GuiceConfigSingleton.inject(UnitOfWork.class);
                        unitOfWork.end();
                        unitOfWork.begin();
                        MlReplicationService replicationService = GuiceConfigSingleton.inject(MlReplicationService.class);
                        TemplateEngine templateEngine = GuiceConfigSingleton.inject(TemplateEngine.class);
                        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);

                        byte[] data = Files.readAllBytes(Paths.get(zipFile.toURI()));
                        ImportReplicationOption.ImportType importType = ImportReplicationOption.ImportType.CREATE_UPDATE;
                        Replication replication = replicationService.importReplication(data, false, new ImportReplicationOption(importType));
                        MlImportReplicationFile mlImportReplication = (MlImportReplicationFile) commonDao.createNewEntity(MlImportReplicationFile.class);
                        HashMap<String, Object> dataMap = new HashMap<>();
                        dataMap.put("block", replication);
                        String html = templateEngine.renderTemplate(SHOW_RESULT, dataMap);
                        mlImportReplication.setDateLoad(new Date());
                        mlImportReplication.setReplicationFile(data);
                        mlImportReplication.setName(replication.getName());
                        mlImportReplication.setReplicationFileName(zipFile.getName());
                        mlImportReplication.setReportFile(html.getBytes());
                        mlImportReplication.setReportFileName("report_" + zipFile.getName().replace(".zip", ".html"));
                        commonDao.persistTransactionalWithoutSecurityCheck(mlImportReplication);
                        System.out.println(zipFile.getName());
                    }
                }
            }
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
