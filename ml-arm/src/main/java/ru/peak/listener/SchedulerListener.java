package ru.peak.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.web.service.MlSchedulerService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created with IntelliJ IDEA.
 * User: a_gumenyuk
 * Date: 23.07.14
 * Time: 18:11
 * Листенер проводящий запуск планировщика заданий
 */
public class SchedulerListener implements ServletContextListener {

    private static final Logger log = LoggerFactory.getLogger(SchedulerListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        log.debug("Begin start MlSchedulerService...");
        GuiceConfigSingleton.inject(MlSchedulerService.class).start();
        log.debug("End start MlSchedulerService. All right.");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        log.debug("Begin shutdown MlSchedulerService...");
        GuiceConfigSingleton.inject(MlSchedulerService.class).shutdown();
        log.debug("End shutdown MlSchedulerService. All right.");
    }
}
