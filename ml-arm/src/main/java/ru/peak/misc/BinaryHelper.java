package ru.peak.misc;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User: v_erohin
 * Date: 03.09.14
 * Time: 14:56
 */
public class BinaryHelper {
    public static final String DIR_NAME = "page_block_image";

    public static boolean isShowLink(String url) {
        return url.endsWith("/" + DIR_NAME);
    }

    public static void renderBinary(HttpServletResponse resp, byte[] bytes) throws IOException {
        resp.setContentLength(bytes.length);
        resp.getOutputStream().write(bytes);
    }
}
