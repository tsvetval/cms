package ru.peak.misc;

/**
 */
public class UploadedFile {
    private byte[] content;
    private String name;

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
