package ru.peak.modules;

import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.multibindings.Multibinder;
import ru.ml.core.common.guice.IGuiceModules;
import ru.peak.ml.core.initializer.jpa.impl.JPADynamicClassInitializerImpl;
import ru.peak.ml.core.initializer.jpa.JPADynamicClassInitializerService;
import ru.peak.ml.core.initializer.MlMetaDataInitializeService;
import ru.peak.ml.core.initializer.impl.MetaDataInitializeServiceImpl;
import ru.peak.ml.core.report.model.MlExcelReport;
import ru.peak.ml.core.report.model.MlJasperReport;
import ru.peak.ml.core.services.AccessService;
import ru.peak.ml.core.report.service.ReportGenerator;
import ru.peak.ml.core.report.service.impl.ExcelReportGenerator;
import ru.peak.ml.core.report.service.impl.JasperReportGenerator;
import ru.peak.ml.prop.MlProperties;
import ru.peak.ml.prop.PropertyProvider;
import ru.peak.ml.prop.impl.CMSPropertiesFileProvider;
import ru.peak.ml.prop.impl.MlPropertiesImpl;
import ru.peak.ml.prop.impl.ProjectPropertiesFileProvider;
import ru.peak.ml.template.TemplateEngine;
import ru.peak.ml.template.impl.TemplateEngineImpl;
import ru.peak.ml.web.settings.DBPropertyProvider;
import ru.peak.security.services.AccessServiceImpl;

/**
 *
 */
public class BindModule implements Module {
    @Override
    public void configure(Binder binder) {
        binder.bind(MlMetaDataInitializeService.class).to(MetaDataInitializeServiceImpl.class);
        binder.bind(JPADynamicClassInitializerService.class).to(JPADynamicClassInitializerImpl.class);
        binder.bind(TemplateEngine.class).to(TemplateEngineImpl.class);
        binder.bind(MlProperties.class).to(MlPropertiesImpl.class);
        binder.bind(IGuiceModules.class).to(GuiceModules.class);
        binder.bind(AccessService.class).to(AccessServiceImpl.class);

        // для получения коллекции элементов через инжектор GuiceConfigSingleton.inject(Key.get(Multibinder.setOf(new TypeLiteral(PropertyProvider.class))))
        Multibinder<PropertyProvider> multibinder = Multibinder.newSetBinder(binder,PropertyProvider.class);
        multibinder.addBinding().to(CMSPropertiesFileProvider.class);
        multibinder.addBinding().to(ProjectPropertiesFileProvider.class);
        multibinder.addBinding().to(DBPropertyProvider.class);

        MapBinder<Class, ReportGenerator> mapbinder= MapBinder.newMapBinder(binder, Class.class, ReportGenerator.class);
        mapbinder.addBinding(MlExcelReport.class).to(ExcelReportGenerator.class);
        mapbinder.addBinding(MlJasperReport.class).to(JasperReportGenerator.class);
    }
}
