package ru.peak.modules;

import com.google.inject.Module;
import ru.ml.core.common.guice.IGuiceModules;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: d_litovchenko
 * Date: 04.08.14
 * Time: 10:37
 * To change this template use File | Settings | File Templates.
 */
public class GuiceModules implements IGuiceModules {
    @Override
    public List<Module> getGuiceModules() {
        List<Module> modules = new ArrayList<>();
        modules.add(new BindModule());
        modules.add(new JpaModule());
        modules.add(new RoteModule());
        return modules;
    }
}
