package ru.peak.modules;

import com.google.inject.servlet.ServletModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.peak.controllers.MlCmsServlet;
import ru.peak.controllers.StaticServlet;
import ru.peak.filter.CustomPersistFilter;
import ru.peak.filter.FileUploadFilter;
import ru.peak.security.filter.SecurityFilter;

/**
 *
 */
public class RoteModule extends ServletModule {
    private static final Logger log = LoggerFactory.getLogger(RoteModule.class);

    public void configureServlets() {
        serve("/public/*").with(StaticServlet.class);
        log.debug("Add PersistFilter");
        filter("/*").through(CustomPersistFilter.class);
       // filter("/*").through(SecurityFilter.class);
        serve("/*").with(MlCmsServlet.class);
        filter("/*").through(FileUploadFilter.class);
    }
}