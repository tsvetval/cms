/**
 * Основной класс описывающий зависимости
 */

require.config({
    urlArgs: "bust=v1",
    baseUrl: document.getElementsByTagName("body")[0].attributes.getNamedItem("cms_context_path").value + '/public/js',
    paths: {
        text: 'libs/text',
        jquery: 'libs/jquery-2.1.3.min',
        jqueryUi: 'libs/jquery-ui-1.11.3.custom/jquery-ui',
        jstree: 'libs/jstree/dist/jstree',
        bootstrap: 'libs/bootstrap.min',
        datetimepicker: 'libs/bootstrap-datetimepicker.min',
        moment: 'libs/moment-with-locales.min',
        spin: 'libs/spin.min',
        misc: 'common/misc',
        markup: 'common/markup',
        log: 'common/log',
        stacktrace: 'libs/stacktrace',
        select2: 'libs/select2/select2.min',
        backbone: 'libs/backbone',
        backbone_queryparams: 'libs/backbone.queryparams',
        inputmask: 'libs/jquery.inputmask.bundle',
        underscore: 'libs/underscore',
        breadcrumbs: 'libs/breadcrumbs',
        channel: 'libs/channel',
        iconselect: 'libs/iconselect/iconselect',
        "ckeditor-core": 'libs/ckeditor/ckeditor',
        "ckeditor-jquery": 'libs/ckeditor/adapters/jquery',
        /*
         data_table : 'libs/data-tables-1.10.4/media/js/jquery.dataTables'
         */
        boot_table: 'libs/bootstrap-table-master/src/bootstrap-table',
        boot_table_ru: 'libs/bootstrap-table-master/src/locale/bootstrap-table-ru-RU',
        table_formatter_mixin: 'libs/mixin/bootstrap-table/formatter',
        jquery_cookie: 'libs/jquery-cookie-master/src/jquery.cookie',
        jszip: 'libs/jszip.min',
        filesaver: 'libs/FileSaver.min'
    },
    shim: {
        /* Set bootstrap dependencies (just jQuery) */
        'bootstrap': ['jquery'],
        'datetimepicker': ['jquery', 'bootstrap', 'moment'],
        'select2': ['jquery', 'bootstrap'],
        'jquery_cookie': ['jquery'],
        'jqueryUi': ['jquery'],
        'ckeditor-jquery': {
            deps: ['jquery', 'ckeditor-core']
        },

        /*
         'data_table' : ['jquery', 'bootstrap'],
         */

        'boot_table': ['jquery', 'bootstrap'],
        'boot_table_ru': ['jquery', 'bootstrap', 'boot_table'],
        'stacktrace': {
            exports: 'printStackTrace'
        },

        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ["underscore", "jquery"],
            exports: "backbone"
        },
        backbone_queryparams: ['backbone'],
        'inputmask': ['jquery'],
        'breadcrumbs': ['backbone']
    }
});


require(["jquery", "bootstrap", "boot", "jquery_cookie"], function ($, bootstrap, boot, jquery_cookie) {

    $(window).on('load reload resize', function () {
        // Высота фона
        $('body').css('min-height', $(window).height());
    });

    /*
     Run boot function
     */
    boot();
});