/**
 * Модуль начальной инициализации. Загружает блоки текущей страницы из базы и размещает их на шаблоне.
 */
define([
    'jquery',
    'log',
    'backbone',
    'underscore',
    './cms/view/SiteView',
    './cms/model/SiteModel',
    './cms/router'
], function ($, log, Backbone, _, SiteView, SiteModel, Route) {
    return function () {
        log.debug('Initialize Cms Site');

        // Создаем модель и вью сайта
        var siteModel = new SiteModel();
        var siteView = new SiteView({model: siteModel});

        // Создаем роутер, инициализируем моделью сайта, чтобы сделать возможным смену страницу из роутера
        var router = new Route({siteModel: siteModel});
        Backbone.history.start({pushState: true});
    }

});