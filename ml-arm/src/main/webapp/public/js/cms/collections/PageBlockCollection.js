define(
    ['log', 'misc', 'backbone', '../model/PageBlockModel'],
    function (log, misc, backbone, PageBlockModel) {
        var PageBlockCollection = backbone.Collection.extend({
            model: PageBlockModel,

            comparator: function(x,y){
                return x.get("blockInfo").get("orderNum") - y.get("blockInfo").get("orderNum");
            },

            checkAllInitialized : function(){
                var notInitialized = this.findWhere({initialized: false});
                return !notInitialized;
            },

            notifyPageBlocks : function(objectEvent){
                //TODO check pageBlock id's
                this.forEach(function(pageBlock){
                    pageBlock.trigger(objectEvent.triggerEvent, objectEvent.options);
                });
            }
        });


        return PageBlockCollection;
    });
