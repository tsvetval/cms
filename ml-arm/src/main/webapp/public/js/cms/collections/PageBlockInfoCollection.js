define(
    ['log', 'misc', 'backbone', '../model/PageBlockInfoModel'],
    function (log, misc, backbone, PageBlockInfoModel) {
        var PageBlockInfoCollection = backbone.Collection.extend({
            model: PageBlockInfoModel,

            checkAllInitialized : function(){
                var notInitialized = this.findWhere({initialized: false});
                return !notInitialized;
            }

        });

        return PageBlockInfoCollection;
    });
