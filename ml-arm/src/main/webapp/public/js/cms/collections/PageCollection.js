define(
    ['log', 'misc', 'backbone', '../model/PageModel'],
    function (log, misc, backbone, PageModel) {
        var PageCollection = backbone.Collection.extend({
            model: PageModel,

            getPageByPath: function (path, params) {
                return this.find(function(model) {
                    return model.get('path') == path;
                });
            },

            getPageByGUID: function (GUID) {
                return this.find(function(model) {
                    return model.get('guid') == GUID;
                });
            },

            notifyPageBlocks : function(objectEvent){
                if(objectEvent.notifyPage == 'all'){
                    this.forEach(function(pageModel){
                        pageModel.notifyPageBlocks(objectEvent);
                    });
                }else{
                    this.forEach(function(pageModel){
                        if(objectEvent.notifyPage.get("guid") == pageModel.get("guid")){
                            pageModel.notifyPageBlocks(objectEvent);
                        }
                    });
                }
            }

        });
        return PageCollection;
    });
