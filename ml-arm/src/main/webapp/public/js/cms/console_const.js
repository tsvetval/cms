define([],function(){
    WELCOME_PAGE = "/console/rest";
    OBJECT_VIEW_PAGE = "/console/rest/objectView";
    OBJECT_EDIT_PAGE = "/console/rest/objectEdit";
    OBJECT_CREATE_PAGE = "/console/rest/objectCreate";
    OBJECT_SELECT_PAGE = "/console/rest/objectSelect";
    HETERO_OBJECTS_SELECT_PAGE = "/console/rest/heteroObjectSelect";
    OBJECT_LINKED_LIST_PAGE = "/console/rest/objectLinkedList";
    REPORT_SELECT_OBJECT_PAGE = "/report/selectObject";

});