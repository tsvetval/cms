define(
    ['log', 'misc', 'backbone'],
    function (log, misc, backbone) {
        /**
         *
         * @param options передаваемый объект
         * @param triggerEvent имя эвента котороый будет распространен
         * @param notifyPage null -текущей странице, 'all' всем страницам, id страницы, [id..] - массив идентификаторов страниц
         * @param notifyPageBlock null - всем блокам, id блокуа, [id..] - массив идентификаторов
         */
        var NotifyEventObject = function(triggerEvent, options, notifyPage, notifyPageBlock, completeCallback){
           this.triggerEvent = triggerEvent;
           this.notifyPage = notifyPage;
           this.notifyPageBlock = notifyPageBlock;
           this.completeCallback = completeCallback;
           this.options  = options;
        };

        return NotifyEventObject;
    });
