define(
    ['log', 'misc', 'backbone'],
    function (log, misc, backbone) {
        /**
         *
         * @param hashParams передаваемый объект параметров для формирования хеша
         * @param modifyHistoryMode (по дефолту null)
         *          null - не трогать window.history,
         *          pushState - делать window.history.pushState
         *          replaceState - делать window.history.replaceState
         * @param extendHashMode расширять ли текущий хеш (по дефолту extendHash)
         *          'extendHash' - то есть если в хеше были параметры то хеш будет расширен новыми
         *          'replaceHash'- хеш будет сформирован на основании новых параметров
         */
        var OpenPageEventObject = function (hashParams, modifyHistoryMode, extendHashMode) {
            this.hashParams = params;
            this.modifyHistoryMode = modifyHistoryMode;
            this.extendHashMode = extendHashMode;
        };

        return OpenPageEventObject;
    });
