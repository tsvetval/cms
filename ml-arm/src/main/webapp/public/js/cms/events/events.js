/**
 *  Константы событий
 * */
define(
    [],
    function () {
        return {
            /**
             * Событие восстановления страиницы
             */
            RESTORE_PAGE : "restorePage",
            /**
             * Событие обновления страницы
             */
            REFRESH_PAGE : "refreshPage",

            /**
             * Событие открытия папки
             */
            OPEN_FOLDER : "openFolder",
            SEARCH : "search",
            GET_SELECTED_IDS : "getSelectedIds",
            RENDER_VIEW : "render",
            OPEN_SUB_FOLDER : "openSubFolder",
            ACTIVATE_FOLDER : "activateFolder"


        };
    });
