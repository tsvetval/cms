define(['log', 'misc', 'backbone', 'underscore'], function (log, misc, backbone, _) {
    var PageBlockInfoModel = backbone.Model.extend({
        defaults: {
            id : undefined,
            guid : undefined,
            zone : undefined,
            orderNum : undefined,
            bootJs : undefined,
            /*Хранит созданный инстанс модели*/
            pageBlock : undefined,
            url : undefined,
            /*Хранит класса (не инстансы) модели и вью*/
            backBoneClasses : undefined,
            /*Признак проведения инициализации (то ест получены модель и вью для данного блока)*/
            initialized : false

        },
        initialize: function(){
            console.log("initialize PageBlockModel");
            this.set('guid', _.uniqueId('PageBlockInfo'));

        },

        /**
         * Производит загрузку реальныой(кастомной) модели и вью для контент блока
         * @return возвращает promise вызов промиса пойдет с this(PageBlockInfo)
         */
        loadRealBlock : function(pageModel){
            var thisPageBlockInfo = this;
            var def = new $.Deferred();
            log.debug("Load bootBlockJs = " + thisPageBlockInfo.get('bootJs'));
            require([thisPageBlockInfo.get('bootJs')], function (bootClasses) {
                thisPageBlockInfo.set('backBoneClasses', bootClasses);
                var pageBlockModel = new bootClasses.model(
                    {
                        pageModel :  pageModel,
                        blockInfo : thisPageBlockInfo
                    }
                );
                log.debug('Create new RealPage Block Model');
                def.resolve(pageBlockModel);
            });
            return def.promise();
        }

    });

    return PageBlockInfoModel;
});
