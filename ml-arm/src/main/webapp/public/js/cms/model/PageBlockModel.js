define(['log', 'misc', 'backbone', 'cms/events/NotifyPageBlocksEventObject', 'cms/events/ChangePageHashEvent', 'cms/page_blocks/DialogPageBlock'],
    function (log, misc, backbone, NotifyEventObject, ChangePageHashEvent, Message) {

        var PageBlockModel = backbone.Model.extend({
            defaults: {
                id: undefined,
                zone: undefined,
                name: "Empty pageBlock",
                url: undefined,
                template: undefined,
                pageModel: undefined,
                active: undefined
            },
            initialize: function () {
                console.log("initialize PageBlockModel");

            },

            notifyPageBlocks: function (data) {
                this.trigger('NOTIFY_PAGE_BLOCKS', data)
            },

            navigationEvent: function (options) {
                this.trigger('NAVIGATION', options);
            },

            openPage: function (url, title, params) {
                this.trigger('OPEN_PAGE', {
                    title: title,
                    url: url,
                    params: params
                });
            },

            closePage: function () {
                this.trigger('CLOSE_PAGE', {});
            },

            replacePage: function (url, title, params) {
                this.trigger('REPLACE_PAGE', {
                    title: title,
                    url: url,
                    params: params
                });
            },

            /**
             *
             * @param hashParams передаваемый объект параметров для формирования хеша
             * @param modifyHistoryMode (по дефолту null)
             *          null - не трогать window.history,
             *          pushState - делать window.history.pushState
             *          replaceState - делать window.history.replaceState
             * @param extendHashMode расширять ли текущий хеш (по дефолту extendHash)
             *          'extendHash' - то есть если в хеше были параметры то хеш будет расширен новыми
             *          'replaceHash'- хеш будет сформирован на основании новых параметров
             */
            changePageHash: function (hashParams, modifyHistoryMode, extendHashMode) {
                this.trigger('CHANGE_PAGE_HASH', new ChangePageHashEvent(hashParams, modifyHistoryMode, extendHashMode));
            },
            //TODO может лучше ограничить только сменой pageTitle
            changePageParams: function (params) {
                this.trigger('CHANGE_PAGE_PARAMS', params);
            },


            updatePageTitle: function (title) {
                this.set("pageTitle", title);
                this.changePageParams({
                    pageTitle: title
                });
            },

            getBlockInfo: function () {
                return this.get("blockInfo");
            },

            getPageModel: function () {
                return this.get("pageModel");
            },

            /**
             * Вызов серверного экшена для данного блока. См. статический метод callPageBlockAction
             * @param options опции
             * @param callback колбек для вызова в случае успешного вызова
             */

            callServerAction: function (options, callback) {
                options.pageBlockId = this.getBlockInfo().get("id");
                options.pageId = this.getPageModel().get('id');
                options.ml_request = true;
                return PageBlockModel.callServerAction(this, options, callback);
            },

            displayErrorMessage: function (header, message) {
                var errorMessage = new Message({
                    title: header,
                    message: message,
                    type: 'errorMessage'
                });
                errorMessage.show();
            },

            displayExtendedErrorMessage: function (header, message, stacktrace) {
                var errorMessage = new Message({
                    title: header,
                    message: message,
                    stacktrace: stacktrace,
                    type: 'stacktraceMessage'
                });
                errorMessage.show();
            }

        });

        /*
         Статические методы-хелперы
         */

        /**
         * Вызов серверного метода страничного блока. После получения ответа сервера вызывается колбек, куда передаются
         * полученые данные.
         *
         * @param {Object} options мапа опций, обязательные поля - см. код
         * в опциях можно задавать обработчики ошибок:
         * onFail           -   универскальный обработчик, будет вызван если не заданы специфичные
         * onServerFail     -   обработчик ошибки сервера
         * onApplicationFail-   обработчик ошибки приложения
         * onSecurityFail   -   обработчик ошибки безопасности
         * onConnectionFail -   обработчик ошибки соединения
         * по умолчанию будт отображено окно с ошибкой
         *
         * @param {Function} callback колбэк, вызываемый в случае успешного выполнения экшена,
         *                 должен на вход получать данные возвращаемые сервером в json-формате
         *
         */
        PageBlockModel.callServerAction = function (pageblock, options, callback) {
            var _this = this;

            var pageBlockId = misc.option(options, "pageBlockId", "Идентификатор блока");
            var action = misc.option(options, "action", "Действие для выполнения на сервере");
            var data = misc.option(options, "data", "Дополнительные параметры запроса", {});
            var ml_request = misc.option(options, "ml_request", "Флаг внутренних запросов к серверу", true);

            data.pageBlockId = pageBlockId;
            data.action = action;
            data.ml_request = ml_request;
            var requestData = data;
            var def = new $.Deferred();
            $.ajax("page_block", {
                type: 'POST',
                timeout: options.timeout,
                data: data,
                success: function (responseData) {
                    if (responseData.error == 'server') {
                        console.log("Server Error: " + responseData.message);

                        if (options.onServerFail) {
                            options.onServerFail(responseData);
                        } else if (options.onFail) {
                            options.onFail(responseData);
                        } else {
                            if (responseData.stacktrace) {
                                console.log("Stack Trace: " + responseData.stacktrace);
                            } else {
                                responseData.stacktrace = "Дополнительные сведения отсутствуют."
                            }
                            pageblock.displayExtendedErrorMessage("Ошибка сервера", responseData.message, responseData.stacktrace);
                        }
                    } else if (responseData.error == 'application') {
                        console.log("Application Error: " + responseData.message);
                        if (options.onApplicationFail) {
                            options.onApplicationFail(responseData);
                        } else if (options.onFail) {
                            options.onFail(responseData);
                        } else {
                            pageblock.displayErrorMessage("Ошибка приложения", responseData.message);
                        }
                    } else if (responseData.error == 'security') {
                        console.log("Security Error: " + responseData.message);
                        if (options.onSecurityFail) {
                            options.onSecurityFail(responseData);
                        } else if (options.onFail) {
                            options.onFail(responseData);
                        } else {
                            pageblock.displayErrorMessage("Ошибка доступа", responseData.message);
                        }
                    } else {
                        if (responseData.needAuth) {
                            window.location = responseData.url;
                        } else {
                            if (callback) callback.apply(pageblock, [responseData, requestData]);
                            def.resolve(responseData);
                        }
                    }
                },
                error: function (result, textStatus, errorThrown) {
                    log.error('Error calling Page Block[id=' + data.pageBlockId +
                    "] action " + action + "." +
                    "\nRequest data:\n" + JSON.stringify(data) +
                    "\nResponse data: " + JSON.stringify(result) +
                    "\nStatus: " + textStatus +
                    "\nError: " + errorThrown);

                    if (XMLHttpRequest.readyState == 4) {
                        // HTTP error (can be checked by XMLHttpRequest.status and XMLHttpRequest.statusText)
                        if (options.onFail) {
                            options.onFail(result, textStatus, errorThrown)
                        }
                    }
                    else if (XMLHttpRequest.readyState == 0) {
                        // Network error (i.e. connection refused, access denied due to CORS, etc.)
                        if (options.onConnectionFail) {
                            options.onConnectionFail(result, textStatus, errorThrown)
                        } else if (options.onFail) {
                            options.onFail(result, textStatus, errorThrown)
                        }
                    } else {
                        if (options.onFail) {
                            options.onFail(result, textStatus, errorThrown)
                        }
                    }
                }
            });
            return def.promise();
        };

        return PageBlockModel;
    });
