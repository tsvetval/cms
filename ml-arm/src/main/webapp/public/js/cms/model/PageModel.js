define(
    ['log', 'misc', 'backbone', '../collections/PageBlockInfoCollection', '../model/PageBlockInfoModel', '../collections/PageBlockCollection', 'underscore', 'cms/events/NotifyPageBlocksEventObject', 'console_const'],
    function (log, misc, backbone, PageBlockInfoCollection, PageBlockInfo, PageBlockCollection, _, NotifyEventObject, console_const) {
        var PageModel = backbone.Model.extend({
            defaults: {
                url: undefined,
                /*Уникальный в рамках сессии(браузерного окна) идентифкатор страницы*/
                guid: undefined,
                /*Хеш страницы*/
                hashParams: undefined,
                /*Строковое представление хеша*/
                stringHash: undefined,
                name: "Empty page",
                /*Структурный шаблон страницы*/
                structureTemplate: undefined,
                initialized: false,
                pageBlockInfoCollection: undefined,
                pageBlockCollection: undefined,
                pageTitle: undefined,
                alreadyRendered: false,
                id: undefined
            },
            initialize: function () {
                console.log("initialize PageModel");
                this.set("pageBlockInfoCollection", new PageBlockInfoCollection());
                this.set("pageBlockCollection", new PageBlockCollection());
                this.set('guid', _.uniqueId('page'));
                this.set('pageTitle', '');
                this.set('hashParams', {});

            },


            /**
             * Производит загрузку структурного шаблона и всех контент блоков на страниук
             * @return {*} возвращает differed объект
             */
            initializePageData: function () {
                var thisPage = this;
                return $.when(this._getStructureTemplate(), this._loadPageBlocksInfo())
                    .then(function (structureTemplateAjax, resultPageBlockInfoAjax) {
                        if (structureTemplateAjax[0] && structureTemplateAjax[0].needAuth) {
                            // Сохраняем текущий урл в куке
                            $.cookie('urlBeforeAuth', window.location.href, {path: '/'});
                            window.location = structureTemplateAjax[0].url;
                        }
                        //TODO add check for success
                        if (resultPageBlockInfoAjax[0].error) {
                            document.write(resultPageBlockInfoAjax[0].errorMessage);
                        } else {
                            log.debug('Finish loading Structure Template from server');
                            thisPage.set({structureTemplate: structureTemplateAjax[0]});

                            log.debug('Finish loading PageBlockInfo from server');
                            resultPageBlockInfoAjax[0].pageBlocks.forEach(function (block) {
                                var pageBlockInfo = new PageBlockInfo({
                                    id: block.id,
                                    zone: block.zone,
                                    orderNum: block.orderNum,
                                    bootJs: block.bootJs
                                });
                                thisPage.get('pageBlockInfoCollection').add(pageBlockInfo);
                            });
                            thisPage.set('id', resultPageBlockInfoAjax[0].page.id);
                            thisPage.set('initialized', true);
                            // Проводим инициализацию реальных блоков возращаем
                            return thisPage._initializePageBlocks()
                        }
                    });
            },


            _getStructureTemplate: function () {
                var thisPage = this;
                // Грузим структурный шаблон
                log.debug('Start loading Structure Template from server');
                return $.ajax(misc.getContextPath() + (this.get('path').charAt(0) != '/' ? "/" : "")
                + this.get('path'), {
                    type: 'GET',
                    data: {
                        ml_request: true
                    }
                });

            },
            // получение данных о контент блоках getPageBlocks
            _loadPageBlocksInfo: function () {
                var thisPage = this;
                log.debug('Start loading PageBlockInfo from server');
                return $.ajax(misc.getContextPath() + (this.get('path').charAt(0) != '/' ? "/" : "")
                + this.get('path'), {
                    type: 'GET',
                    data: {
                        action: 'getPageBlocks',
                        ml_request: true
                    }
                });
            },
            /**
             * По заданному списку pageBlockInfoCollection производит их загрузку
             * @return {*} возвращает deffered объект
             */
            _initializePageBlocks: function () {
                var thisPage = this;
                log.debug('Start to initializePageBlocks');
                var blockInfoCollection = this.get('pageBlockInfoCollection');
                var deferredArray = blockInfoCollection.map(function (blockInfo, i) {
                    return blockInfo.loadRealBlock(thisPage)
                        .then(function (pageBlockModel) {
                            thisPage.get('pageBlockCollection').add(pageBlockModel);
                            thisPage.listenTo(pageBlockModel, 'NOTIFY_PAGE_BLOCKS', thisPage.onNotifyPageBlocks);
                            thisPage.listenTo(pageBlockModel, 'NAVIGATION', thisPage.onNavigation);
                            thisPage.listenTo(pageBlockModel, 'OPEN_PAGE', thisPage.openPage);
                            thisPage.listenTo(pageBlockModel, 'CLOSE_PAGE', thisPage.closePage);
                            thisPage.listenTo(pageBlockModel, 'REPLACE_PAGE', thisPage.replacePage);
                            thisPage.listenTo(pageBlockModel, 'CHANGE_PAGE_HASH',
                                function (event) {
                                    thisPage.modifyPageHash(event.hashParams, event.modifyHistoryMode, event.extendHashMode)
                                });
                            thisPage.listenTo(pageBlockModel, 'CHANGE_PAGE_PARAMS', thisPage.modifyPageParams);
                            thisPage.listenTo(pageBlockModel, 'showPageBlock', thisPage.showPageBlocks);
                            pageBlockModel.set("active", true);

                        });
                });
                return $.when.apply(null, deferredArray);
            },

            /**
             * Показать на спранице педжблоки. Метод найдет не загуженные, загрузит и оттобразит на странице. Загруженные
             * повторно загружаться не будут
             * data.folderId = id папки
             * data.pageBlocks = массив id пэйджблоков
             *
             * */
            showPageBlocks: function (data) {
                var _this = this;
                var pageBlockModels = new PageBlockCollection(); //модели которые должны быть показаны
                var pageBlockIdsForLoad = [];

                var allInfo = this.get("pageBlockCollection");
                //находим не загруженные блоки
                if (data.pageBlocks) {
                    data.pageBlocks.forEach(function (block) {
                        var existBlock = allInfo.filter(function (blk) {
                            return blk.get("blockInfo").get('id') == block;
                        });
                        if (existBlock.length > 0) {
                            var loadedBlock = existBlock[0];
                            pageBlockModels.add(loadedBlock);
                        } else {
                            pageBlockIdsForLoad.push(block);
                        }
                    });
                    //загружаем не загруженные блоки
                    $.when(_this.loadPageBlocks(pageBlockIdsForLoad, data.folderId)).then(function (loadedPageBlocks) {
                        pageBlockModels.add(loadedPageBlocks.toArray());
                        pageBlockModels.forEach(function (block) {
                            var pageBlockInZone = allInfo.filter(function (blk) {
                                return blk.get("blockInfo").get('zone') == block.get("blockInfo").get("zone");
                            });
                            pageBlockInZone.forEach(function (blk) {
                                blk.set("active", false);
                            });
                        });
                        pageBlockModels.forEach(function (block) {
                            block.set("active", true);
                        });
                        _this.trigger('showActiveBlocks');
                        loadedPageBlocks.forEach(function (block) {
                            block.trigger("restorePage", data);
                        });
                    });
                }  else {
                    this.get("pageBlockCollection").forEach(function (block) {
                        block.trigger("restorePage", data);
                    });
                }

            },
            /**
             * Загружает сначала PageBlockInfo по всем id, потом загужает  PageBlockModel для каждого
             *  pageBlockIdsForLoad = массив id пэйджблоков
             * */
            loadPageBlocks: function (pageBlockIdsForLoad, folderId) {
                var _this = this;
                var defered = $.Deferred();
                $.when(_this.loadPageBlockInfos(pageBlockIdsForLoad, folderId)).then(function (blockInfos) {
                    var pageBlockInfos = [];
                    blockInfos.forEach(function (block) {
                        var pageBlockInfo = new PageBlockInfo({
                            id: block.id,
                            zone: block.zone,
                            orderNum: block.orderNum,
                            bootJs: block.bootJs
                        });
                        pageBlockInfos.push(pageBlockInfo);
                        _this.get('pageBlockInfoCollection').add(pageBlockInfo);
                    });
                    var pageBlockModels = [];
                    var deferredArray = pageBlockInfos.map(function (blockInfo, i) {
                        return blockInfo.loadRealBlock(_this)
                            .then(function (pageBlockModel) {
                                _this.get('pageBlockCollection').add(pageBlockModel);
                                _this.listenTo(pageBlockModel, 'NOTIFY_PAGE_BLOCKS', _this.onNotifyPageBlocks);
                                _this.listenTo(pageBlockModel, 'NAVIGATION', _this.onNavigation);
                                _this.listenTo(pageBlockModel, 'OPEN_PAGE', _this.openPage);
                                _this.listenTo(pageBlockModel, 'CLOSE_PAGE', _this.closePage);
                                _this.listenTo(pageBlockModel, 'REPLACE_PAGE', _this.replacePage);
                                _this.listenTo(pageBlockModel, 'CHANGE_PAGE_HASH',
                                    function (event) {
                                        thisPage.modifyPageHash(event.hashParams, event.modifyHistoryMode, event.extendHashMode)
                                    });
                                _this.listenTo(pageBlockModel, 'CHANGE_PAGE_PARAMS', _this.modifyPageParams);
                                _this.listenTo(pageBlockModel, 'showPageBlock', _this.showPageBlocks);
                                pageBlockModels.push(pageBlockModel)
                            });
                    });

                    $.when.apply(window, deferredArray).then(function () {
                        var loadedPageBlockModels = new PageBlockCollection();
                        loadedPageBlockModels.add(pageBlockModels);
                        defered.resolve(loadedPageBlockModels);
                    });
                });
                return defered.promise();
            },

            loadPageBlockInfos: function (pageBlockIds, folderId) {
                return $.ajax(misc.getContextPath() + (this.get('path').charAt(0) != '/' ? "/" : "")
                + this.get('path'), {
                    type: 'GET',
                    data: {
                        action: 'getPageBlockByIDs',
                        pageBlockIds: JSON.stringify(pageBlockIds),
                        folderId: folderId,
                        ml_request: true
                    }
                });
            },

            /**
             * Обработчик события NOTIFY_PAGE_BLOCK, прижедшего с одного из пейдж блоков
             * @param notifyEventObject
             */
            onNotifyPageBlocks: function (notifyEventObject) {
                //TODO check page
                if (!notifyEventObject.notifyPage) {
                    // передаем событие пейдж блокам
                    this.notifyPageBlocks(notifyEventObject);
                } else {
                    // Передаем событие выше в сайт модель
                    this.trigger('NOTIFY_PAGE_BLOCKS', notifyEventObject);
                }

                if (notifyEventObject.completeCallback) {
                    notifyEventObject.completeCallback();
                }
            },

            onNavigation: function (options) {
                // Если меняется тайтл страницы то меняем его
                var _do = misc.option(options, "do", "Действие с навигацией");
                var title = misc.option(options, "title", "Заголовок страницы");
                if (_do == 'modify' && title) {
                    this.set('pageTitle', title);
                }
                // шлем навигационное соообщение дальше
                this.trigger('NAVIGATION', options);
            },

            openPage: function (event) {
                // Если меняется тайтл страницы то меняем его
//            var navigationParams = _.clone(event.params);
//            navigationParams['opener'] =  this.get('guid');
//            this.trigger('NAVIGATION',{
//                do:'push',
//                title: event.title,
//                url: event.url,
//                params:navigationParams
//            });
                // Передаем объект модели странцу открывыющую новую
                event['page_opener'] = this;
                this.trigger('OPEN_PAGE', event);


            },

            closePage: function (event) {
                var pageOpener = this.get("page_opener");
                if (pageOpener) {
                    this.trigger('ACTIVATE_PAGE', pageOpener);
                    this.trigger('REMOVE_PAGE', this);
                } else {
                    this.trigger("OPEN_PAGE", {url: WELCOME_PAGE, params: {}});
                    this.trigger('REMOVE_PAGE', this);
                }
            },

            replacePage: function (event) {
                event['page_opener'] = this.get('page_opener');
                this.trigger('OPEN_PAGE', event);
                this.trigger('REMOVE_PAGE', this);
            },


            notifyPageBlocks: function (notifyEventObject) {
                this.get('pageBlockCollection').notifyPageBlocks(notifyEventObject);
            },

            destroyObject: function () {
                this.trigger('destroy', this);
            },

            modifyPageHash: function (hashParams, modifyHistoryMode, extendHashMode) {
                if (!extendHashMode || extendHashMode == 'extendHash') {
                    _.extend(this.get('hashParams'), hashParams)
                } else {
                    this.set('hashParams', hashParams ? hashParams : {});
                }
                var hash = "#" + JSON.stringify(this.get('hashParams'));
                this.set('stringHash', hash);
                if (modifyHistoryMode === 'pushState') {
                    this.trigger('PUSH_STATE', this);
                    //TODO
                } else if (modifyHistoryMode === 'replaceState') {
                    this.trigger('REPLACE_STATE', this);
                }
                this.modifyPageParams(hashParams);
            },

            modifyPageParams: function (params) {
                var _this = this;
                _.map(params, function (v, k) {
                    _this.set(k, v);
                });
            }
        });

        return PageModel;
    });
