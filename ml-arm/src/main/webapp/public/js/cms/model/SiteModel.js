define(
    [
        'log',
        'misc',
        'backbone',
        'underscore',
        'channel',
        '../collections/PageCollection',
        '../model/PageModel',
        'cms/events/NotifyPageBlocksEventObject'
    ],
    function (log, misc, backbone, _, channel, PageCollection, PageModel, NotifyEventObject) {
        var SiteModel = backbone.Model.extend({
            defaults:   {
                currentUrl:     undefined,
                activePage:     undefined,
                pageCollection: undefined,
                breadcrumbs:    undefined
            },
            initialize: function () {
                console.log("Initialize SiteModel");
                this.set("pageCollection", new PageCollection());
            },

            addPage: function (path, params, page_opener) {
                log.debug('SiteModel - Create new SitePage');
                var thisSiteModel = this;
                var newPage = new PageModel(_.extend({
                    path:      path,
                    siteModel: this
                }, params));
                var hashParams = _.clone(params);
                if (page_opener) {
                    _.extend(hashParams, {opener: page_opener.get('guid')});
                }
                newPage.modifyPageHash(hashParams);
                newPage.set('page_opener', page_opener);

                this.get('pageCollection').add(newPage);
                this.listenTo(newPage, 'OPEN_PAGE', this.openPage);
                //TODO
                this.listenTo(newPage, 'REMOVE_PAGE', function (page) {
                    thisSiteModel.removePage(page, thisSiteModel.get('pageCollection'));
                });
                this.listenTo(newPage, 'ACTIVATE_PAGE', this.setActivePage);

                this.listenTo(newPage, 'NOTIFY_PAGE_BLOCKS', this.notifyPageBlocks);

                // Событие добавления нового состояиния в истории
                this.listenTo(newPage, 'PUSH_STATE', function (page) {
                    var newUrl = misc.getContextPath() +
                        (page.get('path') ? page.get('path') : '')
                        + page.get('stringHash');
                    log.debug('Push new State to History ' + newUrl);
                    window.history.pushState({pageGUID: page.get('guid')}, null, newUrl);
                });

                // Событие замены текущего состояиния в истории
                this.listenTo(newPage, 'REPLACE_STATE', function (page) {
                    var newUrl = misc.getContextPath() +
                        (page.get('path') ? page.get('path') : '')
                        + page.get('stringHash');
                    log.debug('Replace current state in History ' + newUrl);
                    window.history.replaceState({pageGUID: page.get('guid')}, null, newUrl);
                });

                this.listenTo(newPage, 'NAVIGATION', function (event) {
                    this.trigger('NAVIGATION', event)
                });


                this.listenTo(channel, 'FIND_MODEL', function (callback) {
                    alert('TODO SiteModel Alert');
                    callback(this);
                });


                return newPage.initializePageData().then(function () {
                    thisSiteModel.setActivePage(newPage, params);
                    newPage.notifyPageBlocks(new NotifyEventObject('restorePage', params));
                    //TODO кажеьтся нужно убрать
                });
            },

            openPage: function (event) {
                var _this = this;
                //чистим страницы
                var activeIndex = this.get('pageCollection').indexOf(this.get('activePage'));
                var pagesForRemove = this.get('pageCollection').rest(activeIndex + 1);
                _.forEach(pagesForRemove, function (page) {
                    _this.removePage(page, _this.get('pageCollection'));
                });

                //добавляем хеш в историю
//                var newUrl = window.location.pathname + '';
//                window.history.pushState(null, null, newUrl);
                this.addPage(event.url, event.params, event.page_opener);
            },

            removePage: function (page, collection) {
                //TODO удаляем страницу
                collection.remove(page);
                page.destroyObject();
            },

            setActivePage: function (page, params) {
                //todo: use params (need ot pass to page to make initialization)
                log.debug('Site Model Show page');
                var oldPage = this.get('activePage');
                if (oldPage) {
                    // Если уже активная страница
                    if (oldPage == page) {
                        var currentHashParams = JSON.stringify(params.parsedHash);
                        var pageHashParams = JSON.stringify(page.get('hashParams'));
                        if(currentHashParams &&  pageHashParams!= currentHashParams){
                            page.notifyPageBlocks(new NotifyEventObject('refreshPage', JSON.parse(currentHashParams)));
                        }
                        return;
                    }
                    oldPage.set('activePage', false);
                }
                var newUrl = misc.getContextPath() +
                    (page.get('path') ? page.get('path') : '')
                    + page.get('stringHash');

                var skipPushState = misc.option(params, "skipPushState", "Не делать push state", false);
                if (!skipPushState) {
                    log.debug('Push new State to History ' + newUrl);
                    window.history.pushState({pageGUID: page.get('guid')}, null, newUrl);
                }

                page.set('activePage', true);
                this.set('activePage', page);

                if (page.get('alreadyRendered')) {
                    this.trigger('showPage', page);
                } else {
                    this.trigger('renderNewPage', page);
                }
            },

            getPageByPath: function (path, params) {
                // Проверяем нет ли на сайте такой же страницы
                var existsPage = this.get('pageCollection').getPageByPath(path, params);
                return  existsPage;
            },

            getPageByGUID: function (UID) {
                var existsPage = this.get('pageCollection').getPageByGUID(UID);
                return  existsPage;
            },

            notifyPageBlocks: function (notifyEventObject) {
                this.get('pageCollection').notifyPageBlocks(notifyEventObject);
            }


        });

        return SiteModel;
    });
