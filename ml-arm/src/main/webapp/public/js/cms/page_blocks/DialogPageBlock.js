/**
 * отображает диалог. Не используется
 *
 */
define(['jquery'],function($){
    var $dimmer;
    var $okButton;


    var DialogPageBlock = function(data){
        $dimmer = $('<div id="dialog" class="modal dimmer">');

        var $dialog = $('<div class="modal-dialog">');
        $dialog.appendTo($dimmer);

        var $content = $('<div class="modal-content">');
        $content.appendTo($dialog);

        var $header = $('<div class="modal-header">');
        $header.appendTo($content);

        var $body = $('<div class="modal-body">');
        $body.appendTo($content);

        var $footer = $('<div class="modal-footer">');
        $footer.appendTo($content);

        var $dialogHeader = $('<button type="button" class="close" onclick="this.parentNode.parentNode.parentNode.parentNode.style.display = \'none\';">×</button><h2 class="modal-title">' + data.title +'</h2>');
        $dialogHeader.appendTo($header);

        var $dialogBody = $('<div>' + data.message.split('\n').join('<br/>') + '</div>');
        $dialogBody.appendTo($body);

        if(data.type == 'stacktraceMessage'){
            var $stackTraceCaption = $('<div class="stacktrace-header">Дополнительные сведения:</div>');
            $stackTraceCaption.appendTo($body);
            var $stackTraceBody = $('<div class="stacktrace-container"><code>' + data.stacktrace.split('\n').join('<br/>') + '</code></div>');
            $stackTraceBody.appendTo($body);
            $stackTraceBody.hide();
            $stackTraceCaption.on('click', function(){
                var $this = $(this);
                if($this.hasClass('expanded')){
                    $this.removeClass('expanded');
                    $stackTraceBody.slideUp();
                } else {
                    $this.addClass('expanded');
                    $stackTraceBody.slideDown();
                }
            });
        }

        $okButton = $('<button type="button" class="btn dialog-button btn-primary ok-btn">OK</button>');
        $okButton.appendTo($footer);

        if(data.type == 'dialogMessage'){
            $cancelButton = $('<button type="button" class="btn dialog-button btn-default ok-cancel" onclick="this.parentNode.parentNode.parentNode.parentNode.style.display = \'none\'">Отмена</button>');
            $cancelButton.appendTo($footer);
        }

        $dimmer.appendTo($('body'))
    };

    DialogPageBlock.prototype.hide = function(){
        $dimmer.fadeOut();
    };

    DialogPageBlock.prototype.show = function(options){
        var _this = this;
        $dimmer.find('.ok-btn').off('click').on('click', function(){
            if(options && typeof options.okAction == 'function'){
                options.okAction.call();
            }
            _this.hide();
        });
        $dimmer.fadeIn();
        $dimmer.find('.ok-btn').focus();
    };

    return DialogPageBlock;
});
