/**
 * Выпадающее меню. Пока не используется.
 */
define(['jquery', 'misc'], function ($, misc) {

    var EntityDropdownPageBlock = function (eventManager) {
        var _this = this;
        this.eventManager = eventManager;
        this.html = $('<nav class="navbar navbar-default" role="navigation"></nav>');
        var $ul = $('<ul class="nav nav-pills"></ul>');
        this.html.append($ul);
        $.ajax(misc.getContextPath() + '/rest/objects/root/children', { //todo: hardcoded
            success: function (children) {
                for (var i = 0; i < children.length; i++) {
                    _this.addEntity(children[i], $ul);
                }
            }
        });

    };

    EntityDropdownPageBlock.prototype = new function () {

        var level = 0;

        this.addEntity = function (entity, $parentUl, level) {
            if (!level) level = 0;
            var _this = this;
            if (entity.children == 0) {
                var $link = $('<a href="#" >' + entity.props.text + '</a>');
                $link.click(function () {
                    _this.eventManager.trigger(_this.eventManager.events.NEW_CURRENT_ENTITY, entity);
                });
                $parentUl.append($('<li></li>').append($link));
            }
            else {
                var $newParentUl = $('<ul class="dropdown-menu"></ul>');
                var $label = $('<a href="#" data-toggle="dropdown" class="dropdown-toggle">' + entity.props.text + (level == 0 ? '<b class="caret"></b>' : '') + '</a>');
                var liClass = level == 0 ? "dropdown" : "dropdown-submenu";
                $parentUl.append($("<li class='" + liClass + "'></li>")
                    .append($label)
                    .append($newParentUl
                        .append($('<li><img src="public/js/libs/jstree/dist/themes/default/throbber.gif"/></li>')))); //todo: change image!
                var loading = false;
                $label.mouseover(function () {
                    if (!loading) {
                        loading = true;
                        $.ajax(misc.getContextPath() + '/rest/objects/' + entity.id + '/children', {
                            success: function (children) {
                                $newParentUl.empty();
                                for (var i = 0; i < children.length; i++) {
                                    _this.addEntity(children[i], $newParentUl, level + 1);
                                }
                            }
                        });
                    }
                });
                return $newParentUl;
            }
        };

        this.appendTo = function ($to) {
            $to.append(this.html);
        }
    };

    return EntityDropdownPageBlock;
});


