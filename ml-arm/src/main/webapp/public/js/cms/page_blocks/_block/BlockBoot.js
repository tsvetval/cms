define(
    ['log', 'misc', 'backbone', 'cms/page_blocks/_block/model/BlockModel', 'cms/page_blocks/_block/view/BlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });
