define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockModel', 'cms/page_blocks/DialogPageBlock'],
    function (log, misc, backbone, PageBlockModel, Message) {
        var model = PageBlockModel.extend({
            defaults: {
                renderTemplate: undefined
            },

            initialize: function () {
                console.log("initialize BlockModel");

                this.listenTo(this, 'restorePage', function (params) {
                    var objectId = params.objectId;
                    this.prompt(objectId);
                });
            },

            prompt: function (objectId) {
                var _this = this;
                var options = {
                    action: "show",
                    data: {
                        objectId: objectId
                    }
                };

                var callback = function (result) {
                    _this.set('renderTemplate', result.html);
                    _this.trigger('render');
                };

                return this.callServerAction(options, callback);
            },
            action: function (params) {
                var _this = this;
                var options = {
                    action: "action",
                    data: {
                        param: params.param
                    }
                };

                var callback = function (result) {
                    _this.set(data, resuilt.data)
                };

                return this.callServerAction(options, callback);
            }
        });
        return model;
    });
