define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup', 'cms/page_blocks/DialogPageBlock', 'moment'],
    function (log, misc, backbone, PageBlockView, markup, Message) {
        var view = PageBlockView.extend({

            initialize:function () {
                console.log("initialize BlockView");
                this.listenTo(this.model, 'render', this.render);
                this.listenTo(this.model, 'change:data', this.update);
            },

            render:function () {
                var _this = this;
                this.$el.html(this.model.get('renderTemplate'));
                markup.attachActions(this.$el, {
                    actionClick: function () {
                        var params = {
                            param: 1
                        };
                        _this.model.action(params);
                    }
                });

            }

        });
        return view;
    });
