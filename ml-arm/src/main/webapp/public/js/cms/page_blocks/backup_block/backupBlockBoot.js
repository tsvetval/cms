/**
 * Блок резервного копирования БД при помощи репликации. Позволяет собрать пакет обновлений начиная с определенной даты.
 * Контроллер: ru.peak.ml.web.block.controller.impl.BackupBlockController
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/backup_block/model/BackupBlockModel',
        'cms/page_blocks/backup_block/view/BackupBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
