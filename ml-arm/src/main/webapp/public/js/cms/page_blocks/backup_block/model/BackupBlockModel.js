/**
 * Модель блока резервного копирования БД
 */
define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockModel', 'cms/page_blocks/DialogPageBlock'],
    function (log, misc, backbone, PageBlockModel, Message) {
        var model = PageBlockModel.extend({
            defaults: {
                renderTemplate: undefined
            },

            /**
             * инициализация модели
             */
            initialize: function () {
                console.log("initialize BlockModel");

                this.listenTo(this, 'restorePage', function (params) {
                    this.prompt();
                });
            },

            /**
             * запрос шаблона для отображения блока
             */
            prompt: function () {
                var _this = this;
                var options = {
                    action: "show"
                };

                var callback = function (result) {
                    _this.set('renderTemplate', result.html);
                    _this.trigger('render');
                };

                return this.callServerAction(options, callback);
            },

            /**
             * запрос на создание резервной копии БД
             */
            backup: function (data) {
                var _this = this;
                var options = {
                    action: "backup",
                    data: {
                        dateFrom: data.dateFrom,
                        dateTo: data.dateTo,
                        includeInstances: data.includeInstances
                    }
                };

                var callback = function (result) {
                    _this.showBackupComplete(result);
                };

                return this.callServerAction(options, callback);
            },

            /**
             * отображение результатов создания резервной копии БД
             */
            showBackupComplete: function (result) {
                var modelResultMessage = undefined;
                var instanceResultMessage = undefined;

                if (result.replicationModelCount && result.replicationModelName) {
                    modelResultMessage = "Создан блок репликации структуры БД " +
                    "с именем:\n" + result.replicationModelName + "\n" +
                    "Объектов в блоке: " + result.replicationModelCount;
                }

                if (result.replicationInstanceCount && result.replicationInstanceName) {
                    instanceResultMessage = "Создан блок репликации данных" +
                    "с именем:\n" + result.replicationInstanceName + "\n" +
                    "Объектов в блоке: " + result.replicationInstanceCount;
                }

                var message = undefined;
                if (modelResultMessage) {
                    message = modelResultMessage;
                }
                if (instanceResultMessage) {
                    if (message) {
                        message += "\n\n"
                    }
                    message += instanceResultMessage;
                }

                var title = undefined;

                if (!message) {
                    title = "Подготовка резервного копирования БД за период с " +
                    result.dateFrom + " по " + result.dateTo + " " + "завершена с ошибкой";
                    message = "В БД отсутствуют данные, измененные за указанный период"
                } else {
                    title = "Подготовка резервного копирования БД за период с " +
                    result.dateFrom + " по " + result.dateTo + " " + "успешно завершена";
                }

                var dialog = new Message({
                    title: title,
                    message: message,
                    type: 'infoMessage'
                });
                dialog.show();
            }
        });
        return model;
    });
