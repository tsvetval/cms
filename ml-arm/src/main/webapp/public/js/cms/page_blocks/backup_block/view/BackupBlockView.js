/**
 *  Представление блока создания резервной копии БД
 */
define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup', 'cms/page_blocks/DialogPageBlock', "datetimepicker", "moment"],
    function (log, misc, backbone, PageBlockView, markup, Message) {
        var view = PageBlockView.extend({

            /**
             * инициализация представления
             */
            initialize: function () {
                console.log("initialize BlockView");
                this.listenTo(this.model, 'render', this.render);
            },

            /**
             * отображение представления
             */
            render: function () {
                var _this = this;
                this.$el.html(this.model.get('renderTemplate'));

                this.$el.find('#dateFrom, #dateTo').datetimepicker({
                    format: 'DD.MM.YYYY',
                    useCurrent: true,
                    locale: 'ru'
                });

                markup.attachActions(this.$el, {
                    backupClick: function () {
                        var data = {
                            dateFrom: _this.$el.find('#dateFrom').val(),
                            dateTo: _this.$el.find('#dateTo').val(),
                            includeInstances: _this.$el.find('#includeInstances').is(':checked')
                        };
                        _this.model.backup(data);
                    },
                    cancelClick: function () {
                        _this.closePage();
                    }
                });

            }

        });
        return view;
    });
