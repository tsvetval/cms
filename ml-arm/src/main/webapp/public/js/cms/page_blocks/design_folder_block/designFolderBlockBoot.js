define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/design_folder_block/model/DesignFolderBlockModel',
        'cms/page_blocks/design_folder_block/view/DesignFolderBlockView'
    ],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
