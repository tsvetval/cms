define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockModel',
        'cms/page_blocks/list_block/object_list_block/model/ObjectListBlockModel',
        'cms/page_blocks/DialogPageBlock'],
    function (log, misc, backbone, PageBlockModel,
              ObjectListBlockModel,
              Message) {
        var model = ObjectListBlockModel.extend({

            defaults: {},

            initialize: function () {
                console.log("initialize BlockModel");
                var _this = this;
                this.listenTo(this, 'restorePage', function (params) {
                    if (params.folderId) {
                        _this.set('folderId', params.folderId);
                        _this._reset();
                        _this._update();
                    }
                });

                this.listenTo(this, 'openFolder', function (params) {
                    log.debug("DesignFolderBlockModel [openFolder]");
                    if (params.folderId) {
                        _this.set('folderId', params.folderId);
                        _this._reset();
                        _this._update();
                    }
                });
            },


            _update: function () {
                var _this = this;
                $.when(_this._loadListData()).then(
                    function (result) {
                        if (result.CurrentFolder) {
                            _this.set("currentFolder", result.CurrentFolder);
                        } else {
                            _this.unset("currentFolder");
                        }
                        if (result.FolderListData) {
                            _this.set("folderList", result.FolderListData);
                        } else {
                            _this.unset("folderList");
                        }
                        _this.trigger('render');
                    }
                );
            }




        });
        return model;
    });
