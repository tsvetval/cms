<div class="ml-main">
    <div class="ml-main__header m-main__header_breadcrumbs">
        <div class="ml-main__header-bg"></div>
        <div class="ml-main__header-breadcrumbs-wrap breadCrumbHolder module">
            <div id="breadCrumb0" class="ml-main__header-breadcrumbs breadCrumb module">
                <ul>
                    <li>
                        <a href="#"></a>
                    </li>
                    <li>
                        <a href="#">Разработка</a>
                    </li>
                    <li>
                        <a href="#">Верстка</a>
                    </li>
                    <li>
                        <a href="#">Блоки навигации</a>
                    </li>
                    <li>
                        <a href="#">Разработка</a>
                    </li>
                    <li>
                        Папки
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- .ml-main__header -->
    <div class="ml-main__content">
        <div class="ml-main__content-top">
            <div class="ml-main__content-top-title">
                <div class="ml-main__content-top-title-text">Разработка</div>
                <div class="ml-main__content-top-title-items">16 объектов</div>
            </div>
            <div class="ml-main__content-top-add-folder">
                <div class="ml-main__content-top-add-folder-icon"></div>
                <div class="ml-main__content-top-add-folder-title">Создать папку</div>
            </div>
            <div class="ml-main__content-top-actions">
                <div class="ml-main__content-top-actions-title">Действия</div>
                <ul class="ml-main__content-top-actions-list">
                    <li class="m-main__content-top-actions-list_reinitial">Переинициализация</li>
                    <li class="m-main__content-top-actions-list_log">Показать лог</li>
                    <li class="m-main__content-top-actions-list_export">Экспортировать</li>
                </ul>
            </div>
        </div>
        <div class="ml-main__content-folders">
            <!--
                        <div class="ml-main__content-folder col-md-2">Системные классы</div>
                        <div class="ml-main__content-folder col-md-2">Верстка</div>
                        <div class="ml-main__content-folder col-md-2">Тестирование<span>3</span></div>
                        <div class="ml-main__content-folder col-md-2">Прикладные классы</div>
                        <div class="ml-main__content-folder col-md-2">Папки</div>
                        <div class="ml-main__content-folder col-md-2 m-main__content-folder_type_documents">Документы</div>
                        <div class="ml-main__content-folder col-md-2 m-main__content-folder_type_directories">
                            Справочники<span>12</span></div>
                        <div class="ml-main__content-folder col-md-2">Модули</div>
                        <div class="ml-main__content-folder col-md-2">Системные классы</div>
            -->
        </div>
        <div class="ml-main__content-logo">Versoin 1.232.22</div>
    </div>
    <!-- .ml-main__content -->
</div>