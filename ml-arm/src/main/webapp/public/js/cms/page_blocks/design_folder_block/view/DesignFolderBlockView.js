define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup', 'cms/page_blocks/DialogPageBlock', 'moment',
        'text!cms/page_blocks/design_folder_block/template/DesignFolderTemplate.tpl',
        'text!cms/page_blocks/design_folder_block/template/folder.tpl'
    ],
    function (log, misc, backbone, PageBlockView, markup, Message, moment,
              BlockTemplate, FolderTemplate) {
        var view = PageBlockView.extend({

            events: {},

            initialize: function () {
                console.log("initialize BlockView");
                this.listenTo(this.model, 'render', this.render);
            },

            render: function () {
                this.$el.html(_.template(BlockTemplate, {model: this.model}));
                $('.ml-main__content').css('min-height', $(window).height() - 115); // где 115 = 60 (высота header'а) + 40 (высота блока .ml-main__header) + 15 (отступ от низа страницы)
                this.updateCurrentFolder();
                this.updateFolderList();
            },

            updateFolderList: function () {
                var $folderContainer = this.$el.find(".ml-main__content-folders");
                $folderContainer.empty();

                var $folderInfo = this.$el.find(".ml-main__content-top-title-items");
                $folderInfo.empty();

                var folderData = this.model.get('folderList');
                var folderList = [];
                if (folderData) {
                    folderList = this.model.get('folderList').objectList;
                    $.each(folderList, function (index, folder) {
                        var $folder = $(_.template(FolderTemplate, {"folder": folder}));
                        $folderContainer.append($folder);
                    });
                }

                $folderInfo.html(folderList.length + " объектов")
            },

            updateCurrentFolder: function () {
                var currentFolder = this.model.get('currentFolder');
                console.dir(currentFolder);
                this.$el.find(".ml-main__content-top-title-text").html(currentFolder.title)
            }

        });
        return view;
    });
