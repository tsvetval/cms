define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/design_navigation_block/model/DesignNavigationBlockModel',
        'cms/page_blocks/design_navigation_block/view/DesignNavigationBlockView'
    ],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
