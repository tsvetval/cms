/**
 * Модель базового блока отображения объекта в форме
 */
define(
    ['log', 'misc', 'backbone', 'underscore',

        'cms/model/PageBlockModel', 'cms/events/NotifyPageBlocksEventObject',
        'cms/page_blocks/form_block/model/AttrModel',
        'cms/page_blocks/form_block/model/AttrGroupModel',
        'cms/page_blocks/form_block/collections/AttrCollection',
        'cms/page_blocks/form_block/collections/AttrGroupCollection'],
    function (log, misc, backbone, _, PageBlockModel, NotifyEventObject,
              AttrModel, AttrGroupModel, AttrCollection, AttrGroupCollection) {
        var model = PageBlockModel.extend({
            defaults: {
                /*отсортированая Коллекция атрибутов отображаемых на форме*/
                objectId: undefined,
                className: undefined,
                renderTemplate: undefined,
                title: undefined,

                canCreate:undefined,
                canDelete:undefined,
                canEdit:undefined,


                attrList: undefined,
                nonGroupAttrList: undefined,
                groupList: undefined,
                rootGroupList: undefined,
                _tabGroup: undefined,
                pageTitle: 'Просмотр/Редактирование/Создание объекта '
            },

            /**
             * Инициализация модели
             */
            initialize: function () {
                log.debug("initialize FormBlockModel");
                this.set('attrList', new AttrCollection());
                this.set('nonGroupAttrList', new AttrCollection());
                this.set('groupList', new AttrGroupCollection());
                this.set('rootGroupList', new AttrGroupCollection());
                this.initializeListeners();
            },

            /**
             * Инициализация обработчиков событий
             */
            initializeListeners: function () {
                var _this = this;
                /**
                 * Обработчик события "restorePage" — открытие страницы
                 */
                this.listenTo(this, 'restorePage', function (params) {
                    log.debug("FormBlockModel start restorePage");
                    // set params from hash to attrs
                    for (var propName in params) {
                        _this.set(propName, params[propName]);
                    }

                    $.when(_this._loadMetaData(params),
                        _this._loadObjectData(params)).then(
                        function (resultMetaData, resultObjectData) {
                            _this._fillModel(resultMetaData.MetaData);
                            _this._fillModelValues(resultObjectData.ObjectData);
                            _this.set('canCreate',resultObjectData.canCreate);
                            _this.set('canDelete',resultObjectData.canDelete);
                            _this.set('canEdit',resultObjectData.canEdit);
                            var title = _.template(_this.pageTitle, {data: resultObjectData.ObjectData});
                            _this.updatePageTitle(title);
                            _this.trigger('render');
                        }
                    );
                });

                /**
                 * Обработчик события "refreshPage" — обновление страницы
                 */
                this.listenTo(this, 'refreshPage', function () {
                    log.debug("FormBlockModel start refreshPage");
                    var params = {
                        refAttrId: _this.get('refAttrId'),
                        objectId: _this.get('objectId'),
                        className: _this.get('className')
                    };
                    $.when(_this._loadMetaData(params),
                        _this._loadObjectData(params)).then(
                        function (resultMetaData, resultObjectData) {
                            _this._fillModel(resultMetaData.MetaData);
                            _this._fillModelValues(resultObjectData.ObjectData);
                            _this.trigger('render');
                        });
                });
            },

            /**
             * Получение данных объекта (списка значений атрибутов)
             *
             * @param params возможны следующие варианты
             *              {refAttrId, [refObjectId]}
             *              {className, [objectId]}
             * @returns {*}
             */
            _loadObjectData: function (params) {
                var _this = this;
                var data = $.extend({pageBlockId: this.get('blockInfo').get('id'), ml_request: true}
                    , params);
                var options = {
                    action: 'getObjectData',
                    data: data
                };
                return this.callServerAction(options);
            },

            /**
             * Получение метаданных объекта (спска атрибутов)
             *
             * @param params  возможны следующие варианты
             *              {refAttrId, [refObjectId]}
             *              {className, [objectId]}
             * @returns {*}
             */
            _loadMetaData: function (params) {
                var _this = this;
                var data = $.extend({pageBlockId: this.get('blockInfo').get('id'), ml_request: true}
                                    , params);
                var options = {
                    action: 'getMetaData',
                    data: data
                };
                return this.callServerAction(options);
            },


            /**
             * Заполнение метаданных модели
             *
             * @param metaData
             * @private
             */
            _fillModel: function (metaData) {
                var _this = this;
                this.set('entityName', metaData.entityName);
                this.set('entityId', metaData.entityId);
                this.set('description', metaData.description);
                // Create Groups
                if (metaData.groupList) {
                    // создание групп атрибутов
                    metaData.groupList.forEach(function (groupInfo) {
                            var group = new AttrGroupModel(groupInfo);
                            _this.get('groupList').add(group);
                            if (!group.getParentGroupId()) {
                                _this.get('rootGroupList').add(group);
                            }
                        }
                    );
                    // добавление подгрупп к группам
                    _this.get('groupList').each(function (groupModel) {
                            if (!groupModel.getParentGroupId()) {
                                _this.get('rootGroupList').add(groupModel);
                            } else {
                                var parentGroup = _this.get('groupList').get(groupModel.getParentGroupId())
                                parentGroup.get('subGroupCollection').add(groupModel)
                            }
                        }
                    );
                }

                // Создаем атрибуты
                if (metaData.attrList) {
                    metaData.attrList.forEach(function (attrInfo) {
                            var attr = new AttrModel(attrInfo);
                            attr.set('className', _this.get('entityName'));
                            attr.set('pageBlockModel', _this);
                            _this.get('attrList').add(attr);
                            if (!attr.getAttrGroupId()) {
                                _this.get('nonGroupAttrList').add(attr);
                            } else {
                                var ownerGroup = _this.get('groupList').get(attr.getAttrGroupId());
                                ownerGroup.get('attrCollection').add(attr);

                            }
                            // Listen to Attr Events
                            _this.listenTo(attr, 'OPEN_PAGE', function (event) {
                                _this.openPage(event.url, event.title, event.params)
                            });
                        }
                    );
                }
            },

            /**
             * Заполнение данных объекта модели (значений атрибутов)
             *
             * @param ObjectData
             * @private
             */
            _fillModelValues: function (ObjectData) {
                var _this = this;
                this.set('title', ObjectData.title);
                // Для каждого атрибута выставляем значение
                this.get('attrList').forEach(function (attr) {
                    if (typeof ObjectData.attrValues[attr.getAttrEntityFiledName()] !== 'undefined') {
                        attr.setAttrValue(ObjectData.attrValues[attr.getAttrEntityFiledName()])
                    } else {
                        attr.setAttrValue(undefined);
                    }
                });
            },

            /**
             * Загрузка файлов (значение атрибутов типа FILE) на сервер
             *
             * @param fileAttrList
             * @param callback
             * @returns {boolean}
             */
            uploadFiles: function (fileAttrList, callback) {
                if (!fileAttrList || fileAttrList.length == 0) {
                    callback();
                    return true;
                }
                console.log('Start uploading files: ');
                var xhr = new XMLHttpRequest();
                var url = misc.getContextPath() + "/upload";
                xhr.onreadystatechange = function (e) {
                    if (4 == this.readyState) {
                        console.log('Files upoaded: ' + e.target.responseText);
                        callback();
                    }
                };
                xhr.open('post', url, true);
                var fd = new FormData;
                $.each(fileAttrList, function (index, fileAttr) {
                    if (!fileAttr.get('value').deleted && fileAttr.get('value').file) {
                        fd.append(fileAttr.get('value').fileName, fileAttr.get('value').file);
                    }
                });
                xhr.send(fd);
            },

            /**
             * Возвращает значение атрибута по имени
             *
             * @param attrName      -   имя атрибута (entityFieldName)
             * @returns             -   значение атрибута
             */
            getAttrValue: function (attrName) {
                var value = undefined;
                this.get('attrList').forEach(function (attr) {
                    if (attrName == attr.getAttrEntityFiledName()) {
                        value = attr.get('value');
                    }
                });
                return value;
            },

            /**
             * Устанавливает значение атрибута по имени
             *
             * @param attrName      -   имя атрибута (entityFieldName)
             * @param value         -   значение атрибута
             * @returns {boolean}   -   результат выполнения операции
             */
            setAttrValue: function (attrName, value) {
                this.get('attrList').forEach(function (attr) {
                    if (attrName == attr.getAttrEntityFiledName()) {
                        attr.set('value', value);
                        attr.trigger('render');
                        return true;
                    }
                });
                return false;
            },

            /**
             * Установка фокуса формы на атрибуте с указанным именем
             *
             * @param attrName      -   имя атрибута (entityFieldName)
             * @returns {boolean}   -   результат выполнения операции
             */
            setAttrFocus: function (attrName) {
                log.debug('setAttrFocus ' + attrName);
                this.get('attrList').forEach(function (attr) {
                    if (attrName == attr.getAttrEntityFiledName()) {
                        attr.trigger('focus');
                        return true;
                    }
                });
                return false;
            },

            /**
             * Скрыть атрибут с указанным именем
             *
             * @param attrName      -   имя атрибута (entityFieldName)
             */
            hideAttr: function (attrName) {
                this.get('attrList').forEach(function (attr) {
                    if (attrName == attr.getAttrEntityFiledName()) {
                        attr.hide();
                    }
                });
            },

            /**
             * Отобразить атрибут с указанным именем
             *
             * @param attrName      -   имя атрибута (entityFieldName)
             */
            showAttr: function (attrName) {
                this.get('attrList').forEach(function (attr) {
                    if (attrName == attr.getAttrEntityFiledName()) {
                        attr.show();
                    }
                });
            },

            /**
             * Обновить значение атрибута с указанным именем
             *
             * @param attrName      -   имя атрибута (entityFieldName)
             */
            refreshAttr: function (attrName) {
                this.get('attrList').forEach(function (attr) {
                    if (attrName == attr.getAttrEntityFiledName()) {
                        attr.refresh();
                    }
                });
            },

            /**
             * Скрыть атрибуты из заданного списка
             *
             * @param list  -   список имен атрибутов (entityFieldName)
             */
            hideAttrList: function (list) {
                this.get('attrList').forEach(function (attr) {
                    if ($.inArray(attr.getAttrEntityFiledName(), list) != -1) {
                        attr.hide();
                    } else {
                        attr.show();
                    }
                });
            },

            /**
             * Получение группы атрибутов по id
             *
             * @param id            -   идентификатор группы
             * @returns {undefined} -   группа
             */
            getGroupById: function (id) {
                var group = undefined;
                this.get('groupList').forEach(function (_group) {
                    if (id == _group.get("id")) {
                        group = _group;
                    }
                });
                return group;
            }

        });

        return model;
    });
