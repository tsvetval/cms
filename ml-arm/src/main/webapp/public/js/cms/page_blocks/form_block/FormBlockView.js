/**
 * Представление базового блока отображения объекта в форме
 */
define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'cms/page_blocks/DialogPageBlock',
        'cms/page_blocks/form_block/factories/GroupViewFactory',
        'cms/page_blocks/form_block/factories/AttrViewFactory',
        'cms/page_blocks/form_block/collections/AttrGroupCollection',
        'console_const'
    ],
    function (log, misc, backbone, PageBlockView, Message,
                GroupViewFactory,
                AttrViewFactory,
                AttrGroupCollection,
                console_const
        ) {
        var view = PageBlockView.extend({

            events : {
                "dblclick .form-title" : "openObjectClass"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                console.log("initialize FormBlockView");
                this.viewMode = undefined;
            },

            /**
             * Обработчик двойного клика по заголовку (открытие класса)
             */
            openObjectClass: function () {
                this.model.openPage(OBJECT_VIEW_PAGE, 'Просмотр класса...', {objectId : this.model.get('entityId'), className:"MlClass"})
            },

            /**
             * Отрисовка атрибутов без группы
             *
             * @param $content_container
             * @private
             */
            _renderNonGroupAttrs : function($content_container){
                var _this = this;
                // Выводим все атрибуты без групп
                var outAttrContainer = undefined;
                for (var index in this.model.get('nonGroupAttrList').models) {
                    var attrModel =  this.model.get('nonGroupAttrList').models[index];
                    if (attrModel.get('newLine') || !outAttrContainer){
                        // создаем новую строку
                        outAttrContainer = new $("<div/>", {"class": "col-md-24 newLine"});
                        $content_container.append(outAttrContainer);
                    }
                    // создаем контейнер для атрибута
                    var $attrContainer = $('<div/>', {"class": "attrContainer"});
                    outAttrContainer.append($attrContainer);
                    _this._renderAttr(attrModel, $attrContainer);
                    //TODO refactor
                }
            },

            /**
             * Отрисовка атрибута
             *
             * @param attrModel
             * @param $attrContainer
             * @private
             */
            _renderAttr : function(attrModel, $attrContainer) {
                AttrViewFactory.getAttrViewClass(attrModel, this.viewMode, $attrContainer)
                    .then(function (attrViewClass, model, $container) {
                        // Получаем представление для конкретного атрибуты
                        var attrView = new attrViewClass({model: model});
                        attrView.setElement($container);
                        attrView.render();
                    });

            },

            /**
             * Отрисовка групп атрибутов класса
             *
             * @param $content_container
             * @private
             */
            _renderRootGroups : function($content_container){
                var _this = this;
                var tabAttrGroupCollection = new AttrGroupCollection();
                this.model.get('rootGroupList').each(function (groupModel) {
                    // Если есть группы типа таб без родителя
                    if (tabAttrGroupCollection.length == 0 && !groupModel.getGroupType() && !groupModel.getParentGroupId()) {
                        // Пробегаем по всем группам уровня и создаем список табов
                        _this.model.get('rootGroupList').each(function (tmpGroup) {
                            if (!tmpGroup.getGroupType() && !tmpGroup.getParentGroupId()) {
                                tabAttrGroupCollection.add(tmpGroup)
                            }
                        });

                        // создание контейнера для группы
                        var $groupContainer = $('<div/>', {"class": "tabGroupContainer", style: "clear:both;"});
                        $content_container.append($groupContainer);
                        GroupViewFactory.getGroupViewClass({groupType: 'tabList'})
                            .then(function (tabGroupListViewClass) {
                                var tabGroupListView = new tabGroupListViewClass({ model: tabAttrGroupCollection, viewMode : _this.viewMode});
                                tabGroupListView.setElement($groupContainer);
                                tabGroupListView.render();
                            });
                    } else {
                        if (groupModel.getGroupType()) {
                            //TODO add groupContainer
                            GroupViewFactory.getGroupViewClass(groupModel).then(function (GroupViewClass) {
                                var groupView = new GroupViewClass({model : groupModel, viewMode : _this.viewMode});
                                groupView.render();
                            });
                        } else {
                            //do nothing already add
                        }
                    }
                });
            }
        });

        return view;
    });
