/**
 * Коллекция атрибутов
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/form_block/model/AttrModel'],
    function (log, misc, backbone, AttrModel) {
        var AttrCollection = backbone.Collection.extend({
            model: AttrModel
        });
        return AttrCollection;
    });
