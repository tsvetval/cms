/**
 * Коллекция групп аттрибутов
 */
define(
    ['log', 'misc', 'backbone'],
    function (log, misc, backbone) {
        var AttrGroupCollection = backbone.Collection.extend({
            //TODO добавить модель избегая циклической зависимости на AttrGroupModel
            // model: AttrGroupModel,

            /**
             * Получить группу из коллекции по id
             *
             * @param id        -   id группы
             * @returns {*}     -   группа
             */
            getGroupById : function(id){
                return this.get(id)
            }
        });
        return AttrGroupCollection;
    });
