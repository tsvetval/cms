/**
 * Фабрика представлений для атрибутов
 */
define(['log', 'misc', 'backbone'],
    function (log, misc, backbone) {

        var GroupModelFactory = new function () {

            /**
             * Получить представление для атрибута
             *
             * @param attrModel     -   модель атрибута
             * @param viewMode      -   режим просмотра (VIEW, EDIT, CREATE)
             * @param $container    -   jQuery-объект контейнер для представления
             * @returns {*}
             */
            this.getAttrViewClass = function (attrModel, viewMode, $container) {
                log.debug("Trying to create view for " + attrModel.get('entityFieldName') + '[' + attrModel.get('fieldType') + ']')
                var attrViewPath;
                var attrCustomViewPath;
                if(attrModel.get("canEdit")){
                    if (viewMode == 'VIEW') {
                        attrCustomViewPath = attrModel.get('templateView');
                        attrViewPath = 'cms/page_blocks/form_block/object_view_block/view/attrs';
                    } else if (viewMode == 'EDIT') {
                        attrCustomViewPath = attrModel.get('templateEdit');
                        attrViewPath = 'cms/page_blocks/form_block/object_edit_block/view/attrs';
                    } else if (viewMode == 'CREATE') {
                        attrCustomViewPath = attrModel.get('templateCreate');
                        attrViewPath = 'cms/page_blocks/form_block/object_edit_block/view/attrs';
                        //TODO fix attrViewPath
                    } else {
                        log.debug('ERROR !!!!!!!!!!!!!!!! Unexpected value of property viewMode = ' + viewMode);
                    }
                }else{
                    attrCustomViewPath = attrModel.get('templateView');
                    attrViewPath = 'cms/page_blocks/form_block/object_view_block/view/attrs';
                }

                if (!attrCustomViewPath) {
                    if (attrModel.get('view')) {
                        var templateName = attrModel.get('view');
                        attrViewPath = attrViewPath + "/custom/" + templateName;
                    } else if (attrModel.get('fieldType') == 'STRING') {
                        attrViewPath = attrViewPath + '/StringAttrView';
                    } else if (attrModel.get('fieldType') == 'BOOLEAN') {
                        attrViewPath = attrViewPath + '/BooleanAttrView';
                    } else if (attrModel.get('fieldType') == 'DATE') {
                        attrViewPath = attrViewPath + '/DateAttrView';
                    } else if (attrModel.get('fieldType') == 'MANY_TO_ONE') {
                        attrViewPath = attrViewPath + '/ManyToOneAttrView';
                    } else if (attrModel.get('fieldType') == 'MANY_TO_MANY') {
                        attrViewPath = attrViewPath + '/ManyToManyAttrView';
                    } else if (attrModel.get('fieldType') == 'HETERO_LINK') {
                        attrViewPath = attrViewPath + '/HeteroLinkAttrView';
                    } else if (attrModel.get('fieldType') == 'ONE_TO_MANY') {
                        attrViewPath = attrViewPath + '/OneToManyAttrView';
                    } else if (attrModel.get('fieldType') == 'ENUM') {
                        attrViewPath = attrViewPath + '/EnumAttrView';
                    } else if (attrModel.get('fieldType') == 'FILE') {
                        attrViewPath = attrViewPath + '/FileAttrView';
                    } else if (attrModel.get('fieldType') == 'TEXT') {
                        attrViewPath = attrViewPath + '/TextAttrView';
                    } else {
                        attrViewPath = attrViewPath + '/StringAttrView';
                    }
                } else {
                    attrViewPath = getAttrViewPathFromTemplate(attrCustomViewPath);
                    console.log(attrViewPath);
                }

                var def = new $.Deferred();
                require([attrViewPath], function (attrViewDyn) {
                    def.resolve(attrViewDyn, attrModel, $container);
                });
                return def.promise();
            };

            /**
             * Получить представление атрибута с пользовательским шаблоном
             *
             * @param template  -   путь к шаблону
             * @returns {*}
             */
            var getAttrViewPathFromTemplate = function (template) {
                //временная функция для перехода на REST, преобразует старые пути к шаблонам на новые
                var oldTemplateExt = ".hml";
                var oldTemplatePrefix = "/templates/";
                var newTemplatePrefix = "cms/page_blocks/form_block/templates/custom/";
                if (template.indexOf(oldTemplateExt, template.length - oldTemplateExt.length) !== -1) {
                    //старый шаблон — преобразуем к новому
                    return template.replace(oldTemplateExt, "")
                        .replace(oldTemplatePrefix, newTemplatePrefix);
                } else {
                    //новый шаблон — возвращаем как есть
                    return template;
                }
            };
        };

        return GroupModelFactory;
    });
