/**
 * Фабрика представлений для групп атрибутов
 */
define(['log'],
    function (log) {

        var GroupModelFactory = new function () {

            /**
             * Получить представление для группы
             *
             * @param data  -   параметры, должны содержать
             *                  groupType (string)  -   тип группы
             *
             * @returns {*}
             */
            this.getGroupViewClass = function (data) {
               var def = new $.Deferred();
               if (data.groupType == 'tabList'){
                   require(['cms/page_blocks/form_block/view/groups/TabListGroupView'], function (viewClass) {
                       def.resolve(viewClass);
                   });
               }  else {
                   require(['cms/page_blocks/form_block/view/groups/DefaultGroupView'], function (viewClass) {
                       def.resolve(viewClass);
                   });
               }
               return def.promise();
            };
        };

        return GroupModelFactory;
    });
