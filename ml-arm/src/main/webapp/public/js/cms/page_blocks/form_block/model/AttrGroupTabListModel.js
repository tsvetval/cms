/**
 * Модель группы атрибутов (представление в виде табов)
 */
define(
    ['log', 'misc', 'backbone',
        'underscore',
        'cms/page_blocks/form_block/collections/AttrCollection',
        'cms/page_blocks/form_block/model/AttrGroupCollection'
    ],
    function (log, misc, backbone, _, AttrCollection, AttrGroupCollection) {
        var AttrGroupModel = backbone.Model.extend({
            defaults:   {
                /* Заголовок группы*/
                title :  undefined,
                /* Идентификатор группы*/
                groupId :  undefined,
                /**/
                attrCollection :  undefined,
                /*список подгрупп*/
                subGroupCollection : undefined,
                groupType : undefined
            },

            /**
             * инициализация модели
             */
            initialize: function () {
                console.log("initialize PageModel");
                this.set('guid', _.uniqueId('attr'));
                this.set("attrCollection", new AttrCollection());
                this.set("subGroupCollection", new AttrGroupCollection());
            },

            /**
             * получить id группы
             * @returns {*}
             */
            getGroupId : function (){
                return this.get('id');
            },

            /**
             * получить id родительской группы
             * @returns {*}
             */
            getParentGroupId : function (){
                return this.get('parent');
            }

        });

        return AttrGroupModel;
    });
