/**
 * Модель атрибута
 */
define(
    ['log', 'misc', 'backbone', 'underscore', 'jquery','console_const'],
    function (log, misc, backbone, _, $,console_const) {
        var AttrModel = backbone.Model.extend({
            defaults: {
                /*
                 * Модель блока к которому относится атрибут
                 * */
                pageBlockModel: undefined,
                className: undefined,

                description: undefined,
                entityFieldName: undefined,
                groupId: undefined,
                fieldType: undefined,
                value: undefined,
                view: undefined,

                newLine: true,
                viewPos: undefined,
                offset: 0,
                totalLength: 24,
                titleLength: 8,
                tableHeight: 200,
                mandatory : false,
                canEdit: undefined,

                hidden: false
            },

            /**
             * инициализация модели
             */
            initialize: function () {
                log.debug("initialize AttrModel " + this.get('entityFieldName'));
                this.set('guid', _.uniqueId('attr'));
            },

            /**
             * Добавить(установить) значение ссылочного атрибута
             * @param value     -   значение
             */
            addLinkObjectId: function (value) {
                if (this.get('fieldType') == 'MANY_TO_ONE') {
                    this.set('value', {objectId: value.objectId, title: value.title})
                } else if (this.get('fieldType') == 'ONE_TO_MANY' || this.get('fieldType') == 'MANY_TO_MANY') {
                    var idList = [];
                    idList.push(value.objectId);
                    if (this.get('value') && this.get('value').objectList) {
                        this.get('value').objectList.forEach(function (object) {
                            idList.push(object.objectId);
                        });
                    }
                    this.set('value', {idList: idList, title: undefined});
                }
            },

            /**
             *  Сериализация атрибута
             * @returns {*}
             */
            serializeValue: function () {
                if (this.get('fieldType') == 'STRING') {
                    return this.get('value');
                } else if (this.get('fieldType') == 'TEXT') {
                    return this.get('value');
                } else if (this.get('fieldType') == 'LONG' || this.get('fieldType') == 'DOUBLE') {
                    if (typeof this.get('value') !== 'undefined') {
                        return this.get('value').toString();
                    } else {
                        return undefined;
                    }
                } else if (this.get('fieldType') == 'FILE') {
                    if (this.get('value') && this.get('value').deleted) {
                        return "";
                    } else if (this.get('value') && this.get('value').fileName) {
                        return this.get('value').fileName;
                    }
                    return undefined
                } else if (this.get('fieldType') == 'ENUM') {
                    if (this.get('value')) {
                        return this.get('value').code;
                    } else {
                        return "";
                    }
                } else if (this.get('fieldType') == 'DATE') {
                    var value = this.get('value');
                    if (!value) {
                        return undefined;
                    } else {
                        return String(this.get('value'));
                    }
                } else if (this.get('fieldType') == 'BOOLEAN') {
                    if (!this.get('value')){
                        return false;
                    }
                    return this.get('value');
                } else if (this.get('fieldType') == 'MANY_TO_ONE') {
                    if (this.get('value') && this.get('value').objectId) {
                        return this.get('value').objectId.toString()
                    } else {
                        return "";
                    }
                } else if (this.get('fieldType') == 'ONE_TO_MANY' || this.get('fieldType') == 'MANY_TO_MANY') {
                    var idList = [];
                    if (this.get('value') && this.get('value').objectList) {
                        this.get('value').objectList.forEach(function (obj) {
                            idList.push(obj.objectId)
                        })
                    }
                    return idList;

                } else if (this.get('fieldType') == 'HETERO_LINK') {
                    var idList = [];
                    if (this.get('value') && this.get('value').objectList) {
                        this.get('value').objectList.forEach(function (obj) {
                            idList.push(obj.objectId)
                        })
                    }
                    return idList;

                } else {
                    return JSON.stringify(this.get('value'));
                }
            },

            /**
             * Получить имя атрибута
             * @returns {*} -   имя атрибута (entityFieldName)
             */
            getAttrEntityFiledName: function () {
                return this.get('entityFieldName');
            },

            /**
             * Является ли атрибут обязательным для заполнения
             * @returns {*}
             */
            isMandatory: function () {
                return this.get('mandatory') && !this.get('hidden');
            },


            /**
             * Задано ли значение для атрибута
             * @returns {boolean}
             */
            hasValue: function () {
                if (typeof this.get('value') !== 'undefined') {
                    if (typeof this.get('value') == 'string' && this.get('value').trim() === '' ){
                    // Если строка то проверяем что она не пустая
                         return false;
                    }

                    if (this.get('fieldType') == 'FILE' && !(this.get('value').fileName || this.get('value').title) && !this.get('value').deleted) {
                        return false;
                    }
                    if (this.get('fieldType').indexOf('TO_MANY') != -1
                        && this.get('value').objectList && this.get('value').objectList.length == 0){
                        return false;
                    }
                    return true;
                }
                return false;
            },

            /**
             * Генерация события "Подсветить незаполненный обязательный атрибут"
             */
            triggerNotFilledMandatory: function () {
                this.trigger('HighlightMandatory');
            },

            /**
             * Генерация события "Убрать подсветку атрибута"
             */
            triggerRemoveHighlightMandatory: function () {
                this.trigger('RemoveHighlightMandatory');
            },

            /**
             * Получить id группы атрибута
             * @returns {*}
             */
            getAttrGroupId: function () {
                return this.get('groupId');
            },

            /**
             * Установить значение атрибута
             * @param value -   значение
             */
            setAttrValue: function (value) {
                this.set('value', value);
            },

            /**
             * Открыть объект для просмотра (для ссылочных атрибутов)
             *
             * @param objectId  -   id объекта
             * @param mlClass   -   имя класса объекта
             */
            openObject: function (objectId, mlClass) {
                log.debug("Trigger Event [openObject] for className =" + mlClass + " objectId =" + objectId);
                this.openPage(OBJECT_VIEW_PAGE, 'Просмотр объекта ...', {
                    objectId: objectId,
                    className: mlClass
                });
            },

            /**
             * Открыть объект для редактирования (для ссылочных атрибутов)
             *
             * @param objectId  -   id объекта
             * @param mlClass   -   имя класса объекта
             */
            openObjectEdit: function (objectId, mlClass) {
                log.debug("Trigger Event [openObjectEdit] for className =" + mlClass + " objectId =" + objectId);
                this.openPage(OBJECT_EDIT_PAGE, 'редактирование объекта ...', {
                    objectId: objectId,
                    className: mlClass
                });
            },

            /**
             * Открытие просмотра текущего атрибута как объекта MlAttr
             */
            openAttrView: function () {
              log.debug('Open attr for view');
              this.openPage(OBJECT_VIEW_PAGE, 'Просмотр артибута', {objectId : this.get('id'), className:"MlAttr"})
            },

            /**
             * Открыть страницу по URL
             *
             * @param url       -   url страницы
             * @param title     -   заголовок
             * @param params    -   параметры
             */
            openPage: function (url, title, params) {
                this.trigger('OPEN_PAGE', {
                    title: title,
                    url: url,
                    params: params
                });
            },

            /**
             * Вызов серверного метода
             *
             * @param pageblock     -   страничный блок
             * @param options       -   параметры
             * @param callback      -   функция обратного вызова
             * @returns {*}
             */
            callServerAction: function (pageblock, options, callback) {
                return this.get('pageBlockModel').callServerAction(pageblock, options, callback);
            },

            /**
             * Скрыть атрибут
             */
            hide: function () {
                this.set("hidden", true);
            },

            /**
             * Показать атрибут
             */
            show: function () {
                this.set("hidden", false);
            },

            /**
             * Обновить представление атрибута
             */
            refresh: function () {
                this.trigger('render');
            }

        });

        return AttrModel;
    });
