/**
 * Модель блока создания объекта
 */
define(
    ['log', 'misc', 'backbone', 'jquery',
        'cms/page_blocks/form_block/FormBlockModel',
        'cms/events/NotifyPageBlocksEventObject',
        'cms/page_blocks/form_block/model/AttrModel',
        'cms/page_blocks/form_block/model/AttrGroupModel',
        'cms/page_blocks/form_block/collections/AttrCollection',
        'cms/page_blocks/form_block/collections/AttrGroupCollection',
        'console_const'
    ],
    function (log, misc, backbone, $, FormBlockModel, NotifyEventObject, AttrModel, AttrGroupModel, AttrCollection, AttrGroupCollection,console_const) {
        var model = FormBlockModel.extend({
            defaults: {
                objectId: undefined,
                entityName: undefined,
                description: undefined,
                title: undefined,
                attrList: undefined,
                nonGroupAttrList: undefined,
                groupList: undefined,
                rootGroupList: undefined,
                pageTitle: ''
            },

            /**
             * Инициализация модели
             */
            initialize: function () {
                var _this = this;
                log.debug('initialize ObjectCreateBlockModel');
                model.__super__.initialize.call(this);
                this.pageTitle = 'Создание объекта класса <%=data.description%>';
            },

            /**
             * Инициализация обработчиков событий
             */
            initializeListeners: function () {
                model.__super__.initializeListeners.call(this);
                /*
                 * Слушаем событие выбора объектов для ссылочных атрибутов
                 * */
                this.listenTo(this, 'objectsSelectionDone', this.objectsSelectionDone);
                this.listenTo(this, 'linkObjectCreated', this.linkObjectCreated);
            },

            /*
             * После создания линкованого объекта
             * */
            linkObjectCreated: function (params) {
                var _this = this;
                var refAttrId = misc.option(params, "refAttrId", "Атрибут для обновления");
                var objectId = misc.option(params, "objectId", "ID созданного объекта", []);
                var title = misc.option(params, "title", "ID созданного объекта", []);

                // ищем модель атрибута и проставляем ему новое значение
                var attrModel = this.get('attrList').get(refAttrId);
                attrModel.addLinkObjectId({objectId: objectId, title: title});
            },

            /*
             * После выбора ссылочных объектов
             * */
            objectsSelectionDone: function (params) {
                var refAttrId = misc.option(params, "refAttrId", "Атрибут для обновления");
                var addIdList = misc.option(params, "addIdList", "Список ID для добавления", []);
                var removeIdList = misc.option(params, "removeIdList", "Список ID для удаления", []);
                // ищем модель атрибута и проставляем ему новое значение
                var attrModel = this.get('attrList').get(refAttrId);
                var currentValue = attrModel.get('value');
                var newValueIdList = [];
                //TODO refactor this вынести логику в модель атрибутов
                // Удаляем удаленные
                if (currentValue && currentValue.objectList) {
                    currentValue.objectList.forEach(function (object) {
                        if (!_.contains(removeIdList, object.objectId)) {
                            newValueIdList.push(object.objectId)
                        }
                    })
                }
                // Добавляем новые id
                newValueIdList = _.union(newValueIdList, addIdList);
                attrModel.set('value', {idList: newValueIdList, title: undefined});
            },

            /**
             * Отмена создания объекта
             */
            cancelEditObject: function () {
                this.closePage();
            },

            /**
             * Сохранение объекта
             * @returns {boolean}
             */
            saveObject: function () {
                var _this = this;
                //TODO
                // Пробегаем по всем атрибутам и собираем значения
                var notFilledMandatoryAttrs = [];
                var fileAttrList = [];
                var dataForSave = {};
                this.get('attrList').forEach(function (attr) {
                    //Для каждого атрибута проверяется обязательность
                    if (attr.isMandatory() && !attr.hasValue()) {
                        notFilledMandatoryAttrs.push(attr);
                        attr.triggerNotFilledMandatory();
                    } else {
                        attr.triggerRemoveHighlightMandatory();
                    }

                    if (attr.get('fieldType') == 'FILE' && attr.hasValue()) {
                        fileAttrList.push(attr);
                    }
                    // Устанавливаем значение атрибута
                    dataForSave[attr.getAttrEntityFiledName()] = attr.serializeValue();
                });

                if (notFilledMandatoryAttrs.length > 0) {
                    return false;
                }
                // Отправляем запрос на сохранение
                var options = {
                    action: 'saveObject',
                    data: {
                        objectId: _this.get('objectId'),
                        className: _this.get('entityName'),
                        pageBlockId: _this.get('blockInfo').get('id'),
                        refAttrId: _this.get('pageModel').get('refAttrId'),
                        data: JSON.stringify(dataForSave),
                        ml_request: true
                    }
                };

                this.uploadFiles(fileAttrList, function () {
                    log.debug('uploadFiles done.');
                    _this.callServerAction(options).then(function (result) {
                        var opener = _this.get("pageModel").get("page_opener");
                        if (_this.get("pageModel").get('refAttrId')) {
                            $.extend( result, {refAttrId : _this.get("pageModel").get('refAttrId')});
                            _this.notifyPageBlocks(new NotifyEventObject('linkObjectCreated', result, opener));
                            _this.closePage();
                        } else {
                            _this.notifyPageBlocks(new NotifyEventObject('refreshPage', {}, opener));
                            _this.replacePage(
                                OBJECT_EDIT_PAGE,
                                'Просмотр объекта ...',
                                {
                                    objectId: result.objectId,
                                    className: _this.get('className')
                                }
                            );
                        }
                    })
                });
            }

        });
        return model;
    });
