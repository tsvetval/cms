/**
 * Загрузочный модуль блока создания объекта
 *  Контроллер: ru.peak.ml.web.block.controller.impl.form.ObjectCreateBlockController
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/form_block/object_create_block/model/ObjectCreateBlockModel',
        'cms/page_blocks/form_block/object_create_block/view/ObjectCreateBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });
