<div class="container control-panel col-lg-24" id="view_buttons_container">
    <h4 class="form-title col-lg-10 callback">
        <small>&nbsp(Создание объектa класса <%=formModel.get('description')%>)</small>
    </h4>
    <div class="pull-right control-buttons">
        <div>
            <div>
                <button class="btn btn-primary save-object-button" click-action="saveClick">
                    <span class="glyphicon glyphicon-ok"></span> Сохранить
                </button>
                <button class="btn btn-primary cancel-object-button" click-action="cancelClick">
                    <span class="glyphicon glyphicon-repeat"></span> Отмена
                </button>
            </div>
        </div>
    </div>
</div>
<div id="view_content_container">

</div>
