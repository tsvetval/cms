/**
 * Модель блока редактирования объекта
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/form_block/FormBlockModel',
        'cms/events/NotifyPageBlocksEventObject',
        'cms/page_blocks/form_block/model/AttrModel',
        'cms/page_blocks/form_block/model/AttrGroupModel',
        'cms/page_blocks/form_block/collections/AttrCollection',
        'cms/page_blocks/form_block/collections/AttrGroupCollection'
    ],
    function (log, misc, backbone, FormBlockModel, NotifyEventObject, AttrModel, AttrGroupModel, AttrCollection, AttrGroupCollection) {
        var model = FormBlockModel.extend({
            defaults: {
                objectId: undefined,
                entityName: undefined,
                description: undefined,
                title: undefined,
                attrList: undefined,
                nonGroupAttrList: undefined,
                groupList: undefined,
                rootGroupList: undefined,
                pageTitle: '',
                guid: undefined
            },


            /**
             * Инициализация модели
             */
            initialize: function () {
                var _this = this;
                log.debug('initialize ObjectEditBlockModel');
                model.__super__.initialize.call(this);
                this.pageTitle = 'Редактирование объекта <%=data.title%>';
                this.set('guid', _.uniqueId('attr'));
            },

            /**
             * Инициализация обработчиков событий
             */
            initializeListeners: function () {
                model.__super__.initializeListeners.call(this);
                /*
                 * Слушаем событие выбора объектов для ссылочных атрибутов
                 * */
                this.listenTo(this, 'objectsSelectionDone', this.objectsSelectionDone);
                this.listenTo(this, 'linkObjectCreated', this.linkObjectCreated);
            },
            /*
             * После создания линкованого объекта
             * */
            linkObjectCreated: function (params) {
                var _this = this;
                var refAttrId = misc.option(params, "refAttrId", "Атрибут для обновления");
                var objectId = misc.option(params, "objectId", "ID созданного объекта", []);
                var title = misc.option(params, "title", "ID созданного объекта", []);

                // ищем модель атрибута и проставляем ему новое значение
                var attrModel = this.get('attrList').get(refAttrId);
                attrModel.addLinkObjectId({objectId: objectId, title: title});
            },

            /**
             * После выбора ссылочных объектов
             * @param params
             */
            objectsSelectionDone: function (params) {
                var refAttrId = misc.option(params, "refAttrId", "Атрибут для обновления");
                var addIdList = misc.option(params, "addIdList", "Список ID для добавления", []);
                var removeIdList = misc.option(params, "removeIdList", "Список ID для удаления", []);
                // ищем модель атрибута и проставляем ему новое значение
                var attrModel = this.get('attrList').get(refAttrId);
                var currentValue = attrModel.get('value');
                var newValueIdList = [];
                //TODO refactor this вынести логику в модель атрибутов
                // Удаляем удаленные
                if (currentValue && currentValue.objectList) {
                    currentValue.objectList.forEach(function (object) {
                        if (!_.contains(removeIdList, object.objectId)) {
                            newValueIdList.push(object.objectId)
                        }
                    })
                }
                // Добавляем новые
                newValueIdList = _.union(newValueIdList, addIdList);
                attrModel.set('value', {idList: newValueIdList, title: undefined});
            },

            /**
             * Отмена редактирования объекта
             */
            cancelEditObject: function () {
                log.debug("START OPEN OBJECT")
                this.replacePage(
                    OBJECT_VIEW_PAGE,
                    'Просмотр объекта ...',
                    {
                        objectId: this.get('objectId'),
                        className: this.get('className'),
                        refAttrId: this.get('refAttrId')
                    }
                );
            },

            /**
             * Удаление объекта
             * @returns {*}
             */
            deleteObject: function () {
                var _this = this;
                var objectId = this.get('objectId');
                var className = this.get('entityName');
                var options = {
                    action: 'deleteObject',
                    data: {
                        objectId: objectId,
                        className: className,
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    }
                };

                log.debug('Call server to delete Object with id = ' + objectId + '@' + className);
                return this.callServerAction(options).then(function () {
                    log.debug('Receive response from server after delete Object with id = ' + objectId + '@' + className);
                    log.debug('Trigger event to navigate previous BreadCrumb');
                    _this.notifyOpenerObjectChange();
                    _this.closePage();
                });
            },


            /**
             * Сохранение объекта
             * @returns {boolean}
             */
            saveObject: function () {
                var _this = this;
                _this._saveObject(function () {
                    log.debug("END EDIT SAVE");
                    _this.notifyOpenerObjectChange();
                })
            },

            notifyOpenerObjectChange: function(){
                var _this = this;
                var objectId = this.get('objectId');
                var className = this.get('entityName');
                _this.notifyPageBlocks(new NotifyEventObject(
                    'changeObject',
                    {"className":className,"objectId":objectId},
                    'all'
                ));
            },

            /**
             * Сохранение объекта и последующее закрытие редактирования
             * @returns {boolean}
             */
            saveAndCloseObject: function () {
                var _this = this;
                _this._saveObject(function () {
                    log.debug("END EDIT SAVE");
                    _this.notifyOpenerObjectChange()
                    _this.cancelEditObject()
                })
            },


            _saveObject: function (callback) {
                var _this = this;
                // Пробегаем по всем атрибутам и собираем значения
                var notFilledMandatoryAttrs = [];
                var fileAttrList = [];
                var dataForSave = {};
                this.get('attrList').forEach(function (attr) {
                    //Для каждого атрибута проверяется обязательность
                    if (attr.isMandatory() && !attr.hasValue()) {
                        notFilledMandatoryAttrs.push(attr);
                        attr.triggerNotFilledMandatory();
                    } else {
                        attr.triggerRemoveHighlightMandatory();
                    }

                    if (attr.get('fieldType') == 'FILE' && attr.hasValue()) {
                        fileAttrList.push(attr);
                    }
                    // Устанавливаем значение атрибута
                    dataForSave[attr.getAttrEntityFiledName()] = attr.serializeValue();
                });

                if (notFilledMandatoryAttrs.length > 0) {
                    log.debug("Not all mandatory attrs filled");
                    return false;
                    /*
                     // Не сохраняем оповещаем модели
                     notFilledMandatoryAttrs.forEach(function (attr) {
                     attr.triggerNotFilledMandatory();
                     });
                     */
                }
                // Отправляем запрос на сохранение
                var options = {
                    action: 'saveObject',
                    data: {
                        objectId: _this.get('objectId'),
                        className: _this.get('entityName'),
                        pageBlockId: this.get('blockInfo').get('id'),
                        data: JSON.stringify(dataForSave),
                        ml_request: true
                    }
                };
                this.uploadFiles(fileAttrList, function () {
                    log.debug('uploadFiles done.');
                    _this.callServerAction(options).then(function () {
                        if (callback) {
                            callback();
                        }
                    })
                });
            }
        });
        return model;
    });
