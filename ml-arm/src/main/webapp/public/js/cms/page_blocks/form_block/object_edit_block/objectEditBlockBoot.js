/**
 * Загрузочный модуль блока редактирования объекта
 * Контроллер: ru.peak.ml.web.block.controller.impl.form.ObjectEditBlockController
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/form_block/object_edit_block/model/ObjectEditBlockModel',
        'cms/page_blocks/form_block/object_edit_block/view/ObjectEditBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });
