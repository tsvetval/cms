<div class="container control-panel col-lg-24" id="view_buttons_container">
    <h4 class="form-title col-lg-10 callback">
        <%=formModel.get('title')%><small>&nbsp(Редактирование объектa класса <%=formModel.get('description')%>)</small>
    </h4>
    <div class="pull-right control-buttons">
        <div>
            <div>
                <button class="btn btn-primary save-object-button" click-action="saveClick">
                    <span class="glyphicon glyphicon-ok"></span> Сохранить
                </button>
                <button class="btn btn-primary save-close-object-button" click-action="saveAndCloseClick">
                    <span class="glyphicon glyphicon-ok"></span> Сохранить и закрыть
                </button>
                <button class="btn btn-primary cancel-object-button" click-action="cancelClick">
                    <span class="glyphicon glyphicon-repeat"></span> Отмена
                </button>
                <% if (canDelete) { %>
                <button class="btn btn-primary remove-object-button" click-action="deleteClick">
                    <span class="glyphicon glyphicon-remove"></span> Удалить
                </button>
                <%}%>
            </div>
        </div>
    </div>
</div>
<div id="view_content_container">

</div>
