<div class="attr-label-container col-md-offset-<%=attrModel.get('offset')%> col-md-<%=attrModel.get('titleLength')%>">
    <b class="attr-label"><%=attrModel.get('description')%>:</b>
</div>
<div class="col-md-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%> input-group">
    <input value="" readonly style="width: 100%; padding-left: 5px;" class="attrFieldTitle"/>
<!--
    <input type="hidden" class="attrField linked-item" value=""/>
-->
    <span class="btn btn-primary input-group-addon editManyToOne">
              <span class="glyphicon glyphicon-pencil"></span>
    </span>
    <span class="btn btn-primary input-group-addon deleteLinkedObject">
        <span class="glyphicon glyphicon-trash"></span>
    </span>
    <span class="btn btn-primary input-group-addon createClick active-button">
        <span class="glyphicon glyphicon-plus-sign"></span>
    </span>
    <span class="btn btn-primary input-group-addon selectLinkedObject">
        <span class="glyphicon glyphicon-search highlight-button"></span>
    </span>
</div>
