<div class="attr-label-container col-md-offset-<%=attrModel.get('offset')%> col-md-<%=attrModel.get('titleLength')%>">
    <b class="attr-label" style="height: 24px; line-height: 24px;"><%=attrModel.get('description')%>:</b>
</div>
<div>
    <div id="custom-toolbar" class="btn-group custom-toolbar">
           <span class="btn btn-primary moveUp inactive-button">
              <span class="glyphicon glyphicon-chevron-up"></span>
           </span>
            <span class="btn btn-primary moveDown inactive-button">
                <span class="glyphicon glyphicon-chevron-down"></span>
            </span>
           <span class="btn btn-primary editManyToOne inactive-button">
              <span class="glyphicon glyphicon-pencil"></span>
           </span>
            <span class="btn btn-primary deleteLinkedObject inactive-button">
                <span class="glyphicon glyphicon-trash"></span>
            </span>
            <span class="btn btn-primary createClick active-button">
                <span class="glyphicon glyphicon-plus-sign"></span>
            </span>
            <span class="btn btn-primary selectLinkedObject active-button">
                <span class="glyphicon glyphicon-search highlight-button"></span>
            </span>
    </div>

    <table class="table-javascript">
    </table>
</div>