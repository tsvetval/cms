<div id="photo">
    <div class="container-fluid" style="">
        <div class="col-lg-10" style="display: block !important  border: 1px black solid;">
            <label class="checkbox inline" for="showMask" id="maskControl">
                <input type="checkbox" id="showMask">Показать маску</label>
        </div>
        <div class="col-lg-14 row">


                            <span class="btn btn-primary col-lg-2  pull-right" id="addFile"
                                  style="overflow: hidden;position: relative;">
                                <label class="glyphicon glyphicon-file"></label>
                                <input type="file" id="file" name="file"/>
                            </span>
                            <span class="btn btn-primary col-lg-2  pull-right" id="onCamera"
                                  style="overflow: hidden;position: relative;">
                                <label class="glyphicon glyphicon-camera"></label>
                            </span>

        </div>
    </div>
    <div class="container-fluid">
        <div class="col-lg-24">
            <div class="col-lg-12">
                <div style="height: <%= height*sizeFactor%>px; width: <%= width*sizeFactor%>px; display: block !important; border: 1px black solid;">
                    <div id="imgbox"
                         style="overflow : hidden  !important; height: <%= height*sizeFactor%>px; width: <%= width*sizeFactor%>px;"
                         class="bordered">
                        <img id="photo_cut"
                             style="max-width: none !important;"/>
                    </div>
                    <div class="photomask" id="photomask"
                                     style="display: block !important; visibility:hidden; overflow : hidden  !important; ">
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="pull-right " style="border: 1px black solid;">
                    <img id="photo_full"
                         style="display: block ;  overflow : hidden  !important; height:  <%= height*sizeFactor%>px ; width:  <%= width*sizeFactor%>px ;"/>
                </div>
            </div>
        </div>
    </div>
</div>

<div id='camWindow'>
    <div id="webCamDialog"></div>
</div>