<div class="attr-label-container col-md-offset-<%=attrModel.get('offset')%> col-md-<%=attrModel.get('titleLength')%>">
    <b class="attr-label"><%=attrModel.get('description')%>:</b>
</div>
<div class="col-md-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%> input-group">
    <input type="text" class="form-control attrField_fake" readonly="true"/>

    <span class="btn btn-primary input-group-addon buttonDelete" style="overflow: hidden;position: relative;">
        <label class="glyphicon glyphicon-trash"></label>
    </span>
    <span class="btn btn-primary input-group-addon buttonPhoto" style="overflow: hidden;position: relative;">
        <label class="glyphicon glyphicon-camera"></label>
    </span>

    <span class="btn btn-primary input-group-addon" style="overflow: hidden;position: relative;">
        <label class="glyphicon glyphicon-file highlight-button"></label>
        <input  type="file"
                class="attrField"/>
    </span>
</div>
