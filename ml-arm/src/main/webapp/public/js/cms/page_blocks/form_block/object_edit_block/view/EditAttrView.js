/**
 * Базовое представление для атрибута в режиме редактирования
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/form_block/view/AttrView',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/StringAttrView.tpl'],
    function (log, misc, backbone, AttrView, DefaultTemplate) {
        var view = AttrView.extend({

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                _.extend(this.events, AttrView.prototype.events);
                this.listenTo(this.model, 'focus', this.setFocus);
            },

            /**
             * Добавление обработчиков к обязательным для заполнения полям
             */
            addMandatoryEvents : function(){
                if (!this.$attrLabel) {
                    this.$attrLabel = this.$el.find('.attr-label');
                }

                if (this.$attrLabel && this.model.get('mandatory')){
                    this.$attrLabel.addClass('mandatory-label');
                    this.listenTo(this.model, 'HighlightMandatory', this.highlightMandatory);
                    this.listenTo(this.model, 'RemoveHighlightMandatory', this.removeHighlightMandatory);
                } else {
                    if (!this.$attrLabel) {
                        log.warn('Can not add mandatory for attr');
                    }
                }
            },

            /**
             * Отображение readonly-атрибутов
             */
            addReadOnly: function () {
                if (!this.$inputField) {
                    this.$inputField = this.$el.find('.attrField');
                }

                if (this.$inputField && this.model.get('readOnly')) {
                    this.$inputField.attr('disabled', 'disabled');
                } else {
                    if (!this.$inputField) {
                        log.warn('Can not add readOnly for attr');
                    }
                }
            },

            /**
             * Подсветить обязательный атрибут
             */
            highlightMandatory : function(){
                var groupId = this.model.get("groupId");
                if (groupId) {
                    var group = this.model.get("pageBlockModel").getGroupById(groupId);
                    if (group) {
                        group.highlightMandatory();
                    }
                }
                this.$attrLabel.addClass('highlight-mandatory-label');
            },

            /**
             * Удалить подсветку обязательного атрибута
             */
            removeHighlightMandatory : function(){
                this.$attrLabel.removeClass('highlight-mandatory-label');
                var groupId = this.model.get("groupId");
                if (groupId) {
                    var group = this.model.get("pageBlockModel").getGroupById(groupId);
                    if (group) {
                        group.removeHighlightMandatory();
                    }
                }
            },

            /**
             * Установить фокус формы на атрибут
             */
            setFocus: function () {
                if (!this.$inputField) {
                    this.$inputField = this.$el.find('.attrField');
                }

                if (this.$inputField) {
                    this.$inputField.focus();
                }
            }

        });

        return view;
    });
