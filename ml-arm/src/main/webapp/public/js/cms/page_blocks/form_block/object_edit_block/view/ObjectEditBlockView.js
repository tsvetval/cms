/**
 * Представление блока создания объекта
 */
define(
    ['log', 'misc', 'backbone', 'underscore',
        'cms/page_blocks/form_block/FormBlockView',
        'markup',
        'cms/page_blocks/DialogPageBlock',
        'cms/page_blocks/form_block/collections/AttrGroupCollection',
        'cms/page_blocks/form_block/factories/GroupViewFactory',
        'cms/page_blocks/form_block/factories/AttrViewFactory',
        'text!cms/page_blocks/form_block/object_edit_block/templates/ObjectEditTemplate.tpl'],
    function (log, misc, backbone, _, FormBlockView, markup, Message, AttrGroupCollection,
              GroupViewFactory, AttrViewFactory, ObjectEditTemplate) {
        var view = FormBlockView.extend({

            events: {
                "click .save-object-button": "saveObjectClick",
                "click .save-close-object-button": "saveAndCloseObjectClick",
                "click .cancel-object-button": "cancelEditObjectClick",
                "click .remove-object-button": "deleteObjectClick"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                log.debug("initialize ObjectViewBlockView");
                _.extend(this.events, FormBlockView.prototype.events);
                this.viewMode = 'EDIT';
                this.listenTo(this.model, 'render', this.render);
                this.listenTo(this.model, 'renderAttribute', this.renderAttribute);
            },

            /**
             * Обработка клика по кнопке "Удалить"
             * @returns {boolean}
             */
            deleteObjectClick: function () {
                var _this = this;
                log.debug('Click delete event catched');
                var dialogConfirm = new Message({
                    title: "Подтверждение удаления",
                    message: "Вы действительно хотите удалить объект?",
                    type: 'dialogMessage'
                });
                dialogConfirm.show({
                    okAction: function () {
                        _this.model.deleteObject();
                    }
                });
            },

            /**
             * Обработка клика по кнопке "Сохранить"
             * @returns {boolean}
             */
            saveObjectClick: function () {
                this.model.saveObject();
            },

            /**
             * Обработка клика по кнопке "Сохранить и закрыть"
             * @returns {boolean}
             */
            saveAndCloseObjectClick: function () {
                this.model.saveAndCloseObject();
            },

            /**
             * Обработка клика по кнопке "Отмена"
             * @returns {boolean}
             */
            cancelEditObjectClick: function () {
                this.model.cancelEditObject();
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                var _this = this;
                this.$el.html(_.template(ObjectEditTemplate, {formModel: this.model,canDelete:_this.model.get('canDelete')}));
                // create container
                var $content_container = _this.$el.find('#view_content_container');
                _this._renderNonGroupAttrs($content_container);
                // Выводим группы
                _this._renderRootGroups($content_container);
                return true;
            }
        });

        return view;
    });
