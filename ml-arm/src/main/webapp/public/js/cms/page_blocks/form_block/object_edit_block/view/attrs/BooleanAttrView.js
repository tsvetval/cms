/**
 * Представление для атрибута типа BOOLEAN
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/form_block/object_edit_block/view/EditAttrView',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/BooleanAttrView.tpl'],
    function (log, misc, backbone,
              EditAttrView, DefaultTemplate) {
        var view = EditAttrView.extend({
            $inputField: undefined,

            events: {
                "change .attrField": "changeClick"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, EditAttrView.prototype.events);
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                if (this.isHidden()) {
                    return;
                }
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.addMandatoryEvents();
                this.addReadOnly();

                this.$inputField = this.$el.find('.attrField');
                if (this.model.get('value')) {
                    this.$inputField.attr('checked', true);
                } else {
                    this.$inputField.attr('checked', false);
                }
            },

            /**
             * Обработчик изменения значения HTML-элемента
             */
            changeClick: function () {
                this.model.attributes['value'] = this.$inputField.is(':checked');
            }

        });

        return view;
    });
