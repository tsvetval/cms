/**
 * Представление для атрибута типа DATE
 */
define(
    ['log', 'misc', 'backbone', 'datetimepicker', 'moment',
        'cms/page_blocks/form_block/object_edit_block/view/EditAttrView',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/DateAttrView.tpl'],
    function (log, misc, backbone, datetimepicker,moment,
              EditAttrView, DefaultTemplate) {
        var view = EditAttrView.extend({

            $inputField: undefined,
            events: {
                "keyup .attrField": "keyPressed",
                "blur .attrField": "refreshDateValue"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                this.viewDatePattern = 'DD.MM.YYYY';
                if (this.model.get('fieldFormat') && !(this.model.get('fieldFormat') == '')) {
                    this.viewDatePattern = moment().toMomentFormatString(this.model.get('fieldFormat'));
                }
                _.extend(this.events, EditAttrView.prototype.events);
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                if (this.isHidden()) {
                    return;
                }
                var _this = this;
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.addMandatoryEvents();
                this.addReadOnly();

                this.$inputField = this.$el.find('.attrField');
                this.printDateFromValue();

                this.$inputField.datetimepicker({
                    format: this.viewDatePattern,
                    useCurrent: false,
                    locale: 'ru',
                    language: 'ru'
                });

                this.$inputField.on("dp.change",function (e) {
                    _this.keyPressed();
                });
                this.$inputField.on("dp.hide",function (e) {
                      if(_this.$inputField.val() != ''){
                          _this.keyPressed();
                      }
                });

            },

            /**
             * Форматированный вывод даты
             */
            printDateFromValue : function(){
                var title = '';
                if (this.model.get('value')) {
                    //var dateMoment = moment(this.model.get('value'));
                    title = this.model.get('value');//dateMoment/*.utc()*/.format(this.viewDatePattern);
                }

                this.$inputField.val(title);
            },

            /**
             * Обновление значения даты
             */
            refreshDateValue: function(){
                if (!moment(this.$inputField.val(), this.viewDatePattern, true).isValid()) {
                    this.printDateFromValue();
                }
            },

            /**
             * Обработчик изменения значения HTML-элемента
             */
            keyPressed: function () {
                if (this.$inputField.val().length == 0) {
                    this.model.attributes['value'] = "";
                } else if (moment(this.$inputField.val(), this.viewDatePattern, true).isValid()) {
                    //var date = misc.parseDateByTemplate(this.$inputField.val(), this.viewDatePattern);
                    this.model.attributes['value'] =this.$inputField.val();// String(date.valueOf());
                }
            }

        });

        return view;
    });
