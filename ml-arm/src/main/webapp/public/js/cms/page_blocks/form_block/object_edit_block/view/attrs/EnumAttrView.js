/**
 * Представление для атрибута типа ENUM
 */
define(
    ['log', 'misc', 'backbone', 'select2',
        'cms/page_blocks/form_block/object_edit_block/view/EditAttrView',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/EnumAttrView.tpl'],
    function (log, misc, backbone, select2,
              EditAttrView, DefaultTemplate) {
        var view = EditAttrView.extend({
            $inputField: undefined,

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, EditAttrView.prototype.events);
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                if (this.isHidden()) {
                    return;
                }
                var _this = this;
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.addMandatoryEvents();
                this.addReadOnly();

                this.$inputField = this.$el.find('.attrField');
                var select2Data = [];
                this.model.get('enumList').forEach(function (enumObj) {
                    select2Data.push({id: enumObj.code, text: enumObj.title});
                });
                _this.$inputField.select2({
                    width: '100%',
                    data: select2Data,
                    allowClear: true,
                    placeholder: "Выберите значение"
                });

                if (this.model.get('value') && this.model.get('value').code) {
                    this.$inputField.val(this.model.get('value').code).trigger("change");
                } else {
                    this.$inputField.val(null).trigger("change");
                }

                this.$inputField.on("change", function () {
                    _this.changeSelection(_this.$inputField.val());
                });

            },

            /**
             * Обработчик изменения значения HTML-элемента
             */
            changeSelection: function (code) {
                this.model.attributes['value'] = {code: code}
            }
        });


        return view;
    });
