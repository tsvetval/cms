/**
 * Представление для атрибута типа FILE
 */
define(
    ['log', 'misc', 'backbone', 'moment', 'jquery',
        'cms/page_blocks/form_block/object_edit_block/view/EditAttrView',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/FileAttrView.tpl'],
    function (log, misc, backbone, moment, $,
              EditAttrView, DefaultTemplate) {
        var view = EditAttrView.extend({
            $inputField: undefined,
            $inputFieldFake: undefined,
            $buttonDelete: undefined,

            events: {
                "click .file-download-link": "downloadFile",
                "change .attrField": "selectFile"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, EditAttrView.prototype.events);
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                var _this = this;
                if (this.isHidden()) {
                    return;
                }

                this.$el.html(_.template(this.viewTemplate, {
                    attrModel: this.model
                }));

                var title = '';
                this.$buttonDelete = this.$el.find('.buttonDelete');
                if (this.model.get('value')) {
                    title = this.model.get('value').title;
                    this.$buttonDelete.find('label').addClass('highlight-button');
                }

                this.$buttonDelete.on('click', function () {
                    _this.deleteFile();
                });

                this.addMandatoryEvents();
                this.addReadOnly();
                this.$inputField = this.$el.find('.attrField');
                this.$inputFieldFake = this.$el.find('.attrField_fake');
                this.$inputFieldFake.val(title);
            },

            /**
             * Обработчик изменения значения HTML-элемента
             */
            selectFile: function () {
                this.$inputFieldFake.val(this.$inputField.val());
                var fileList = this.$inputField.prop('files');
                this.model.attributes['value'] = {fileName : fileList[0].name, file : fileList[0]}
            },

            /**
             * Очистка значения атрибута
             */
            deleteFile: function () {
                this.model.attributes['value'] = {fileName: undefined, file: undefined, deleted: true};
                this.render();
            },

            /**
             * Скачать файл
             */
            downloadFile: function () {
                var _this = this;
                var objectId = $(event.srcElement).attr('objectId');
                this.model.callServerAction(
                    {
                        action: 'downloadFile',
                        data: {
                            objectId: objectId,
                            className: _this.model.get('className'),
                            attrName: _this.model.get('entityFieldName')
                        }
                    },
                    function (result) {
                        window.location = result.url;
                    }
                );
            }

        });

        return view;
    });
