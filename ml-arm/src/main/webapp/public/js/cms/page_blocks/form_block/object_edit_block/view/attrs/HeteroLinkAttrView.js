/**
 * Представление для атрибута типа MANY_TO_MANY
 */
define(
    ['log', 'misc', 'backbone', 'underscore',
        'cms/page_blocks/form_block/object_edit_block/view/attrs/OneToManyAttrView',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/HeteroLinkAttrView.tpl',
        'console_const'],
    function (log, misc, backbone, _,
              OneToManyAttrView, DefaultTemplate,console_const) {
        var view =OneToManyAttrView.extend({
            events: {
                "click .deleteLinkedObject": "deleteLinkedObject",
                "click .editManyToOne": "editManyToOne",
                "click .createClick": "createClick",
                "click .selectLinkedObject": "selectLinkedObject",
                "click .file-download-link": "downloadFile",
                "click .moveUp": "moveUpClick",
                "click .moveDown": "moveDownClick"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, OneToManyAttrView.prototype.events);
            },

            render: function () {
                if (this.isHidden()) {
                    return;
                }
                var _this = this;
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.$table = this.$el.find('.table-javascript');
                this.$toolbar = this.$el.find('.custom-toolbar');
                if (!this.model.get("ordered")) {
                    this.$toolbar.find(".moveUp, .moveDown").hide();
                }

                this.$attrLabelContainer = this.$el.find('.attr-label-container');

                this.addMandatoryEvents();
                this.addReadOnly();

                var columns = [];
                columns.push({
                    field: 'state',
                    checkbox: true
                });
                columns.push({
                    field: 'objectId',
                    visible: false
                });

                var column = {
                    field: 'title',
                    title: 'Название',
                    align: 'left',
                    valign: 'top',
                    sortable: true,
                    formatter: undefined
                };
                columns.push(column);
                //Формируем значения колонок таблицы
                var data = this.createTableDataFromValue();

                this.$table.bootstrapTable(/*'append', */{
                    toolbar: _this.$toolbar,
                    label : _this.$attrLabelContainer,
                    idField: 'objectId',
                    data: data,
                    cache: false,

                    /* height: 400,*/
                    striped: true,
                    pagination: true,
                    pageSize: 5,
                    pageList: [5, 10, 20, 50],
                    showColumns: true,
                    minimumCountColumns: 1,
                    search: true,
                    clickToSelect: true,
                    columns: columns,
                    toolbarAlign :  'right'
                }).on('all.bs.table', function (name, args) {
                        _this.updateButtonState();
                    });

            },
            /**
             * Заполнение таблицы данных о связанных объектах
             */
            createTableDataFromValue: function () {
                var data = [];
                if (this.model.get('value') && this.model.get('value').objectList) {
                    this.model.get('value').objectList.forEach(function (objectData) {
                        data.push($.extend(objectData.title,
                            {objectId: objectData.objectId, title:objectData.title}));
                    });
                }
                return data;
            }, /**
             * Выбрать связанные объекты
             */
            selectLinkedObject: function () {
                var selects = this.$table.bootstrapTable('getData');
                var ids = $.map(selects, function (row) {
                    return row.objectId;
                });
                var params = {
                    objectId: this.model.get('pageBlockModel').get('objectId'),
                    refAttrId: this.model.get('id'),
                    className: undefined,
                    selectMode: "multiselect",
                    selectedList: ids
                };
                this.model.openPage(
                    HETERO_OBJECTS_SELECT_PAGE,
                    'Выбор списка объектов ...',
                    params
                );
            } ,
            /**
             * Отображение значение связанного объекта
             */
            renderAttrValue: function () {
                var _this = this;
                if (this.model.get('value') && !this.model.get('value').objectList) {
                    //this.$titleHolderElement.val('Идет получение заголовка...');
                    this.model.callServerAction({
                        action: "getRefAttrValues",
                        data: {
                            objectId: _this.model.get('pageBlockModel').get('objectId'),
                            className: _this.model.get('className'),
                            attrId: this.model.get('id'),
                            idList: JSON.stringify(_this.model.get('value').idList)
                        }
                    }).then(function (result) {
                            _this.model.set('value', result.result);
                        });
                } else {
                    var data = this.createTableDataFromValue();
                    this.$table.bootstrapTable('load', data);
                }
            }

        });

        return view;
    });
