/**
 * Представление для атрибута типа MANY_TO_MANY
 */
define(
    ['log', 'misc', 'backbone', 'underscore',
        'cms/page_blocks/form_block/object_edit_block/view/attrs/OneToManyAttrView',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/OneToManyAttrView.tpl'],
    function (log, misc, backbone, _,
              OneToManyAttrView, DefaultTemplate) {
        var view = OneToManyAttrView.extend({
            events: {
                "click .deleteLinkedObject": "deleteLinkedObject",
                "click .editManyToOne": "editManyToOne",
                "click .createClick": "createClick",
                "click .selectLinkedObject": "selectLinkedObject",
                "click .file-download-link": "downloadFile",
                "click .moveUp": "moveUpClick",
                "click .moveDown": "moveDownClick"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, OneToManyAttrView.prototype.events);
            }

        });

        return view;
    });
