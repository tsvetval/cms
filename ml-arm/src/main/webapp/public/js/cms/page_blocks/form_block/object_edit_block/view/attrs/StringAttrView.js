/**
 * Представление для атрибута типа STRING
 */
define(
    ['log', 'misc', 'backbone', 'inputmask',
        'cms/page_blocks/form_block/object_edit_block/view/EditAttrView',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/StringAttrView.tpl'],
    function (log, misc, backbone, inputmask,
              EditAttrView, DefaultTemplate) {
        var view = EditAttrView.extend({
            $inputField : undefined,

            events: {
                "keyup .attrField": "keyPressed"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, EditAttrView.prototype.events);
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                if (this.isHidden()) {
                    return;
                }
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.$inputField = this.$el.find('.attrField');
                this.$inputField.val(this.model.get('value'));
                if (this.model.has('inputmask')) {
                    var maskData = {
                        mask: JSON.parse(this.model.get('inputmask'))
                    };
                    this.$inputField.inputmask(maskData);
                }
                this.addMandatoryEvents();
                this.addReadOnly();
            },

            /**
             * Обработчик изменения значения HTML-элемента
             */
            keyPressed: function (val) {
                if (this.$inputField.val().length == 0) {
                    this.model.set('value', "");
                } else {
                    this.model.set('value', this.$inputField.val());
                }
            }

        });

        return view;
    });
