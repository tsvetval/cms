/**
 * Представление для атрибута типа TEXT
 */
define(
    ['log', 'misc', 'backbone', 'inputmask', 'ckeditor-jquery',
        'cms/page_blocks/form_block/object_edit_block/view/EditAttrView',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/TextAttrView.tpl'],
    function (log, misc, backbone, inputmask, ckeditor,
              EditAttrView, DefaultTemplate) {
        var view = EditAttrView.extend({
            $inputField: undefined,
            $editor: undefined,

            //events: {
            //    "keyup .attrField": "keyPressed"
            //},

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, EditAttrView.prototype.events);
                this.listenTo(this.model.get('pageBlockModel').get('pageModel').get('siteModel').get('pageCollection'), 'change', this.remove);
            },

            remove: function () {
                for (var instance in CKEDITOR.instances) {
                    var editor = CKEDITOR.instances[instance];
                    if (editor.owner == this.model.guid) {
                        editor.destroy();
                    }
                }
                Backbone.View.prototype.remove.apply(this, arguments);
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                var _this = this;
                if (this.isHidden()) {
                    return;
                }
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.$inputField = this.$el.find('textarea');
                this.$inputField.ckeditor();
                this.$editor = this.$inputField.ckeditorGet();
                this.$editor.owner = this.model.guid;

                this.$editor.on('change', function () {
                    _this.keyPressed();
                });
                this.$editor.setData(this.model.get('value'));
                this.addMandatoryEvents();
                this.addReadOnly();
            },

            /**
             * Обработчик изменения значения HTML-элемента
             */
            keyPressed: function (val) {
                if (this.$editor.getData().length == 0) {
                    this.model.unset('value');
                } else {
                    this.model.set('value', this.$editor.getData());
                }
            }

        });

        return view;
    });
