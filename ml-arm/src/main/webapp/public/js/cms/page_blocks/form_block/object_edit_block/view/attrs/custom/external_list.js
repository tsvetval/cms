/**
 * Представление для отображение атрибута типа DATE
 */
define(
    ['log', 'misc', 'backbone', 'moment',
        'cms/page_blocks/form_block/view/AttrView',
        'text!cms/page_blocks/form_block/object_view_block/templates/attrs/ExternalList.tpl',
        'console_const'],
    function (log, misc, backbone, moment, AttrView, DefaultTemplate,console_const) {
        var view = AttrView.extend({
            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, AttrView.prototype.events);
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                var _this = this;
                if (this.isHidden()) {
                    return;
                }
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.$el.find(".openLink").on('click', function () {
                    _this.selectLinkedObject(_this.model);
                });
            },

            selectLinkedObject: function (model) {
                var params = {
                    objectId: model.get('pageBlockModel').get('objectId'),
                    refAttrId: model.get('id'),
                    className: model.get('linkClass'),
                    description: model.get('pageBlockModel').get('title') + " → " + model.get('description'),
                    selectMode: "multiselect"
                };
                this.model.openPage(
                    OBJECT_LINKED_LIST_PAGE,
                    'Просмотр списка объектов ...',
                    params
                );
            }
        });
        return view;
    });
