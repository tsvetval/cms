/**
 * Представление для атрибута типа FILE
 */
define(
    ['log', 'misc', 'backbone', 'moment', 'jquery','jcrop','jqueryUi',
        'cms/page_blocks/form_block/object_edit_block/view/EditAttrView',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/photo_file.tpl',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/photo_crop_window.tpl'
        ],
    function (log, misc, backbone, moment, $, jcrop,jqueryUi,
              EditAttrView, DefaultTemplate,photoCrop) {
        var view = EditAttrView.extend({
            $inputField: undefined,
            $inputFieldFake: undefined,
            $buttonDelete: undefined,
            sizeFactor: undefined,
            width: undefined,
            height: undefined,
            fileName : undefined,
            jcrop_api : undefined,

            events: {
                "click .file-download-link": "downloadFile",
                "change .attrField": "selectFile",
                "click .buttonPhoto": "doPhoto"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, EditAttrView.prototype.events);

            },

            /**
             * Отрисовка представления
             */
            render: function () {
                var _this = this;
                if (this.isHidden()) {
                    return;
                }

                this.$el.html(_.template(this.viewTemplate, {
                    attrModel: this.model
                }));

                var title = '';
                this.$buttonDelete = this.$el.find('.buttonDelete');
                if (this.model.get('value')) {
                    title = this.model.get('value').title;
                    this.$buttonDelete.find('label').addClass('highlight-button');
                }

                this.$buttonDelete.on('click', function () {
                    _this.deleteFile();
                });

                this.addMandatoryEvents();
                this.addReadOnly();
                this.$inputField = this.$el.find('.attrField');
                this.$inputFieldFake = this.$el.find('.attrField_fake');
                this.$inputFieldFake.val(title);
                this.sizeFactor = 0.8;
                this.height = 600;
                this.width = 480;
            },

            /**
             * Обработчик изменения значения HTML-элемента
             */
            selectFile: function () {
                this.$inputFieldFake.val(this.$inputField.val());
                var fileList = this.$inputField.prop('files');
                this.model.attributes['value'] = {fileName : fileList[0].name, file : fileList[0]}
            },

            /**
             * Очистка значения атрибута
             */
            deleteFile: function () {
                this.model.attributes['value'] = {fileName: undefined, file: undefined, deleted: true};
                this.render();
            },

            /**
             * Скачать файл
             */
            downloadFile: function () {
                var _this = this;
                var objectId = $(event.srcElement).attr('objectId');
                this.model.callServerAction(
                    {
                        action: 'downloadFile',
                        data: {
                            objectId: objectId,
                            className: _this.model.get('className'),
                            attrName: _this.model.get('entityFieldName')
                        }
                    },
                    function (result) {
                        window.location = result.url;
                    }
                );
            },

            doPhoto: function(){
                var _this = this;
                _this.$el.append(_.template(photoCrop,{sizeFactor:_this.sizeFactor,width: _this.width,height:_this.height}));
               // _this.$el.find("#photo").modal("show");
                _this.$el.find("#photo").dialog( {
                    title:"Фотография",
                    dialogClass:"no-close",
                    autoOpen:true,
                    width:1100,
                    height:625,
                    buttons: {
                        "Закрыть": function() {
                            $( this ).dialog( "close" );
                        }
                    }
                });

                _this.$el.find("#photo").dialog('open');
                var showMask = $("#showMask");
                var file = $("#file");
                showMask.on('click',function(){_this.showOrHideMask()});
                file.on('change',function(){
                    _this.uploadFile(_this.getPhotoInfo);
                });
                //_this.$el.find("#onCamera").on("click",function(){
                $("#onCamera").on("click",function(){
                    _this.loadApplet();
                });
            },
            loadApplet: function(){
                var _this = this;
                window.onStartLoading = function(){}
                window.onExitFunction = function(){
                    $('#camWindow').dialog('close');
                }
                window.onCompleteLoading = function(fileName){
                    $('#camWindow').dialog('close');
                    _this.getPhotoInfo(fileName);
                }
                $('#camWindow').dialog(
                    {
                        title:"Сделать снимок",
                        dialogClass:"no-close",
                        autoOpen:true,
                        width:940,
                        height:650
                    }
                );
                dtjava.addOnloadCallback(_this.loadApp);

            },
            loadApp: function(){
                var _this = this;
                dtjava.embed(
                    {
                        id:'fxApp',
                        url: misc.getContextPath() + '/public/applet/webCamera.jnlp',
                        placeholder:'webCamDialog',
                        width:896,
                        height:570,
                        params:{
                            host:'http://' + location.host +"/" + misc.getContextPath()+ "/upload?userLogin="+$.cookie('userLogin'),
                            onCompleteUploadFunction:"onCompleteLoading",
                            onStartUploadFunction:"onStartLoading",
                            onExitFunction:"onExitFunction",
                            photoId:"photo",
                            placeholder:'webCamDialog'
                        }
                    },
                    {
                        javafx:'2.2+'
                    },
                    {}
                );
            }
            ,
            showOrHideMask: function(){
                var _this = this;
                var mask = $('#photomask');
                if($("#showMask").is(':checked')){
                    mask.css("top", 0);
                    mask.css("left", 5);
                    mask.css("height", _this.height*_this.sizeFactor);
                    mask.css("width", _this.width*_this.sizeFactor);
                    //mask.css("display", "block !important");
                    mask.css("visibility", "visible");
                }else{
                    mask.css("visibility", "hidden");
                }
            },
            uploadFile: function(callback){
                var _this = this;
                var importFile = $('#file')[0].files[0];
                console.log('Start uploading file: ' + importFile.name);
                var xhr = new XMLHttpRequest();
                var url = misc.getContextPath() + "/upload";
                xhr.onreadystatechange = function (e) {
                    if (4 == this.readyState) {
                        console.log('File uploaded: ' + e.target.responseText);
                        callback.apply(_this, [importFile.name]);
                    }
                };
                xhr.open('post', url, true);
                var fd = new FormData;
                fd.append(importFile.name,importFile);
                xhr.send(fd);

            },
            getPhotoInfo: function(name){
                var _this = this;
                if (_this.jcrop_api) {
                    _this.jcrop_api.destroy();
                    $('#photo_full').removeAttr("style");
                }
                _this.fileName = name;
                $.ajax({
                    type:"POST",
                    url: misc.getContextPath() +'/util/crop',
                    data:{'fileName':name,action:'getInfo'}
                }).then(
                        function(resp){
                            _this.showPhoto(resp);
                        }
                    )
            } ,
            showPhoto: function(info){
                var _this = this;
                var photoFull = $('#photo_full');
                photoFull.attr("src",  misc.getContextPath()+"/getTempFile?fileName="+info.fileName);

                photoFull.css("width", info.width);
                photoFull.css("height", info.height);
                _this.model.set('boundx', info.width);
                _this.model.set('boundy', info.height);
                $('#photo_cut').attr("src",  misc.getContextPath()+"/getTempFile?fileName="+info.fileName);
                photoFull.css("display", 'none !important;');
                //_this.sizeFactor = info.height/info.width;
                photoFull.Jcrop({
                    setSelect:[0, 0, info.width, info.height],
                    onChange: function(c){_this.updatePreview(c)},
                    onSelect: function(c){_this.saveDate(c)},
                    aspectRatio:_this.sizeFactor//info.height/info.width//
                }, function () {
                    var bounds = this.getBounds();
                    _this.model.set('boundx', bounds[0]);
                    _this.model.set('boundy', bounds[1]);
                    _this.jcrop_api = this;
                });
            },
            updatePreview: function(c){
                var _this = this;
                if (parseInt(c.w) > 0) {
                    var rx = _this.width / c.w;
                    var ry =  _this.height  / c.h;
                    var factor = _this.sizeFactor;

                    $('#photo_cut').css({
                        width:Math.round(rx * factor * _this.model.get('boundx')) + 'px',
                        height:Math.round(ry * factor * _this.model.get('boundy')) + 'px',
                        marginLeft:'-' + Math.round(rx * c.x * factor) + 'px',
                        marginTop:'-' + Math.round(ry * c.y * factor) + 'px'
                    });
                }
            },
            saveDate: function(c) {
                var _this = this;
                var rrr = c.x + ',' + c.y + ',' + c.h + ',' + c.w;
                var options = {
                    action: "savePhoto",
                    x: c.x,
                    y: c.y,
                    height: c.h,
                    width: c.w,
                    fileName:_this.fileName
                }
                $.ajax({
                    type:"POST",
                    url:misc.getContextPath() +'/util/crop',
                    async:false,
                    data:options}).then(function (data) {
                        _this.$inputFieldFake.val(data.fileName);
                        _this.model.attributes['value'] = {fileName : data.fileName};
                    });
                _this.updatePreview(c);
        }

        });

        return view;
    });
