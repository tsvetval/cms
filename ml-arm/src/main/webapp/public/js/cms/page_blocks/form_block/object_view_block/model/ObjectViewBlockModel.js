/**
 * Модель блока просмотра объекта
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/form_block/FormBlockModel',
        'cms/events/NotifyPageBlocksEventObject',
        'cms/page_blocks/form_block/model/AttrModel',
        'cms/page_blocks/form_block/model/AttrGroupModel',
        'cms/page_blocks/form_block/collections/AttrCollection',
        'cms/page_blocks/form_block/collections/AttrGroupCollection',
        'console_const'
    ],
    function (log, misc, backbone, FormBlockModel, NotifyEventObject, AttrModel, AttrGroupModel, AttrCollection, AttrGroupCollection,console_const) {
        var model = FormBlockModel.extend({
            defaults: {
                entityName: undefined,
                description: undefined,
                title: undefined,
                attrList: undefined,
                nonGroupAttrList: undefined,
                groupList: undefined,
                rootGroupList: undefined,
                pageTitle : ''
            },

            /**
             * Инициализация модели
             */
            initialize: function () {
                log.debug('initialize ObjectViewBlockModel');
                model.__super__.initialize.call(this);
                this.pageTitle = 'Просмотр объекта <%=data.title%>';
            },

            /**
             * Переход в режим редактирования объекта
             */
            startEditObject : function(){
                this.replacePage(
                    OBJECT_EDIT_PAGE,
                    'Просмотр объекта ...',
                    {
                        objectId:  this.get('objectId'),
                        className: this.get('className')
                    }
                );
            }
        });
        return model;
    });
