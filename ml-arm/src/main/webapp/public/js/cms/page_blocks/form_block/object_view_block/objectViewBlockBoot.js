/**
 * Загрузочный модуль блока просмотра объекта
 * Контроллер: ru.peak.ml.web.block.controller.impl.form.ObjectViewBlockController
 */
define(
    ['log', 'misc', 'backbone',
     'cms/page_blocks/form_block/object_view_block/model/ObjectViewBlockModel',
     'cms/page_blocks/form_block/object_view_block/view/ObjectViewBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });
