<div class="container control-panel col-lg-24" id="view_buttons_container">
    <h4 class="form-title col-lg-10 callback">
        <%=formModel.get('title')%><small>&nbsp(просмотр объектa класса <%=formModel.get('description')%>)</small>
    </h4>
    <div class="pull-right control-buttons">
        <div>
        <% if (canEdit) { %>
            <button class="btn btn-primary edit-object-button">
                <span class="glyphicon glyphicon-pencil"></span> Редактировать
            </button>
        <%}%>
        </div>
    </div>
</div>
<div id="view_content_container">

</div>
