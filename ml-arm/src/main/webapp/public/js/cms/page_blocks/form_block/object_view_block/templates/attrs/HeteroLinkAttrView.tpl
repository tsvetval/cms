<div class="col-md-offset-<%=attrModel.get('offset')%> col-md-<%=attrModel.get('titleLength')%>">
    <b class="attr-label"><%=attrModel.get('description')%>:</b>
</div>
<div class="col-md-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
    <span class="attr-list-preview">
    </span>
</div>
<div class="newLine attr-list-details col-md-<%=(attrModel.get('totalLength'))%>"
     style="overflow: auto; max-height: <%=attrModel.get('tableHeight')%>px; display: none;">
    <table class="table-javascript">
    </table>
</div>


