<div class="col-md-offset-<%=attrModel.get('offset')%> col-md-<%=attrModel.get('titleLength')%>">
    <b class="attr-label"><%=attrModel.get('description')%>:</b>
</div>
<div class="col-md-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
    <%if (attrModel.get('value')){%>
        <a class="ml-details"
           objectId="<%=attrModel.get('value').objectId%>"
           mlClass="<%=attrModel.get('value').mlClass%>">
            <%=attrModel.get('value').title%>
        </a>
    <%}%>
</div>
