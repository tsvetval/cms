/**
 * Представление блока просмотра объекта
 */
define(
    ['log', 'misc', 'backbone', 'underscore',
        'cms/page_blocks/form_block/FormBlockView',
        'markup',
        'cms/page_blocks/DialogPageBlock',
        'cms/page_blocks/form_block/collections/AttrGroupCollection',
        'cms/page_blocks/form_block/factories/GroupViewFactory',
        'cms/page_blocks/form_block/factories/AttrViewFactory',
        'text!cms/page_blocks/form_block/object_view_block/templates/ObjectViewTemplate.tpl'],
    function (log, misc, backbone, _, FormBlockView, markup, Message, AttrGroupCollection,
              GroupViewFactory, AttrViewFactory, ObjectViewTemplate) {
        var view = FormBlockView.extend({

            events : {
                "click .edit-object-button" : "editObjectClick"
            },

            /**
             * Иницализация представления
             */
            initialize: function () {
                log.debug("initialize ObjectViewBlockView");
                this.viewMode = 'VIEW';
                this.listenTo(this.model, 'render', this.render);
                this.listenTo(this.model, 'renderAttribute', this.renderAttribute);
                _.extend(this.events, FormBlockView.prototype.events);
            },


            /**
             * Обработчик клика по кнопке "Редактировать"
             */
            editObjectClick : function(){
                this.model.startEditObject();
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                var _this = this;
                this.$el.html(_.template(ObjectViewTemplate, {formModel: this.model,canEdit:_this.model.get("canEdit")}));
                // create container
                var $content_container = _this.$el.find('#view_content_container');
                _this._renderNonGroupAttrs($content_container);
                // Выводим группы
                _this._renderRootGroups($content_container);
                return true;
            }
        });

        return view;
    });
