/**
 * Представление для отображение атрибута типа BOOLEAN
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/form_block/view/AttrView',
        'text!cms/page_blocks/form_block/object_view_block/templates/attrs/BooleanAttrView.tpl'],
    function (log, misc, backbone,
              AttrView, DefaultTemplate) {
        var view = AttrView.extend({

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, AttrView.prototype.events);
            }
        });

        return view;
    });
