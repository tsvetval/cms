/**
 * Представление для отображение атрибута типа FILE
 */
define(
    ['log', 'misc', 'backbone', 'moment',
        'cms/page_blocks/form_block/view/AttrView',
        'text!cms/page_blocks/form_block/object_view_block/templates/attrs/FileAttrView.tpl'],
    function (log, misc, backbone, moment, AttrView, DefaultTemplate) {
        var view = AttrView.extend({
            events : {
                "click .file-download-link" : "downloadFile"
            },
            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, AttrView.prototype.events);
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                if (this.isHidden()) {
                    return;
                }
                var title = '';
                if (this.model.get('value')) {
                    title = this.model.get('value').title;
                }
                this.$el.html(_.template(this.viewTemplate, {
                    attrModel: this.model,
                    fileTitle: title

                }));
            },

            /**
             * Скачивание файла
             */
            downloadFile: function () {
                var _this = this;
                var objectId = $(event.srcElement).attr('objectId');
                this.model.callServerAction(
                    {
                        action: 'downloadFile',
                        data: {
                            objectId: objectId,
                            className: _this.model.get('className'),
                            attrName: _this.model.get('entityFieldName')
                        }
                    },
                    function (result) {
                        window.location = result.url;
                    }
                );
            }

        });

        return view;
    });
