/**
 * Представление для отображение атрибута типа HETERO_LINK
 */
define(
    ['log', 'misc', 'backbone', 'underscore', 'jquery', 'boot_table_ru', 'moment',
        'cms/page_blocks/form_block/view/AttrView',
        'text!cms/page_blocks/form_block/object_view_block/templates/attrs/HeteroLinkAttrView.tpl',
        'table_formatter_mixin'
    ],
    function (log, misc, backbone, _, $, boot_table, moment,
              AttrView, DefaultTemplate, table_formatter_mixin) {
        var view = AttrView.extend({
            events : {
                "click .attr-list-preview-item": "linkObjectPreviewClick",
                "click .file-download-link": "downloadFile"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, AttrView.prototype.events);
            },

            /**
             * Отрисовка представления
             */
            render : function() {
                if (this.isHidden()) {
                    return;
                }
                var _this = this;
                this.$el.html( _.template(this.viewTemplate, {attrModel : this.model}));
                var $previewContainer = this.$el.find('.attr-list-preview');

                this.model.get('value').objectList.forEach(function(objectValue){
                    var $objectContainer = $('<span/>',
                        {"class" : "attr-list-preview-item",
                            "objectId" : objectValue.objectId,
                            "mlClass" : objectValue.mlClass}
                    );
                    $objectContainer.html(objectValue.title + ' ');
                    $previewContainer.append($objectContainer);
                    $previewContainer.append('\n');
                });
            },

            /**
             * Создание таблицы атрибутов связанных объектов
             */
            createAttrTable: function () {
                var _this = this;
                this.$table = this.$el.find('.table-javascript');

                var columns = [];
                columns.push({
                    field: 'objectId',
                    visible: false
                });
                this.model.get('value').attrList.forEach(function (columnAttr) {
                    var formatter = undefined;
                    var sortable = true;
                    if (columnAttr.fieldType === "ONE_TO_MANY" || columnAttr.fieldType === "MANY_TO_MANY") {
                        formatter = _this.formatterOneToMany;
                        sortable = false;
                    } else if (columnAttr.fieldType === "MANY_TO_ONE") {
                        formatter = _this.formatterManyToOne;
                        sortable = false;
                    } else if (columnAttr.fieldType === "BOOLEAN") {
                        formatter = _this.formatterBoolean;
                        sortable = true;
                    } else if (columnAttr.fieldType === "ENUM") {
                        formatter = _this.formatterEnum;
                        sortable = true;
                    } else if (columnAttr.fieldType === "DATE") {
                        formatter = _this.formatterDateWithOptions({dateFormat : columnAttr.fieldFormat});;
                        sortable = true;
                    } else if (columnAttr.fieldType === "FILE") {
                        formatter = _this.formatterFile;
                        sortable = true;
                    } else if (columnAttr.fieldType === "LONG_LINK") {
                        sortable = false;
                    }

                    var column = {
                        field: columnAttr.entityFieldName,
                        title: columnAttr.description,
                        align: 'left',
                        valign: 'top',
                        sortable: sortable,
                        formatter: formatter
                    };
                    columns.push(column);
                });

                var data = this.createTableDataFromValue();

                this.$table.bootstrapTable({
                    toolbar: _this.$toolbar,
                    label: _this.$attrLabelContainer,
                    idField: 'objectId',
                    data: data,
                    cache: false,
                    striped: true,
                    pagination: true,
                    pageSize: 5,
                    pageList: [5, 10, 20, 50],
                    showColumns: true,
                    minimumCountColumns: 1,
                    search: true,
                    clickToSelect: true,
                    columns: columns,
                    toolbarAlign: 'right'
                });
            },

            /**
             * Привязка методов скрытия-отображения таблицы атрибутов связанных объектов
             */
            attachLinkedAttrCollapse: function () {
                var _this = this;
                this.attrTable = undefined;
                //скрытие-показ списка связанных объектов (ONE_TO_MANY, MANY_TO_MANY) по нажатию на +
                this.$el.find('.attr-list-title').on('click', function () {
                    if (!_this.attrTable) {
                        _this.attrTable = _this.createAttrTable();
                    }
                    var $container = $(this).parent().parent();
                    if ($(this).hasClass('expanded-list')) {
                        $(this).removeClass('expanded-list');
                        $container.find('.attr-list-details').first().slideUp('fast',
                            function () {
                                $container.find('.attr-list-preview').first().fadeIn('fast');
                            }
                        );
                    } else {
                        $(this).addClass('expanded-list');
                        $container.find('.attr-list-preview').first().fadeOut('fast',
                            function () {
                                $container.find('.attr-list-details').first().slideDown('fast');
                            }
                        );
                    }
                });
            },

            /**
             * Заполнение данных об атрибутах связанных объектов
             */
            createTableDataFromValue: function () {
                var data = [];
                if (this.model.get('value') && this.model.get('value').objectList) {
                    this.model.get('value').objectList.forEach(function (objectData) {
                        data.push($.extend(objectData.attrValues, {objectId: objectData.objectId}));
                    });
                }
                return data;
            },

            /**
             * Обработка клика по связанному объекту (переход к просмотру объекта)
             */
            linkObjectPreviewClick : function(){
                this.model.openObject($(event.srcElement).attr('objectId'), $(event.srcElement).attr('mlClass'));
                return false;
            }

        });

        _.extend(AttrView.prototype, table_formatter_mixin);

        return view;
    });
