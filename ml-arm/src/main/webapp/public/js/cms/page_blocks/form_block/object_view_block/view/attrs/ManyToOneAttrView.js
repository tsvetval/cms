/**
 * Представление для отображение атрибута типа MANY_TO_ONE
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/form_block/view/AttrView',
        'text!cms/page_blocks/form_block/object_view_block/templates/attrs/ManyToOneAttrView.tpl'],
    function (log, misc, backbone,
              AttrView, DefaultTemplate) {
        var view = AttrView.extend({
            events : {
                "click .ml-details" : "objectDetailsClick"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, AttrView.prototype.events);
            },

            /**
             * Обработка клика по связанному объекту (переход к просмотру объекта)
             */
            objectDetailsClick : function(){
                this.model.openObject($(event.srcElement).attr('objectId'), $(event.srcElement).attr('mlClass'));
                return false;
            }
        });

        return view;
    });
