/**
 * Представление для отображение атрибута типа DATE
 */
define(
    ['log', 'misc', 'backbone', 'moment',
        'cms/page_blocks/form_block/view/AttrView',
        'text!cms/page_blocks/form_block/object_view_block/templates/attrs/DateAttrView.tpl'],
    function (log, misc, backbone, moment, AttrView, DefaultTemplate) {
        var view = AttrView.extend({
            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, AttrView.prototype.events);
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                if (this.isHidden()) {
                    return;
                }
                var title = '';
                if (this.model.get('value')) {
                    var dateMoment = moment(this.model.get('value'));
                    title = dateMoment/*.utc()*/.format("HH:mm");

                }
                this.$el.html(_.template(this.viewTemplate, {
                    attrModel: this.model,
                    dateTitle: title
                }));
            }

        });

        return view;
    });
