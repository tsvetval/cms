define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/form_block/object_edit_block/view/EditAttrView',
        'text!cms/page_blocks/form_block/templates/custom/create/codename_class.tpl'],
    function (log, misc, backbone, EditAttrView, DefaultTemplate) {
        var view = EditAttrView.extend({
            $inputField: undefined,

            events: {
                "keyup .attrField": "keyPressed",
                "blur .attrField": "done"
            },

            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
            },

            render: function () {
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.$inputField = this.$el.find('.attrField');
                this.$inputField.val(this.model.get('value'));
                this.addMandatoryEvents();
            },

            keyPressed: function (e) {
                this.model.attributes['value'] = this.$inputField.val();
                if (e.keyCode == 13) {
                    this.done();
                }
            },

            done: function () {
                var entityName = this.model.get('value');
                var tableName = this.model.get('pageBlockModel').getAttrValue('tableName');
                if (tableName == undefined || tableName.length == 0) {
                    this.model.get('pageBlockModel').setAttrValue('tableName', entityName);
                    this.model.get('pageBlockModel').setAttrFocus('tableName');
                }
            }

        });

        return view;
    });
