define(
    ['log', 'misc', 'backbone', 'select2',
        'cms/page_blocks/form_block/view/AttrView',
        'text!cms/page_blocks/form_block/templates/custom/create/dropdown_list_attrgroup.tpl'],
    function (log, misc, backbone, select2, AttrView, DefaultTemplate) {
        var view = AttrView.extend({
            $inputField: undefined,

            events: {
                "keyup .attrField": "keyPressed"
            },

            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
            },

            render: function () {
                var _this = this;
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.$inputField = this.$el.find('.attrField');
                var mlClass = this.model.get('pageBlockModel').getAttrValue('mlClass');
                this.model.callServerAction(
                    {
                        action: 'getAttrGroupListForMlClass',
                        data: {
                            mlClass: mlClass.objectId
                        }
                    },
                    function (result) {
                        var select2Data = [];
                        result.AttrGroupListData.objectList.forEach(function (attrGroup) {
                            select2Data.push({id: attrGroup.objectId, text: attrGroup.title});
                        });
                        _this.$inputField.select2({
                            width: '100%',
                            data: select2Data,
                            allowClear: true,
                            placeholder: "Выберите группу атрибута"
                        });
                        if (_this.model.get('value') && _this.model.get('value').objectId) {
                            _this.$inputField.val(_this.model.get('value').objectId).trigger("change");
                        } else {
                            _this.$inputField.val(null).trigger("change");
                        }
                        _this.$inputField.on("change", function () {
                            _this.changeSelection(_this.$inputField.val());
                        })
                    }
                );
            },

            changeSelection: function (value) {
                this.model.attributes['value'] = {objectId: value};
            },

            keyPressed: function (val) {
                this.model.attributes['value'] = this.$inputField.val();
            }

        });

        return view;
    });
