define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/form_block/view/AttrView',
        'text!cms/page_blocks/form_block/templates/custom/create/iconselect.tpl',
        'iconselect'],
    function (log, misc, backbone, AttrView, DefaultTemplate, IconSelect) {
        var view = AttrView.extend({
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                this.getIconList();
            },

            render: function () {
                console.dir(this.model);
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
            },

            getIconList: function () {
                var _this = this;
                this.model.callServerAction(
                    {
                        action: 'getIconList',
                        data: {
                            currentIcon: _this.model.get('value')
                        }
                    },
                    function (result) {
                        var iconList = JSON.parse(result.iconList);
                        var iconFolder = result.iconFolder;
                        var iconIndex = result.iconIndex + 1;
                        var iconSelect = new IconSelect("icon-select",
                            {
                                'selectedIconWidth': 23,
                                'selectedIconHeight': 23,
                                'selectedBoxPadding': 0,
                                'iconsWidth': 48,
                                'iconsHeight': 48,
                                'boxIconSpace': 1,
                                'vectoralIconNumber': 0,
                                'horizontalIconNumber': 6
                            }
                        );
                        var icons = [];
                        icons.push({'iconFilePath': '', 'iconValue': ''});
                        $.each(iconList, function (index, icon) {
                            icons.push({'iconFilePath': iconFolder + "/" + icon, 'iconValue': icon});
                        });
                        iconSelect.refresh(icons);
                        iconSelect.setSelectedIndex(iconIndex);
                        _this.$el.find('.icon-select').on('changed', function (e) {
                            _this.model.set('value', iconSelect.getSelectedValue());
                        });
                    }
                );
            }

        });

        return view;
    });
