define(
    ['log', 'misc', 'backbone', 'inputmask',
        'cms/page_blocks/form_block/object_edit_block/view/EditAttrView',
        'text!cms/page_blocks/form_block/templates/custom/create/password.tpl'],
    function (log, misc, backbone, inputmask,
              EditAttrView, DefaultTemplate) {
        var view = EditAttrView.extend({
            $inputField: undefined,

            events: {
                "keyup .attrField": "keyPressed"
            },

            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, EditAttrView.prototype.events);
            },

            render: function () {
                if (this.isHidden()) {
                    return;
                }
                this.model.unset('value');
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.$inputField = this.$el.find('.attrField');
                this.addMandatoryEvents();
            },

            keyPressed: function (val) {
                this.model.set('value', this.$inputField.val());
            }

        });

        return view;
    });
