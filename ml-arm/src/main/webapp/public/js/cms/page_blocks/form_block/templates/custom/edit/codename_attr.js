define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/form_block/object_edit_block/view/EditAttrView',
        'text!cms/page_blocks/form_block/templates/custom/edit/codename_attr.tpl'],
    function (log, misc, backbone, EditAttrView, DefaultTemplate) {
        var view = EditAttrView.extend({
            $inputField: undefined,

            events: {
                "keyup .attrField": "keyPressed",
                "blur .attrField": "done"
            },

            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
            },

            render: function () {
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.$inputField = this.$el.find('.attrField');
                this.$inputField.val(this.model.get('value'));
                this.addMandatoryEvents();
            },

            keyPressed: function (e) {
                this.model.attributes['value'] = this.$inputField.val();
                if (e.keyCode == 13) {
                    this.done();
                }
            },

            done: function () {
                var entityFieldName = this.model.get('value');
                var tableFieldName = this.model.get('pageBlockModel').getAttrValue('tableFieldName');
                if (tableFieldName == undefined || tableFieldName.length == 0) {
                    this.model.get('pageBlockModel').setAttrValue('tableFieldName', entityFieldName);
                    this.model.get('pageBlockModel').setAttrFocus('tableFieldName');
                }
            }

        });

        return view;
    });
