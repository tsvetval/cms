define(
    ['log', 'misc', 'backbone', 'select2',
        'cms/page_blocks/form_block/view/AttrView',
        'text!cms/page_blocks/form_block/templates/custom/edit/dropdown_list_mlclasses.tpl'],
    function (log, misc, backbone, select2, AttrView, DefaultTemplate) {
        var view = AttrView.extend({
            $inputField: undefined,

            events: {
                "keyup .attrField": "keyPressed"
            },

            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
            },

            render: function () {
                var _this = this;
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.$inputField = this.$el.find('.attrField');
                var attrType = this.model.get('pageBlockModel').getAttrValue('fieldType');
                this.model.callServerAction(
                    {
                        action: 'getMlClassInstanceList',
                        data: {
                            mlClass: "MlClass"//attrType.code
                        }
                    },
                    function (result) {
                        var select2Data = [];
                        result.MlClassInstanceListData.objectList.forEach(function (attrView) {
                            select2Data.push({id: attrView.objectId, text: attrView.title});
                        });
                        _this.$inputField.select2({
                            width: '100%',
                            data: select2Data,
                            allowClear: true,
                            placeholder: "Выберите класс"
                        });
                        if (_this.model.get('value') && _this.model.get('value').objectId) {
                            _this.$inputField.val(_this.model.get('value').objectId).trigger("change");
                        } else {
                            _this.$inputField.val(null).trigger("change");
                        }
                        _this.$inputField.on("change", function () {
                            _this.changeSelection(_this.$inputField.val());
                        })
                    }
                );
            },

            changeSelection: function (value) {
                this.model.attributes['value'] = {objectId: value};
            },

            keyPressed: function (val) {
                this.model.attributes['value'] = this.$inputField.val();
            }

        });

        return view;
    });
