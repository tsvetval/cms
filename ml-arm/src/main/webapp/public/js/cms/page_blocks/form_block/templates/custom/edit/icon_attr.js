define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/form_block/view/AttrView',
        'text!cms/page_blocks/form_block/templates/custom/edit/icon_attr.tpl',
        'select2'],
    function (log, misc, backbone, AttrView, DefaultTemplate) {
        var view = AttrView.extend({
            icons: ["asterisk", "plus", "euro", "minus", "cloud", "envelope", "pencil", "glass", "music", "search",
                "heart", "star", "star-empty", "user", "film", "th-large", "th", "th-list", "ok", "remove",
                "zoom-in", "zoom-out", "off", "signal", "cog", "trash", "home", "file", "time", "road",
                "download-alt", "download", "upload", "inbox", "play-circle", "repeat", "refresh",
                "list-alt", "lock", "flag", "headphones", "volume-off", "volume-down", "volume-up", "qrcode",
                "barcode", "tag", "tags", "book", "bookmark", "print", "camera", "font", "bold", "italic",
                "text-height", "text-width", "align-left", "align-center", "align-right", "align-justify",
                "list", "indent-left", "indent-right", "facetime-video", "picture", "map-marker", "adjust",
                "tint", "edit", "share", "check", "move", "step-backward", "fast-backward", "backward",
                "play", "pause", "stop", "forward", "fast-forward", "step-forward", "eject", "chevron-left",
                "chevron-right", "plus-sign", "minus-sign", "remove-sign", "ok-sign", "question-sign",
                "info-sign", "screenshot", "remove-circle", "ok-circle", "ban-circle", "arrow-left",
                "arrow-right", "arrow-up", "arrow-down", "share-alt", "resize-full", "resize-small",
                "exclamation-sign", "gift", "leaf", "fire", "eye-open", "eye-close", "warning-sign", "plane",
                "calendar", "random", "comment", "magnet", "chevron-up", "chevron-down", "retweet",
                "shopping-cart", "folder-close", "folder-open", "resize-vertical", "resize-horizontal",
                "hdd", "bullhorn", "bell", "certificate", "thumbs-up", "thumbs-down", "hand-right", "hand-left",
                "hand-up", "hand-down", "circle-arrow-right", "circle-arrow-left", "circle-arrow-up",
                "circle-arrow-down", "globe", "wrench", "tasks", "filter", "briefcase", "fullscreen",
                "dashboard", "paperclip", "heart-empty", "link", "phone", "pushpin", "usd", "gbp", "sort",
                "sort-by-alphabet", "sort-by-alphabet-alt", "sort-by-order", "sort-by-order-alt",
                "sort-by-attributes", "sort-by-attributes-alt", "unchecked", "expand", "collapse-down",
                "collapse-up", "log-in", "flash", "log-out", "new-window", "record", "save", "open",
                "saved", "import", "export", "send", "floppy-disk", "floppy-saved", "floppy-remove",
                "floppy-save", "floppy-open", "credit-card", "transfer", "cutlery", "header", "compressed",
                "earphone", "phone-alt", "tower", "stats", "sd-video", "hd-video", "subtitles",
                "sound-stereo", "sound-dolby", "sound-5-1", "sound-6-1", "sound-7-1", "copyright-mark",
                "registration-mark", "cloud-download", "cloud-upload", "tree-conifer", "tree-deciduous"],

            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
            },

            render: function () {
                var _this = this;
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                var $select = this.$el.find(".icon-attr");
                $select.css("border-left", "1px solid grey");

                $.each(this.icons, function (index, icon) {
                    var iconClass = "glyphicon glyphicon-" + icon;
                    var $option = $("<option>" + icon + "</option>").val(iconClass);
                    if (_this.model.get('value') == iconClass) {
                        $option.attr("selected", "selected");
                    }
                    $select.append($option);
                });

                function format(state) {
                    if (state.text.trim() != "(empty)")
                        return "<span class='glyphicon glyphicon-" + state.text + "'></span> (" + state.text + ")";
                    return "(empty)";
                }

                $select.select2({
                    width: '100%',
                    formatResult: format,
                    formatSelection: format,
                    escapeMarkup: function (m) {
                        return m;
                    }
                });

                $select.on('change', function () {
                    _this.model.set('value', $select.val());
                });
            }
        });

        return view;
    });
