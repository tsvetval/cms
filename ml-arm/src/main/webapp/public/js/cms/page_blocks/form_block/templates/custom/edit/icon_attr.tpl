<div class="attr-label-container col-lg-offset-<%=attrModel.get('offset')%> col-lg-<%=attrModel.get('titleLength')%>">
    <b><%=attrModel.get('description')%>:</b>
</div>

<div class="select2holder col-md-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
    <select type="hidden" class="attrField icon-attr"/>
</div>