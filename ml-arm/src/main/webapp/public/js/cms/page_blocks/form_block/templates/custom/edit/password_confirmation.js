define(
    ['log', 'misc', 'backbone', 'inputmask',
        'cms/page_blocks/form_block/object_edit_block/view/EditAttrView',
        'text!cms/page_blocks/form_block/templates/custom/edit/password_confirmation.tpl'],
    function (log, misc, backbone, inputmask,
              EditAttrView, DefaultTemplate) {
        var view = EditAttrView.extend({
            $inputField: undefined,

            events: {
                "keyup .attrField": "keyPressed",
                "blur .attrField": "done"
            },

            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, EditAttrView.prototype.events);
            },

            render: function () {
                if (this.isHidden()) {
                    return;
                }
                this.model.set('value', "");
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.$inputField = this.$el.find('.attrField');
                this.addMandatoryEvents();
            },

            keyPressed: function (e) {
                this.model.set('value', this.$inputField.val());
                if (e.keyCode == 13) {
                    this.done();
                }
            },

            done: function () {
                var confirmation = this.model.get('value');
                var password = this.model.get('pageBlockModel').getAttrValue('password');
                if (confirmation != password) {
                    this.model.unset('value');
                }
            }


        });

        return view;
    });