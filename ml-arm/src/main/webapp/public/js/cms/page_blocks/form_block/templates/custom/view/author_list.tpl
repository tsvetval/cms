<div class="col-md-offset-<%=attrModel.get('offset')%> col-md-<%=attrModel.get('titleLength')%>">
    <b><%=attrModel.get('description')%>:</b>
</div>
<div class="col-md-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
    <select multiple style="width: 100%">
        <% _.each(attrModel.get('value').objectList, function(author) {%>
        <option value="<%= author.objectId %>"><%= author.title %></option>
        <%});%>
    </select>
</div>
