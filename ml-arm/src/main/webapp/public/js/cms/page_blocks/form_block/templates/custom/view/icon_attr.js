define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/form_block/view/AttrView',
        'text!cms/page_blocks/form_block/templates/custom/view/icon_attr.tpl'],
    function (log, misc, backbone, AttrView, DefaultTemplate) {
        var view = AttrView.extend({
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
            }

        });

        return view;
    });
