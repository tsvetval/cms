define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/form_block/view/AttrView',
        'text!cms/page_blocks/form_block/templates/custom/view/iconselect.tpl'],
    function (log, misc, backbone, AttrView, DefaultTemplate) {
        var view = AttrView.extend({
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                var iconFilename = this.model.get('value');
                if (iconFilename) {
                    this.getIconURL(iconFilename);
                }
                this.listenTo(this.model, 'change:iconURL', this.render);
            },

            render: function () {
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
            },

            getIconURL: function (filename) {
                var _this = this;
                this.model.callServerAction(
                    {
                        action: 'getIconURL',
                        data: {
                            filename: filename
                        }
                    },
                    function (result) {
                        _this.model.set('iconURL', result.iconURL);
                    }
                );
            }

        });

        return view;
    });
