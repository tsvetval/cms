<div class="col-lg-offset-<%=attrModel.get('offset')%> col-lg-<%=attrModel.get('titleLength')%>">
    <b><%=attrModel.get('description')%>:</b>
</div>
<div class="col-lg-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
    <% if (typeof(attrModel.get('iconURL')) != "undefined") { %>
    <div class="iconselect" style="background-image: url('<%=attrModel.get('iconURL')%>');"></div>
    <% } %>
</div>


