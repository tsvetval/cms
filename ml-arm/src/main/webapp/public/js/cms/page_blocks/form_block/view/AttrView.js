define(
    ['log', 'misc', 'backbone'],
    function (log, misc, backbone) {
        var view = backbone.View.extend({
            $attrLabel : undefined,

            events: {
                "dblclick .attr-label": "openAttrView"
            },


            initialize: function () {
                console.log("initialize AttrView for model class = " + this.model.getAttrEntityFiledName());
                this.listenTo(this.model, 'render', this.render);
                this.listenTo(this.model, 'change:hidden', this.render);

            },

            render: function () {
                if (this.isHidden()) {
                    return;
                }
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
            },

            isHidden: function () {
                var hidden = this.model.get('hidden');
                if (hidden) {
                    this.$el.empty();
                    return true;
                } else {
                    return false;
                }
            },

            openAttrView: function () {
                this.model.openAttrView()
            }
        });

        return view;
    });
