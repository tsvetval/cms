define(
    ['log', 'misc', 'backbone', 'bootstrap',
        'cms/page_blocks/form_block/factories/GroupViewFactory',
        'cms/page_blocks/form_block/factories/AttrViewFactory'
    ],
    function (log, misc, backbone, bootstrap,
              GroupViewFactory, AttrViewFactory) {
        /*model : GroupModel*/
        /*
         * представление для групп в виду табов свеху (содержит сразу несколько табов)
         * */
        var view = backbone.View.extend({
            initialize: function (options) {
                console.log("initialize TabListGroupView");
                this.viewMode = options.viewMode;
            } ,

            render : function() {
                var _this = this;
                var $content_container = $('<div/>', {"class" : 'default-group-container'});
                this.$el.append($content_container);
                // Выводим все атрибуты группы
                var outAttrContainer = undefined;
                for (var index in this.model.get('attrCollection').models) {
                    var attrModel =  this.model.get('attrCollection').models[index];
                    if (attrModel.get('newLine') || !outAttrContainer){
                    // создаем новую строку
                        outAttrContainer = new $('<div/>', {/*id: block.get('blockInfo').get('guid'),*/ "class": "col-md-24 newLine"});
                        $content_container.append(outAttrContainer);
                    }
                    // создаем контейнер для атрибута
                    var $attrContainer = $('<div/>', {/*id: block.get('blockInfo').get('guid'),*/ "class": "attrContainer"});
                    outAttrContainer.append($attrContainer);
                     //TODO refactor
                    _this._renderAttr(attrModel, $attrContainer);

                }

                //render groups
            },
            _renderAttr : function(attrModel, $attrContainer) {
                AttrViewFactory.getAttrViewClass(attrModel, this.viewMode, $attrContainer)
                    .then(function (attrViewClass, model, $container) {
                        // Получаем представление для конкретного атрибуты
                        var attrView = new attrViewClass({model: model});
                        attrView.setElement($container);
                        attrView.render();
                    });

            }

        });

        return view;
    });
