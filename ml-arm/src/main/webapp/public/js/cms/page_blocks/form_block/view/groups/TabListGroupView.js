define(
    ['log', 'misc', 'backbone', 'jquery', 'bootstrap', 'underscore',
      'cms/page_blocks/form_block/factories/GroupViewFactory',
      'text!cms/page_blocks/form_block/templates/groups/TabGroupViewTemplate.tpl'
    ],
    function (log, misc, backbone, $, bootstrap, _,
              GroupViewFactory, viewTemplate) {
        /*model : attrGroupCollection*/
        /*
        * представление для групп в виду табов свеху (содержит сразу несколько табов)
        * */
        var view = backbone.View.extend({
            initialize: function (options) {
                log.debug("initialize TabListGroupView");
                this.viewMode = options.viewMode;
                this.listenTo(this.model, 'highlightMandatory', this.highlightMandatory);
                this.listenTo(this.model, 'removeHighlightMandatory', this.removeHighlightMandatory);
            },

            render : function() {
                var _this = this;
                this.$el.html( _.template(viewTemplate, {attrGroupCollection : this.model}));
                var $tabsDiv = this.$el.find('.nav-tabs');
                var $contentDiv = this.$el.find('.tab-content');
                this.model.each(function(groupModel, i){
                    // create the tab
                    $tabsDiv.append($('<li data-groupId="' + groupModel.getGroupId() + '">' +
                    '<a href="#attr-group-content-' + groupModel.getGroupId()
                    + '" data-toggle="tab">' + groupModel.getTitle() + '</a></li>'));
                    // create the tab content
                    var $tabGroupContainer = $('<div class="tab-pane" id="attr-group-content-'+ groupModel.getGroupId() +'"></div>');
                    $contentDiv.append($tabGroupContainer);
                    // make the first tab active
                    if (i == 0) $tabsDiv.find('a:first').tab('show');

                    var groupViewClass = GroupViewFactory.getGroupViewClass(groupModel).then(function(GroupViewClass){
                        var groupView = new  GroupViewClass({model:groupModel, viewMode : _this.viewMode});
                        groupView.setElement($tabGroupContainer);
                        groupView.render();
                    });
                });
                // for right $contentDiv height
                $contentDiv.append( $('<div/>', {"class" : "newLine"}))

            },

            highlightMandatory: function (groupId) {
                var $tabHeader = this.$el.find('li[data-groupId=' + groupId + ']');
                if (!$tabHeader.hasClass('ml-mandatory-group')) {
                    $tabHeader.addClass('ml-mandatory-group');
                }
            },

            removeHighlightMandatory: function (groupId) {
                console.log("=========removeHighlightMandatory================");
                //TODO check other attr on the same tab
                var attrList = this.model.getGroupById(groupId).get('attrCollection').models;
                var groupIsMandatory = false;
                $.each(attrList, function (index, attr) {
                    if (attr.get('mandatory') && !attr.hasValue()) {
                        groupIsMandatory = true;
                    }
                });
                var $tabHeader = this.$el.find('li[data-groupId=' + groupId + ']');
                if (!groupIsMandatory && $tabHeader.hasClass('ml-mandatory-group')) {
                    $tabHeader.removeClass('ml-mandatory-group');
                }
            }
        });

        return view;
    });
