/*
* Отображает историю объектов. Если у класса включена история.
* Контроллер: ru.peak.ml.web.block.controller.impl.HistoryPageBlockController
* */

define(
    ['log', 'misc', 'backbone', 'cms/page_blocks/history_block/model/HistoryBlockModel', 'cms/page_blocks/history_block/view/HistoryBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });
