define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockModel'],
    function (log, misc, backbone, PageBlockModel) {
        var model = PageBlockModel.extend({
            defaults: {
                renderTemplate: undefined,
                currentPage: undefined,
                className: undefined,
                objectId : undefined,
                historyUtilId : undefined,
                title: undefined
            },
            initialize: function () {
                var _this = this;
                console.log("initialize ObjectListBlockModel");
                this.listenTo(this, 'restorePage', function(params){
                    if(params.objectId && params.className)
                        _this.loadHistoryList.apply(this, arguments);
                });
            },

            loadHistoryList: function (options) {

                var _this = this;
                this.set('className', options.className);
                this.set('objectId', options.objectId);
                this.set('historyUtilId', options.historyUtilId);
                this.syncRenderTemplate().then(
                    function () {
                        _this.triggerRender()
                    });
            },
            refreshCurrentObjectList: function(){
                var _this = this;
                this.syncRenderTemplate().then(
                    function () {
                        _this.triggerRender()
                    });
            },
            setCurrentPage: function (pageNumber) {
                var _this = this;
                this.set('currentPage', pageNumber);
                this.syncRenderTemplate().then(
                    function () {
                        _this.triggerRender()
                    });
            },

            syncRenderTemplate: function () {
                var _this = this;
                return $.ajax('page_block', {
                    type: 'POST',
                    data: {
                        action: 'show',
                        className: this.get('className'),
                        objectId: this.get('objectId'),
                        historyUtilId: this.get('historyUtilId'),
                        pageBlockId: this.get('blockInfo').get('id'),
                        currentPage: this.get('currentPage'),
                        ml_request: true
                    },
                    success: function (result) {
                        _this.set('title', result.title);
                        _this.set('renderTemplate', result.html);

                        // Обновляем текущую крошку
                        _this.navigationEvent({
                            "do":'modify',
                            "title": result.title // todo: нужно получать с сервера только название папки, а не весь заголовок вроде  "Список объектов Авторы"
                        });
                        
                    }
                });
            },

            triggerRender: function () {
                this.trigger('render');
            }


        });

        return model;
    });
