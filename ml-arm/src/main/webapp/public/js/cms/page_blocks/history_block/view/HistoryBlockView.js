define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup'],
    function (log, misc, backbone, PageBlockView, markup) {
        var view = PageBlockView.extend({
            initialize:function () {
                console.log("initialize ObjectListBlockView");
                this.listenTo(this.model, 'render', this.render)
            },

            render:function () {
                var _this = this;
                this.$el.html(this.model.get('renderTemplate'));
                markup.attachActions(this.$el, {

                        pageClick:function (number) {
                            _this.model.setCurrentPage(number);
//                            log.debug("Trigger Event [displayListPage] pageClick action, show page = " + number);
//                            Context.eventManager.trigger('displayListPage', {currentPage:number}, [${currentPageBlock.id}]);
                        }
                    }
                );
                //alert('ObjectListBlock do displayFolder');
            }

        });

        return view;
    });
