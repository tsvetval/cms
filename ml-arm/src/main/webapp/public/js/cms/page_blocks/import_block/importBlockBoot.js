/**
 * Блока импорта данных. Импорт через Excel.
 * Контроллер: ru.peak.ml.web.block.controller.impl.ImportObjectsBlockController
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/import_block/model/ImportBlockModel',
        'cms/page_blocks/import_block/view/ImportBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });
