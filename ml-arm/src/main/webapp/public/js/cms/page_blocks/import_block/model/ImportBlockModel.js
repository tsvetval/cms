/**
 * модель блока импорта данных
 */
define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockModel', 'cms/page_blocks/DialogPageBlock'],
    function (log, misc, backbone, PageBlockModel, Message) {
        var model = PageBlockModel.extend({
            defaults: {
                renderTemplate: undefined,
                folderId: undefined,
                className: undefined,
                data: undefined
            },

            /**
             * инициализация модели
             */
            initialize: function () {
                console.log("initialize BlockModel");

                this.listenTo(this, 'restorePage', function (params) {
                    var folderId = params.folderId;
                    var className = params.className;
                    this.set('folderId', folderId);
                    this.set('className', className);
                    this.prompt({
                        folderId:folderId,
                        className: className
                    });
                });
            },

            /**
             * запрос шаблона для отображения блока
             */
            prompt: function (params) {
                var _this = this;
                var options = {
                    action: "show",
                    data: {
                        folderId: params.folderId,
                        className: params.className
                    }
                };

                var callback = function (result) {
                    _this.set('renderTemplate', result.html);
                    _this.set('className', result.className);
                    _this.trigger('render');
                };

                return this.callServerAction(options, callback);
            },

            /**
             * импорт данных
             */
            importData: function (data) {
                this.uploadFile(data, this.startImport);
            },

            /**
             * загрузка файла
             * @param data      -   данные для загрузки
             * @param callback  -   функция которая будет вызвана по завершению загрузки
             */
            uploadFile: function (data, callback) {
                var _this = this;
                console.log('Start uploading file: ' + data.file.name);
                var xhr = new XMLHttpRequest();
                var url = misc.getContextPath() + "/upload";
                xhr.onreadystatechange = function (e) {
                    if (4 == this.readyState) {
                        console.log('File uploaded: ' + e.target.responseText);
                        callback.apply(_this, [data]);
                    }
                };
                xhr.open('post', url, true);
                var fd = new FormData;
                fd.append(data.file.name,data.file);
                xhr.send(fd);
            },

            /**
             * начало импорта данных (вызывается после завершении загрузки файла с данными)
             */
            startImport: function (data) {
                var _this = this;
                var options = {
                    action: "import",
                    data: {
                        filename: data.file.name,
                        className: _this.get("className"),
                        keyAttrListStr: data.primaryKeyAttrs
                    }
                };

                var callback = function (result) {
                    _this.showImportComplete(result);
                };

                return this.callServerAction(options, callback);
            },

            /**
             * отображение результатов импорта, вызывается по окончанию импорта данных
             */
            showImportComplete: function(result){
                var dialog = new Message({
                    title: "Импорт успешно завершен",
                    message: "Создано объектов " + result.importCreateCount + ", обновлено " + result.importUpdateCount,
                    type: 'infoMessage'
                });
                dialog.show();
            },

            /**
             * запрос на скачивание XLS-шаблона для импорт данных
             *
             * @param fillData  -   заполнять ли шаблон данными
             */
            downloadTemplate: function(fillData){
                var _this = this;
                var options = {
                    action: "download",
                    data: {
                        className: _this.get("className"),
                        fillData: fillData
                    }
                };

                var callback = function (result) {
                    window.location.href = result.url;
                };

                return this.callServerAction(options, callback);
            }
        });
        return model;
    });
