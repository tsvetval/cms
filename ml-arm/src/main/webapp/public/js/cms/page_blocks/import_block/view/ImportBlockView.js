/**
 *  представление для блока импорта данных
 *
 */
define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup', 'cms/page_blocks/DialogPageBlock', 'moment'],
    function (log, misc, backbone, PageBlockView, markup, Message) {
        var view = PageBlockView.extend({

            /**
             * инициализация представления
             */
            initialize:function () {
                console.log("initialize BlockView");
                this.listenTo(this.model, 'render', this.render);
            },

            /**
             * отображение представления
             */
            render:function () {
                var _this = this;
                this.$el.html(this.model.get('renderTemplate'));
                markup.attachActions(this.$el, {
                    //обработчик клика по кнопке "Импортировать"
                    importClick: function () {
                        var primaryKeyAttrs = [];
                        $.each(_this.$el.find('input[type=checkbox]'),
                            function(index, elem){
                               var $elem = $(elem);
                               if($elem.is(':checked')){
                                    primaryKeyAttrs.push($elem.attr('id'));
                               }
                            });
                        var importFile = _this.$el.find('#importFile')[0].files[0];
                        var data = {
                            primaryKeyAttrs: JSON.stringify(primaryKeyAttrs),
                            file: importFile
                        };
                        _this.model.importData(data);
                    },
                    //обработчик клика по кнопке "Скачать шаблон"
                    downloadClick: function(){
                        var fillData = $('#fillData').is(":checked");
                        _this.model.downloadTemplate(fillData);
                    }
                });

            }

        });
        return view;
    });
