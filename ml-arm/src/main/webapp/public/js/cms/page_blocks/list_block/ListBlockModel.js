/**
 *  Базовый модуль модели блока спика объектов
 *
 */
define(
    ['log', 'misc', 'backbone', 'underscore',
        'cms/model/PageBlockModel',
        'cms/events/NotifyPageBlocksEventObject',
        'console_const'],
    function (log, misc, backbone, _,
              PageBlockModel,
              NotifyEventObject,
              console_const) {
        var model = PageBlockModel.extend({
            defaults: {
                // id папки для отображения
                folderId: undefined,
                // имя класса объектов для отображения
                className: undefined,

                // метаданные текущей папки
                currentFolder: undefined,
                // URL для отображения в iFrame
                url: undefined,

                // список вложенных папок
                folders: undefined,
                // список вложенных объектов
                objectList: undefined,

                // номер текущей страницы списка для отображения
                currentPage: undefined,
                // число объектов для отображения на странице
                objectsPerPage: undefined,
                // общее число объектов в списке
                recordsCount: undefined,
                // атрибут для сортировки
                orderAttr: undefined,
                // тип сортировки
                orderType: undefined,
                // id выбраных объектов
                selectedIds: new Array(),
                // Показывать ли кнопку Создать
                showCreate:undefined,
                // Показывать ли кнопку Удалить
                showDelete:undefined,
                // Список классов какие объекты создавать
                createdClasses:undefined,
                 //Данные классификатора
                classifierId: undefined,
                classifierValue: undefined,

                pageTitle: 'Просмотр папки <%=folderTitle%>'
            },

            initialize: function () {
                log.debug("initialize ListBlockModel");
                var _this = this;

                this.listenTo(this, 'restorePage', function (params) {
                    log.debug("ObjectListBlockModel start restorePage");
                    _this.set('folderId', params.folderId);
                    _this.set('classifierData', params.data);
                    _this.set('className', params.className);
                    _this._reset();
                    _this._update();
                });

                this.listenTo(this, 'refreshPage', function () {
                    log.debug("ObjectListBlockModel start refreshPage");
                    _this._update();
                });

                this.listenTo(this, 'openFolder', function (params) {
                    log.debug("ObjectListBlockModel start openFolder");
                    if (params.folderId) {
                        _this.set('folderId', params.folderId);
                        _this.set('classifierId', params.classifierId);
                        _this.set('classifierValue', params.value);
                        _this._reset();
                        _this._update();
                    }
                });

                this.listenTo(this, 'search', function (params) {
                    var type = misc.option(params, "type", "Тип фильтрации (простой или расширенный)");
                    var criteria = misc.option(params, "criteria", "Критерий поиска");
                    var searchField = misc.option(params, "searchField", "Поле для поиска", "");
                    var data = {
                        useFilter: type
                    };
                    if (type == 'simple') {
                        data.simpleFilter = criteria;
                        data.searchField = searchField;
                    } else {
                        data.extendedFilterSettings = criteria;
                    }
                    _this.set('search', data);
                    _this.set('currentPage', 1);
                    _this._update();
                });

                this.listenTo(this,'getSelectedIds',function(param){
                    var data = {
                        className:_this.get("className"),
                        selectedIds: _this.get("selectedIds")
                    };
                    param.callback(data);
                });
            },

            /**
             *  Сброс параметров модели
             *
             * @private
             */
            _reset: function () {
                this.unset("recordsCount");
                this.unset("currentPage");
                this.unset("objectsPerPage");
                this.unset("orderAttr");
                this.unset("orderType");
                this.unset("search");
                this.set('selectedIds', []);
            },

            /**
             * Обновление данных модели
             *
             * @private
             */
            _update: function () {
                var _this = this;
                $.when(_this._loadListData()).then(
                    function (result) {
                        if (result.CurrentFolder) {
                            var title = _.template(_this.get("pageTitle"), {data : result, folderTitle: result.CurrentFolder.title});
                            _this.updatePageTitle(title);
                            _this.set("currentFolder", result.CurrentFolder);
                        } else {
                            _this.unset("currentFolder");
                        }

                        if (result.CurrentFolder && result.CurrentFolder.url) {
                            _this.set("url", result.CurrentFolder.url);
                        } else {
                            _this.unset("url");
                        }

                        if (result.ObjectClassName) {
                            _this.set("className", result.ObjectClassName);
                        } else {
                            _this.unset("className");
                        }
                        if (result.ObjectListData) {
                            _this.set("objectList", result.ObjectListData);
                        } else {
                            _this.unset("objectList");
                        }
                        if (result.folders) {
                            _this.set("folders", result.folders);
                        } else {
                            _this.unset("folders");
                        }
                        if (result.RecordsCount) {
                            _this.set("recordsCount", result.RecordsCount);
                        } else {
                            _this.unset("recordsCount");
                        }
                        if (result.createdClasses) {
                            _this.set("createdClasses", result.createdClasses);
                        } else {
                            _this.unset("createdClasses");
                        }
                        _this.set("showCreate",result.canCreate);
                        _this.set("showDelete",result.canDelete);
                        _this.set('ClassDescription', result.ClassDescription);
                        _this._updatePageTitle();
                        _this.trigger('render');
                    }
                );
            },

            /**
             * Обновление заголовка страницы
             *
             * @private
             */
            _updatePageTitle: function () {
                var currentFolder = this.get("currentFolder");
                var className = this.get("className");
                var title = undefined;
                if (!currentFolder && !className) {
                    title = "Просмотр корневой папки";
                } else if (currentFolder) {
                    title = "Просмотр папки " + currentFolder.text;
                } else if (className) {
                    title = "Просмотр списка объектов " + className;
                }
                this.set("pageTitle", title);
                this.changePageParams({
                    pageTitle: title
                });
            },


            /**
             * Получение списка объектов в соответствии с параметрами модели
             *
             * @private
             */
            _loadListData: function () {
                var options = {
                    action: 'getObjectData',
                    data: {
                        folderId: this.get('folderId'),
                        className: this.get('className'),
                        currentPage: this.get('currentPage'),
                        classifierId: this.get('classifierId'),
                        classifierValue: this.get('classifierValue'),
                        objectsPerPage: this.get('objectsPerPage'),
                        orderAttr: this.get('orderAttr'),
                        orderType: this.get('orderType'),
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    }
                };
                if (this.get("search")) {
                    $.extend(options.data, this.get("search"));
                }
                return this.callServerAction(options);
            },

            /**
             * Открытие папки
             *
             * @param folderId  -   id папки для открытия
             */
            openFolder: function (folderId) {
                var _this = this;
                this.unset('currentPage');
                this.set('folderId', folderId);
                this.set('className', undefined);
                this.notifyPageBlocks(new NotifyEventObject('openSubFolder', {folderId: folderId}));
                this.notifyPageBlocks(new NotifyEventObject('activateFolder', {folderId: folderId}));
                _this._update();
            },

            /**
             * Открытие объекта
             *
             * @param objectId  -   id объекта
             * @param className -   имя (entityName) класса объекта
             */
            openObject: function (objectId, className) {
                this.openPage(
                    OBJECT_VIEW_PAGE,
                    'Просмотр объекта ...',
                    {
                        objectId: objectId,
                        className: className
                    }
                );
            },

            /**
             * Добавление списка идентификаторов к списку связанных объектов
             * @param ids
             */
            addSelectedIds : function(ids){
                this.set('selectedIds', _.union(this.get('selectedIds'), ids));
            },

            /**
             * Удаление списка идентификаторов из списка связанных объектов
             * @param ids
             */
            removeSelectedIds : function(ids){
                this.set('selectedIds', _.difference(this.get('selectedIds'), ids));
            },
            /**
             * Создание объекта
             *
             * @param className -   имя (entityName) класса объекта
             */
            createObject: function (className) {
                this.openPage(OBJECT_CREATE_PAGE, 'Создание объекта ...', {
                    className: className
                });
            },
            /**
             * Удаление выбранных объектов
             */
            deleteObjects:function(){
                var _this = this;
                var objectIds = this.get("selectedIds");
                var className = this.get('className');
                var options = {
                    action: 'deleteObjects',
                    data: {
                        objectIds: JSON.stringify(objectIds),
                        className: className,
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    }
                };

                log.debug('Call server to delete Object with id = ' + objectIds + '@' + className);
                return this.callServerAction(options).then(function(){
                    log.debug('Receive response from server after delete Object with id = ' + objectIds + '@' + className);
                    log.debug('Trigger event to navigate previous BreadCrumb');
                    _this.set("selectedIds", []);
                    var opener = _this.get("pageModel").get("page_opener");
                    _this.notifyPageBlocks(new NotifyEventObject(
                        'refreshPage',
                        {},
                        opener
                    ));
                    _this.closePage();
                });
            },
            /**
             * Смена текущей страницы/количества объектов на странице
             *
             * @param page              -   номер текущеей страницы
             * @param objectsPerPage    -   число объектов для отображения на странице
             */
            changePage: function (page, objectsPerPage) {
                this.set("currentPage", page);
                this.set("objectsPerPage", objectsPerPage);
                this._update();
            },

            /**
             * Смена сортировки списка объектов
             *
             * @param orderAttr     -   атрибут для сортировки
             * @param orderType     -   тип сортировки (asc/desc)
             */
            changeSort: function (orderAttr, orderType) {
                this.set("orderAttr", orderAttr);
                this.set("orderType", orderType);
                this._update();
            }

        });

        return model;
    });
