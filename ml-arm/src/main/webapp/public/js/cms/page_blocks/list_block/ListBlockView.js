/**
 *  Базовый модуль представления блока спика объектов
 *
 */
define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'cms/page_blocks/DialogPageBlock'],
    function (log, misc, backbone, PageBlockView, Message) {
        var view = PageBlockView.extend({
            initialize: function () {
                console.log("initialize ListBlockView");
                this.viewMode = undefined;
            }
        });

        return view;
    });
