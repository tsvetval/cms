/**
 * Модель блока списка объектов
 *
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/list_block/ListBlockModel',
        'cms/events/NotifyPageBlocksEventObject'
    ],
    function (log, misc, backbone, ListBlockModel, NotifyEventObject) {
        var model = ListBlockModel.extend({
            defaults: {
                className: undefined,
                folderId: undefined,
                objectId: undefined,
                pageTitle: 'undefined'
            },

            /**
             * инициализация модели
             *
             */
            initialize: function () {
                var _this = this;
                log.debug('initialize ClassObjectListBlockModel');
                model.__super__.initialize.call(_this)
                var title = 'Просмотр папки <%=folderTitle%>';
                _this.set("pageTitle", title);
                this.off('restorePage');  //убираем не нужный слушатель
                this.listenTo(this, 'restorePage', function (params) { // добавляем нужный
                     _this.getClassName(params.objectId,
                     function(){
                        log.debug("ClassObjectListBlockModel start restorePage");
                        _this._reset();
                        _this._update();
                     });
                });
            },
            getClassName: function(objectId, callback){
                var _this = this;
                var options = {
                    action: 'getClass',
                    data: {
                        objectId: objectId,
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    }
                };
                _this.callServerAction(options, function(result){
                    _this.set("className",result.class);
                    callback.apply();
                })
            }
        });
        return model;
    });
