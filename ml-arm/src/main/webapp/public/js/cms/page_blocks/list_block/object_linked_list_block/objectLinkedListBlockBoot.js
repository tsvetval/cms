/**
 * Блок просмотра связанных объектов (REST)
 * Контроллер: ru.peak.ml.web.block.controller.impl.list.ObjectLinkedListBlockController
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/list_block/object_linked_List_block/model/ObjectLinkedListBlockModel',
        'cms/page_blocks/list_block/object_linked_list_block/view/ObjectLinkedListBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
