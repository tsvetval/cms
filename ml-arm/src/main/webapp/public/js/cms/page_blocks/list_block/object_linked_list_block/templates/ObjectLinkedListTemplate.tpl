<div class="container control-panel col-lg-24" id="view_buttons_container">
    <h4 class="current-folder-title col-lg-10">Просмотр объектов <%=listModel.get('description')%></h4>

    <div class="pull-right control-buttons">
        <div>
            <div>
                <button class="btn btn-primary save-button">
                    <span class="glyphicon glyphicon-ok"></span> Сохранить
                </button>
                <button class="btn btn-primary edit-button">
                    <span class="glyphicon glyphicon-edit"></span> Редактировать
                </button>
                <button class="btn btn-primary cancel-button">
                    <span class="glyphicon glyphicon-repeat"></span> Отмена
                </button>
            </div>
        </div>
    </div>
</div>
<div class="object-list-container">
</div>
