/**
 * Представление для блока выбора связанных объектов
 *
 */
define(
    ['log', 'misc', 'backbone', 'underscore',
        'boot_table_ru',
        'cms/page_blocks/list_block/object_list_block/view/ObjectListBlockView',
        'cms/page_blocks/DialogPageBlock',
        'text!cms/page_blocks/list_block/object_linked_list_block/templates/ObjectLinkedListTemplate.tpl',
        'text!cms/page_blocks/list_block/object_list_block/templates/ObjectTableTemplate.tpl',
        'text!cms/page_blocks/list_block/object_linked_list_block/templates/ObjectLinkedListToolbarTemplate.tpl',
        'text!cms/page_blocks/list_block/object_list_block/templates/FolderItemTemplate.tpl',
        'console_const'
    ],
    function (log, misc, backbone, _,
              boot_table,
              ObjectListBlockView,
              Message,
              ObjectLinkedListTemplate,
              ObjectTableTemplate,
              ObjectLinkedListToolbarTemplate,
              FolderItemTemplate,
              console_const) {
        var view = ObjectListBlockView.extend({

            events: {
                "click .save-button": "saveClick",
                "click .edit-button": "editClick",
                "click .cancel-button": "cancelClick",
                "click .show-full-text": "showFullTextClick",
                "click .hide-full-text": "hideFullTextClick",
                "click .file-download-link": "downloadFile",

                "click .deleteLinkedObject": "deleteLinkedObject",
                "click .editLinkedObject": "editLinkedObject",
                "click .createClick": "createClick",
                "click .selectLinkedObject": "selectLinkedObject",

                "click .moveUp": "moveUpClick",
                "click .moveDown": "moveDownClick"
            },

            /**
             * инициализация представления
             */
            initialize: function () {
                log.debug("initialize ObjectListBlockView");
                this.listenTo(this.model, 'render', this.render);
                this.listenTo(this.model, 'change:objectList', this.renderAttrValue);
            },

            /**
             * отрисовка представления
             * @returns {boolean}
             */
            render: function () {
                var _this = this;
                this.$el.html(_.template(ObjectLinkedListTemplate, {listModel: this.model}));

                var selectMode = this.model.get('selectMode');
                if (selectMode == 'noselect') {
                    this.$el.find(".save-button").hide();
                } else {
                    this.$el.find(".edit-button").hide();
                }

                var $objectListContainer = this.$el.find('.object-list-container');
                var objectList = this.model.get('objectList');
                this.createObjectListTable($objectListContainer, objectList);
                return true;
            },


            renderAttrValue: function () {
                var _this = this;
                if (this.model.get('objectList') && !this.model.get('objectList').attrList) {
                    this.model.callServerAction({
                        action: "getRefAttrValues",
                        data: {
                            objectId: _this.model.get('objectId'),
                            className: _this.model.get('className'),
                            attrId: this.model.get('refAttrId'),
                            idList: JSON.stringify(_this.model.get('objectList').idList)
                        }
                    }).then(function (result) {
                        _this.model.set('objectList', result.result);
                        _this.render();
                    });
                }
            },
















            /**
             * Создание таблицы связанных объектов
             * @param $container    -   элемент-контейнер (jQuery-объект)
             * @param data          -   данные для отображения
             */
            createObjectListTable: function ($container, data) {
                var _this = this;

                var selectMode = this.model.get('selectMode');
                if (selectMode == 'noselect') {
                    this.$toolbar = undefined;
                } else {
                    this.$toolbar = $(_.template(ObjectLinkedListToolbarTemplate, {}));
                    if (!this.model.get("ordered")) {
                        this.$toolbar.find(".moveUp, .moveDown").hide();
                    }
                    $container.append(this.$toolbar);
                }

                this.$table = $(_.template(ObjectTableTemplate, {}));
                $container.append(this.$table);

                //Формируем колонки таблицы
                var columns = [];
                if (this.model.get('selectMode') != 'noselect') {
                    columns.push({
                        field: 'state',
                        checkbox: this.model.get('selectMode') == 'multiselect',
                        radio: this.model.get('selectMode') != 'multiselect'
                    });
                }

                columns.push({
                    field: 'objectId',
                    visible: false
                });

                data.attrList.forEach(function (columnAttr) {
                    var formatter = undefined;
                    var sortable = true;
                    if (columnAttr.fieldType === "ONE_TO_MANY" || columnAttr.fieldType === "MANY_TO_MANY") {
                        formatter = _this.formatterOneToMany;
                        sortable = false;
                    } else if (columnAttr.fieldType === "MANY_TO_ONE") {
                        formatter = _this.formatterManyToOne;
                        sortable = false;
                    } else if (columnAttr.fieldType === "BOOLEAN") {
                        formatter = _this.formatterBoolean;
                        sortable = true;
                    } else if (columnAttr.fieldType === "ENUM") {
                        formatter = _this.formatterEnum;
                        sortable = true;
                    } else if (columnAttr.fieldType === "DATE") {
                        formatter = _this.formatterDateWithOptions({dateFormat : columnAttr.fieldFormat});;
                        sortable = true;
                    } else if (columnAttr.fieldType === "FILE") {
                        formatter = _this.formatterFile;
                        sortable = true;
                    } else if (columnAttr.fieldType === "LONG_LINK") {
                        sortable = false;
                    }

                    var column = {
                        field: columnAttr.entityFieldName,
                        title: columnAttr.description,
                        align: 'left',
                        valign: 'top',
                        sortable: sortable,
                        formatter: formatter
                    };
                    columns.push(column);
                });


                //Формируем значения колонок таблицы
                var data = this.createTableDataFromValue();
                //Получаем количество объектов
                var totalRows = this.model.get('recordsCount');
                //Текущая страница
                var pageNumber = this.model.get('currentPage');
                //Количество объектов на странице
                var pageSize = this.model.get('objectsPerPage');
                //Наименование столбца для сортировки
                var sortName = this.model.get('orderAttr');
                //Тип сортировки
                var sortOrder = this.model.get('orderType');

                this.$table.bootstrapTable({
                    toolbar: _this.$toolbar,
                    idField: 'objectId',
                    data: data,
                    cache: false,
                    striped: true,
                    pagination: true,
                    pageNumber: pageNumber,
                    pageSize: pageSize,
                    pageList: [5, 10, 20, 50],
                    totalRows: totalRows,
                    sortName: sortName,
                    sortOrder: sortOrder,
                    showColumns: true,
                    minimumCountColumns: 1,
                    search: true,
                    toolbarAlign: "right",
                    clickToSelect: true,
                    sidePagination: "server",
                    columns: columns
                }).on('page-change.bs.table', function (e, number, size) {
                    console.log('Event: page-change.bs.table, data: ' + number + ', ' + size);
                    _this.model.changePage(number, size);
                }).on('sort.bs.table', function (e, name, order) {
                    console.log('Event: sort.bs.table, data: ' + name + ', ' + order);
                    _this.model.changeSort(name, order);
                }).on('all.bs.table', function (name, args) {
                    _this.updateButtonState();
                });
                _this.updateButtonState();
            },

            /**
             * Обновление состояния кнопок для работы со связанными объектами
             */
            updateButtonState: function () {
                if (this.model.get('selectMode') == 'noselect') {
                    return;
                }

                var selects = this.$table.bootstrapTable('getSelections');
                if (selects.length > 0) {
                    this.$toolbar.find(".deleteLinkedObject")
                        .removeClass("inactive-button")
                        .addClass("active-button");
                } else {
                    this.$toolbar.find(".deleteLinkedObject")
                        .removeClass("active-button")
                        .addClass("inactive-button");
                }

                if (selects.length == 1) {
                    this.$toolbar.find(".moveUp, .moveDown, .editLinkedObject")
                        .removeClass("inactive-button")
                        .addClass("active-button");
                } else {
                    this.$toolbar.find(".moveUp, .moveDown, .editLinkedObject")
                        .removeClass("active-button")
                        .addClass("inactive-button");
                }
            },


            /**
             * отмена
             */
            cancelClick: function () {
                this.model.closePage();
            },

            /**
             * преход в режим редактирования
             */
            editClick: function () {
                this.model.set('selectMode', 'multiselect');
                this.render();
            },

            /**
             * Подвинуть выделенный связанный объект вверх в упорядоченном списке
             */
            moveUpClick: function () {
                var selects = this.$table.bootstrapTable('getSelections');
                var ids = $.map(selects, function (row) {
                    return row.objectId;
                });
                if (ids && ids.length == 1) {
                    var list = this.model.get('objectList');
                    var pos = this.getItemPosInList(ids[0]);
                    if (pos > 0) {
                        var prevItem = list[pos - 1];
                        list[pos - 1] = list[pos];
                        list[pos] = prevItem;
                        this.render();
                    }
                }
            },

            /**
             * Подвинуть выделенный связанный объект вниз в упорядоченном списке
             */
            moveDownClick: function () {
                var selects = this.$table.bootstrapTable('getSelections');
                var ids = $.map(selects, function (row) {
                    return row.objectId;
                });
                var list = this.model.get('objectList');
                if (ids && ids.length == 1) {
                    var pos = this.getItemPosInList(ids[0]);
                    if (pos > -1 && pos < list.length - 1) {
                        var nextItem = list[pos + 1];
                        list[pos + 1] = list[pos];
                        list[pos] = nextItem;
                        this.render();
                    }
                }
            },

            /**
             * Получить положение объекта в упорядоченном списке
             */
            getItemPosInList: function (id) {
                var list = this.model.get('objectList');
                var pos = -1;
                $.each(list, function (index, item) {
                    if (item.objectId == id) {
                        pos = index;
                    }
                });
                return pos;
            },


            /**
             * Удалить связанный объект (удаление связи)
             */
            deleteLinkedObject: function () {
                var selects = this.$table.bootstrapTable('getSelections');
                var ids = $.map(selects, function (row) {
                    return row.objectId;
                });
                // Удаляем из коллекции модели все выбранные объекты
                if (ids && ids.length > 0) {
                    var value = this.model.get('objectList');
                    var newObjectList = _.reject(value.objectList, function (obj) {
                        return _.contains(ids, obj.objectId);
                    });
                    value.objectList = newObjectList;
                    this.model.set('objectList', value);
                    this.render();
                }
            },

            /**
             * Редактировать связанный объект
             */
            editLinkedObject: function () {
                var selects = this.$table.bootstrapTable('getSelections');
                var ids = $.map(selects, function (row) {
                    return row.objectId;
                });
                if (ids && ids.length == 1) {
                    var params = {
                        refAttrId: this.model.get('refAttrId'),
                        objectId: ids[0],
                        className: this.model.get('className')
                    };
                    this.model.openPage(
                        OBJECT_EDIT_PAGE,
                        'Редактирование объекта ...',
                        params
                    );
                }
            },

            /**
             * Создать связанный объект
             */
            createClick: function () {
                var params = {
                    refAttrId: this.model.get('refAttrId'),
                    refObjectId: this.model.get('objectId')
                };
                this.model.openPage(
                    OBJECT_CREATE_PAGE,
                    'Создание объекта ...',
                    params
                );
            },

            /**
             * Выбрать связанные объекты
             */
            selectLinkedObject: function () {
                var selects = this.$table.bootstrapTable('getData');
                var ids = $.map(selects, function (row) {
                    return row.objectId;
                });
                var params = {
                    objectId: this.model.get('objectId'),
                    refAttrId: this.model.get('refAttrId'),
                    className: this.model.get('className'),
                    selectMode: "multiselect",
                    selectedList: ids
                };
                this.model.openPage(
                    OBJECT_SELECT_PAGE,
                    'Выбор списка объектов ...',
                    params
                );
            },

            saveClick: function () {
                this.model.saveObject();
            }
        });

        return view;
    });
