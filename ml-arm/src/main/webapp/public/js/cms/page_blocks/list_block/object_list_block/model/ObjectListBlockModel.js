/**
 * Модель блока списка объектов
 *
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/list_block/ListBlockModel',
        'cms/events/NotifyPageBlocksEventObject'
    ],
    function (log, misc, backbone, ListBlockModel, NotifyEventObject) {
        var model = ListBlockModel.extend({
            defaults: {
                className: undefined,
                folderId: undefined,
                pageTitle: 'undefined'
            },

            /**
             * инициализация модели
             *
             */
            initialize: function () {
                var _this = this;
                log.debug('initialize ObjectListBlockModel');
                model.__super__.initialize.call(this)
                var title = 'Просмотр папки <%=folderTitle%>';

                this.set("pageTitle", title);
                _this.listenTo(_this,"changeObject",_this.onObjectChange)
            },
            onObjectChange: function(objectData){
                var _this = this;
                var className = _this.get("className");
                if(className = objectData.className){
                    var object = _this.get("objectList").objectList.filter(function(data){ return data.objectId == objectData.objectId});
                    if(object.length>0){
                        _this.trigger("refreshPage");
                    }
                }
            }
        });
        return model;
    });
