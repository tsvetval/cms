/**
 * Блок просмотра списка объектов
 * Контроллер: ru.peak.ml.web.block.controller.impl.list.ObjectListBlockController
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/list_block/object_list_block/model/ObjectListBlockModel',
        'cms/page_blocks/list_block/object_list_block/view/ObjectListBlockViewDesign'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
