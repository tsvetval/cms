<style>
    .item {
        margin: 20px;
        display: inline-block;
        vertical-align: top;
        width: 100px;
        cursor: pointer;
    }

    .item .icon {
        text-align: center;
        font-size: 36pt;
        color: #3071a9;
    }

    .item .title {
        text-align: center;
        max-width: 100px;
    }
</style>

<div class="folder-list-container"></div>
