<style>
    .current-folder-title {
        text-indent: 20px;
    }
</style>

<% if (typeof(listModel.get('currentFolder')) != "undefined") { %>
<h4 class="current-folder-title">Просмотр папки &laquo;<%=listModel.get('currentFolder').text%>&raquo;</h4>
<% } else { %>
<h4 class="current-folder-title">Просмотр корневой папки</h4>
<% } %>
<div class="folder-list-container">
</div>
<div class="object-list-container">
</div>
