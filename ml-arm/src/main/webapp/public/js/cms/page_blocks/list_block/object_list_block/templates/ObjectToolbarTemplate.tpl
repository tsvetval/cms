<div class="object-list-toolbar">
<% if (showCreate) { %>
    <% if (createdClasses) { %>
    <div class="btn-group">
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
          Создать
          <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
       <% _.each(createdClasses, function(cls) { %> <li> <a class="create-other-object" className="<%= cls.className %>" href="#"><%= cls.title %></a></li> <% });  %>
       </ul>
    </div>
    <% } else { %>
        <button class="btn btn-primary create-object-button">
            <span class="glyphicon glyphicon-plus"></span> Создать
        </button>
    <%}}%>
<% if (showDelete) { %>
    <button class="btn btn-primary delete-object-button">
            <span class="glyphicon glyphicon-minus"></span> Удалить
    </button>
<%}%>
</div>
