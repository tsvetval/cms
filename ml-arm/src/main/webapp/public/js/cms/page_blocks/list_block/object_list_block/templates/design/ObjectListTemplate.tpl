<% if (typeof(listModel.get('currentFolder')) != "undefined") { %>
<div class="ml-main__content-title">
    <div class="ml-main__content-title-text"><%=listModel.get('currentFolder').text%></div>
    <%if (listModel.get('folders')){%>
        <div class="ml-main__content-title-items"><%=listModel.get('folders').length%></div>
    <%}%>
</div>

<!--
<h4 class="current-folder-title">Просмотр папки &laquo;<%=listModel.get('currentFolder').text%>&raquo;</h4>
-->
<% } else { %>
    <div class="ml-main__content-title-text">Просмотр корневой папки</div>
<% } %>
<div class="ml-main__content-delimiter"></div>
<div class="folder-list-container">

</div>
<div class="object-list-container">
</div>
<div class="ml-main__content-delimiter"></div>
