/**
 * Представление блока списка объектов
 *
 */
define(
    ['log', 'misc', 'backbone', 'underscore', 'moment',
        'boot_table_ru',
        'cms/page_blocks/list_block/ListBlockView',
        'markup',
        'cms/page_blocks/DialogPageBlock',
        'text!cms/page_blocks/list_block/object_list_block/templates/design/ObjectListTemplate.tpl',
        'text!cms/page_blocks/list_block/object_list_block/templates/FolderContainerTemplate.tpl',
        'text!cms/page_blocks/list_block/object_list_block/templates/ObjectTableTemplate.tpl',
        'text!cms/page_blocks/list_block/object_list_block/templates/ObjectToolbarTemplate.tpl',
        'text!cms/page_blocks/list_block/object_list_block/templates/design/FolderItemTemplate.tpl',
        'text!cms/page_blocks/list_block/object_list_block/templates/IFrameTemplate.tpl',
        'table_formatter_mixin'
    ],
    function (log, misc, backbone, _, moment,
              boot_table,
              ListBlockView,
              markup,
              Message,
              ObjectListTemplate,
              FolderContainerTemplate,
              ObjectTableTemplate,
              ObjectToolbarTemplate,
              FolderItemTemplate,
              IFrameTemplate,
              table_formatter_mixin) {

        var view = ListBlockView.extend({

            events: {
                "click .open-object-button": "openObjectClick",
                "click .create-object-button": "createObjectClick",
                "click .delete-object-button": "deleteObjectClick",
                "click .show-full-text": "showFullTextClick",
                "click .hide-full-text": "hideFullTextClick",
                "click .file-download-link": "downloadFile",
                "click .select-row": "selectRow",
                "click .create-other-object": "createOtherObjectClick"
            },

            /**
             * открытие объекта из списка
             *
             */
            openObjectClick: function () {
                var mlClass = this.model.get('className');
                var objectId = $(event.srcElement).attr('objectId');
                this.model.get('objectList').objectList.forEach(function(object){
                        if(objectId == object.objectId){
                            mlClass = object.mlClass;
                        }
                    }
                );
                this.model.openObject(objectId, mlClass);
            },
            /**
             * Удаление выбранного списка объектов
             * */
            deleteObjectClick:function(){
                var _this = this;
                if(_this.model.get("selectedIds").length > 0){
                    log.debug('Click delete event catched');
                    var dialogConfirm = new Message({
                        title: "Подтверждение удаления",
                        message: "Вы действительно хотите удалить "+_this.model.get("selectedIds").length+" объектов?",
                        type: 'dialogMessage'
                    });
                    dialogConfirm.show({
                        okAction: function () {
                            _this.model.deleteObjects();
                        }
                    });
                }
            },
            /**
             * создание объекта
             *
             */
            createObjectClick: function () {
                this.model.createObject(this.model.get('className'));
            },

            /**
             * создание объекта класса потомка
             *
             */
            createOtherObjectClick:function (event) {
                this.model.createObject(event.target.getAttribute('className'));
            },

            /**
             * развернуть список связанных объектов
             *
             */
            showFullTextClick: function () {
                $(event.srcElement).hide();
                $(event.srcElement).next().show();
                $(event.srcElement).prevAll(".ellipsis").hide();
                $(event.srcElement).prevAll(".one-many-text-container").removeClass("collapsed");
            },

            /**
             * свернуть список связанных объектов
             *
             */
            hideFullTextClick: function () {
                $(event.srcElement).hide();
                $(event.srcElement).prev().show();
                $(event.srcElement).prevAll(".ellipsis").show();
                $(event.srcElement).prevAll(".one-many-text-container").addClass("collapsed");
            },

            selectRow:function(){
                if(event.toElement.checked){
                    this.model.set("selectedIds",_.union(this.model.get("selectedIds"), $(event.srcElement).attr('objectId')));
                }else{
                    this.model.set("selectedIds", _.difference(this.model.get("selectedIds"), $(event.srcElement).attr('objectId')));
                }

            },

            /**
             * инициализация представления
             *
             */
            initialize: function () {
                log.debug("initialize ObjectListBlockViewDesign");
                this.listenTo(this.model, 'render', this.render);
            },

            /**
             * отрисовка представления
             *
             */
            render: function () {
                var _this = this;
                // TODO дял чего?
                if (this.model.get('url')) {
                    this.$el.html(_.template(IFrameTemplate, {url: this.model.get('url')}));
                    return true;
                }
                // Добавляем шаблон для заголовка и разметки блока
                this.$el.html(_.template(ObjectListTemplate, {listModel: this.model}));
                var $folderListContainer = this.$el.find('.folder-list-container');
                var $objectListContainer = this.$el.find('.object-list-container');
                //$folderListContainer.html(_.template(FolderContainerTemplate, {listModel: this.model}));

                var currentFolder = this.model.get('currentFolder');
/*                if (currentFolder) {
                    var parentFolderId = currentFolder.parent;
                    if (parentFolderId) {
                        var folder = {
                            id: parentFolderId,
                            title: "Вверх"
                        };
                        this.createParentFolderButton($folderListContainer, folder);
                    } else {
                        var root = {
                            id: undefined,
                            title: "Вверх"
                        };
                        this.createParentFolderButton($folderListContainer, root);
                    }
                }*/

                var folderList = this.model.get('folders');
                if (folderList) {
                    this.createFolderListView($folderListContainer, folderList);
                }

                var className = this.model.get('className');
                if (className) {
                    var objectList = this.model.get('objectList');
                    this.createObjectListTable($objectListContainer, objectList);
                }

                return true;
            },

            /**
             * создание кнопки "Вверх"
             *
             */
            createParentFolderButton: function ($container, parentFolder) {
                var _this = this;
                var $items = $container.find('.folder-list-container');
                var $folder = $(_.template(FolderItemTemplate, {folder: parentFolder}));
                var $icon = $("<span></span>");
                $icon.addClass("glyphicon glyphicon-circle-arrow-up");
                $folder.find(".icon").append($icon);
                $folder.on('click', function () {
                    _this.model.openFolder(parentFolder.id);
                });
                $items.append($folder);
            },

            /**
             * отображение вложенных папок
             *
             */
            createFolderListView: function ($container, folderList) {
                var _this = this;
                //var $items = $container.find('.folder-list-container');
                folderList.forEach(function (folder) {
                    var $folder = $(_.template(FolderItemTemplate, {folder: folder}));
                    //var $icon = $("<div></div>");

                    if (folder.icon) {
                        //TODO
                        $folder.addClass("ml-main__common-folder");
                        //$folder.addClass(folder.icon);
                        //$icon.addClass("iconURL")   //todo сделать отображение иконок по url
                    } else {
                        $folder.addClass("ml-main__common-folder");
                    }
                    //$folder.find(".icon").append($icon);
                    $folder.on('click', function () {
                        _this.model.openFolder({folderId: folder.id, pageBlocks: folder.pageBlocks});
                    });
                    $container.append($folder);
                });
            },

            /**
             * запрос URL иконки для папки и её отображене
             *
             */
            setIcon: function ($elem, filename) {
                var options = {
                    action: "getIconURL",
                    data: {
                        filename: filename
                    }
                };

                var callback = function (result) {
                    $elem.css("background-image", "url(" + result.iconURL + ")");
                };

                return this.model.callServerAction(options, callback);
            },


            /**
             * Создание таблицы связанных объектов
             * @param $container    -   элемент-контейнер (jQuery-объект)
             * @param data          -   данные для отображения
             */
            createObjectListTable: function ($container, data) {
                var _this = this;
                //this.$toolbar = $(_.template(ObjectToolbarTemplate, {createdClasses:_this.model.get('createdClasses'),showCreate:_this.model.get("showCreate"),showDelete:_this.model.get("showDelete")}));
                //$container.append(this.$toolbar);

                this.$tableContainer = $(_.template(ObjectTableTemplate, {}));
                this.$table = this.$tableContainer.find('table');
                $container.append(this.$tableContainer);

                //Формируем колонки таблицы
                var columns = [];
                columns.push({
                    field: 'state',
                    checkbox: true,
                    formatter: function (value, row, index) {
                        return _this.stateFormatter(value, row, index)
                    }
                });
                columns.push({
                    field: 'objectId',
                    visible: true,
                    formatter: _this.formatterSelect
                });



                data.attrList.forEach(function (columnAttr) {
                    var formatter = undefined;
                    var sortable = true;
                    if (columnAttr.fieldType === "ONE_TO_MANY" || columnAttr.fieldType === "MANY_TO_MANY") {
                        formatter = _this.formatterOneToMany;
                        sortable = false;
                    } else if (columnAttr.fieldType === "MANY_TO_ONE") {
                        formatter = _this.formatterManyToOne;
                        sortable = true;
                    } else if (columnAttr.fieldType === "BOOLEAN") {
                        formatter = _this.formatterBoolean;
                        sortable = true;
                    } else if (columnAttr.fieldType === "ENUM") {
                        formatter = _this.formatterEnum;
                        sortable = true;
                    } else if (columnAttr.fieldType === "DATE") {
                        formatter = _this.formatterDateWithOptions({dateFormat : columnAttr.fieldFormat});
                        sortable = true;
                    } else if (columnAttr.fieldType === "FILE") {
                        formatter = _this.formatterFile;
                        sortable = true;
                    } else if (columnAttr.fieldType === "LONG_LINK") {
                        sortable = false;
                    } else if (columnAttr.fieldType === "HETERO_LINK") {
                        formatter = _this.formatterHeteroLink;
                        sortable = false;
                    }


                    var column = {
                        field: columnAttr.entityFieldName,
                        title: columnAttr.description,
                        align: 'left',
                        valign: 'top',
                        sortable: sortable,
                        formatter: formatter
                    };
                    columns.push(column);
                });


                //Формируем значения колонок таблицы
                var data = this.createTableDataFromValue();
                //Получаем количество объектов
                var totalRows = this.model.get('recordsCount');
                //Текущая страница
                var pageNumber = this.model.get('currentPage');
                //Количество объектов на странице
                var pageSize = this.model.get('objectsPerPage');
                //Наименование столбца для сортировки
                var sortName = this.model.get('orderAttr');
                //Тип сортировки
                var sortOrder = this.model.get('orderType');

                this.$table.bootstrapTable({
                    //toolbar: _this.$toolbar,
                    idField: 'objectId',
                    data: data,
                    cache: false,
                    striped: true,
                    pagination: false,
                    pageNumber: pageNumber,
                    pageSize: pageSize,
                    pageList: [5, 10, 20, 50],
                    totalRows: totalRows,
                    sortName: sortName,
                    sortOrder: sortOrder,
                    showColumns: false,
/*                    minimumCountColumns: 1,*/ //True to show the columns drop down list.
                    search: false,
                    clickToSelect: false,
                    sidePagination: "server",
                    columns: columns
                }).on('page-change.bs.table', function (e, number, size) {
                    console.log('Event: page-change.bs.table, data: ' + number + ', ' + size);
                    _this.model.changePage(number, size);
                }).on('sort.bs.table', function (e, name, order) {
                    console.log('Event: sort.bs.table, data: ' + name + ', ' + order);
                    _this.model.changeSort(name, order);
                }).on('check.bs.table', function (e, row) {
                    var ids = [row.objectId];
                    _this.model.addSelectedIds(ids);
                }).on('uncheck.bs.table', function (e, row) {
                    var ids = [row.objectId];
                    _this.model.removeSelectedIds(ids);
                }).on('check-all.bs.table', function (e) {
                    var selects = _this.$table.bootstrapTable('getData');
                    var ids = $.map(selects, function (row) {
                        return row.objectId;
                    });
                    _this.model.addSelectedIds(ids);
                }).on('uncheck-all.bs.table', function (e) {
                    var selects = _this.$table.bootstrapTable('getData');
                    var ids = $.map(selects, function (row) {
                        return row.objectId;
                    });
                    _this.model.removeSelectedIds(ids);
                });

                // Подключаем внешний скролл
                //$.each(this.$tableContainer.find('.scrollbar-external'), function(i, v) {
                    this.$tableContainer.find('.scrollbar-external').scrollbar({
                        "autoScrollSize": true,
                        "scrollx": this.$tableContainer.find('.external-scroll_x')
                    });
                    this.$tableContainer.find('.external-scroll_x').css('top', this.$tableContainer.find('thead').first().height() - 10);
                    //$(v).parent().siblings('.external-scroll_x.scroll-scrollx_visible').css('top', function() {
                    //    return $('.m-main__content-table_header-row', $(this).siblings('.scroll-wrapper')).height();
                    //});
                //});



            },


            /**
             * заполнение данных об объектах списка
             *
             */
            createTableDataFromValue: function () {
                var data = [];
                this.model.get('objectList').objectList.forEach(function (objectData) {
                    data.push($.extend(objectData.attrValues, {objectId: objectData.objectId}));
                });
                return data;
            },

            /**
             *  форматтер для отображения выбранных элементов (чекбокс)
             */
            stateFormatter: function (value, row, index) {
                var _objectId = row.objectId;
                return {
                    checked: (_.contains(this.model.get('selectedIds'), _objectId))
                }
            },

            /**
             * скачивание файла
             *
             */
            downloadFile: function () {
                var _this = this;
                var objectId = $(event.srcElement).attr('objectId');
                var attrName = $(event.srcElement).attr('attrName');
                this.model.callServerAction(
                    {
                        action: 'downloadFile',
                        data: {
                            objectId: objectId,
                            className: _this.model.get('className'),
                            attrName: attrName
                        }
                    },
                    function (result) {
                        window.location = result.url;
                    }
                );
            }
        });
        _.extend(ListBlockView.prototype, table_formatter_mixin);
        return view;
    });
