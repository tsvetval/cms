/**
 *  Модель блока выбора связанных объектов
 *
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/list_block/object_list_block/model/ObjectListBlockModel',
        'cms/events/NotifyPageBlocksEventObject'
    ],
    function (log, misc, backbone, ObjectListBlockModel, NotifyEventObject) {
        var model = ObjectListBlockModel.extend({
            defaults: {
                /*
                 * id объекта владельца атрибута
                 * */
                objectId: undefined,
                className: undefined,
                pageTitle: undefined,
                /*
                 * Изначальных список выбранных идентификаторв
                 * */
                selectedList : undefined,
                /*
                 * Список добавленных идентификаторв
                 * */
                addIdList : undefined,
                /*
                * Список удалененых идентификаторв
                * */
                removeIdList : undefined
            },

            /**
             * инициализация модели
             */
            initialize: function () {
                log.debug("initialize ListBlockModel");
                var _this = this;
                this.pageTitle = 'Выбор объектов <%=folderTitle%>';

                this.listenTo(this, 'restorePage', function (params) {
                    log.debug("ObjectListBlockModel start restorePage");
                    _this.set('refAttrId', params.refAttrId);
                    _this.set('className', params.className);
                    _this.get('pageModel').set('className', params.className);
                    _this.set('selectMode', params.selectMode);
                    _this.set('objectId', params.objectId);
                    if (params.selectedList) {
                        _this.set('selectedList', params.selectedList);
                    } else {
                        _this.set('selectedList', []);
                    }
                    _this.set('addIdList', []);
                    _this.set('removeIdList', []);
                    _this._reset();
                    _this._update();
                });

                this.listenTo(this, 'refreshPage', function () {
                    log.debug("ObjectListBlockModel start refreshPage");
                    _this._update();
                });

                this.listenTo(this, 'search', function (params) {
                    var type = misc.option(params, "type", "Тип фильтрации (простой или расширенный)");
                    var criteria = misc.option(params, "criteria", "Критерий поиска");
                    var data = {
                        useFilter: type
                    };
                    if (type == 'simple') {
                        data.simpleFilter = criteria;
                    } else {
                        data.extendedFilterSettings = criteria;
                    }
                    _this.set('search', data);
                    _this._update();
                });
            },

            /**
             * Добавление списка идентификаторов к списку связанных объектов
             * @param ids
             */
            addSelectedIds : function(ids){
                var notInSelectedList = _.difference(ids, this.get('selectedList'));
                this.set('addIdList', _.union(this.get('addIdList'), notInSelectedList));
                this.set('removeIdList', _.difference(this.get('removeIdList'), ids));

            },

            /**
             * Удаление списка идентификаторов из списка связанных объектов
             * @param ids
             */
            removeSelectedIds : function(ids){
                var inSelectedListForRemove = _.intersection(ids, this.get('selectedList'));
                this.set('removeIdList', _.union(this.get('removeIdList'), inSelectedListForRemove));
                this.set('addIdList', _.difference(this.get('addIdList'), ids));

            },

            /*
             * Получаем данные с сервера
             * */
            _loadListData: function () {
                var options = {
                    action: 'getObjectData',
                    data: {
                        objectId: this.get('objectId'),
                        className: this.get('className'),
                        refAttrId: this.get('refAttrId'),
                        currentPage: this.get('currentPage'),
                        objectsPerPage: this.get('objectsPerPage'),
                        orderAttr: this.get('orderAttr'),
                        orderType: this.get('orderType'),
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true

                    }
                };
                if (this.get("search")) {
                    $.extend(options.data, this.get("search"));
                }
                return this.callServerAction(options);
            },

            /**
             * Обновление заголовка страницы
             * @private
             */
            _updatePageTitle: function () {
                var title = "Выбор объектов " + this.get('ClassDescription');
                this.updatePageTitle(title);
            },

            /**
             * Завершение выбора связанных объектов
             */
            selectionComplete: function () {
                var data = {
                    addIdList:    this.get('addIdList'),
                    removeIdList:    this.get('removeIdList'),
                    refAttrId: this.get('refAttrId')
                };
                var opener = this.get("pageModel").get("page_opener");

                this.notifyPageBlocks(new NotifyEventObject(
                    'objectsSelectionDone',
                    data,
                    opener
                ));
                this.closePage();
            }

        });
        return model;
    });
