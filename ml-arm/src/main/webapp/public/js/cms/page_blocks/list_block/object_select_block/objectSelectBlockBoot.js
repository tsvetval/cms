/**
 * Блок выбора объектов (REST)
 * Контроллер: ru.peak.ml.web.block.controller.impl.list.ObjectListSelectBlockController
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/list_block/object_select_block/model/ObjectSelectBlockModel',
        'cms/page_blocks/list_block/object_select_block/view/ObjectSelectBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
