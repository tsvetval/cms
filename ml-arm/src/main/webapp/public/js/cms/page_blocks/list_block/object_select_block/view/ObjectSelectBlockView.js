/**
 * Представление для блока выбора связанных объектов
 *
 */
define(
    ['log', 'misc', 'backbone', 'underscore',
        'boot_table_ru',
        'cms/page_blocks/list_block/object_list_block/view/ObjectListBlockView',
        'cms/page_blocks/DialogPageBlock',
        'text!cms/page_blocks/list_block/object_select_block/templates/ObjectSelectTemplate.tpl',
        'text!cms/page_blocks/list_block/object_list_block/templates/ObjectTableTemplate.tpl',
        'text!cms/page_blocks/list_block/object_select_block/templates/ObjectSelectToolbarTemplate.tpl',
        'text!cms/page_blocks/list_block/object_list_block/templates/FolderItemTemplate.tpl'
    ],
    function (log, misc, backbone, _,
              boot_table,
              ObjectListBlockView,
              Message,
              ObjectSelectTemplate,
              ObjectTableTemplate,
              ObjectToolbarTemplate,
              FolderItemTemplate) {
        var view = ObjectListBlockView.extend({

            events: {
                "click .object-selection-complete": "objectSelectionComplete",
                "click .object-selection-cancel": "objectSelectionCancel",
                "click .show-full-text": "showFullTextClick",
                "click .hide-full-text": "hideFullTextClick",
                "click .file-download-link": "downloadFile"
            },

            /**
             * инициализация представления
             */
            initialize: function () {
                log.debug("initialize ObjectListBlockView");
                this.listenTo(this.model, 'render', this.render);
            },

            /**
             * отрисовка представления
             * @returns {boolean}
             */
            render: function () {
                var _this = this;
                this.$el.html(_.template(ObjectSelectTemplate, {listModel: this.model}));
                var $objectListContainer = this.$el.find('.object-list-container');

                var objectList = this.model.get('objectList');
                this.createObjectListTable($objectListContainer, objectList);

                return true;
            },


            /**
             * Создание таблицы связанных объектов
             * @param $container    -   элемент-контейнер (jQuery-объект)
             * @param data          -   данные для отображения
             */
            createObjectListTable: function ($container, data) {
                var _this = this;

                this.$toolbar = $(_.template(ObjectToolbarTemplate, {}));
                $container.append(this.$toolbar);

                this.$table = $(_.template(ObjectTableTemplate, {}));
                $container.append(this.$table);

                //Формируем колонки таблицы
                var columns = [];
                if (this.model.get('selectMode') != 'noselect') {
                    columns.push({
                        field: 'state',
                        checkbox: this.model.get('selectMode') == 'multiselect',
                        radio: this.model.get('selectMode') != 'multiselect',
                        formatter: function (value, row, index) {
                            return _this.stateFormatter(value, row, index)
                        }
                    });
                } else {
                    _this.$toolbar.find(".object-selection-complete").hide();
                }

                columns.push({
                    field: 'objectId',
                    visible: false
                });

                data.attrList.forEach(function (columnAttr) {
                    var formatter = undefined;
                    var sortable = true;
                    if (columnAttr.fieldType === "ONE_TO_MANY" || columnAttr.fieldType === "MANY_TO_MANY") {
                        formatter = _this.formatterOneToMany;
                        sortable = false;
                    } else if (columnAttr.fieldType === "MANY_TO_ONE") {
                        formatter = _this.formatterManyToOne;
                        sortable = false;
                    } else if (columnAttr.fieldType === "BOOLEAN") {
                        formatter = _this.formatterBoolean;
                        sortable = true;
                    } else if (columnAttr.fieldType === "ENUM") {
                        formatter = _this.formatterEnum;
                        sortable = true;
                    } else if (columnAttr.fieldType === "DATE") {
                        formatter = _this.formatterDateWithOptions({dateFormat : columnAttr.fieldFormat});;
                        sortable = true;
                    } else if (columnAttr.fieldType === "FILE") {
                        formatter = _this.formatterFile;
                        sortable = true;
                    } else if (columnAttr.fieldType === "LONG_LINK") {
                        sortable = false;
                    }

                    var column = {
                        field: columnAttr.entityFieldName,
                        title: columnAttr.description,
                        align: 'left',
                        valign: 'top',
                        sortable: sortable,
                        formatter: formatter
                    };
                    columns.push(column);
                });


                //Формируем значения колонок таблицы
                var data = this.createTableDataFromValue();
                //Получаем количество объектов
                var totalRows = this.model.get('recordsCount');
                //Текущая страница
                var pageNumber = this.model.get('currentPage');
                //Количество объектов на странице
                var pageSize = this.model.get('objectsPerPage');
                //Наименование столбца для сортировки
                var sortName = this.model.get('orderAttr');
                //Тип сортировки
                var sortOrder = this.model.get('orderType');

                this.$table.bootstrapTable({
                    toolbar: _this.$toolbar,
                    idField: 'objectId',
                    data: data,
                    cache: false,
                    striped: true,
                    pagination: true,
                    pageNumber: pageNumber,
                    pageSize: pageSize,
                    pageList: [5, 10, 20, 50],
                    totalRows: totalRows,
                    sortName: sortName,
                    sortOrder: sortOrder,
                    showColumns: true,
                    minimumCountColumns: 1,
                    search: false,
                    clickToSelect: true,
                    sidePagination: "server",
                    columns: columns
                }).on('page-change.bs.table', function (e, number, size) {
                    console.log('Event: page-change.bs.table, data: ' + number + ', ' + size);
                    _this.model.changePage(number, size);
                }).on('sort.bs.table', function (e, name, order) {
                    console.log('Event: sort.bs.table, data: ' + name + ', ' + order);
                    _this.model.changeSort(name, order);
                }).on('check.bs.table', function (e, row) {
                    var ids = [row.objectId];
                    _this.model.addSelectedIds(ids);
                }).on('uncheck.bs.table', function (e, row) {
                    var ids = [row.objectId];
                    _this.model.removeSelectedIds(ids);
                }).on('check-all.bs.table', function (e) {
                    var selects = _this.$table.bootstrapTable('getData');
                    var ids = $.map(selects, function (row) {
                        return row.objectId;
                    });
                    _this.model.addSelectedIds(ids);
                }).on('uncheck-all.bs.table', function (e) {
                    var selects = _this.$table.bootstrapTable('getData');
                    var ids = $.map(selects, function (row) {
                        return row.objectId;
                    });
                    _this.model.removeSelectedIds(ids);
                });
            },

            /**
             *  форматтер для отображения выбранных элементов (чекбокс)
             */
            stateFormatter: function (value, row, index) {
                if (this.model.get('selectMode') == 'multiselect') {
                    return {
                        /*disabled: true,*/
                        checked: (_.contains(this.model.get('selectedList'), row.objectId) && !_.contains(this.model.get('removeIdList'), row.objectId)) || _.contains(this.model.get('selectedList'), row.addIdList)
                    }
                } else {
                    return _.contains(this.model.get('selectedList'), row.objectId) && !_.contains(this.model.get('removeIdList'), row.objectId);
                }
            },

            /**
             * завершение выбора связанных объектов
             */
            objectSelectionComplete: function () {
                if (this.model.get('selectMode') == 'select' && this.model.get('addIdList').length == 0) {
                    this.model.closePage();
                } else {
                    this.model.selectionComplete();
                }
            },

            /**
             * отмена выбора связанных объектов
             */
            objectSelectionCancel: function () {
                this.model.closePage();
            }

        });

        return view;
    });
