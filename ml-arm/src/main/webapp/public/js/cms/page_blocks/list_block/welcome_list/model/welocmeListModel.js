/**
 *  Модель блока выбора связанных объектов
 *
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/list_block/object_list_block/model/ObjectListBlockModel',
        'cms/events/NotifyPageBlocksEventObject'
    ],
    function (log, misc, backbone, ObjectListBlockModel, NotifyEventObject) {
        var model = ObjectListBlockModel.extend({
            defaults: {
                /*
                 * id объекта владельца атрибута
                 * */
                objectId: undefined,
                className: undefined,
                pageTitle: undefined
            },

            /**
             * инициализация модели
             */
            initialize: function () {
                log.debug("initialize WelcomeBlockModel");
                var _this = this;
                this.pageTitle = 'Начало работы';

                this.listenTo(this, 'restorePage', function (params) {
                    log.debug("ObjectListBlockModel start restorePage");
                    _this.set('folderId', params.folderId);
                    _this.set('classifierData', params.data);
                    _this.set('className', params.className);
                    _this._reset();
                    _this._update();
                });
            }



            /*
             * Получаем данные с сервера
             * */
/*            _loadListData: function () {
                var options = {
                    action: 'getObjectData',
                    data: {
                        objectId: this.get('objectId'),
                        className: this.get('className'),
                        refAttrId: this.get('refAttrId'),
                        currentPage: this.get('currentPage'),
                        objectsPerPage: this.get('objectsPerPage'),
                        orderAttr: this.get('orderAttr'),
                        orderType: this.get('orderType'),
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true

                    }
                };
                if (this.get("search")) {
                    $.extend(options.data, this.get("search"));
                }
                return this.callServerAction(options);
            },*/

            /**
             * Обновление заголовка страницы
             * @private
             */
/*            _updatePageTitle: function () {
                var title = "Выбор объектов " + this.get('ClassDescription');
                this.updatePageTitle(title);
            },*/

        });
        return model;
    });
