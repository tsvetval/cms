/**
 * Представление для блока выбора связанных объектов
 *
 */
define(
    ['log', 'misc', 'backbone', 'underscore',
        'boot_table_ru',
        'cms/page_blocks/list_block/object_list_block/view/ObjectListBlockView',
        'cms/page_blocks/DialogPageBlock',
        'text!cms/page_blocks/list_block/object_select_block/templates/ObjectSelectTemplate.tpl',
        'text!cms/page_blocks/list_block/object_list_block/templates/ObjectTableTemplate.tpl',
        'text!cms/page_blocks/list_block/object_select_block/templates/ObjectSelectToolbarTemplate.tpl',
        'text!cms/page_blocks/list_block/object_list_block/templates/FolderItemTemplate.tpl'
    ],
    function (log, misc, backbone, _,
              boot_table,
              ObjectListBlockView,
              Message,
              ObjectSelectTemplate,
              ObjectTableTemplate,
              ObjectToolbarTemplate,
              FolderItemTemplate) {
        var view = ObjectListBlockView.extend({

            events: {
            },

            /**
             * инициализация представления
             */
            initialize: function () {
                log.debug("initialize WelcomeListBlockView");
                view.__super__.initialize.call(this);
                _.extend(this.events, ObjectListBlockView.prototype.events);
                this.listenTo(this.model, 'render', this.render);
            }

        });

        return view;
    });
