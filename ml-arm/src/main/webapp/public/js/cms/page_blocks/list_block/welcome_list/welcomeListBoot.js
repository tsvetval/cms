/**
 * Блок просмотра списка объектов
 * Контроллер: ru.peak.ml.web.block.controller.impl.list.ObjectListBlockController
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/list_block/welcome_list/model/welocmeListModel',
        'cms/page_blocks/list_block/welcome_list/view/welcomeListView'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
