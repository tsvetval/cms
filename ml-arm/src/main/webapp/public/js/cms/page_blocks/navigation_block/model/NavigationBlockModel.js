define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockModel', 'cms/events/NotifyPageBlocksEventObject','console_const'],
    function (log, misc, backbone, PageBlockModel, NotifyEventObject, console_const) {
        var model = PageBlockModel.extend({
            defaults: {
                /*Идентификатор текущего каталога дерева*/
                folderId: undefined
            },
            initialize: function () {
                console.log("initialize NavigationBlockModel");


                var _this = this;

                this.listenTo(this, 'restorePage', function (options) {
                    if (options.folderId && options.folderId != this.get('folderId')) {
                        _this.changePageHash({folderId: options.folderId}, 'replaceState', 'extendHash');
                        _this.updatePageTitle('Список объектов ...');
                        _this.set("folderId",options.folderId);
                    }
                    _this.trigger('render');
                });

                this.listenTo(this, 'refreshPage', function (options) {
                    //_this.openFolder(options.folderId, 'replaceState');
                    _this.trigger('activateFolder', options);
                });
            },

            openFolder: function (data) {
                var folderInfo = JSON.parse(data.folderId);
                this.updatePageTitle('Список объектов ...');
                var mode = data.historyMode || 'pushState';
                if(folderInfo.folderId){
                    var folderId = folderInfo.folderId;
                    var classifierId = folderInfo.classifierId;
                    var value = folderInfo.value;

                    this.set('folderId', folderInfo.folderId);
                    this.trigger("showPageBlock",{folderId:folderInfo.folderId,pageBlocks:data.pageBlocks, data: data.folderId});
                    this.changePageHash({folderId: folderInfo.folderId}, mode, 'extendHash');
                    this.notifyPageBlocks(new NotifyEventObject('openFolder', {folderId: folderInfo.folderId, classifierId: classifierId,value:value}));
                }else{
                    this.set('folderId', data.folderId);
                    this.trigger("showPageBlock",data);
                    this.changePageHash({folderId: data.folderId}, mode, 'extendHash');
                    this.notifyPageBlocks(new NotifyEventObject('openFolder', {folderId: data.folderId}));
                }
            },

            findFolderPath: function(folderId){
                var def = new $.Deferred();
                this.callServerAction({
                        action: "findFolderPath",
                        data: {
                            folderId: folderId
                        }
                    }, function (result) {
                       def.resolve(result);
                    }
                );
                return def.promise();
            },

            renameFolder: function (folderId, title) {
                this.callServerAction({
                        action: "modifyNode",
                        data: {
                            objectId: folderId,
                            text: title
                        }
                    }
                );
            },

            deleteFolder: function (folderId) {
                var _this = this;
                this.callServerAction({
                        action: "removeNode",
                        data: {
                            objectId: folderId
                        }
                    }, function (result) {
                        var options = {
                            parentId: result[0].parentId
                        };
                        _this.trigger('folderDeleted', options);
                    }
                );
            },

            createFolder: function (parentId, title, nodeId) {
                var _this = this;
                this.callServerAction({
                        action: "createNode",
                        data: {
                            parentId: parentId,
                            text: title
                        }
                    }, function (result) {
                        console.log("Node Id: " + nodeId + " Folder Id: " + result[0].objectId);
                        var options = {
                            nodeId: nodeId,
                            folderId: result[0].objectId
                        };
                        _this.trigger('folderCreated', options);
                    }
                );
            },

            editFolder: function (folderId) {
                var _this = this;
                this.replacePage(
                    OBJECT_EDIT_PAGE,
                    'Просмотр объекта ...',
                    {
                        objectId: folderId,
                        className: 'MlFolder'
                    }
                );
            }

        });

        return model;
    });
