/**
 *  Блок навигации(дерево в консоли)
 *  Контроллер: ru.peak.ml.web.block.controller.impl.NavigationPageBlockController
* */
define(
    ['log', 'misc', 'backbone', 'cms/page_blocks/navigation_block/model/NavigationBlockModel', 'cms/page_blocks/navigation_block/view/NavigationBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });
