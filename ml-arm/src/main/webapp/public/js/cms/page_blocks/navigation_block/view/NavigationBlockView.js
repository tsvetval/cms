define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'jstree'],
    function (log, misc, backbone, PageBlockView, jstree) {
        var view = PageBlockView.extend({

            initialize:function () {

                console.log("initialize NavigationBlockView");
                var _this = this;
                this.$treeContainer = $('<div>Загрузка...</div>');

                this.listenTo(this.model, 'render', this.render);
                // При восстановление состояния страницы (при первичной загрузке или при навигацие)
                this.model.on('restorePage', function (options) {
                    if (options.folderId) {
                        // Когда дерево будет загружено, выделить узел согласно текущей папке
                        //todo: узел может быть в подпапке, для успешного выделения необходимо
                        //todo: предварительно загрузить дерево родителей
                        _this.$treeContainer.on("loaded.jstree", function (e, nodeData) {
                            _this.$treeContainer
                                .jstree("deselect_all")
                                .jstree("select_node", "#" + options.folderId, true);
                        });

                        _this.$treeContainer.on("rename_node.jstree", function (e, data) {
                            _this.model.renameFolder(data.node.id, data.text);
                        });

                        _this.$treeContainer.on("delete_node.jstree", function (e, data) {
                            _this.model.deleteFolder(data.node.id);
                        });

                        _this.$treeContainer.on("create_node.jstree", function (e, data) {
                            var node = _this.$treeContainer.jstree("get_node", data.parent);
                            var folderId = node.id;
                            _this.model.createFolder(folderId, data.node.text, data.node.id);
                        });
                    }
                });

                this.listenTo(this.model, 'folderCreated', this.folderCreated);
                this.listenTo(this.model, 'folderDeleted', this.folderDeleted);

                this.model.on('activateFolder', function (options) {
                    if (options.folderId) {
                        _this.setActiveFolder(options.folderId);
                    } else {
                        _this.$treeContainer.jstree("deselect_all");
                    }
                });

                this.model.on('openSubFolder', function (options) {
                    if (options.folderId) {
                        _this.setActiveFolder(options.folderId);
                        _this.model.openFolder(options);
                    } else {
                        _this.$treeContainer.jstree("deselect_all");
                        _this.model.openFolder(options);
                    }
                });
            },

            /*
             // Delegated events for creating new items, and clearing completed ones.
             events: {
             'keypress #new-todo':		'createOnEnter',
             'click #clear-completed':	'clearCompleted',
             'click #toggle-all':		'toggleAllComplete'
             },
             */

            setActiveFolder:function (folderId) {
                var _this = this;
                var callback = function () {
                    _this.$treeContainer.jstree("deselect_all").jstree('select_node', folderId);
                };
                var currentFolderId = _this.model.get('folderId');
                //раскрываем текущую папку и в коллбэкие помечаем нужную подпапку выделенной
                this.$treeContainer.jstree('open_node', currentFolderId, callback);
            },

            render:function () {
                var _this = this;
                log.debug("Initialize jstree and append it to container");
                this.$treeContainer.appendTo(this.$el);
                this.$treeContainer.jstree({
                    'plugins':["contextmenu", "json_data"],
                    'rules':{'multiple':false},
                    'lang':{new_node:"Новый каталог", loading:"Загрузка ..."},
                    'core':{
                        'animation':0,
                        'check_callback':true,

                        // Настраиваем получение данных с сервера. Настройки аналогичны настройкам функции $.ajax. Все запросы
                        // для обновления данных (динамическая подгрузка, обновление по запросу и т. д) используют эти настройки.
                        // В нашем случае все запросы проходят через экшен "folderChildren"
                        // Информация по плагину: http://www.jstree.com/docs/json/
                        'data':{
                            'type':'post',
                            // URL может быть либо строкой либо функцией возвращающей строку
                            'url':function () {
                                return 'page_block';
                            },
                            // Формируем данные для отправки на сервер
                            'data':function (node) {
                                var nodeData = node.id!="#"?JSON.parse(node.id):node.id;
                                var folderId;
                                var classifierId;
                                var value;
                                if(nodeData.folderId){
                                    folderId = nodeData.folderId;
                                    classifierId = nodeData.classifierId;
                                    value = nodeData.value;
                                }else{
                                    folderId = node.id;
                                }

                                return {
                                    'ml_request':true,
                                    'pageBlockId':_this.model.get('blockInfo').get('id'), // Идентификатор блока
                                    'id':folderId, // Идентификатор ноды (для корневого узла будет '#')
                                    'classifierId':classifierId,
                                    'value':value,
                                    'action': 'getFolderChildren' // Действие для сервера
                                };
                            }
                        }
                    },
                    'contextmenu':{
                        select_node:true, // Эта опция необходима для корректной работы переименования и создания подпапок
                        items:function (node) {
                            return {
                                rename: {
                                    "label": "Переименовать",
                                    "action": function () {
                                        _this.$treeContainer.jstree("edit", node);
                                    }
                                },
                                remove: {
                                    "label": "Удалить",
                                    "action": function () {
                                        _this.$treeContainer.jstree("delete_node", node);
                                    }
                                },
                                refresh:{
                                    "label":"Обновить",
                                    "seperator_after":true,
                                    "seperator_before": true,
                                    "action": function () {
                                        _this.refreshTree();
                                    }
                                },
                                create: {
                                    "label": "Создать",
                                    "action": function () {
                                        _this.$treeContainer.jstree("create_node", node,
                                            {
                                                text: "Новая папка",
                                                icon: "glyphicon glyphicon-folder-open"
                                            });
                                    }
                                },
                                edit: {
                                    "label": "Редактировать",
                                    "action": function () {
                                        _this.model.editFolder(node.id);
                                    }
                                }
                            }
                        }
                    }
                });

                this.$treeContainer.on("select_node.jstree", function (e, nodeData) {
                    _this.model.openFolder({folderId:nodeData.node.id,pageBlocks:nodeData.node.original.pageBlocks});
                });
                if(this.model.get("folderId")){
                    _this.$treeContainer.on('loaded.jstree', function (e, data) {
                        var deffereds = $.Deferred(function (def) { def.resolve(); });
                        var folderId = _this.model.get("folderId");
                        var def = _this.model.findFolderPath(folderId);
                        $.when(def).then(function(nodeValues){
                            for (var j = 0; j < nodeValues .length-1; j++) {
                                deffereds = (function(name, deferreds) {
                                    return deferreds.pipe(function () {
                                        return $.Deferred(function(def) {
                                            _this.$treeContainer.jstree("open_node", $("#"+name+"_anchor"), function () {
                                                def.resolve();
                                            });
                                        });
                                    });
                                })(nodeValues [j], deffereds);
                            }
                            $.when(deffereds).then(function(){
                               _this.$treeContainer.jstree("select_node", $("#"+folderId+"_anchor"));
                            });
                        });
                    });
                }
                log.debug("Finish rendering NavigationBlock");
            },

            refreshTree: function () {
                this.$treeContainer.jstree("refresh");
            },

            folderCreated: function (options) {
                var _this = this;

                if (options.nodeId && options.folderId) {
                    var node = _this.$treeContainer.jstree("get_node", options.nodeId);
                    node.id = options.folderId;
                    _this.$treeContainer.jstree("refresh");
                    //TODO добавить переход в режим редактирования
                    //_this.$treeContainer.jstree("edit", node.id);
                }
            },

            folderDeleted: function (options) {
                if (options.parentId) {
                    this.$treeContainer.jstree("deselect_all").jstree('select_node', options.parentId);
                    this.model.openFolder({folderId:options.parentId});
                }
            }

        });

        return view;
    });
