/**
 * Загрузочный модуль блока добавления объектов в репликацию
 * Контроллер: ru.peak.ml.web.block.controller.impl.AddObjectToReplicationStepUtilBlockController
 */
define(
    ['log', 'misc', 'backbone', 'cms/page_blocks/replication_block/add_to_replication/model/AddToReplicationModel'
        , 'cms/page_blocks/replication_block/add_to_replication/view/AddToReplicationView'],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });
