/**
 * Представление блока добавления объектов в репликацию
 *
 */
define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup'],
    function (log, misc, backbone, PageBlockView, markup) {
        var view = PageBlockView.extend({
            /**
             * Инициализация представления
             */
            initialize:function () {
                console.log("initialize AddToReplicationView");
                this.listenTo(this.model, 'render', this.render)
            },

            /**
             * Отрисовка представления
             */
            render:function () {
                var _this = this;
                this.$el.html(this.model.get('renderTemplate'));
                markup.attachActions(this.$el, {
                        // Обработка клика по номеру страницы
                        pageClick:function (number) {
                            _this.model.setCurrentPage(number);
                        },
                        // Обработка клика по кнопке "Добавить"
                        addToReplication:function(data){
                            _this.model.addToReplication(data);
                        }

                    }
                );
            }

        });

        return view;
    });