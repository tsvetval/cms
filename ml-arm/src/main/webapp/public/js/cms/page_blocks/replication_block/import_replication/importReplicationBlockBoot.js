/**
 * Загрузочный модуль бока импорта файла репликации
 * Контроллер: ru.peak.ml.web.block.controller.impl.ImportReplicationPackageUtilBlockController
 */
define(
    ['log', 'misc', 'backbone', 'cms/page_blocks/replication_block/import_replication/model/ImportReplicationModel'
        , 'cms/page_blocks/replication_block/import_replication/view/ImportReplicationView'],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });
