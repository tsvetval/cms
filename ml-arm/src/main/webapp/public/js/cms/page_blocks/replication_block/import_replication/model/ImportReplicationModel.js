/**
 * Модель блока импорта файла репликации
 */
define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockModel',
        'cms/page_blocks/DialogPageBlock'],
    function (log, misc, backbone, PageBlockModel, DialogPageBlock) {
        var model = PageBlockModel.extend({
            defaults: {
                renderTemplate: undefined,
                title: undefined,
                fileName: undefined,
                openEventIdList: []
            },

            /**
             * Инициализация модели
             */
            initialize: function () {
                var _this = this;
                console.log("initialize ObjectListBlockModel");
                this.listenTo(this, 'restorePage', function (params) {
                    _this.loadSelect.apply(this, arguments);
                });
            },

            /**
             *
             * @param options
             */
            loadSelect: function (options) {
                var _this = this;
                this.syncRenderTemplate().then(
                    function () {
                        _this.triggerRender()
                    });
            },

            /**
             * Обновление списка объектов репликации
             */
            refreshCurrentObjectList: function () {
                var _this = this;
                this.syncRenderTemplate().then(
                    function () {
                        _this.triggerRender()
                    });
            },

            /**
             * Установка текущей страницы списка объектов             *
             * @param pageNumber
             */
            setCurrentPage: function (pageNumber) {
                var _this = this;
                this.set('currentPage', pageNumber);
                this.syncRenderTemplate().then(
                    function () {
                        _this.triggerRender()
                    });
            },

            /**
             * Отрисовка шаблона блока
             */
            syncRenderTemplate: function () {
                var _this = this;
                return $.ajax('page_block', {
                    type: 'POST',
                    data: {
                        action: 'show',
                        className: this.get('className'),
                        objectId: this.get('objectId'),
                        historyUtilId: this.get('historyUtilId'),
                        pageBlockId: this.get('blockInfo').get('id'),
                        currentPage: this.get('currentPage'),
                        ml_request: true
                    },
                    success: function (result) {
                        _this.set('title', result.title);
                        _this.set('renderTemplate', result.html);

                        // Обновляем текущую крошку
                        _this.navigationEvent({
                            "do": 'modify',
                            "title": result.title
                        });

                    }
                });
            },

            /**
             * Импорт файла репликации
             * @param data
             */
            importFile: function (data) {
                var _this = this;
                var form = $('#' + data.formId);
                var files = [];
                files.push($('#replicationFile')[0].files[0]);
                this.set("fileName",$('#replicationFile')[0].files[0].name);
                this.uploadFiles(files, function () {
                    _this.getFileInfo(data);
                });
            },

            /**
             * Получение и отображение информации о файле репликации
             * @param data
             * @returns {*}
             */
            getFileInfo: function (data) {
                var _this = this;
                var options = new Object();
                options.action = 'showFileInfo';
                data.fileName = this.get('fileName');
                options.data = data;
                return this.callServerAction(options, function (result) {
                    _this.set('title', result.title);
                    _this.set('renderTemplate', result.html);
                    _this.triggerRender();
                });
            },

            /**
             * Загрузка файла репликации на сервер
             * @param files
             * @param callback
             */
            uploadFiles: function (files, callback) {
                console.log('Start uploading files: ' + files[0]);
                var xhr = new XMLHttpRequest();
                var url = misc.getContextPath() + "/upload";
                xhr.onreadystatechange = function (e) {
                    if (4 == this.readyState) {
                        console.log('Files upoaded: ' + e.target.responseText);
                        callback();
                    }
                };
                xhr.open('post', url, true);
                var fd = new FormData;
                $.each(files, function (index, file) {
                    fd.append(file.name, file);
                });
                xhr.send(fd);
            },

            /**
             * Выполнение репликации
             * @param data
             * @returns {*}
             */
            doReplication:function(data){
                var _this = this;
                var options = new Object();
                options.action = data.action;
                options.data = data;
                options.pageBlockId = this.get('blockInfo').get('id');
                return this.callServerAction(options, function (result) {
                    _this.set('title', result.title);
                    _this.set('renderTemplate', result.html);
                    _this.triggerRender();
                });
            },

            /**
             * Генерация события для отрисовки представления
             */
            triggerRender: function () {
                this.trigger('render');
            },

            /**
             * Отображение сообщения о результате импорта файла репликации
             *
             * @param result
             */
            showResult: function (result) {
                if (result.showType) {
                    if (result.showType == 'modal') {
                        this.showModal(result);
                    } else if (result.showType == 'execJs') {
                        this[result.functionName](result);
                    }
                }
            },

            /**
             * Отображение информационного сообщения
             *
             * @param result
             */
            showModal: function (result) {
                var dialog = new DialogPageBlock({
                    title: result.title,
                    message: result.content,
                    type: 'infoMessage'
                });

                dialog.show();
            }
        });

        return model;
    });

