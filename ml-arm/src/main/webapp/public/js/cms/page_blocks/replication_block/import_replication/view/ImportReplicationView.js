/**
 * Представление блока импорта файла репликации
 */
define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup'],
    function (log, misc, backbone, PageBlockView, markup) {
        var view = PageBlockView.extend({
            /**
             * Инициализация представления
             */
            initialize:function () {
                console.log("initialize AddToReplicationView");
                this.listenTo(this.model, 'render', this.render)
            },

            /**
             * Отрисовка представления
             */
            render:function () {
                var _this = this;
                this.$el.html(this.model.get('renderTemplate'));
                markup.attachActions(this.$el, {
                        /**
                         * Выбор текущей страницы
                         * @param number
                         */
                        pageClick:function (number) {
                            _this.model.setCurrentPage(number);
                        },

                        /**
                         * Загрузка файла импорта ("Добавить")
                         * @param data
                         */
                        importFile:function(data){
                            _this.model.importFile(data);
                        },

                        /**
                         * Подтверждение выполнения репликации ("Применить пакет обновлений")
                         * @param data
                         */
                        doReplication: function(data){
                            _this.model.doReplication(data);
                        }

                    }
                );
            }
        });

        return view;
    });