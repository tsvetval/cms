/**
 * Коллекция атрибутов
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/report_block/model/ParamModel'],
    function (log, misc, backbone, ParamModel) {
        var ParamCollection = backbone.Collection.extend({
            model: ParamModel
        });
        return ParamCollection;
    });
