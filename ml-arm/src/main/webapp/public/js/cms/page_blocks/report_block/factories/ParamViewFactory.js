/**
 * Фабрика представлений для атрибутов
 */
define(['log', 'misc', 'backbone'],
    function (log, misc, backbone) {

        var GroupModelFactory = new function () {

            /**
             * Получить представление для атрибута
             *
             * @param paramModel     -   модель атрибута
             * @param $container    -   jQuery-объект контейнер для представления
             * @returns {*}
             */
            this.getParamViewClass = function (paramModel, viewMode, $container) {
                log.debug("Trying to create view for " + paramModel.get('code') + '[' + paramModel.get('type') + ']')
                var paramViewPath = "cms/page_blocks/report_block/params/view";
                var paramCustomViewPath = paramModel.get('templateView');

                if (!paramCustomViewPath) {
                    if (paramModel.get('type') == 'STRING') {
                        paramViewPath = paramViewPath + '/StringParamView';
                    } else if (paramModel.get('type') == 'BOOLEAN') {
                        paramViewPath = paramViewPath + '/BooleanParamView';
                    } else if (paramModel.get('type') == 'LONG') {
                        paramViewPath = paramViewPath + '/LongParamView';
                    } else if (paramModel.get('type') == 'DATE') {
                        paramViewPath = paramViewPath + '/DateParamView';
                    } else if (paramModel.get('type') == 'LINK') {
                        if(paramModel.get("singleChoice")){
                            paramViewPath = paramViewPath + '/ManyToOneParamView';
                        }else{
                            paramViewPath = paramViewPath + '/OneToManyParamView';
                        }
                    } else {
                        paramViewPath = paramViewPath + '/StringParamView';
                    }
                } else {
                    paramViewPath = getparamViewPathFromTemplate(paramCustomViewPath);
                    console.log(paramViewPath);
                }

                var def = new $.Deferred();
                require([paramViewPath], function (attrViewDyn) {
                    def.resolve(attrViewDyn, paramModel, $container);
                });
                return def.promise();
            };

            /**
             * Получить представление атрибута с пользовательским шаблоном
             *
             * @param template  -   путь к шаблону
             * @returns {*}
             */
            var getparamViewPathFromTemplate = function (template) {
                //временная функция для перехода на REST, преобразует старые пути к шаблонам на новые
                var oldTemplateExt = ".hml";
                var oldTemplatePrefix = "/templates/";
                var newTemplatePrefix = "cms/page_blocks/form_block/templates/custom/";
                if (template.indexOf(oldTemplateExt, template.length - oldTemplateExt.length) !== -1) {
                    //старый шаблон — преобразуем к новому
                    return template.replace(oldTemplateExt, "")
                        .replace(oldTemplatePrefix, newTemplatePrefix);
                } else {
                    //новый шаблон — возвращаем как есть
                    return template;
                }
            };
        };

        return GroupModelFactory;
    });
