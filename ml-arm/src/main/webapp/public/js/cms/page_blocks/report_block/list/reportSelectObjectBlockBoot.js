/**
 * Блок выбора параметров типа LINK(Когда параметр ссылается на объект(ы))
 * Контроллер: ru.peak.ml.core.report.controller.ReportSelectObjectController
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/report_block/list/model/ObjectSelectReportBlockModel',
        'cms/page_blocks/report_block/list/view/ObjectSelectReportBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
