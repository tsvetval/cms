/**
 * Модель атрибута
 */
define(
    ['log', 'misc', 'backbone', 'underscore', 'jquery','console_const'],
    function (log, misc, backbone, _, $,console_const) {
        var ParamModel = backbone.Model.extend({
            defaults: {
                /*
                 * Модель блока к которому относится атрибут
                 * */
                pageBlockModel: undefined,
                id:undefined,
                name: undefined,
                code: undefined,
                type: undefined,
                mandatory : false,

                singleChoice: undefined,
                className: undefined,

                offset: 0,
                totalLength: 24,
                titleLength: 8,
                tableHeight: 200,

                value:undefined,

                hidden: false
            },

            /**
             * инициализация модели
             */
            initialize: function () {
                log.debug("initialize ParamModel " + this.get('code'));
                this.set('guid', _.uniqueId('param'));
            },

            /**
             * Добавить(установить) значение ссылочного атрибута
             * @param value     -   значение
             */
            addLinkObjectId: function (value) {
                if (this.get('fieldType') == 'MANY_TO_ONE') {
                    this.set('value', {objectId: value.objectId, title: value.title})
                } else if (this.get('fieldType') == 'ONE_TO_MANY' || this.get('fieldType') == 'MANY_TO_MANY') {
                    var idList = [];
                    idList.push(value.objectId);
                    if (this.get('value') && this.get('value').objectList) {
                        this.get('value').objectList.forEach(function (object) {
                            idList.push(object.objectId);
                        });
                    }
                    this.set('value', {idList: idList, title: undefined});
                }
            },

            /**
             *  Сериализация параметра
             * @returns {*}
             */
            serializeValue: function () {
                if (this.get('type') == 'STRING') {
                    return this.get('value');
                } else if (this.get('type') == 'LONG' || this.get('type') == 'DOUBLE') {
                    if (typeof this.get('value') !== 'undefined') {
                        return this.get('value').toString();
                    } else {
                        return undefined;
                    }
                } else if (this.get('type') == 'DATE') {
                    return this.get('value');
                } else if (this.get('type') == 'BOOLEAN') {
                    if (!this.get('value')){
                        return false;
                    }
                    return this.get('value');
                } else if (this.get('type') == 'LINK') {
                    if(this.get("singleChoice")){
                        if (this.get('value') && this.get('value').objectId) {
                            return this.get('value').objectId.toString()
                        } else {
                            return undefined;
                        }
                    }else{
                        var idList = [];
                        if (this.get('value') && this.get('value').objectList) {
                            this.get('value').objectList.forEach(function (obj) {
                                idList.push(obj.objectId)
                            })
                        }
                        return idList;
                    }

                } else {
                    return JSON.stringify(this.get('value'));
                }
            },

            /**
             * Получить имя атрибута
             * @returns {*} -   имя атрибута (entityFieldName)
             */
            getParamCode: function () {
                return this.get('code');
            },

            /**
             * Является ли атрибут обязательным для заполнения
             * @returns {*}
             */
            isMandatory: function () {
                return this.get('mandatory') && !this.get('hidden');
            },


            /**
             * Задано ли значение для атрибута
             * @returns {boolean}
             */
            hasValue: function () {
                if (typeof this.get('value') !== 'undefined') {
                    if (this.get('type').indexOf('LINK') != -1
                        && this.get('value').objectList && this.get('value').objectList.length == 0){
                        return false;
                    }
                    return true;
                }
                return false;
            },

            /**
             * Генерация события "Подсветить незаполненный обязательный атрибут"
             */
            triggerNotFilledMandatory: function () {
                this.trigger('HighlightMandatory');
            },

            /**
             * Генерация события "Убрать подсветку атрибута"
             */
            triggerRemoveHighlightMandatory: function () {
                this.trigger('RemoveHighlightMandatory');
            },

            /**
             * Установить значение атрибута
             * @param value -   значение
             */
            setAttrValue: function (value) {
                this.set('value', value);
            },

            /**
             * Открыть объект для редактирования (для ссылочных атрибутов)
             *
             * @param objectId  -   id объекта
             * @param mlClass   -   имя класса объекта
             */
            openObjectEdit: function (objectId, mlClass) {
                log.debug("Trigger Event [openObjectEdit] for className =" + mlClass + " objectId =" + objectId);
                this.openPage(OBJECT_EDIT_PAGE, 'редактирование объекта ...', {
                    objectId: objectId,
                    className: mlClass
                });
            },


            /**
             * Открыть страницу по URL
             *
             * @param url       -   url страницы
             * @param title     -   заголовок
             * @param params    -   параметры
             */
            openPage: function (url, title, params) {
                this.trigger('OPEN_PAGE', {
                    title: title,
                    url: url,
                    params: params
                });
            },

            /**
             * Вызов серверного метода
             *
             * @param pageblock     -   страничный блок
             * @param options       -   параметры
             * @param callback      -   функция обратного вызова
             * @returns {*}
             */
            callServerAction: function (pageblock, options, callback) {
                return this.get('pageBlockModel').callServerAction(pageblock, options, callback);
            },

            /**
             * Скрыть атрибут
             */
            hide: function () {
                this.set("hidden", true);
            },

            /**
             * Показать атрибут
             */
            show: function () {
                this.set("hidden", false);
            },

            /**
             * Обновить представление атрибута
             */
            refresh: function () {
                this.trigger('render');
            }

        });

        return ParamModel;
    });
