<div class="attr-label-container col-md-offset-<%=paramModel.get('offset')%> col-md-<%=paramModel.get('titleLength')%>">
    <b class="attr-label"><%=paramModel.get('name')%>:</b>
</div>
<div class="col-md-<%=(paramModel.get('totalLength') - paramModel.get('titleLength') - paramModel.get('offset'))%> input-group">
    <input value="" readonly style="width: 100%; padding-left: 5px;" class="attrFieldTitle"/>
<!--
    <input type="hidden" class="attrField linked-item" value=""/>
-->
    <span class="btn btn-primary input-group-addon editManyToOne">
              <span class="glyphicon glyphicon-pencil"></span>
    </span>
    <span class="btn btn-primary input-group-addon deleteLinkedObject">
        <span class="glyphicon glyphicon-trash"></span>
    </span>
    <span class="btn btn-primary input-group-addon selectLinkedObject">
        <span class="glyphicon glyphicon-search highlight-button"></span>
    </span>
</div>
