<div class="attr-label-container col-md-offset-<%=paramModel.get('offset')%> col-md-<%=paramModel.get('titleLength')%>">
    <b class="attr-label" style="height: 24px; line-height: 24px;"><%=paramModel.get('name')%>:</b>
</div>
<div>
    <div id="custom-toolbar" class="btn-group custom-toolbar">
           <span class="btn btn-primary editManyToOne inactive-button">
              <span class="glyphicon glyphicon-pencil"></span>
           </span>
            <span class="btn btn-primary deleteLinkedObject inactive-button">
                <span class="glyphicon glyphicon-trash"></span>
            </span>
            <span class="btn btn-primary selectLinkedObject active-button">
                <span class="glyphicon glyphicon-search highlight-button"></span>
            </span>
    </div>

    <table class="table-javascript">
    </table>
</div>