/**
 * Представление для атрибута типа BOOLEAN
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/report_block/params/ParamView',
        'text!cms/page_blocks/report_block/params/templates/BooleanParamView.tpl'],
    function (log, misc, backbone,
              ParamView, DefaultTemplate) {
        var view = ParamView.extend({
            $inputField: undefined,

            events: {
                "change .attrField": "changeClick"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, ParamView.prototype.events);
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                if (this.isHidden()) {
                    return;
                }
                this.$el.html(_.template(this.viewTemplate, {paramModel: this.model}));
                this.addMandatoryEvents();
                this.addReadOnly();

                this.$inputField = this.$el.find('.attrField');
                if (this.model.get('value')) {
                    this.$inputField.attr('checked', true);
                } else {
                    this.$inputField.attr('checked', false);
                }
            },

            /**
             * Обработчик изменения значения HTML-элемента
             */
            changeClick: function () {
                this.model.attributes['value'] = this.$inputField.is(':checked');
            }

        });

        return view;
    });
