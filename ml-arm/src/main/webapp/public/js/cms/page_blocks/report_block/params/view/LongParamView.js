/**
 * Представление для атрибута типа LONG
 */
define(
    ['log', 'misc', 'backbone', 'inputmask',
        'cms/page_blocks/report_block/params/ParamView',
        'text!cms/page_blocks/report_block/params/templates/LongParamView.tpl'],
    function (log, misc, backbone, inputmask,
              ParamView, DefaultTemplate) {
        var view = ParamView.extend({
            $inputField: undefined,

            events: {
                "keyup .attrField": "keyPressed"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, ParamView.prototype.events);
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                if (this.isHidden()) {
                    return;
                }
                this.$el.html(_.template(this.viewTemplate, {paramModel: this.model}));
                this.$inputField = this.$el.find('.attrField');
                this.$inputField.val(this.model.get('value'));
                if (this.model.has('inputmask')) {
                    var maskData = {
                        mask: JSON.parse(this.model.get('inputmask'))
                    };
                    this.$inputField.inputmask(maskData);
                }
                this.addMandatoryEvents();
                this.addReadOnly();
            },

            /**
             * Обработчик изменения значения HTML-элемента
             */
            keyPressed: function () {
                if (this.$inputField.val().length == 0) {
                    this.model.unset('value');
                } else {
                    this.model.set('value', this.$inputField.val());
                }
            }

        });

        return view;
    });
