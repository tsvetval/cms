/**
 * Представление для атрибута типа MANY_TO_ONE
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/report_block/params/ParamView',
        'text!cms/page_blocks/report_block/params/templates/ManyToOneParamView.tpl',
        'console_const'],
    function (log, misc, backbone,
              ParamView, DefaultTemplate,console_const) {
        var view = ParamView.extend({
            /*
             $valueHolderElement: undefined,
             */
            $titleHolderElement: undefined,

            events: {
                "click .editManyToOne": "editManyToOne",
                "click .deleteLinkedObject": "deleteLinkedObject",
                "click .createClick": "createClick",
                "click .selectLinkedObject": "selectLinkedObject"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;

                this.listenTo(this.model, 'change:value', this.renderAttrValue);
                _.extend(this.events, ParamView.prototype.events);
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                if (this.isHidden()) {
                    return;
                }
                this.$el.html(_.template(this.viewTemplate, {paramModel: this.model}));

                this.addMandatoryEvents();
                this.addReadOnly();

                //this.$valueHolderElement = this.$el.find('.attrField');
                this.$titleHolderElement = this.$el.find('.attrFieldTitle');

                if (this.model.get('value') && this.model.get('value').title) {
                    this.$titleHolderElement.val(this.model.get('value').title);
                } else {
                    this.$titleHolderElement.val('');
                }
            },

            /**
             * Отображение значения атрибута
             */
            renderAttrValue: function () {
                var _this = this;
                if (this.model.get('value') && !this.model.get('value').title) {
                    this.$titleHolderElement.val('Идет получение заголовка...');
                    this.model.callServerAction({
                        action: "getRefParamValues",
                        data: {
                            objectId: _this.model.get('pageBlockModel').get('objectId'),
                            className: _this.model.get('className'),
                            paramId: this.model.get('id'),
                            idList: JSON.stringify(_this.model.get('value').idList)
                        }
                    }).then(function (result) {
                        _this.model.set('value', {objectId: result.result.id, title: result.result.title});
                    });
                } else {
                    if (this.model.get('value')) {
                        this.$titleHolderElement.val(this.model.get('value').title);
                    } else {
                        this.$titleHolderElement.val('');
                    }
                }
            },

            /**
             * Получить id выбранного связанного объекта
             */
            getSelectedObjectId: function () {
                if (this.model.get('value')) {
                    return this.model.get('value').objectId;
                } else {
                    return undefined;
                }
            },

            /**
             * Редактировать связанный объект
             */
            editManyToOne: function () {
                if (this.model.get('value') && this.model.get('value').objectId) {
                    this.model.openObjectEdit(this.model.get('value').objectId, this.model.get('linkClass'));
                }
            },

            /**
             * Удалить связанный объект (удаление связи)
             */
            deleteLinkedObject: function () {
                this.model.set('value', undefined);
            },

            /**
             * Выбрать связанный объект
             */
            selectLinkedObject: function () {
                var objectList = [];
                var selectedObjectId = this.getSelectedObjectId();
                if (selectedObjectId) {
                    objectList.push( String(selectedObjectId));
                }
                var params = {
                    paramId: this.model.get('id'),
                    className: this.model.get('className'),
                    selectMode: "select",
                    objectId: this.model.get('id'),
                    selectedList: objectList
                };

                this.model.openPage(
                    REPORT_SELECT_OBJECT_PAGE,
                    'Просмотра объекта ...',
                    params
                );
            }

        });

        return view;
    });
