/**
 * Представление для атрибута типа ONE_TO_MANY
 */
define(
    ['log', 'misc', 'backbone', 'underscore', 'jquery', 'boot_table_ru', 'moment',
        'cms/page_blocks/report_block/params/ParamView',
        'text!cms/page_blocks/report_block/params/templates/OneToManyParamView.tpl',
        'table_formatter_mixin','console_const'],
    function (log, misc, backbone, _, $, boot_table, moment,
              ParamView, DefaultTemplate, table_formatter_mixin,console_const) {
        var view = ParamView.extend({
            events: {
                "click .deleteLinkedObject": "deleteLinkedObject",
                "click .editManyToOne": "editManyToOne",
                "click .createClick": "createClick",
                "click .selectLinkedObject": "selectLinkedObject",
                "click .file-download-link": "downloadFile",
                "click .moveUp": "moveUpClick",
                "click .moveDown": "moveDownClick",
                "click .show-full-text": "showFullTextClick",
                "click .hide-full-text": "hideFullTextClick"

            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                this.$toolbar = undefined;
                this.$table = undefined;
                this.listenTo(this.model, 'change:value', this.renderAttrValue);
                _.extend(this.events, ParamView.prototype.events);
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                if (this.isHidden()) {
                    return;
                }
                var _this = this;
                this.$el.html(_.template(this.viewTemplate, {paramModel: this.model}));
                this.$table = this.$el.find('.table-javascript');
                this.$toolbar = this.$el.find('.custom-toolbar');
                if (!this.model.get("ordered")) {
                    this.$toolbar.find(".moveUp, .moveDown").hide();
                }

                this.$attrLabelContainer = this.$el.find('.attr-label-container');

                this.addMandatoryEvents();
                this.addReadOnly();

                //Формируем колонки таблицы
                var columns = [];
                columns.push({
                    field: 'state',
                    checkbox: true
                });
                columns.push({
                    field: 'objectId',
                    visible: false
                });

                this.model.get('attrList').forEach(function (columnAttr) {
                    var formatter = undefined;
                    var sortable = true;
                    if (columnAttr.fieldType === "ONE_TO_MANY" || columnAttr.fieldType === "MANY_TO_MANY") {
                        formatter = _this.formatterOneToMany;
                        sortable = false;
                    } else if (columnAttr.fieldType === "MANY_TO_ONE") {
                        formatter = _this.formatterManyToOne;
                        sortable = false;
                    } else if (columnAttr.fieldType === "BOOLEAN") {
                        formatter = _this.formatterBoolean;
                        sortable = true;
                    } else if (columnAttr.fieldType === "ENUM") {
                        formatter = _this.formatterEnum;
                        sortable = true;
                    } else if (columnAttr.fieldType === "DATE") {
                        formatter = _this.formatterDateWithOptions({dateFormat : columnAttr.fieldFormat});;
                        sortable = true;
                    } else if (columnAttr.fieldType === "FILE") {
                        formatter = _this.formatterFile;
                        sortable = true;
                    } else if (columnAttr.fieldType === "LONG_LINK") {
                        sortable = false;
                    }

                    var column = {
                        field: columnAttr.entityFieldName,
                        title: columnAttr.description,
                        align: 'left',
                        valign: 'top',
                        sortable: sortable,
                        formatter: formatter
                    };
                    columns.push(column);
                });

                //Формируем значения колонок таблицы
                var data = this.createTableDataFromValue();


                this.$table.bootstrapTable(/*'append', */{
                    toolbar: _this.$toolbar,
                    label : _this.$attrLabelContainer,
                    idField: 'objectId',
                    data: data,
                    cache: false,

                    /* height: 400,*/
                    striped: true,
                    pagination: true,
                    pageSize: 5,
                    pageList: [5, 10, 20, 50],
                    showColumns: true,
                    minimumCountColumns: 1,
                    search: true,
                    clickToSelect: true,
                    columns: columns,
                    toolbarAlign :  'right'
                }).on('all.bs.table', function (name, args) {
                    _this.updateButtonState();
                });
                _this.updateButtonState();
            },

            /**
             * Обновление состояния кнопок для работы со связанными объектами
             */
            updateButtonState: function () {
                var selects = this.$table.bootstrapTable('getSelections');
                if (selects.length > 0) {
                    this.$toolbar.find(".deleteLinkedObject")
                        .removeClass("inactive-button")
                        .addClass("active-button");
                } else {
                    this.$toolbar.find(".deleteLinkedObject")
                        .removeClass("active-button")
                        .addClass("inactive-button");
                }

                if (selects.length == 1) {
                    this.$toolbar.find(".moveUp, .moveDown, .editManyToOne")
                        .removeClass("inactive-button")
                        .addClass("active-button");
                } else {
                    this.$toolbar.find(".moveUp, .moveDown, .editManyToOne")
                        .removeClass("active-button")
                        .addClass("inactive-button");
                }
            },

            /**
             * Заполнение таблицы данных о связанных объектах
             */
            createTableDataFromValue: function () {
                var data = [];
                if (this.model.get('value') && this.model.get('value').objectList) {
                    this.model.get('value').objectList.forEach(function (objectData) {
                        data.push($.extend(objectData.attrValues, {objectId: objectData.objectId}));
                    });
                }
                return data;
            },

            /**
             * Просмотр связанного объекта
             */
            linkObjectPreviewClick: function () {
                this.model.openObject($(event.srcElement).attr('objectId'), $(event.srcElement).attr('mlClass'));
                return false;
            },

            /**
             * Удалить связанный объект (удаление связи)
             */
            deleteLinkedObject: function () {
                var selects = this.$table.bootstrapTable('getSelections');
                var ids = $.map(selects, function (row) {
                    return row.objectId;
                });
                // Удаляем из коллекции модели все выбранные объекты
                if (ids && ids.length > 0) {
                    var newObjectList = _.reject(this.model.get('value').objectList, function (obj) {
                        return _.contains(ids, obj.objectId);
                    });
                    this.model.get('value').objectList = newObjectList;
                    this.model.trigger('change:value');
                }
            },

            /**
             * Редактировать связанный объект
             */
            editManyToOne: function () {
                var selects = this.$table.bootstrapTable('getSelections');
                var ids = $.map(selects, function (row) {
                    return row.objectId;
                });
                if (ids && ids.length == 1) {
                    var params = {
                        refAttrId: this.model.get('id'),
                        objectId: ids[0],
                        className: this.model.get('linkClass')
                    };
                    this.model.openPage(
                        OBJECT_EDIT_PAGE,
                        'Редактирование объекта ...',
                        params
                    );
                }
            },

            /**
             * Подвинуть выделенный связанный объект вверх в упорядоченном списке
             */
            moveUpClick: function () {
                var selects = this.$table.bootstrapTable('getSelections');
                var ids = $.map(selects, function (row) {
                    return row.objectId;
                });
                if (ids && ids.length == 1) {
                    var pos = this.getItemPosInList(ids[0]);
                    if (pos > 0) {
                        var prevItem = this.model.get('value').objectList[pos - 1];
                        this.model.get('value').objectList[pos - 1] = this.model.get('value').objectList[pos];
                        this.model.get('value').objectList[pos] = prevItem;
                        this.render();
                    }
                }
            },

            /**
             * Подвинуть выделенный связанный объект вниз в упорядоченном списке
             */
            moveDownClick: function () {
                var selects = this.$table.bootstrapTable('getSelections');
                var ids = $.map(selects, function (row) {
                    return row.objectId;
                });
                if (ids && ids.length == 1) {
                    var pos = this.getItemPosInList(ids[0]);
                    if (pos > -1 && pos < this.model.get('value').objectList.length - 1) {
                        var nextItem = this.model.get('value').objectList[pos + 1];
                        this.model.get('value').objectList[pos + 1] = this.model.get('value').objectList[pos];
                        this.model.get('value').objectList[pos] = nextItem;
                        this.render();
                    }
                }
            },

            /**
             * Получить положение объекта в упорядоченном списке
             */
            getItemPosInList: function (id) {
                var list = this.model.get('value').objectList;
                var pos = -1;
                $.each(list, function (index, item) {
                    if (item.objectId == id) {
                        pos = index;
                    }
                });
                return pos;
            },

            /**
             * Выбрать связанные объекты
             */
            selectLinkedObject: function () {
                var selects = this.$table.bootstrapTable('getData');
                var ids = $.map(selects, function (row) {
                    return row.objectId;
                });
                var params = {
                    paramId: this.model.get('id'),
                    objectId: this.model.get('pageBlockModel').get('objectId'),
                    className: this.model.get('linkClass'),
                    selectMode: "multiselect",
                    selectedList: ids
                };
                this.model.openPage(
                    REPORT_SELECT_OBJECT_PAGE,
                    'Выбор списка объектов ...',
                    params
                );
            },

            /**
             * Отображение значение связанного объекта
             */
            renderAttrValue: function () {
                var _this = this;
                if (this.model.get('value') && !this.model.get('value').attrList) {
                    //this.$titleHolderElement.val('Идет получение заголовка...');
                    this.model.callServerAction({
                        action: "getRefParamValues",
                        data: {
                            objectId: _this.model.get('pageBlockModel').get('objectId'),
                            className: _this.model.get('className'),
                            paramId: this.model.get('id'),
                            idList: JSON.stringify(_this.model.get('value').idList)
                        }
                    }).then(function (result) {
                        _this.model.set('value', result.result);
                    });
                } else {
                    var data = this.createTableDataFromValue();
                    this.$table.bootstrapTable('load', data);
                }
            },
            /**
             * развернуть список связанных объектов
             *
             */
            showFullTextClick: function () {
                $(event.srcElement).hide();
                $(event.srcElement).next().show();
                $(event.srcElement).prevAll(".ellipsis").hide();
                $(event.srcElement).prevAll(".one-many-text-container").removeClass("collapsed");
            },

            /**
             * свернуть список связанных объектов
             *
             */
            hideFullTextClick: function () {
                $(event.srcElement).hide();
                $(event.srcElement).prev().show();
                $(event.srcElement).prevAll(".ellipsis").show();
                $(event.srcElement).prevAll(".one-many-text-container").addClass("collapsed");
            }
        });

        _.extend(ParamView.prototype, table_formatter_mixin);

        return view;
    });
