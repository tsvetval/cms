/*
*  Блок формирования отчетов.
*  Кнтроллер: ru.peak.ml.core.report.controller.ReportExecutionController
* */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/report_block/model/ReportBlockModel',
        'cms/page_blocks/report_block/view/ReportBlockView'
    ],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
