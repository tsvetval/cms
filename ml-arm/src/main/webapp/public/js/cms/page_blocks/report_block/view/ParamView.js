define(
    ['log', 'misc', 'backbone'],
    function (log, misc, backbone) {
        var view = backbone.View.extend({
            $attrLabel : undefined,


            initialize: function () {
                console.log("initialize AttrView for model class = " + this.model.getParamCode());
                this.listenTo(this.model, 'render', this.render);
                this.listenTo(this.model, 'generate-report-button', this.render);

            },

            render: function () {
                if (this.isHidden()) {
                    return;
                }
                this.$el.html(_.template(this.viewTemplate, {paramModel: this.model}));
            },

            isHidden: function () {
                var hidden = this.model.get('hidden');
                if (hidden) {
                    this.$el.empty();
                    return true;
                } else {
                    return false;
                }
            }


        });

        return view;
    });
