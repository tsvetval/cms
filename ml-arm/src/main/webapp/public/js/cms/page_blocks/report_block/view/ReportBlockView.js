define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup', 'cms/page_blocks/DialogPageBlock', 'moment',
        'cms/page_blocks/report_block/factories/ParamViewFactory',
        'text!cms/page_blocks/report_block/templates/ReportTemplate.tpl'],
    function (log, misc, backbone, PageBlockView, markup, Message,moment,
              ParamViewFactory,
              ReportTemplate) {
        var view = PageBlockView.extend({

            events: {
                "click .generate-report-button": "generateReport"
            },

            initialize: function () {
                console.log("initialize BlockView");
                this.listenTo(this.model, 'render', this.render);
                this.listenTo(this.model, 'change:serverData', this.update);
            },

            /**
             * Отрисовка параметров
             *
             * @param $content_container
             * @private
             */
            _renderParams : function($content_container){
                var _this = this;
                // Выводим все атрибуты без групп
                var outAttrContainer = undefined;
                for (var index in this.model.get('paramsList').models) {
                    var paramModel =  this.model.get('paramsList').models[index];
                    if (paramModel.get('newLine') || !outAttrContainer){
                        // создаем новую строку
                        outAttrContainer = new $('<div/>', {"class": "col-md-24 newLine"});
                        $content_container.append(outAttrContainer);
                    }
                    // создаем контейнер для атрибута
                    var $paramContainer = $('<div/>', {"class": "attrContainer newLine"});
                    outAttrContainer.append($paramContainer);
                    _this._renderParam(paramModel, $paramContainer);
                    //TODO refactor
                }
            },

            _renderParam : function(paramModel, $paramContainer) {
                ParamViewFactory.getParamViewClass(paramModel, this.viewMode, $paramContainer)
                    .then(function (attrViewClass, model, $container) {
                        // Получаем представление для конкретного атрибуты
                        var attrView = new attrViewClass({model: model});
                        attrView.setElement($container);
                        attrView.render();
                    });

            },
            render: function () {
                var _this = this;
                this.$el.html(_.template(ReportTemplate, {reportModel: this.model}));
                // create container
                var $content_container = _this.$el.find('#report_container');
                _this._renderParams($content_container);
            },

            generateReport: function (e) {
                this.model.generateReport();
            },

            update: function () {
                this.$el.find(".server-data-container").html(this.model.get("serverData"));
            }

        });
        return view;
    });
