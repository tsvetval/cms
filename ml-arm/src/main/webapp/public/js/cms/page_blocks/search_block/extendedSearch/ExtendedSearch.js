define(
    [
        "jquery",
        "misc",
        'backbone',
        'moment',
        'cms/page_blocks/search_block/extendedSearch/Id',
        'cms/page_blocks/search_block/extendedSearch/Operation',
        'cms/page_blocks/search_block/extendedSearch/IdTree',
        'cms/page_blocks/search_block/extendedSearch/FiltersPanelView',
        'datetimepicker',
        'inputmask'

    ], function ($, misc, Backbone, moment, Id, Operation, IdTree, FiltersPanelView) {

        var view = Backbone.View.extend({

            initialize: function (options) {
                var _this = this;

                this.blockModel = misc.option(options, "blockModel", "Модель блока, к которому подключен поиск");

                var createLoadFunction = function () {
                    var cache = {};
                    return function (className) {
                        return function (callback) {
                            if (typeof className === "object") {
                                className = className.objectId + "@" + className.mlClass;
                            }

                            if (cache[className]) {
                                callback.apply(this, [cache[className]]);
                            }
                            else {
                                _this.blockModel.callServerAction({
                                        action: "getSearchMetaData",
                                        data: {
                                            className: className,
                                            type: "extended"
                                        }
                                    },
                                    function (result) {

                                        var attrs = result.attrList.objectList;

                                        // Используем только атрибуты с соответсвующим свойством
                                        //attrs = attrs.filter(function(x){return x.useInExtendedSearch==true});

                                        // Используем только атрибуты, для типа которых есть операции, а
                                        // также атрибуты связи
                                        attrs = attrs.filter(function (x) {
                                            return operations[x.attrValues.fieldType.code] != undefined || x.attrValues.linkClass != undefined;
                                        });

                                        // Конвертируем данные с сервера в формат расширенных фильтров
                                        var children = attrs.map(function (next) {
                                            if (next.attrValues.linkClass) {
                                                return {
                                                    id: new Id(next.attrValues.entityFieldName,
                                                                next.attrValues.fieldType.code,
                                                                next.attrValues.description || next.attrValues.entityFieldName
                                                    ),
                                                    branch: new IdTree({
                                                        loadFunction: createLoadFunction(next.attrValues.linkClass)
                                                    })
                                                }
                                            }
                                            else {
                                                return new Id(next.attrValues.entityFieldName,
                                                                next.attrValues.fieldType.code,
                                                                next.attrValues.description || next.attrValues.entityFieldName,
                                                                next.attrValues.enumList.objectList,
                                                                next.attrValues.fieldFormat);
                                            }
                                        });
                                        cache[className] = children;
                                        callback.apply(this, [children]);
                                    }
                                );
                            }
                        };
                    }
                }();


                var operations = {
                    "STRING": [
                        new Operation("equals", "равен"),
                        new Operation("not_equals", "не равен"),
                        new Operation("startsWith", "начинается с"),
                        new Operation("endsWith", "заканчивается на"),
                        new Operation("contains", "содержит"),
                        new Operation("not_contains", "не содержит")
                    ],
                    "LONG": [
                        new Operation("equals", "равен"),
                        new Operation("not_equals", "не равно"),
                        new Operation("less", "меньше"),
                        new Operation("greater", "больше"),
                        new Operation("less_or_equals", "меньше или равно"),
                        new Operation("greater_or_equals", "больше или равно")
                    ],
                    "LONG_LINK":[
                        new Operation("contains", "содержит"),
                        new Operation("not_contains", "не содержит")
                    ],
                    "DATE": [
                        //TODO implement
/*                        new Operation("equals", "равен"),
                        new Operation("not_equals", "не равно"),*/
                        new Operation("from", "с"),
                        new Operation("to", "по")
                    ],
                    "ENUM": [
                        new Operation("equals", "равен"),
                        new Operation("not_equals", "не равен")
                    ]
                };

                var typeInputs = {
                    "DATE": function (idObj) {
                        if (idObj.format){
                            var dateFormat = moment().toMomentFormatString(idObj.format);
                            return $('<input/>').datetimepicker({
                                format: dateFormat,
                                useCurrent: false,
                                locale: 'ru',
                                language: 'ru'
                            });
                        } else {
                            return $('<input/>').datetimepicker({format: 'DD.MM.YYYY', useCurrent: false, locale: 'ru'})
                        }
                    },
                    "LONG": function (idObj) {
                        return $('<input/>').inputmask("integer")
                    },
                    "ENUM": function (idObj) {
                        return $('<select/>')
                    }
                };

                this.listenTo(this.blockModel, 'change:searchMetaData', function (model) {
                    var idTree = new IdTree({
                        loadFunction: createLoadFunction(model.get("searchMetaData").entityName)
                    });

                    if (this.rootPanelHolder) this.rootPanelHolder.clean();
                    this.rootPanelHolder = new FiltersPanelView({
                        idTree: idTree,
                        deep: 0,
                        operations: operations,
                        typeInputs: typeInputs
                    });

                    this.$el.append(this.rootPanelHolder.$html);
                    this.$el.data("extendedFilterView", this.rootPanelHolder);

                });

                this.listenTo(this.blockModel, 'resetSearch', function () {
                    var idTree = new IdTree({
                        loadFunction: createLoadFunction(this.blockModel.get("searchMetaData").entityName)
                    });

                    if (this.rootPanelHolder) this.rootPanelHolder.clean();
                    this.rootPanelHolder = new FiltersPanelView({
                        idTree: idTree,
                        deep: 0,
                        operations: operations,
                        typeInputs: typeInputs
                    });

                    this.$el.append(this.rootPanelHolder.$html);
                    this.$el.data("extendedFilterView", this.rootPanelHolder);

                });

            },

            render: function () {
                return this;
            }
        });

        return view;
    });