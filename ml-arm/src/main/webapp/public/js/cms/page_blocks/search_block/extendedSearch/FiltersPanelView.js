define(
    [
        "jquery",
        "misc",
        'cms/page_blocks/search_block/extendedSearch/Id',
        'cms/page_blocks/search_block/extendedSearch/Operation',
        'cms/page_blocks/search_block/extendedSearch/IdTree',
        'select2'
    ],

    function ($, misc, Id, Operation, IdTree) {

        /*
            View for single filter
         */
        var FilterView = function (options) {

            this.id = misc.option(options, "id", "ID", FilterView.createId());
            this.panel = misc.option(options, "panel", "Панель, содержащая данный фильтр");
            this.deep = misc.option(options, "deep", "Глубина вложенности панели", 0);
            this.idTree = misc.option(options, "idTree", "Дерево элементов для отображения");
            this.operations = misc.option(options, "operations", "Мапа типов и операций");
            this.typeInputs = misc.option(options, "typeInputs", "Специфичные для типов поля вводов");


            var _this = this;

            var $html = $("<div style='margin:5px 0px;' class='FilterPanelView input-group col-md-1'/>");

            // Select id list
            var $idSelect = $("<div  class='input-group-addon'><select/></div>");
            $html.append($idSelect);

            // Select operation list
            var $opSelect = $("<div class='input-group-addon'><select class='opSelect'/></div>");
            $html.append($opSelect);

            // Value input
            var $value = $("<div class='input-group-addon'><span><input type='text'/><span></div>");
            $html.append($value);

            // Delete button
            var $deleteButton = $("<div  class='input-group-addon'/>").append($("<button class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-trash'/></button>"));
            $html.append($deleteButton);


            // Sub filterViews
            this.subPanel = null;

            /*
             Logic
             */


            $idSelect.find("select").change(function () {
                var idObj = _this.idMap[$(this).val()];
                if (idObj instanceof Id) {
                    $opSelect.removeClass('hide');
                    $value.removeClass('hide');
                    $opSelect.find("select").empty();

                    if(_this.subPanel) {
                        _this.subPanel.clean();
                        _this.subPanel = null;
                    }

                    if (_this.operations[idObj.type]) {
                        var typeOperations = _this.operations[idObj.type];
                        for (var i = 0; i < typeOperations.length; i++) {
                            var op = typeOperations[i];
                            $opSelect.find("select").append("<option value='" + op.value + "'>" + op.title + "</option>");
                        }
                    }
                    if (_this.typeInputs[idObj.type]) {
                        $value.find("span").empty();
                        $value.find("span").append(_this.typeInputs[idObj.type](idObj));
                        if (idObj.enumList) {
                            for (var i = 0; i < idObj.enumList.length; i++) {
                                $value.find("select").append("<option value='" + idObj.enumList[i].attrValues.code + "'>" + idObj.enumList[i].attrValues.title + "</option>");
                            }
                        }
                    }
                    else {
                        $value.find("span").empty();
                        $value.find("span").append($("<input type='text'/>"));
                    }
                }
                else {
                    $opSelect.addClass('hide');
                    $value.addClass('hide');

                    _this.createSubPanel(idObj.branch);

                }
            });

            this.$html = $html;
            this.$idSelect = $idSelect;
            this.$opSelect = $opSelect;
            this.$deleteButton = $deleteButton;
            this.$value = $value;
            this.idMap = {length: 0};


            /*
             Выполняем загрузку атрибутов
             */
            this.waitForIds();


            $deleteButton.find("button").click(function () {
                _this.panel.deleteFilter(_this);
            });

        };

        FilterView.createId = (function () {
            var counter = 0;
            return function () {
                return counter++;
            }
        })();

        FilterView.prototype.appendTo = function ($container) {
            $container.append(this.$html);
        };

        FilterView.prototype.hide = function () {
            this.$html.hide();
        };

        FilterView.prototype.show = function () {
            this.$html.css('display', '');
        };

        FilterView.prototype.clean = function () {
            if (this.subPanel) this.subPanel.clean();
        };


        FilterView.prototype.setDeleteEnabled = function(enabled) {
            if(enabled===false) {
                this.$deleteButton.addClass("hide");
            }
            else {
                this.$deleteButton.removeClass("hide");
            }
        };

        FilterView.prototype.createSubPanel = function(idTree) {
            var _this = this;
            if(_this.subPanel) _this.subPanel.clean();

            _this.subPanel = new FiltersPanelView({
                idTree: idTree,
                deep:   _this.deep + 1,
                operations: _this.operations,
                typeInputs: _this.typeInputs
            });
            _this.subPanel.$html.insertAfter(_this.$html);

            return this.subPanel;
        };

        FilterView.prototype.waitForIds = function (callback) {
            var _this = this;

            if (!_this.idsLoaded) {
                var $content = $("> *", this.$html);
                var $replacer = $("<img src='public/images/loading.gif'/>");
                this.$html.append($replacer);

                $content.hide();
                this.idTree.get([], function (children) {
                    _this.$idSelect.find("select").empty();
                    _this.$idSelect.find("select").append($("<option></option>"));
                    for (var i = 0; i < children.length; i++) {
                        var idObj = children[i];
                        if (idObj instanceof Id) {
                            _this.$idSelect.find("select").append($("<option value='" + idObj.name + "'>" + idObj.title + "</option>"));
                            _this.idMap[idObj.name] = idObj;
                        }
                        else {
                            var id = idObj.id;
                            _this.$idSelect.find("select").append($("<option value='" + id.name + "'>" + id.title + "</option>"));
                            _this.idMap[id.name] = idObj;
                        }
                        //TODO тут есть ошибка что если верхним атрибутом будет ссылочный атрибут
                        // В этом случае нужно его инициализировать но если он указывает на тот же атрибут то уходим в бесконечный цикл
/*
                        if (_this.idMap.length == 0 ) {
                            _this.$idSelect.find("select").change();
                        }
*/
                        _this.idMap.length++;
                    }
                   // _this.$idSelect.find("select").prop("selectedIndex", -1);
                    _this.$idSelect.find("select").select2({
                        'width': 200,
                        //'allowClear': true,
                        'placeholder': "Выберите значение"
                    });
                    $replacer.remove();
                    $content.css('display', '');
                    _this.idsLoaded = children;
                    if (callback) callback.apply(this, [children]);
                });
            }
            else {
                if (callback) callback.apply(this, [_this.idsLoaded]);
            }
        };

        FilterView.prototype.getJson = function () {
            var result = {};
            result.entityName = this.$idSelect.find("select").val();
            if (!this.subPanel) {
                result.op = this.$opSelect.find("select").val();
                result.value = this.$value.find("input, select").val();
                //TODO validate date
/*                if (this.idMap[result.entityName].type == 'DATE'){
                    var date = misc.parseDateByTemplate(result.value, this.viewDatePattern);
                    result.value =  String(date.valueOf());
                }*/
            }
            else {
                result.children = this.subPanel.getJson();
            }
            return  result;
        };

        FilterView.prototype.fromJson = function (json) {
            var _this = this;
            this.waitForIds(function (children) {
                _this.$idSelect.find("select").val(json.entityName);
                _this.$idSelect.find("select").change();
                if (json.children) {
//                    _this.createSubPanel(children);
                    _this.subPanel.fromJson(json.children);
                }
                else {
                    _this.$opSelect.find("select").val(json.op);
                    _this.$value.find("input").val(json.value);
                }
            });
        };



        /*
            View for filters panel
         */

        var FiltersPanelView = function (options) {
            var _this = this;

            this.idTree = misc.option(options, "idTree", "Список доступных entityName (ветвь дерева)");
            this.operations = misc.option(options, "operations", "Мапа типов и операций");
            this.typeInputs = misc.option(options, "typeInputs", "Специфичные для типов поля вводов");

            this.deep = misc.option(options, "deep", "Глубина вложенности", 0);
            this.filterViews = [];

            var $html = $("<div class='FiltersPanelView "+(this.deep>0 ? "subpanel" : "")+"' />");
            this.$html = $html;

            this.$filterViews = $("<div class='FiltersPanelView_list'/>");
            $html.append(this.$filterViews);

            var $addButton = $("<button class='btn btn-primary'><span class='glyphicon glyphicon-plus'/></button>");
            $html.append($addButton);

            $addButton.click(function () {
                _this.addFilter();
            });

            // Этот вызов необходим для того, стобы подгрузить объекты до того, как
            // пользователь начнет добавлять фильтры
            this.idTree.get([], function (children) {});

            // Add one filter
            this.addFilter();
        };

        FiltersPanelView.prototype.addFilter = function () {
            var filter = new FilterView({
                deep:   this.deep,
                idTree: this.idTree,
                panel:  this,
                operations: this.operations,
                typeInputs: this.typeInputs
            });
            this.filterViews.push(filter); //todo: make sure, that on delete it's deleted
            this.$filterViews.append(filter.$html);
            this.checkCount();
            return filter;
        };

        FiltersPanelView.prototype.deleteFilter = function (filter) {
            filter.clean();
            filter.$html.remove();
            this.filterViews = this.filterViews.filter(function (x) {return x.id != filter.id});
            this.checkCount();
        };

        FiltersPanelView.prototype.clean = function () {
            for (var i = 0; i < this.filterViews.length; i++) {
                var filter = this.filterViews[i];
                this.deleteFilter(filter);
            }
            this.$html.remove();
        };

        FiltersPanelView.prototype.checkCount = function () {
            for (var i = 0; i < this.filterViews.length; i++) {
                var view = this.filterViews[i];
                view.setDeleteEnabled(this.filterViews.length>1);
            }
        };

        FiltersPanelView.prototype.getJson = function () {
            var result = [];
            for (var i = 0; i < this.filterViews.length; i++) {
                var filterView = this.filterViews[i];
                result.push(filterView.getJson());
            }
            return result;
        };

        FiltersPanelView.prototype.fromJson = function (json) {
            var _this = this;

            // Удаляем уже созданные фильтры
            this.filterViews.map(function (next) {_this.deleteFilter(next)});

            // После загрузки дерева элементов загружаем фильтры
            for (var i = 0; i < json.length; i++) {
                var next = json[i];
                var filter = _this.addFilter();
                filter.fromJson(next);
            }
        };

        return FiltersPanelView;

    });