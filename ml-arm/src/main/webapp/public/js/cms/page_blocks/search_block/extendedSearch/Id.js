define([], function(){
    var Id = function (name, type, title, enumList, format) {
        this.name = name;
        this.type = type;
        this.title = title;
        this.enumList = enumList;
        this.format = format;
    };
    return Id;
});