define(
    [
        "jquery",
        "misc",
        'cms/page_blocks/search_block/extendedSearch/Id'
    ],
    function ($, misc, Id) {

        var IdTree = function (options) {

            this.loadFunction = misc.option(options, "loadFunction", "Функция для динамической подгрузки дерева");
            this.loaded = false;
            this.children = [];
        };

        IdTree.prototype.get = function (path, callback) {
            var _this = this;

            // Если дерево еще не было загружено с сервера - загружаем
            if (!this.loaded) {
                this.loadFunction(function (children) {
                    _this.loaded = true;
                    _this.children = children;
                    _this.get(path, callback);
                });
            }
            else {
                if (path.length == 0) {
                    callback.apply(this, [this.children]);
                }
                else {
                    var head = path[0];
                    for (var i = 0; i < this.children.length; i++) {
                        var next = this.children[i];
                        if (next instanceof Id) {
                            if (next.name == head.name) {
                                callback.apply(this, [next]);
                                return;
                            }
                        }
                        else if (next.id.name == head.name) {
                            next.branch.get(path.slice(1), callback)
                            return;
                        }
                    }
                    throw new Error("Element with name '" + head.name + "' is not found");
                }
            }
        };

        return IdTree;
    });