define([], function(){

    var Operation = function(value, title) {
        this.value = value;
        this.title = title;
    };

    return Operation;

});