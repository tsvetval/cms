define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockModel', 'cms/events/NotifyPageBlocksEventObject'],
    function (log, misc, backbone, PageBlockModel, NotifyEventObject) {

        var model = PageBlockModel.extend({
            defaults: {
                /*Идентификатор текущего каталога дерева если есть*/
                folderId: undefined,
                className: undefined,
                refAttrId: undefined,
                refClassName: undefined,
                search: undefined,
                searchMetaData: undefined
            },

            initialize: function () {
                var _this = this;
                console.log("initialize SimpleSearchBlockModel");
                this.listenTo(this, 'restorePage', this.prompt);
                this.listenTo(this, 'openFolder', this.prompt);
                this.listenTo(this, 'change:search', function (params) {
                    if (this.get('search')) {
                        var s = params.get('search');
                        s.refClassName = params.get('refClassName');
                        params.set('search', s);
                        _this.notifyPageBlocks(new NotifyEventObject('search', params.get("search")));
                    }
                });
            },

            prompt: function (params) {
                var _this = this;
                _this.set("refClassName", params.refClassName);
                _this.set("refAttrId", params.refAttrId);
                if (params.folderId) {
                    _this.set("folderId", params.folderId);
                }

                var folderId = _this.get('folderId'),
                    refClassName = _this.get('refClassName'),
                    refAttrId = _this.get('refAttrId'),
                    className = _this.get('pageModel').get('className');
                if (!folderId && !className && !refAttrId) {
                    console.log("[folderId], [className] and [refAttrId] are missing in [getSearchMetaData] action of [SearchBlock]. Request rejected.")
                    return false;
                }

                _this.callServerAction({
                        action: "getSearchMetaData",
                        data: {
                            folderId: _this.get('folderId'),
                            refClassName: _this.get('refClassName'),
                            refAttrId: _this.get('refAttrId'),
                            className: _this.get('pageModel').get('className'),
                            objectId:  params.objectId,
                            type: "simple"
                        }
                    },
                    function (result) {
                        if (result.ok) {
                            delete result.ok;
                            _this.set("searchMetaData", result);
                            _this.trigger('render');
                        }
                    }
                );
            }
        });
        return model;
    });
