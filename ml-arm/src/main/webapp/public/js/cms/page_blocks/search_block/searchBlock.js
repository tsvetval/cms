/**
 * Блок фильтров. простой и расширенный
 * Кнтроллер: ru.peak.ml.web.block.controller.impl.SearchBlockController
 * */

define(
    [
        'log',
        'misc',
        'backbone',
        'cms/page_blocks/search_block/model/SearchBlockModel',
        'cms/page_blocks/search_block/view/SearchBlockView'
    ],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });
