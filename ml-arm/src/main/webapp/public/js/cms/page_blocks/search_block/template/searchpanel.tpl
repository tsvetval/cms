<div style="padding-top: 5px; border-bottom: solid 1px #dedede;">
    <div class="input-group searchbar simpleSearchPanel panel-collapse" id="simpleSearchPanel"
         style="width: 200px; padding-bottom: 5px">
        <div class="input-group-addon">
            <span>Поиск:</span>
        </div>

        <div class="btn-group">
            <input type="text" id="simpleFilterText" class="simpleFilterText dropdown-toggle" data-toggle="dropdown">
            <ul class="dropdown-menu search-choice-container"></ul>
        </div>

        <div class="input-group-btn">
            <button id="simpleFilterButton" class="btn btn-primary doSimpleSearchButton">
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </div>

        <div class="input-group-btn" style="padding-left: 5px;">
            <button data-target="#extendedSearchPanel" id="extendedSearchButton"
                    class="btn btn-primary switchToExtendedSearchButton">
                Расширенный поиск
            </button>
        </div>

        <div class="input-group-btn" style="padding-left: 5px;">
            <button class="btn btn-primary resetButton">
                Сброс
            </button>
        </div>
    </div>

    <div id="extendedSearchPanel" class="extendedSearchPanel hide">
        <div class="input-group-btn" style="padding-left: 5px;">
            <button id="simpleSearchButton" class="btn btn-primary switchToSimpleSearchButton">
                Простой поиск
            </button>
        </div>
        <div class="input-group-btn" style="padding-left: 5px;">
            <button class="btn btn-primary resetButton">
                Сброс
            </button>
        </div>
        <div style="margin-top: 20px; margin-left: 50px;">
            <div class="extendedFiltersContainer"></div>

            <button class="btn btn-primary doExtendedSearchButton">
                <span class="glyphicon glyphicon-search"></span> Найти
            </button>
        </div>
    </div>
</div>
