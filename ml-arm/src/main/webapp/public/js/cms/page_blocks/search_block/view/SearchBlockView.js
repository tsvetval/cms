define(
    [
        'log',
        'misc',
        'backbone',
        'markup',
        'cms/view/PageBlockView',
        'cms/events/NotifyPageBlocksEventObject',
        'cms/page_blocks/search_block/extendedSearch/ExtendedSearch',
        'text!cms/page_blocks/search_block/template/searchpanel.tpl'
    ],
    function (log, misc, backbone, markup, PageBlockView, NotifyEventObject, ExtendedSearch, SearchPanelTemplate) {
        var view = PageBlockView.extend({

            events: {
                "click .resetButton": "resetSearch",

                "click .switchToSimpleSearchButton": "simpleSearchClick",
                "click .switchToExtendedSearchButton": "extendedSearchClick",

                "click .doSimpleSearchButton": "doSimpleSearchClick",
                "click .doExtendedSearchButton": "doExtendedSearchClick",

                "keydown .simpleFilterText": "simpleSearchEnter"
            },

            defaults: {
                renderTemplate: undefined
            },

            initialize: function () {
                var _this = this;
                console.log("initialize SimpleSearchBlockView");

                this.listenTo(this.model, 'render', _this.render);

                this.listenTo(this.model, 'openFolder', function (params) {
                    _this.$el.find("#simpleFilter").val("");
                    _this.model.set('search', undefined);
                });

                this.extendedSearch = new ExtendedSearch({blockModel: this.model});
            },

            render: function () {
                var _this = this;
                var $html = $(_.template(SearchPanelTemplate, {}));

                var simpleAttrs = this.model.get("searchMetaData").attrList.objectList;
                var $dropdown = $html.find(".dropdown-menu");
                $dropdown.empty();
                $.each(simpleAttrs, function (index, elem) {
                    var $option = $('<li><span class="prefix">' + elem.attrValues.description + ' содержит: </span><span class="query"/></li>')
                    $option.on('click', function () {
                        var criteria = _this.$el.find("input.simpleFilterText").val();
                        _this.model.set('search', {
                            type: 'simple',
                            criteria: criteria,
                            searchField: elem.attrValues.entityFieldName
                        });
                    });
                    $dropdown.append($option);
                });
                $html.find("input.simpleFilterText").on('keyup', function () {
                    $dropdown.find(".query").html($(this).val());
                });

                this.$extendedSearchHtml = this.extendedSearch.render().$el;
                $html.find(".extendedFiltersContainer").append(this.$extendedSearchHtml);

                _this.$el.html($html);
            },

            simpleSearchEnter: function (e) {
                var code = e.keyCode || e.which;
                if (code == 13) {
                    this.doSimpleSearchClick();
                } else if (code == 27) {
                    this.resetSearch();
                }
            },

            resetSearch: function () {
                this.$el.find("input.simpleFilterText").val("");
                this.doSimpleSearchClick();
                this.model.trigger('resetSearch');
            },

            simpleSearchClick: function () {
                this.resetSearch();
                this.$el.find(".simpleSearchPanel").removeClass('hide');
                this.$el.find(".extendedSearchPanel").addClass('hide');
            },

            extendedSearchClick: function () {
                this.resetSearch();
                this.$el.find(".simpleSearchPanel").addClass('hide');
                this.$el.find(".extendedSearchPanel").removeClass('hide');
            },

            doSimpleSearchClick: function () {
                var criteria = this.$el.find("input.simpleFilterText").val();
                this.model.set('search', {type: 'simple', criteria: criteria});
            },

            doExtendedSearchClick: function () {
                var extFilterJson = this.$extendedSearchHtml.data('extendedFilterView').getJson();
                this.model.set('search', {type: 'extended', criteria: JSON.stringify(extFilterJson)});
            }

        });

        return view;
    });
