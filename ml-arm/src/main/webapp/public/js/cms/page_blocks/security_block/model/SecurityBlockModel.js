define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockModel', 'cms/page_blocks/DialogPageBlock'],
    function (log, misc, backbone, PageBlockModel, Message) {
        var model = PageBlockModel.extend({
            defaults: {
                renderTemplate: undefined
            },
            initialize: function () {
                console.log("initialize SecurityBlockModel");

                this.listenTo(this, 'restorePage', function (params) {
                    this.prompt();
                });
            },

            prompt: function () {
                var _this = this;
                var options = {
                    action: "getUserData"
                };

                var callback = function (result) {
                };

                return this.callServerAction(options).then(function(result){
                    if (result.login){
                        _this.set('login', result.login);
                    } else {
                        _this.unset('login')
                    }

                    if (result.helpUrl){
                        _this.set('helpUrl', result.helpUrl);
                    } else {
                        _this.unset('helpUrl')
                    }
                    _this.trigger('render');
                });
            },

            login: function (login, password) {
                var _this = this;
                var options = {
                    action: 'login',
                    data: {
                        login: login,
                        password: password
                    }
                };

                return this.callServerAction(options).then(function(result){
                    if (result.authorized == true) {
                        var url;
                        if (result.useHomePageUrl || !$.cookie('urlBeforeAuth') || $.cookie('urlBeforeAuth').indexOf(window.location.pathname) >= 0) {
                            url = result.url ? result.url : misc.getContextPath();
                        } else {
                            url = $.cookie('urlBeforeAuth');
                        }
                        $.removeCookie('urlBeforeAuth',{path    : '/'});
                        window.location.href = url;
                    } else {
                        var errorMessage = new Message({
                            title: "Ошибка аутентификации",
                            message: "Неверное имя пользователя или пароль",
                            type: 'dialogError'
                        });
                        errorMessage.show();
                    }

                });
            },

            logout: function () {
                var _this = this;
                var options = {
                    action: 'logout'
                };
                return this.callServerAction(options).then(function(result){
                    window.location.href = result.logoutUrl;
                });
            }

        });
        return model;
    });
