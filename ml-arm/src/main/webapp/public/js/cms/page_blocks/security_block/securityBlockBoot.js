/**
 * Блок авторизации
 * Контроллер: ru.peak.ml.web.block.controller.impl.SecurityBlockController
 * */
define(
    ['log', 'misc', 'backbone', 'cms/page_blocks/security_block/model/SecurityBlockModel', 'cms/page_blocks/security_block/view/SecurityBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });
