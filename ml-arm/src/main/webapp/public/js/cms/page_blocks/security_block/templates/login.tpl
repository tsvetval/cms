<div id="content" style="margin-top: 200px;">
    <div class="col-md-8 col-md-offset-8">
        <div class="centered">
            <h2>Добро пожаловать!
                <div id='test'>
                    <small>тестовые логин и пароль: admin/admin</small>
                </div>
                <div id='error' style="display: none;">
                    <small style="color: red">Ошибка аутентификации!</small>
                </div>
            </h2>
            <div>
                <hr/>
                <div class="row">
                    <label class="col-md-8" style="text-align: left">Имя пользователя</label>

                    <div class="col-md-16">
                        <input class="input form-control" id="login" name="login" value="admin">
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-8" style="text-align: left">Пароль</label>

                    <div class="col-md-16">
                        <input class="input form-control" id="password" name="password" type="password"
                               value="admin">
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-24">
                        <button id="loginButton" class="col-md-3 btn-primary form-control callback" click-action="loginClick">Войти</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
