<div>
    <div class="logout-block">
        <div class="input-group">
            <input type="text" class="form-control" readonly="true" value="<%=login%>">
                <span id="logoutButton" class="btn btn-primary input-group-addon callback">
                    <span class="glyphicon glyphicon-log-out"></span> Выход
                </span>
            <%if (showHelp){%>
                <span id="helpPage" class="btn btn-primary input-group-addon callback active-button">
                    <span class="glyphicon glyphicon-question-sign"></span></span>
            <%}%>
        </div>
    </div>
</div>