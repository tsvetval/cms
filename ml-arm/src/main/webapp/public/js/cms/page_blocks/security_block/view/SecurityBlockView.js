define(
    ['log', 'misc', 'backbone',
        'cms/view/PageBlockView', 'cms/page_blocks/DialogPageBlock',
        'text!cms/page_blocks/security_block/templates/login.tpl',
        'text!cms/page_blocks/security_block/templates/logout.tpl'
],
    function (log, misc, backbone, PageBlockView, Message,
              loginTemplate, logOutTemplate) {
        var view = PageBlockView.extend({

            events : {
                "click #loginButton" : "loginClick",
                "click #logoutButton" : "logoutClick",
                "keyup #login": "keyPress",
                "keyup #password": "keyPress",
                "click #helpPage" : "showHelp"
            },

            initialize:function () {
                console.log("initialize SecurityBlockView");
                this.listenTo(this.model, 'render', this.render);
            },
            render:function () {
                var _this = this;
                if (this.model.get('login')){
                    this.$el.html(_.template(logOutTemplate, {
                        login: this.model.get('login'),
                        showHelp : this.model.get('helpUrl') && (this.model.get('helpUrl') != '')
                    }));

                } else {
                    this.$el.html(_.template(loginTemplate));
                }
            },

            keyPress : function(event) {
                if(event.keyCode == 13){
                    this.loginClick();
                }
            },

            loginClick:function () {
                var login = this.$el.find('input#login').val();
                var password = this.$el.find('input#password').val();
                this.model.login(login, password);
            },

            logoutClick:function () {
                this.model.logout();
            },

            showHelp : function(){
                window.open(misc.getContextPath() + "/" + this.model.get('helpUrl'));
            }

        });

        return view;
    });
