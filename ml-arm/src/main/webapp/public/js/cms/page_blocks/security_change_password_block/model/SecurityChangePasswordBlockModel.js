/**
 * модель блока смены пароля
 */
define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockModel'],
    function (log, misc, backbone, PageBlockModel) {
        var model = PageBlockModel.extend({
            defaults: {
                renderTemplate: undefined
            },

            /**
             * инициализация модели
             */
            initialize: function () {
                console.log("initialize SecurityBlockModel");

                this.listenTo(this, 'restorePage', function (params) {
                    this.prompt();
                });
            },

            /**
             * запрос шаблона для отображения блока
             */
            prompt: function () {
                var _this = this;
                var options = {
                    action: "show"
                };

                var callback = function (result) {
                    _this.set('renderTemplate', result.html);
                    _this.trigger('render');
                };

                return this.callServerAction(options, callback);
            },

            /**
             * смена пароля
             *
             * @param oldPassword   -   старый пароль
             * @param password      -   новый пароль
             * @param confirmation  -   подтверждение пароля
             */
            changePassword: function (oldPassword, password, confirmation) {
                var _this = this;
                var options = {
                    action: 'change',
                    data: {
                        oldPassword: oldPassword,
                        password: password,
                        confirmation: confirmation
                    }
                };

                var callback = function (result) {
                    if (result.passwordChanged == true) {
                        url = result.url ? result.url : misc.getContextPath();
                        window.location.href = url;
                    } else {
                        var errorMessage = new Message({
                            title:   "Ошибка смены пароля",
                            message: "При смене пароля возникла непредвиденная ошибка на сервере. Обратитесь к Администратору.",
                            type:    'dialogError'
                        });
                        errorMessage.show();
                    }
                };

                return this.callServerAction(options, callback);
            },

            /**
             * валидация
             *
             * @param oldPassword   -   старый пароль
             * @param password      -   новый пароль
             * @param confirmation  -   подтверждение пароля
             */
            validate: function (oldPassword, password, confirmation) {
                var _this = this;
                var options = {
                    action: 'validate',
                    data: {
                        oldPassword: oldPassword,
                        password: password,
                        confirmation: confirmation
                    }
                };

                var callback = function (validationResult) {
                    _this.set('validationResult',validationResult);
                };

                return this.callServerAction(options, callback);
            }
        });
        return model;
    });
