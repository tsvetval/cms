/**
 * загрузочный модуль блока смены пароля
 * Контроллер: ru.peak.ml.web.block.controller.impl.SecurityChangePasswordBlockController
 */
define(
    ['log', 'misc', 'backbone', 'cms/page_blocks/security_change_password_block/model/SecurityChangePasswordBlockModel', 'cms/page_blocks/security_change_password_block/view/SecurityChangePasswordBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });
