/**
 * представление блока смены пароля
 */
define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup', 'cms/page_blocks/DialogPageBlock'],
    function (log, misc, backbone, PageBlockView, markup, Message) {
        var view = PageBlockView.extend({
            initialize:function () {
                console.log("initialize SecurityChangePasswordBlockView");
                this.listenTo(this.model, 'render', this.render);
                this.listenTo(this.model, 'change:validationResult', this.markValidation);
            },
            render:function () {
                var _this = this;
                this.$el.html(this.model.get('renderTemplate'));
                // Аттачим функции управления целым объектом
                markup.attachActions(this.$el, {
                    changePasswordClick:function () {
                        var oldPassword = _this.$el.find('input#oldPassword').val();
                        var password = _this.$el.find('input#password').val();
                        var confirmation = _this.$el.find('input#confirmation').val();
                        _this.model.changePassword(oldPassword, password, confirmation);
                    },
                    performValidation:function () {
                        var oldPassword = _this.$el.find('input#oldPassword').val();
                        var password = _this.$el.find('input#password').val();
                        var confirmation = _this.$el.find('input#confirmation').val();
                        _this.model.validate(oldPassword, password, confirmation);
                    }
                });
            },
            /**
             *  отображения статуса выполнения всех условий валидации
             */
            markValidation: function(){
                var validation = this.model.get('validationResult');
                this.displayValidationResultOnElem($('#oldPasswordLabel'), validation.oldPasswordIsOk);
                this.displayValidationResultOnElem($('#confirmationLabel'), validation.confirmationIsOk);
                this.displayValidationResultOnElem($('#passwordLengthLabel'), validation.passwordLengthIsOk);
                this.displayValidationResultOnElem($('#passwordAlphabetLabel'), validation.passwordAlphabetIsOk);
                this.displayValidationResultOnElem($('#passwordUniqueLabel'), validation.passwordUniqueIsOk);
            },
            /**
             *  отображения статуса выполнения условия валидации
             *
             * @param $elem     -   jQuery-элемент
             * @param result    -   результат выполнения
             */
            displayValidationResultOnElem: function($elem, result){
                if(result){
                    $elem.removeClass('invalid').addClass('valid');
                } else {
                    $elem.removeClass('valid').addClass('invalid');
                }
            }
        });
        return view;
    });
