define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockModel',
        'cms/page_blocks/DialogPageBlock'],
    function (log, misc, backbone, PageBlockModel, DialogPageBlock) {
        var model = PageBlockModel.extend({
            defaults: {
                title: undefined,
                position:0,

                currentFile: undefined,
                fileList: [],
                lines: []
            },

            initialize: function () {
                console.log("initialize SecurityBlockModel");
                this.position = 0;
                this.listenTo(this, 'restorePage', function (params) {
                    this.prompt();
                });
            },

            prompt: function () {
                var _this = this;
                var options = {
                    action: "show"
                };

                var callback = function (result) {
                    _this.set('fileList', JSON.parse(result.fileList));
                    _this.trigger('render');
                };

                return this.callServerAction(options, callback);
            },

            showFile: function () {
                var _this = this;
                var fileName = this.get('fileName');
                if (fileName) {
                    var options = {
                        action: "showFile",
                        data: {
                            fileName: fileName,
                            position: this.get('position')
                        }
                    };
                    var callback = function (result) {
                        _this.set('position', result.position);
                        _this.set('lines', JSON.parse(result.lines));
                    };
                    return this.callServerAction(options, callback);
                }
            },

            clearFile: function () {
                var _this = this;
                var fileName = this.get('fileName');
                if (fileName) {
                    var options = {
                        action: "clearFile",
                        data: {
                            fileName: fileName
                        }
                    };
                    var callback = function (result) {
                        _this.showFile();
                    };
                    return this.callServerAction(options, callback);
                }
            },

            download:function(){
                var fileName = this.get('fileName');
                if (fileName) {
                    var url = 'page_block?action=download&ml_request=true&';
                    url += 'fileName='+fileName;
                    url += '&pageBlockId='+ this.get('blockInfo').get('id');
                    window.location = url;
                }
            }

        });

        return model;
    });

