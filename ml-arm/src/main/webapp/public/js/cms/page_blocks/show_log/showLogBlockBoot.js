/**
 * Блок просмотра логов сервера.
 * Контроллер: ru.peak.ml.web.block.controller.impl.ShowLogBlockController
 * */

define(
    ['log', 'misc', 'backbone', 'cms/page_blocks/show_log/model/showLogModel'
        , 'cms/page_blocks/show_log/view/showLogView'],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });
