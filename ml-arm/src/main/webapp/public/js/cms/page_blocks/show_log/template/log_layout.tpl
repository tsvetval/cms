<div class="row">
    <div class="col-lg-offset-1">
        <select id="fileName" class="select2 attrField fileName"></select>
        <button class="callback download">Скачать</button>
        <button class="callback showFile">Обновить</button>
        <button class="callback clearFile">Очистить</button>
        <button class="callback previous300">Предыдущие 300</button>
        <button class="callback next300">Следующие 300</button>
    </div>
    <div id="LOG" class="col-lg-offset-1 row"
         style='font-family: "Courier New"; font-size: small;font-weight: bold'></div>
</div>
