define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup', 'select2',
        'text!cms/page_blocks/show_log/template/log_layout.tpl',
        'text!cms/page_blocks/show_log/template/log_line.tpl'
    ],
    function (log, misc, backbone, PageBlockView, markup, select2,
              LayoutTemplate,
              LineTemplate) {
        var view = PageBlockView.extend({

            events: {
                "click .download": "downloadClick",
                "click .showFile": "showFileClick",
                "click .clearFile": "clearFileClick",
                "click .previous300": "previous300Click",
                "click .next300": "next300Click",
                "change .fileName": "fileNameChange"
            },

            initialize:function () {
                console.log("initialize AddToReplicationView");
                this.listenTo(this.model, 'render', this.render);
                this.listenTo(this.model, 'change:lines', this.showLogLines);
            },

            render:function () {
                this.$el.html(_.template(LayoutTemplate, {}));
                this.$logContainer = this.$el.find("#LOG");
                var fileList = this.model.get('fileList');
                var data = [];
                $.each(fileList, function (index, elem) {
                    data.push({id: elem, text: elem});
                });
                this.$el.find('.fileName').select2({
                    data: data,
                    dropdownAutoWidth : true
                });
                var defaultFile = 'ml-cms-arm.log';
                if ($.inArray(defaultFile, fileList) != -1) {
                    this.$el.find('.fileName').val(defaultFile).trigger('change');
                }
            },

            showLogLines: function () {
                var _this = this;
                this.$logContainer.empty();
                $.each(this.model.get('lines'), function (index, elem) {
                    var color = '#a9a9a9';
                    var lineClass = 'col-lg-offset-1';
                    if (elem.indexOf("DEBUG") >= 0) {
                        color = 'black';
                        lineClass = '';
                    } else if (elem.indexOf("INFO") >= 0) {
                        color = 'a9a9a9';
                        lineClass = '';
                    } else if (elem.indexOf("WARNING") >= 0) {
                        color = 'green';
                        lineClass = '';

                    } else if (elem.indexOf("SEVERE") >= 0) {
                        color = '#8b0000';
                        lineClass = '';
                    } else if (elem.indexOf("ERROR") >= 0) {
                        color = 'red';
                        lineClass = '';
                    } else if (elem.startsWith("\t")) {
                        lineClass = 'col-lg-offset-1';
                    }
                    var line = {
                        content: elem,
                        color: color,
                        "class": lineClass
                    };
                    var $lineString = $(_.template(LineTemplate, {line: line}));
                    _this.$logContainer.append($lineString);
                });
            },

            downloadClick: function () {
                var fileName = this.$el.find('#fileName').val();
                this.model.set('fileName', fileName);
                this.model.download();
            },

            showFileClick: function () {
                var fileName = this.$el.find('#fileName').val();
                this.model.set('position', 0);
                this.model.set('fileName', fileName);
                this.model.showFile();
            },

            clearFileClick: function () {
                var fileName = this.$el.find('#fileName').val();
                this.model.set('position', 0);
                this.model.set('fileName', fileName);
                this.model.clearFile();
            },

            previous300Click: function () {
                var position = this.model.get('position');
                position = position + 300;
                this.model.set('position', position);
                this.model.showFile();
            },

            next300Click: function () {
                var position = this.model.get('position');
                position = position - 300;
                if (position < 0) {
                    position = 0;
                }
                this.model.set('position', position);
                this.model.showFile();
            },

            fileNameChange: function () {
                var fileName = this.$el.find('#fileName').val();
                this.model.set('position', 0);
                this.model.set('fileName', fileName);
                this.model.showFile();
            }

        });
        return view;
    });