define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockModel', 'cms/page_blocks/DialogPageBlock'],
    function (log, misc, backbone, PageBlockModel, Message) {
        var model = PageBlockModel.extend({
            defaults: {
                renderTemplate: undefined
            },

            initialize: function () {
                console.log("initialize StaticBlockModel");

                this.listenTo(this, 'restorePage', function () {
                    this.prompt();
                });
            },

            prompt: function () {
                var _this = this;
                var options = {
                    action: "show"
                };

                var callback = function (result) {
                    _this.set('renderTemplate', result.html);
                    _this.trigger('render');
                };

                return this.callServerAction(options, callback);
            }
        });
        return model;
    });
