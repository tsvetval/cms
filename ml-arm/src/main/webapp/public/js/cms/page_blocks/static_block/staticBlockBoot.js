/**
 * Блок статического контента
 * Контроллер: ru.peak.ml.web.block.controller.impl.MlStaticPageBlockController
 * */

define(
    ['log', 'misc', 'backbone', 'cms/page_blocks/static_block/model/StaticBlockModel', 'cms/page_blocks/static_block/view/StaticBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });

