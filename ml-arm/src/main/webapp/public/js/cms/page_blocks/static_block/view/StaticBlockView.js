define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup', 'cms/page_blocks/DialogPageBlock', 'moment'],
    function (log, misc, backbone, PageBlockView, markup, Message) {
        var view = PageBlockView.extend({

            initialize:function () {
                console.log("initialize StaticBlockView");
                this.listenTo(this.model, 'render', this.render);
            },

            render:function () {
                var _this = this;
                this.$el.html(this.model.get('renderTemplate'));
            }

        });
        return view;
    });
