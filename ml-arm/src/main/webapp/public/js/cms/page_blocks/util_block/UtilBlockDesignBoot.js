/**
 * Блок утилит. Представляет из себя pageBlock с утилитами(кнопками)
 * Контроллер: ru.peak.ml.web.block.controller.impl.UtilsPageBlockController
 * */
define(
    ['log', 'misc', 'backbone', 'cms/page_blocks/util_block/model/UtilBlockDesignModel', 'cms/page_blocks/util_block/view/UtilBlockDesignView'],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });
