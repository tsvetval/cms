define(
    [
        'log',
        'misc',
        'backbone',
        'cms/model/PageBlockModel',
        '../util/model/UtilInfoModel',
        '../util/collection/UtilInfoCollection',
        '../util/collection/UtilCollection',
        'cms/page_blocks/DialogPageBlock'
    ],
    function (log, misc, backbone, PageBlockModel, UtilInfoModel, UtilInfoCollection, UtilCollection, DialogPageBlock) {
        var model = PageBlockModel.extend({
            defaults: {
                utilInfoCollection: undefined,
                /*Коллекция реальных утилит*/
                utilCollection: undefined,
                actionForFindUtils: undefined,
                initialized: false
            },
            initialize: function () {

                log.debug("initialize UtilBlockModel");
                var _this = this;

                this.listenTo(this, 'openFolder', function (params) {
                    _this.set("params", params);
                    _this.set('folderId', params.folderId);
                    _this.set('initialized', false);
                    _this.set('actionForFindUtils', "getFolderUtils");
                    _this.loadUtilsList();
                });

                this.listenTo(this, 'restorePage', function (params) {
                    if (!_this.get('initialized')) {
                        _this.set("params", params);
                        if (params.objectId) {
                            _this.set("objectId", params.objectId);
                            _this.set("className", params.className);
                            _this.set("actionForFindUtils", "getObjectUtils");
                        } else if (params.folderId) {
                            _this.set('folderId', params.folderId);
                            _this.set('actionForFindUtils', "getFolderUtils");
                        } else {
                            return;
                        }
                        _this.loadUtilsList();
                    }
                });
            },


            onNavigation: function (options) {
                this.trigger('NAVIGATION', options);
            },
            loadUtilsList: function () {
                var _this = this;
                this.set("utilInfoCollection", new UtilInfoCollection());
                this.set("utilCollection", new UtilCollection());
                this.set('initialized', true);
                //var thisUtilListBlockModel = this;
                $.when(this._loadInnerUtil()).then(function () {
                    log.debug("Loading Utils complete");
                    _this.trigger('render');
                });
            },


            _loadInnerUtil: function () {
                var _this = this;
                // Грузим информацию по утилитам
                return $.when(_this._loadUtilBlocksInfo())
                    .then(function (blockInfoList) {
                        blockInfoList.forEach(function (block) {
                            var utilBlockInfo = new UtilInfoModel({
                                id: block.id,
                                block: block,
                                bootJs: block.utilBootJs,
                                allData: block,
                                parent: _this,
                                params: _this.get('params')
                            });
                            _this.get('utilInfoCollection').add(utilBlockInfo);
                        });
                        // Созадем реальные блоки утилит
                        var utilInfoCollection = _this.get('utilInfoCollection');
                        var deferredArray = utilInfoCollection.map(function (blockInfo, i) {
                            return blockInfo.loadRealUtilBlock(_this)
                                .then(function (utilBlockModel) {
                                    _this.get('utilCollection').add(utilBlockModel);
                                    utilBlockModel.parent = _this;
                                    //TODO возможно не нужны
                                    _this.listenTo(utilBlockModel, 'NOTIFY_PAGE_BLOCKS', _this.onNotifyPageBlocks);
                                    _this.listenTo(utilBlockModel, 'NAVIGATION', _this.onNavigation);
                                });
                        });
                        return $.when.apply(null, deferredArray);
                    });
            },

            _loadUtilBlocksInfo: function () {
                var thisPage = this;
                log.debug('Start loading utils with method ' + this.get("actionForFindUtils"));
                var options = {
                    action: this.get("actionForFindUtils"),
                    data: {
                        action: this.get("actionForFindUtils"),
                        folderId: this.get('folderId'),
                        className: this.get('className'),
                        objectId: this.get('objectId'),
                        pageBlockId: this.get('blockInfo').get('id'),
                        currentPage: this.get('currentPage'),
                        ml_request: true
                    }
                };

                return this.callServerAction(options);
            },


            showModal:function(result){

                var dialog = new DialogPageBlock({
                    title: result.title,
                    message: result.content,
                    type: 'infoMessage'
                });

                dialog.show();
            }
        });

        return model;
    });
