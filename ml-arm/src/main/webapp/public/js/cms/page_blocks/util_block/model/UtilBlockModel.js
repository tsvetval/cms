define(
    [
        'log',
        'misc',
        'backbone',
        'cms/model/PageBlockModel',
        '../util/model/UtilInfoModel',
        '../util/collection/UtilInfoCollection',
        '../util/collection/UtilCollection',
        'cms/page_blocks/DialogPageBlock'
    ],
    function (log, misc, backbone, PageBlockModel, UtilInfoModel, UtilInfoCollection, UtilCollection, DialogPageBlock) {
        var model = PageBlockModel.extend({
            defaults: {
                utilInfoCollection: undefined,
                utilCollection: undefined,
                actionForFindUtils: undefined,
                initialized: false
            },
            initialize: function () {

                console.log("initialize UtilBlockModel");
                var _this = this;
                this.listenTo(this, 'openFolder', function (params) {
                    _this.set("params", params);
                    _this.set('folderId', params.folderId);
                    _this.set('initialized', false);
                    _this.set('actionForFindUtils', "getFolderUtils");
                    _this.loadUtilsListForFolder.apply(this, arguments);

                });
                this.listenTo(this, 'restorePage', function (params) {
                    if (!_this.get('initialized')) {
                        _this.set("params", params);
                        if (params.objectId) {
                            _this.set("objectId", params.objectId);
                            _this.set("className", params.className);
                            _this.set("actionForFindUtils", "getObjectUtils");
                        } else if (params.folderId) {
                            _this.set('folderId', params.folderId);
                            _this.set('actionForFindUtils', "getFolderUtils");
                        } else {
                            return;
                        }
                        _this.loadUtilsListForFolder.apply(this, arguments);
                    }
                });


            },
            loadInnerUtil: function () {
                var _this = this;
                $.when(_this._loadUtilBlocksInfo()).then(function () {
                    var utilInfoCollection = _this.get('utilInfoCollection');
                    var deferredArray = utilInfoCollection.map(function (blockInfo, i) {
                        return blockInfo.loadRealBlock(_this)
                            .then(function (pageBlockModel) {
                                _this.get('utilCollection').add(pageBlockModel);
                                pageBlockModel.parent = _this;
                                _this.listenTo(pageBlockModel, 'NOTIFY_PAGE_BLOCKS', _this.onNotifyPageBlocks);
                                _this.listenTo(pageBlockModel, 'NAVIGATION', _this.onNavigation);
                            });
                    });
                    $.when.apply(null, deferredArray);
                });
            },
            onNavigation: function (options) {
                this.trigger('NAVIGATION', options);
            },
            loadUtilsListForFolder: function () {
                this.set("utilInfoCollection", new UtilInfoCollection());
                this.set("utilCollection", new UtilCollection());
                this.set('initialized', true);
                var thisUtilListBlockModel = this;
                this.syncRenderTemplate().then(
                    function () {
                        thisUtilListBlockModel.triggerRender()
                    });

            },

            syncRenderTemplate: function () {
                var thisObjectUtilsListBlockModel = this;
                return $.ajax('page_block', {
                    type: 'POST',
                    data: {
                        action: 'show',
                        folderId: this.get('folderId'),
                        pageBlockId: this.get('blockInfo').get('id'),
                        currentPage: this.get('currentPage'),
                        objectId :  this.get('objectId'),
                        ml_request: true
                    },
                    success: function (result) {
                        thisObjectUtilsListBlockModel.set('renderTemplate', result.html);


                        thisObjectUtilsListBlockModel.loadInnerUtil();
                    }
                });
            },

            _loadUtilBlocksInfo: function (callback) {
                var thisPage = this;
                log.debug('Start loading PageBlockInfo from server');
                var options = {
                    action: this.get("actionForFindUtils"),
                    data:   {
                        action: this.get("actionForFindUtils"),
                        folderId: this.get('folderId'),
                        className: this.get('className'),
                        objectId :  this.get('objectId'),
                        pageBlockId: this.get('blockInfo').get('id'),
                        currentPage: this.get('currentPage'),
                        ml_request: true
                    }
                };

                var callbackRequest = function (result) {
                    result.forEach(function (block) {
                        var pageBlockInfo = new UtilInfoModel({
                            id: block.id,
                            block: block,
                            bootJs: block.utilBootJs,
                            allData: block,
                            parent: thisPage,
                            params: thisPage.get('params')
                        });
                        thisPage.get('utilInfoCollection').add(pageBlockInfo);
                    });
                    thisPage.set('initialized', true);
                    if (callback) callback();
                };

                return this.callServerAction(options, callbackRequest);

/*


                return $.ajax('page_block', {
                    type: 'POST',
                    data: {
                        action: this.get("actionForFindUtils"),
                        folderId: this.get('folderId'),
                        className: this.get('className'),
                        objectId :  this.get('objectId'),
                        pageBlockId: this.get('blockInfo').get('id'),
                        currentPage: this.get('currentPage'),
                        ml_request: true
                    },
                    success: function (result) {

                        log.debug('Finish loading PageBlockInfo from server');
                        result.forEach(function (block) {
                            var pageBlockInfo = new UtilInfoModel({
                                id: block.id,
                                block: block,
                                bootJs: 'cms/page_blocks/util_block/util/utilBoot',
                                parent: thisPage,
                                params: thisPage.get('params')
                            });
                            thisPage.get('utilInfoCollection').add(pageBlockInfo);
                        });
                        thisPage.set('initialized', true);
                        if (callback) callback();
                    }
                });*/

            },

            triggerRender: function () {
                this.trigger('render');
            } ,

            showModal:function(result){

                var dialog = new DialogPageBlock({
                    title: result.title,
                    message: result.content,
                    type: 'infoMessage'
                });

                dialog.show();
            }


        });

        return model;
    });
