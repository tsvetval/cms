define(
    ['log', 'misc', 'backbone', './../model/UtilModel'],
    function (log, misc, backbone, UtilModel) {
        var UtilCollection = backbone.Collection.extend({
            model: UtilModel,

            checkAllInitialized : function(){
                var notInitialized = this.findWhere({initialized: false});
                return !notInitialized;
            },

            notifyPageBlocks : function(objectEvent){
                //TODO check pageBlock id's
                this.forEach(function(pageBlock){
                    pageBlock.trigger(objectEvent.triggerEvent, objectEvent.options);
                });
            }
        });


        return UtilCollection;
    });