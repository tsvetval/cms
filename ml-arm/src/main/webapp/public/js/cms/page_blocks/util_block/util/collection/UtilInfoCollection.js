define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockInfoModel'],
    function (log, misc, backbone, PageBlockInfoModel) {
        var UtilInfoCollection = backbone.Collection.extend({
            model: PageBlockInfoModel,

            checkAllInitialized : function(){
                var notInitialized = this.findWhere({initialized: false});
                return !notInitialized;
            }

        });

        return UtilInfoCollection;
    });
