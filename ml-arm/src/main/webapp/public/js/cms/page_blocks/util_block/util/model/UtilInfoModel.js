define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockModel'],
    function (log, misc, backbone, PageBlockModel) {
        var model = PageBlockModel.extend({
            defaults:      {
                orderNum:     undefined,
                buttonLabel:  undefined,
                buttonImgUrl: undefined,
                method:       undefined,
                openType:     undefined,
                utilId:       undefined,
                url:          undefined,
                allData:      undefined
            },
            initialize:    function (option) {
                console.log("initialize UtilInfoModel");
                this.orderNum = option.block.orderNumber;
                this.buttonLabel = option.block.buttonLabel;
                this.buttonImgUrl = option.block.buttonImgUrl;
                this.buttonIcon = option.block.buttonIcon;
                this.url = option.block.url;
                this.method = option.block.method;
                if(option.block.openType){
                    this.openType = option.block.openType.code;
                }
                this.confirmExec = option.block.confirmExec;
                this.confirmExecMsg = option.block.confirmExecMsg;
                this.utilId = option.block.utilId;
                this.allData = option.block.allData;
            },
            /**
             * Производит загрузку реальныой(кастомной) модели и вью для контент блока
             * @return возвращает promise вызов промиса пойдет с this(PageBlockInfo)
             */
            loadRealBlock: function (blockModel) {
                var thisPageBlockInfo = this;
                var def = new $.Deferred();
                log.debug("Load bootBlockJs = " + thisPageBlockInfo.get('bootJs'));
                require([thisPageBlockInfo.get('bootJs')], function (bootClasses) {
                    thisPageBlockInfo.set('backBoneClasses', bootClasses);
                    var pageBlockModel = new bootClasses.model(
                        {
                            blockModel: blockModel,
                            blockInfo:  thisPageBlockInfo
                        }
                    );
                    log.debug('Create new RealPage Block Model');
                    def.resolve(pageBlockModel);
                });
                return def.promise();
            }

        });

        return model;
    });