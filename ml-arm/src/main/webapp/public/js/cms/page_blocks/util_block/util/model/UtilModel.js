define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockModel', 'markup', 'cms/page_blocks/DialogPageBlock', 'cms/events/NotifyPageBlocksEventObject'],
    function (log, misc, backbone, PageBlockModel, markup, Message, NotifyEventObject) {
        var model = PageBlockModel.extend({
            defaults: {
                parent: undefined
//            guid : undefined,
//            zone : undefined,
//            orderNum : undefined,
//            name: "Empty page",
//            url : undefined,
//            template : undefined
            },
            initialize: function () {
                console.log("initialize UtilModel");
                var _this = this;
                _this.triggerRender.apply(this, arguments);

            },

            triggerRender: function () {
                this.renderPageBlock(this);
                this.trigger('render');
            },
            /**
             * Производит создание контейнера и загрузку контент блока
             * @param blockInfo
             * @return {*} differed объект (pageBlockInfo в качетве параметра)
             */
            renderPageBlock: function (block) {
                var thisPageView = this;
                // Для каждого пейдж блока создаем контейнер и добавляем к элементу
                var $blockContainer = $('<span class="span4" style="margin: 1px"/>', {id: block.get('blockInfo').get('guid')});
                $blockContainer.appendTo($('#UtilsPageBlock_' + block.get('blockInfo').get('parent').get('blockInfo').id));
                //грузим блок
                var pageBlockViewClass = block.get('blockInfo').get('backBoneClasses').view;
                var pageBlockView = new pageBlockViewClass({model: block});
                //thisPageView.listenTo(pageBlockView, 'all', thisPageView.proxyEventFromPageBlock);
                //thisPageView.pageBlockViews.push(pageBlockView);

                pageBlockView.setElement($blockContainer);
                pageBlockView.render();

            },
            openURL: function (data) {
                var utilInfo = this.get('blockInfo');
                var _this = this;

                function exec() {
                    if (utilInfo.method == 'GET') {
                        if (utilInfo.openType == 'CRAMB') {
                            _this.parent.openPage(data.url, 'Просмотр объекта ...', _this.get('blockInfo').get('params'));
                        } else if (utilInfo.openType == 'TAB') {
                            window.open(misc.getContextPath() + data.url, '_blank');
                        } else if (utilInfo.openType == 'WINDOW') {
                            window.open(misc.getContextPath() + data.url, '_blank', 'toolbar=0,location=0,menubar=0');
                        }
                    } else {
                        _this.fillDataForExec(data)
                    }
                }

                if (utilInfo.confirmExec) {
                    var dialog = new Message({
                        title: 'Подтверждение',
                        message: utilInfo.confirmExecMsg,
                        type: 'dialogMessage'
                    });
                    dialog.show({
                        okAction: exec
                    });
                }
                else {
                    exec();
                }
            },
            execUtil: function (data) {
                var _this = this;
                var params = _this.get("blockInfo").get('params');
                params.utilId = this.get('blockInfo').get('id');
                params.pageBlockId = _this.get('blockModel').get('blockInfo').get('id');
                params.ml_request = true;

                $.extend(params, data);

                var callback = function (result) {
                    _this.showResult(result);
                };

                var url = misc.getContextPath() + this.get("blockInfo").url;
                var requestData = params;
                var def = new $.Deferred();
                $.ajax(url, {
                    type: 'POST',
                    data: requestData,
                    success: function (responseData) {
                        if (responseData.error == 'server') {
                            console.log("Server Error: " + responseData.message);
                            if (responseData.stacktrace) {
                                console.log("Stack Trace: " + responseData.stacktrace);
                            } else {
                                responseData.stacktrace = "Дополнительные сведения отсутствуют."
                            }
                            _this.displayExtendedErrorMessage("Ошибка сервера", responseData.message, responseData.stacktrace);
                        } else if (responseData.error == 'application') {
                            console.log("Application Error: " + responseData.message);
                            _this.displayErrorMessage("Ошибка приложения", responseData.message);
                        } else if (responseData.error == 'security') {
                            console.log("Security Error: " + responseData.message);
                            _this.displayErrorMessage("Ошибка доступа", responseData.message);
                        } else {
                            if (responseData.needAuth) {
                                window.location = responseData.url;
                            } else {
                                if (callback) callback.apply(_this, [responseData, requestData]);
                                def.resolve(responseData);
                            }
                        }
                    },
                    error: function (result, textStatus, errorThrown) {
                        log.error('Error while exec util, params:' + JSON.stringify(params),
                            "\nStatus: " + textStatus +
                            "\nError: " + errorThrown);
                    }
                });
            },

            fillDataForExec: function (data) {
                var _this = this;
                 //TODO rewrite this
                var pbData = [];
                this.parent.notifyPageBlocks(new NotifyEventObject('getSelectedIds', {
                        callback: function (param) {
                            pbData.push(JSON.stringify(param))
                            //data.blockData = JSON.stringify(param);
                            //_this.execUtil(data);
                        }
                    }, null, null, function onFinish() {
                        data.blockData = JSON.stringify(pbData);
                        _this.execUtil(data);
                    }
                ));
            },


            execUtilWizard: function (data) {
                var _this = this;
                var params = _this.get("blockInfo").get('params');
                $.extend(params, data);

                var options = {
                    action: data.action,
                    data: params
                };
                var callback = function (result) {
                    _this.showResult(result);
                };

                return this.callServerAction(options, callback);
            },

            execComboBoxUtil: function (data) {
                var _this = this;
                var param = _this.get("blockInfo").get('params');
                var options = {
                    action: $('#' + data.comboId).val(),
                    data: param
                };
                var callback = function (result) {
                    _this.showResult(result);
                };

                return this.callServerAction(options, callback);
            },
            showResult: function (result) {
                if (result.showType) {
                    if (result.showType == 'modal') {
                        this.showModal(result);
                    } else if (result.showType == 'execJs') {
                        this[result.functionName](result);
                    }
                }
                else if (result.downloadLink) {
                    window.location = result.url;
                }
            },
            showModal: function (result) {
                this.parent.showModal(result);
            },

            openHistory: function (result) {
                this.navigationEvent({
                    "do": 'push',
                    "title": 'Просмотр истории объекта ...',
                    "url": '/history',
                    "params": result.params
                });
            }

        });

        return model;
    });