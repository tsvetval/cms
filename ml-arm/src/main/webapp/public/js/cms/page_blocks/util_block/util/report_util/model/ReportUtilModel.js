define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockModel', 'markup', 'cms/page_blocks/DialogPageBlock', 'cms/events/NotifyPageBlocksEventObject'],
    function (log, misc, backbone, PageBlockModel, markup, Message, NotifyEventObject) {
        var model = PageBlockModel.extend({
            defaults: {
                parent: undefined
//            guid : undefined,
//            zone : undefined,
//            orderNum : undefined,
//            name: "Empty page",
//            url : undefined,
//            template : undefined
            },
            initialize: function () {
                console.log("initialize ReportUtilModel");
                var _this = this;
                _this.triggerRender.apply(this, arguments);

            },

            triggerRender: function () {
                this.renderPageBlock(this);
                this.trigger('render');
            },
            /**
             * Производит создание контейнера и загрузку контент блока
             * @param blockInfo
             * @return {*} differed объект (pageBlockInfo в качетве параметра)
             */
            renderPageBlock: function (block) {
                var thisPageView = this;
                // Для каждого пейдж блока создаем контейнер и добавляем к элементу
                var $blockContainer = $('<span class="span4" style="margin: 1px"/>', {id: block.get('blockInfo').get('guid')});
                $blockContainer.appendTo($('#UtilsPageBlock_' + block.get('blockInfo').get('parent').get('blockInfo').id));
                //грузим блок
                var pageBlockViewClass = block.get('blockInfo').get('backBoneClasses').view;
                var pageBlockView = new pageBlockViewClass({model: block});
                //thisPageView.listenTo(pageBlockView, 'all', thisPageView.proxyEventFromPageBlock);
                //thisPageView.pageBlockViews.push(pageBlockView);

                pageBlockView.setElement($blockContainer);
                pageBlockView.render();

            },
            openReport: function(objectId){
                var utilInfo = this.get('blockInfo');
                if (utilInfo.openType == 'CRAMB') {
                    this.parent.openPage(utilInfo.url, 'Просмотр объекта ...', {objectId: objectId});
                } else if (utilInfo.openType == 'TAB') {
                    window.open(misc.getContextPath() + utilInfo.url, '_blank');
                } else if (utilInfo.openType == 'WINDOW') {
                    window.open(misc.getContextPath() + utilInfo.url, '_blank', 'toolbar=0,location=0,menubar=0');
                }
            }

        });

        return model;
    });