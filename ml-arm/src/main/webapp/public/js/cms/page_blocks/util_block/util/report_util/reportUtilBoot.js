define(
    ['log', 'misc', 'backbone', './model/ReportUtilModel', './view/ReportUtilView'],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });
