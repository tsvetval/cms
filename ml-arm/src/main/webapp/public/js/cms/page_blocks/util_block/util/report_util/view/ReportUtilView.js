define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup'],
    function (log, misc, backbone, PageBlockView, markup) {
        var view = PageBlockView.extend({

                events: {
                    "click .open-report": "openReport"
                },

                initialize: function () {
                    console.log("initialize UtilView");
                    this.listenTo(this.model, 'render', this.render)
                },
                render:     function () {
                    var thisUtilBlockView = this;
                    var utilInfo = this.model.get('blockInfo');
                    if(utilInfo.get('allData').reports.length>1){
                        var html = "<div class='btn-group'>";
                        html += "<button type='button' class='btn btn-primary btn-mini dropdown-toggle callback' data-toggle='dropdown' aria-expanded='false' >";
                        html += utilInfo.buttonLabel;
                        html += "<span class='caret'></span> </button>";
                        html += "<ul class='dropdown-menu' role='menu'>";
                        utilInfo.get('allData').reports.forEach(function(report){
                            html += "<li><a class='open-report' objectId='"+report.id+"' href='#'>"+report.name+"</a></li>";
                        });
                        html += "</ul></div>";
                        this.$el.html(html);
                    }else{
                        var html = "<button type='button' objectId='"+utilInfo.get('allData').reports[0].id+"' class='open-report btn btn-primary btn-mini callback' >";
                        html += utilInfo.buttonLabel;
                        html += "</button>";
                        this.$el.html(html);
                    }
                },
                openReport: function(event){
                    this.model.openReport(event.target.getAttribute('objectId'));
                }

            }
        );

        return view;
    });
