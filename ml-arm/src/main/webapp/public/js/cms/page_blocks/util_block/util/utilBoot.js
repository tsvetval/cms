define(
    ['log', 'misc', 'backbone', './model/UtilModel', './view/UtilView'],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });
