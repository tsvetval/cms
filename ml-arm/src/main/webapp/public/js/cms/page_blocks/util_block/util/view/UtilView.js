define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup'],
    function (log, misc, backbone, PageBlockView, markup) {
        var view = PageBlockView.extend({
                initialize: function () {
                    console.log("initialize UtilView");
                    this.listenTo(this.model, 'render', this.render)
                },
                render:     function () {
                    var thisUtilBlockView = this;
                    var utilInfo = this.model.get('blockInfo');
                    var html = "<button class='btn btn-primary btn-mini callback' ";
                    if (utilInfo.url) {
                        html += "click-action='openUrl' click-params='{\"url\":\"" + utilInfo.url + "\"}'>";
                    }
                    if (utilInfo.buttonImgUrl) {
                        html += "<img src='" + utilInfo.buttonImgUrl + "'/>";
                    }
                    if (utilInfo.buttonIcon) {
                        html += "<span class='" + utilInfo.buttonIcon + "'></span> ";
                    }
                    html += utilInfo.buttonLabel + '</button>';
                    this.$el.html(html);


                    markup.attachActions(this.$el, {
                            execUtil:         function () {
                                thisUtilBlockView.model.fillDataForExec();
                            },
                            execUtilWizard:   function (data) {
                                thisUtilBlockView.model.execUtilWizard(data);
                            },
                            execComboBoxUtil: function (data) {
                                thisUtilBlockView.model.execComboBoxUtil(data);
                            },
                            openUrl:          function (data) {
                                thisUtilBlockView.model.openURL(data);
                            }
                        }
                    );
                }
            }
        );

        return view;
    });
