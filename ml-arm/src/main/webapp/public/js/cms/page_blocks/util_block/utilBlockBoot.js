/**
 * Блок утилит. Представляет из себя pageBlock с утилитами(кнопками)
 * Контроллер: ru.peak.ml.web.block.controller.impl.UtilsPageBlockController
 * */
define(
    ['log', 'misc', 'backbone', 'cms/page_blocks/util_block/model/UtilBlockModel', 'cms/page_blocks/util_block/view/UtilBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });
