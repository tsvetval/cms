define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView'],
    function (log, misc, backbone, PageBlockView) {
        var view = PageBlockView.extend({
            initialize: function(){
                console.log("initialize UtilBlockView");
                this.listenTo(this.model, 'render', this.render)
            },
            render:function () {
                var thisObjectListBlockView = this;
                this.$el.html(this.model.get('renderTemplate'));

            }


            });

        return view;
    });
