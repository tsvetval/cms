define(
    ['jquery', 'log', 'misc', 'backbone', 'cms/view/PageBlockView',
        'text!cms/page_blocks/util_block/template/utilInitTemplate.tpl',
        'text!cms/page_blocks/util_block/template/utilContainer.tpl'
    ],
    function ($, log, misc, backbone, PageBlockView, initTemplate, utilContainerTemplate) {
        var view = PageBlockView.extend({
            initialize: function () {
                console.log("initialize UtilBlockView");
                this.listenTo(this.model, 'render', this.render)
            },
            render: function () {
                var _this = this;

                if (this.model.get('utilCollection') && this.model.get('utilCollection').length > 0) {
                    this.$el.html(_.template(initTemplate, {}));
                    this.$container = this.$el.find('.dropdown-menu');
                    this.model.get('utilCollection').forEach(function (utilModel) {
                            var $utilContainer =  $(_.template(utilContainerTemplate, {}));
                            _this.$container.append($utilContainer);
                            //var view = utilModel.blockInfo.backBoneClasses.view({model : utilModel});
                            //view.setElement($utilContainer);
                            utilModel.renderUtil($utilContainer);
                        }
                    );
                }
            }


        });

        return view;
    });
