define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockModel', 'cms/page_blocks/DialogPageBlock', 'jszip', 'filesaver',
        'text!cms/page_blocks/wizard_block/template/JavaControllerTemplate.tpl.java',
        'text!cms/page_blocks/wizard_block/template/JSBootTemplate.tpl.js',
        'text!cms/page_blocks/wizard_block/template/JSModelTemplate.tpl.js',
        'text!cms/page_blocks/wizard_block/template/JSViewTemplate.tpl.js'
    ],
    function (log, misc, backbone, PageBlockModel, Message, JSZip, FileSaver,
              JavaControllerTemplate,
              JSBootTemplate,
              JSModelTemplate,
              JSViewTemplate) {
        var model = PageBlockModel.extend({
            defaults: {
                objectId: undefined,
                className: undefined,

                jsBootFilename: undefined,
                jsModelFilename: undefined,
                jsViewFilename: undefined,
                javaControllerFilename: undefined,
                javaClass: undefined,

                jsModelRequirePath: undefined,
                jsViewRequirePath: undefined
            },

            initialize: function () {
                var _this = this;
                console.log("initialize BlockModel");

                this.listenTo(this, 'restorePage', function (params) {
                    _this.set("objectId", params.objectId);
                    _this.set("className", params.className);
                    _this.prompt();
                });
            },

            prompt: function () {
                var _this = this;
                var objectId = this.get('objectId');
                var className = this.get('className');
                var options = {
                    action: 'getPageBlockInfo',
                    data: {
                        objectId: objectId,
                        className: className,
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    }
                };
                return this.callServerAction(options).then(function (result) {

                    var jsBootFilename = result.jsBootFilename;
                    var jsModelFilename = jsBootFilename.substring(0, jsBootFilename.lastIndexOf("/")) + '/model/' + _this.capitalizeFirstLetter(jsBootFilename.substring(jsBootFilename.lastIndexOf("/") + 1, jsBootFilename.length).replace('Boot', 'Model'));
                    var jsViewFilename = jsBootFilename.substring(0, jsBootFilename.lastIndexOf("/")) + '/view/' + _this.capitalizeFirstLetter(jsBootFilename.substring(jsBootFilename.lastIndexOf("/") + 1, jsBootFilename.length).replace('Boot', 'View'));
                    var jsModelRequirePath = jsModelFilename.replace(".js", "");
                    var jsViewRequirePath = jsViewFilename.replace(".js", "");
                    var javaControllerFilename = result.javaControllerFilename;
                    var javaClass = javaControllerFilename.substring(javaControllerFilename.lastIndexOf("/") + 1, javaControllerFilename.lastIndexOf("."));
                    _this.set("jsBootFilename", jsBootFilename);
                    _this.set("jsModelFilename", jsModelFilename);
                    _this.set("jsViewFilename", jsViewFilename);
                    _this.set("jsModelRequirePath", jsModelRequirePath);
                    _this.set("jsViewRequirePath", jsViewRequirePath);
                    _this.set("javaControllerFilename", javaControllerFilename);
                    _this.set("javaClass", javaClass);

                    _this.set("jsBootFilenameShort", jsBootFilename.substr(jsBootFilename.lastIndexOf("/") + 1));
                    _this.set("jsModelFilenameShort", jsModelFilename.substr(jsModelFilename.lastIndexOf("/") + 1));
                    _this.set("jsViewFilenameShort", jsViewFilename.substr(jsViewFilename.lastIndexOf("/") + 1));
                    _this.set("javaControllerFilenameShort", javaControllerFilename.substr(javaControllerFilename.lastIndexOf("/") + 1));

                    _this.trigger('render');
                });
            },

            download: function (filename, filetype) {
                var _this = this;
                var javaControllerFilename = _this.get("javaControllerFilename");
                var jsBootFilename = "public/js/" + _this.get("jsBootFilename");
                var jsModelFilename = "public/js/" + _this.get("jsModelFilename");
                var jsViewFilename = "public/js/" + _this.get("jsViewFilename");

                if (!filename) {
                    var zip = new JSZip();

                    var javaControllerFile = zip.file(javaControllerFilename, _.template(JavaControllerTemplate, {model: _this}));
                    var jsBootFile = zip.file(jsBootFilename, _.template(JSBootTemplate, {model: _this}));
                    var jsModelFile = zip.file(jsModelFilename, _.template(JSModelTemplate, {model: _this}));
                    var jsViewFile = zip.file(jsViewFilename, _.template(JSViewTemplate, {model: _this}));

                    var content = zip.generate({type: "blob"});
                    saveAs(content, "template.zip");
                } else {
                    var content;
                    if (filetype == 'controller') {
                        content = _.template(JavaControllerTemplate, {model: _this})
                    } else if (filetype = 'boot') {
                        content = _.template(JSBootTemplate, {model: _this})
                    } else if (filetype = 'model') {
                        content = _.template(JSModelTemplate, {model: _this})
                    } else if (filetype = 'view') {
                        content = _.template(JSViewTemplate, {model: _this})
                    }

                    var blob = new Blob([content], {type: "text/plain;charset=utf-8"});
                    saveAs(blob, filename);
                }
            },

            capitalizeFirstLetter: function (string) {
                return string.charAt(0).toUpperCase() + string.slice(1);
            }
        });
        return model;
    });
