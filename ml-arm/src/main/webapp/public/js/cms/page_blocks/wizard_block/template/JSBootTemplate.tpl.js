define(
    ['log', 'misc', 'backbone',
        '<%=model.get("jsModelRequirePath")%>',
        '<%=model.get("jsViewRequirePath")%>'
    ],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
