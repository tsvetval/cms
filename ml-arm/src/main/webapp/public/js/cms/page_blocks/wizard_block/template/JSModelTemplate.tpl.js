define(
    ['log', 'misc', 'backbone', 'cms/model/PageBlockModel', 'cms/page_blocks/DialogPageBlock'],
    function (log, misc, backbone, PageBlockModel, Message) {
        var model = PageBlockModel.extend({

            defaults: {
                clientData: undefined,
                serverData: undefined,
                renderTemplate: undefined
            },

            initialize: function () {
                console.log("initialize BlockModel");
                var _this = this;
                this.listenTo(this, 'restorePage', function (params) {
                    var clientData = params.clientData;
                    _this.set("clientData", clientData);

                    //это только, если используется шаблонизатор Play
                    //в этом случае мы отправляем запрос на сервер, чтобы получить шаблон (renderTemplate)
                    //и после этого вызываем событие 'render'
                    //если используется rest интерфейс, то шаблон можно загрузить локально
                    //в методе render представления через require.text
                    //с помощбю underscore (_.template()) можно проставить в нем переменные
                    this.prompt(clientData);
                });
            },

            prompt: function (clientData) {
                var _this = this;
                var options = {
                    action: "show",
                    data: {
                        clientData: clientData
                    }
                };

                var callback = function (result) {
                    _this.set('renderTemplate', result.html);
                    _this.trigger('render');
                };

                return this.callServerAction(options, callback);
            },

            action: function (param) {
                var _this = this;
                var options = {
                    action: "someAction",
                    data: {
                        param: param
                    }
                };

                var callback = function (result) {
                    _this.set("serverData", result.serverData);
                };

                return this.callServerAction(options, callback);
            }
        });
        return model;
    });
