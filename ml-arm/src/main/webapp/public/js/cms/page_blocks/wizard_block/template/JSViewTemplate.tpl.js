define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup', 'cms/page_blocks/DialogPageBlock', 'moment'],
    function (log, misc, backbone, PageBlockView, markup, Message) {
        var view = PageBlockView.extend({

            events: {
                "click .update-button": "updateClick"
            },

            initialize: function () {
                console.log("initialize BlockView");
                this.listenTo(this.model, 'render', this.render);
                this.listenTo(this.model, 'change:serverData', this.update);
            },

            render: function () {
                this.$el.html(this.model.get('renderTemplate'));
            },

            updateClick: function (e) {
                var param = $(e.currentTarget).data('param');
                //<a class="update-button" data-param="someValue">
                this.model.update(param);
            },

            update: function () {
                this.$el.find(".server-data-container").html(this.model.get("serverData"));
            }

        });
        return view;
    });
