package ru.peak.ml.web.block.controller.impl;

import com.google.inject.Inject;
import ru.peak.ml.web.service.annotations.PageBlockAction;

public class<%=model.get('javaClass')%>implements BlockController{
        private static final Logger log=LoggerFactory.getLogger(<%=model.get('javaClass')%>.class);

        @Inject
        CommonDao commonDao;

        @Inject
        MetaDataHolder metaDataHolder;

        private static final String TEMPLATE_PATH="path/to/template/file.hml";


        public void serve(MlHttpServletRequest params,MlPageBlockBase mlInstance,MlHttpServletResponse resp)throws MlApplicationException,MlServerException{
                log.error(String.format("Unknown action: [%s] in class [%s] ",params.getString("action"),this.getClass()));
                throw new MlApplicationException(String.format("В контроллере %s Данный функционал (%s) пока не реализован",this.getClass(),params.getString("action")));
        }

        @PageBlockAction(action = "show")
        public void show(MlHttpServletRequest params,MlHttpServletResponse resp,MlPageBlockBase mlInstance){
                String param;
                String serverData;
                if(params.hasString("clientData")){
                        String clientData=params.getString("clientData");
                        serverData=getDataByClientData(clientData);
                }else{
                        throw new MlApplicationException("Некорректные параметры запроса, в запросе должны присутствовать [clientData]");
                }

                //это только, если используется шаблонизатор Play
                Map<String, Object>data=new HashMap<>();
                data.put("serverData",serverData);
                resp.renderTemplateToJson(TEMPLATE_PATH,data);
                //================================================

                resp.setDataType(MlHttpServletResponse.DataType.JSON);
                resp.addDataToJson("serverData",new JsonPrimitive(serverData));
        }

        @PageBlockAction(action = "someAction")
        public void someAction(MlHttpServletRequest params,MlHttpServletResponse resp,MlPageBlockBase mlInstance){
                String param;
                String data;
                if(params.hasString("param")){
                        param=params.getString("param");
                        data=getDataByParam(param);
                }else{
                        throw new MlApplicationException("Некорректные параметры запроса, в запросе должны присутствовать [param]");
                }
                resp.setDataType(MlHttpServletResponse.DataType.JSON);
                resp.addDataToJson("serverData",new JsonPrimitive(data));
        }

        public String getDataByClientData(String clientData){
                return"someData";
        }

        public String getDataByParam(String param){
                return"someData";
        }
}

