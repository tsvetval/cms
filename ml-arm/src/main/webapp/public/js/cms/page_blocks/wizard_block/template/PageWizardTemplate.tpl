<style>
    tr.download {
        cursor: pointer;
    }
</style>

<div class="container">
    <h2>Файлы шаблонов страничного блока:</h2>
    <table class="table table-hover">
        <tr class="download" data-filename="<%=model.get('javaControllerFilenameShort')%>" data-filetype="controller">
            <td>Контроллер блока:</td>
            <td><%=model.get("javaControllerFilename")%></td>
        </tr>
        <tr class="download" data-filename="<%=model.get('jsBootFilenameShort')%>" data-filetype="boot">
            <td>Загрузочный модуль:</td>
            <td><%=model.get("jsBootFilename")%></td>
        </tr>
        <tr class="download" data-filename="<%=model.get('jsModelFilenameShort')%>" data-filetype="model">
            <td>Модель:</td>
            <td><%=model.get("jsModelFilename")%></td>
        </tr>
        <tr class="download" data-filename="<%=model.get('jsViewFilenameShort')%>" data-filetype="view">
            <td>Представление:</td>
            <td><%=model.get("jsViewFilename")%></td>
        </tr>
    </table>

    <div class="btn btn-primary download">Скачать одним архивом</div>
</div>


