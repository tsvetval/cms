define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup', 'cms/page_blocks/DialogPageBlock', 'moment',
        'text!cms/page_blocks/wizard_block/template/PageWizardTemplate.tpl',],
    function (log, misc, backbone, PageBlockView, markup, Message, moment,
              PageWizardTemplate) {

        var view = PageBlockView.extend({

            events: {
                "click .download": "downloadClick"
            },

            initialize: function () {
                console.log("initialize BlockView");
                this.listenTo(this.model, 'render', this.render);
                this.listenTo(this.model, 'change:data', this.update);
            },

            render: function () {
                var _this = this;
                this.$el.html(_.template(PageWizardTemplate, {model: this.model}));
                return true;
            },

            downloadClick: function (e) {
                var filename = $(e.currentTarget).data('filename');
                var filetype = $(e.currentTarget).data('filetype');
                this.model.download(filename, filetype);
            }

        });
        return view;
    });
