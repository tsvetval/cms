/*global define*/
define([
    'jquery',
    'log',
    'misc',
    'backbone',
    'underscore'

], function ($, log, misc, Backbone, _) {
    /*    'use strict';*/

    var CommonRouter = Backbone.Router.extend({
        routes:      {
            '*args': 'onRoute'
        },
        contextPath: misc.getContextPath(),
        initialize:  function (options) {
            log.debug('Initialize CommonRouter');

            this.siteModel = misc.option(options, "siteModel", "Модель сайта, необходима для отображения новых страниц");
            /*
             this.listenTo(this.siteModel, 'NAVIGATION', this.navigation);
             */

            // Роутер не перехватывает изменения хеша, нужно перехыватывать их вручную и отправлять на обработку
            var _this = this;
//            $(window).bind('hashchange', function () {
//                _this.onRoute(window.location.pathname + window.location.hash);
//            });

            window.addEventListener('popstate', function (e) {
                log.debug('Handle popstate event (Back or Forward button)')
                _this.onRoute(window.location.pathname + window.location.hash);
                ///alert(e.state.path + e.state.note);
            }, false);

            /*
             $(window).unload(function () {
             alert('Handler for .unload() called.');
             window.history.clean();
             var currentState = window.history.state;
             //window.history.replaceState({pageGUID : currentState.pageGUID, doRefresh : true}, null, newUrl);

             });
             */
//            window.onbeforeunload = function(){alert('111')};
            //this.HandleBackFunctionality;

            window.onbeforeunload = function (evt) {
                log.debug('Handle onbeforeunload event (refresh button)');

                var historyState = window.history.state;
                window.history.replaceState(_.extend({doRefresh: true}, historyState), null, window.location.href);
            };

        },

        /*
         * Функция для перехвата всех изменений урла страницы браузера
         */
        onRoute:     function (path) {
            var historyState = window.history.state;
            if (historyState && historyState.pageGUID && !historyState.doRefresh) {
                // Это значит что сработало вперед/назад
                page = this.siteModel.getPageByGUID(historyState.pageGUID);
                if (!page) {
                    window.history.go(-1);
                } else {
                    var params = {
                        parsedHash: this.parsePath(window.location.hash).params,
                        skipPushState: true
                    };
                    this.siteModel.setActivePage(page, params);
                }
            } else {
                // Сработал либо рефреш либо новая страница
                var requestParams = this.parsePath(path + window.location.hash);
                log.debug('Router.onRoute with path = ' + requestParams.page + ' and Hash = ' + window.location.hash);
                // Ищем путь среди уже существующих крошек
                var page = this.siteModel.getPageByPath(requestParams.page, requestParams.params);
                if (page) {
                    this.siteModel.setActivePage(page);
                } else {
                    //Парсим хеш

                    this.siteModel.addPage(requestParams.page, requestParams.params);
                }
            }

            return;

        },

        /*
         openPage: function(path) {
         log.debug('Router.openPage with path = ' + path);

         // Ищем страницу среди уже загруженых, если найдена - показываем
         var page = this.siteModel.getPageByPath(path);
         if (page){
         this.siteModel.setActivePage(page, this.parsePath(path).params);
         }
         // ... если страница не найдена - запрашиваем с сервера и показываем
         else {
         var parsed = this.parsePath(path);
         this.siteModel.addPage(parsed.page, parsed.params);
         var breadcrumbs = this.siteModel.get("breadcrumbs").collection;
         var active = breadcrumbs.findActive();
         active.set("page", page);
         }
         },
         */



        /*
         Utils
         */
        parsePath:   function (url) {

            // Remove firth char of context path
            var contextPath = misc.getContextPath();
            contextPath = contextPath.charAt(0) == '/' ? contextPath.substr(1) : contextPath;

            var path = url;
            path = path.indexOf('/' + contextPath) == 0 ? path.substring(('/' + contextPath).length) : path;
            path = path.indexOf(contextPath) == 0 ? path.substring(contextPath.length) : path;
            path = path.indexOf("#") >= 0 ? path.substring(0, path.indexOf("#")) : path;

            var params = {};
            var tmpHash = url.indexOf("#") >= 0 ? url.substring(url.indexOf("#") + 1) : undefined;
            try {
                params = tmpHash ? JSON.parse(decodeURIComponent(tmpHash)) : {};
            } catch (e) {
                log.debug("ERROR: could not parse hash as json Hash = " + tmpHash);
                log.debug("try to eval hash = " + tmpHash);
                try {
                    eval('var tmpHashEval = ' + tmpHash);
                    if (tmpHashEval){
                        log.debug("Evaluation success new JSONString = " + JSON.stringify(tmpHashEval));
                        params = tmpHashEval;
                    } else {
                        log.debug("Evaluation fail");
                    }
                } catch (e){
                    log.debug("ERROR: could not eval hash as json Hash = " + tmpHash);
                }
            }

            return {
                page:   path,
                params: params
            }
        },

        HandleBackFunctionality: function HandleBackFunctionality() {
            if (window.event) {
                if (window.event.clientX < 40 && window.event.clientY < 0) {
                    alert('Browser back button is clicked…');
                }
                else {
                    alert('Browser refresh button is clicked…');
                }
            }
            else {
                if (event.currentTarget.performance.navigation.type == 1) {
                    alert('Browser refresh button is clicked…');
                }
                if (event.currentTarget.performance.navigation.type == 2) {
                    alert('Browser back button is clicked…');
                }
            }
        }
    });

    return CommonRouter;
});