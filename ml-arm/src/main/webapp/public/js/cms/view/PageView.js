define([
    // Libs
    'backbone',
    'log'
    // Deps

], function (Backbone, log) {

    var PageView = Backbone.View.extend({
        // pageBlockViews: [],

        initialize: function () {

            log.debug('Initialize PageView');
//            _.bindAll(this);
//            this.collection.on('reset', this.renderAll);
            this.listenTo(this.model,"renderPageBlock", this.renderPageBlock);
            this.listenTo(this.model,"showActiveBlocks", this.showActiveBlocks);

        },

        renderPage:              function (callback) {
            var thisPageView = this;
            thisPageView.renderStructureTemplate();
            this.renderAllPageBlocks().then(function () {
                callback(thisPageView)
            });
        },
        /*
         * Производит рендеринг структурного шаблона
         * */
        renderStructureTemplate: function () {
            log.debug('Render StructureTemplate');
            this.$el.html(this.model.get('structureTemplate'));
        },
        /**
         * По заданному списку pageBlockInfoCollection производит их загрузку и последующий
         * рендеринг в элементах вью
         * @return {*} возвращает deffered объект
         */
        renderAllPageBlocks:     function () {
            log.debug('Start to renderAllPageBlocks');
            var thisPageView = this;
            var blockInfoCollection = this.model.get('pageBlockCollection');
            var deferredArray = blockInfoCollection.each(function (block) {
                return thisPageView.renderPageBlock(block)
            });
            return $.when.apply(null, deferredArray);
        },

        /**
         * Производит создание контейнера и загрузку контент блока
         * @param blockInfo
         * @return {*} differed объект (pageBlockInfo в качетве параметра)
         */
        renderPageBlock: function (block) {
            var thisPageView = this;
            // Для каждого пейдж блока создаем контейнер и добавляем к элементу
            var $blockContainer = $('<div/>', {"id": block.get('blockInfo').get('guid'), "class": "PageBlockInfo"});
            var zoneContainer = this.$('#' + block.get('blockInfo').get('zone'));
            var divList = zoneContainer.find(".PageBlockInfo");
            if(divList.length == 0){
                $blockContainer.appendTo(zoneContainer);
            }else{
                var blockInfoList = thisPageView.model.get("pageBlockCollection").filter(function(blockModel){
                    return  blockModel.get('blockInfo').get('zone') == block.get('blockInfo').get('zone') && blockModel.get('active');
                })
                var divBefore = null;
                for(var i=0; i<blockInfoList.length;i++){
                    if(blockInfoList[i].get('blockInfo').get("orderNum")<block.get('blockInfo').get("orderNum")){
                        divBefore = $('#'+blockInfoList[i].get('blockInfo').get('guid'))
                    }
                }
                if(divBefore){
                    if(divBefore.length>0){
                        divBefore.after($blockContainer);
                    }else{
                        zoneContainer.append($blockContainer);
                    }
                }else{
                    zoneContainer.prepend($blockContainer);
                }
            }
            //грузим блок
            var pageBlockViewClass = block.get('blockInfo').get('backBoneClasses').view;
            var pageBlockView = new pageBlockViewClass({model: block});
            //thisPageView.listenTo(pageBlockView, 'all', thisPageView.proxyEventFromPageBlock);
            //thisPageView.pageBlockViews.push(pageBlockView);

            pageBlockView.setElement($blockContainer);
            //TODO
            //pageBlockView.render();

        },
        /** Скрывает не контейнеры не активных блоков, показывает или создает(если нет) контейнер активных блоков.
         *
         * */
        showActiveBlocks:function(){
            var thisPageView = this;
            //скрываем не активные
            var inactiveBlockCollection = this.model.get('pageBlockCollection').filter(function(pageBlockModel){
                return !pageBlockModel.get("active");
            });
            inactiveBlockCollection.forEach(function(block){
                $("#"+block.get("blockInfo").get('guid')).hide();
            });
            //показываем активные
            var activeBlockCollection = this.model.get('pageBlockCollection').filter(function(pageBlockModel){
                return pageBlockModel.get("active");
            });

            activeBlockCollection.forEach(function(block){
                var div = $('#'+block.get('blockInfo').get('guid'));
                if(div.length == 0){  // нет контейнера для блока
                    thisPageView.renderPageBlock(block);
                }else{
                    $("#"+block.get("blockInfo").get('guid')).show();
                }
            });
        },

        proxyEventFromSiteView: function () {

        },


        /**
         * Пролучает события от пейдж блоков
         * @param eventName
         * @param eventObject
         */
        proxyEventFromPageBlock: function (eventName, eventObject) {
            alert('proxyEventFromPageBlock');
            if (eventObject.notifyPage == null || eventObject.notifyPage == this.model.get('guid')) {
                // Извещаем о событии текущую страницу

            } else {
                // передаем событие
            }
            log.debug('proxyEventFromPageBlock ' + eventName);
            this.trigger(eventName, eventObject);
        }


    });

    return PageView;

});