define([

    // Libs
    'backbone',
    'misc',

    // Deps
    './PageView',
    '../model/SiteModel',
    'log',
    'breadcrumbs'

], function(Backbone, misc, PageView, SiteModel, log, Breadcrumbs) {

    var View = Backbone.View.extend({
        el: '#ml-cms-page-container',

        initialize: function(options) {
            //this.breadcrumbs = misc.option(options, "breadcrumbs", "");


            log.debug('Initialize SiteView');
            this.listenTo(this.model, 'renderNewPage', this.renderNewPage);
            this.listenTo(this.model, 'showPage', this.showPage);

            // TODO Инициализируем хлебные крошки если нужно
            this.breadcrumbs =   new Breadcrumbs.view({model : this.model});
            //$("#navigation-container").append(this.breadcrumbs.render().$el);
        },

        showPage : function(page){
            log.debug('SiteView showPage');
            this.$el.find("> *").detach();
            this.$el.append(page.get('pageView').$el);
            var nav = $("#navigation-container");
            if (nav){
                nav.append(this.breadcrumbs.render().$el);
            }

        },

        renderNewPage : function(page){
            log.debug('SiteView start renderNewPage');
            var _this = this;
            var pageView = new PageView({model : page});
            page.set('pageView', pageView);

            pageView.renderPage(function(pageView){
                _this.$el.find("> *").detach();
                _this.$el.append(pageView.$el);
                var nav = $("#navigation-container");
                if (nav){
                    nav.append(_this.breadcrumbs.render().$el);
                }
            });
            page.set('alreadyRendered', true);
        },


        /**
         * Обработчик событий от страниц сайта (и контент блоков на них)
         * @param eventObject
         */
        onPageEvent : function(eventName, eventObject){
           log.debug("Handle event from page");
        }

    });

    return View;

});