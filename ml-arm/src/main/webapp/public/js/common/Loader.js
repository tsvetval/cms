/**
 * Не используется. Отображает крутилку на выбранном диве(по умолчанию на body)
 */

define(["jquery", "spin"], function ($, Spinner) {
    var $body = $(document.body);
    var Loader = function ($target) {
        var _this = this;

        var $spinner = $("<div style='position: absolute; left: 50%; top: 50%;'/>");
        var $html = $("<div style='position:absolute; z-index:99999; left:0px; top: 0px; width: 100%; height: 100%; background: rgba(231, 231, 231, 0.45);'/>");
        $body.append($html.append($spinner));

        this.$html = $html;
        this.$target = $target;

        var opts = {
            lines:      13, // The number of lines to draw
            length:     10, // The length of each line
            width:      4, // The line thickness
            radius:     16, // The radius of the inner circle
            corners:    1, // Corner roundness (0..1)
            rotate:     0, // The rotation offset
            direction:  1, // 1: clockwise, -1: counterclockwise
            color:      '#000', // #rgb or #rrggbb or array of colors
            speed:      1, // Rounds per second
            trail:      60, // Afterglow percentage
            shadow:     false, // Whether to render a shadow
            hwaccel:    false, // Whether to use hardware acceleration
            className:  'spinner', // The CSS class to assign to the spinner
            zIndex:     2e9, // The z-index (defaults to 2000000000)
            top:        'auto', // Top position relative to parent in px
            left:       'auto', // Left position relative to parent in px
            background: '#FF0000'
        };
        var spinner = new Spinner(opts).spin($spinner.get(0));

        this.checkPositionListener = function () {_this.checkPosition();};

        this.hide();
    };

    Loader.prototype.setTarget = function ($target) {
        this.$target = $target;
    };

    Loader.prototype.checkPosition = function () {
        var top = this.$target.position().top;
        var left = this.$target.position().left;
        var windowHeight = $(window).height();
        var windowWidth = $(window).width();
        var height = windowHeight - top;
        var width = windowWidth - left;

        this.$html.css('top', top + "px");
        this.$html.css('left', left + "px");
        this.$html.css('width', width + "px");
        this.$html.css('height', height + "px");
    };

    Loader.prototype.hide = function () {
        $(window).unbind('resize', this.checkPositionListener);
        this.$html.hide();
    };

    Loader.prototype.show = function () {
        $(window).on('resize', this.checkPositionListener);
        this.$html.show();
        this.checkPosition();
    };

    return Loader;
});