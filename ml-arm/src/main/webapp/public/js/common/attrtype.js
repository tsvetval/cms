define(["jquery"], function ($) {
    var attrtype = new function () {

        var attrViewData = undefined;

        var attrsToHide = {
            STRING:[
                'linkAttr',
                'linkClass',
                'autoIncrement',
                'linkFilter',
                'lazy',
                'enumList',
                'longLinkValue',
                'manyToManyTableName',
                'manyToManyFieldNameM',
                'manyToManyFieldNameN',
                'notShowCreate',
                'notShowChoose',
                'notShowEdit',
                'notShowDelete',
                'notShowCreateInEdit',
                'notShowChooseInEdit',
                'notShowEditInEdit',
                'notShowDeleteInEdit',
                'tableHeight',
                'defaultSqlValue',
                'systemField',
                'ordered'
            ],
            LONG:[
                'linkAttr',
                'linkClass',
                'linkFilter',
                'lazy',
                'longLinkValue',
                'enumList',
                'longLinkValue',
                'manyToManyTableName',
                'manyToManyFieldNameM',
                'manyToManyFieldNameN',
                'notShowCreate',
                'notShowChoose',
                'notShowEdit',
                'notShowDelete',
                'notShowCreateInEdit',
                'notShowChooseInEdit',
                'notShowEditInEdit',
                'notShowDeleteInEdit',
                'tableHeight',
                'defaultSqlValue',
                'systemField',
                'ordered'
            ],
            BOOLEAN:[
                'linkAttr',
                'linkClass',
                'autoIncrement',
                'linkFilter',
                'lazy',
                'enumList',
                'primaryKey',
                'fieldFormat',
                'longLinkValue',
                'inputmask',
                'manyToManyTableName',
                'manyToManyFieldNameM',
                'manyToManyFieldNameN',
                'notShowCreate',
                'notShowChoose',
                'notShowEdit',
                'notShowDelete',
                'notShowCreateInEdit',
                'notShowChooseInEdit',
                'notShowEditInEdit',
                'notShowDeleteInEdit',
                'tableHeight',
                'defaultSqlValue',
                'systemField',
                'ordered'
            ],
            DATE:[
                'linkAttr',
                'linkClass',
                'autoIncrement',
                'linkFilter',
                'lazy',
                'enumList',
                'primaryKey',
                'longLinkValue',
                'inputmask',
                'manyToManyTableName',
                'manyToManyFieldNameM',
                'manyToManyFieldNameN',
                'notShowCreate',
                'notShowChoose',
                'notShowEdit',
                'notShowDelete',
                'notShowCreateInEdit',
                'notShowChooseInEdit',
                'notShowEditInEdit',
                'notShowDeleteInEdit',
                'tableHeight',
                'defaultSqlValue',
                'systemField',
                'ordered'
            ],
            ENUM:[
                'linkAttr',
                'linkClass',
                'autoIncrement',
                'linkFilter',
                'lazy',
                'primaryKey',
                'fieldFormat',
                'longLinkValue',
                'inputmask',
                'manyToManyTableName',
                'manyToManyFieldNameM',
                'manyToManyFieldNameN',
                'notShowCreate',
                'notShowChoose',
                'notShowEdit',
                'notShowDelete',
                'notShowCreateInEdit',
                'notShowChooseInEdit',
                'notShowEditInEdit',
                'notShowDeleteInEdit',
                'tableHeight',
                'defaultSqlValue',
                'systemField',
                'ordered'
            ],
            ONE_TO_MANY:[
                'tableFieldName',
                'linkClass',
                'autoIncrement',
                'enumList',
                'defaultValue',
                'primaryKey',
                'fieldFormat',
                'longLinkValue',
                'inputmask',
                'virtual',
                'manyToManyTableName',
                'manyToManyFieldNameM',
                'manyToManyFieldNameN',
                'tableHeight',
                'defaultSqlValue',
                'systemField'
            ],
            MANY_TO_ONE:[
                'linkAttr',
                'autoIncrement',
                'enumList',
                //'defaultValue',
                'primaryKey',
                'fieldFormat',
                'longLinkValue',
                'inputmask',
                'virtual',
                'manyToManyTableName',
                'manyToManyFieldNameM',
                'manyToManyFieldNameN',
                'defaultSqlValue',
                'systemField',
                'ordered'
            ],
            MANY_TO_MANY:[
                'tableFieldName',
                'linkAttr',
                'autoIncrement',
                'enumList',
                'defaultValue',
                'primaryKey',
                'fieldFormat',
                'longLinkValue',
                'inputmask',
                'virtual',
                'defaultSqlValue',
                'systemField'
            ],
            LONG_LINK:[
                'linkAttr',
                'linkClass',
                'autoIncrement',
                'enumList',
                'defaultValue',
                'primaryKey',
                'fieldFormat',
                'inputmask',
                'mandatory',
                'manyToManyTableName',
                'manyToManyFieldNameM',
                'manyToManyFieldNameN',
                'tableHeight',
                'defaultSqlValue',
                'systemField',
                'ordered'
            ],
            DOUBLE:[
                'linkAttr',
                'linkClass',
                'autoIncrement',
                'linkFilter',
                'lazy',
                'enumList',
                'longLinkValue',
                'primaryKey',
                'manyToManyTableName',
                'manyToManyFieldNameM',
                'manyToManyFieldNameN',
                'notShowCreate',
                'notShowChoose',
                'notShowEdit',
                'notShowDelete',
                'notShowCreateInEdit',
                'notShowChooseInEdit',
                'notShowEditInEdit',
                'notShowDeleteInEdit',
                'tableHeight',
                'defaultSqlValue',
                'systemField',
                'ordered'
            ],
            TEXT:[
                'linkAttr',
                'linkClass',
                'autoIncrement',
                'linkFilter',
                'lazy',
                'enumList',
                'primaryKey',
                'longLinkValue',
                //'inputmask',
                'manyToManyTableName',
                'manyToManyFieldNameM',
                'manyToManyFieldNameN',
                'notShowCreate',
                'notShowChoose',
                'notShowEdit',
                'notShowDelete',
                'notShowCreateInEdit',
                'notShowChooseInEdit',
                'notShowEditInEdit',
                'notShowDeleteInEdit',
                'tableHeight',
                'defaultSqlValue',
                'systemField',
                'ordered'
            ],
            FILE:[
                'linkAttr',
                'linkClass',
                'autoIncrement',
                'linkFilter',
                'lazy',
                'enumList',
                'defaultValue',
                'primaryKey',
                'fieldFormat',
                'longLinkValue',
                'inputmask',
                'manyToManyTableName',
                'manyToManyFieldNameM',
                'manyToManyFieldNameN',
                'notShowCreate',
                'notShowChoose',
                'notShowEdit',
                'notShowDelete',
                'notShowCreateInEdit',
                'notShowChooseInEdit',
                'notShowEditInEdit',
                'notShowDeleteInEdit',
                'tableHeight',
                'defaultSqlValue',
                'systemField',
                'ordered'
            ],
            ONE_TO_ONE:[
                'linkClass',
                'autoIncrement',
                'enumList',
                'defaultValue',
                'primaryKey',
                'fieldFormat',
                'longLinkValue',
                'inputmask',
                'defaultSqlValue',
                'systemField'
            ]
        };

        var isFieldHidden = function (attrName, attrType) {
            return attrsToHide[attrType].indexOf(attrName) != -1;
        };

        this.updateFieldsState = function (type) {
            if (type != null && attrsToHide[type] == null) {
                return;
            }
            $.each($('.MlAttr_attr_container'), function (index, element) {
                var $element = $(element);
                var name = $element.attr('attrName');
                if (name == null) {
                    return;
                }
/*
                if (type == null) {
                    type = undefined;
                }
*/
                if (attrsToHide[type] == null) {
                    return;
                }
                if (isFieldHidden(name, type)) {
                    $element.hide();
                } else {
                    $element.show();
                }
            });
        };

        //Отфильтровать список доступных представлений по типу данных
        this.updateAttrViewList = function(type){
            var $select = $('input[name=view]');

            $select.data('attrType', type);
            var attrViewAll = $select.data('attrViewAll');

            var attrViewFiltered = [{id: null, text: '', fieldtype: null}];
            if(attrViewAll != undefined){
                for (var i = 0; i < attrViewAll.length; i++) {
                    if(attrViewAll[i].fieldtype == type){
                        attrViewFiltered.push(attrViewAll[i]);
                    }
                }
                $select.select2({data: attrViewFiltered});
            }
        }
    }
    return  attrtype;
});