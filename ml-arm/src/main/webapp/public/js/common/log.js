/**
 * Класс ведения клиентских логов
 * */
define(['stacktrace'], function (stacktrace) {
    var log = new function () {

        this.error = function (message) {
            this.outWithFormat("!!!!!!!! Error: " + message, 'ERROR');
        };

        this.warn = function (message) {
            this.outWithFormat("!!!!!!!! Warn: " + message, 'WARNING');
        },

        this.debug = function (message) {
            this.outWithFormat(message);
        };

        this.outWithFormat = function (message, mode) {
            var trace = this._getTrace(stacktrace());

            var d = new Date();
            var curr_hour = d.getHours();
            var curr_min = d.getMinutes();
            var curr_sec = d.getSeconds();
            var curr_msec = d.getMilliseconds();
            curr_min = curr_min + "";
            if (curr_min.length === 1) curr_min = "0" + curr_min;
            curr_sec = curr_sec + "";
            if (curr_sec.length === 1) curr_sec = "0" + curr_sec;
            curr_msec = "000" + curr_msec;
            curr_msec = curr_msec.substring(curr_msec.length - 3);
            var datum = curr_hour + ":" + curr_min + ":" + curr_sec + ":" + curr_msec;
            var newline = datum + " - " + message;
            newline = newline + " TRACE: " + trace;


            this._ConsoleOut(newline, mode);
        };


        this._ConsoleOut = function (message, mode) {
            if (mode == 'ERROR') {
                console.error(message)
            } else if (mode == 'WARNING') {
                console.warn(message)
            } else {
                console.log(message);
            }
        };

        this._getTrace = function (traceList) {
            var result = '';
            if (traceList && traceList.length > 6) {
                result = traceList[6];
            }
            return result;
        };


    };


    return log;
});

