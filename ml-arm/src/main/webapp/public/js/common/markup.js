/**
 * Класс утилит
 */
define(["jquery", "misc"], function ($, misc) {
    var markup = new function () {

        /**
         * Function for add callbacks into markup, using special attributes and callbacks map
         * @param {jQuery} $parent scope for search elements with callbacks
         * @param {Object} callbacks мапа колбеков. Ключи используются для связи колбеков с HTML элементами по атрибуту 'action-name'
         */
        this.attachActions = function($parent, callbacks)
        {
            $(".callback", $parent).each(function(){
                var $this = $(this);

                var stopPropagation = $this.attr('stopPropagation');

                var actions = [];

                $.each(this.attributes, function() {
                    if(this.specified) {
                        if(this.name.indexOf('-action') > -1){
                            var action = {
                                event: this.name.replace('-action',''),
                                name: this.value
                            };
                            if($this.attr(action.event + '-params')){
                                var params = $this.attr(action.event + '-params');
                                try {
                                    action.params = params ? JSON.parse(params) : undefined;
                                } catch(e) {
                                    throw new Error("Could not parse string as JSON: '" + params + "'");
                                }
                                $this.data('params',action.params);
                            }
                            actions.push(action);
                        }
                    }
                });

                $.each(actions, function(index,action){
                    if(callbacks[action.name]){
                        var callback = callbacks[action.name];
                        $this.off(action.event).on(action.event, function(){
                            var params = $this.data('params');
                            callback.apply($this, [params]);
                            if (stopPropagation){
                                return false;
                            }
                            return true;
                        });
                    }
                });
            });
        };

        this.makeMandatory = function($elem, f, options){
            if(f===undefined)
            {
                f = function(){
                    var $this = $(this);
                    var finishEdit = function(){
                        if(!$this.val())
                        {
                            $this.addClass("ml-mandatory-empty");
                        }
                        else {
                            $this.removeClass("ml-mandatory-empty");
                        }
                    };
                    $this.focus(function () {
                        $this.removeClass("ml-mandatory-empty");
                    });
                    $this.blur(finishEdit);
                    $this.change(finishEdit);
                }
            }
            var onCheck = misc.option(options, "onCheck", "Функция для проверки заполнености поля", function(){
                $(this).change();
                return !!$(this).val();
            });
            $elem.each(f);
            $elem.addClass('ml-markup-mandatory');
            $elem.data('ml.markup.onCheckMandatory', onCheck);
        }

    };
    return  markup;
});

