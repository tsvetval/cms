/**
 * Класс утилит
 */
define(["jquery", "big"], function ($, Big) {
    var misc = new function () {
        /**
         * Реализация наследования в стандартном ООП стиле. Создает для дочернего класса @childClass промежуточный
         * прототип, связаный с прототипом родительского класса @parentClass. Таким образом, любые обращения к
         * инстансу дочернего класса могут обращаться к прототипу родительского класса (т. е. наследовать его
         * функционал), при этом можно добавлять новые методы в прототип дочернего класса, не боясь изменить
         * прототип родительского класса.
         *
         * @param parentClass родительский класс
         * @param childClass дочерний класс
         *
         * @returns {*} дочерний класс с перенастроенными прототипами. После такого наследования переопределять
         * прототип нельзя, так как это сломает цепочку наследования, можно только изменять его содержимое (добавлять
         * или удалять новые члены)
         *
         * @see http://javascript.ru/tutorial/object/inheritance Хорошая статья про наследование
         */
        this.extend = function (parentClass, childClass) {
            var F = function () {
            };
            F.prototype = parentClass.prototype;
            childClass.prototype = new F();
            childClass.prototype.constructor = childClass;
            /*Записываем класс родитель в поле superclass для последующего
             вызова родительских методов в конструкторе и при перекрытии методов.*/
            childClass.superclass = parentClass.prototype;
            return childClass;
        };

        /**
         * Извлечение опции из мапы опций. Если опция не определена, или мапа опция не определена, используется
         * значение по умолчанию. Если значение по умолчанию не определено, выбрасывается исключение.
         * @param {Object<String,*>} options мапа опций
         * @param {String} item название опции для поиска в мапе опций
         * @param {String} comment пояснение к опции; используется при выбрасывание исключения и для
         * увеличения читаемости кода
         * @param {*} def значение опции по умолчаниею; используется, если опция отсутствует в
         * мапе или мапа не определена
         * @returns {*} значение опции из мапы, если оно задано, значение по умолчанию, если опция не найдена;
         * выбрасывается исключение, если опция не найдена и значение по умолчанию не задано
         */
        this.option = function (options, item, comment, def) {
            if (options === undefined || options[item] === undefined) {
                if (def !== undefined) {
                    return def;
                }
                comment = (comment !== undefined) ? " (" + comment + ") " : " ";
                throw new Error("Option '" + item + "'" + comment + "is not defined!");
            }
            return options[item];
        };

        /**
         * Проверяет, что obj является подмножеством container
         * @param container
         * @param obj
         * @returns {boolean}
         */
        this.contains = function (container, obj) {
            if (typeof obj == "string" || typeof obj == "number" || typeof obj == "boolean") {
                return obj == container;
            }
            for (var a in obj) {
                if (!container[a]) return false;
                if (!this.contains(container[a], obj[a])) return false;
            }
            return true;
        };


        this.createGuid = function createGuid() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        };

        this.createMlId = function createMlId(id, className) {
            if (typeof id == "undefined" || isNaN(parseInt(id)) || typeof className == "undefined") {
                throw new Error("Error creating ml id. Wrong parameters: : id = " + id, +", className = " + className);
            }
            return id + '@' + className;
        };

        this.parseMlId = function parseMlId(mlId) {
            if (!mlId) {
                throw new Error("Error parsing ml id.");
            }
            var array = mlId.split('@');
            if (array.length != 2) {
                throw new Error("Error parsing ml id.");
            }
            return {
                id:        array[0],
                className: array[0]
            };
        };

        this.uploadFiles = function (files, callback) {
            console.log('Start uploading files: ' + files[0]);
            var xhr = new XMLHttpRequest();
            var url = this.getContextPath() + "/upload";
            xhr.onreadystatechange = function (e) {
                if (4 == this.readyState) {
                    console.log('Files upoaded: ' + e.target.responseText);
                    callback();
                }
            };
            xhr.open('post', url, true);
            var fd = new FormData;
            $.each(files, function (index, file) {
                fd.append(file.name, file);
            });
            xhr.send(fd);
        };

        this.getContextPath = function () {
            var attr = document.getElementsByTagName("body")[0].attributes.getNamedItem("cms_context_path");
            if (!attr) throw new Error("Body of document must have 'cms_context_path' attribute!");
            return attr.value;
        };

        this.parseLittleEndian = function(hex) {
            var result = new Big(0);
            var pow = 0;
            while (hex.length > 0) {
                var v = new Big(parseInt(hex.substring(0, 2), 16));
                var r = new Big(2).pow(pow);
                result = result.plus(v.mul(r));
                //result += parseInt(hex.substring(0, 2), 16) * Math.pow(2, pow);
                hex = hex.substring(2, hex.length);
                pow += 8;
            }
            return result.toString();
        }

    };
    return  misc;
});

