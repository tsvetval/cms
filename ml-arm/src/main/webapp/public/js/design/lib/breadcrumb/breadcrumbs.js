/**
 * Блок отображения контента
 */
define(['jquery', 'misc', 'backbone', 'log', 'underscore'],
    function ($, misc, Backbone, log, _) {
        var contextPath = misc.getContextPath();
        var BreadcrumbsView = Backbone.View.extend({
            events: {
                "click .first": "homeClick"
            },

            className: "BreadcrumbsView",
            //crumbSeparatorTemplate: "<img class='crumb-chevron-<%=pageGUID%>' style='vertical-align: middle;' src='" + contextPath + "/public/images/Chevron.gif'/>",
            //crumbTemplate: "<span class='crumb crumb-<%=pageGUID%>'><span><a href='#' class='title' id='crumb-<%=pageGUID%>'><%=pageTitle%></a><span></span>",
            crumbTemplate: '<li class ="crumb crumb-<%=pageGUID%>"> <a href="#" class="title" id="crumb-<%=pageGUID%>"><%=pageTitle%></a></li>',

            maxFinalElementLength: 400,
            minFinalElementLength: 200,
            minimumCompressionElements: 4,
            endElementsToLeaveOpen: 1,
            beginingElementsToLeaveOpen: 1,
            timeExpansionAnimation: 800,
            timeCompressionAnimation: 500,
            timeInitialCollapse: 600,
            easing:  undefined,//(typeof($.easing) == 'object') ? "easeOutQuad" : "swing",
            overlayClass: 'chevronOverlay',
            previewWidth: 15,


            initialize: function () {
                var _this = this;
                // Устанавливаем слушателей
                /*
                 this.listenTo(this.model.get('pageCollection'), "add", function (page) {
                 log.debug('BreadCrumb intercept Add event from pageCollection');
                 _this.onAdd(page)
                 });

                 this.listenTo(this.model.get('pageCollection'), "remove", function (crumb) {
                 log.debug('BreadCrumb intercept Remove event from pageCollection');
                 _this.onRemove(crumb)
                 });
                 */
                this.listenTo(this.model, "change:activePage", function (siteModel) {
                    log.debug('BreadCrumb intercept ChangeActive event from siteModel');
                    var activePage = siteModel.get('activePage');

                    var activeIndex = this.model.get('pageCollection').indexOf(activePage);
                    this.model.get('pageCollection').forEach(function (page, i) {
                        var crumbView = _this.getCrumbView(page);
                        crumbView.removeClass("active");
                        if (i > activeIndex) {
                            crumbView.addClass("ghost");
                        } else {
                            crumbView.removeClass("ghost");
                        }
                    });
                    _this.getCrumbView(activePage).addClass("active");

                });
                this.listenTo(this.model.get('pageCollection'), "change:pageTitle", function (crumb) {
                    _this.getCrumbView(crumb).find(".title").text(crumb.get("pageTitle"));
                });


                this.$el.append('<ul style="width: 5000px;">');
                this.$container = this.$el.find('ul');


            },

            getCrumbView: function (crumb) {
                return this.$el.find(".crumb-" + crumb.get('guid'))
            },

            onAdd: function (page) {
                var _this = this;
                var $crumb = $(_.template(this.crumbTemplate, {
                    pageGUID: page.get('guid'),
                    pageTitle: _.escape(page.get('pageTitle'))
                }));

                $crumb.click(function (e) {
                    _this.model.setActivePage(page);
                    e.preventDefault();
                });
                if (page.get("active") == true) $crumb.addClass('active');
                if (page.get("href")) $crumb.find('.title').attr('href', page.get("href"));
                //if (page.get("isHome")) $crumb.find(".title").html("<span class='glyphicon glyphicon-home'></span>");
                this.$container.append($crumb);

                //$crumb.mouseenter(function () {
                //    if ($crumb.hasClass("collapsed")) {
                //        $crumb.data('need-collapse', true);
                //        $crumb.removeClass("collapsed");
                //    }
                //});
                //
                //$crumb.mouseleave(function () {
                //    if ($crumb.data("need-collapse")) {
                //        $crumb.addClass("collapsed");
                //        $crumb.data('need-collapse', false);
                //    }
                //});

                //var $shadow = $("<div class='shadow'></div>");
                //$crumb.append($shadow);

                //this.collapsedRecalculate();
                //this.collapsedCheck();
            },

            onRemove: function (crumb) {
                this.$el.find(".crumb-" + crumb.get('guid')).remove();
                this.$el.find(".crumb-chevron-" + crumb.get('guid')).remove();
                //this.collapsedRecalculate();
                //this.collapsedCheck();
            },


            render: function () {
                var _this = this;
                this.$container.empty();

                // добавляем home крамб
                this.$el.find('ul').append('<li class="first"><a href="/">');

                // Добавляем все крамбы
                this.model.get('pageCollection').each(function (page) {
                    _this.onAdd(page);
                });

                this.$el.append(this.$container);


                //TODO
                //_this.collapsedCheck();

                // По отрендеренным крамбам проводим раскрашивание
                var activePage = this.model.get('activePage');
                var activeIndex = this.model.get('pageCollection').indexOf(activePage);
                this.model.get('pageCollection').forEach(function (page, i) {
                    var crumbView = _this.getCrumbView(page);
                    crumbView.removeClass("active");
                    if (i > activeIndex) {
                        crumbView.addClass("ghost");
                    } else {
                        crumbView.removeClass("ghost");
                    }
                });

                if (this.model.get('pageCollection').length > 0) {
                    var lastPage = this.model.get('pageCollection').models[this.model.get('pageCollection').length - 1];
                    _this.getCrumbView(lastPage).addClass("last");
                }

                _this.getCrumbView(activePage).addClass("active");

                //this._breadCrumbElements = this.$container.find('li');
                //this.compressBreadCrumb();
                return this;
            },


            homeClick: function (e) {
                var homeUrl = misc.getContextPath();
                if ($.cookie('homePage')) {
                    homeUrl = homeUrl + "/" + $.cookie('homePage');
                }
                window.location.href = homeUrl;
                e.preventDefault();
            },

            compressBreadCrumb: function () {
                var _this = this;
                var _breadCrumbElements = this.$container.find('li');
                // Factor to determine if we should compress the element at all
                var finalElement = $(_breadCrumbElements[_breadCrumbElements.length - 1]);


                // If the final element is really long, compress more elements
                if ($(finalElement).width() > _this.maxFinalElementLength) {
                    if (_this.beginingElementsToLeaveOpen > 0) {
                        _this.beginingElementsToLeaveOpen--;

                    }
                    if (_this.endElementsToLeaveOpen > 0) {
                        _this.endElementsToLeaveOpen--;
                    }
                }
                // If the final element is within the short and long range, compress to the default end elements and 1 less beginning elements
                if ($(finalElement).width() < _this.maxFinalElementLength && $(finalElement).width() > _this.minFinalElementLength) {
                    if (_this.beginingElementsToLeaveOpen > 0) {
                        _this.beginingElementsToLeaveOpen--;

                    }
                }

                var itemsToRemove = _breadCrumbElements.length - 1 - _this.endElementsToLeaveOpen;

                // We compress only elements determined by the formula setting below

                //TODO : Make this smarter, it's only checking the final elements length.  It could also check the amount of elements.
                $(_breadCrumbElements[_breadCrumbElements.length - 1]).css(
                    {
                        background: 'none'
                    });

                $(_breadCrumbElements).each(function (i, listElement) {
                    if (i > _this.beginingElementsToLeaveOpen && i < itemsToRemove) {

                        $(listElement).find('a').wrap('<span></span>').width($(listElement).find('a').width() + 10);

                        // Add the overlay png.
                        $(listElement).append($('<div class="' + _this.overlayClass + '"></div>').css(
                            {
                                display: 'block'
                            })).css(
                            {
                                background: 'none'
                            });
/*
                        if (isIE6OrLess()) {
                            fixPNG($(listElement).find('.' + _this.overlayClass).css(
                                {
                                    width: '20px',
                                    right: "-1px"
                                }));
                        }
*/
                        var options =
                        {
                            id: i,
                            width: $(listElement).width(),
                            listElement: $(listElement).find('span'),
                            isAnimating: false,
                            element: $(listElement).find('span'),
                            timeExpansionAnimation : _this.timeExpansionAnimation,
                            timeCompressionAnimation : _this.timeCompressionAnimation,
                            previewWidth : _this.previewWidth

                        };
                        $(listElement).bind('mouseover', options, _this.expandBreadCrumb).bind('mouseout', options, _this.shrinkBreadCrumb);
                        $(listElement).find('a').unbind('mouseover', _this.expandBreadCrumb).unbind('mouseout', _this.shrinkBreadCrumb);
                        listElement.autoInterval = setInterval(function () {
                            clearInterval(listElement.autoInterval);
                            $(listElement).find('span').animate(
                                {
                                    width: _this.previewWidth
                                }, _this.timeInitialCollapse, _this.easing);
                        }, (150 * (i - 2)));

                    }
                });

            },

            expandBreadCrumb: function (e) {
                var _this = this;
                var elementID = e.data.id;
                var originalWidth = e.data.width;
                $(e.data.element).stop();
                $(e.data.element).animate(
                    {
                        width: originalWidth
                    },
                    {
                        duration: e.data.timeExpansionAnimation,
                        easing: _this.easing,
                        queue: false
                    });
                return false;

            },

            shrinkBreadCrumb: function (e) {
                var _this = this;
                var elementID = e.data.id;
                $(e.data.element).stop();
                $(e.data.element).animate(
                    {
                        width: e.data.previewWidth
                    },
                    {
                        duration: e.data.timeCompressionAnimation,
                        easing: _this.easing,
                        queue: false
                    });
                return false;
            }
        });

        return {
            view: BreadcrumbsView
            //collection: BreadcrumbCollection,
            //model: BreadcrumbModel
        }
    })
;


