(function ($, undefined) {
    "use strict";
    //var span = document.createElement('span');
    //span.className = 'glyphicons glyphicons-comments flip jstree-comment'
    var template = '<i class="jstree-icon jstree-themeicon" role="presentation"></i><span class="ml-list__bg"></span><span class="ml-list__item-title"><%=title%></span>';
    var closeTemplate = '<span class="ml-list__close"></span>';

    $.jstree.defaults.tree_customizer = $.noop;
    $.jstree.plugins.tree_customizer = function (options, parent) {
        this.bind = function () {
            parent.bind.call(this);
        };
        /**
         * При отрисовке нод правит html
         * @param obj
         * @param deep
         * @param callback
         * @param force_draw
         * @returns {*}
         */
        this.redraw_node = function (obj, deep, callback, force_draw) {
            var _this = this;
            var node = this.get_node(obj);
            node.icon = "";
            var li = parent.redraw_node.call(this, obj, deep, callback, force_draw);

            if (node.parent === '#') {
                $(li).append(closeTemplate);
                var additionalHtml = template.replace("<%=title%>", node.text);
                $(li).find('.jstree-anchor').html(additionalHtml);

                $(li).find('.ml-list__close').on('click', function () {
                    // Закрываем ноду по стрелке вверх
                    _this.close_node(obj);
                    return false;
                });
            }
            return li;
        };
    };
})(jQuery);