/**
 * Блок отображения контента
 */
define(['jquery', 'misc', 'backbone', 'log', 'underscore'],
    function ($, misc, Backbone, log, _) {
        var contextPath = misc.getContextPath();
        var BreadcrumbsView = Backbone.View.extend({
            events: {
                "click .home": "homeClick"
            },

            className: "BreadcrumbsView",
            crumbSeparatorTemplate: "<img class='crumb-chevron-<%=pageGUID%>' style='vertical-align: middle;' src='" + contextPath + "/public/images/Chevron.gif'/>",
            crumbTemplate: "<span class='crumb crumb-<%=pageGUID%>'><span><a href='#' class='title' id='crumb-<%=pageGUID%>'><%=pageTitle%></a><span></span>",

            initialize: function () {
                var _this = this;
                this.listenTo(this.model.get('pageCollection'), "add", function (page) {
                    log.debug('BreadCrumb intercept Add event from pageCollection');
                    _this.onAdd(page)
                });
                this.listenTo(this.model.get('pageCollection'), "remove", function (crumb) {
                    log.debug('BreadCrumb intercept Remove event from pageCollection');
                    _this.onRemove(crumb)
                });
                this.listenTo(this.model, "change:activePage", function (siteModel) {
                    log.debug('BreadCrumb intercept ChangeActive event from siteModel');
                    var activePage = siteModel.get('activePage');

                    var activeIndex = this.model.get('pageCollection').indexOf(activePage);
                    this.model.get('pageCollection').forEach(function (page, i) {
                        var crumbView = _this.getCrumbView(page);
                        crumbView.removeClass("active");
                        if (i > activeIndex) {
                            crumbView.addClass("ghost");
                        } else {
                            crumbView.removeClass("ghost");
                        }
                    });
                    _this.getCrumbView(activePage).addClass("active");

                });
                this.listenTo(this.model.get('pageCollection'), "change:pageTitle", function (crumb) {
                    _this.getCrumbView(crumb).find(".title").text(crumb.get("pageTitle"));
                });

                this.$container = $("<div class='container'/>");

                $(window).resize(function () {
                    _this.collapsedCheck();
                });

            },

            getCrumbView: function (crumb) {
                return this.$el.find(".crumb-" + crumb.get('guid'))
            },

            onAdd: function (page) {
                var _this = this;
                if (!this.$container.is(":empty")) {
                    this.$container.append($(_.template(this.crumbSeparatorTemplate, {pageGUID: page.get('guid')})));
                }
                var $crumb = $(_.template(this.crumbTemplate, {
                    pageGUID: page.get('guid'),
                    pageTitle: _.escape(page.get('pageTitle'))
                }));

                $crumb.click(function (e) {
                    _this.model.setActivePage(page);
                    e.preventDefault();
                });
                if (page.get("active") == true) $crumb.addClass('active');
                if (page.get("href")) $crumb.find('.title').attr('href', page.get("href"));
                if (page.get("isHome")) $crumb.find(".title").html("<span class='glyphicon glyphicon-home'></span>");

                this.$container.append($crumb);

                $crumb.mouseenter(function () {
                    if ($crumb.hasClass("collapsed")) {
                        $crumb.data('need-collapse', true);
                        $crumb.removeClass("collapsed");
                    }
                });

                $crumb.mouseleave(function () {
                    if ($crumb.data("need-collapse")) {
                        $crumb.addClass("collapsed");
                        $crumb.data('need-collapse', false);
                    }
                });

                var $shadow = $("<div class='shadow'></div>");
                $crumb.append($shadow);

                this.collapsedRecalculate();
                this.collapsedCheck();
            },

            onRemove: function (crumb) {
                this.$el.find(".crumb-" + crumb.get('guid')).remove();
                this.$el.find(".crumb-chevron-" + crumb.get('guid')).remove();
                this.collapsedRecalculate();
                this.collapsedCheck();
            },

            collapsedRecalculate: function () {
                this.collapsedFullWidth = this.getItemsWidth();
            },

            collapsedCheck: function () {
                if (this.collapsed) {
                    var containerWidth = this.$el.outerWidth();
                    if (this.collapsedFullWidth < containerWidth) {
                        this.collapse(false);
                    }
                    else {
                        this.collapse(true);
                    }
                }
                else {
                    var containerWidth = this.$el.outerWidth();
                    var fullWidth = this.getItemsWidth();
                    if (fullWidth > containerWidth) {
                        this.collapse(true);
                    }
                }
            },

            collapse: function (doCollapse) {
                if (doCollapse) {
                    var itemsWidth = this.getItemsWidth();

                    var $crumbs = this.$container.find("> .crumb");
                    $crumbs.addClass("collapsed");
                    var asArray = $.makeArray($crumbs).slice();
                    $(asArray.slice(asArray.length - 2)).removeClass("collapsed");

                    this.collapsed = true;
                    this.collapsedFullWidth = itemsWidth;
                }
                else {
                    this.$container.find("> .crumb").removeClass("collapsed");
                    this.collapsed = false;
                }
            },

            render: function () {
                var _this = this;

                // Fill crumbs
                this.$el.empty();
                this.$container.empty();
                this.model.get('pageCollection').each(function (page) {
                    _this.onAdd(page);
                });
                this.$el.append(this.$container);
                this.$el.append($("<div class='shadow'/>"));

                // Добавляем крошку Home
                this.$container.prepend($('<span class="crumb home"><a class="title" href="#"><span class="glyphicon glyphicon-home"></span></a></span>'));

                _this.collapsedCheck();

                // По отрендеренным крамбам проводим раскрашивание
                var activePage = this.model.get('activePage');

                var activeIndex = this.model.get('pageCollection').indexOf(activePage);
                this.model.get('pageCollection').forEach(function (page, i) {
                    var crumbView = _this.getCrumbView(page);
                    crumbView.removeClass("active");
                    if (i > activeIndex) {
                        crumbView.addClass("ghost");
                    } else {
                        crumbView.removeClass("ghost");
                    }
                });
                _this.getCrumbView(activePage).addClass("active");

                return this;
            },

            appendTo: function ($container) {
                $container.append(this.$el);
                this.collapsedCheck();
                return this;
            },

            getItemsWidth: function () {
                var width = 0;
                var wasCollapsed = this.collapsed;
                if (wasCollapsed) this.collapse(false);
                this.$el.find(".container > *").each(function () {
                    var $this = $(this);
                    width += $this.outerWidth(true);
                });
                if (wasCollapsed) this.collapse(true);
                return width;
            },

            homeClick: function () {
                var homeUrl = misc.getContextPath();
                if ($.cookie('homePage')) {
                    homeUrl = homeUrl + "/" +$.cookie('homePage');
                }
                window.location.href = homeUrl;
            }
        });

        return {
            view: BreadcrumbsView
            //collection: BreadcrumbCollection,
            //model: BreadcrumbModel
        }
    });


