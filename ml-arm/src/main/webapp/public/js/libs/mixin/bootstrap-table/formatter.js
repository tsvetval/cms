define(
    ['log', 'misc', 'backbone', 'underscore', 'moment', 'jquery',
        'text!libs/mixin/bootstrap-table/templates/OneToManyTemplate.tpl'
    ],
    function (log, misc, backbone, _, moment, $,
              OneToManyTemplate) {

        var Formatter = {

            formatterOneToMany: function (value, row, index) {
                var result = [];
                if (value && value.objectList && value.objectList.length > 0) {
                    value.objectList.forEach(function (obj) {
                        result.push(obj.title)
                    })
                }

                if (result.length <= 5) {
                    return result.join('</br>');
                } else {
                    return _.template(OneToManyTemplate, {result: result});
                }
            },

            formatterHeteroLink: function (value, row, index) {
                var result = [];
                if (value && value.objectList && value.objectList.length > 0) {
                    value.objectList.forEach(function (obj) {
                        result.push(obj.title)
                    })
                }

                if (result.length <= 5) {
                    return result.join('</br>');
                } else {
                    return _.template(OneToManyTemplate, {result: result});
                }
            },

            formatterManyToOne: function (value, row, index) {
                var result = '';
                if (value && value.title) {
                    result = value.title;
                }
                return result;
            },

            formatterBoolean: function (value, row, index) {
                var result = '<input disabled="true" type="checkbox" '
                    + (value ? 'checked = "checked"' : '') + '"/>';
                return result;
            },

            formatterEnum: function (value, row, index) {
                var result = '';
                if (value && value.title) {
                    result = value.title;
                }
                return result;
            },

            formatterSelect: function (value, row, index) {
                var result = '<div class="icon">' +
                    '<span class="glyphicon glyphicon-chevron-right open-object-button clickable"' +
                    'objectId="' + value + '">' +
                    '</span>'+
                    '</div>';
                return result;
            },

            formatterDateWithOptions: function (formatOptions){
                var _formatOptions = $.extend({dateFormat : "DD.MM.YYYY"}, formatOptions);
                return function(value, row, index) {
                    if (!value || value.length == 0) {
                        return "";
                    }
                    //var dateMoment = moment(value)/*.utc()*/;

                    return value;//dateMoment.format(_formatOptions.dateFormat);
                };
            },

            formatterDate: function (value, row, index) {
                if (!value || value.length == 0) {
                    return "";
                }
                var dateMoment = moment(value)/*.utc()*/;

                return dateMoment.format("DD.MM.YYYY");
            },

            formatterFile: function (value, row, index) {
                if (!value || value.length == 0) {
                    return "";
                }

                var link = $('<a>', {
                    text: value.title,
                    "title": value.title,
                    objectId: value.objectId,
                    attrName: value.entityFieldName,
                    href: '#',
                    "class": 'file-download-link'
                });
                return link[0].outerHTML;
            }
        };

        return Formatter;

    });