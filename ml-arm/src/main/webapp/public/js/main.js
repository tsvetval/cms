/**
 * Основной класс описывающий зависимости
 */

require.config({
    urlArgs: "bust=v1",
    baseUrl: document.getElementsByTagName("body")[0].attributes.getNamedItem("cms_context_path").value + '/public/js',
    paths: {
        text: "libs/text",
        jquery: 'libs/jquery-2.1.3.min',
        jqueryUi: 'libs/jquery-ui-1.11.3.custom/jquery-ui',
        jstree: 'libs/jstree/dist/jstree',
        bootstrap: 'libs/bootstrap.min',
        datetimepicker: 'libs/bootstrap-datetimepicker.min',
        moment: 'libs/moment-with-locales.min',
        moment_java: 'libs/java_moment_format',
        spin: 'libs/spin.min',
        misc: 'common/misc',
        markup: 'common/markup',
        log: 'common/log',
        stacktrace: 'libs/stacktrace',
        select2: 'libs/select2/select2.min',
        backbone: 'libs/backbone',
        backbone_queryparams: 'libs/backbone.queryparams',
        inputmask: 'libs/jquery.inputmask.bundle',
        underscore: 'libs/underscore',
        breadcrumbs: 'libs/breadcrumbs',
        channel: 'libs/channel',
        iconselect: 'libs/iconselect/iconselect',
        "ckeditor-core": 'libs/ckeditor/ckeditor',
        "ckeditor-jquery": 'libs/ckeditor/adapters/jquery',
/*
        data_table : 'libs/data-tables-1.10.4/media/js/jquery.dataTables'
*/
        boot_table : 'libs/bootstrap-table-master/src/bootstrap-table',
        boot_table_ru: 'libs/bootstrap-table-master/src/locale/bootstrap-table-ru-RU',
        table_formatter_mixin: 'libs/mixin/bootstrap-table/formatter',
        jquery_cookie: 'libs/jquery-cookie-master/src/jquery.cookie',
        jszip: 'libs/jszip.min',
        filesaver: 'libs/FileSaver.min',
        console_const:'cms/console_const' ,
        jcrop: 'libs/jquery.Jcrop.min',
        dtjava: 'libs/dtjava',
        big : 'libs/big'
    },
    shim: {
        /* Set bootstrap dependencies (just jQuery) */
        'bootstrap': ['jquery'],
        'datetimepicker': ['jquery', 'bootstrap', 'moment'],
        'select2': ['jquery', 'bootstrap'],
        'jquery_cookie': ['jquery'],
        'jqueryUi': ['jquery'],
        'moment_java': ['moment'],
        'ckeditor-jquery': {
            deps: ['jquery', 'ckeditor-core']
        },

/*
        'data_table' : ['jquery', 'bootstrap'],
*/

        'boot_table' : ['jquery', 'bootstrap'],
        'boot_table_ru' : ['jquery', 'bootstrap', 'boot_table'],
        'stacktrace': {
            exports: 'printStackTrace'
        },

        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ["underscore", "jquery"],
            exports: "backbone"
        },
        backbone_queryparams: ['backbone'],
        'inputmask': ['jquery'],
        'breadcrumbs': ['backbone']
    }
});


require(["jquery", "bootstrap", "boot", "jquery_cookie", "moment_java"], function ($, bootstrap, boot, jquery_cookie) {

    /*
     Configure layout
     */
    $(document).mouseup(function (e) {
        $('body').css('cursor', 'normal');
        $(document).unbind('mousemove');
    });

    // todo: need to move this logic of appropriate page view or block view

    $('.leftpanel').mousemove(function (e) {
        var x = e.pageX - $(this).offset().left;
        var total_width = $(this).width();
        total_width += (parseInt($(this).css('padding-left')) + parseInt($(this).css('padding-right')));
        total_width += (parseInt($(this).css('margin-left')) + parseInt($(this).css('margin-right')));
        var min = 0;
        var max = 3;
        if ((total_width - x) > min && (total_width - x) < max) {
            $(this).css('cursor', 'col-resize');
            $(this).data('can_resize', true);
        } else {
            $(this).css('cursor', 'normal');
            $(this).data('can_resize', false);
        }
    });

    $('.rightpanel').mousemove(function (e) {
        var x = e.pageX - $(this).offset().left;
        if (x >= 0 && x < 3) {
            $(this).css('cursor', 'col-resize');
            $(this).data('can_resize', true);
        } else {
            $(this).css('cursor', 'normal');
            $(this).data('can_resize', false);
        }
    });

    $('.panel').mousedown(function (e) {
        var $panel = $(this);
        if ($panel.data('can_resize') == true) {
            e.preventDefault();
            $(document).mousemove(function (e) {
                $('body').css('cursor', 'col-resize');
                var max = parseInt($panel.css("max-width"));
                var min = parseInt($panel.css("min-width"));
                var width = $panel.width();

                var new_width;
                if ($panel.hasClass('rightpanel')) {
                    new_width = $(window).width() - e.pageX;
                } else if ($panel.hasClass('leftpanel')) {
                    new_width = e.pageX;
                }

                if (((width > max) && (new_width > width)) ||
                    ((width < min) && (new_width < width))) {
                    return;
                }
                $panel.css("width", new_width);
            });
        }
    });

    /*
     Run boot function
     */
    boot();
});