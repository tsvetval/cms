package ddl.creator;

import com.google.inject.ImplementedBy;
import ddl.creator.impl.DDLGeneratorPostgresImpl;

/**
 */
@ImplementedBy(DDLGeneratorPostgresImpl.class)
public interface DDLGenerator {
  public static final String SEQUENCE_POSTFIX = "_SEQ";

  boolean tableExist(String tableName);

  void createTable(String tableName);

  void renameTable(String tableName, String newTableName);

  void dropTable(String tableName);

  void addColumn(String tableName, String columnName, Class type);

  void addColumnIfNotExists(String tableName, String columnName, Class type);

  void addColumn(String tableName, String columnName, Class type, String defaultValue);

  void addColumn(String tableName, String columnName, Class type, String defaultValue, Boolean notNull);

  //    ALTER TABLE tttt ADD COLUMN e character varying(2333) NOT NULL;
  void addColumn(String tableName, String columnName, Class type, String defaultValue, Boolean notNull, int size);

  //ALTER TABLE test2 RENAME rr  TO rr33;
  void renameColumn(String tableName, String columnName, String newColumnName);

  //ALTER TABLE test2 DROP COLUMN ecded;
  void dropColumn(String tableName, String columnName);

    //ALTER TABLE "Book" DROP CONSTRAINT "Book_PK";
    void dropPrimaryKey(String tableName);
  //ALTER TABLE test2 ALTER COLUMN ecded TYPE text;
  void changeType(String tableName, String columnName, Class newType);

  //ALTER TABLE test2 ADD CONSTRAINT pk PRIMARY KEY (id);
  void createPrimaryKey(String tableName, String columnName);

  boolean sequenceExists(String sequenceName);

  void createSequence(String sequenceName);

  void renameSequence(String sequenceName, String newSequenceName);

  void dropSequence(String sequenceName);

  //CREATE INDEX rfe
  //ON test2 (id ASC NULLS LAST);
  void createIndex(String tableName, String indexName, String columnName);

  void dropIndex(String indexName);

  //ALTER TABLE test2 ADD CONSTRAINT ee FOREIGN KEY (id) REFERENCES ee (ww) ON UPDATE NO ACTION ON DELETE NO ACTION;
  void createForeignKey(String tableName, String fkName, String columnName, String refTableName, String refColumnName);

  void dropForeignKey(String tableName, String fkName);

  void createView(String viewName, String sqlQuery);

  void dropView(String viewName);

  boolean columnExist(String tableName, String fieldName);

  void changeDefaultSQLValue(String tableName, String columnName, String defaultValue, boolean widthDelimeter);

}
