package ddl.creator;

import ddl.creator.impl.DDLGeneratorPostgresImpl;
import org.eclipse.persistence.jpa.JpaEntityManager;

import javax.persistence.EntityManager;

/**
 */
public class DDLGeneratorFactory {

    public static DDLGenerator createDDLGenerator(EntityManager entityManager) {
        DDLGenerator ddlGenerator;
        if (entityManager.unwrap(JpaEntityManager.class).getSession().getPlatform().toString().equalsIgnoreCase("PostgreSQLPlatform")){
            ddlGenerator = new DDLGeneratorPostgresImpl(entityManager);
        }else{
            throw new RuntimeException("Платформа "+entityManager.unwrap(JpaEntityManager.class).getSession().getPlatform().toString()+" не поддерживается!");
        }
        return ddlGenerator;
    }
}
