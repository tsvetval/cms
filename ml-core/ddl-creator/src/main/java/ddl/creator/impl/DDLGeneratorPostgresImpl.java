package ddl.creator.impl;

import com.google.inject.Inject;
import ddl.creator.DDLGenerator;
import org.eclipse.persistence.internal.databaseaccess.FieldTypeDefinition;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 */
public class DDLGeneratorPostgresImpl implements DDLGenerator {

    private static final Logger log = LoggerFactory.getLogger(DDLGeneratorPostgresImpl.class);

   // @Inject
    private EntityManager entityManager;
    private String startDelimeter;
    private String endDelimiter;

    @Inject
    public DDLGeneratorPostgresImpl( EntityManager entityManager) {
        this.entityManager = entityManager;
        startDelimeter = entityManager.unwrap(JpaEntityManager.class).getAbstractSession().getPlatform().getStartDelimiter();
        endDelimiter = entityManager.unwrap(JpaEntityManager.class).getAbstractSession().getPlatform().getEndDelimiter();
    }

    @Override
    public boolean columnExist(String tableName, String fieldName) {
        String sql = "SELECT EXISTS(\n" +
                " SELECT * \n" +
                " FROM information_schema.columns " +
                " WHERE table_name=? and column_name=? "+
                ");";
        Query query = entityManager.createNativeQuery(sql).setParameter(1,tableName).setParameter(2,fieldName);
        return (Boolean) query.getSingleResult();
    }

    @Override
    public void changeDefaultSQLValue(String tableName, String columnName, String defaultValue, boolean widthDelimeter) {
        String sql = "ALTER TABLE "+ withDelimeters(tableName)
                +" ALTER COLUMN "+ withDelimeters(columnName)+ " SET DEFAULT ";
        if(widthDelimeter){
            sql += "'"+defaultValue+"'" ;
        }else{
            sql += defaultValue ;
        }
        execSQL(sql);
    }

    @Override
    public boolean tableExist(String tableName) {
        String sql = "SELECT EXISTS(\n" +
                "    SELECT * \n" +
                "    FROM information_schema.tables \n" +
                "    WHERE  \n" +
                "      table_name = ?\n" +
                ");";
        Query query = entityManager.createNativeQuery(sql).setParameter(1,tableName);
        return (Boolean) query.getSingleResult();
    }

    @Override
    public void createTable(String tableName) {
        String sql = "CREATE TABLE " + withDelimeters(tableName) + " ()";
        execSQL(sql);
    }

    @Override
    public void renameTable(String tableName, String newTableName) {
        String sql = "ALTER TABLE " + withDelimeters(tableName) + " RENAME TO " + withDelimeters(newTableName);
        execSQL(sql);
    }

    @Override
    public void dropTable(String tableName) {
        String sql = "DROP TABLE " + withDelimeters(tableName);
        execSQL(sql);
    }

    @Override
    public void addColumn(String tableName, String columnName, Class type) {
        addColumn(tableName, columnName, type, "",false);
    }

    @Override
    public void addColumn(String tableName, String columnName, Class type, String defaultValue) {
        addColumn(tableName,columnName,type,defaultValue,false);
    }

  @Override
  public void addColumnIfNotExists(String tableName, String columnName, Class type) {
    if (!columnExist(tableName, columnName)) {
      addColumn(tableName, columnName, type);
    }
  }
    @Override
    public void addColumn(String tableName, String columnName, Class type, String defaultValue, Boolean notNull) {
        FieldTypeDefinition definition = entityManager.unwrap(JpaEntityManager.class).getAbstractSession().getPlatform().getFieldTypeDefinition(type);
        addColumn(tableName, columnName, type,defaultValue, notNull, definition.getDefaultSize());
    }

    //    ALTER TABLE tttt ADD COLUMN e character varying(2333) NOT NULL;
    @Override
    public void addColumn(String tableName, String columnName, Class type, String defaultValue, Boolean notNull, int size) {
        FieldTypeDefinition definition = entityManager.unwrap(JpaEntityManager.class).getAbstractSession().getPlatform().getFieldTypeDefinition(type);
        String sql = "ALTER TABLE " + withDelimeters(tableName) + " ADD COLUMN " + withDelimeters(columnName) + " " + definition.getName();
        if (definition.isSizeRequired()) {
            sql += " (" + size + ")";
        }
        if (notNull) {
            sql += " NOT NULL";
        }
        if(defaultValue == null || !"".equals(defaultValue)){
            sql += " DEFAULT "+defaultValue;
        }
        execSQL(sql);
    }

    //ALTER TABLE test2 RENAME rr  TO rr33;
    @Override
    public void renameColumn(String tableName, String columnName, String newColumnName) {
        String sql = "ALTER TABLE " + withDelimeters(tableName) + " RENAME " + withDelimeters(columnName) + " TO " + withDelimeters(newColumnName);
        execSQL(sql);
    }

    //ALTER TABLE test2 DROP COLUMN ecded;
    @Override
    public void dropColumn(String tableName, String columnName) {
        String sql = "ALTER TABLE " + withDelimeters(tableName) + " DROP COLUMN " + withDelimeters(columnName);
        execSQL(sql);
    }

    //ALTER TABLE test2 ALTER COLUMN ecded TYPE text;
    @Override
    public void changeType(String tableName, String columnName, Class newType) {
        FieldTypeDefinition definition = entityManager.unwrap(JpaEntityManager.class).getAbstractSession().getPlatform().getFieldTypeDefinition(newType);
        String sql = "ALTER TABLE " + withDelimeters(tableName) + " ALTER COLUMN " + withDelimeters(columnName) + " TYPE " + definition.getName();
        if (definition.isSizeRequired()) {
            sql += " (" + definition.getDefaultSize() + ")";
        }
        execSQL(sql);
    }

    //ALTER TABLE test2 ADD CONSTRAINT pk PRIMARY KEY (id);
    @Override
    public void createPrimaryKey(String tableName, String columnName) {
        String sql = "ALTER TABLE " + withDelimeters(tableName) + " ADD CONSTRAINT " + withDelimeters( tableName+"_PK") + " PRIMARY KEY (" + withDelimeters(columnName) + ")";
        execSQL(sql);
    }
    //ALTER TABLE "Book" DROP CONSTRAINT "Book_PK";
    @Override
    public void dropPrimaryKey(String tableName) {
        String sql = "ALTER TABLE " + withDelimeters(tableName) + " DROP CONSTRAINT IF EXISTS " + withDelimeters(tableName+"_PK");
        execSQL(sql);
    }

    @Override
    public boolean sequenceExists(String sequenceName) {
        String sql = "SELECT EXISTS(\n" +
                " SELECT * \n" +
                " FROM information_schema.sequences " +
                " WHERE sequence_name=?"+
                ");";
        Query query = entityManager.createNativeQuery(sql).setParameter(1,sequenceName);
        return (Boolean) query.getSingleResult();
    }


    @Override
    public void createSequence(String sequenceName) {
        String sql = "CREATE SEQUENCE " + withDelimeters(sequenceName);
        execSQL(sql);
    }

    @Override
    public void renameSequence(String sequenceName, String newSequenceName) {
        String sql = "ALTER TABLE "+withDelimeters(sequenceName)+" RENAME TO " + withDelimeters(newSequenceName);
        execSQL(sql);
    }

    @Override
    public void dropSequence(String sequenceName) {
        String sql = "DROP SEQUENCE " + withDelimeters(sequenceName);
        execSQL(sql);
    }

    //CREATE INDEX rfe
    //ON test2 (id ASC NULLS LAST);
    @Override
    public void createIndex(String tableName, String indexName, String columnName) {
        String sql = "CREATE INDEX " + withDelimeters(indexName) + " ON " + withDelimeters(tableName) + " (" + withDelimeters(columnName) + ")";
        execSQL(sql);
    }

    @Override
    public void dropIndex(String indexName) {
        String sql = "DROP INDEX " + withDelimeters(indexName);
        execSQL(sql);
    }

    //ALTER TABLE test2 ADD CONSTRAINT ee FOREIGN KEY (id) REFERENCES ee (ww) ON UPDATE NO ACTION ON DELETE NO ACTION;
    @Override
    public void createForeignKey(String tableName, String fkName, String columnName, String refTableName, String refColumnName) {
        String sql = "ALTER TABLE " + withDelimeters(tableName) + " ADD CONSTRAINT " + withDelimeters(fkName) + " FOREIGN KEY (" +
                withDelimeters(columnName) + ") REFERENCES " + withDelimeters(refTableName) + " (" + withDelimeters(refColumnName) + ")";
        execSQL(sql);
    }

    @Override
    public void dropForeignKey(String tableName, String fkName) {
        String sql = "ALTER TABLE " + withDelimeters(tableName) + " DROP CONSTRAINT " + withDelimeters(fkName);
        execSQL(sql);
    }

    @Override
    public void createView(String viewName, String sqlQuery) {
        String sql = " CREATE VIEW " + withDelimeters(viewName) + " AS " + sqlQuery;
        execSQL(sql);
    }

    @Override
    public void dropView(String viewName) {
        String sql = "DROP VIEW " + withDelimeters(viewName);
        execSQL(sql);
    }

    private String withDelimeters(String dbName) {
        return startDelimeter + dbName + endDelimiter;
    }

    private void execSQL(String sql) {
        log.debug(sql);
        entityManager.createNativeQuery(sql).executeUpdate();
    }
}
