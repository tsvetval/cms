package test;

import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.eclipse.persistence.dynamic.DynamicClassLoader;
import org.eclipse.persistence.logging.SessionLog;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class EntityManagerBuilder {
    public static volatile EntityManagerBuilder instance;

    public static EntityManagerBuilder getInstance() {
        if (instance == null){
            instance = new EntityManagerBuilder();
        }
        return instance;
    }


    private EntityManagerBuilder(){

    }



    DynamicClassLoader dcl = new DynamicClassLoader(Thread.currentThread().getContextClassLoader()/*currentLoader*/);
    EntityManagerFactory entityManagerFactory;

    public DynamicClassLoader getDcl() {
        return dcl;
    }

    public void setDcl(DynamicClassLoader dcl) {
        this.dcl = dcl;
    }

    /*    private DataSource createDataSource() {
        ClientDataSource dataSource = new ClientDataSource();
        dataSource.setServerName("localhost");
        dataSource.setPortNumber(1527);
        dataSource.setDatabaseName("sample");
        dataSource.setUser("app");
        dataSource.setPassword("app");
        return dataSource;
    }*/

    public EntityManagerFactory getEntityManagerFactory() {
        Map<String, Object> dbProps = new HashMap<String, Object>();
        //DynamicClassLoader dcl = new DynamicClassLoader(Thread.currentThread().getContextClassLoader()/*currentLoader*/);

        dbProps.put(PersistenceUnitProperties.DDL_GENERATION, PersistenceUnitProperties.CREATE_OR_EXTEND/*DROP_AND_CREATE*/);
        dbProps.put(PersistenceUnitProperties.DDL_GENERATION_MODE, PersistenceUnitProperties.DDL_DATABASE_GENERATION/*DDL_DATABASE_GENERATION*/);

        dbProps.put(PersistenceUnitProperties.NON_JTA_DATASOURCE, "");
        dbProps.put(PersistenceUnitProperties.JTA_DATASOURCE, "");

        dbProps.put(PersistenceUnitProperties.CATEGORY_LOGGING_LEVEL_ + SessionLog.SQL, "FINE");

        dbProps.put(PersistenceUnitProperties.JDBC_URL,
                String.format("jdbc:postgresql://%s:%s/%s",
                        "localhost",
                        "5432",
                        "testddlcreator"));

        dbProps.put(PersistenceUnitProperties.JDBC_DRIVER, "org.postgresql.Driver");
        dbProps.put(PersistenceUnitProperties.JDBC_USER, "postgres");
        dbProps.put(PersistenceUnitProperties.JDBC_PASSWORD, "postgres");

        /*ADD after*/
        dbProps.put(PersistenceUnitProperties.CLASSLOADER, dcl);
        dbProps.put(PersistenceUnitProperties.WEAVING, "static");

        entityManagerFactory = Persistence.createEntityManagerFactory("myTestPU", dbProps);
        return entityManagerFactory;
        //return fact.createEntityManager();
    }

    public EntityManagerFactory getEntityManagerFactoryCreated() {
        return entityManagerFactory;

    }

    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }
}

