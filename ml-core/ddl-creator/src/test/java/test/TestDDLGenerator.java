package test;

import ddl.creator.DDLGenerator;
import ddl.creator.DDLGeneratorFactory;
import org.junit.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created with IntelliJ IDEA.
 * User: d_litovchenko
 * Date: 06.03.14
 * Time: 17:44
 * To change this template use File | Settings | File Templates.
 */
public class TestDDLGenerator {

    private static DDLGenerator ddlGenerator;
    private static EntityManager entityManager;
    private static Connection connection;

    @BeforeClass
    public static void initDB() throws Exception{
        Class.forName("org.postgresql.Driver");
         connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/", "postgres", "postgres");
        System.out.println("create database testddlcreator");
        connection.createStatement().execute("create database testddlcreator");
        EntityManagerBuilder entityManagerBuilder = EntityManagerBuilder.getInstance();
        EntityManagerFactory emf = entityManagerBuilder.getEntityManagerFactory();
        entityManager = emf.createEntityManager();
        ddlGenerator = DDLGeneratorFactory.createDDLGenerator(entityManager);
    }

    @AfterClass
    public static void dropDB() throws Exception{

        System.out.println("drop database testddlcreator");
        connection.createStatement().execute(" SELECT pg_terminate_backend(pg_stat_activity.procpid) " +
                " FROM pg_stat_activity" +
                " WHERE pg_stat_activity.datname = 'testddlcreator'" +
                "    AND procpid <> pg_backend_pid()");
        connection.createStatement().execute("drop database testddlcreator");
    }

    @Before
    public void openTransaction(){
        System.out.println("open session");
        entityManager.getTransaction().begin();
    }
    @After
    public void closeTransaction(){
        System.out.println("close session");
        entityManager.getTransaction().commit();
    }
    @Test
    public void testCreateTable(){
        ddlGenerator.createTable("test");
        entityManager.createNativeQuery("select * from \"test\"").getResultList();
        ddlGenerator.dropTable("test");
    }

    @Test
    public void testRenameTable(){
        ddlGenerator.createTable("test");
        ddlGenerator.renameTable("test", "test2");
        entityManager.createNativeQuery("select * from \"test2\"").getResultList();
        ddlGenerator.dropTable("test2");
    }

    @Test
    public void addColumnToTable(){
        ddlGenerator.createTable("test");
        ddlGenerator.addColumn("test", "rrr",Long.class);
        entityManager.createNativeQuery("select \"rrr\" from \"test\"").getResultList();
        ddlGenerator.dropTable("test");
    }

    @Test
    public void renameColumn(){
        ddlGenerator.createTable("test");
        ddlGenerator.addColumn("test", "rrr",Long.class);
        ddlGenerator.renameColumn("test", "rrr", "id");
        entityManager.createNativeQuery("select \"id\" from \"test\"").getResultList();
        ddlGenerator.dropTable("test");
    }

    @Test
    public void columnChangeType(){
        ddlGenerator.createTable("test");
        ddlGenerator.addColumn("test", "rrr",Long.class);
        ddlGenerator.changeType("test","rrr",String.class);
        ddlGenerator.dropTable("test");
    }

    @Test
    public void createSequence(){
        ddlGenerator.createSequence("test_seq");
        ddlGenerator.dropSequence("test_seq");
    }

    @Test
    public void createIndex(){
        ddlGenerator.createTable("test");
        ddlGenerator.addColumn("test", "rrr", Long.class);
        ddlGenerator.createIndex("test", "index1", "rrr");
        ddlGenerator.dropIndex("index1");
        ddlGenerator.dropTable("test");
    }

    @Test
    public void createPK(){
        ddlGenerator.createTable("test");
        ddlGenerator.addColumn("test", "rrr", Long.class);
        ddlGenerator.createPrimaryKey("test", "rrr");
        ddlGenerator.dropTable("test");
    }

    @Test
    public void createFK(){
        ddlGenerator.createTable("test");
        ddlGenerator.addColumn("test", "id", Long.class);
        ddlGenerator.createPrimaryKey("test", "id");
        ddlGenerator.createTable("test2");
        ddlGenerator.addColumn("test2", "rrr", Long.class);
        ddlGenerator.createForeignKey("test2", "kg", "rrr", "test", "id");
        ddlGenerator.dropTable("test2");
        ddlGenerator.dropTable("test");
    }

}
