package ru.ml.core.common;

import java.sql.Clob;
import java.sql.Timestamp;

/**
 *
 */
public enum AttrType {
    STRING(String.class),
    LONG(Long.class),
    BOOLEAN(Boolean.class),
    DATE(Timestamp.class),
    ENUM(String.class),  /*перечислимое ппердставляется просто строкой, перечислимое хранятся в таблице Enumerate*/
    ONE_TO_MANY(Long.class), /*Обратная ссылка*/
    MANY_TO_ONE(Long.class),
    MANY_TO_MANY(Long.class),
    LONG_LINK(String.class), /*Дальняя ссылка предназначена для отображения дальних связей объекта (например Человек->Паспорт->МРЕО->Назвние)*/
    DOUBLE(Double.class),
    TEXT(Clob.class),
    FILE(byte[].class),
    ONE_TO_ONE(Long.class),
    HETERO_LINK(Long.class) ;

    private AttrType(Class aClass) {
        this.aClass = aClass;
    }

    private Class aClass;

    public Class getaClass() {
        return aClass;
    }
}
