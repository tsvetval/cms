package ru.ml.core.common;

/**
 * Created by d_litovchenko on 19.02.15.
 */
public enum FetchType {
    NONE,
    OUTER_JOIN,
    INNER_JOIN,
    BATCH_JOIN,
    BATCH_EXISTS,
    BATCH_IN
}
