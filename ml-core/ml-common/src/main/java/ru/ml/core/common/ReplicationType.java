package ru.ml.core.common;

/**
 */
public enum ReplicationType {
    NOT_REPLICATE,
    REPLICATE,
    CASCADE_REPLICATE
}
