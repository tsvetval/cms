package ru.ml.core.common;

/**
 */
public class RoleType {
    public static final String ANONYMOUS_ROLE = "ANONYMOUS_ROLE";
    public static final String MAIN_ADMIN_ROLE = "MAIN_ADMIN_ROLE";
    public static final String ADMIN_ROLE = "ADMIN_ROLE";
    public static final String USERS_ROLE = "USERS_ROLE";
}
