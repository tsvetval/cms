package ru.ml.core.common.exceptions;

public class MlApplicationException extends RuntimeException {
    public MlApplicationException(String message) {
        super(message);
    }

    public MlApplicationException(String message, Throwable cause) {
        super(message, cause);    //To change body of overridden methods use File | Settings | File Templates.
    }
}
