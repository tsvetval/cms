package ru.ml.core.common.exceptions;

/**
 *
 */
public class MlSecurityException extends RuntimeException {
    public MlSecurityException(String message) {
        super(message);
    }

    public MlSecurityException(String message, Throwable cause) {
        super(message, cause);
    }
}
