package ru.ml.core.common.exceptions;

public class MlServerException extends RuntimeException {
    public MlServerException(String message, Throwable cause) {
        super(message, cause);
    }

    public MlServerException(String message) {
        super(message);
    }
}
