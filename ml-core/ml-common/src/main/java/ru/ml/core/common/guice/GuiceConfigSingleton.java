package ru.ml.core.common.guice;


import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Module;

import java.util.List;

/**
 *
 */
public class GuiceConfigSingleton {
    private volatile static GuiceConfigSingleton instance;
    private Injector injector;

    MlDynamicClassLoader dcl = new MlDynamicClassLoader(Thread.currentThread().getContextClassLoader()/*currentLoader*/);


    private GuiceConfigSingleton(List<Module> modules) {
        injector = Guice.createInjector(modules);
        //injector.getInstance(PersistService.class).start();
    }

    public static GuiceConfigSingleton getInstance(List<Module> modules) {
        if (instance == null) {
            synchronized (GuiceConfigSingleton.class) {
                if (instance == null)
                    instance = new GuiceConfigSingleton(modules);
            }
        }
        return instance;
    }

    public static GuiceConfigSingleton getInstance() {
        if (instance != null) {
            return instance;
        } else {
            throw new RuntimeException("Не инициирован инжектор Guice");
        }
    }

    public static <T> T inject(Class<T> aClass){
        return instance.getInjector().getInstance(aClass);
    }
    public static <T> T inject(Key<T> aClass){
        return instance.getInjector().getInstance(aClass);
    }

    public void setInjector(Injector injector) {
        this.injector = injector;
    }

    public Injector getInjector() {
        return injector;
    }

    public MlDynamicClassLoader getDcl() {
        return dcl;
    }
}
