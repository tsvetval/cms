package ru.ml.core.common.guice;

import com.google.inject.Module;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: d_litovchenko
 * Date: 04.08.14
 * Time: 10:36
 * To change this template use File | Settings | File Templates.
 */
public interface IGuiceModules {
    public List<Module> getGuiceModules();
}
