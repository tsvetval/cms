package ru.ml.core.common.guice;

import org.eclipse.persistence.dynamic.DynamicClassLoader;
import org.eclipse.persistence.dynamic.DynamicClassWriter;

/**
 * Created with IntelliJ IDEA.
 * User: d_litovchenko
 * Date: 12.08.14
 * Time: 17:20
 * To change this template use File | Settings | File Templates.
 */
public class MlDynamicClassLoader extends DynamicClassLoader {
    public MlDynamicClassLoader(ClassLoader delegate) {
        super(delegate);
    }

    public MlDynamicClassLoader(ClassLoader delegate, DynamicClassWriter writer) {
        super(delegate, writer);
    }

    public void removeClassWriter(String className){
        getClassWriters().remove(className);
    }

    public boolean isClassNotLoaded(String className){
        return findLoadedClass(className) == null;
    }
}
