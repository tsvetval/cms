package ru.peak.ml.core.filter;

import ru.peak.ml.core.model.system.MlAttr;

/**
 */
public class ExtendedFilterParameter {
    private String paramPostfix;
    private String name;
    private Operation operation;
    private Object value;
    private String paramPrefix;
    private MlAttr mlAttr;

    public enum Operation{
        equals,
        not_equals,
        less,
        greater,
        less_or_equals,
        greater_or_equals,

        contains,
        not_contains,
        startsWith,
        endsWith,

        from,
        to
    }

    public ExtendedFilterParameter(String name, String operation, Object value) {
        this.name = name;
        this.operation = Operation.valueOf(operation);
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getParamPrefix() {
        return paramPrefix;
    }

    public void setParamPrefix(String paramPrefix) {
        this.paramPrefix = paramPrefix;
    }

    public MlAttr getMlAttr() {
        return mlAttr;
    }

    public void setMlAttr(MlAttr mlAttr) {
        this.mlAttr = mlAttr;
    }

    public String getParamPostfix() {
        return paramPostfix;
    }

    public void setParamPostfix(String paramPostfix) {
        this.paramPostfix = paramPostfix;
    }
}
