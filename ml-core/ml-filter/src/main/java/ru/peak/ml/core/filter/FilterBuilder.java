package ru.peak.ml.core.filter;

import com.google.inject.Inject;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.filter.result.ExtendedFilterResult;
import ru.peak.ml.core.filter.result.FilterResult;
import ru.peak.ml.core.filter.result.SimpleFilterResult;
import ru.peak.ml.core.filter.strategy.ExtendedFilterStrategy;
import ru.peak.ml.core.filter.strategy.FilterStrategy;
import ru.peak.ml.core.filter.strategy.NoneFilterStrategy;
import ru.peak.ml.core.filter.strategy.SimpleFilterStrategy;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.system.MlClass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by d_litovchenko on 09.04.15.
 */
public class FilterBuilder {
    @Inject
    MetaDataHolder metaDataHolder;

    private Strategy strategy = Strategy.none;
    private String simpleSearchValue;
    private String simpleSearchAttrName;
    private Long rowPerPage = 20L;
    private Long pageNumber = 1L;
    private List<ExtendedFilterParameter> extendedFilterParameters = new ArrayList<>();
    private List<String> additionalCondition = new ArrayList<>();
    private String orderAttr;
    private String orderType;
    private Map<String,Object> additionalParameters = new HashMap<>();

    public enum Strategy {simple,extended,none}

    private MlClass queryMlClass;

    public MlClass getQueryMlClass() {
        return queryMlClass;
    }

    public FilterBuilder setQueryMlClass(MlClass queryMlClass) {
        this.queryMlClass = queryMlClass;
        return this;
    }

    public FilterBuilder setQueryMlClass(String queryMlClassName) {
        this.queryMlClass = metaDataHolder.getMlClassByName(queryMlClassName);
        return this;
    }

    public FilterResult buildFilterResult(){
        if(strategy == null){
            throw new MlServerException("Тип фильтра не выбран");
        }
        FilterStrategy filterStrategy = null;
        FilterResult filterResult;
        if(getQueryMlClass() == null){
            throw new MlServerException("Не указан MlClass для выборки");
        }
        switch (strategy){
            case extended:
                filterResult = new ExtendedFilterResult();
                ((ExtendedFilterResult)filterResult).setExtendedFilterParameters(getExtendedFilterParameters());
                filterStrategy = GuiceConfigSingleton.inject(ExtendedFilterStrategy.class);
                break;
            case simple:
                filterResult = new SimpleFilterResult();
                ((SimpleFilterResult)filterResult).setSimpleSearchValue(getSimpleSearchValue());
                ((SimpleFilterResult)filterResult).setSimpleSearchAttrName(getSimpleSearchAttrName());
                filterStrategy = GuiceConfigSingleton.inject(SimpleFilterStrategy.class);
                break;
            case none:
                filterResult = new FilterResult();
                filterStrategy = GuiceConfigSingleton.inject(NoneFilterStrategy.class);
                break;
            default:
                throw new MlServerException(String.format("Filter strategy %s not implemented",strategy.name()));
        }

        filterResult.setStrategy(strategy);
        filterResult.setQueryMlClass(getQueryMlClass());
        filterResult.setRowPerPage(getRowPerPage());
        filterResult.setPageCurrent(getPageNumber());
        filterResult.setAdditionalParamsMap(getAdditionalParameters());
        filterResult.setAdditionalConditions(getAdditionalCondition());
        filterResult.setOrderAttr(getOrderAttr());
        filterResult.setOrderType(getOrderType());
        filterStrategy.setFilterResult(filterResult);
        filterResult = filterStrategy.createResult();

        return filterResult;
    }

    public Map<String, Object> getAdditionalParameters() {
        return additionalParameters;
    }

    public FilterBuilder setRowPerPage(Long rowPerPage) {
        this.rowPerPage = rowPerPage;
        return this;
    }

    public FilterBuilder setPageNumber(Long pageNumber) {
        this.pageNumber = pageNumber;
        return this;
    }

    public FilterBuilder setAdditionalParameters(Map<String, Object> additionalParameters) {
        this.additionalParameters = additionalParameters;
        return this;
    }

    /* simple filter*/
    public FilterBuilder useSimpleSearch(){
        this.strategy = Strategy.simple;
        return this;
    }

    public FilterBuilder setSimpleSearchValue(String simpleSearchValue){
        this.simpleSearchValue = simpleSearchValue;
        return this;
    }
    public FilterBuilder setSimpleSearchAttr(String attrName){
        this.simpleSearchAttrName = attrName;
        return this;
    }

    /* Extended filter*/
    public FilterBuilder useExtendedSearch(){
        this.strategy = Strategy.extended;
        return this;
    }

    public FilterBuilder addExtendedParameter(String attrName, String operation, Object value) {
        extendedFilterParameters.add(new ExtendedFilterParameter(attrName,operation,value));
        return this;
    }

    public String getSimpleSearchValue() {
        return simpleSearchValue;
    }

    public String getSimpleSearchAttrName() {
        return simpleSearchAttrName;
    }

    public void setSimpleSearchAttrName(String simpleSearchAttrName) {
        this.simpleSearchAttrName = simpleSearchAttrName;
    }
    public Long getRowPerPage() {
        return rowPerPage;
    }

    public Long getPageNumber() {
        return pageNumber;
    }

    public List<ExtendedFilterParameter> getExtendedFilterParameters() {
        return extendedFilterParameters;
    }

    public List<String> getAdditionalCondition() {
        return additionalCondition;
    }

    public FilterBuilder addAdditionalCondition(String additionalCondition) {
        if(additionalCondition!=null){
            this.additionalCondition.add(additionalCondition);
        }
        return this;
    }

    public FilterBuilder addAdditionalCondition(List<String> additionalCondition) {
        this.additionalCondition.addAll(additionalCondition);
        return this;
    }

    public String getOrderAttr() {
        return orderAttr;
    }

    public void setOrderAttr(String orderAttr) {
        this.orderAttr = orderAttr;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }
}

