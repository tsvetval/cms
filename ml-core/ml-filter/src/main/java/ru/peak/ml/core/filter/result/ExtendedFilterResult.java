package ru.peak.ml.core.filter.result;

import ru.peak.ml.core.filter.ExtendedFilterParameter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by d_litovchenko on 14.04.15.
 */
public class ExtendedFilterResult extends FilterResult {

    private List<ExtendedFilterParameter> extendedFilterParameters = new ArrayList<>();

    public List<ExtendedFilterParameter> getExtendedFilterParameters() {
        return extendedFilterParameters;
    }

    public void setExtendedFilterParameters(List<ExtendedFilterParameter> extendedFilterParameters) {
        this.extendedFilterParameters = extendedFilterParameters;
    }
}
