package ru.peak.ml.core.filter.result;

import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.filter.FilterBuilder;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.system.MlClass;

import javax.persistence.Parameter;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Фильтр
 */
public class FilterResult {
    private List<MlDynamicEntityImpl> resultList;
    private Long recordsCount;
    private String rowQuery;
    private Long pagesCount;
    private String countRecordQuery;

    private Long pageCurrent;
    private Long rowPerPage;
    private MlClass queryMlClass;
    private Map<String,Object> paramsMap = new HashMap<>();
    private Map<String,Object> additionalParamsMap;
    private FilterBuilder.Strategy strategy;
    private List<String> additionalConditions;
    private String orderAttr;
    private String orderType;


    public List<MlDynamicEntityImpl> getResultList() {
       return getResultList(true);
    }

    public List<MlDynamicEntityImpl> getResultList(boolean checkSecurity) {
        if(resultList == null){
            CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
            Query query;
            if (checkSecurity) {
                query = commonDao.getQueryWithSecurityCheck(rowQuery, queryMlClass.getEntityName());
            } else {
                query = commonDao.getQueryWithoutSecurityCheck(rowQuery);
            }

            query.setFirstResult((int) ((pageCurrent - 1) * rowPerPage)).setMaxResults(rowPerPage.intValue());
            for(String paramName: paramsMap.keySet()){
                query.setParameter(paramName,paramsMap.get(paramName));
            }
            putAdditionalParameters(query);
            resultList = query.getResultList();
        }
        return resultList;
    }

    private void putAdditionalParameters(Query query) {
        for(String paramName: additionalParamsMap.keySet()){
            for(Parameter parameter: query.getParameters()){
                if(parameter.getName().equals(paramName)){
                    query.setParameter(paramName,additionalParamsMap.get(paramName));
                }
            }
        }
    }
    public Long getRecordsCount() {
     return  getRecordsCount(true);
    }

    public Long getRecordsCount(boolean checkSecurity) {
        if(recordsCount == null){
            CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
            Query query;
            if (checkSecurity) {
                query = commonDao.getQueryWithSecurityCheck(countRecordQuery, queryMlClass.getEntityName());
            } else {
                query = commonDao.getQueryWithoutSecurityCheck(countRecordQuery);
            }

            for(String paramName: paramsMap.keySet()){
                query.setParameter(paramName,paramsMap.get(paramName));
            }
            putAdditionalParameters(query);
            recordsCount = (Long) query.getSingleResult();
            pagesCount = recordsCount / rowPerPage;
            long addOnePage = recordsCount % rowPerPage;
            if (addOnePage > 0) {
                pagesCount++;
            }
        }
        return recordsCount;
    }

    public void setRecordsCount(Long recordsCount) {
        this.recordsCount = recordsCount;
    }


    public Long getPagesCount() {
      return getPagesCount(true);
    }

    public Long getPagesCount(boolean checkSecurity) {
        if (pagesCount == null ){
            getRecordsCount(checkSecurity);
        }
        return pagesCount;
    }

    public void setPagesCount(Long pagesCount) {
        this.pagesCount = pagesCount;
    }

    public Long getPageCurrent() {
        return pageCurrent;
    }

    public void setPageCurrent(Long pageCurrent) {
        this.pageCurrent = pageCurrent;
    }

    public void addParameter(String name,Object value){
        paramsMap.put(name,value);
    }

    public Long getRowPerPage() {
        return rowPerPage;
    }

    public void setRowPerPage(Long rowPerPage) {
        this.rowPerPage = rowPerPage;
    }

    public String getRowQuery() {
        return rowQuery;
    }

    public void setRowQuery(String rowQuery) {
        this.rowQuery = rowQuery;
    }

    public String getCountRecordQuery() {
        return countRecordQuery;
    }

    public void setCountRecordQuery(String countRecordQuery) {
        this.countRecordQuery = countRecordQuery;
    }

    public MlClass getQueryMlClass() {
        return queryMlClass;
    }

    public void setQueryMlClass(MlClass queryMlClass) {
        this.queryMlClass = queryMlClass;
    }

    public FilterBuilder.Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(FilterBuilder.Strategy strategy) {
        this.strategy = strategy;
    }

    public List<String> getAdditionalConditions() {
        return additionalConditions;
    }

    public void setAdditionalConditions(List<String> additionalConditions) {
        this.additionalConditions = additionalConditions;
    }

    public String getOrderAttr() {
        return orderAttr;
    }

    public void setOrderAttr(String orderAttr) {
        this.orderAttr = orderAttr;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public void setAdditionalParamsMap(Map<String, Object> additionalParamsMap) {
        this.additionalParamsMap = additionalParamsMap;
    }

}
