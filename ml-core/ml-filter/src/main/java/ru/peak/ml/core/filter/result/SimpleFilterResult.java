package ru.peak.ml.core.filter.result;

/**
 * Created by d_litovchenko on 14.04.15.
 */
public class SimpleFilterResult extends FilterResult {

    private String simpleSearchValue;
    private String simpleSearchAttrName;

    public String getSimpleSearchValue() {
        return simpleSearchValue;
    }

    public void setSimpleSearchValue(String simpleSearchValue) {
        this.simpleSearchValue = simpleSearchValue;
    }

    public String getSimpleSearchAttrName() {
        return simpleSearchAttrName;
    }

    public void setSimpleSearchAttrName(String simpleSearchAttrName) {
        this.simpleSearchAttrName = simpleSearchAttrName;
    }
}
