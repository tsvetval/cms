package ru.peak.ml.core.filter.strategy;

import ru.ml.core.common.exceptions.MlServerException;
import ru.peak.ml.core.filter.ExtendedFilterParameter;
import ru.peak.ml.core.filter.result.ExtendedFilterResult;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by d_litovchenko on 09.04.15.
 */
public class ExtendedFilterStrategy implements FilterStrategy<ExtendedFilterResult> {
    private static final String joinTableName = "o";
    private Map<String, Object> params;
    private ExtendedFilterResult filterResult;
    Integer joinTablePostfix;

    @Override
    public ExtendedFilterResult createResult() {
        params = new HashMap<>();

        String selectStatement = createSelectStatement();
        String joinCondition = appendJoinCondition();
        String whereCause = whereCause();
        String order = addOrder();
        String countSelectStatement = createCountSelectStatement();

        filterResult.setRowQuery(selectStatement + joinCondition + whereCause + order);
        filterResult.setCountRecordQuery(countSelectStatement + joinCondition + whereCause);
        for (String key : params.keySet()) {
            filterResult.addParameter(key, params.get(key));
        }
        return filterResult;
    }


    @Override
    public void setFilterResult(ExtendedFilterResult filterResult) {
        this.filterResult = filterResult;
    }

    private String createSelectStatement() {
        return "select o from " + filterResult.getQueryMlClass().getEntityName() + " o ";
    }

    private String createCountSelectStatement() {
        return "select count(o) from " + filterResult.getQueryMlClass().getEntityName() + " o ";
    }

    private String appendJoinCondition() {
        StringBuilder builder = new StringBuilder();
        joinTablePostfix = 1;
        for (ExtendedFilterParameter parameter : filterResult.getExtendedFilterParameters()) {
            String[] paths = parameter.getName().split("\\.");
            MlAttr attr = findAttr(paths, filterResult.getQueryMlClass(), 0);
            String attrName = addJoinCond(paths, builder);
            parameter.setMlAttr(attr);
            parameter.setParamPrefix(attrName);
        }
        return builder.toString();
    }

    private String addJoinCond(String[] paths, StringBuilder builder) {
        String attrPrefix = joinTableName;
        if (paths.length > 1) {
            for (int i = 0; i < paths.length - 1; i++) {
                String attrs = paths[i];
                builder.append(" join ").append(attrPrefix).append(".").append(attrs).append(" ");
                joinTablePostfix++;
                attrPrefix = joinTableName + joinTablePostfix;
                builder.append(attrPrefix).append(" ");
            }
        }
        return attrPrefix;
    }

    private String addOrder() {
        StringBuilder builder = new StringBuilder();
        if (filterResult.getOrderAttr() != null && !filterResult.getOrderAttr().isEmpty()) {
            MlAttr attr = filterResult.getQueryMlClass().getAttr(filterResult.getOrderAttr());
            if (attr == null) {
                throw new MlServerException(String.format("Can't find order attr with name [%s]", filterResult.getOrderAttr()));
            }
            builder.append(" order by o.").append(attr.getEntityFieldName());
            if (filterResult.getOrderType() != null) {
                builder.append(" ").append(filterResult.getOrderType());
            } else {
                builder.append(" asc");
            }
        }
        return builder.toString();
    }

    private MlAttr findAttr(String[] paths, MlClass queryMlClass, Integer index) {
        MlAttr res = queryMlClass.getAttr(paths[index]);
        switch (res.getFieldType()) {
            case ONE_TO_MANY:
                res = findAttr(paths, res.getAttrLinkClass(), ++index);
                break;
            case MANY_TO_MANY:
                res = findAttr(paths, res.getLinkClass(), ++index);
                break;
            case MANY_TO_ONE:
                res = findAttr(paths, res.getLinkClass(), ++index);
                break;
        }
        return res;
    }

    private String whereCause() {
        StringBuilder builder = new StringBuilder(" where 1=1 ");
        Integer i = 0;
        for (ExtendedFilterParameter parameter : filterResult.getExtendedFilterParameters()) {
            i++;
            parameter.setParamPostfix(i.toString());
            MlAttr attr = parameter.getMlAttr();
            String paramName = parameter.getParamPrefix() + "_" + attr.getEntityFieldName() + "_" + parameter.getParamPostfix();
            switch (attr.getFieldType()) {
                case TEXT:
                case STRING:
                    addStringCauseAndCastData(builder, parameter, attr, paramName);
                    break;
                case LONG:
                    addLongCauseAndCastData(builder, parameter, attr, paramName);
                    break;
                case DATE:
                    addDateCauseAndCastData(builder, parameter, attr, paramName);
                    break;
                case LONG_LINK:
                    addLongLinkCauseAndCastData(builder, parameter, attr, paramName);
                    break;
                case ENUM:
                    addEnumLinkCauseAndCastData(builder, parameter, attr, paramName);

            }
        }
        addAdditional(builder);

        return builder.toString();
    }

    private void addAdditional(StringBuilder builder) {
        if (filterResult.getAdditionalConditions() != null && !filterResult.getAdditionalConditions().isEmpty()) {
            for (String condition : filterResult.getAdditionalConditions()) {
                builder.append(" and ").append("(").append(condition).append(") ");
            }
        }
    }

    private void addEnumLinkCauseAndCastData(StringBuilder builder, ExtendedFilterParameter parameter, MlAttr attr, String paramName) {
        switch (parameter.getOperation()) {
            case equals:
                builder.append("and " + parameter.getParamPrefix() + "." + attr.getEntityFieldName() + " = :" + paramName + "\n");
                break;
            case not_equals:
                builder.append("and " + parameter.getParamPrefix() + "." + attr.getEntityFieldName() + " != :" + paramName + "\n");
                break;
        }
        params.put(paramName, parameter.getValue());
    }

    private void addLongLinkCauseAndCastData(StringBuilder builder, ExtendedFilterParameter parameter, MlAttr attr, String paramName) {
        MlAttr llAttr = null;
        MlClass mlClass = filterResult.getQueryMlClass();
        for (String path : attr.getLongLinkValue().split("->")) {
            llAttr = mlClass.getAttr(path);
            if (llAttr.getLinkClass() != null) {
                mlClass = llAttr.getLinkClass();
            }
        }
        String attrName = attr.getLongLinkValue().replaceAll("->", ".");
        switch (llAttr.getFieldType()) {
            case TEXT:
            case STRING:
                switch (parameter.getOperation()) {
                    case contains:
                        builder.append(" and UPPER(o.").append(attrName).append(") like UPPER(concat('%',:" + paramName + ",'%'))");
                        break;
                    case not_contains:
                        builder.append(" and UPPER(o.").append(attrName).append(") not like UPPER(concat('%',:" + paramName + ",'%'))");
                        break;
                }
                params.put(paramName, parameter.getValue());
                break;
            case LONG:
                switch (parameter.getOperation()) {
                    case contains:
                        builder.append(" and cast( o." + attrName + " as text) like UPPER(concat('%',:" + paramName + ",'%')) ");
                        break;
                    case not_contains:
                        builder.append(" and cast( o." + attrName + " as text) not like UPPER(concat('%',:" + paramName + ",'%')) ");
                        break;
                }
                params.put(paramName, parameter.getValue());
                break;
            case DATE:
                switch (parameter.getOperation()) {
                    case contains:
                        builder.append(" and FUNC('to_char',o." + attrName + ",'dd.mm.yyyy hh24:mi:ss' ) like concat('%',:" + paramName + ",'%')");
                        break;
                    case not_contains:
                        builder.append(" and FUNC('to_char',o." + attrName + ",'dd.mm.yyyy hh24:mi:ss' ) not like concat('%',:" + paramName + ",'%')");
                        break;

                }
                params.put(paramName, parameter.getValue());
                break;
            case ENUM:
                //TODO fix
                switch (parameter.getOperation()) {
                    case contains:
                        builder.append(" and (select upper(e.title) from MlEnum e where e.mlAttr.id = " + llAttr.getId() + " and e.code = o." + attrName + ") like concat('%',:" + paramName + ",'%')");
                        break;
                    case not_contains:
                        builder.append(" and (select upper(e.title) from MlEnum e where e.mlAttr.id = " + llAttr.getId() + " and e.code = o." + attrName + ") not like concat('%',:" + paramName + ",'%')");
                        break;
                }
                params.put(paramName, parameter.getValue());
                break;
        }
    }

    private void addDateCauseAndCastData(StringBuilder builder, ExtendedFilterParameter parameter, MlAttr attr, String paramName) {
        switch (parameter.getOperation()) {
            case equals:
                builder.append("and " + parameter.getParamPrefix() + "." + attr.getEntityFieldName() + " = :" + paramName + "\n");
                break;
            case not_equals:
                builder.append("and " + parameter.getParamPrefix() + "." + attr.getEntityFieldName() + " != :" + paramName + "\n");
                break;
            case from:
                builder.append("and " + parameter.getParamPrefix() + "." + attr.getEntityFieldName() + " >= :" + paramName + "\n");
                break;
            case to:
                builder.append("and " + parameter.getParamPrefix() + "." + attr.getEntityFieldName() + " <= :" + paramName + "\n");
                break;
        }
        if (parameter.getValue() instanceof String) {
            String dateFormatString =  "dd.MM.yyyy";
            if (attr.getFormat() != null && !attr.getFormat().trim().isEmpty()) {
                dateFormatString = attr.getFormat();
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatString);
            try {
                Date filterParam = dateFormat.parse(parameter.getValue().toString());
                Calendar cal = Calendar.getInstance();
                cal.setTime(filterParam);
                switch (parameter.getOperation()) {
                    case from:
                        if (!dateFormatString.contains("H")) {
                            cal.set(Calendar.HOUR_OF_DAY, 0);
                        }
                        if (!dateFormatString.contains("m")) {
                            cal.set(Calendar.MINUTE, 0);
                        }
                        if (!dateFormatString.contains("s")) {
                            cal.set(Calendar.SECOND, 0);
                        }
                        if (!dateFormatString.contains("S")) {
                            cal.set(Calendar.MILLISECOND, 0);
                        }
                        break;
                    case to:
                        if (!dateFormatString.contains("H")) {
                            cal.set(Calendar.HOUR_OF_DAY, 23);
                        }
                        if (!dateFormatString.contains("m")) {
                            cal.set(Calendar.MINUTE, 59);
                        }
                        if (!dateFormatString.contains("s")) {
                            cal.set(Calendar.SECOND, 59);
                        }
                        if (!dateFormatString.contains("S")) {
                            cal.set(Calendar.MILLISECOND, 999);
                        }
                        break;
                }

                params.put(paramName, cal.getTime());
            } catch (ParseException e) {
                throw new MlServerException("Неверный формат даты. Ожидаемый "+dateFormatString, e);
            }
        } else {
            params.put(paramName, parameter.getValue());
        }
    }

    private void addLongCauseAndCastData(StringBuilder builder, ExtendedFilterParameter parameter, MlAttr attr, String paramName) {
        switch (parameter.getOperation()) {
            case equals:
                builder.append("and " + parameter.getParamPrefix() + "." + attr.getEntityFieldName() + " = :" + paramName + "\n");
                break;
            case not_equals:
                builder.append("and " + parameter.getParamPrefix() + "." + attr.getEntityFieldName() + " != :" + paramName + "\n");
                break;
            case less:
                builder.append("and " + parameter.getParamPrefix() + "." + attr.getEntityFieldName() + " < :" + paramName + "\n");
                break;
            case greater:
                builder.append("and " + parameter.getParamPrefix() + "." + attr.getEntityFieldName() + " > :" + paramName + "\n");
                break;
            case less_or_equals:
                builder.append("and " + parameter.getParamPrefix() + "." + attr.getEntityFieldName() + " <= :" + paramName + "\n");
                break;
            case greater_or_equals:
                builder.append("and " + parameter.getParamPrefix() + "." + attr.getEntityFieldName() + " >= :" + paramName + "\n");
                break;
        }

        params.put(paramName, Long.parseLong(parameter.getValue().toString()));
    }

    private void addStringCauseAndCastData(StringBuilder builder, ExtendedFilterParameter parameter, MlAttr attr, String paramName) {
        switch (parameter.getOperation()) {
            case equals:
                builder.append("and UPPER(" + parameter.getParamPrefix() + "." + attr.getEntityFieldName() + ") = UPPER(:" + paramName + ")\n");
                break;
            case not_equals:
                builder.append("and UPPER(" + parameter.getParamPrefix() + "." + attr.getEntityFieldName() + ") != UPPER(:" + paramName + ")\n");
                break;
            case contains:
                builder.append("and UPPER(" + parameter.getParamPrefix() + "." + attr.getEntityFieldName() + ") like UPPER(concat('%',:" + paramName + ",'%'))\n");
                break;
            case not_contains:
                builder.append("and UPPER(" + parameter.getParamPrefix() + "." + attr.getEntityFieldName() + ") not like UPPER(concat('%',:" + paramName + ",'%'))\n");
                break;
            case startsWith:
                builder.append("and UPPER(" + parameter.getParamPrefix() + "." + attr.getEntityFieldName() + ") like UPPER(concat(:" + paramName + ",'%'))\n");
                break;
            case endsWith:
                builder.append("and UPPER(" + parameter.getParamPrefix() + "." + attr.getEntityFieldName() + ") like UPPER(concat('%',:" + paramName + "))\n");
                break;
        }
        params.put(paramName, parameter.getValue());
    }
}
