package ru.peak.ml.core.filter.strategy;

import ru.peak.ml.core.filter.result.FilterResult;

/**
 * Created by d_litovchenko on 09.04.15.
 */
public interface FilterStrategy<T extends FilterResult > {

    public T createResult();

    public void setFilterResult(T filterResult);
}
