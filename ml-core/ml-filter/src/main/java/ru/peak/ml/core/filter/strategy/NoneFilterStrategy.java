package ru.peak.ml.core.filter.strategy;

import ru.ml.core.common.exceptions.MlServerException;
import ru.peak.ml.core.filter.result.FilterResult;
import ru.peak.ml.core.model.system.MlAttr;

/**
 * Created by d_litovchenko on 09.04.15.
 */
public class NoneFilterStrategy implements FilterStrategy<FilterResult> {

    FilterResult filterResult;
    @Override
    public FilterResult createResult() {
        filterResult.setRowQuery(createSelectStatement(filterResult));
        filterResult.setCountRecordQuery(createCountSelectStatement(filterResult));
        return filterResult;
    }

    @Override
    public void setFilterResult(FilterResult filterResult) {
        this.filterResult = filterResult;
    }

    private String createSelectStatement(FilterResult filterResult) {
        StringBuilder builder = new StringBuilder("select o from "+filterResult.getQueryMlClass().getEntityName()+" o ");
        if(filterResult.getAdditionalConditions()!=null && !filterResult.getAdditionalConditions().isEmpty()){
            builder.append(" where 1 = 1 ");
            for(String condition: filterResult.getAdditionalConditions()){
                builder.append(" and ").append(condition);
            }
        }
        builder.append(addOrder(filterResult));
        return builder.toString();
    }

    private String createCountSelectStatement(FilterResult filterResult) {
        StringBuilder builder = new StringBuilder("select count(o) from "+filterResult.getQueryMlClass().getEntityName()+" o ");
        if(filterResult.getAdditionalConditions()!=null && !filterResult.getAdditionalConditions().isEmpty()){
            builder.append(" where 1 = 1 ");
            for(String condition: filterResult.getAdditionalConditions()){
                builder.append(" and ").append("(").append(condition).append(") ");
            }
        }
        return builder.toString();
    }

    private String addOrder(FilterResult filterResult) {
        StringBuilder builder = new StringBuilder();
        if(filterResult.getOrderAttr() != null && !filterResult.getOrderAttr().isEmpty()){
            MlAttr attr = filterResult.getQueryMlClass().getAttr(filterResult.getOrderAttr());
            if (attr == null) {
                throw new MlServerException(String.format("Can't find order attr with name [%s]", filterResult.getOrderAttr()));
            }
            builder.append(" order by o.").append(attr.getEntityFieldName());
            if (filterResult.getOrderType() != null) {
                builder.append(" ").append(filterResult.getOrderType());
            } else {
                builder.append(" asc");
            }
        }
        return builder.toString();
    }


}
