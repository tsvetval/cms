package ru.peak.ml.core.bootstrap.converter;

import ru.peak.ml.core.bootstrap.model.MlAttrSystemModel;
import ru.peak.ml.core.bootstrap.model.MlClassSystemModel;
import ru.ml.core.common.AttrType;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class SystemMlClassConverter {

    public List<MlClass> convertToMlClassList(List<MlClassSystemModel> bootClassList) {
        List<MlClass> result = new ArrayList<>();
        for (MlClassSystemModel bootClass : bootClassList) {
            result.add(convertRootClass(bootClass));
        }

        // конвертируем n-1 конвертируем m-n
        for (MlClassSystemModel bootClass : bootClassList){
            for (MlAttrSystemModel bootAttr : bootClass.getAttrSet()){
                if (bootAttr.getFieldType().equals(AttrType.MANY_TO_ONE ) || bootAttr.getFieldType().equals(AttrType.MANY_TO_MANY)){
                    MlAttr manyToOneAttr = getMlAttrById(result, bootAttr.getId());
                    manyToOneAttr.set(MlAttr.LINK_CLASS, getMlClassById(result, bootAttr.getLinkClass().getId()));
                }
            }
        }

        // конвертируем 1-n
        for (MlClassSystemModel bootClass : bootClassList){
            for (MlAttrSystemModel bootAttr : bootClass.getAttrSet()){
                if (bootAttr.getFieldType().equals(AttrType.ONE_TO_MANY)){
                    MlAttr oneToMany = getMlAttrById(result, bootAttr.getId());
                    oneToMany.set(MlAttr.LINK_ATTR, getMlAttrById(result, bootAttr.getLinkAttr().getId()));
                }
            }
        }

        return result;
    }

    private MlClass getMlClassById(List<MlClass> mlClassList, Long id) {
        for (MlClass mlClass : mlClassList) {
            if (mlClass.getId().equals(id)){
                return mlClass;
            }
        }
        return null;
    }
    private MlAttr getMlAttrById(List<MlClass> mlClassList, Long id) {
        for (MlClass mlClass : mlClassList) {
            for (MlAttr attr : mlClass.getAttrSet()){
                if (attr.getId().equals(id)){
                    return attr;
                }
            }
        }
        return null;
    }



        private MlClass convertRootClass(MlClassSystemModel bootClass) {
        MlClass resultClass = new MlClass();
        resultClass.setId(bootClass.getId());
        resultClass.setEntityName(bootClass.getEntityName());
        resultClass.setTableName(bootClass.getTableName());
        resultClass.setJavaClass(bootClass.getJavaClass());
        resultClass.set("isCacheable", false);

        List<MlAttr> attrList = new ArrayList<>();
        resultClass.set("attrSet", attrList);
        for (MlAttrSystemModel bootAttr : bootClass.getAttrSet()){
            attrList.add(convertCommonAttrValues(bootAttr, resultClass));
        }

        return resultClass;
    }


    private MlAttr convertCommonAttrValues(MlAttrSystemModel bootAttr, MlClass classOwner) {
        MlAttr resultAttr = new MlAttr();
        resultAttr.setId(bootAttr.getId());
        resultAttr.setEntityFieldName(bootAttr.getEntityFieldName());
        resultAttr.setTableFieldName(bootAttr.getTableFieldName());
        resultAttr.setTableFieldName(bootAttr.getTableFieldName());
        resultAttr.setPrimaryKey(bootAttr.getPrimaryKey());
        resultAttr.setAutoIncrement(bootAttr.getAutoIncrement());
        resultAttr.setFieldType(bootAttr.getFieldType());
        resultAttr.set("lazy", bootAttr.getLazy());
        resultAttr.setVirtual(bootAttr.isVirtual());
        resultAttr.setOrdered(bootAttr.isOrdered());
        resultAttr.setOrdered(bootAttr.isOrdered());
        resultAttr.setMlClass(classOwner);
        return resultAttr;
    }

}
