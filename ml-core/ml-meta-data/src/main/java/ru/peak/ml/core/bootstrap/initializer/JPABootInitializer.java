package ru.peak.ml.core.bootstrap.initializer;

import com.google.inject.ImplementedBy;
import org.eclipse.persistence.jpa.dynamic.JPADynamicTypeBuilder;
import ru.peak.ml.core.bootstrap.initializer.impl.JPABootInitializerImpl;
import ru.peak.ml.core.bootstrap.model.MlClassSystemModel;

import java.util.Collection;
import java.util.Map;

/**
 *
 */
@ImplementedBy(JPABootInitializerImpl.class)
public interface JPABootInitializer {
    Map<String, JPADynamicTypeBuilder> initializeClasses(Collection<MlClassSystemModel> detachedMlClassSystemModelList, Boolean fullReload) throws ClassNotFoundException;
}
