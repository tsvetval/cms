package ru.peak.ml.core.bootstrap.initializer.impl;

import ddl.creator.DDLGenerator;
import org.eclipse.persistence.annotations.BatchFetchType;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.dynamic.DynamicType;
import org.eclipse.persistence.internal.dynamic.DynamicTypeImpl;
import org.eclipse.persistence.internal.helper.DatabaseField;
import org.eclipse.persistence.jpa.dynamic.JPADynamicHelper;
import org.eclipse.persistence.jpa.dynamic.JPADynamicTypeBuilder;
import org.eclipse.persistence.mappings.*;
import org.eclipse.persistence.sequencing.NativeSequence;
import org.eclipse.persistence.sequencing.Sequence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.AttrType;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.bootstrap.initializer.JPABootInitializer;
import ru.peak.ml.core.bootstrap.model.MlAttrSystemModel;
import ru.peak.ml.core.bootstrap.model.MlClassSystemModel;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.system.MlAttr;

import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 */
public class JPABootInitializerImpl implements JPABootInitializer {
    private static final Logger log = LoggerFactory.getLogger(JPABootInitializerImpl.class);
    private static final String ORDER_COLUMN_SUFFIX = "_order";

    @Inject
    EntityManagerFactory emf;

    private final String JPA_DYNAMIC_MODEL_CLASS_PACKAGE = "ru.peak.ml.jpa.model.dynamic.bootstrap.";

    Map<String, JPADynamicTypeBuilder> initializedTypeBuilderMap;

    @Override
    public Map<String, JPADynamicTypeBuilder> initializeClasses(Collection<MlClassSystemModel> detachedMlClassSystemModelList, Boolean fullReload) throws ClassNotFoundException {
        // Список динамик типов на переинииализацию в JPA
        //detachedMlClassSystemModelList = addAllRelationClasses(detachedMlClassSystemModelList);
        List<DynamicType> typeList = new ArrayList<>();
        JPADynamicHelper helper = new JPADynamicHelper(emf);
        initializedTypeBuilderMap = new HashMap<>();
        log.debug("Start to initialize ClassList size = " + detachedMlClassSystemModelList.size());
        //Создаем мапу классов на инициализацию
        Map<String, MlClassSystemModel> notInitializedEntityTypes = new ConcurrentHashMap<>();
        for (MlClassSystemModel MlClassSystemModel : detachedMlClassSystemModelList) {
            notInitializedEntityTypes.put(MlClassSystemModel.getEntityName(), MlClassSystemModel);
        }
        List<MlClassSystemModel> classesForInitialization = new ArrayList<>();
        classesForInitialization.addAll(notInitializedEntityTypes.values());

        // Создаем типы по всем классам (Необходимо для линковки)
        while (notInitializedEntityTypes.size() != 0) {
            List<MlClassSystemModel> noyInitializedMlClassSystemModeles = new ArrayList<>(notInitializedEntityTypes.values());
            for (MlClassSystemModel MlClassSystemModel : noyInitializedMlClassSystemModeles) {
                // Добавляем класс к нашему метаописанию
                log.debug(String.format("Initialize type for MlClassSystemModel [%s]", MlClassSystemModel.getEntityName()));

                if (MlClassSystemModel.getParent() == null || // Нет родителя
                        // Родитель уже проинициализирован
                        !notInitializedEntityTypes.containsKey(MlClassSystemModel.getParent().getEntityName())) {
                    JPADynamicTypeBuilder tmpType = initializeClass(MlClassSystemModel);
                    initializedTypeBuilderMap.put(MlClassSystemModel.getEntityName(), tmpType);
                    notInitializedEntityTypes.remove(MlClassSystemModel.getEntityName());
                }
            }
        }

        // Инициализируем простые атрибуту и первичные ключи
        for (MlClassSystemModel MlClassSystemModel : classesForInitialization) {
            log.debug(String.format("Initialize Simple Attrs for MlClassSystemModel [%s]", MlClassSystemModel.getEntityName()));
            initializeSimpleAttr(MlClassSystemModel, surroundDelimiter(MlClassSystemModel.getTableName()));
        }
        // инициализируем  MANY_TO_ONE
        for (MlClassSystemModel MlClassSystemModel : classesForInitialization) {
            log.debug(String.format("Initialize MANY_TO_ONE for MlClassSystemModel [%s]", MlClassSystemModel.getEntityName()));
            initializeManyToOneAttr(MlClassSystemModel, surroundDelimiter(MlClassSystemModel.getTableName()), helper);
        }

        // инициализируем ONE_TO_MANY
        for (MlClassSystemModel MlClassSystemModel : classesForInitialization) {
            log.debug(String.format("Initialize ONE_TO_MANY for MlClassSystemModel [%s]", MlClassSystemModel.getEntityName()));
            initializeOneToManyAttr(MlClassSystemModel, surroundDelimiter(MlClassSystemModel.getTableName()), helper);
        }

        // инициализируем MANY_TO_MANY
        for (MlClassSystemModel MlClassSystemModel : classesForInitialization) {
            log.debug(String.format("Initialize MANY_TO_MANY for MlClassSystemModel [%s]", MlClassSystemModel.getEntityName()));
            initializeManyToManyAttr(MlClassSystemModel, surroundDelimiter(MlClassSystemModel.getTableName()), helper);
        }

        // Проводим создание энтитей
        for (JPADynamicTypeBuilder builder : initializedTypeBuilderMap.values()) {
            typeList.add(builder.getType());
        }
        log.debug("Clear JPQLParseCache and register Types");
        helper.getSession().getProject().getJPQLParseCache().getCache().clear();
        helper.addTypes(false, false, typeList.toArray(new DynamicType[typeList.size()]));
        log.debug("Initialization finished success");
        return initializedTypeBuilderMap;
    }

//    private Collection<MlClassSystemModel> addAllRelationClasses(Collection<MlClassSystemModel> detachedMlClassSystemModelList) {
//        List<MlClassSystemModel> result = new ArrayList<>();
//        EntityManager em = emf.createEntityManager();
//        for (MlClassSystemModel bootClass : detachedMlClassSystemModelList) {
//            if (!(bootClass instanceof MlClassSystemModel)) {
//                return detachedMlClassSystemModelList;
//            }
//            result.add(bootClass);
//            result.addAll(em.createQuery("select distinct o.MlClassSystemModel from MlAttr o where o.linkClass.id = " + bootClass.getId()).getResultList());
//        }
//        return result;
//    }
//
    /**
     * Метод проводит начальную инициализацию энтити в JPA (фактически только болванку, это необходимо в силу алгоритма)
     *
     * @param mlClass
     * @return
     * @throws ClassNotFoundException
     */
    private JPADynamicTypeBuilder initializeClass(MlClassSystemModel mlClass) throws ClassNotFoundException {
        // Удаляем старый класс если есть
        JPADynamicHelper helper = new JPADynamicHelper(emf);
        // em.detach(mlClass);
        //TODO вроде это не нужно
        if (helper.getType(mlClass.getEntityName()) == null || helper.getType(mlClass.getEntityName()).getParentType() == null) {
            helper.removeType(mlClass.getEntityName());
        }

        // Инициализируем Энтити и их первичные ключи
        //TODO do with parents
        JPADynamicTypeBuilder dynamicTypeBuilder = null;
        // Наследования нет
        Class<?> newJpaClass;
        if (mlClass.getJavaClass() != null && !"".equals(mlClass.getJavaClass())) {
            newJpaClass = GuiceConfigSingleton.getInstance().getDcl().createDynamicClass(mlClass.getJavaClass());
        } else {
            Class newParentClass = MlDynamicEntityImpl.class;

            newJpaClass = GuiceConfigSingleton.getInstance().getDcl().createDynamicClass(JPA_DYNAMIC_MODEL_CLASS_PACKAGE + mlClass.getEntityName(), newParentClass);
        }

        dynamicTypeBuilder = new JPADynamicTypeBuilder(newJpaClass, null, surroundDelimiter(mlClass.getTableName())/*tableList.toArray(new String[tableList.size()])*/);
        if (mlClass.getCacheable()) {
            dynamicTypeBuilder.getType().getDescriptor().setCacheIsolation(CacheIsolationType.SHARED);
            log.debug(String.format("Cache enable for class [%s]", mlClass.getEntityName()));
        } else {
            dynamicTypeBuilder.getType().getDescriptor().setCacheIsolation(CacheIsolationType.ISOLATED);
            log.debug(String.format("Cache disable for class [%s]", mlClass.getEntityName()));
        }

        return dynamicTypeBuilder;
    }


    private void initializeOneToManyAttr(MlClassSystemModel mlClass, String parentClassPath, JPADynamicHelper helper) {
        JPADynamicTypeBuilder dynamicTypeBuilder = initializedTypeBuilderMap.get(mlClass.getEntityName());
        for (MlAttrSystemModel mlAttr : mlClass.getAttrSet()) {
            if (!mlAttr.getMlClass().getEntityName().equals(mlClass.getEntityName())
                    || mlAttr.isVirtual()) {
                //атрибут родительского класса не инициализируем (он родительского так проинициализирован)
                //не инициализируем виртуальный атрибут
                continue;
            }
            if (mlAttr.getFieldType().equals(AttrType.ONE_TO_MANY)) {
                log.debug(String.format("Initialize ONE_TO_MANY Attr [%s]", mlAttr.getEntityFieldName()));
                OneToManyMapping link = dynamicTypeBuilder.addOneToManyMapping(mlAttr.getEntityFieldName(),
                        helper.getType(mlAttr.getLinkAttr().getMlClass().getEntityName()) == null ?
                                initializedTypeBuilderMap.get(mlAttr.getLinkAttr().getMlClass().getEntityName()).getType() :
                                helper.getType(mlAttr.getLinkAttr().getMlClass().getEntityName()),
                        surroundDelimiter(mlAttr.getLinkAttr().getTableFieldName()));
                setFetchType(mlAttr,link);
                link.setIsLazy(mlAttr.getLazy() != null ? mlAttr.getLazy() : false);
                if (mlAttr.isOrdered()) {
                    String orderField = mlAttr.getLinkAttr().getEntityFieldName() + ORDER_COLUMN_SUFFIX;
                    link.setListOrderFieldName(surroundDelimiter(orderField));
                }
            }
        }
    }

    private void initializeManyToOneAttr(MlClassSystemModel mlClass, String parentClassPath, JPADynamicHelper helper) {
        JPADynamicTypeBuilder dynamicTypeBuilder = initializedTypeBuilderMap.get(mlClass.getEntityName());

        DynamicTypeImpl parentType = null;
        if (mlClass.getParent() != null) {
            JPADynamicTypeBuilder parentTypeBuilder = initializedTypeBuilderMap.get(mlClass.getParent().getEntityName());
            if (parentTypeBuilder == null) {
                parentType = (DynamicTypeImpl) helper.getType(mlClass.getParent().getEntityName());
            } else {
                parentType = (DynamicTypeImpl) parentTypeBuilder.getType();
            }
            ((DynamicTypeImpl) dynamicTypeBuilder.getType()).getMappingsRequiringInitialization().addAll(parentType.getMappingsRequiringInitialization());
        }

        for (MlAttrSystemModel mlAttr : mlClass.getAttrSet()) {
            if (mlAttr.isVirtual()) {
                continue;
            }
            if (!mlAttr.getMlClass().getEntityName().equals(mlClass.getEntityName())) {
                //атрибут родительского класса не инициализируем (он родительского так проинициализирован)
                if (mlAttr.getFieldType().equals(AttrType.MANY_TO_ONE)) {
                    if (parentType != null) {
                        for (DatabaseMapping mapping : parentType.getDescriptor().getMappings()) {
                            if (mapping.getAttributeName().equals(mlAttr.getEntityFieldName())) {
                                ((DynamicTypeImpl) dynamicTypeBuilder.getType()).getMappingsRequiringInitialization().add(mapping);
                            }
                        }
                    }
                }
                continue;
            }
            if (mlAttr.getFieldType().equals(AttrType.MANY_TO_ONE)) {
                log.debug(String.format("Initialize MANY_TO_ONE Attr [%s]", mlAttr.getEntityFieldName()));
                OneToOneMapping mapping = dynamicTypeBuilder.addOneToOneMapping(mlAttr.getEntityFieldName(),
                        helper.getType(mlAttr.getLinkClass().getEntityName()) == null ?
                                initializedTypeBuilderMap.get(mlAttr.getLinkClass().getEntityName()).getType() :
                                helper.getType(mlAttr.getLinkClass().getEntityName()),
                        surroundDelimiter(mlAttr.getTableFieldName()));
                mapping.setIsLazy(mlAttr.getLazy() != null ? mlAttr.getLazy() : false);
                setFetchType(mlAttr,mapping);

            }
        }
    }

    private void initializeManyToManyAttr(MlClassSystemModel mlClass, String parentClassPath, JPADynamicHelper helper) {
        JPADynamicTypeBuilder dynamicTypeBuilder = initializedTypeBuilderMap.get(mlClass.getEntityName());
        for (MlAttrSystemModel mlAttr : mlClass.getAttrSet()) {
            if (!mlAttr.getMlClass().getEntityName().equals(mlClass.getEntityName())
                    || mlAttr.isVirtual()) {
                //атрибут родительского класса не инициализируем (он родительского так проинициализирован)
                //либо атрибут виртуальный
                continue;
            }
            if (mlAttr.getFieldType().equals(AttrType.MANY_TO_MANY)) {
                log.debug(String.format("Initialize MANY_TO_MANY Attr [%s]", mlAttr.getEntityFieldName()));

                String relationTableName;
                if (mlAttr.getManyToManyTableName() != null && !mlAttr.getManyToManyTableName().equals("")) {
                    relationTableName = surroundDelimiter(mlAttr.getManyToManyTableName());
                } else {
                    relationTableName = surroundDelimiter(getMNTableName(mlAttr.getMlClass().getTableName(), mlAttr.getLinkClass().getTableName()));
                }
                ManyToManyMapping mapping = addManyToManyMapping(dynamicTypeBuilder, mlAttr.getEntityFieldName(),
                        helper.getType(mlAttr.getLinkClass().getEntityName()) == null ?
                                initializedTypeBuilderMap.get(mlAttr.getLinkClass().getEntityName()).getType() : helper.getType(mlAttr.getLinkClass().getEntityName()),
                        relationTableName);
                if (mlAttr.getManyToManyFieldNameM() != null && !mlAttr.getManyToManyFieldNameM().equals("")) {
                    mapping.getSourceRelationKeyFields().clear();
                    mapping.setSourceRelationKeyFieldName(surroundDelimiter(mlAttr.getManyToManyFieldNameM()));
                }
                if (mlAttr.getManyToManyFieldNameN() != null && !mlAttr.getManyToManyFieldNameN().equals("")) {
                    mapping.getTargetRelationKeyFields().clear();
                    mapping.setTargetRelationKeyFieldName(surroundDelimiter(mlAttr.getManyToManyFieldNameN()));
                }

                dynamicTypeBuilder.addMapping(mapping);
                mapping.setIsLazy(mlAttr.getLazy() != null ? mlAttr.getLazy() : false);
                setFetchType(mlAttr,mapping);
                //Если список упорядоченный, добавляем упорядочивание
                if (mlAttr.isOrdered()) {
                    String orderColumn = getMNOrdrColumn(mlAttr.getLinkClass().getEntityName());
                    mapping.setListOrderFieldName(surroundDelimiter(orderColumn));
                    //закомментил потому что выбрасовалось исключение при инициализации существующих связий
                    //mapping.setOrderCorrectionType(OrderCorrectionType.EXCEPTION);
                }
            }
        }
    }

    public ManyToManyMapping addManyToManyMapping(JPADynamicTypeBuilder dynamicTypeBuilder, String name, DynamicType refType, String relationshipTableName) {
        ManyToManyMapping mapping = new ManyToManyMapping();
        mapping.setAttributeName(name);
        mapping.setReferenceClass(refType.getJavaClass());
        mapping.setRelationTableName(relationshipTableName);
        //TODO разобраться с построением отношений
        for (DatabaseField sourcePK : dynamicTypeBuilder.getType().getDescriptor().getPrimaryKeyFields()) {
            String relField = sourcePK.getName();
            relField = dynamicTypeBuilder.getType().getDescriptor().getTableName() + "_" + relField;
            mapping.addSourceRelationKeyFieldName(surroundDelimiter(relField.replaceAll("\"", "")), sourcePK.getQualifiedName());
        }
        for (DatabaseField targetPK : refType.getDescriptor().getPrimaryKeyFields()) {
            String relField = targetPK.getName();
            relField = refType.getDescriptor().getTableName() + "_" + relField;
            mapping.addTargetRelationKeyFieldName(surroundDelimiter(relField.replaceAll("\"", "")), targetPK.getQualifiedName());
        }
        mapping.useTransparentList();
        return mapping;
        //addMapping(mapping);
    }

    private void setFetchType(MlAttrSystemModel attr, ForeignReferenceMapping mapping){
        /*switch (attr.getFetchType()){
            case OUTER_JOIN:
                mapping.useOuterJoinFetch();
                break;
            case INNER_JOIN:
                mapping.useInnerJoinFetch();
                break;
            case BATCH_JOIN:
                mapping.setBatchFetchType(BatchFetchType.JOIN);
                break;
            case BATCH_EXISTS:
                mapping.setBatchFetchType(BatchFetchType.EXISTS);
                break;
            case BATCH_IN:
                mapping.setBatchFetchType(BatchFetchType.IN);
                break;
            case NONE:
                break;
        }*/

    }

    private void initializeSimpleAttr(MlClassSystemModel mlClass, String parentClassPath) {
        JPADynamicTypeBuilder dynamicTypeBuilder = initializedTypeBuilderMap.get(mlClass.getEntityName());


        for (MlAttrSystemModel mlAttr : mlClass.getAttrSet()) {
            log.debug(String.format("Try Initialize Simple Attr [%s] for class [%s]", mlAttr.getEntityFieldName(), mlClass.getEntityName()));
            if (!mlAttr.getMlClass().getEntityName().equals(mlClass.getEntityName())
                    || mlAttr.isVirtual()) {
                log.debug(String.format("[%s] for class [%s] is not simple, skip it", mlAttr.getEntityFieldName(), mlClass.getEntityName()));
                //атрибут родительского класса не инициализируем (он родительского так проинициализирован)
                //не инициализируем виртуальный атрибут
                continue;
            }

            if (mlAttr.getFieldType().equals(AttrType.BOOLEAN) ||
                    mlAttr.getFieldType().equals(AttrType.DATE) ||
                    mlAttr.getFieldType().equals(AttrType.STRING) ||
                    mlAttr.getFieldType().equals(AttrType.TEXT) ||
                    mlAttr.getFieldType().equals(AttrType.ENUM) ||
                    mlAttr.getFieldType().equals(AttrType.DOUBLE) ||
                    mlAttr.getFieldType().equals(AttrType.FILE) ||
                    mlAttr.getFieldType().equals(AttrType.LONG)) {

                log.debug(String.format("Initialize MlAttr [%s]", mlAttr.getEntityFieldName()));

                if (mlAttr.getPrimaryKey() != null && mlAttr.getPrimaryKey()) {
                    log.debug(String.format("Set PK for attr [%s]", mlAttr.getEntityFieldName()));
                    dynamicTypeBuilder.setPrimaryKeyFields(surroundDelimiter(mlAttr.getEntityFieldName()));
                }

                if (mlAttr.getAutoIncrement() != null && mlAttr.getAutoIncrement() && mlClass.getParent() == null) {
                    // Для классоа с родителем не нужно создавать автоинкрементную сиквенс она берется у предка
                    log.debug(String.format("Initialize Sequence for attr [%s]", mlAttr.getEntityFieldName()));
                    // TODO rafactor via platform
                    String sequenceName = surroundDelimiter(mlAttr.getMlClass().getTableName() + "_" + mlAttr.getTableFieldName() + DDLGenerator.SEQUENCE_POSTFIX);

                    JPADynamicHelper helper = new JPADynamicHelper(emf);
                    Map sequencesMap = new HashMap();
                    if (helper.getSession().getProject().getLogin() != null
                            && helper.getSession().getProject().getLogin().getSequences() != null) {
                        sequencesMap = helper.getSession().getProject().getLogin().getSequences();
                    } else {
                        helper.getSession().getProject().getLogin().setSequences(sequencesMap);
                    }

                    Sequence sequence = new NativeSequence(sequenceName, 1, false);
                    if (!sequencesMap.containsKey(sequenceName)) {
                        sequencesMap.put(sequenceName, sequence);
                    } else {
                        sequence = (Sequence) sequencesMap.get(sequenceName);
                    }

                    dynamicTypeBuilder.configureSequencing(sequence, sequenceName, surroundDelimiter(mlAttr.getTableFieldName()));
                    log.debug(String.format("Sequence name = [%s]", sequenceName));
                }
                switch (mlAttr.getFieldType()) {
                    case TEXT:
                    case STRING:
                        dynamicTypeBuilder.addDirectMapping(mlAttr.getEntityFieldName(), String.class, parentClassPath + "." + surroundDelimiter(mlAttr.getTableFieldName()));
                        break;
                    case LONG:
                        dynamicTypeBuilder.addDirectMapping(mlAttr.getEntityFieldName(), Long.class, parentClassPath + "." + surroundDelimiter(mlAttr.getTableFieldName()));
                        break;
                    case BOOLEAN:
                        dynamicTypeBuilder.addDirectMapping(mlAttr.getEntityFieldName(), Boolean.class, parentClassPath + "." + surroundDelimiter(mlAttr.getTableFieldName()));
                        break;
                    case DATE:
                        dynamicTypeBuilder.addDirectMapping(mlAttr.getEntityFieldName(), Date.class, parentClassPath + "." + surroundDelimiter(mlAttr.getTableFieldName()));
                        break;
                    case ENUM:
                        dynamicTypeBuilder.addDirectMapping(mlAttr.getEntityFieldName(), String.class, parentClassPath + "." + surroundDelimiter(mlAttr.getTableFieldName()));
                        break;
                    case DOUBLE:
                        dynamicTypeBuilder.addDirectMapping(mlAttr.getEntityFieldName(), Double.class, parentClassPath + "." + surroundDelimiter(mlAttr.getTableFieldName()));
                        break;
                    case FILE:
                        dynamicTypeBuilder.addDirectMapping(mlAttr.getEntityFieldName(), byte[].class, parentClassPath + "." + surroundDelimiter(mlAttr.getTableFieldName())).setIsLazy(true);
                        dynamicTypeBuilder.addDirectMapping(mlAttr.getEntityFieldName() + "_filename", String.class, parentClassPath + "." + surroundDelimiter(mlAttr.getTableFieldName() + "_filename"));
                        break;
                }


            }
        }
    }

    private String surroundDelimiter(String original) {
        JPADynamicHelper helper = new JPADynamicHelper(emf);
        return helper.getSession().getPlatform().getStartDelimiter() + original + helper.getSession().getPlatform().getEndDelimiter();
    }

    private String getMNTableName(String sourсeTable, String destTable) {
        if (sourсeTable.compareTo(destTable) > 0) {
            return "MN_" + destTable + "_" + sourсeTable;
        } else {
            return "MN_" + sourсeTable + "_" + destTable;
        }
    }

    private String getMNOrdrColumn(String entityFieldName) {
        return entityFieldName + ORDER_COLUMN_SUFFIX;
    }

}
