package ru.peak.ml.core.bootstrap.model;

import ru.ml.core.common.*;
import ru.ml.core.common.FetchType;

import javax.persistence.*;

/**
 * Данный класс нужен только при старте сисстемы далее он не используется
 */
@Entity
@Table(name = "\"MlAttr\"")
public class MlAttrSystemModel {
  @Id
  private Long id;

  private String entityFieldName;
  private String tableFieldName;

  private Boolean systemField;
  private Boolean primaryKey;
  private Boolean autoIncrement;
  private String replicationType;
  private String fieldType;
  private Boolean lazy;
  private Boolean virtual;
  private Boolean ordered;
  private String manyToManyTableName;
  private String manyToManyFieldNameM;
  private String manyToManyFieldNameN;
  private String fetchType;
  //private MlClassSystemModel linkClass;
  /**
   * Линковый атрибут
   */
  @ManyToOne
  @JoinColumn(name = "linkAttr", insertable = true, updatable = true)
  private MlAttrSystemModel linkAttr;

  @ManyToOne
  @JoinColumn(name = "linkClass", insertable = true, updatable = true)
  private MlClassSystemModel linkClass;

  /**
   * Класс атрибута
   */
  @ManyToOne
  @JoinColumn(name = "mlClass", insertable = true, updatable = true)
  private MlClassSystemModel mlClass;

    /*Для ссылочных атрибутов*/

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getEntityFieldName() {
    return entityFieldName;
  }

  public void setEntityFieldName(String entityFieldName) {
    this.entityFieldName = entityFieldName;
  }

  public String getTableFieldName() {
    return tableFieldName;
  }

  public void setTableFieldName(String tableFieldName) {
    this.tableFieldName = tableFieldName;
  }

  public Boolean getSystemField() {
    return systemField;
  }

  public void setSystemField(Boolean system) {
    systemField = system;
  }

  public MlClassSystemModel getMlClass() {
    return mlClass;
  }

  public void setMlClass(MlClassSystemModel mlClass) {
    this.mlClass = mlClass;
  }

  public Boolean getPrimaryKey() {
    return primaryKey;
  }

  public void setAutoIncrement(Boolean autoIncrement) {
    this.autoIncrement = autoIncrement;
  }


  public Boolean getAutoIncrement() {
    return autoIncrement;  //To change body of implemented methods use File | Settings | File Templates.
  }

  public void setFieldType(String fieldType) {
    this.fieldType = fieldType;
  }

  public AttrType getFieldType() {
    if (fieldType != null) {
      return AttrType.valueOf(fieldType);
    }
    return null;
  }

    public ru.ml.core.common.FetchType getFetchType() {
        if (fetchType != null) {
            return FetchType.valueOf(fetchType);
        }
        return null;
    }

    public void setLinkAttr(MlAttrSystemModel linkAttr) {
    this.linkAttr = linkAttr;
  }

  public MlAttrSystemModel getLinkAttr() {
    return linkAttr;  //To change body of implemented methods use File | Settings | File Templates.
  }


  public void setPrimaryKey(Boolean primaryKey) {
    this.primaryKey = primaryKey;
  }

  public MlClassSystemModel getLinkClass() {
    return linkClass;
  }

  public void setLinkClass(MlClassSystemModel linkClass) {
    this.linkClass = linkClass;
  }

  public Boolean getLazy() {
    return lazy;
  }

  public void setLazy(Boolean lazy) {
    this.lazy = lazy;
  }

  public void setReplicationType(String replicationType) {
    this.replicationType = replicationType;
  }

  public Boolean isVirtual() {
    return virtual;
  }

  public void setVirtual(Boolean virtual) {
    this.virtual = virtual;
  }

  public Boolean isOrdered() {
    return ordered;
  }

  public void setOrdered(Boolean ordered) {
    this.ordered = ordered;
  }

  public String getManyToManyTableName() {
    return manyToManyTableName;
  }

  public void setManyToManyTableName(String manyToManyTableName) {
    this.manyToManyTableName = manyToManyTableName;
  }

  public String getManyToManyFieldNameM() {
    return manyToManyFieldNameM;
  }

  public void setManyToManyFieldNameM(String manyToManyFieldNameM) {
    this.manyToManyFieldNameM = manyToManyFieldNameM;
  }

  public String getManyToManyFieldNameN() {
    return manyToManyFieldNameN;
  }

  public void setManyToManyFieldNameN(String manyToManyFieldNameN) {
    this.manyToManyFieldNameN = manyToManyFieldNameN;
  }
}
