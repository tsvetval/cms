package ru.peak.ml.core.bootstrap.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Данный класс нужен только при старте сисстемы далее он не используется
 */
@Entity
@Table(name = "\"MlClass\"")

/*
@SequenceGenerator(name = "ML_CLASS_SEQ", sequenceName="ML_CLASS_SEQ", allocationSize=1)
*/

public class MlClassSystemModel {
    @Id
    private Long id;


    private String entityName;
    private String tableName;
/*
    private Boolean isSystem;
*/
    private Boolean isCacheable;

    /**
     * От какого клаасса JAVA наследовать DynamicEntity (Если null то без наследования)
     */
    private String javaClass;

    /**
     * Список атрибутов класса
     */
    @OneToMany(mappedBy = "mlClass")
    List<MlAttrSystemModel> attrSet;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

/*
    public Boolean getSystem() {
        return isSystem;
    }

    public void setSystem(Boolean system) {
        isSystem = system;
    }
*/

    public String getJavaClass() {
        return javaClass;
    }

    public void setJavaClass(String javaClass) {
        this.javaClass = javaClass;
    }

    public List<MlAttrSystemModel> getAttrSet() {
        return attrSet;
    }

    public MlClassSystemModel getParent() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public MlAttrSystemModel getPrimaryKeyAttr() {
        for(MlAttrSystemModel attr: getAttrSet()){
            if(attr.getPrimaryKey()){
                return attr;
            }
        }
        return null;
    }

    public Boolean getCacheable() {
        return isCacheable;
    }

/*
    public List<MlAttrGroup> getGroupList() {
        return null;
    }
*/
}
