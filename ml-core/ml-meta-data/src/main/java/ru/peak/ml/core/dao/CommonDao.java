package ru.peak.ml.core.dao;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import org.eclipse.persistence.dynamic.DynamicEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlSecurityException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.value.DefaultValueStrategy;
import ru.peak.ml.core.handler.MlClassHandler;
import ru.peak.ml.core.holders.EnumHolder;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.MLUID;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.security.MlClassAccessType;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.model.system.MlEnum;
import ru.peak.ml.core.services.AccessService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * DAO для работы с объектами CMS
 */
public class CommonDao<T extends MlDynamicEntityImpl> {
    private static final Logger log = LoggerFactory.getLogger(CommonDao.class);

    //private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

    @Inject
    protected EntityManager entityManager;
    @Inject
    MetaDataHolder metaDataHolder;

    /**
     * Получить типизированный jpql-запрос к базе без проверки прав пользователя
     * Не использовать! Оставлен для поддержки старых проектов. При рефакторинге сделать приватным.
     *
     * @param query -   строка jpql-запроса
     * @param clazz -   класс для типизированного запроса
     * @return -   типизированный запрос
     */
    @Deprecated //TODO сделать приватным
    public<M extends T> TypedQuery<M> getTypedQuery(String query, Class<M> clazz) {
        return entityManager.createQuery(query, clazz);
    }

    /**
     * Получить jpql-запрос к базе без проверки прав пользователя.
     * Не использовать! Оставлен для поддержки старых проектов. При рефакторинге сделать приватным.
     *
     * @param query -   строка jpql-запроса
     * @return -   запрос
     */
    @Deprecated //TODO сделать приватным
    //Сделай приватным и в описании напиши, что должно использоваться вместо этого запроса, т.к. этот Deprecated
    public Query getQuery(String query) {
        return entityManager.createQuery(query);
    }


    /**
     * Получить типизированный jpql-запрос к базе без проверки прав пользователя
     * Используется для системных и анонимных операций
     *
     * @param query -   строка jpql-запроса
     * @param clazz -   класс для типизированного запроса
     * @return -   типизированный запрос
     */
    public<M extends T> TypedQuery<M> getTypedQueryWithoutSecurityCheck(String query, Class<M> clazz) {
        return getTypedQuery(query, clazz);
    }

    /**
     * Получить типизированный jpql-запрос к базе без проверки прав пользователя
     * Используется для системных и анонимных операций
     *
     * @param query       -   строка jpql-запроса
     * @param mlClassName -   имя класса (entityName) для типизированного запроса
     * @return -   типизированный запрос
     */
    public TypedQuery<T> getTypedQueryWithoutSecurityCheck(String query, String mlClassName) {
        Class clazz = metaDataHolder.getEntityClassByName(mlClassName);
        return getTypedQueryWithoutSecurityCheck(query, clazz);
    }

    /**
     * Получить типизированный jpql-запрос к базе c проверкой прав пользователя на чтение объектов класса
     * Используется для получения списка объектов из пользовательского интерфейса
     *
     * @param query       -   строка jpql-запроса
     * @param mlClassName -   имя класса (entityName) для типизированного запроса, используется для проверки прав доступа на чтение
     * @return -   типизированный запрос
     */
    public TypedQuery<T> getTypedQueryWithSecurityCheck(String query, String mlClassName) {
        checkClassAccess(mlClassName, MlClassAccessType.READ);
        Class<T> clazz = metaDataHolder.getEntityClassByName(mlClassName);
        return getTypedQuery(query, clazz);
    }


    /**
     * Получить jpql-запрос к базе без проверки прав пользователя.
     * Используется для системных и анонимных операций
     *
     * @param query -   строка jpql-запроса
     * @return -   запрос
     */
    public Query getQueryWithoutSecurityCheck(String query) {
        return getQuery(query);
    }

    /**
     * Получить jpql-запрос к базе с проверкой прав пользователя на чтение объектов класса
     * Используется для получения списка объектов из пользовательского интерфейса
     *
     * @param query       -   строка jpql-запроса
     * @param mlClassName -   имя класса (entityName) для типизированного запроса, используется для проверки прав доступа на чтение
     * @return -   запрос
     */
    public Query getQueryWithSecurityCheck(String query, String mlClassName) {
        checkClassAccess(mlClassName, MlClassAccessType.READ);
        return getQuery(query);
    }


    /**
     * Создание объекта в базе данных с проверкой прав пользователя на создание объектов класса
     *
     * @param object -   объект для создания
     * @return
     */
    @Transactional
    public <M extends T> M persistTransactionalWithSecurityCheck(M object) {
        if (object.getPropertiesMap().containsKey("lastChange")) {
            object.set("lastChange", new Timestamp(new Date().getTime()));
        }
        return this.persistWithSecurityCheck(object);
    }

    /**
     * Создание объекта в базе данных без проверки прав пользователя на создание объектов класса
     *
     * @param object -   объект для создания
     * @return
     */
    @Transactional
    public <M extends T> M persistTransactionalWithoutSecurityCheck(M object) {
        if (object.getPropertiesMap().containsKey("lastChange")) {
            object.set("lastChange", new Timestamp(new Date().getTime()));
        }
        return this.persistWithoutSecurityCheck(object);
    }

    /**
     * Обновление объектов в базе данных с проверкой прав пользователя на изменение объектов класса
     *
     * @param object -   объект для обновления
     * @return
     */
    @Transactional
    public<M extends T> M mergeTransactional(M object) {
        if (object.getPropertiesMap().containsKey("lastChange")) {
            object.set("lastChange", new Timestamp(new Date().getTime()));
        }
        return this.mergeWithSecurityCheck(object);
    }

    /**
     * Удаление объектов в базе данных с проверкой прав пользователя на удаление объектов класса
     *
     * @param object -   объект для удаления
     * @return
     */
    @Transactional
    public<M extends T> void removeTransactional(M object) {
        this.removeWithSecurityCheck(object);
    }


    /**
     * Выполняет типизированный запрос к базе данных с получением единственного результата
     * Не используется, удалить!
     *
     * @param query -   jpql-запрос
     * @param clazz -   класс объекта
     * @return -   первый элемент из возвращаемого списка или null, если список пустой
     */
    @Deprecated
    public <M extends T> M getSingleResult(String query, Class<M> clazz) {
        TypedQuery<M> typedQuery = entityManager.createQuery(query, clazz);
        List<M> resultList = typedQuery.getResultList();
        if (!resultList.isEmpty()) {
            return resultList.get(0);
        } else {
            return null;
        }
    }

    /**
     * Переинициализация entity manager'а.
     * Не используется, удалить!
     */
    @Deprecated
    public void reInitEntityManager() {
        entityManager = GuiceConfigSingleton.inject(EntityManagerFactory.class).createEntityManager();
    }

    /**
     * Выполняет типизированный запрос к базе данных с получением списка объектов
     * Вместо данного метода нужно использовать getTypedQueryWithoutSecurityCheck или getTypedQueryWithSecurityCheck
     * в зависимости от необходимости проверять права доступа к объектам класса на чтение!
     *
     * @param query -   jpql-запрос
     * @param clazz -   класс объекта
     * @return -   список объектов, выбранных запросом
     */
    @Deprecated
    public<M extends T> List<M> getResultList(String query, Class<M> clazz) {
        TypedQuery<M> typedQuery = entityManager.createQuery(query, clazz);
        return typedQuery.getResultList();
    }

    /**
     * Выполняет типизированный запрос к базе данных с получением списка объектов
     * Вместо данного метода нужно использовать getTypedQueryWithoutSecurityCheck или getTypedQueryWithSecurityCheck
     * в зависимости от необходимости проверять права доступа к объектам класса на чтение!
     *
     * @param query       -   jpql-запрос
     * @param mlClassName -   имя класс объекта
     * @return -   список объектов, выбранных запросом
     */
    @Deprecated
    public List<T> getResultList(String query, String mlClassName) {
        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
        Class<T> clazz = metaDataHolder.getEntityClassByName(mlClassName);
        return getResultList(query, clazz);
    }


    /**
     * Поиск объекта по id
     *
     * @param id    -   идентификатор объекта
     * @param clazz -   класс объекта
     * @return -   объект заданного класса с указанным id или null, если такой объект не существует
     */
    //TODO добавить проверку прав доступа на чтение к объектам данного класса
    public<M extends T> M findById(Object id, Class<M> clazz) {
        return entityManager.find(clazz, id);
    }

    /**
     * Поиск объекта по id
     *
     * @param id          -   идентификатор объекта
     * @param mlClassName -   имя класса объекта
     * @return -   объект заданного класса с указанным id или null, если такой объект не существует
     */
    //TODO добавить проверку прав доступа на чтение к объектам данного класса
    public T findById(Object id, String mlClassName) {
        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
        Class<T> clazz = metaDataHolder.getEntityClassByName(mlClassName);
        return findById(id, clazz);
    }

    public List<T> getAll(Class<T> clazz) {
      return entityManager.createQuery("select o from " + clazz.getSimpleName() + " o", clazz).getResultList();
    }

    /**
     * Создание объекта с выполнением хэндлеров
     * Не использовать! Оставлен для поддержки старых проектов. При рефакторинге сделать приватным.
     *
     * @param object -   объект для создания
     */
    @Deprecated
    public T persist(T object) {
        return persist(object, true, true);
    }

    /**
     * Создание объекта с возможностью задать необходимость выполнения хэндлеров
     * Не использовать! Оставлен для поддержки старых проектов. При рефакторинге сделать приватным.
     *
     * @param object            -   объект для создания
     * @param runBeforeHandlers -   запускать хэндлер перед созданием
     * @param runAfterHandlers  -   запускать хэндлер после создания
     * @return
     */
    @Deprecated
    public T persist(T object, boolean runBeforeHandlers, boolean runAfterHandlers) {
        MlDynamicEntityImpl dynamicEntity = object;
        MlClassHandler handler = getHandlerForClassString(dynamicEntity.getInstanceMlClass().getHandler());

        if (runBeforeHandlers && handler != null) {
            handler.beforeCreate(dynamicEntity);
        }
        entityManager.persist(object);
        if (runAfterHandlers && handler != null) {
            handler.afterCreate(dynamicEntity);
        }
        return object;
    }

    /**
     * Создание объекта с выполнением хэндлеров.
     * Проверяет наличие прав доступа на создание объектов класса
     *
     * @param object -   объект для создания
     * @return
     */
    public<M extends T> M persistWithSecurityCheck(M object) {
        return persistWithSecurityCheck(object, true, true);
    }

    /**
     * Создание объекта с возможностью задать необходимость выполнения хэндлеров.
     * Проверяет наличие прав доступа на создание объектов класса
     *
     * @param object            -   объект для создания
     * @param runBeforeHandlers -   запускать хэндлер перед созданием
     * @param runAfterHandlers  -   запускать хэндлер после создания
     * @return
     */
    public<M extends T> M persistWithSecurityCheck(M object, boolean runBeforeHandlers, boolean runAfterHandlers) {

        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
        MlClass mlClass = metaDataHolder.getMlClassByEntityDynamicClass(object.getClass());
        checkClassAccess(mlClass, MlClassAccessType.CREATE);

        MlClassHandler<M> handler = getHandlerForClassString(object.getInstanceMlClass().getHandler());

        if (runBeforeHandlers && handler != null) {
            handler.beforeCreate(object);
        }
        entityManager.persist(object);
        if (runAfterHandlers && handler != null) {
            handler.afterCreate(object);
        }
        return object;
    }

    /**
     * Создание объекта с выполнением хэндлеров.
     * Не проверяет наличие прав доступа.
     * Использовать только для системных и анонимных вызывов
     *
     * @param object -   объект для создания
     * @return
     */
    public<M extends T> M persistWithoutSecurityCheck(M object) {
        return persistWithoutSecurityCheck(object, true, true);
    }

    /**
     * Создание объекта с возможностью задать необходимость выполнения хэндлеров.
     * Не проверяет наличие прав доступа.
     * Использовать только для системных и анонимных вызывов
     *
     * @param object            -   объект для создания
     * @param runBeforeHandlers -   запускать хэндлер перед созданием
     * @param runAfterHandlers  -   запускать хэндлер после создания
     * @return
     */
    //TODO refactor соединить с сеюрной проверкой
    public<M extends T> M persistWithoutSecurityCheck(M object, boolean runBeforeHandlers, boolean runAfterHandlers) {
        MlClassHandler handler = getHandlerForClassString(object.getInstanceMlClass().getHandler());

        if (runBeforeHandlers && handler != null) {
            handler.beforeCreate(object);
        }
        entityManager.persist(object);
        if (runAfterHandlers && handler != null) {
            handler.afterCreate(object);
        }
        return object;
    }


    /**
     * Обновление объекта с выполнением хэндлеров
     * Не использовать! Оставлен для поддержки старых проектов. При рефакторинге сделать приватным.
     *
     * @param object -   объект для обновления
     */
    @Deprecated
    public T merge(T object) {
        return merge(object, true, true);
    }


    /**
     * Обновление объекта с возможностью задать необходимость выполнения хэндлеров
     * Не использовать! Оставлен для поддержки старых проектов. При рефакторинге сделать приватным.
     *
     * @param object            -   объект для обновления
     * @param runBeforeHandlers -   запускать хэндлер перед созданием
     * @param runAfterHandlers  -   запускать хэндлер после создания
     * @return
     */
    @Deprecated
    public<M extends T> M merge(M object, boolean runBeforeHandlers, boolean runAfterHandlers) {
        MlClassHandler handler = getHandlerForClassString(object.getInstanceMlClass().getHandler());
        if (runBeforeHandlers && handler != null) {
            handler.beforeUpdate(object);
        }
        entityManager.merge(object);
        if (runAfterHandlers && handler != null) {
            handler.afterUpdate(object, runBeforeHandlers);
        }
        return object;
    }


    /**
     * Обновление объекта с выполнением хэндлеров.
     * Проверяет наличие прав доступа на обновление объектов класса
     *
     * @param object -   объект для обновления
     * @return
     */
    public<M extends T> M mergeWithSecurityCheck(M object) {
        return mergeWithSecurityCheck(object, true, true);
    }

    /**
     * Обновление объекта с возможностью задать необходимость выполнения хэндлеров.
     * Проверяет наличие прав доступа на обновление объектов класса
     *
     * @param object            -   объект для обновления
     * @param runBeforeHandlers -   запускать хэндлер перед обновлением
     * @param runAfterHandlers  -   запускать хэндлер после обновления
     * @return
     */
    public<M extends T> M mergeWithSecurityCheck(M object, boolean runBeforeHandlers, boolean runAfterHandlers) {

        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
        MlClass mlClass = metaDataHolder.getMlClassByEntityDynamicClass(object.getClass());
        checkClassAccess(mlClass, MlClassAccessType.UPDATE);

        MlClassHandler handler = getHandlerForClassString(object.getInstanceMlClass().getHandler());
        if (runBeforeHandlers && handler != null) {
            handler.beforeUpdate(object);
        }
        entityManager.merge(object);
        if (runAfterHandlers && handler != null) {
            handler.afterUpdate(object, runBeforeHandlers);
        }
        return object;
    }

    /**
     * Обновление объекта с выполнением хэндлеров.
     * Не проверяет наличие прав доступа.
     * Использовать только для системных и анонимных вызывов
     *
     * @param object -   объект для обновления
     * @return
     */
    public<M extends T> M mergeWithoutSecurityCheck(M object) {
        return mergeWithoutSecurityCheck(object, true, true);
    }

    /**
     * Создание объекта с возможностью задать необходимость выполнения хэндлеров.
     * Не проверяет наличие прав доступа.
     * Использовать только для системных и анонимных вызывов
     *
     * @param object            -   объект для обновления
     * @param runBeforeHandlers -   запускать хэндлер перед обновлением
     * @param runAfterHandlers  -   запускать хэндлер после обновлениян
     * @return
     */
    public<M extends T> M mergeWithoutSecurityCheck(M object, boolean runBeforeHandlers, boolean runAfterHandlers) {
        MlClassHandler handler = getHandlerForClassString(object.getInstanceMlClass().getHandler());
        if (runBeforeHandlers && handler != null) {
            handler.beforeUpdate(object);
        }
        entityManager.merge(object);
        if (runAfterHandlers && handler != null) {
            handler.afterUpdate(object, runBeforeHandlers);
        }
        return object;
    }


    /**
     * Удаление объекта с выполнением хэндлеров
     * Не использовать! Оставлен для поддержки старых проектов. При рефакторинге сделать приватным.
     *
     * @param object -   объект для удаления
     */
    @Deprecated
    public void remove(T object) {
        remove(object, true, true);
    }

    /**
     * Удаление объекта с возможностью задать необходимость выполнения хэндлеров
     * Не использовать! Оставлен для поддержки старых проектов. При рефакторинге сделать приватным.
     *
     * @param object            -   объект для удаления
     * @param runBeforeHandlers -   запускать хэндлер перед созданием
     * @param runAfterHandlers  -   запускать хэндлер после создания
     * @return
     */
    @Deprecated
    public void remove(T object, boolean runBeforeHandlers, boolean runAfterHandlers) {
        MlDynamicEntityImpl dynamicEntity = (MlDynamicEntityImpl) object;
        MlClassHandler handler = getHandlerForClassString(dynamicEntity.getInstanceMlClass().getHandler());
        if (runBeforeHandlers && handler != null) {
            handler.beforeDelete(dynamicEntity);
        }
        entityManager.remove(object);
        entityManager.flush();
        if (runAfterHandlers && handler != null) {
            handler.afterDelete(dynamicEntity);
        }
    }

    /**
     * Удаление объекта с выполнением хэндлеров.
     * Не проверяет наличие прав доступа
     *
     * @param object -   объект для удаления
     * @return
     */
    public<M extends T> void removeWithoutSecurityCheck(M object) {
        removeWithoutSecurityCheck(object, true, true);
    }

    /**
     * Удаление объекта с возможностью задать необходимость выполнения хэндлеров.
     * Не проверяет наличие прав доступа
     *
     * @param object            -   объект для удаления
     * @param runBeforeHandlers -   запускать хэндлер перед удалением
     * @param runAfterHandlers  -   запускать хэндлер после удаления
     * @return
     */
    public<M extends T> void removeWithoutSecurityCheck(M object, boolean runBeforeHandlers, boolean runAfterHandlers) {
        MlClassHandler handler = getHandlerForClassString(object.getInstanceMlClass().getHandler());
        if (runBeforeHandlers && handler != null) {
            handler.beforeDelete(object);
        }
        entityManager.remove(object);
        entityManager.flush();
        if (runAfterHandlers && handler != null) {
            handler.afterDelete(object);
        }
    }

    /**
     * Удаление объекта с выполнением хэндлеров.
     * Проверяет наличие прав доступа на удаление объектов класса
     *
     * @param object -   объект для удаления
     * @return
     */
    public<M extends T> void removeWithSecurityCheck(M object) {
        removeWithSecurityCheck(object, true, true);
    }

    /**
     * Удаление объекта с возможностью задать необходимость выполнения хэндлеров.
     * Проверяет наличие прав доступа на удаление объектов класса
     *
     * @param object            -   объект для удаления
     * @param runBeforeHandlers -   запускать хэндлер перед удалением
     * @param runAfterHandlers  -   запускать хэндлер после удаления
     * @return
     */
    public<M extends T> void removeWithSecurityCheck(M object, boolean runBeforeHandlers, boolean runAfterHandlers) {

        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
        MlClass mlClass = metaDataHolder.getMlClassByEntityDynamicClass(object.getClass());
        checkClassAccess(mlClass, MlClassAccessType.DELETE);

        MlClassHandler handler = getHandlerForClassString(object.getInstanceMlClass().getHandler());
        if (runBeforeHandlers && handler != null) {
            handler.beforeDelete(object);
        }
        entityManager.remove(object);
        entityManager.flush();
        if (runAfterHandlers && handler != null) {
            handler.afterDelete(object);
        }
    }


    /**
     * Рефреш объекта
     *
     * @param object
     */
    public<M extends T> void refresh(M object) {
        entityManager.refresh(object);
    }

    /**
     * Получение MLUID объекта
     *
     * @param objectId - id Объекта
     * @param mlClass  - имя класса объекта
     * @return
     */
    public MLUID getMlUID(Long objectId, String mlClass) {
        T clazz = findById(objectId, mlClass);
        if (clazz.get(MlAttr.GUID) == null) {
            clazz.set(MlAttr.GUID, UUID.randomUUID().toString());
            entityManager.persist(clazz);
        }
        return new MLUID(clazz);
    }

    /**
     * Получить объект по MLUID
     *
     * @param mlUID
     * @return
     */
    public T getByMlUID(MLUID mlUID) {
        List<T> result = entityManager.createQuery("select o from " + mlUID.getClassName() + " o where o." + MlAttr.GUID + " = :guid").setParameter("guid", mlUID.getGuid()).getResultList();
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }

    /**
     * Получение MLUID объекта
     *
     * @param entity
     * @return
     */
    public MLUID getMlUID(DynamicEntity entity) {
        return getMlUID((Long) entity.get(MlAttr.ID), metaDataHolder.getMlClassByEntityDynamicClass(entity.getClass()).getEntityName());
    }

    /**
     * Создание нового объекта заданного класса
     *
     * @param clazz -   класс объекта
     * @return M
     */
    public<M extends T> M createNewEntity(Class<M> clazz) {
        return createNewEntity(clazz, true);
    }

    /**
     * Создает новый объект заданного класса и заполняет для него дефолтные значения
     *
     * @param clazz класс объекта
     * @return DynamicEntity объект
     */
    public<M extends T> M createNewEntity(Class<M> clazz, boolean fillDefaults) {
        try {
            M entity =  clazz.newInstance();
            if (fillDefaults) {
                fillObjectDefaultValues(entity);
            }
            return entity;
        } catch (InstantiationException | IllegalAccessException e) {
            log.error("Can't create object instance for class " + clazz.getName(), e);
            //TODO
            throw new RuntimeException(e);
        }
    }

    /**
     * заполняет дефолтные значения для объекта
     *
     * @param object объект
     */
    public void fillObjectDefaultValues(DynamicEntity object) {
        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
        MlClass mlClass = metaDataHolder.getMlClassByEntityDynamicClass(object.getClass());

        for (MlAttr mlAttr : mlClass.getAttrSet()) {
            // Для каждого атрибута заданного класса пытаемся заполнить дефолтные значения если есть
            if (mlAttr.getEntityFieldName().equals(MlAttr.GUID)) {
                if (object.get(mlAttr.getEntityFieldName()) == null) {
                    object.set(mlAttr.getEntityFieldName(), UUID.randomUUID().toString());
                }
            } else if (mlAttr.getEntityFieldName().equals(MlAttr.LAST_CHANGE)) {
                object.set(mlAttr.getEntityFieldName(), new Date());
            } else if (mlAttr.getDefaultValue() != null && !mlAttr.getDefaultValue().equals("")) {
                Object defaultValue = DefaultValueStrategy.getGenerator(mlAttr).generateValue();
                switch (mlAttr.getFieldType()) {
                    case MANY_TO_ONE:
                        String linkObjectClassName = mlAttr.getLinkClass().getEntityName();
                        Long linkObjectId = Long.parseLong(defaultValue.toString());
                        MlDynamicEntityImpl linkObject = (MlDynamicEntityImpl) this.findById(linkObjectId, linkObjectClassName);
                        object.set(mlAttr.getEntityFieldName(), linkObject);
                        break;
                    case ONE_TO_MANY:
                        break;
                    case MANY_TO_MANY:
                        Gson gson = new Gson();
                        String clsName = mlAttr.getLinkClass().getEntityName();
                        List<Long> ids = gson.fromJson(defaultValue.toString(),new TypeToken<List<Long>>(){}.getType());
                        List<MlDynamicEntityImpl> res = new ArrayList<>();
                        ids.forEach(id->{
                            res.add(findById(id,clsName));
                        });
                        object.set(mlAttr.getEntityFieldName(), res);
                        break;
                    case LONG:
                        object.set(mlAttr.getEntityFieldName(), Long.parseLong(defaultValue.toString()));
                        break;
                    case BOOLEAN:
                        object.set(mlAttr.getEntityFieldName(), Boolean.parseBoolean(defaultValue.toString()));
                        break;
                    case DOUBLE:
                        object.set(mlAttr.getEntityFieldName(), Double.parseDouble(defaultValue.toString()));
                        break;
                    case DATE:
                        if (defaultValue.toString().equals("${CURRENT_DATE}")) {
                            object.set(mlAttr.getEntityFieldName(), new Date());
                        } else {
                            try {
                                String dateFormatString = "dd.MM.yyyy";
                                if (mlAttr.getFormat() != null && !mlAttr.getFormat().trim().isEmpty()) {
                                    dateFormatString = mlAttr.getFormat();
                                }
                                SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatString);

                                object.set(mlAttr.getEntityFieldName(), dateFormat.parse(defaultValue.toString()));
                            } catch (ParseException e) {
                                log.error(String.format("Wrong default value date format for attr [%s]", mlAttr.getEntityFieldName()));
                                throw new MlServerException(String.format("Wrong default value date format for attr [%s]", mlAttr.getEntityFieldName()));
                            }
                        }
                        break;
                    case ENUM:
                        EnumHolder enumHolder = GuiceConfigSingleton.inject(EnumHolder.class);
                        MlAttr enumAttr = mlAttr;
                        if(mlClass.getParent()!=null){
                            enumAttr = findParentAttr(mlClass,mlAttr);
                        }
                        MlEnum mlEnum = enumHolder.getEnum(enumAttr, defaultValue.toString());
                        if (mlEnum != null) {
                            object.set(mlAttr.getEntityFieldName(), defaultValue.toString());
                        }
                        break;
                    default:
                        object.set(mlAttr.getEntityFieldName(), defaultValue);
                }
            }
        }

    }

    private MlAttr findParentAttr(MlClass mlClass, MlAttr mlAttr) {
        MlAttr result = mlClass.getParent().getAttr(mlAttr.getEntityFieldName());
        if(result == null){
            result = mlAttr;
        }
        if(mlClass.getParent().getParent()!=null){
            result = findParentAttr(mlClass.getParent(), result);
        }
        return result;
    }

    /**
     * Клонирование объекта
     *
     * @param forClone -   исходный объект
     * @return -   клонированный объект
     */
    public MlDynamicEntityImpl cloneObject(DynamicEntity forClone) {
        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
        MlClass mlClass = metaDataHolder.getMlClassByEntityDynamicClass(forClone.getClass());

        MlDynamicEntityImpl clone = (MlDynamicEntityImpl) this.createNewEntity(metaDataHolder.getEntityClassByName(mlClass.getEntityName()));
        for (MlAttr attr : mlClass.getAttrSet()) {
            if (!attr.getPrimaryKey()) {
                clone.set(attr.getEntityFieldName(), forClone.get(attr.getEntityFieldName()));
            }
        }
        return clone;
    }

    public void evictL2Cache(DynamicEntity entity) {
        MlClass mlClass = metaDataHolder.getMlClassByEntityDynamicClass(entity.getClass());
        if (mlClass.getCacheable()) {
            if (entityManager.getEntityManagerFactory().getCache().contains(entity.getClass(), entity.get(mlClass.getPrimaryKeyAttr().getEntityFieldName()))) {
                entityManager.getEntityManagerFactory().getCache().evict(entity.getClass(), entity.get(mlClass.getPrimaryKeyAttr().getEntityFieldName()));
            }
        }
    }

    /**
     * Детач объекта
     *
     * @param entity
     */
    public void detach(DynamicEntity entity) {
        entityManager.detach(entity);
    }


    private MlClassHandler getHandlerForClassString(String handlerClassName) {
        // Получаем класс и инстанс хендлера
        if (handlerClassName != null && !"".equals(handlerClassName)) {
            Class handlerClass = null;
            try {
                handlerClass = Class.forName(handlerClassName);
            } catch (ClassNotFoundException e) {
                log.error("", e);
            }
            return (MlClassHandler) GuiceConfigSingleton.inject(handlerClass);
        }
        return null;
    }

    /**
     * Проверка прав доступа к классу.
     * Выбрасывает исключение при отсутствии необходимых прав
     *
     * @param mlClass    -       класс объекта
     * @param accessType -       тип доступа (создание, чтение, обновление, удаление)
     */
    private void checkClassAccess(MlClass mlClass, MlClassAccessType accessType) {
        AccessService accessService = GuiceConfigSingleton.inject(AccessService.class);
        Boolean securityCheck = accessService.checkAccessClass(mlClass, accessType);
        if (!securityCheck) {
            String message = String.format("Отсутствуют права доступа на %s объектов класса \"%s\" [%s].", accessType.getOperationName(), mlClass.getTitle(), mlClass.getEntityName());
            throw new MlSecurityException(message);
        }
    }

    /**
     * Проверка прав доступа к классу.
     * Выбрасывает исключение при отсутствии необходимых прав
     *
     * @param mlClassName -       имя класса объекта
     * @param accessType  -       тип доступа (создание, чтение, обновление, удаление)
     */
    private void checkClassAccess(String mlClassName, MlClassAccessType accessType) {
        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
        MlClass mlClass = metaDataHolder.getMlClassByName(mlClassName);
        checkClassAccess(mlClass, accessType);
    }


}
