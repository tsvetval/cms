package ru.peak.ml.core.dao.value;

import ru.peak.ml.core.model.system.MlAttr;

/**
 * Created by d_litovchenko on 04.03.15.
 */
public abstract class DefaultValueGenerator {
    protected MlAttr mlAttr;

    public void setMlAttr(MlAttr mlAttr) {
        this.mlAttr = mlAttr;
    }
    public abstract Object generateValue();
}
