package ru.peak.ml.core.dao.value;

import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.value.impl.DoNothingValueGenerator;
import ru.peak.ml.core.dao.value.impl.JPQLSingleResultValueGenerator;
import ru.peak.ml.core.model.system.MlAttr;

/**
 * Created by d_litovchenko on 04.03.15.
 */
public class DefaultValueStrategy {

    public static DefaultValueGenerator getGenerator(MlAttr mlAttr){
        DefaultValueGenerator defaultValueGenerator = GuiceConfigSingleton.inject(DoNothingValueGenerator.class);
        if(mlAttr.getDefaultValue().toString().startsWith(JPQLSingleResultValueGenerator.COMMAND_NAME)){
            defaultValueGenerator = GuiceConfigSingleton.inject(JPQLSingleResultValueGenerator.class);
        }
        defaultValueGenerator.setMlAttr(mlAttr);
        return defaultValueGenerator;
    }
}
