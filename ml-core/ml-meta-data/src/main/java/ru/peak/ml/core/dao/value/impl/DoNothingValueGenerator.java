package ru.peak.ml.core.dao.value.impl;

import ru.peak.ml.core.dao.value.DefaultValueGenerator;
import ru.peak.ml.core.model.system.MlAttr;

/**
 * Created by d_litovchenko on 04.03.15.
 */
public class DoNothingValueGenerator extends DefaultValueGenerator {

    @Override
    public Object generateValue() {
        return mlAttr.getDefaultValue();
    }

}
