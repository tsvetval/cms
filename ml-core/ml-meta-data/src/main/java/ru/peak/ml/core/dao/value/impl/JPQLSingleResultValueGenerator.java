package ru.peak.ml.core.dao.value.impl;

import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.dao.value.DefaultValueGenerator;
import ru.peak.ml.core.model.system.MlAttr;

import java.util.List;


/**
 * Created by d_litovchenko on 04.03.15.
 */
public class JPQLSingleResultValueGenerator extends DefaultValueGenerator {
    public static final String COMMAND_NAME = "#JPQL_SINGLE_RESULT=";
    @Override
    public Object generateValue() {
        String defaultValue = mlAttr.getDefaultValue().toString();
        String jpql = defaultValue.replaceFirst(COMMAND_NAME,"");
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        List data = commonDao.getResultList(jpql, mlAttr.getFieldType().getaClass());
        if(data.size()!=1){
            throw new MlApplicationException(String.format("Ошибка генерации значения по умолчанию для поля %s класса %s! Запрос вернул больше одной записи",mlAttr.getDescription(),mlAttr.getMlClass().getDescription()));
        }
        return data.get(0);
    }

}
