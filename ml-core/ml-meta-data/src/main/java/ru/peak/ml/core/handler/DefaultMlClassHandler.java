package ru.peak.ml.core.handler;

import org.eclipse.persistence.internal.dynamic.DynamicEntityImpl;

/**
 *
 */
public abstract class DefaultMlClassHandler<T extends DynamicEntityImpl> implements MlClassHandler<T> {

    @Override
    public void beforeCreate(T entity) {
    }

    @Override
    public void afterCreate(T entity) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void beforeUpdate(T entity) {
    }

    @Override
    public void afterUpdate(T entity, boolean runBeforeHandlers) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void beforeDelete(T entity) {
    }

    @Override
    public void afterDelete(T entity) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void afterSelect(T entity) {
        //To change body of implemented methods use File | Settings | File Templates.
    }


}
