package ru.peak.ml.core.handler;

import org.eclipse.persistence.internal.dynamic.DynamicEntityImpl;

/**
 * Interface for
 */
public interface MlClassHandler<T extends DynamicEntityImpl> {

    void beforeCreate(T entity);

    void afterCreate(T entity);

    void beforeUpdate(T entity);

    void afterUpdate(T entity, boolean runBeforeHandlers);

    void beforeDelete(T entity);

    void afterDelete(T entity);

    void afterSelect(T entity);

}
