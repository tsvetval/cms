package ru.peak.ml.core.handler;

import org.eclipse.persistence.dynamic.DynamicEntity;
import ru.peak.ml.core.model.MLUID;

/**
 *
 */
public interface MlReplicationHandler {
    public void beforeReplication(MLUID mluid);
    public void afterReplication(DynamicEntity dynamicEntity);
}
