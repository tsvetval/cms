package ru.peak.ml.core.handler.validator;

import org.eclipse.persistence.internal.dynamic.DynamicEntityImpl;
import org.eclipse.persistence.internal.sessions.ObjectChangeSet;

/**
 */
public abstract class AbstractValidator<T extends DynamicEntityImpl> {
    public abstract void validateBeforeCreate(T entity);

    public abstract void validateBeforeUpdate(T entity, ObjectChangeSet objectChangeSet);

    public abstract void validateBeforeDelete(T entity);
}
