package ru.peak.ml.core.holders;

import com.google.inject.ImplementedBy;
import ru.peak.ml.core.holders.impl.EnumHolderImpl;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlEnum;

import java.util.Collection;

/**
 *
 */
@ImplementedBy(EnumHolderImpl.class)
public interface EnumHolder {
    void addEnum(MlEnum newEnum);

    Collection<MlEnum> getEnumList(MlAttr IMlAttr);

    MlEnum getEnum(MlAttr IMlAttr, String enumCode);
}
