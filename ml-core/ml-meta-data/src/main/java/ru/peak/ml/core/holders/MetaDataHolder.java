package ru.peak.ml.core.holders;

import com.google.inject.ImplementedBy;
import ru.peak.ml.core.holders.impl.MetaDataHolderImpl;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlAttrGroup;
import ru.peak.ml.core.model.system.MlClass;

import java.util.List;

/**
 *
 */
@ImplementedBy(MetaDataHolderImpl.class)
public interface MetaDataHolder {


    void updateMlClass(MlClass mlClass) throws ClassNotFoundException;

    void addMlClass(MlClass clazz, Class entityClass) throws ClassNotFoundException;

    void addMlAttr(MlAttr attr);

    MlAttr getAttrById(Long id);

    MlClass getMlClassByName(String mlClassName);

    MlClass getMlClassById(Long id);

    Class getEntityClassByName(String entityClassName);

    MlAttr getAttr(MlClass mlClass, String attrName);

    MlAttr getAttr(String mlClass, String attrName);

    List<MlAttr> getPrimaryKeyAttrList(MlClass mlClass);

    MlClass getMlClassByEntityDynamicClass(Class clazz);

    List<MlAttrGroup> getGroupList(MlClass mlClass);

    public List<MlAttr> getInListAttrList(MlClass mlClass);
    public List<MlAttr> getInFormAttrList(MlClass mlClass);
    public List<MlAttr> getPrimaryKey(MlClass mlClass);

    public List<MlAttr> getSimpleSearchAttrList(MlClass mlClass);

    public List<MlAttr> getExtendedSearchAttrList(MlClass mlClass);

    public List<MlClass> getChildrenByClassName(String parentName);
}
