package ru.peak.ml.core.holders;

import com.google.inject.ImplementedBy;
import ru.peak.ml.core.holders.impl.SecurityHolderImpl;
import ru.peak.ml.core.model.folder.MlFolder;
import ru.peak.ml.core.model.page.MlPage;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.security.MlRole;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.model.util.MlUtil;

import java.util.List;
import java.util.Set;

/**
 *
 */
@ImplementedBy(SecurityHolderImpl.class)
public interface SecurityHolder {
    void initAllRoles();

    void addRole(MlRole role);

    Boolean checkAccessPage(List<MlRole> roles, MlPage mlPage);
    Boolean checkAccessPageBlock(List<MlRole> roles, MlPageBlockBase mlPageBlock);

    Set<MlFolder> getRootFolders(List<MlRole> roles);

    boolean checkAccessFolder(List<MlRole> roles, MlFolder folder);

    boolean checkAccessUtil(List<MlRole> roles, MlUtil util);

    boolean checkAccessClassCreate(List<MlRole> roles, MlClass mlClass);
    boolean checkAccessClassRead(List<MlRole> roles, MlClass mlClass);
    boolean checkAccessClassUpdate(List<MlRole> roles, MlClass mlClass);
    boolean checkAccessClassDelete(List<MlRole> roles, MlClass mlClass);

    boolean checkAccessAttrShow(List<MlRole> roles, MlAttr mlAttr);
    boolean checkAccessAttrEdit(List<MlRole> roles, MlAttr mlAttr);
    List<String> getAdditionalQueryForClass(List<MlRole> roles, MlClass mlClass);
}
