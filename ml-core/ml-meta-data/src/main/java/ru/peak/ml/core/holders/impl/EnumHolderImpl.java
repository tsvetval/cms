package ru.peak.ml.core.holders.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.peak.ml.core.holders.EnumHolder;
import ru.peak.ml.core.initializer.impl.MetaDataInitializeServiceImpl;
import ru.peak.ml.core.metadata.comparators.MlEnumOrderComparator;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlEnum;

import javax.inject.Singleton;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Хранилище проинициализированных энумов
 */
@Singleton
public class EnumHolderImpl implements EnumHolder {
    private static final Logger log = LoggerFactory.getLogger(MetaDataInitializeServiceImpl.class);
    /**
     * key -attr id
     * value - map key = enum Code
     * value - enum
     */
    private final Map<Long, Map<String, MlEnum>> mlAttr_mlEnumListMap = new ConcurrentHashMap<>();


    public EnumHolderImpl() {
    }


    @Override
    public void addEnum(MlEnum newEnum) {
        log.debug(String.format("Try Add enum [%s] with id [%d] ", newEnum.getCode(), newEnum.getId()));
        if (newEnum.getAttr() != null) {
            Map<String, MlEnum> enumMap = mlAttr_mlEnumListMap.get(newEnum.getAttr().getId());
            if (enumMap == null) {
                enumMap = new HashMap<>();
                mlAttr_mlEnumListMap.put(newEnum.getAttr().getId(), enumMap);
            } else {
                removeEnumById(enumMap, newEnum.getId());
            }
            enumMap.put(newEnum.getCode(), newEnum);
        } else {
            log.warn(String.format("Enum [%s] with id [%d] has empty attr filed. Skip it",  newEnum.getCode(), newEnum.getId()));
        }
    }

    private void removeEnumById(Map<String, MlEnum> enumMap, Long id) {
        for (Iterator<Map.Entry<String, MlEnum>> it = enumMap.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, MlEnum> entry = it.next();
            if (entry.getValue().getId().equals(id)) {
                it.remove();
            }
        }
    }

    @Override
    public Collection<MlEnum> getEnumList(MlAttr IMlAttr) {
        Map<String, MlEnum> enumMap = mlAttr_mlEnumListMap.get(IMlAttr.getId());
        if (enumMap != null) {
            List<MlEnum> enumList = new ArrayList<MlEnum>(enumMap.values());
            Collections.sort(enumList, new MlEnumOrderComparator());
            return enumList;
        }
        return new ArrayList<>();
    }

    @Override
    public MlEnum getEnum(MlAttr IMlAttr, String enumCode) {
        Map<String, MlEnum> enumMap = mlAttr_mlEnumListMap.get(IMlAttr.getId());
        if (enumMap != null) {
            return enumMap.get(enumCode);
        }
        return null;
    }


}
