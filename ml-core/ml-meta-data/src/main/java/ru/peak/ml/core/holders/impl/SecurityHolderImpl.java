package ru.peak.ml.core.holders.impl;

import ru.ml.core.common.RoleType;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.holders.SecurityHolder;
import ru.peak.ml.core.model.folder.MlFolder;
import ru.peak.ml.core.model.page.MlPage;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.security.*;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.model.util.MlUtil;

import javax.inject.Singleton;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 */
@Singleton
public class SecurityHolderImpl implements SecurityHolder {

    private final Map<Long, Set<MlPage>> roles_Pages = new ConcurrentHashMap<>();

    private final Map<Long, Set<MlFolder>> roles_Folders = new ConcurrentHashMap<>();
    private final Map<Long, Set<MlFolder>> roles_RootFolders = new ConcurrentHashMap<>();

    private final Map<MlFolder, Set<Long>> folders_PageBlocksId = new ConcurrentHashMap<>();

    private final Map<Long, Set<MlUtil>> roles_Utils = new ConcurrentHashMap<>();

    private final Map<Long, Set<MlClass>> roles_ClassesCreate = new ConcurrentHashMap<>();
    private final Map<Long, Set<MlClass>> roles_ClassesRead = new ConcurrentHashMap<>();
    private final Map<Long, Set<MlClass>> roles_ClassesUpdate = new ConcurrentHashMap<>();
    private final Map<Long, Set<MlClass>> roles_ClassesDelete = new ConcurrentHashMap<>();

    private final Map<Long, Set<MlAttr>> roles_AttrNotShow = new ConcurrentHashMap<>();
    private final Map<Long, Set<MlAttr>> roles_AttrNotEdit = new ConcurrentHashMap<>();

    private final Map<Long, Map<MlClass,String>> roles_ClassesQuery = new ConcurrentHashMap<>();


    public SecurityHolderImpl() {
    }


    @Override
    public void initAllRoles() {
        roles_Pages.clear();
        roles_Folders.clear();
        roles_Utils.clear();

        roles_ClassesCreate.clear();
        roles_ClassesRead.clear();
        roles_ClassesUpdate.clear();
        roles_ClassesDelete.clear();
        roles_AttrNotShow.clear();
        roles_AttrNotEdit.clear();
        roles_ClassesQuery.clear();
        folders_PageBlocksId.clear();

        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        List<MlRole> mlRoles = commonDao.getResultList("select o from MlRole o", MlRole.class);
        for (MlRole role : mlRoles) {
            addRole(role);
        }
        fillFolderPageBlocks();
    }


    @Override
    public void addRole(MlRole role) {
        roles_Pages.remove(role.getId());
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        Set<MlPage> pagesIds = new HashSet<>();
        for (MlPageAccess mlPageAccess : role.getPageAccess()) {
            if (mlPageAccess.getUrlRegexp() != null && !mlPageAccess.getUrlRegexp().equals("")) {
                List<MlPage> mlPages = commonDao.getTypedQueryWithoutSecurityCheck("select o from MlPage o where sql(' url ~ ?' ,:regexp)", MlPage.class).setParameter("regexp", mlPageAccess.getUrlRegexp()).getResultList();
                pagesIds.addAll(mlPages);
            }
            pagesIds.addAll(mlPageAccess.getPages());
        }
        roles_Pages.put(role.getId(), pagesIds);

        // Инициализируем доступные папки
        roles_Folders.remove(role.getId());
        roles_RootFolders.remove(role.getId());

        Set<MlFolder> folders = new HashSet<>();
        for (MlFolderAccess folderAccess : role.getFolderAccess()) {
            folders.addAll(folderAccess.getFolders());
//            for(MlFolder folder: folderAccess.getFolders()){
//                while (folder.getParent()!=null){
//                    folders.add(folder.getParent());
//                    folder = folder.getParent();
//                }
//            }
        }
        roles_Folders.put(role.getId(), folders);

        // инициализируем рутовые папки
        Set<MlFolder> rootFolders = new HashSet<>();
        for (MlFolder folder : folders) {
            if (folder.getParent() == null ||
                    // Есть родитель но нет доступа к нему
                    !folders.contains(folder.getParent())) {
                rootFolders.add(folder);
            }
        }
        roles_RootFolders.put(role.getId(), rootFolders);


        roles_Utils.remove(role.getId());
        Set<MlUtil> utils = new HashSet<>();
        for (MlUtilAccess utilAccess : role.getUtilAccess()) {
            utils.addAll(utilAccess.getUtils());
        }
        roles_Utils.put(role.getId(), utils);

        roles_ClassesCreate.remove(role.getId());
        roles_ClassesRead.remove(role.getId());
        roles_ClassesUpdate.remove(role.getId());
        roles_ClassesDelete.remove(role.getId());
        roles_ClassesQuery.remove(role.getId());
        Set<MlClass> classesCreate = new HashSet<>();
        Set<MlClass> classesRead = new HashSet<>();
        Set<MlClass> classesUpdate = new HashSet<>();
        Set<MlClass> classesDelete = new HashSet<>();
        Map<MlClass,String> additionalQuery = new ConcurrentHashMap<>();
        for (MlClassAccess classAccess : role.getClassAccess()) {
            if(classAccess.isCreate()){
                classesCreate.add(classAccess.getMlClass());
            }
            if(classAccess.isRead()){
                classesRead.add(classAccess.getMlClass());
            }
            if(classAccess.isUpdate()){
                classesUpdate.add(classAccess.getMlClass());
            }
            if(classAccess.isDelete()){
                classesDelete.add(classAccess.getMlClass());
            }
            if(classAccess.getAdditionalQuery()!=null && !classAccess.getAdditionalQuery().isEmpty()){
                additionalQuery.put(classAccess.getMlClass(),classAccess.getAdditionalQuery());
            }
        }
        roles_ClassesCreate.put(role.getId(), classesCreate);
        roles_ClassesRead.put(role.getId(), classesRead);
        roles_ClassesUpdate.put(role.getId(), classesUpdate);
        roles_ClassesDelete.put(role.getId(), classesDelete);
        roles_ClassesQuery.put(role.getId(), additionalQuery);

        roles_AttrNotShow.remove(role.getId());
        roles_AttrNotEdit.remove(role.getId());
        Set<MlAttr> attrNotShow = new HashSet<>();
        Set<MlAttr> attrNotEdit = new HashSet<>();
        for(MlAttrAccess attrAccess: role.getAttrAccess()){
            if(attrAccess.isNotShow()){
                attrNotShow.add(attrAccess.getMlAttr());
            }
            if(attrAccess.isNotEdit()){
                attrNotEdit.add(attrAccess.getMlAttr());
            }
        }
        roles_AttrNotShow.put(role.getId(), attrNotShow);
        roles_AttrNotEdit.put(role.getId(), attrNotEdit);
    }

    private void fillFolderPageBlocks() {
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        List<MlFolder> mlFolders = commonDao.getAll(MlFolder.class);
        for(MlFolder folder: mlFolders){
            if(folder.getPageBlocks()!= null && !folder.getPageBlocks().isEmpty()){
                Set<Long> pageBlockIds = new HashSet<>();
                for(MlPageBlockBase pageBlock: folder.getPageBlocks()){
                    pageBlockIds.add(pageBlock.getId());
                }
                folders_PageBlocksId.put(folder,pageBlockIds);
            }
        }
    }


    @Override
    public Boolean checkAccessPage(List<MlRole> roles, MlPage mlPage) {
        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                return true;
            }
            if (roles_Pages.get(role.getId()).contains(mlPage)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Set<MlFolder> getRootFolders(List<MlRole> roles) {
        Set<MlFolder> result = new HashSet();

        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                throw new RuntimeException("Check for MAIN_ADMIN_ROLE before ");
            }
            result.addAll(roles_RootFolders.get(role.getId()));
        }
        return result;
    }

    @Override
    public boolean checkAccessFolder(List<MlRole> roles, MlFolder folder) {
        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                return true;
            }
            if (roles_Folders.get(role.getId()).contains(folder)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessUtil(List<MlRole> roles, MlUtil util) {
        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                return true;
            }
            if (roles_Utils.get(role.getId()).contains(util)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessClassCreate(List<MlRole> roles, MlClass mlClass) {
        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                return true;
            }
            if (roles_ClassesCreate.get(role.getId()).contains(mlClass)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessClassRead(List<MlRole> roles, MlClass mlClass) {
        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                return true;
            }
            if (roles_ClassesRead.get(role.getId()).contains(mlClass)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessClassUpdate(List<MlRole> roles, MlClass mlClass) {
        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                return true;
            }
            if (roles_ClassesUpdate.get(role.getId()).contains(mlClass)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessClassDelete(List<MlRole> roles, MlClass mlClass) {
        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                return true;
            }
            if (roles_ClassesDelete.get(role.getId()).contains(mlClass)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessAttrEdit(List<MlRole> roles, MlAttr mlAttr) {
        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                return true;
            }
            if (roles_AttrNotEdit.get(role.getId()).contains(mlAttr)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean checkAccessAttrShow(List<MlRole> roles, MlAttr mlAttr) {
        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                return true;
            }
            if (roles_AttrNotShow.get(role.getId()).contains(mlAttr)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public List<String> getAdditionalQueryForClass(List<MlRole> roles, MlClass mlClass) {
        List<String> result = new ArrayList<>();
        for(MlRole role: roles){
            if(roles_ClassesQuery.get(role.getId()).containsKey(mlClass)){
                result.add(roles_ClassesQuery.get(role.getId()).get(mlClass));
            }
        }
        return result;
    }

    @Override
    public Boolean checkAccessPageBlock(List<MlRole> roles, MlPageBlockBase mlPageBlock) {
        for(MlRole role: roles){
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                return true;
            }
            for(MlFolder folder: roles_Folders.get(role.getId())){
                for(Long pageBlockId: folders_PageBlocksId.get(folder)){
                    if(pageBlockId.equals(mlPageBlock.getId())){
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
