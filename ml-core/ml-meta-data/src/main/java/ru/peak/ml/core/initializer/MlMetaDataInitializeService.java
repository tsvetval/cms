package ru.peak.ml.core.initializer;


import org.eclipse.persistence.jpa.dynamic.JPADynamicTypeBuilder;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.model.system.MlEnum;

import java.util.Collection;
import java.util.Map;

/**
 *
 */
public interface MlMetaDataInitializeService {
    /**
     * Проводит инициализацию мета данных
     */
    public void initializeAllMetaData();

    public void initializeClasses(Collection<MlClass> detachedMlClassList, Boolean fullReload) throws ClassNotFoundException;


    /**
     * добавляет заданный класс в хранилище мета данных
     * @param entity объект  класса MlBootClass
     * @param entityClass  java класс для данного объекта
     * @throws ClassNotFoundException если для заданного класса не найден  entityClass
     */
    public void addMlClass(MlClass entity, Class entityClass) throws ClassNotFoundException;

    /**
     * добавляет заданный класс в хранилище мета данных
     * @param entity  entity объект  класса MlClass
     * @throws ClassNotFoundException
     */
    public void updateMlClass(MlClass entity) throws ClassNotFoundException;

    /**добавляет заданное перечислимое в хранилище мета данных
     *
     * @param newEnum перечислимое
     */
    public void addEnum(MlEnum newEnum);
}
