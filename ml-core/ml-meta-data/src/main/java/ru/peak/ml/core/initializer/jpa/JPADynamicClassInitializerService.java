package ru.peak.ml.core.initializer.jpa;

import com.google.inject.ImplementedBy;
import org.eclipse.persistence.jpa.dynamic.JPADynamicTypeBuilder;
import ru.peak.ml.core.initializer.jpa.impl.JPADynamicClassInitializerImpl;
import ru.peak.ml.core.model.system.MlClass;

import java.util.Collection;
import java.util.Map;

/**
 *
 */
@ImplementedBy(JPADynamicClassInitializerImpl.class)
public interface JPADynamicClassInitializerService {

    public Map<String, JPADynamicTypeBuilder> initializeClasses(Collection<MlClass> detachedMlClassList, Boolean fullReload) throws ClassNotFoundException;

    void deleteClassWriter(MlClass entity);
}
