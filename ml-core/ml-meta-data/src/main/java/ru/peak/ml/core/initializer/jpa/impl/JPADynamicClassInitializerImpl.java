package ru.peak.ml.core.initializer.jpa.impl;

import ddl.creator.DDLGenerator;
import org.eclipse.persistence.annotations.BatchFetchType;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.descriptors.ClassDescriptor;
import org.eclipse.persistence.dynamic.DynamicType;
import org.eclipse.persistence.internal.dynamic.DynamicTypeImpl;
import org.eclipse.persistence.internal.helper.DatabaseField;
import org.eclipse.persistence.jpa.dynamic.JPADynamicHelper;
import org.eclipse.persistence.jpa.dynamic.JPADynamicTypeBuilder;
import org.eclipse.persistence.mappings.*;
import org.eclipse.persistence.sequencing.NativeSequence;
import org.eclipse.persistence.sequencing.Sequence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.AttrType;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.initializer.jpa.JPADynamicClassInitializerService;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.Blob;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 */
public class JPADynamicClassInitializerImpl implements JPADynamicClassInitializerService {
    private static final Logger log = LoggerFactory.getLogger(JPADynamicClassInitializerImpl.class);

    private final String JPA_DYNAMIC_MODEL_CLASS_PACKAGE = "ru.peak.ml.jpa.model.dynamic.";
    private final String INHERIT_FIELD_NAME = "realDynamicEntity";

    @Inject
    EntityManagerFactory emf;

    //@Inject
    //DDLGenerator ddlGenerator;

    Map<String, JPADynamicTypeBuilder> initializedTypeBuilderMap;


    @Override
    public void deleteClassWriter(MlClass entity) {
        if (entity.getJavaClass() != null && !entity.getJavaClass().isEmpty()) {
            log.debug(String.format("Remove ClassWriter for class %s , mlclass %d", entity.getJavaClass(), entity.getId()));
            GuiceConfigSingleton.getInstance().getDcl().removeClassWriter(entity.getJavaClass());
        } else {
            log.debug(String.format("Remove ClassWriter for class %s , mlclass %d", JPA_DYNAMIC_MODEL_CLASS_PACKAGE + entity.getEntityName(), entity.getId()));
            GuiceConfigSingleton.getInstance().getDcl().removeClassWriter(JPA_DYNAMIC_MODEL_CLASS_PACKAGE + entity.getEntityName());
        }
    }

    public Map<String, JPADynamicTypeBuilder> initializeClasses(Collection<MlClass> detachedMlClassList, Boolean fullReload) throws ClassNotFoundException {
        // Список динамик типов на переинииализацию в JPA
        detachedMlClassList = addAllRelationClasses(detachedMlClassList);
        List<DynamicType> typeList = new ArrayList<>();
        JPADynamicHelper helper = new JPADynamicHelper(emf);
        initializedTypeBuilderMap = new HashMap<>();
        log.debug("Start to initialize ClassList size = " + detachedMlClassList.size());

        //Создаем мапу классов на инициализацию
        Map<String, MlClass> notInitializedEntityTypes = new ConcurrentHashMap<>();
        for (MlClass mlClass : detachedMlClassList) {
            notInitializedEntityTypes.put(mlClass.getEntityName(), mlClass);
            // !!!!Добавляем всех родителей класса в инициализацию (нужно для правильной работы JPA)
            if (mlClass.getParent() != null) {
                MlClass rootParent = getRootParent(mlClass);
                if (!notInitializedEntityTypes.containsKey(rootParent.getEntityName())) {
                    notInitializedEntityTypes.put(rootParent.getEntityName(), rootParent);
                }

                fillMapWithClassTree(notInitializedEntityTypes, helper, rootParent);
            }
        }
        List<MlClass> classesForInitialization = new ArrayList<>();
        classesForInitialization.addAll(notInitializedEntityTypes.values());

        // Создаем типы по всем классам (Необходимо для линковки)
        while (notInitializedEntityTypes.size() != 0) {
            List<MlClass> noyInitializedMLClasses = new ArrayList<>(notInitializedEntityTypes.values());
            for (MlClass mlClass : noyInitializedMLClasses) {
                // Добавляем класс к нашему метаописанию
                log.debug(String.format("Initialize type for MlClass [%s]", mlClass.getEntityName()));

                if (mlClass.getParent() == null || // Нет родителя
                        // Родитель уже проинициализирован
                        !notInitializedEntityTypes.containsKey(mlClass.getParent().getEntityName())) {
                    DynamicType parentType = null;
                    DynamicType rootParentType = null;
                    // Определяем класс родителя
                    if (mlClass.getParent() != null) {
                        MlClass rootParentClass = getRootParent(mlClass);
                        if (initializedTypeBuilderMap.containsKey(mlClass.getParent().getEntityName())) {
                            parentType = initializedTypeBuilderMap.get(mlClass.getParent().getEntityName()).getType();
                            rootParentType = initializedTypeBuilderMap.get(rootParentClass.getEntityName()).getType();

                        } else {
                            parentType = helper.getType(mlClass.getParent().getEntityName());
                            rootParentType = helper.getType(rootParentClass.getEntityName());
                        }
                    }
                    JPADynamicTypeBuilder tmpType = initializeClass(mlClass, parentType, rootParentType);
                    initializedTypeBuilderMap.put(mlClass.getEntityName(), tmpType);
                    notInitializedEntityTypes.remove(mlClass.getEntityName());
                }
            }
        }

        //инициализируем атрибуты родительсских классов
        for (MlClass mlClass : classesForInitialization) {
            initializeParentAttrList(mlClass);
        }

        // Инициализируем простые атрибуту и первичные ключи
        for (MlClass mlClass : classesForInitialization) {
            log.debug(String.format("Initialize Simple Attrs for MlClass [%s]", mlClass.getEntityName()));
            initializeSimpleAttr(mlClass, surroundDelimiter(mlClass.getTableName()));
        }
        // инициализируем  MANY_TO_ONE
        for (MlClass mlClass : classesForInitialization) {
            log.debug(String.format("Initialize MANY_TO_ONE for MlClass [%s]", mlClass.getEntityName()));
            initializeManyToOneAttr(mlClass, helper);
        }


        // инициализируем ONE_TO_MANY
        for (MlClass mlClass : classesForInitialization) {
            log.debug(String.format("Initialize ONE_TO_MANY for MlClass [%s]", mlClass.getEntityName()));
            initializeOneToManyAttr(mlClass, helper);
        }

        // инициализируем MANY_TO_MANY
        for (MlClass mlClass : classesForInitialization) {
            log.debug(String.format("Initialize MANY_TO_MANY for MlClass [%s]", mlClass.getEntityName()));
            initializeManyToManyAttr(mlClass, helper);
        }

        //инициализация HETERO_LINK
        for (MlClass mlClass : classesForInitialization) {
            log.debug(String.format("Initialize HETERO_LINK for MlClass [%s]", mlClass.getEntityName()));
            initializeHeteroLinkAttr(mlClass, helper);
        }
        // добавляем потомкам связи родителей
        for (MlClass mlClass : classesForInitialization) {
            if(mlClass.getParent()!=null){
                log.debug(String.format("Initialize relations from parent for MlClass [%s]", mlClass.getEntityName()));
                addRelationsFromParent(mlClass, helper);
            }
        }

        for (MlClass mlClass : classesForInitialization) {
            log.debug(String.format("Initialize History for MlClass [%s]", mlClass.getEntityName()));
            initializeHistoryPolicy(mlClass, surroundDelimiter(mlClass.getTableName()));
        }

        //initHeteroLinkClass(classesForInitialization);

        // Проводим создание энтитей
        for (JPADynamicTypeBuilder builder : initializedTypeBuilderMap.values()) {
            typeList.add(builder.getType());
        }
        log.debug("Clear JPQLParseCache and register Types");
        helper.getSession().getProject().getJPQLParseCache().getCache().clear();
        helper.addTypes(false, false, typeList.toArray(new DynamicType[typeList.size()]));
        log.debug("Initialization finished success");
        return initializedTypeBuilderMap;
    }

    /*private void initHeteroLinkClass(List<MlClass> classesForInitialization) {
        for(MlClass mlClass : classesForInitialization){
            if(mlClass.getEntityName().equals("MlHeteroLink")){
                JPADynamicTypeBuilder dynamicTypeBuilder = initializedTypeBuilderMap.get(mlClass.getEntityName());
                VariableOneToOneMapping oneToOneMapping = new VariableOneToOneMapping();
                oneToOneMapping.setTypeFieldName("class");
                Vector vector= new Vector();
                vector.add("\"MlHeteroLink\".\"objectId\"");
                oneToOneMapping.setForeignKeyFieldNames(vector);
                Vector vector1= new Vector();

                Association association = new Association("\"MlHeteroLink\".\"objectId\"","id");
                vector1.add(association);
                oneToOneMapping.setSourceToTargetQueryKeyFieldAssociations(vector1);

                for(MlClass aClass: classesForInitialization){
                    JPADynamicTypeBuilder dynamicTypeBuilder1 = initializedTypeBuilderMap.get(aClass.getEntityName());
                    oneToOneMapping.addClassIndicator(dynamicTypeBuilder1.getType().getJavaClass(),aClass.getEntityName());
                }
                oneToOneMapping.setAttributeName("object");
                oneToOneMapping.setReferenceClass(MlPageBlockBase.class);

                //oneToOneMapping.addForeignKeyField(new DatabaseField("link"), new DatabaseField("id"));
                dynamicTypeBuilder.addMapping(oneToOneMapping);
            }
        }
    }*/

    private void initializeHeteroLinkAttr(MlClass mlClass, JPADynamicHelper helper) {
        JPADynamicTypeBuilder dynamicTypeBuilder = initializedTypeBuilderMap.get(mlClass.getEntityName());
        for (MlAttr mlAttr : mlClass.getAttrSet()) {
            if (!mlAttr.getMlClass().getEntityName().equals(mlClass.getEntityName())) {
                //атрибут родительского класса не инициализируем (он родительского так проинициализирован)
                //либо атрибут виртуальный
                continue;
            }
            if (mlAttr.getFieldType().equals(AttrType.HETERO_LINK)) {
                log.debug(String.format("Initialize HETERO_LINK Attr [%s]", mlAttr.getEntityFieldName()));

                String relationTableName = surroundDelimiter(MlAttr.HETERO_LINK_TABLE_PREFIX+"_"+mlAttr.getMlClass().getTableName()+"_"+mlAttr.getEntityFieldName());
                ManyToManyMapping mapping = addManyToManyMapping(dynamicTypeBuilder, mlAttr.getEntityFieldName(),
                        helper.getType("MlHeteroLink") == null ?
                                initializedTypeBuilderMap.get("MlHeteroLink").getType() : helper.getType("MlHeteroLink"),
                        relationTableName);
                //if (mlAttr.getManyToManyFieldNameM() != null && !mlAttr.getManyToManyFieldNameM().equals("")) {
                    mapping.getSourceRelationKeyFields().clear();
                    mapping.setSourceRelationKeyFieldName(surroundDelimiter(mlAttr.getMlClass().getTableName()+"_id"));
               // }
               // if (mlAttr.getManyToManyFieldNameN() != null && !mlAttr.getManyToManyFieldNameN().equals("")) {
                    mapping.getTargetRelationKeyFields().clear();
                    mapping.setTargetRelationKeyFieldName(surroundDelimiter(MlAttr.HETERO_LINK_FIELD_NAME));
                //}

                dynamicTypeBuilder.addMapping(mapping);
                mapping.setIsLazy(mlAttr.getLazy() != null ? mlAttr.getLazy() : false);

                //Если список упорядоченный, добавляем упорядочивание
                if (mlAttr.isOrdered()) {
                    String orderColumn = MlAttr.HETERO_LINK_TABLE_PREFIX+"_"+mlAttr.getMlClass().getTableName()+"_"+mlAttr.getEntityFieldName()+"_order";
                    mapping.setListOrderFieldName(surroundDelimiter(orderColumn));
                    //закомментил потому что выбрасовалось исключение при инициализации существующих связий
                    //mapping.setOrderCorrectionType(OrderCorrectionType.EXCEPTION);
                }
            }
        }
    }

    private Collection<MlClass> addAllRelationClasses(Collection<MlClass> detachedMlClassList) {
        List<MlClass> result = new ArrayList<>();
        EntityManager commonDao = emf.createEntityManager();
        for (MlClass mlClass : detachedMlClassList) {
            result.add(mlClass);
            result.addAll(commonDao.createQuery("select distinct o.mlClass from MlAttr o where o.linkClass.id = " + mlClass.getId()).getResultList());
        }
        return result;
    }

    private void fillMapWithClassTree(Map<String, MlClass> notInitializedEntityTypes, JPADynamicHelper helper, MlClass parnetClass) {
        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);

        if (helper.getType(parnetClass.getEntityName()) != null && helper.getType(parnetClass.getEntityName()).getDescriptor()
                .getInheritancePolicy() != null) {
            List<ClassDescriptor> classDescriptorList = helper.getType(parnetClass.getEntityName()).getDescriptor()
                    .getInheritancePolicy().getChildDescriptors();
            for (ClassDescriptor classDescriptor : classDescriptorList) {
                MlClass childClass = metaDataHolder.getMlClassByName(classDescriptor.getAlias());
                if (childClass != null && !notInitializedEntityTypes.containsKey(childClass.getEntityName())) {
                    if (!notInitializedEntityTypes.containsKey(childClass.getEntityName())) {
                        // Если есть дочернии класс которго еще нет в мапе на переинициализацию то так же добавляем его
                        notInitializedEntityTypes.put(childClass.getEntityName(), childClass);
                    }
                    // Добавляем на переинициализацию все дочернии классы текущего
                    fillMapWithClassTree(notInitializedEntityTypes, helper, childClass);
                }
            }
        }

    }

    private MlClass getRootParent(MlClass mlClass) {
        if (mlClass.getParent() == null) {
            return mlClass;
        } else {
            return getRootParent(mlClass.getParent());
        }
    }

    private void insertManyToManyClassesForInitialize(MlClass mlClass, List<MlClass> detachedMlClassList, Map<String, MlClass> notInitializedEntityTypes) {
        for (MlAttr bootAttr : mlClass.getAttrSet()) {
            if (bootAttr.getFieldType() == AttrType.MANY_TO_MANY) {
                if (!detachedMlClassList.contains(bootAttr.getLinkClass())) {
                    detachedMlClassList.add(bootAttr.getLinkClass());
                    notInitializedEntityTypes.put(bootAttr.getLinkClass().getEntityName(), bootAttr.getLinkClass());
                    //рекурсия, т.к. можем добавлять класс у которого есть MANY_TO_MANY
                    insertManyToManyClassesForInitialize(bootAttr.getLinkClass(), detachedMlClassList, notInitializedEntityTypes);
                }
            }
        }
    }

    private void initializeHistoryPolicy(MlClass mlClass, String parentClassPath) {
        JPADynamicTypeBuilder dynamicTypeBuilder = initializedTypeBuilderMap.get(mlClass.getEntityName());
        log.debug("Start initialize history");
        if (mlClass.hasHistory()) {
            MlHistoryPolicy policy = new MlHistoryPolicy();
            policy.addHistoryTableName(surroundDelimiter(mlClass.getTableName() + MlClass.HISTORY_TABLE_POSTFIX));
            policy.addStartFieldName(surroundDelimiter(MlAttr.HISTORY_START_DATE_FIELD_NAME));
            policy.addEndFieldName(surroundDelimiter(MlAttr.HISTORY_END_DATE_FIELD_NAME));
            policy.setUserField(surroundDelimiter(MlAttr.HISTORY_USER_LOGIN_FIELD_NAME));
            policy.setActionType(surroundDelimiter(MlAttr.HISTORY_ACTION_TYPE_FIELD_NAME));
            policy.getHistoricalTables().get(0).setName(surroundDelimiter(mlClass.getTableName()));
            dynamicTypeBuilder.getType().getDescriptor().setHistoryPolicy(policy);
        }
        log.debug("Initialization history finished");
    }

    /**
     * Метод проводит начальную инициализацию энтити в JPA (фактически только болванку, это необходимо в силу алгоритма)
     *
     * @param mlClass    инициализируемый класс
     * @param parentType непосредственный родитель инициализируемого класса
     * @param rootParent самы верхний родитель иерархии инициализируемого класса
     * @return проинициализированный тип
     * @throws ClassNotFoundException
     */
    private JPADynamicTypeBuilder initializeClass(MlClass mlClass, DynamicType parentType, DynamicType rootParent) throws ClassNotFoundException {
        // Удаляем старый класс если есть
        JPADynamicHelper helper = new JPADynamicHelper(emf);
        // em.detach(mlClass);
        //TODO вроде это не нужно
        if (rootParent != null) {
            try {
                helper.removeType(rootParent.getDescriptor().getAlias());
            } catch (Exception e) {
                log.warn("Skip remove type error!");
            }
        } else {
            if (helper.getType(mlClass.getEntityName()) == null || helper.getType(mlClass.getEntityName()).getParentType() == null) {
                try {
                    helper.removeType(mlClass.getEntityName());
                } catch (Exception e) {
                    log.warn("Skip remove type error!");
                }
            }
        }

        deleteClassWriter(mlClass);
        // Инициализируем Энтити и их первичные ключи
        //TODO do with parents
        JPADynamicTypeBuilder dynamicTypeBuilder;
        if (parentType == null || rootParent == null) {
            // Наследования нет
            Class<?> newJpaClass;
            if (mlClass.getJavaClass() != null && !mlClass.getJavaClass().isEmpty()) {
                //Добавлено для того чтобы классы попадали в главный загрузчик классов
                Class.forName(mlClass.getJavaClass());
                newJpaClass = GuiceConfigSingleton.getInstance().getDcl().createDynamicClass(mlClass.getJavaClass());
            } else {

                newJpaClass = GuiceConfigSingleton.getInstance().getDcl()
                        .createDynamicClass(JPA_DYNAMIC_MODEL_CLASS_PACKAGE + mlClass.getEntityName(), MlDynamicEntityImpl.class);
            }

            dynamicTypeBuilder = new JPADynamicTypeBuilder(newJpaClass, null, surroundDelimiter(mlClass.getTableName())/*tableList.toArray(new String[tableList.size()])*/);
            if (mlClass.getCacheable()) {
                dynamicTypeBuilder.getType().getDescriptor().setCacheIsolation(CacheIsolationType.SHARED);
                log.debug(String.format("Cache enable for class [%s]", mlClass.getEntityName()));
            } else {
                dynamicTypeBuilder.getType().getDescriptor().setCacheIsolation(CacheIsolationType.ISOLATED);
                log.debug(String.format("Cache disable for class [%s]", mlClass.getEntityName()));
            }
        } else {
            // Есть наследование
            Class<?> newJpaClass;
            if (mlClass.getJavaClass() != null && !"".equals(mlClass.getJavaClass())) {
                newJpaClass = GuiceConfigSingleton.getInstance().getDcl().createDynamicClass(mlClass.getJavaClass());
            } else {
                String packageName = JPA_DYNAMIC_MODEL_CLASS_PACKAGE;
                while (!GuiceConfigSingleton.getInstance().getDcl().isClassNotLoaded(packageName + mlClass.getEntityName())) {
                    Class child = GuiceConfigSingleton.getInstance().getDcl().loadClass(packageName + mlClass.getEntityName());
                    if (!child.getSuperclass().equals(parentType.getJavaClass())) {
                        packageName += "a.";
                    } else {
                        break;
                    }
                }
                newJpaClass = GuiceConfigSingleton.getInstance().getDcl().createDynamicClass(packageName + mlClass.getEntityName(), parentType.getJavaClass());
            }

            dynamicTypeBuilder = new JPADynamicTypeBuilder(newJpaClass, parentType, surroundDelimiter(mlClass.getTableName()));

            // Устанавливаем политики для наследования
            ClassDescriptor classDescriptor = dynamicTypeBuilder.getType().getDescriptor();
            classDescriptor.getInheritancePolicy().setParentClass(parentType.getJavaClass());
            classDescriptor.getInheritancePolicy().setParentDescriptor(parentType.getDescriptor());
            classDescriptor.getInheritancePolicy().getRootParentDescriptor();

            ClassDescriptor rootParentDescriptor = rootParent.getDescriptor();
            rootParentDescriptor.getInheritancePolicy().setShouldUseClassNameAsIndicator(false);
            rootParentDescriptor.getInheritancePolicy().setShouldOuterJoinSubclasses(true);
            rootParentDescriptor.getInheritancePolicy().addClassIndicator(parentType.getJavaClass(), mlClass.getParent().getEntityName());
            //TODO detect what for
            rootParentDescriptor.getInheritancePolicy().setClassIndicatorField(new DatabaseField(surroundDelimiter(INHERIT_FIELD_NAME), rootParent.getDescriptor().getTableName()));
            rootParentDescriptor.getInheritancePolicy().addClassIndicator(dynamicTypeBuilder.getType().getJavaClass(), mlClass.getEntityName());

            dynamicTypeBuilder.getType().getDescriptor().setCacheIsolation(rootParent.getDescriptor().getCacheIsolation());
        }
        return dynamicTypeBuilder;
    }


    private void initializeOneToManyAttr(MlClass mlClass, JPADynamicHelper helper) {
        JPADynamicTypeBuilder dynamicTypeBuilder = initializedTypeBuilderMap.get(mlClass.getEntityName());
        for (MlAttr mlAttr : mlClass.getAttrSet()) {
            if (!mlAttr.getMlClass().getEntityName().equals(mlClass.getEntityName())
                    || mlAttr.isVirtual()) {
                //атрибут родительского класса не инициализируем (он родительского так проинициализирован)
                //не инициализируем виртуальный атрибут
                continue;
            }
            if (mlAttr.getFieldType().equals(AttrType.ONE_TO_MANY)) {
                log.debug(String.format("Initialize ONE_TO_MANY Attr [%s]", mlAttr.getEntityFieldName()));
                OneToManyMapping link;
                if(mlClass.getParent()!=null){
                    JPADynamicTypeBuilder parent = initializedTypeBuilderMap.get(mlClass.getParent().getEntityName());
                    link = parent.addOneToManyMapping(mlAttr.getEntityFieldName(),
                            initializedTypeBuilderMap.get(mlAttr.getLinkAttr().getMlClass().getEntityName()) == null ?
                                    helper.getType(mlAttr.getLinkAttr().getMlClass().getEntityName()) :
                                    initializedTypeBuilderMap.get(mlAttr.getLinkAttr().getMlClass().getEntityName()).getType(),
                            surroundDelimiter(mlAttr.getLinkAttr().getTableFieldName()));
                }else{
                    link = dynamicTypeBuilder.addOneToManyMapping(mlAttr.getEntityFieldName(),
                        initializedTypeBuilderMap.get(mlAttr.getLinkAttr().getMlClass().getEntityName()) == null ?
                                helper.getType(mlAttr.getLinkAttr().getMlClass().getEntityName()) :
                                initializedTypeBuilderMap.get(mlAttr.getLinkAttr().getMlClass().getEntityName()).getType(),
                        surroundDelimiter(mlAttr.getLinkAttr().getTableFieldName()));
                }
                link.setIsLazy(mlAttr.getLazy() != null ? mlAttr.getLazy() : false);
                if (mlAttr.isOrdered()) {
                    String orderField = mlAttr.getLinkAttr().getEntityFieldName() + "_order";
                    link.setListOrderFieldName(surroundDelimiter(orderField));
                }
                setFetchType(mlAttr,link);
            }
        }
    }

    private void addRelationsFromParent(MlClass mlClass, JPADynamicHelper helper){
        JPADynamicTypeBuilder dynamicTypeBuilder = initializedTypeBuilderMap.get(mlClass.getEntityName());
        DynamicTypeImpl parentType = null;
        if (mlClass.getParent() != null) {
            JPADynamicTypeBuilder parentTypeBuilder = initializedTypeBuilderMap.get(mlClass.getParent().getEntityName());
            if (parentTypeBuilder == null) {
                parentType = (DynamicTypeImpl) helper.getType(mlClass.getParent().getEntityName());
            } else {
                parentType = (DynamicTypeImpl) parentTypeBuilder.getType();
            }
        }
        for (MlAttr mlAttr : mlClass.getAttrSet()) {
            if (mlAttr.isVirtual()) {
                continue;
            }
            if (!mlAttr.getMlClass().getEntityName().equals(mlClass.getEntityName())) {
                //атрибут родительского класса не инициализируем (он родительского так проинициализирован)
                if (mlAttr.getFieldType().equals(AttrType.MANY_TO_ONE)) {
                    if (parentType != null) {
                        for (DatabaseMapping mapping : parentType.getDescriptor().getMappings()) {
                            if (mapping.getAttributeName().equals(mlAttr.getEntityFieldName())) {
                                ((DynamicTypeImpl) dynamicTypeBuilder.getType()).getMappingsRequiringInitialization().add(mapping);
                            }
                        }
                    }
                }
                continue;
            }
        }
    }

    private void initializeManyToOneAttr(MlClass mlClass, JPADynamicHelper helper) {
        JPADynamicTypeBuilder dynamicTypeBuilder = initializedTypeBuilderMap.get(mlClass.getEntityName());

        DynamicTypeImpl parentType = null;
        if (mlClass.getParent() != null) {
            JPADynamicTypeBuilder parentTypeBuilder = initializedTypeBuilderMap.get(mlClass.getParent().getEntityName());
            if (parentTypeBuilder == null) {
                parentType = (DynamicTypeImpl) helper.getType(mlClass.getParent().getEntityName());
            } else {
                parentType = (DynamicTypeImpl) parentTypeBuilder.getType();
            }
            ((DynamicTypeImpl) dynamicTypeBuilder.getType()).getMappingsRequiringInitialization().addAll(parentType.getMappingsRequiringInitialization());
        }

        for (MlAttr mlAttr : mlClass.getAttrSet()) {
            if (mlAttr.isVirtual()) {
                continue;
            }
            if (mlAttr.getMlClass().getEntityName().equals(mlClass.getEntityName()) && mlAttr.getFieldType().equals(AttrType.MANY_TO_ONE)) {
                log.debug(String.format("Initialize MANY_TO_ONE Attr [%s]", mlAttr.getEntityFieldName()));
                OneToOneMapping mapping = dynamicTypeBuilder.addOneToOneMapping(mlAttr.getEntityFieldName(),
                        initializedTypeBuilderMap.get(mlAttr.getLinkClass().getEntityName()) == null ?
                                helper.getType(mlAttr.getLinkClass().getEntityName()) :
                                initializedTypeBuilderMap.get(mlAttr.getLinkClass().getEntityName()).getType(),
                        surroundDelimiter(mlAttr.getTableFieldName()));
                mapping.setIsLazy(mlAttr.getLazy() != null ? mlAttr.getLazy() : false);
                setFetchType(mlAttr,mapping);
            }
        }
    }

    private void initializeManyToManyAttr(MlClass mlClass, JPADynamicHelper helper) {
        JPADynamicTypeBuilder dynamicTypeBuilder = initializedTypeBuilderMap.get(mlClass.getEntityName());
        for (MlAttr mlAttr : mlClass.getAttrSet()) {
            if (!mlAttr.getMlClass().getEntityName().equals(mlClass.getEntityName())
                    || mlAttr.isVirtual()) {
                //атрибут родительского класса не инициализируем (он родительского так проинициализирован)
                //либо атрибут виртуальный
                continue;
            }
            if (mlAttr.getFieldType().equals(AttrType.MANY_TO_MANY)) {
                log.debug(String.format("Initialize MANY_TO_MANY Attr [%s]", mlAttr.getEntityFieldName()));

                String relationTableName;
                if (mlAttr.getManyToManyTableName() != null && !mlAttr.getManyToManyTableName().equals("")) {
                    relationTableName = surroundDelimiter(mlAttr.getManyToManyTableName());
                } else {
                    relationTableName = surroundDelimiter(getMNTableName(mlAttr.getMlClass().getTableName(), mlAttr.getLinkClass().getTableName()));
                }
                ManyToManyMapping mapping = addManyToManyMapping(dynamicTypeBuilder, mlAttr.getEntityFieldName(),
                        initializedTypeBuilderMap.get(mlAttr.getLinkClass().getEntityName()) == null ?
                                helper.getType(mlAttr.getLinkClass().getEntityName()) :
                                initializedTypeBuilderMap.get(mlAttr.getLinkClass().getEntityName()).getType(),
                        relationTableName);
                if (mlAttr.getManyToManyFieldNameM() != null && !mlAttr.getManyToManyFieldNameM().equals("")) {
                    mapping.getSourceRelationKeyFields().clear();
                    mapping.setSourceRelationKeyFieldName(surroundDelimiter(mlAttr.getManyToManyFieldNameM()));
                }
                if (mlAttr.getManyToManyFieldNameN() != null && !mlAttr.getManyToManyFieldNameN().equals("")) {
                    mapping.getTargetRelationKeyFields().clear();
                    mapping.setTargetRelationKeyFieldName(surroundDelimiter(mlAttr.getManyToManyFieldNameN()));
                }

                dynamicTypeBuilder.addMapping(mapping);
                mapping.setIsLazy(mlAttr.getLazy() != null ? mlAttr.getLazy() : false);
                setFetchType(mlAttr,mapping);
                //Если список упорядоченный, добавляем упорядочивание
                if (mlAttr.isOrdered()) {
                    String orderColumn = getMNOrderColumn(mlAttr.getLinkClass().getTableName());
                    mapping.setListOrderFieldName(surroundDelimiter(orderColumn));
                    //закомментил потому что выбрасовалось исключение при инициализации существующих связий
                    //mapping.setOrderCorrectionType(OrderCorrectionType.EXCEPTION);
                }
            }
        }
    }

    public ManyToManyMapping addManyToManyMapping(JPADynamicTypeBuilder dynamicTypeBuilder, String name, DynamicType refType, String relationshipTableName) {
        ManyToManyMapping mapping = new ManyToManyMapping();
        mapping.setAttributeName(name);
        mapping.setReferenceClass(refType.getJavaClass());
        mapping.setRelationTableName(relationshipTableName);
        //TODO разобраться с построением отношений
        for (DatabaseField sourcePK : dynamicTypeBuilder.getType().getDescriptor().getPrimaryKeyFields()) {
            String relField = sourcePK.getName();
            relField = dynamicTypeBuilder.getType().getDescriptor().getTableName() + "_" + relField;
            mapping.addSourceRelationKeyFieldName(surroundDelimiter(relField.replaceAll("\"", "")), sourcePK.getQualifiedName());
        }
        for (DatabaseField targetPK : refType.getDescriptor().getPrimaryKeyFields()) {
            String relField = targetPK.getName();
            relField = refType.getDescriptor().getTableName() + "_" + relField;
            mapping.addTargetRelationKeyFieldName(surroundDelimiter(relField.replaceAll("\"", "")), targetPK.getQualifiedName());
        }
        mapping.useTransparentList();

        return mapping;
        //addMapping(mapping);
    }


    private void initializeSimpleAttr(MlClass mlClass, String parentClassPath) {
        JPADynamicTypeBuilder dynamicTypeBuilder = initializedTypeBuilderMap.get(mlClass.getEntityName());
        if (mlClass.getParent() != null) {
            log.debug(String.format("Parent detected Set PK for attr [%s]", getRootParent(mlClass).getPrimaryKeyAttr().getEntityFieldName()));
            dynamicTypeBuilder.setPrimaryKeyFields(surroundDelimiter(getRootParent(mlClass).getPrimaryKeyAttr().getEntityFieldName()));
        }
        for (MlAttr mlAttr : mlClass.getAttrSet()) {
            log.debug(String.format("Try Initialize Simple Attr [%s] for class [%s]", mlAttr.getEntityFieldName(), mlClass.getEntityName()));
            if (!mlAttr.getMlClass().getEntityName().equals(mlClass.getEntityName())
                    || mlAttr.isVirtual()) {
                log.debug(String.format("[%s] for class [%s] is not simple, skip it", mlAttr.getEntityFieldName(), mlClass.getEntityName()));
                //атрибут родительского класса не инициализируем (он родительского так проинициализирован)
                //не инициализируем виртуальный атрибут
                continue;
            }

            if (mlAttr.getFieldType().equals(AttrType.BOOLEAN) ||
                    mlAttr.getFieldType().equals(AttrType.DATE) ||
                    mlAttr.getFieldType().equals(AttrType.STRING) ||
                    mlAttr.getFieldType().equals(AttrType.TEXT) ||
                    mlAttr.getFieldType().equals(AttrType.ENUM) ||
                    mlAttr.getFieldType().equals(AttrType.DOUBLE) ||
                    mlAttr.getFieldType().equals(AttrType.FILE) ||
                    mlAttr.getFieldType().equals(AttrType.LONG)) {

                log.debug(String.format("Initialize MlAttr [%s]", mlAttr.getEntityFieldName()));

                if (mlAttr.getPrimaryKey() != null && mlAttr.getPrimaryKey()) {
                    log.debug(String.format("Set PK for attr [%s]", mlAttr.getEntityFieldName()));
                    dynamicTypeBuilder.setPrimaryKeyFields(surroundDelimiter(mlAttr.getEntityFieldName()));
                }

                if (mlAttr.getAutoIncrement() != null && mlAttr.getAutoIncrement() && mlClass.getParent() == null) {
                    // Для классоа с родителем не нужно создавать автоинкрементную сиквенс она берется у предка
                    log.debug(String.format("Initialize Sequence for attr [%s]", mlAttr.getEntityFieldName()));
                    // TODO rafactor via platform
                    String sequenceName = surroundDelimiter(mlAttr.getMlClass().getTableName() + "_" + mlAttr.getTableFieldName() + DDLGenerator.SEQUENCE_POSTFIX);

                    JPADynamicHelper helper = new JPADynamicHelper(emf);
                    Map sequencesMap = new HashMap();
                    if (helper.getSession().getProject().getLogin() != null
                            && helper.getSession().getProject().getLogin().getSequences() != null) {
                        sequencesMap = helper.getSession().getProject().getLogin().getSequences();
                    } else {
                        helper.getSession().getProject().getLogin().setSequences(sequencesMap);
                    }

                    Sequence sequence = new NativeSequence(sequenceName, 1, false);
                    if (!sequencesMap.containsKey(sequenceName)) {
                        sequencesMap.put(sequenceName, sequence);
                    } else {
                        sequence = (Sequence) sequencesMap.get(sequenceName);
                    }

                    dynamicTypeBuilder.configureSequencing(sequence, sequenceName, surroundDelimiter(mlAttr.getTableFieldName()));
                    log.debug(String.format("Sequence name = [%s]", sequenceName));
                }
                switch (mlAttr.getFieldType()) {
                    case TEXT:
                    case STRING:
                        dynamicTypeBuilder.addDirectMapping(mlAttr.getEntityFieldName(), String.class, parentClassPath + "." + surroundDelimiter(mlAttr.getTableFieldName()));
                        break;
                    case LONG:
                        dynamicTypeBuilder.addDirectMapping(mlAttr.getEntityFieldName(), Long.class, parentClassPath + "." + surroundDelimiter(mlAttr.getTableFieldName()));
                        break;
                    case BOOLEAN:
                        dynamicTypeBuilder.addDirectMapping(mlAttr.getEntityFieldName(), Boolean.class, parentClassPath + "." + surroundDelimiter(mlAttr.getTableFieldName()));
                        break;
                    case DATE:
                        dynamicTypeBuilder.addDirectMapping(mlAttr.getEntityFieldName(), Date.class, parentClassPath + "." + surroundDelimiter(mlAttr.getTableFieldName()));
                        break;
                    case ENUM:
                        dynamicTypeBuilder.addDirectMapping(mlAttr.getEntityFieldName(), String.class, parentClassPath + "." + surroundDelimiter(mlAttr.getTableFieldName()));
                        break;
                    case DOUBLE:
                        dynamicTypeBuilder.addDirectMapping(mlAttr.getEntityFieldName(), Double.class, parentClassPath + "." + surroundDelimiter(mlAttr.getTableFieldName()));
                        break;
                    case FILE:

                        dynamicTypeBuilder.addDirectMapping(mlAttr.getEntityFieldName(),byte[].class, parentClassPath + "." + surroundDelimiter(mlAttr.getTableFieldName()))
                                .setIsLazy(true);
                        dynamicTypeBuilder.addDirectMapping(mlAttr.getEntityFieldName() + "_filename", String.class, parentClassPath + "." + surroundDelimiter(mlAttr.getTableFieldName() + "_filename"));
                        break;
                }


            }
        }
    }

    private String surroundDelimiter(String original) {
        JPADynamicHelper helper = new JPADynamicHelper(emf);
        return helper.getSession().getPlatform().getStartDelimiter() + original + helper.getSession().getPlatform().getEndDelimiter();
    }

    private String getMNTableName(String sourсeTable, String destTable) {
        if (sourсeTable.compareTo(destTable) > 0) {
            return "MN_" + destTable + "_" + sourсeTable;
        } else {
            return "MN_" + sourсeTable + "_" + destTable;
        }
    }

    private String getMNOrderColumn(String tableName) {
        return tableName + "_order";
    }

    private List<String> getParentTables(MlClass mlClass) {
        List<String> result = new ArrayList<>();
        result.add(surroundDelimiter(mlClass.getTableName()));
        MlClass tmpClass = mlClass.getParent();
        while (tmpClass != null) {
            if (mlClass.getParent().equals(mlClass)) {
                log.error("Circular parent link for Class " + mlClass.getEntityName());
                throw new RuntimeException("Circular parent link for Class " + mlClass.getEntityName());
            }
            result.add(surroundDelimiter(tmpClass.getTableName()));
            tmpClass = tmpClass.getParent();
        }
        return result;
    }


    private void initializeParentAttrList(MlClass mlClass) {
        MlClass tmpClass = mlClass.getParent();
        while (tmpClass != null) {
            for (MlAttr mlAttr : tmpClass.getAttrSet()) {
                //TODO подумать на тему первичных ключей
                overrideAttr(mlClass, mlAttr);
                if (/*!mlAttr.getPrimaryKey() &&*/ !mlClassHasAttrWithSameEntityFieldName(mlClass, mlAttr)) {
                    log.debug(String.format("Add attr [%s] to Class [%s] from it parentClass [%s]",
                            mlAttr.getEntityFieldName(), mlClass.getEntityName(), mlAttr.getMlClass().getEntityName()));
                    mlClass.getAttrSet().add(mlAttr);
                }
            }
            tmpClass = tmpClass.getParent();
        }
    }

    private void overrideAttr(MlClass mlClass, MlAttr mlAttr) {
        for (MlAttr tmpMlAttr : mlClass.getAttrSet()) {
            if (tmpMlAttr.getEntityFieldName().equals(mlAttr.getEntityFieldName())) {
                if (tmpMlAttr.isVirtual()) {
                    tmpMlAttr.setOverridedAttr(mlAttr);
                }
            }
        }
    }

    private boolean mlClassHasAttrWithSameEntityFieldName(MlClass mlClass, MlAttr MlAttr) {
        for (MlAttr tmpMlAttr : mlClass.getAttrSet()) {
            if (tmpMlAttr.getEntityFieldName().equals(MlAttr.getEntityFieldName())) {
                return true;
            }
        }
        return false;
    }

    private void setFetchType(MlAttr attr, ForeignReferenceMapping mapping){
        switch (attr.getFetchType()){
            case OUTER_JOIN:
                mapping.useOuterJoinFetch();
                break;
            case INNER_JOIN:
                mapping.useInnerJoinFetch();
                break;
            case BATCH_JOIN:
                mapping.setBatchFetchType(BatchFetchType.JOIN);
                break;
            case BATCH_EXISTS:
                mapping.setBatchFetchType(BatchFetchType.EXISTS);
                break;
            case BATCH_IN:
                mapping.setBatchFetchType(BatchFetchType.IN);
                break;
            case NONE:
                break;
        }

    }
}
