package ru.peak.ml.core.initializer.jpa.impl;

import org.eclipse.persistence.descriptors.ClassDescriptor;
import org.eclipse.persistence.dynamic.DynamicEntity;
import org.eclipse.persistence.expressions.Expression;
import org.eclipse.persistence.expressions.ExpressionBuilder;
import org.eclipse.persistence.history.HistoryPolicy;
import org.eclipse.persistence.internal.expressions.SQLInsertStatement;
import org.eclipse.persistence.internal.expressions.SQLUpdateStatement;
import org.eclipse.persistence.internal.helper.ClassConstants;
import org.eclipse.persistence.internal.helper.DatabaseField;
import org.eclipse.persistence.internal.helper.DatabaseTable;
import org.eclipse.persistence.internal.queries.StatementQueryMechanism;
import org.eclipse.persistence.internal.sessions.AbstractRecord;
import org.eclipse.persistence.internal.sessions.AbstractSession;
import org.eclipse.persistence.mappings.DatabaseMapping;
import org.eclipse.persistence.queries.DeleteAllQuery;
import org.eclipse.persistence.queries.ModifyQuery;
import org.eclipse.persistence.queries.ObjectLevelModifyQuery;
import org.eclipse.persistence.sessions.DatabaseRecord;
import org.eclipse.persistence.sessions.changesets.ChangeRecord;
import org.eclipse.persistence.sessions.changesets.CollectionChangeRecord;
import org.eclipse.persistence.sessions.changesets.ObjectChangeSet;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.ml.core.dao.CommonDao;

import javax.servlet.http.HttpServletRequest;

/**
 * Политика ведения истории для обхектов(где включена галка вести историю)
 */
public class MlHistoryPolicy extends HistoryPolicy {

    private String userField;
    private String actionType;

    public String getUserField() {
        return userField;
    }

    public void setUserField(String userField) {
        this.userField = userField;
    }

    @Override
    public void logicalInsert(ObjectLevelModifyQuery writeQuery, boolean isUpdate) {
        ClassDescriptor descriptor = getDescriptor();
        AbstractRecord modifyRow = null;
        AbstractRecord originalModifyRow = writeQuery.getModifyRow();
        Object currentTime = null;
        if (isUpdate) {
            modifyRow = descriptor.getObjectBuilder().buildRow(writeQuery.getObject(), writeQuery.getSession(), DatabaseMapping.WriteType.UPDATE); // Bug 319276
            // If anyone added items to the modify row, then they should also be added here.
            modifyRow.putAll(originalModifyRow);
        } else {
            modifyRow = originalModifyRow;
            // If update would have already discovered timestamp to use.
            currentTime = getCurrentTime(writeQuery.getSession());
        }
        StatementQueryMechanism insertMechanism = new StatementQueryMechanism(writeQuery);

        insertManyToMany(modifyRow, writeQuery);

        for (int i = 0; i < getHistoricalTables().size(); i++) {
            DatabaseTable table = getHistoricalTables().get(i);
            if (isUpdate && !checkWastedVersioning(modifyRow, table)) {
                continue;
            }
            if (!isUpdate) {
                modifyRow.add(getStart(i), currentTime);
            }
            insertActionType(modifyRow, isUpdate);
            insertUser(modifyRow);

            SQLInsertStatement insertStatement = new SQLInsertStatement();
            insertStatement.setTable(table);
            insertMechanism.getSQLStatements().add(insertStatement);
        }
        if (insertMechanism.hasMultipleStatements()) {
            writeQuery.setTranslationRow(modifyRow);
            writeQuery.setModifyRow(modifyRow);
            insertMechanism.insertObject();
        }
    }

    @Override
    public void logicalDelete(ModifyQuery writeQuery, boolean isUpdate, boolean isShallow) {
        ClassDescriptor descriptor = writeQuery.getDescriptor();
        AbstractRecord originalModifyRow = writeQuery.getModifyRow();
        AbstractRecord modifyRow = new DatabaseRecord();
        StatementQueryMechanism updateMechanism = new StatementQueryMechanism(writeQuery);
        Object currentTime = getCurrentTime(writeQuery.getSession());
        if (writeQuery instanceof ObjectLevelModifyQuery) {
            insertManyToMany(modifyRow, (ObjectLevelModifyQuery) writeQuery);
        }
        for (int i = 0; i < getHistoricalTables().size(); i++) {
            DatabaseTable table = getHistoricalTables().get(i);

            if (isUpdate && !(checkWastedVersioning(originalModifyRow, table) || checkWastedVersioning(modifyRow, table))) {
                continue;
            }
            SQLUpdateStatement updateStatement = new SQLUpdateStatement();
            updateStatement.setTable(table);
            Expression whereClause = null;
            if (writeQuery instanceof DeleteAllQuery) {
                if (writeQuery.getSelectionCriteria() != null) {
                    whereClause = (Expression) writeQuery.getSelectionCriteria().clone();
                }
            } else {
                whereClause = descriptor.getObjectBuilder().buildPrimaryKeyExpression(table);
            }
            ExpressionBuilder builder = ((whereClause == null) ? new ExpressionBuilder() : whereClause.getBuilder());
            whereClause = builder.getField(getEnd(i)).isNull().and(whereClause);
            updateStatement.setWhereClause(whereClause);

            modifyRow = new DatabaseRecord();
            modifyRow.add(getEnd(i), currentTime);

            // save a little time here and add the same timestamp value for
            // the start field in the logicalInsert.
            if (isUpdate) {
                if (isShallow) {
                    // Bug 319276 - increment the timestamp by 1 to avoid unique constraint violation potential
                    java.sql.Timestamp incrementedTime = (java.sql.Timestamp) currentTime;
                    incrementedTime.setTime(incrementedTime.getTime() + getMinimumTimeIncrement(writeQuery.getSession()));
                    originalModifyRow.add(getStart(i), incrementedTime);
                } else {
                    originalModifyRow.add(getStart(i), currentTime);
                }
            }
            updateMechanism.getSQLStatements().add(updateStatement);
        }
        if (updateMechanism.hasMultipleStatements()) {
            writeQuery.setModifyRow(modifyRow);
            updateMechanism.updateObject();
            writeQuery.setModifyRow(originalModifyRow);
        }
    }

    private void insertManyToMany(AbstractRecord modifyRow, ObjectLevelModifyQuery writeQuery) {
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        if (writeQuery.getObjectChangeSet() != null) {
            for (ChangeRecord changeRecord : writeQuery.getObjectChangeSet().getChanges()) {
                if (changeRecord instanceof CollectionChangeRecord) {
                    CollectionChangeRecord collectionChangeRecord = (CollectionChangeRecord) changeRecord;
                    StringBuilder added = new StringBuilder();
                    StringBuilder deleted = new StringBuilder();
                    for (Object key : collectionChangeRecord.getAddObjectList().keySet()) {
                        System.out.println(collectionChangeRecord.getAddObjectList().get(key));
                        if (collectionChangeRecord.getAddObjectList().get(key) instanceof ObjectChangeSet) {
                            ObjectChangeSet objectChangeSet = (ObjectChangeSet) collectionChangeRecord.getAddObjectList().get(key);
                            DynamicEntity dynamicEntity = commonDao.findById(objectChangeSet.getId(), objectChangeSet.getClassType(writeQuery.getSession()));
                            MlDynamicEntityImpl mlDynamicEntity = (MlDynamicEntityImpl) dynamicEntity;
                            added.append(mlDynamicEntity.getTitle()).append(", ");
                        }
                    }
                    for (Object key : collectionChangeRecord.getRemoveObjectList().keySet()) {
                        System.out.println(collectionChangeRecord.getRemoveObjectList().get(key));
                        if (collectionChangeRecord.getRemoveObjectList().get(key) instanceof ObjectChangeSet) {
                            ObjectChangeSet objectChangeSet = (ObjectChangeSet) collectionChangeRecord.getRemoveObjectList().get(key);
                            DynamicEntity dynamicEntity = commonDao.findById(objectChangeSet.getId(), objectChangeSet.getClassType(writeQuery.getSession()));
                            MlDynamicEntityImpl mlDynamicEntity = (MlDynamicEntityImpl) dynamicEntity;
                            deleted.append(mlDynamicEntity.getTitle()).append(", ");
                        }
                    }
                    DatabaseField editField = new DatabaseField(surroundDelimiter(collectionChangeRecord.getAttribute(), writeQuery.getSession()));
                    editField.setType(ClassConstants.STRING);
                    String historyString = "";
                    if (added.length() > 0) {
                        historyString = "Добавлено: " + added.substring(0, added.length() - 2) + "\r\n";
                    }
                    if (deleted.length() > 0) {
                        historyString = historyString + "Удалено: " + deleted.substring(0, deleted.length() - 2) + "\r\n";
                    }
                    modifyRow.add(editField, historyString);

                }
            }
        }

    }

    private String surroundDelimiter(String attribute, AbstractSession session) {
        return session.getPlatform().getStartDelimiter() + attribute + session.getPlatform().getEndDelimiter();
    }

    private void insertActionType(AbstractRecord modifyRow, boolean isUpdate) {
        DatabaseField actionType = new DatabaseField(this.actionType);
        actionType.setType(ClassConstants.STRING);
        if (isUpdate) {
            modifyRow.add(actionType, "Редактирование");
        } else {
            modifyRow.add(actionType, "Создание");
        }
    }

    private void insertUser(AbstractRecord modifyRow) {
        DatabaseField userField = new DatabaseField(this.userField);
        userField.setType(ClassConstants.STRING);
        HttpServletRequest request = GuiceConfigSingleton.inject(HttpServletRequest.class);
        if (request.getSession().getAttribute("user") != null) {
            modifyRow.add(userField, ((MlUser) request.getSession().getAttribute("user")).getLogin());
        } else {
            modifyRow.add(userField, "system");
        }
    }


    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getActionType() {
        return actionType;
    }
}
