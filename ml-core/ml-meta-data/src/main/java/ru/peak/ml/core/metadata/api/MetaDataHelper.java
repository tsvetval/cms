package ru.peak.ml.core.metadata.api;

import org.eclipse.persistence.dynamic.DynamicEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.holders.impl.MetaDataHolderImpl;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;

import java.util.List;

/**
 * Хелпер для доступа к мета-данным
 * use MetaDataHolder via GUICE
 */
@Deprecated
public class MetaDataHelper {
    private static final Logger log = LoggerFactory.getLogger(MetaDataHelper.class);

/*
    public static Boolean hasMLUID(String mlClassName) {
        MlClass MlClass = getMlClassByName(mlClassName);
        if (getMlAttr(MlClass, MlAttr.GUID) != null &&
                getMlAttr(MlClass, MlAttr.ID) != null &&
                getMlAttr(MlClass, MlAttr.LAST_CHANGE) != null) {
            return true;
        } else {
            return false;
        }
    }
*/

    /**
     * Возвращает MlClass для заданного объекта
     *
     * @param object переданный DynamicEntity
     * @return MlClass
     */
    public static MlClass getMlClassByEntity(DynamicEntity object) {
        return GuiceConfigSingleton.inject(MetaDataHolder.class).getMlClassByEntityDynamicClass(object.getClass());
    }

    public static MlClass getMlClassByEntity(Object object) {
        return GuiceConfigSingleton.inject(MetaDataHolder.class).getMlClassByEntityDynamicClass(object.getClass());
    }

    /**
     * Возвращает Java класс для заданного кодового имени MlClass.entityName
     *
     * @param name
     * @return
     */
    public static Class getEntityClassByName(String name) {
        //TODO
        return GuiceConfigSingleton.inject(MetaDataHolder.class).getEntityClassByName(name);
    }


    /**
     * Возвращает объект MLClass по имени энтити
     *
     * @param name Кодовое имя
     * @return MLClass
     */
    public static MlClass getMlClassByName(String name) {
        return GuiceConfigSingleton.inject(MetaDataHolder.class).getMlClassByName(name);
    }

    public static MlClass getMlClassById(Long id) {
        //TODO
        return GuiceConfigSingleton.inject(MetaDataHolder.class).getMlClassById(id);
    }

    public static MlAttr getAttrByName(String mlClassName, String entityFieldName) {
        MlClass mlClass = getMlClassByName(mlClassName);
        return GuiceConfigSingleton.inject(MetaDataHolder.class).getAttr(mlClass, entityFieldName);
    }

    public static MlAttr getMlAttrById(Long id) {
        return GuiceConfigSingleton.inject(MetaDataHolder.class).getAttrById(id);
    }


    public static <T extends DynamicEntity> Class<T> getClassByMlClass(String mlClassName) {
        return GuiceConfigSingleton.inject(MetaDataHolder.class).getEntityClassByName(mlClassName);
    }


    /**
     * Возвращает список атрибутов MlClass'а по имени класса
     *
     * @param mlClassName
     * @return
     */
    public static List<MlAttr> getMlClassAttributes(String mlClassName) {
        MlClass MlClass = getMlClassByName(mlClassName);
        return getMlClassAttributes(MlClass);
    }

    /**
     * Возвращает список атрибутов MlClass'а
     *
     * @param MlClass
     * @return
     */
    public static List<MlAttr> getMlClassAttributes(MlClass MlClass) {
        return (List<MlAttr>) MlClass.getAttrSet();
    }

    public static MlAttr getMlAttr(MlClass MlClass, String mlAttr) {
        return GuiceConfigSingleton.inject(MetaDataHolder.class).getAttr(MlClass, mlAttr);
    }

    public static List<MlAttr> getPrimaryKeyAttrList(MlClass MlClass) {
        return GuiceConfigSingleton.inject(MetaDataHolder.class).getPrimaryKeyAttrList(MlClass);
    }

    public static List<MlAttr> getInListAttrList(MlClass MlClass) {
        return MlClass.getInListAttrList();

    }


    public static MlClass getMlClassByEntityDynamicClass(Class clazz) {
        return GuiceConfigSingleton.inject(MetaDataHolder.class).getMlClassByEntityDynamicClass(clazz);
    }
}
