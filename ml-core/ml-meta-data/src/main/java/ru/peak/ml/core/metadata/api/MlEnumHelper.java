package ru.peak.ml.core.metadata.api;

import org.eclipse.persistence.internal.dynamic.DynamicEntityImpl;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.holders.EnumHolder;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.holders.impl.EnumHolderImpl;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.model.system.MlEnum;

import java.util.Collection;

/**
 *  Хелпер может использоваться в шаблонах
 *  use EnumHolder via GUICE
 */
@Deprecated
public class MlEnumHelper {
    public static Collection<MlEnum> getEnumCollection(MlAttr MlAttr) {
        return GuiceConfigSingleton.inject(EnumHolder.class).getEnumList(MlAttr);
    }

    public static Collection<MlEnum> getEnumCollection(DynamicEntityImpl mlAttr) {
        return GuiceConfigSingleton.inject(EnumHolder.class).getEnumList((MlAttr)mlAttr);
    }

    public static MlEnum getEnum(MlAttr MlAttr, String enumCode) {
        return GuiceConfigSingleton.inject(EnumHolder.class).getEnum(MlAttr, enumCode);
    }

    public static MlEnum getEnum(DynamicEntityImpl mlAttr, String enumCode) {
        return GuiceConfigSingleton.inject(EnumHolder.class).getEnum((MlAttr)mlAttr, enumCode);
    }

    public static MlEnum getEnum(String mlClass, String mlAttr, String enumCode){
        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
        MlClass iMlClass = metaDataHolder.getMlClassByName(mlClass);
        MlAttr MlAttr = metaDataHolder.getAttr(iMlClass,mlAttr);
        return getEnum(MlAttr,enumCode);
    }
}