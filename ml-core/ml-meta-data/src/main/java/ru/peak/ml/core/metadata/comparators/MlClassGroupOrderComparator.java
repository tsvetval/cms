package ru.peak.ml.core.metadata.comparators;


import ru.peak.ml.core.model.system.MlAttrGroup;

import java.util.Comparator;

/**
 * Сортирует группы по порядку идентификаторов
 */
public class MlClassGroupOrderComparator implements Comparator<MlAttrGroup> {
    @Override
    public int compare(MlAttrGroup o1, MlAttrGroup o2) {
        return o1.getId().compareTo(o2.getId());
    }
}
