package ru.peak.ml.core.metadata.comparators;

import ru.peak.ml.core.model.system.MlEnum;

import java.util.Comparator;

public class MlEnumOrderComparator implements Comparator<MlEnum> {
    @Override
    public int compare(MlEnum o1, MlEnum o2) {
        if (o1.getOrder() == null && o2.getOrder() == null) return 0;
        else if (o1.getOrder() == null && o2.getOrder() != null) return -1;
        else if (o1.getOrder() != null && o2.getOrder() == null) return 1;
        else return o1.getOrder().compareTo(o2.getOrder());
    }

}
