package ru.peak.ml.core.metadata.comparators;

import ru.peak.ml.core.model.system.MlAttr;

import java.util.Comparator;

/**
 *
 */
public class MlInListAttrsOrderComparator implements Comparator<MlAttr> {
    @Override
    public int compare(MlAttr o1, MlAttr o2) {
        if (o1.getViewPos() == null && o2.getViewPos() == null) return 0;
        else if (o1.getViewPos() == null && o2.getViewPos() != null) return -1;
        else if (o1.getViewPos() != null && o2.getViewPos() == null) return 1;
        else return o1.getViewPos().compareTo(o2.getViewPos());
    }
}
