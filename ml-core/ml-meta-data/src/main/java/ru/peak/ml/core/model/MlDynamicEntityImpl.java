package ru.peak.ml.core.model;

import org.eclipse.persistence.descriptors.ClassDescriptor;
import org.eclipse.persistence.exceptions.DynamicException;
import org.eclipse.persistence.indirection.IndirectContainer;
import org.eclipse.persistence.indirection.IndirectList;
import org.eclipse.persistence.indirection.ValueHolderInterface;
import org.eclipse.persistence.internal.dynamic.DynamicEntityImpl;
import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import org.eclipse.persistence.mappings.DatabaseMapping;
import ru.ml.core.common.AttrType;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.holders.EnumHolder;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.model.system.MlEnum;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 */
public abstract class MlDynamicEntityImpl extends DynamicEntityImpl {
    protected Map<String, Object> virtualPropertiesMap = new HashMap<>();

    public MlClass getInstanceMlClass() {
        return GuiceConfigSingleton.inject(MetaDataHolder.class).getMlClassByEntityDynamicClass(this.getClass());
    }


    @Override
    public <T> T get(String propertyName) throws DynamicException {
        T result;
        if (isVirtual(propertyName)) {
            result = getVirtual(propertyName);
        } else {

    // Скопировано с DynamicEntityImpl из за неправильной работы с fetch группами
    // при гете любого атрибута, инстанциируется весь объект

            DynamicPropertiesManager dpm = fetchPropertiesManager();
            if (dpm.contains(propertyName)) {
                if (_persistence_getFetchGroup() != null && !_persistence_getFetchGroup().containsAttribute(propertyName)) {
                    String errorMsg = _persistence_getFetchGroup().onUnfetchedAttribute(this,
                            propertyName);
                    if (errorMsg != null) {
                        throw DynamicException.invalidPropertyName(dpm.getType(), propertyName);
                    }
                }
                PropertyWrapper wrapper = propertiesMap.get(propertyName);
                if (wrapper == null) { // properties can be added after constructor is called
                    wrapper = new PropertyWrapper();
                    propertiesMap.put(propertyName, wrapper);
                }
                Object value = wrapper.getValue();
                // trigger any indirection
                if (value instanceof ValueHolderInterface) {
                    value = ((ValueHolderInterface) value).getValue();
                } else if (value instanceof IndirectContainer) {
                    value = ((IndirectContainer) value).getValueHolder().getValue();
                }
                try {
                    return (T) value;
                } catch (ClassCastException cce) {
                    ClassDescriptor descriptor = getType().getDescriptor();
                    DatabaseMapping dm = null;
                    if (descriptor != null) {
                        dm = descriptor.getMappingForAttributeName(propertyName);
                    } else {
                        dm = new UnknownMapping(propertyName);
                    }
                    throw DynamicException.invalidGetPropertyType(dm, cce);
                }
            } else {
                throw DynamicException.invalidPropertyName(dpm.getType(), propertyName);
            }
        }
        return result;
    }

    /**
     * Возвращает значение первичного ключа в виде строки
     *
     * @return Возвращает значение первичного ключа в виде строки
     */
    public String getOUID() {
        StringBuilder result = new StringBuilder("");
        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
        List<MlAttr> pkAttrList = metaDataHolder.getPrimaryKeyAttrList(metaDataHolder.getMlClassByEntityDynamicClass(this.getClass()));
        // TODO sort list ???
        for (MlAttr attr : pkAttrList) {
            if (result.length() > 0) {
                result.append("#");
            }
            if(this.get(attr.getEntityFieldName())!=null){
                result.append(this.get(attr.getEntityFieldName()).toString());
            }else{
                result.append("null");
            }
        }
        return result.toString();
    }

    private static Pattern titlePattern = Pattern.compile("\\$\\{(.+?)\\}");

    public String getTitle() {
        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
        String title = metaDataHolder.getMlClassByEntityDynamicClass(this.getClass()).getTitleFormat();

        if (title == null) {
            title = "";
        }
        try {
            List<String> attrNameList = new ArrayList<>();
            Matcher m = titlePattern.matcher(title);
            while (m.find()) {
                attrNameList.add(m.group().substring(2, m.group().length() - 1));
            }
            for (String attrName : attrNameList) {
                String attrStub = String.format("\\$\\{%s\\}", attrName);
                String attrFormat = "";
                if (attrName.contains(",")) {
                    attrFormat = attrName.substring(attrName.indexOf(",") + 1).trim();
                    attrName = attrName.substring(0, attrName.indexOf(","));
                }
                Object attrValue = this.get(attrName);
                if (attrValue != null) {
                    MlAttr mlAttr = metaDataHolder.getAttr(this.getInstanceMlClass().getEntityName(), attrName);
                    if (mlAttr.getFieldType().equals(AttrType.MANY_TO_ONE)) {//заголовок - ссылка на другой класс
                        title = title.replaceAll(attrStub, ((MlDynamicEntityImpl) attrValue).getTitle()); // рекурсивно получаем тайтл этого другого класса
                    } else if (mlAttr.getFieldType().equals(AttrType.DATE)) { // заголовок - дата
                        Date dateValue = (Date) attrValue;
                        if(mlAttr.getFormat()!=null){
                            attrFormat = mlAttr.getFormat();
                        }else{
                            attrFormat = "dd.MM.YYYY HH:mm";
                        }
                        DateFormat dateFormat = new SimpleDateFormat(attrFormat);
                        title = title.replaceAll(attrStub, dateFormat.format(dateValue));
                    } else if (mlAttr.getFieldType().equals(AttrType.ENUM)) {// заголовок - енум
                        MlEnum mlEnum = GuiceConfigSingleton.inject(EnumHolder.class).getEnum(mlAttr, attrValue.toString());
                        title = title.replaceAll(attrStub, mlEnum.getTitle());
                    } else if (mlAttr.getFieldType().equals(AttrType.MANY_TO_MANY)) { // Заголовок - M-N (Кто бы до такого додумался...)
                        IndirectList vector = (IndirectList) attrValue;
                        StringBuilder sb = new StringBuilder();
                        for (Object o : vector) {
                            sb.append(((MlDynamicEntityImpl) o).getTitle());// рекурсивно получаем тайтлы всех связанных объектов
                            sb.append(", ");
                        }
                        String titlePart = sb.toString();
                        titlePart = titlePart.substring(0, titlePart.length() - 2);
                        title = title.replaceAll(attrStub, titlePart);
                    } else { // другой заголовок
                        title = title.replaceAll(attrStub, attrValue.toString());
                    }
                } else {
                    title = title.replaceAll(attrStub, "");
                }
            }
        } catch (Exception e) {
            title = "<Формат заголовка задан неверно>";
        }
        if (title.isEmpty()) {
            return "без заголовка";
        } else return title;
    }

/*
    public void setOneToManyListValues(MlClass mlClass, String attrName, List<MlDynamicEntityImpl> newEntityList) {
        setOneToManyListValues(MetaDataHelper.getMlAttr(mlClass, attrName), newEntityList);
    }

    public void setOneToManyListValues(MlAttr IMlAttr, List<MlDynamicEntityImpl> newEntityList) {
        if (IMlAttr.getFieldType() != AttrType.ONE_TO_MANY) {
            throw new IllegalArgumentException(String.format("Wrong attr type: expected ONE_TO_MANY, but received %s", IMlAttr.getFieldType().name()));
        }
        Collection<MlDynamicEntityImpl> currentEntityList = new ArrayList<MlDynamicEntityImpl>((Collection) this.get(IMlAttr.getEntityFieldName()));
        // Удаляем все объекты не находящиеся в пришедшем списку
        for (MlDynamicEntityImpl tmpEntity : currentEntityList) {
            if (!newEntityList.contains(tmpEntity)) {
                removeOneToManyValue(IMlAttr, tmpEntity);
            }
        }
        // Добавляем все объекты не находящиеся в  текущем списке
        for (MlDynamicEntityImpl tmpEntity : newEntityList) {
            if (!currentEntityList.contains(tmpEntity)) {
                addOneToManyValue(IMlAttr, tmpEntity);
            }
        }

    }

    public void setManyToManyListValues(IMlClass IMlClass, String attrName, List<MlDynamicEntityImpl> newEntityList) {
        setManyToManyListValues(MetaDataHelper.getMlAttr(IMlClass, attrName), newEntityList);
    }

    public void setManyToManyListValues(IMlAttr IMlAttr, List<MlDynamicEntityImpl> newEntityList) {
        if (IMlAttr.getFieldType() != AttrType.MANY_TO_MANY) {
            throw new IllegalArgumentException(String.format("Wrong attr type: expected MANY_TO_MANY, but received %s", IMlAttr.getFieldType().name()));
        }
        Collection<MlDynamicEntityImpl> currentEntityList = new ArrayList<MlDynamicEntityImpl>((Collection) this.get(IMlAttr.getEntityFieldName()));
        // Удаляем все объекты не находящиеся в пришедшем списку
        for (MlDynamicEntityImpl tmpEntity : currentEntityList) {
            if (!newEntityList.contains(tmpEntity)) {
                removeManyToManyValue(IMlAttr, tmpEntity);
            }
        }
        // Добавляем все объекты не находящиеся в  текущем списке
        for (MlDynamicEntityImpl tmpEntity : newEntityList) {
            if (!currentEntityList.contains(tmpEntity)) {
                addManyToManyValue(IMlAttr, tmpEntity);
            }
        }
    }
*/

    /**
     * Добавляет элемент в коллекцию ONE_TO_MANY
     * mlAttr - атрибут типа ONE_TO_MANY к которому добавляется элемент
     * entity - добавляемый элемент
     * this - объект-владелец атрибута
     */
    public void addOneToManyValue(MlAttr IMlAttr, MlDynamicEntityImpl entity) {
        ((Collection) this.get(IMlAttr.getEntityFieldName())).add(entity);
        if (this.getClass().getSimpleName().equals("MlAttr") && this.get("fieldType").equals(AttrType.ENUM.toString())) {
            return;
        }
        entity.set(IMlAttr.getLinkAttr().getEntityFieldName(), this);
    }

/*
    public void addManyToManyValue(IMlAttr IMlAttr, MlDynamicEntityImpl entity) {
        ((Collection) this.get(IMlAttr.getEntityFieldName())).add(entity);
    }
*/

    /**
     * Удаляет элемент из коллекции ONE_TO_MANY
     * mlAttr - атрибут типа ONE_TO_MANY у которого удаляется элемент
     * entity - удаляемый элемент
     * this - объект-владелец атрибута
     */
    public void removeOneToManyValue(MlAttr IMlAttr, MlDynamicEntityImpl entity) {
        entity.set(IMlAttr.getLinkAttr().getEntityFieldName(), null);
        ((Collection) this.get(IMlAttr.getEntityFieldName())).remove(entity);
    }

/*
    public void removeManyToManyValue(IMlAttr IMlAttr, MlDynamicEntityImpl entity) {
        ((Collection) this.get(IMlAttr.getEntityFieldName())).remove(entity);
    }
*/

    private boolean isVirtual(String propertyName) {
        MlClass mlClass = getInstanceMlClass();
        if (mlClass == null) {
            // Актуально только для начальной инициализации кокда холдеры пустые
            return !fetchPropertiesManager().contains(propertyName);
        }
        // if null
        MlAttr mlAttr = GuiceConfigSingleton.inject(MetaDataHolder.class).getAttr(mlClass, propertyName);
        return mlAttr != null && mlAttr.isVirtual() && !mlAttr.isOverrided();
    }

    private <T> T getVirtual(String propertyName) {
        return (T) virtualPropertiesMap.get(propertyName);
    }

}
