package ru.peak.ml.core.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;

/**
 * Created by d_litovchenko on 16.03.15.
 */
public class Test extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }
}
