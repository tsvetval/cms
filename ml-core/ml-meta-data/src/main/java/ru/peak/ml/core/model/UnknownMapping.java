package ru.peak.ml.core.model;

import org.eclipse.persistence.internal.descriptors.DescriptorIterator;
import org.eclipse.persistence.internal.identitymaps.CacheKey;
import org.eclipse.persistence.internal.queries.JoinedAttributeManager;
import org.eclipse.persistence.internal.sessions.*;
import org.eclipse.persistence.mappings.DatabaseMapping;
import org.eclipse.persistence.queries.ObjectBuildingQuery;
import org.eclipse.persistence.queries.ObjectLevelReadQuery;
import org.eclipse.persistence.sessions.remote.DistributedSession;

import java.util.Map;

public class UnknownMapping extends DatabaseMapping {

    /**
     * Instantiates a new unknown mapping.
     *
     * @param propertyName the property name
     */
    public UnknownMapping(String propertyName) {
        setAttributeName(propertyName);
    }

    /* (non-Javadoc)
     * @see org.eclipse.persistence.mappings.DatabaseMapping#buildBackupClone(java.lang.Object, java.lang.Object, org.eclipse.persistence.internal.sessions.UnitOfWorkImpl)
     */
    public void buildBackupClone(Object clone, Object backup, UnitOfWorkImpl unitOfWork) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.persistence.mappings.DatabaseMapping#buildClone(java.lang.Object, org.eclipse.persistence.internal.identitymaps.CacheKey, java.lang.Object, java.lang.Integer, org.eclipse.persistence.internal.sessions.AbstractSession)
     */
    @Override
    public void buildClone(Object original, CacheKey cacheKey, Object clone, Integer refreshCascade, AbstractSession cloningSession) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.persistence.mappings.DatabaseMapping#buildCloneFromRow(org.eclipse.persistence.internal.sessions.AbstractRecord, org.eclipse.persistence.internal.queries.JoinedAttributeManager, java.lang.Object, org.eclipse.persistence.internal.identitymaps.CacheKey, org.eclipse.persistence.queries.ObjectBuildingQuery, org.eclipse.persistence.internal.sessions.UnitOfWorkImpl, org.eclipse.persistence.internal.sessions.AbstractSession)
     */
    public void buildCloneFromRow(AbstractRecord databaseRow,
                                  JoinedAttributeManager joinManager, Object clone, CacheKey sharedCacheKey, ObjectBuildingQuery sourceQuery,
                                  UnitOfWorkImpl unitOfWork, AbstractSession executionSession) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.persistence.mappings.DatabaseMapping#cascadePerformRemoveIfRequired(java.lang.Object, org.eclipse.persistence.internal.sessions.UnitOfWorkImpl, java.util.Map)
     */
    public void cascadePerformRemoveIfRequired(Object object, UnitOfWorkImpl uow,
                                               Map visitedObjects) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.persistence.mappings.DatabaseMapping#cascadeRegisterNewIfRequired(java.lang.Object, org.eclipse.persistence.internal.sessions.UnitOfWorkImpl, java.util.Map)
     */
    public void cascadeRegisterNewIfRequired(Object object, UnitOfWorkImpl uow,
                                             Map visitedObjects) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.persistence.mappings.DatabaseMapping#compareForChange(java.lang.Object, java.lang.Object, org.eclipse.persistence.internal.sessions.ObjectChangeSet, org.eclipse.persistence.internal.sessions.AbstractSession)
     */
    public ChangeRecord compareForChange(Object clone, Object backup, ObjectChangeSet owner,
                                         AbstractSession session) {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.persistence.mappings.DatabaseMapping#compareObjects(java.lang.Object, java.lang.Object, org.eclipse.persistence.internal.sessions.AbstractSession)
     */
    public boolean compareObjects(Object firstObject, Object secondObject,
                                  AbstractSession session) {
        return false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.persistence.mappings.DatabaseMapping#fixObjectReferences(java.lang.Object, java.util.Map, java.util.Map, org.eclipse.persistence.queries.ObjectLevelReadQuery, org.eclipse.persistence.sessions.remote.RemoteSession)
     */
    public void fixObjectReferences(Object object, Map objectDescriptors, Map processedObjects,
                                    ObjectLevelReadQuery query, DistributedSession session) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.persistence.mappings.DatabaseMapping#iterate(org.eclipse.persistence.internal.descriptors.DescriptorIterator)
     */
    public void iterate(DescriptorIterator iterator) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.persistence.mappings.DatabaseMapping#mergeChangesIntoObject(java.lang.Object, org.eclipse.persistence.internal.sessions.ChangeRecord, java.lang.Object, org.eclipse.persistence.internal.sessions.MergeManager, org.eclipse.persistence.internal.sessions.AbstractSession)
     */
    public void mergeChangesIntoObject(Object target, ChangeRecord changeRecord, Object source,
                                       MergeManager mergeManager, AbstractSession targetSession) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.persistence.mappings.DatabaseMapping#mergeIntoObject(java.lang.Object, boolean, java.lang.Object, org.eclipse.persistence.internal.sessions.MergeManager, org.eclipse.persistence.internal.sessions.AbstractSession)
     */
    public void mergeIntoObject(Object target, boolean isTargetUninitialized, Object source,
                                MergeManager mergeManager, AbstractSession targetSession) {
    }
}
