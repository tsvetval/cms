package ru.peak.ml.core.model.folder;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;

import java.util.List;

public class MlFolder extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    protected static class Properties {
        public static final String ID = "id";
        public static final String TITLE = "title";
        public static final String PARENT = "parent";
        public static final String CHILD_CLASS = "childClass";
        public static final String CHILD_CLASS_CONDITION = "childClassCondition";
        public static final String ICON = "icon";
        public static final String ORDER = "order";
        public static final String URL = "url";
        public static final String ATTR_SET_TO_SHOW = "attrSetToShow";
        public static final String CHILD_FOLDERS = "childFolders";
        public static final String PAGE_BLOCK = "pageBlock";
        public static final String CLASSIFIER_JSON = "classifierJSON";

    }

    public Long getId() {
        return get(Properties.ID);
    }

    public void setId(Long id) {
        set(Properties.ID, id);
    }

    public String getTitle() {
        return get(Properties.TITLE);
    }

    public String getIcon() {
        return get(Properties.ICON);
    }

    public void setTitle(String title) {
        set("title", title);
    }

    public MlFolder getParent() {
        return get(Properties.PARENT);
    }

    public void setParent(MlFolder parent) {
        set(Properties.PARENT, parent);
    }

    public MlClass getChildClass() {
        return get(Properties.CHILD_CLASS);
    }

    public void setChildClass(MlClass childClass) {
        set(Properties.CHILD_CLASS, childClass);
    }

    public String getChildClassCondition() {
        return get(Properties.CHILD_CLASS_CONDITION);
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }


    public List<MlAttr> getAttrListToShow() {
        return (List<MlAttr>) this.getPropertiesMap().get(Properties.ATTR_SET_TO_SHOW).getValue();
    }

    public Long getOrder() {
        return get(Properties.ORDER);
    }

    public void setOrder(Long order) {
        set(Properties.ORDER, order);
    }

    public List<MlFolder> getChildFolders() {
        return get(Properties.CHILD_FOLDERS);
    }

    public void setChildFolders(List<MlFolder> childFolders) {
        set(Properties.CHILD_FOLDERS, childFolders);
    }

    public String getUrl() {
        return get(Properties.URL);
    }

    public void setUrl(String url) {
        set(Properties.URL, url);
    }

    public void setIconURL(String iconURL) {
        set("iconURL", iconURL);
    }

    public String getIconURL() {
        return get("iconURL");
    }

    public List<MlPageBlockBase> getPageBlocks(){
        return get(Properties.PAGE_BLOCK);
    }

    public void setPageBlock(List<MlPageBlockBase> value){
        set(Properties.PAGE_BLOCK, value);
    }
    /** Классификатор папок.
     * Исползуется для генерации папок по значениям полей таблицы. Например вывести авторов по годам рождения или книги по авторам.
     * для папки будут сгенерированны подпапки с уникальными значениями из таблицы.
     *
     * Формируется запрос вида: select distinct [attrName|selectValue] from ... [where whereCondition] [order by orderCondition]
     *
     *
     * Поле заполняется json массивом, объектами вида:
     * {"id":"1","attrName":"name","whereCondition":"","orderCondition":"","selectValue":""}
     * id - строка, должен быть уникальным в массиве. обязательный.
     * attrName - строка, имя поля. обязательный.
     * whereCondition - условие where. не обязательный.
     * orderCondition - не обязательный.
     * selectValue - выражение используемое в select. не обязательный. если заполнен то будет использован вместо attrName
     *
     * Порядок следавания объектов имеет значение.
     *
     * */
    public String getClassifierJSON(){
        return get(Properties.CLASSIFIER_JSON);
    }

    public boolean isClassifier(){
        return getClassifierJSON()!=null;
    }

    /*
        Helper methods
     */

    /**
     * Возвращает список отображаемых атрибутов MlClass'а
     */
/*
    public static List<MlAttr> getMlClassAttributesToShow(MlClass IMlClass) {
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        DynamicEntity folder = commonDao.getSingleResult("select f from MlFolder f where f.childClass.id = " + IMlClass.getId(), MlAttr.class);
        List<IMlAttr> attrSetToShow = folder.get("attrSetToShow");
        return attrSetToShow;
    }

*/
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MlFolder) {
            return this.getId().equals(((MlFolder) obj).getId());
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        if (this.getId() == null) {
            return super.hashCode();
        } else {
            return this.getId().intValue();
        }
    }

}
