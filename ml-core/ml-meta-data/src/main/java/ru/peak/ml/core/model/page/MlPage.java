package ru.peak.ml.core.model.page;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;

import java.util.List;

/**
 *
 */
public class MlPage extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    public Long getId() {
        return get("id");
    }

    public String getUrl() {
        return get("url");
    }

    public String getTitle() {
        return get("title");
    }

    public String getTemplate() {
        return get("template");
    }
    public String getProjectTemplate() {
        return get("projectTemplate");
    }


    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public List<MlPageBlockBase> getPageBlocks() {
        return get("blocks");
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof MlPage && this.getId().equals(((MlPage) obj).getId());
    }

    @Override
    public int hashCode() {
        if (this.getId() == null) {
            return super.hashCode();
        } else {
            return this.getId().intValue();
        }
    }
}
