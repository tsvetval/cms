package ru.peak.ml.core.model.page;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;

/**
 *
 */
public class MlPageBlockBase extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    public static final String MlPageBlockBase = "MlPageBlockBase";
    public static final String ORDER_NUMBER = "orderNum";
    public static final String ID = "id";
    public static final String ZONE = "zone";
    public static final String CONTROLLER_JAVA_CLASS = "controllerJavaClass";
    public static final String TEMPLATE = "template";
    public static final String PAGE = "page";

    public static final String BOOT_JS = "bootJs";


    public Long getOrderNumber(){

        return this.get(ORDER_NUMBER) == null ? 0l : (Long)this.get(ORDER_NUMBER);
    }

    public String getControllerJavaClass(){
        return this.get(CONTROLLER_JAVA_CLASS) == null ? null : (String)this.get(CONTROLLER_JAVA_CLASS);
    }

    public String getBootJs(){
        return this.get(BOOT_JS) == null ? null : (String)this.get(BOOT_JS);
    }

    public String getTemplate(){
        return this.get(TEMPLATE) == null ? "" : (String)this.get(TEMPLATE);
    }

    public MlPage getMlPage(){
        return get(PAGE);
    }

    public Long getId(){
        return get(ID);
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }

}
