package ru.peak.ml.core.model.security;

public enum MlAttrAccessType {
    SHOW ("отображение"),
    EDIT ("редактирование");

    private final String operationName;

    public String getOperationName() {
        return operationName;
    }

    MlAttrAccessType(String operationName) {
        this.operationName = operationName;
    }
}
