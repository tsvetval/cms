package ru.peak.ml.core.model.security;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.system.MlClass;

/**
 */
public class MlClassAccess extends MlDynamicEntityImpl {

    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    public Long getId() {
        return (Long) this.getPropertiesMap().get("id").getValue();
    }

    public void setId(Long id) {
        set("id", id);
    }

    public MlClass getMlClass() {
        return get("mlClass");
    }

    public void setMlClass(MlClass mlClass) {
        this.set("mlClass", mlClass);
    }

    public String getAdditionalQuery(){
        return get("additionalQuery");
    }

    public Boolean isCreate() {
        return get("create");
    }

    public void setCreate(Boolean create) {
        this.set("create", create);
    }

    public Boolean isRead() {
        return get("read");
    }

    public void setRead(Boolean read) {
        this.set("read", read);
    }

    public Boolean isUpdate() {
        return get("update");
    }

    public void setUpdate(Boolean update) {
        this.set("update", update);
    }

    public Boolean isDelete() {
        return get("delete");
    }

    public void setDelete(Boolean delete) {
        this.set("delete", delete);
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }
}
