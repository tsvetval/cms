package ru.peak.ml.core.model.security;

public enum MlClassAccessType {
    CREATE ("создание"),
    READ ("чтение"),
    UPDATE ("изменение"),
    DELETE ("удаление");

    private final String operationName;

    public String getOperationName() {
        return operationName;
    }

    MlClassAccessType(String operationName) {
        this.operationName = operationName;
    }
}
