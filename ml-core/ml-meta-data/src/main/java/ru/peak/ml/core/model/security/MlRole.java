package ru.peak.ml.core.model.security;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.ml.core.common.RoleType;
import ru.peak.ml.core.model.MlDynamicEntityImpl;

import java.util.List;

/**
 */
public class MlRole extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    public Long getId() {
        return (Long) this.getPropertiesMap().get("id").getValue();
    }

    public void setId(Long id) {
        set("id", id);
    }

    public String getName() {
        return (String) this.getPropertiesMap().get("name").getValue();
    }

    public void setName(String name) {
        set("name", name);
    }

    public List<MlPageAccess> getPageAccess(){
        return get("pageAccess");
    }
    public List<MlFolderAccess> getFolderAccess(){
        return get("folderAccess");
    }
    public List<MlUtilAccess> getUtilAccess(){
        return get("utilAccess");
    }
    public List<MlClassAccess> getClassAccess(){
        return get("classAccess");
    }
    public List<MlAttrAccess> getAttrAccess(){
        return get("attrAccess");
    }

    public String getRoleType(){
        return (String) get("roleType");
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
