package ru.peak.ml.core.model.security;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;

/**
 */
public class MlSecuritySettings extends MlDynamicEntityImpl {

    public static String ID = "id";
    public static String AUTH_TYPE = "authType";
    public static String PASSWORD_STRENGTH_LENGTH = "passwordStrengthLength";
    public static String PASSWORD_STRENGTH_ALPHABET = "passwordStrengthAlphabet";
    public static String FAIL_AUTH_COUNT = "failAuthCount";
    public static String BLOCK_TIME_AFTER_FAIL_AUTH = "blockTimeAfterFailAuth";
    public static String BLOCK_AFTER_NO_USE = "blockAfterNotUse";
    public static String SESSION_LIFE_TIME = "sessionLifeTime";
    public static String PASSWORD_LIFE_TIME = "passwordLifeTime";
    public static String FOR_ADMIN = "forAdmin";

    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }

    public AuthType getAuthType() {
        return AuthType.valueOf((String) get(AUTH_TYPE));
    }

    public Long getPasswordStrengthLength() {
        return get(PASSWORD_STRENGTH_LENGTH);
    }

    public PasswordAlphabet getPasswordStrengthAlphabet() {
        return PasswordAlphabet.valueOf((String) get(PASSWORD_STRENGTH_ALPHABET));
    }

    public Long getFailAuthCount() {
        return get(FAIL_AUTH_COUNT);
    }

    public Long getBlockTimeAfterFailAuth() {
        return get(BLOCK_TIME_AFTER_FAIL_AUTH);
    }

    public Long getBlockAfterNotUse() {
        return get(BLOCK_AFTER_NO_USE);
    }

    public Long getSessionLifeTime() {
        return get(SESSION_LIFE_TIME);
    }

    public Long getPasswordLifeTime() {
        return get(PASSWORD_LIFE_TIME);
    }

    public Boolean getForAdmin() {
        return get(FOR_ADMIN);
    }


    public enum AuthType {
        TOKEN_AUTH,
        PASSWORD_AUTH
    }

    public enum PasswordAlphabet {
        NO_CONTROL,
        CYKILLIC_LC,  //Обязательное использование строчных кириллических символов
        CYKILLIC_LC_UC,//Обязательное использование строчных и заглавных кириллических символов
        CYKILLIC_LC_UC_NUMBER //Обязательное использование строчных, заглавных кириллических символов и цифр
    }
}
