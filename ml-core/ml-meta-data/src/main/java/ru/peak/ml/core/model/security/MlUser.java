package ru.peak.ml.core.model.security;

import org.apache.commons.codec.digest.DigestUtils;
import org.eclipse.persistence.dynamic.DynamicEntity;
import org.eclipse.persistence.exceptions.DynamicException;
import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;

import java.util.Date;
import java.util.List;

/**
 */
public class MlUser extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();
    private static final String FAKE_PASSWORD = "********";

    private boolean anonymous;


    public boolean isAnonymous() {
        return anonymous;
    }

    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }

    public Long getId() {
        return (Long) this.getPropertiesMap().get("id").getValue();
    }

    public void setId(Long id) {
        set("id", id);
    }

    public String getLogin() {
        return (String) this.getPropertiesMap().get("login").getValue();
    }

    public void setLogin(String login) {
        set("login", login);
    }

    public void setPassword(String password) {
        set("password", password);
    }

    public List<MlRole> getRoles() {
        return get("roles");
    }


    public Long getFailAuthCount() {
        return get("failAuthCount");
    }

    public void setFailAuthCount(Long failAuthCount) {
        set("failAuthCount", failAuthCount);
    }

    public Date getLastChange() {
        return get("lastChange");
    }

    public void setLastChange(Date lastChange) {
        set("lastChange", lastChange);
    }

    public Date getLastLogin() {
        return get("lastLogin");
    }

    public void setLastLogin(Date lastLogin) {
        set("lastLogin", lastLogin);
    }

    public Date getLastPasswordChange() {
        return get("lastPasswordChange");
    }

    public void setLastPasswordChange(Date lastPasswordChange) {
        set("lastPasswordChange", lastPasswordChange);
    }

    public Date getTemporalBlockedBefore() {
        return get("temporalBlockedBefore");
    }

    public String getHomePage() {
        return get("homePage");
    }


    public void setTemporalBlockedBefore(Date temporalBlockedBefore) {
        set("temporalBlockedBefore", temporalBlockedBefore);
    }

    public Boolean isBlocked() {
        return get("isBlocked") == null ? false : (Boolean) get("isBlocked");
    }

    public void setBlocked(Boolean isBlocked) {
        set("isBlocked", isBlocked);
    }

    public Boolean isTemporalBlocked() {
        return get("isTemporalBlocked") == null ? false : (Boolean) get("isTemporalBlocked");
    }

    public void setTemporalBlocked(Boolean isBlocked) {
        set("isTemporalBlocked", isBlocked);
    }

    public Boolean isNeedChangePassword() {
        return get("needChangePassword") == null ? true : (Boolean) get("needChangePassword");
    }

    public void setNeedChangePassword(Boolean needChangePassword) {
        set("needChangePassword", needChangePassword);
    }

    //тут возвращаем фейковый пароль
    public String getPassword() {
        return FAKE_PASSWORD;
    }

    //тут возвращаем хэш из базы
    public String getHash() {
        return (String) this.getPropertiesMap().get("password").getValue();
    }

    @Override
    public <T> T get(String propertyName) throws DynamicException {
        if ("password".equals(propertyName)) {
            return (T) getPassword();
        } else {
            return super.get(propertyName);
        }
    }

    @Override
    public DynamicEntity set(String propertyName, Object value) throws DynamicException {
        if ("password".equals(propertyName)) {
            if (value != null && !value.toString().isEmpty()) {
                String hash = DigestUtils.sha512Hex(value.toString());
                return super.set("password", hash);
            } else {
                return this;
            }
        } else {
            return super.set(propertyName, value);
        }
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
