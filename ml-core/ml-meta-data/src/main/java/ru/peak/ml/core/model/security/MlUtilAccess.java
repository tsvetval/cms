package ru.peak.ml.core.model.security;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.util.MlUtil;

import java.util.List;

/**
 */
public class MlUtilAccess extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    public Long getId() {
        return (Long) this.getPropertiesMap().get("id").getValue();
    }

    public void setId(Long id) {
        set("id", id);
    }

    public List<MlUtil> getUtils(){
        return get("utils");
    }

    /*public Boolean isShowChildren(){
        return get("showChildren");
    }
*/
    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
