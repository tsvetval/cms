package ru.peak.ml.core.model.system;

import org.eclipse.persistence.indirection.ValueHolder;
import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import org.eclipse.persistence.internal.indirection.UnitOfWorkQueryValueHolder;
import ru.ml.core.common.AttrType;
import ru.ml.core.common.FetchType;
import ru.ml.core.common.exceptions.MlServerException;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.ml.core.common.ReplicationType;

import java.util.Date;

/**
 *
 */
public class MlAttr extends MlDynamicEntityImpl {

    public static final String ID = "id";
/*
    public static final String NAME = "name";
*/
    public static final String ENTITY_FIELD_NAME = "entityFieldName";
    public static final String TABLE_FIELD_NAME = "tableFieldName";
    public static final String FILED_TYPE = "fieldType";
    public static final String REPLICATION_TYPE = "replicationType";
    public static final String GUID = "guid";
    public static final String LAST_CHANGE = "lastChange";
    public static final String VIRTUAL = "virtual";
    public static final String ORDERED = "ordered";
    public static final String DESCRIPTION = "description";
    public static final String DEFAULT_VALUE = "defaultValue";
    public static final String LONG_LINK_VALUE = "longLinkValue";
    public static final String MANY_TO_MANY_TABLE_NAME = "manyToManyTableName";
    public static final String MANY_TO_MANY_FIELD_NAME_M = "manyToManyFieldNameM";
    public static final String MANY_TO_MANY_FIELD_NAME_N = "manyToManyFieldNameN";


    public static final String NOT_SHOW_CREATE = "notShowCreate";
    public static final String NOT_SHOW_CHOOSE = "notShowChoose";
    public static final String NOT_SHOW_EDIT = "notShowEdit";
    public static final String NOT_SHOW_DELETE = "notShowDelete";
    public static final String NOT_SHOW_CREATE_IN_EDIT = "notShowCreateInEdit";
    public static final String NOT_SHOW_CHOOSE_IN_EDIT = "notShowChooseInEdit";
    public static final String NOT_SHOW_EDIT_IN_EDIT = "notShowEditInEdit";
    public static final String NOT_SHOW_DELETE_IN_EDIT = "notShowDeleteInEdit";

    public static final String NEW_LINE = "newLine";
    public static final String VIEW_POS = "viewPos";
    public static final String OFFSET = "offset";
    public static final String TOTAL_LENGTH = "totalLength";
    public static final String TITLE_LENGTH = "titleLength";
    public static final String TABLE_HEIGHT = "tableHeight";
    public static final String MANDATORY = "mandatory";
    public static final String MASK = "inputmask";
    public static final String READONLY = "readOnly";

    public static final String FORMAT = "fieldFormat";

    public static final String VIEW = "view";

    public static final String TEMPLATE_VIEW = "templateView";
    public static final String TEMPLATE_EDIT = "templateEdit";
    public static final String TEMPLATE_CREATE = "templateCreate";

    public static final String HISTORY_START_DATE_FIELD_NAME = "start_date";
    public static final String HISTORY_END_DATE_FIELD_NAME = "end_date";
    public static final String HISTORY_USER_LOGIN_FIELD_NAME = "UserLogin";
    public static final String HISTORY_ACTION_TYPE_FIELD_NAME = "ActionType";

    public static final String PRIMARY_KEY = "primaryKey";
    public static final String SYSTEM_FIELD = "systemField";
    public static final String AUTO_INCREMENT = "autoIncrement";
    public static final String LINK_ATTR = "linkAttr";
    public static final String ML_CLASS = "mlClass";
    public static final String LINK_CLASS = "linkClass";
    public static final String DEFAULT_SQL_VALUE = "defaultSqlValue";

    public static final String FILE_NAME_POSTFIX = "_filename";
    public static final String USE_IN_SIMPLE_SEARCH = "useInSimpleSearch";
    public static final String USE_IN_EXTENDED_SEARCH = "useInExtendedSearch";

    public static final String HETERO_LINK_TABLE_PREFIX = "Hetero";
    public static final String HETERO_LINK_FIELD_NAME = "link";
    public static final String FETCH_TYPE = "fetchType";



    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    private MlAttr overridedAttr;

    public MlAttr getOverridedAttrIfExist() {
        return overridedAttr == null ? this : overridedAttr;
    }

    public Boolean isOverrided() {
        return overridedAttr != null;
    }

    public void setOverridedAttr(MlAttr overridedAttr) {
        this.overridedAttr = overridedAttr;
    }

    public Long getId() {
        return get(ID);
    }

    public void setId(Long id) {
        set(ID, id);
    }

    public void setLastChange(Date lastChange) {
        set(LAST_CHANGE, lastChange);
    }

    public void setGUID(String guid) {
        set(GUID, guid);
    }

    public String getDescription() {
        return get(DESCRIPTION);
    }

    public void setDescription(String description) {
        set(DESCRIPTION, description);
    }

    public Object getDefaultValue() {
        return get(DEFAULT_VALUE);
    }

    public void setDefaultValue(Object defaultValue) {
        set(DEFAULT_VALUE, defaultValue);
    }

    public String getDefaultSqlValue() {
        return get(DEFAULT_SQL_VALUE);
    }

    public String getLongLinkValue() {
        return get(LONG_LINK_VALUE);
    }

    public void setDefaultSqlValue(String value) {
        set(DEFAULT_SQL_VALUE, value);
    }


    public String getEntityFieldName() {
        return (String) this.getPropertiesMap().get(MlAttr.ENTITY_FIELD_NAME).getValue();
    }

    public void setEntityFieldName(String entityFieldName) {
        set(MlAttr.ENTITY_FIELD_NAME, entityFieldName);
    }

    public String getTableFieldName() {
        return (String) this.getPropertiesMap().get(TABLE_FIELD_NAME).getValue();
    }

    public void setTableFieldName(String tableFieldName) {
        set(TABLE_FIELD_NAME, tableFieldName);
    }

    public ReplicationType getReplicationType() {
        return get(REPLICATION_TYPE) != null ? ReplicationType.valueOf((String) get(REPLICATION_TYPE)) : null;
    }

    public void setReplicationType(ReplicationType replicationType) {
        set(REPLICATION_TYPE, replicationType.name());
    }

    public Boolean getSystemField() {
        return (Boolean) this.getPropertiesMap().get(SYSTEM_FIELD).getValue();
    }

    public Boolean getPrimaryKey() {
        return (Boolean) this.getPropertiesMap().get(PRIMARY_KEY).getValue();
    }

    public void setPrimaryKey(Boolean primaryKey) {
        set(PRIMARY_KEY, primaryKey);
    }

    public Boolean getAutoIncrement() {
        return (Boolean) this.getPropertiesMap().get(AUTO_INCREMENT).getValue();
    }

    public AttrType getFieldType() {
        String type = (String) this.getPropertiesMap().get(FILED_TYPE).getValue();
        return type != null ? AttrType.valueOf(type) : null;
    }

    public void setFieldType(AttrType attrType) {
        set(FILED_TYPE, attrType.toString());
    }

    public MlAttr getLinkAttr() {
        Object linkAttr = this.getPropertiesMap().get(LINK_ATTR).getValue();
        if (linkAttr instanceof ValueHolder) {
            return (MlAttr) ((ValueHolder) linkAttr).getValue();
        }
        return (MlAttr) ((UnitOfWorkQueryValueHolder) linkAttr).getValue();
    }

    public MlClass getLinkClass() {
        Object linkClass = this.getPropertiesMap().get(LINK_CLASS).getValue();
        if (linkClass instanceof ValueHolder) {
            return (MlClass) ((ValueHolder) linkClass).getValue();
        }
        return (MlClass) ((UnitOfWorkQueryValueHolder) linkClass).getValue();
    }


    public MlAttrGroup getGroup() {
        Object mlClass = this.getPropertiesMap().get("group").getValue();
        if (mlClass instanceof ValueHolder) {
            return (MlAttrGroup) ((ValueHolder) mlClass).getValue();
        }
        return (MlAttrGroup) ((UnitOfWorkQueryValueHolder) mlClass).getValue();
    }

    public MlClass getMlClass() {
        Object mlClass = this.getPropertiesMap().get("mlClass").getValue();
        if (mlClass instanceof ValueHolder) {
            return (MlClass) ((ValueHolder) mlClass).getValue();
        }
        return (MlClass) ((UnitOfWorkQueryValueHolder) mlClass).getValue();
    }

    public void setMlClass(MlClass mlClass) {
        set("mlClass", mlClass);
    }

    public void setSystemField(Boolean system) {
        set(SYSTEM_FIELD, system);
    }

    public void setInList(Boolean inList) {
        set("inList", inList);
    }

    public void setInForm(Boolean inForm) {
        set("inForm", inForm);
    }

    public void setAutoIncrement(Boolean autoIncrement) {
        set(AUTO_INCREMENT, autoIncrement);
    }

    public DynamicPropertiesManager getDPM() {
        return DPM;
    }

    public void setDPM(DynamicPropertiesManager DPM) {
        this.DPM = DPM;
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Boolean getInList() {
        return (Boolean) this.getPropertiesMap().get("inList").getValue();
    }

    public Boolean getInForm() {
        return (Boolean) this.getPropertiesMap().get("inForm").getValue();
    }


    public Boolean isUseInSimpleSearch() {
        return this.getPropertiesMap().get(USE_IN_SIMPLE_SEARCH).getValue() == null ? false : (Boolean) this.getPropertiesMap().get(USE_IN_SIMPLE_SEARCH).getValue();
    }

    public Boolean isUseInExtendedSearch() {
        return this.getPropertiesMap().get(USE_IN_EXTENDED_SEARCH).getValue() == null ? false : (Boolean) this.getPropertiesMap().get(USE_IN_EXTENDED_SEARCH).getValue();
    }

    public Long getViewPos() {
        return (Long) this.getPropertiesMap().get("viewPos").getValue();
    }

    public Boolean isNewLine() {
        return (Boolean) this.getPropertiesMap().get(NEW_LINE).getValue();
    }

    public Long getOffset() {
        return (Long) this.getPropertiesMap().get(OFFSET).getValue();
    }

    public Long getTotalLength() {
        return (Long) this.getPropertiesMap().get(TOTAL_LENGTH).getValue();
    }

    public Long getTitleLength() {
        return (Long) this.getPropertiesMap().get(TITLE_LENGTH).getValue();
    }

    public Long getTableHeight() {
        return (Long) this.getPropertiesMap().get(TABLE_HEIGHT).getValue();
    }

    public Boolean getLazy() {
        return (Boolean) this.getPropertiesMap().get("lazy").getValue();
    }

    public Boolean isVirtual() {
        return (Boolean) this.getPropertiesMap().get(VIRTUAL).getValue();
    }

    public void setVirtual(Boolean virtual) {
        set(VIRTUAL, virtual);
    }

    public Boolean isOrdered() {
        return this.getPropertiesMap().get(ORDERED).getValue() == null ? false
                : (Boolean) this.getPropertiesMap().get(ORDERED).getValue();
    }

    public void setOrdered(Boolean ordered) {
        set(ORDERED, ordered);
    }

    public Boolean isMandatory() {
        return this.getPropertiesMap().get(MANDATORY).getValue() == null ? false
                : (Boolean) this.getPropertiesMap().get(MANDATORY).getValue();
    }

    public Boolean isReadOnly() {
        return this.getPropertiesMap().get(READONLY).getValue() == null ? false
                : (Boolean) this.getPropertiesMap().get(READONLY).getValue();
    }

    public Boolean isNotShowCreate() {
        return this.getPropertiesMap().get(NOT_SHOW_CREATE).getValue() == null ? false
                : (Boolean) this.getPropertiesMap().get(NOT_SHOW_CREATE).getValue();
    }

    public Boolean isNotShowChoose() {
        return this.getPropertiesMap().get(NOT_SHOW_CHOOSE).getValue() == null ?
                false : (Boolean) this.getPropertiesMap().get(NOT_SHOW_CHOOSE).getValue();
    }

    public Boolean isNotShowDelete() {
        return this.getPropertiesMap().get(NOT_SHOW_DELETE).getValue() == null ?
                false : (Boolean) this.getPropertiesMap().get(NOT_SHOW_DELETE).getValue();
    }

    public Boolean isNotShowEdit() {
        return this.getPropertiesMap().get(NOT_SHOW_EDIT).getValue() == null ?
                false : (Boolean) this.getPropertiesMap().get(NOT_SHOW_EDIT).getValue();
    }

    public Boolean isNotShowCreateInEdit() {
        return this.getPropertiesMap().get(NOT_SHOW_CREATE_IN_EDIT).getValue() == null ? false
                : (Boolean) this.getPropertiesMap().get(NOT_SHOW_CREATE_IN_EDIT).getValue();
    }

    public Boolean isNotShowChooseInEdit() {
        return this.getPropertiesMap().get(NOT_SHOW_CHOOSE_IN_EDIT).getValue() == null ?
                false : (Boolean) this.getPropertiesMap().get(NOT_SHOW_CHOOSE_IN_EDIT).getValue();
    }

    public Boolean isNotShowDeleteInEdit() {
        return this.getPropertiesMap().get(NOT_SHOW_DELETE_IN_EDIT).getValue() == null ?
                false : (Boolean) this.getPropertiesMap().get(NOT_SHOW_DELETE_IN_EDIT).getValue();
    }

    public Boolean isNotShowEditInEdit() {
        return this.getPropertiesMap().get(NOT_SHOW_EDIT_IN_EDIT).getValue() == null ?
                false : (Boolean) this.getPropertiesMap().get(NOT_SHOW_EDIT_IN_EDIT).getValue();
    }

    public String getManyToManyTableName() {
        return (String) this.getPropertiesMap().get(MANY_TO_MANY_TABLE_NAME).getValue();
    }

    public String getManyToManyFieldNameM() {
        return (String) this.getPropertiesMap().get(MANY_TO_MANY_FIELD_NAME_M).getValue();
    }

    public String getManyToManyFieldNameN() {
        return (String) this.getPropertiesMap().get(MANY_TO_MANY_FIELD_NAME_N).getValue();
    }

    public String getTemplateView() {
        return (String) this.getPropertiesMap().get(TEMPLATE_VIEW).getValue();
    }

    public String getTemplateEdit() {
        return (String) this.getPropertiesMap().get(TEMPLATE_EDIT).getValue();
    }

    public String getTemplateCreate() {
        return (String) this.getPropertiesMap().get(TEMPLATE_CREATE).getValue();
    }

    public MlAttrView getView() {
        Object mlClass = this.getPropertiesMap().get(VIEW).getValue();
        if (mlClass instanceof ValueHolder) {
            return (MlAttrView) ((ValueHolder) mlClass).getValue();
        }
        return (MlAttrView) ((UnitOfWorkQueryValueHolder) mlClass).getValue();
    }

    public String getMask() {
        return (String) this.getPropertiesMap().get(MASK).getValue();
    }

    public FetchType getFetchType(){
        return FetchType.valueOf((String) get(FETCH_TYPE));
    }

    public MlClass getAttrLinkClass() {
        if (getFieldType() == AttrType.ONE_TO_MANY) {
            return getLinkAttr().getMlClass();
        } else if (getFieldType() == AttrType.MANY_TO_MANY || getFieldType() == AttrType.MANY_TO_ONE) {
            return getLinkClass();
        } else {
            throw new MlServerException(String.format("Атрибут [%s] является не ссылочным, невозможно получить ссылочный класс", getEntityFieldName()));
        }
    }

    public String getFormat() {
        if (this.getPropertiesMap().get(MlAttr.FORMAT) != null) {
            return (String) this.getPropertiesMap().get(MlAttr.FORMAT).getValue();
        } else {
            return null;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MlAttr) {
            return this.getId().equals(((MlAttr) obj).getId());
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        if (this.getId() == null) {
            return super.hashCode();
        } else {
            return this.getId().intValue();
        }
    }

}
