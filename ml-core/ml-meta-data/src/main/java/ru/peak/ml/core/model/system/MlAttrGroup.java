package ru.peak.ml.core.model.system;


import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.metadata.comparators.MlInFormAttrsOrderComparator;
import ru.peak.ml.core.model.MlDynamicEntityImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 */
public class MlAttrGroup extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    private List<MlAttrGroup> subGroups;
    private List<MlAttr> attrList;

    public static final String ID = "id";
    public static final String TITLE = "title";
    public static final String PARENT = "parent";
    public static final String LINKED_CLASS = "linkedClass";


    public Long getId() {
        return get(ID);
    }

    @Override
    public String getTitle() {
        return get(TITLE);
    }

    public MlAttrGroup getParent() {
        return get(PARENT);
    }

    public MlClass getMlClass() {
        return get(LINKED_CLASS);
    }

    //todo; implement?
    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public List<MlAttrGroup> getSubGroups() {
        if (subGroups == null) {
            subGroups = new ArrayList<>();
            if (this.getMlClass().getGroupList() != null) {
                for (MlAttrGroup group : this.getMlClass().getGroupList()) {
                    if (group.getParent() != null && group.getParent().equals(this)) {
                        subGroups.add(group);
                    }
                }
            }
        }
        return subGroups;
    }

    public List<MlAttr> getAttrList() {
        if (attrList == null) {
            attrList = new ArrayList<>();
            if (this.getMlClass().getAttrSet() != null) {
                for (MlAttr attr : this.getMlClass().getAttrSet()) {
                    if (attr.getGroup() != null && attr.getGroup().equals(this)) {
                        attrList.add(attr);
                    }
                }
            }
            Collections.sort(attrList, new MlInFormAttrsOrderComparator());
        }
        return attrList == null ? new ArrayList<MlAttr>() : attrList;
    }

    private List<MlAttr> inFormAttrList;

    public List<MlAttr> getInFormAttrList() {
        if (inFormAttrList == null) {
            inFormAttrList = new ArrayList<>();
            for (MlAttr attr : this.getAttrList()) {
                if (attr.getInForm()) {
                    inFormAttrList.add(attr);
                }
            }
        }
        return inFormAttrList == null ? new ArrayList<MlAttr>() : inFormAttrList;
    }

}
