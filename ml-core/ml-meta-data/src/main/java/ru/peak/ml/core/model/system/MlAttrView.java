package ru.peak.ml.core.model.system;


import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.ml.core.common.AttrType;
import ru.peak.ml.core.model.MlDynamicEntityImpl;

/**
 *
 */
public class MlAttrView extends MlDynamicEntityImpl {
    public static final String LIST_1_N_EXTERNAL = "LIST_1-N_EXTERNAL";
    public static final String LIST_M_N_EXTERNAL = "LIST_M-N_EXTERNAL";
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    public Long getId() {
        return get("id");
    }

    @Override
    public String getTitle() {
        return get("title");
    }

    public String getCode() {
        return get("code");
    }

    public AttrType getAttrType() {
        String attrType = get("attrType");
        if (attrType != null) {
            return AttrType.valueOf(attrType);
        }
        return null;
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }


    public String getTemplateName() {
        return get("templateName");
    }
}
