package ru.peak.ml.core.model.system;

import org.eclipse.persistence.indirection.ValueHolder;
import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import org.eclipse.persistence.internal.indirection.UnitOfWorkQueryValueHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.metadata.comparators.MlInFormAttrsOrderComparator;
import ru.peak.ml.core.model.MlDynamicEntityImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Класс мета данных содержащий описание класса
 */
public class MlClass extends MlDynamicEntityImpl {
    private static final Logger log = LoggerFactory.getLogger(MlClass.class);
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();
    private List<MlAttrGroup> rootGroups;

    public final static String TABLE_NAME = "tableName";
    public final static String HAS_HISTORY = "hasHistory";
    public final static String HISTORY_TABLE_POSTFIX = "History";
    public final static String ATTR_SET = "attrSet";
    public final static String ENTITY_NAME = "entityName";
    public final static String IS_CACHEABLE = "isCacheable";
    public final static String IS_ABSTRACT = "isAbstract";
    public final static String JAVA_CLASS = "javaClass";
    public final static String PARENT = "parent";
    public final static String DESCRIPTION = "description";
    public final static String ID = "id";
    public final static String IS_SYSTEM = "isSystem";
    public final static String TITLE_FORMAT = "titleFormat";
    public final static String HANDLER_CLASS_NAME = "handlerClassName";
    public final static String REPLICATION_HANDLER_CLASS_NAME = "replicationHandlerClassName";


    public Long getId() {
        return (Long) this.getPropertiesMap().get(ID).getValue();
    }

    public void setId(Long id) {
        this.set(ID, id);
    }

    public String getDescription() {
        return (String) this.getPropertiesMap().get(DESCRIPTION).getValue();
    }

    public void setDescription(String description) {
        this.set(DESCRIPTION, description);
    }

    public String getTitleFormat() {
        return (String) this.getPropertiesMap().get(TITLE_FORMAT).getValue();
    }

    public String getEntityName() {
        return (String) this.getPropertiesMap().get(ENTITY_NAME).getValue();
    }

    public void setEntityName(String entityName) {
        this.set(ENTITY_NAME, entityName);
    }

    public String getTableName() {
        return (String) this.getPropertiesMap().get(MlClass.TABLE_NAME).getValue();
    }

    public void setTableName(String tableName) {
        this.set(TABLE_NAME, tableName);
    }

    public Boolean getSystem() {
        return (Boolean) this.getPropertiesMap().get(IS_SYSTEM).getValue();
    }

    public Boolean getCacheable() {
        return (Boolean) this.getPropertiesMap().get(IS_CACHEABLE).getValue();
    }
    public Boolean getAbstract() {
        return (Boolean) this.getPropertiesMap().get(IS_ABSTRACT).getValue();
    }

    public List<MlAttrGroup> getGroupList() {
        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
        return metaDataHolder.getGroupList(this);
    }

    public void setSystem(Boolean system) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getJavaClass() {
        return (String) this.getPropertiesMap().get(JAVA_CLASS).getValue();
    }

    public void setJavaClass(String javaClass) {
        set(JAVA_CLASS, javaClass);
    }

    public List<MlAttr> getAttrSet() {
        return (List<MlAttr>) this.getPropertiesMap().get("attrSet").getValue();
    }

    public MlClass getParent() {
        //TODO проверить почему иногда возвращается ValueHolder, а иногда UnitOfWorkQueryValueHolder
        Object parent = this.getPropertiesMap().get("parent").getValue();
        if (parent instanceof ValueHolder) {
            return (MlClass) ((ValueHolder) parent).getValue();
        }
        return (MlClass) ((UnitOfWorkQueryValueHolder) parent).getValue();
    }


    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean hasHistory() {
        Boolean result = (Boolean) this.getPropertiesMap().get(MlClass.HAS_HISTORY).getValue();
        return result == null ? false : result;
    }

    public String getHandlerName() {
        return (String) this.getPropertiesMap().get(HANDLER_CLASS_NAME).getValue();
    }

    public String getReplicationHandlerName() {
        return (String) this.getPropertiesMap().get(REPLICATION_HANDLER_CLASS_NAME).getValue();
    }

    public String getHandler() {
        return (String) this.getPropertiesMap().get(HANDLER_CLASS_NAME).getValue();
    }

    public String getReplicationHandler() {
        return (String) this.getPropertiesMap().get(REPLICATION_HANDLER_CLASS_NAME).getValue();

    }

    public MlAttr getAttr(String entityName) {
        List<MlAttr> attrSet = getAttrSet();
        for (MlAttr attr : attrSet) {
            if (attr.getEntityFieldName().equals(entityName)) {
                return attr;
            }
        }
        return null;
    }

    public MlAttr getPrimaryKeyAttr() {
        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
        List<MlAttr> pkList = metaDataHolder.getPrimaryKey(this);
        if (pkList != null && !pkList.isEmpty()) {
            return pkList.get(0);
        }
        // Если еще не определили то ищем среди атрибутов текущего класса
        for (MlAttr attr : getAttrSet()) {
            if (attr.getPrimaryKey()) {
                return attr;
            }
        }

        return null;
    }

    @Deprecated
    public List<MlAttrGroup> getRootGroups() {
        if (rootGroups == null) {
            rootGroups = new ArrayList<>();
            if (this.getGroupList() != null) {
                for (MlAttrGroup group : this.getGroupList()) {
                    if (group.getParent() == null) {
                        rootGroups.add(group);
                    }
                }
            }
        }
        return rootGroups;
    }

    public List<MlAttr> getInListAttrList() {
        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
        return metaDataHolder.getInListAttrList(this);
    }


    public List<MlAttr> getInFormAttrList() {
        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
        return metaDataHolder.getInFormAttrList(this);
    }

    private List<MlAttr> inFormUnGroupAttrList;

    @Deprecated
    public List<MlAttr> getInFormUnGroupAttrList() {
        //TODO перенести в синглтон
        inFormUnGroupAttrList = new ArrayList<>();
        List<String> attrNameList = new ArrayList<>();

        //добавляем свои атрибуты
        if (this.getAttrSet() != null) {
            for (MlAttr attr : this.getAttrSet()) {
                if (attr.getInForm() && attr.getGroup() == null) {
                    inFormUnGroupAttrList.add(attr);
                    attrNameList.add(attr.getEntityFieldName());
                }
            }
        }

        //достаем атрибуты родителей
        List<MlClass> parentList = new ArrayList<>();
        MlClass parent = this.getParent();
        while (parent != null) {
            parentList.add(parent);
            parent = parent.getParent();
        }
        for (MlClass p : parentList) {
            if (p.getAttrSet() != null) {
                for (MlAttr attr : p.getAttrSet()) {
                    if (attr.getInForm() && !attrNameList.contains(attr.getEntityFieldName())
                            && attr.getGroup() == null) {
                        inFormUnGroupAttrList.add(attr);
                        attrNameList.add(attr.getEntityFieldName());
                    }
                }
            }
        }

        //сортируем по viewPos
        Collections.sort(inFormUnGroupAttrList, new MlInFormAttrsOrderComparator());
        return inFormUnGroupAttrList;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MlClass) {
            return this.getId().equals(((MlClass) obj).getId());
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        if (this.getId() == null) {
            return super.hashCode();
        } else {
            return this.getId().intValue();
        }
    }

}
