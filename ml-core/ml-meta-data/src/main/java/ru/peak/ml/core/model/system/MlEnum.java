package ru.peak.ml.core.model.system;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;

/**
 *
 */
public class MlEnum extends MlDynamicEntityImpl {
    public static final String  MlEnum_ID = "id";
    public static final String  MlEnum_TITLE = "title";
    public static final String  MlEnum_CODE = "code";
    public static final String  MlEnum_ORDER = "order";
    public static final String  MlEnum_DESCRIPTION = "description";
    public static final String  MlEnum_MLATTR = "mlAttr";
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();
    public Long getId() {
        return get(MlEnum_ID);
    }

    @Override
    public String getTitle() {
        return get(MlEnum_TITLE);
    }

    public String getCode() {
        return get(MlEnum_CODE);
    }

    public String getDescription() {
        return get(MlEnum_DESCRIPTION);
    }

    public MlAttr getAttr() {
        return get(MlEnum_MLATTR);
    }

    public Long getOrder() {
        return get(MlEnum_ORDER);
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
