package ru.peak.ml.core.model.util;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;

/**
 *
 */
public class MlUtil extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Long getId() {
        return get("id");
    }

    public Long getOrderNumber() {
        return get("orderNumber");
    }

    public String getButtonLabel() {
        return get("buttonLabel");
    }

    public String getButtonImgUrl() {
        return get("buttonImgUrl");
    }

    public String getButtonIcon() {
        return get("buttonIcon");
    }

    public String getControllerJavaClass() {
        return get("controllerJavaClass");
    }

    public String getUrl() {
        return get("url");
    }

    public String getMethod() {
        return get("method");
    }

    public String getOpenType() {
        return get("openType");
    }

    public String getDesription() {
        return get("description");
    }

    public Boolean getConfirmExec() {
        return get("confirmExec");
    }

    public String getConfirmExecMsg() {
        return get("confirmExecMsg");
    }

    public String getUtilBootJs(){
        return get("utilBootJs");
    }

    public Boolean getShowInAllObjects(){
        return get("showInAllObjects");
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MlUtil) {
            return this.getId().equals(((MlUtil) obj).getId());
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        if (this.getId() == null) {
            return super.hashCode();
        } else {
            return this.getId().intValue();
        }
    }
}
