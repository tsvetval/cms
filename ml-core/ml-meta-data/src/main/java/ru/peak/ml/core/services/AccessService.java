package ru.peak.ml.core.services;

import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.folder.MlFolder;
import ru.peak.ml.core.model.page.MlPage;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.security.MlAttrAccessType;
import ru.peak.ml.core.model.security.MlClassAccessType;
import ru.peak.ml.core.model.security.MlRole;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.model.util.MlUtil;

import java.util.List;

public interface AccessService {

    public boolean isAnonymous();

    public MlUser getCurrentUser();

    public List<MlRole> getUserRoles();

    public boolean hasRole(MlUser mlUser, String roleType);


    public boolean hasAdminRole();

    public boolean hasAdminRole(MlUser mlUser);

    public MlRole getRole(String roleType);

    public MlRole getAnonymousRole();

    public List<MlFolder> getUserRootFolders();

    public List<? extends MlDynamicEntityImpl> checkAccessFolder(List<? extends MlDynamicEntityImpl> folders);
    public Boolean checkAccessFolder(MlFolder folder);

    public Boolean checkAccessPage(MlPage mlPage);
    public Boolean checkAccessPageBlock(MlPageBlockBase pageBlock);

    public List<MlUtil> checkAccessUtil(List<MlUtil> utils);

    public Boolean checkAccessUtil(MlUtil util);

    public Boolean checkAccessClass(MlClass mlClass, MlClassAccessType accessType);
    public Boolean checkAccessAttr(MlAttr mlAttr, MlAttrAccessType attrAccessType);
    public List<MlAttr> checkAccessAttrs(List<MlAttr> mlAttrs, MlAttrAccessType attrAccessType);
    public List<String> getAdditionalQueryForClass(MlClass mlClass);
}
