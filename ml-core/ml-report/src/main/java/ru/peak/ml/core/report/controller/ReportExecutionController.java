package ru.peak.ml.core.report.controller;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import org.eclipse.persistence.dynamic.DynamicEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.AttrType;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.metadata.api.MetaDataHelper;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.model.util.MlUtil;
import ru.peak.ml.core.report.model.MlReport;
import ru.peak.ml.core.report.model.MlReportParameter;
import ru.peak.ml.core.report.model.MlUtilReport;
import ru.peak.ml.core.report.model.ReportParameterType;
import ru.peak.ml.core.report.service.ReportService;
import ru.peak.ml.core.report.service.dto.ReportResult;
import ru.peak.ml.core.services.AccessService;
import ru.peak.ml.web.block.controller.BlockController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.gson.MlGsonBuilder;
import ru.peak.ml.web.gson.adapter.serializers.MlDynamicEntitySerializerImpl;
import ru.peak.ml.web.service.annotations.PageBlockAction;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReportExecutionController implements BlockController {
    private static final Logger log = LoggerFactory.getLogger(ReportExecutionController.class);

    @Inject
    CommonDao commonDao;

    @Inject
    AccessService accessService;

    @Inject
    ReportService reportService;

    private static final String TEMPLATE_PATH = "path/to/template/file.hml";


    public void serve(MlHttpServletRequest params, MlPageBlockBase mlInstance, MlHttpServletResponse resp) throws MlApplicationException, MlServerException {
        log.error(String.format("Unknown action: [%s] in class [%s] ", params.getString("action"), this.getClass()));
        throw new MlApplicationException(String.format("В контроллере %s Данный функционал (%s) пока не реализован", this.getClass(), params.getString("action")));
    }

    @PageBlockAction(action = "show")
    public void show(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        if (params.hasString("objectId")) {
            Long reportId = params.getLong("objectId");
            MlReport mlReport = (MlReport) commonDao.findById(reportId, MlReport.class);
            if (checkAccess(mlReport)) {
                JsonElement metaDataJson = serializeObjectDataToResponse(mlReport);
                resp.setDataType(MlHttpServletResponse.DataType.JSON);
                resp.addDataToJson("MetaData", metaDataJson);
            } else {
                throw new MlApplicationException("Недостаточно прав!");
            }
        } else {
            throw new MlApplicationException("Некорректные параметры запроса, в запросе должны присутствовать [objectId]");
        }

    }

    private boolean checkAccess(MlReport mlReport) {
        List<MlUtil> utilReports = commonDao.getQueryWithoutSecurityCheck("select o from MlUtilReport o join o.reports r where r.id = :id ")
                .setParameter("id", mlReport.getId()).getResultList();
        for (MlUtil util : utilReports) {
            if (accessService.checkAccessUtil(util)) {
                return true;
            }
        }
        return false;
    }

    @PageBlockAction(action = "generateReport")
    public void generateReport(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        if (params.hasString("objectId")) {
            Long reportId = params.getLong("objectId");
            MlReport mlReport = (MlReport) commonDao.findById(reportId, MlReport.class);
            if (checkAccess(mlReport)) {
                MlUser user = (MlUser) params.getRequest().getSession().getAttribute("user");
                Gson gson = new Gson();
                Map<String, Object> objectMap = gson.fromJson(params.getString("data"),
                        new TypeToken<Map<String, Object>>() {
                        }.getType());
                objectMap.put("user", user);

                ReportResult reportResult = reportService.generateReport(mlReport, objectMap);
                resp.setDataType(MlHttpServletResponse.DataType.JSON);
                resp.setDownloadData(reportResult.getReport());
                resp.setDownloadFileName(reportResult.getReportName());
            } else {
                throw new MlApplicationException("Недостаточно прав!");
            }
        } else {
            throw new MlApplicationException("Некорректные параметры запроса, в запросе должны присутствовать [objectId]");
        }
    }

    @PageBlockAction(action = "getRefParamValues")
    public void getRefParamValues(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        JsonElement result = null;
        if (params.hasString("idList") && params.hasLong("paramId")) {
            CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
            Long paramId = params.getLong("paramId");
            MlReportParameter parameter = (MlReportParameter) commonDao.findById(paramId, "MlReportParameter");
            if (checkAccess(parameter.getReport())) {
                String idListStr = params.getString("idList");
                Gson gson = new Gson();
                if (parameter.getParameterType() == ReportParameterType.LINK) {
                    Type type = new TypeToken<List<Long>>() {
                    }.getType();
                    List<Long> idList = gson.fromJson(idListStr, type);
                    if (parameter.getSingleChoice() && !idList.isEmpty()) {
                        String className = parameter.getLinkMlClass().getEntityName();
                        DynamicEntity instance = commonDao.findById(idList.get(0), className);
                        result = new JsonObject();
                        ((JsonObject) result).addProperty("title", ((MlDynamicEntityImpl) instance).getTitle());
                        ((JsonObject) result).addProperty("id", idList.get(0));
                    } else {
                        //TODO
                        MlDynamicEntitySerializerImpl serializer = GuiceConfigSingleton.inject(MlDynamicEntitySerializerImpl.class);
                        String inArray = idList.toString().replace("[", "(").replace("]", ")");
                        MlClass linkClass = parameter.getLinkMlClass();
                        String jpql = String.format("select o from %s o where o.id in %s", linkClass.getEntityName(), inArray);
                        List<MlDynamicEntityImpl> selectedObjectList = commonDao.getResultList(jpql, linkClass.getEntityName());
                        //selectedObjectList = sortEntityListByIdList(selectedObjectList, idList);
                        result = serializer.serializeObjectList(selectedObjectList, linkClass.getInListAttrList(), true);
                    }
                }
            }else{
                throw new MlApplicationException("Недостаточно прав!");
            }
        }
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        resp.addDataToJson("result", result);
    }

    public String getDataByClientData(String clientData) {
        return "someData";
    }

    public String getDataByParam(String param) {
        return "someData";
    }

    protected JsonElement serializeObjectDataToResponse(MlReport mlReport) {
        JsonObject result = new JsonObject();
        result.add("name", new JsonPrimitive(mlReport.getName()));
        result.add("id", new JsonPrimitive(mlReport.getId()));
        JsonArray params = new JsonArray();
        for (MlReportParameter parameter : mlReport.getParameters()) {
            JsonObject param = new JsonObject();
            param.add("id", new JsonPrimitive(parameter.getId()));
            param.add("name", new JsonPrimitive(parameter.getName()));
            param.add("code", new JsonPrimitive(parameter.getCode()));
            param.add("mandatory", new JsonPrimitive(parameter.getMandatory()));
            param.add("type", new JsonPrimitive(parameter.getParameterType().toString()));
            if (parameter.getParameterType() == ReportParameterType.LINK) {
                param.add("singleChoice", new JsonPrimitive(parameter.getSingleChoice()));
                param.add("className", new JsonPrimitive(parameter.getLinkMlClass().getEntityName()));
                if (!parameter.getSingleChoice()) {
                    JsonObject objectDataJson = (JsonObject) MlGsonBuilder.getViewFormSerializer()
                            .toJsonTree(parameter.getLinkMlClass());
                    param.add("attrList", objectDataJson.get("attrList"));
                }
            }
            params.add(param);
        }
        result.add("params", params);
        return result;

    }
}

