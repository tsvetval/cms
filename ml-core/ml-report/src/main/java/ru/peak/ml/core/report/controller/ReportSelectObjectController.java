package ru.peak.ml.core.report.controller;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.filter.result.FilterResult;
import ru.peak.ml.core.filter.FilterBuilder;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.report.model.MlReportParameter;
import ru.peak.ml.web.block.controller.impl.list.ObjectListBlockController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.gson.adapter.serializers.MlDynamicEntitySerializerImpl;
import ru.peak.ml.web.service.annotations.PageBlockAction;

import java.util.List;

/**
 * Created by d_litovchenko on 06.04.15.
 */
public class ReportSelectObjectController extends ObjectListBlockController {
      @Inject
    CommonDao commonDao;
    /**
     * Получение списа объектов связанного класса
     *
     * @param params    -   параметры запроса могут содержать
     *                  refAttrId (long)    -   id атрибута связи
     *                  linkFilter (string) -   условия фильтрации связанных объектов
     *                  objectId (long)     -   id объекта-владельца связи
     *
     * @param resp
     * @param currentPageBlock
     */
    @PageBlockAction(action = "getObjectData")
    public void getObjectListData(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase currentPageBlock) {
        resp.setDataType(MlHttpServletResponse.DataType.JSON);

        Long id = Long.parseLong(params.getString("paramId", null));
        MlReportParameter mlReportParameter = (MlReportParameter) commonDao.findById(id, "MlReportParameter");

        MlClass refClass = mlReportParameter.getLinkMlClass();


        String linkFilter = mlReportParameter.getFilter();
        String linkCondition = null;
        if (linkFilter != null && !linkFilter.isEmpty()) {
            linkCondition = linkFilter;
        }

        FilterBuilder filterBuilder = GuiceConfigSingleton.inject(FilterBuilder.class);
        filterBuilder.setQueryMlClass(refClass);
        fillServiceFromRequest(filterBuilder,params, refClass);
        filterBuilder.addAdditionalCondition(linkCondition);


        FilterResult filterResult = filterBuilder.buildFilterResult();
        List<MlDynamicEntityImpl> objectList = filterResult.getResultList();
        resp.addDataToJson("RecordsCount", new JsonPrimitive(filterResult.getRecordsCount()));
        resp.addDataToJson("PagesCount", new JsonPrimitive(filterResult.getPagesCount()));

        List<MlAttr> objectAttrList = refClass.getInListAttrList();
        resp.addDataToJson("ObjectClassName", new JsonPrimitive(refClass.getEntityName()));
        resp.addDataToJson("ClassDescription", new JsonPrimitive(refClass.getDescription()));

        if (objectList != null) {
            MlDynamicEntitySerializerImpl serializer = GuiceConfigSingleton.inject(MlDynamicEntitySerializerImpl.class);
            JsonElement objectListDataJson = serializer.serializeObjectList(objectList, objectAttrList, true);
            resp.addDataToJson("ObjectListData", objectListDataJson);
        }

    }
}
