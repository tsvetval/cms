package ru.peak.ml.core.report.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;

import java.util.List;

/**
 * Created by d_litovchenko on 31.03.15.
 */
public class MlExcelReport extends MlReport {

    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }
    protected static class Properties {
        public static final String QUERIES = "queries";
    }

    public List<MlExcelReportQuery> getQueries(){
        return get(Properties.QUERIES);
    }
}
