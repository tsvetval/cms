package ru.peak.ml.core.report.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;

import java.util.Date;

/**
 * Created by d_litovchenko on 31.03.15.
 */
public class MlExcelReportQuery extends MlDynamicEntityImpl {

    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    protected static class Properties {
        public static final String CODE = "code";
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String QUERY = "query";
        public static final String EXCEL_REPORT = "excelReport";
        public static final String GUID = "guid";
        public static final String LAST_CHANGE = "lastChange";
    }
    public String getCode(){
        return get(Properties.CODE);
    }
    public void setCode(String value){
        set(Properties.CODE, value);
    }
    public Long getId(){
        return get(Properties.ID);
    }
    public void setId(Long value){
        set(Properties.ID, value);
    }
    public String getName(){
        return get(Properties.NAME);
    }
    public void setName(String value){
        set(Properties.NAME, value);
    }
    public String getQuery(){
        return get(Properties.QUERY);
    }
    public void setQuery(String value){
        set(Properties.QUERY, value);
    }
    public MlExcelReport getExcelReport(){
        return get(Properties.EXCEL_REPORT);
    }
    public void setExcelReport(MlExcelReport value){
        set(Properties.EXCEL_REPORT, value);
    }
    public String getGuid(){
        return get(Properties.GUID);
    }
    public void setGuid(String value){
        set(Properties.GUID, value);
    }
    public Date getLastChange(){
        return get(Properties.LAST_CHANGE);
    }
    public void setLastChange(Date value){
        set(Properties.LAST_CHANGE, value);
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }
}
