package ru.peak.ml.core.report.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.security.MlUser;

import java.util.Date;

/**
 * Created by d_litovchenko on 31.03.15.
 */
public class MlGeneratedReport extends MlDynamicEntityImpl {

    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    protected static class Properties {
        public static final String REPORT_FILE = "reportFile";
        public static final String REPORT_FILE_FILENAME = "reportFile_filename";
        public static final String REPORT = "report";
        public static final String USER = "user";
        public static final String GENERATED_DATE = "generatedDate";
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }

    public void setReportFile(byte[] data){
        set(Properties.REPORT_FILE,data);
    }
    public void setReportFileFileName(String data){
        set(Properties.REPORT_FILE_FILENAME,data);
    }
    public void setReport(MlReport data){
        set(Properties.REPORT,data);
    }
    public void setUser(MlUser data){
        set(Properties.USER,data);
    }
    public void setGeneratedDate(Date data){
        set(Properties.GENERATED_DATE,data);
    }




}
