package ru.peak.ml.core.report.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;

/**
 * Created by d_litovchenko on 31.03.15.
 */
public class MlJasperReport extends MlReport {

    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }
}
