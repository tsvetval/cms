package ru.peak.ml.core.report.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.system.MlClass;

/**
 * Created by d_litovchenko on 31.03.15.
 */
public class MlReportParameter extends MlDynamicEntityImpl {

    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }
    protected static class Properties {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String CODE = "code";
        public static final String TYPE = "type";
        public static final String DEFAULT_VALUE = "defaultValue";
        public static final String VIEW_HANDLER = "viewHandler";
        public static final String MANDATORY = "mandatory";
        public static final String ML_CLASS = "mlClass";
        public static final String REPORT = "report";
        public static final String FILTER = "filter";
        public static final String SINGLE_CHOICE = "singleChoice";
    }
    public Long getId(){
        return get(Properties.ID);
    }
    public Boolean getMandatory(){
        return get(Properties.MANDATORY);
    }
    public String getCode(){
        return get(Properties.CODE);
    }
    public String getName(){
        return get(Properties.NAME);
    }
    public String getFilter(){
        return get(Properties.FILTER);
    }
    public ReportParameterType getParameterType(){
        return ReportParameterType.valueOf((String) get(Properties.TYPE));
    }
    public Boolean getSingleChoice(){
        return get(Properties.SINGLE_CHOICE);
    }
    public MlClass getLinkMlClass(){
        return get(Properties.ML_CLASS);
    }
    public MlReport getReport(){
        return get(Properties.REPORT);
    }

}
