package ru.peak.ml.core.report.model;

/**
 * Created by d_litovchenko on 31.03.15.
 */
public enum ReportParameterType {
    STRING,LONG,BOOLEAN,DATE,LINK
}
