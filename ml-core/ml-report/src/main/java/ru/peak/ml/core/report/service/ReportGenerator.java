package ru.peak.ml.core.report.service;


import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import ru.peak.ml.core.report.model.MlReport;

import java.util.Map;

/**
 * Created by d_litovchenko on 01.04.15.
 */
public interface ReportGenerator {
    byte[] generateReport(MlReport mlReport, Map<String, Object> reportData) throws InvalidFormatException;
}
