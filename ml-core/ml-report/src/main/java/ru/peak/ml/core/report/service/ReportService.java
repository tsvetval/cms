package ru.peak.ml.core.report.service;

import com.google.inject.ImplementedBy;
import ru.peak.ml.core.report.model.MlReport;
import ru.peak.ml.core.report.service.dto.ReportResult;
import ru.peak.ml.core.report.service.impl.ReportServiceImpl;

import java.util.Map;

/**
 * Created by d_litovchenko on 31.03.15.
 */
@ImplementedBy(ReportServiceImpl.class)
public interface ReportService {
    public ReportResult generateReport(MlReport mlReport, Map<String,Object> reportData);
}
