package ru.peak.ml.core.report.service.dto;

public class ReportResult {

    private byte[] report;
    private String reportName;

    public byte[] getReport() {
        return report;
    }

    public void setReport(byte[] report) {
        this.report = report;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }
}
