package ru.peak.ml.core.report.service.impl;


import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.report.model.MlReport;
import ru.peak.ml.core.report.service.ReportGenerator;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by d_litovchenko on 01.04.15.
 */
public class JasperReportGenerator implements ReportGenerator {
    @Override
    public byte[] generateReport(MlReport mlReport, Map<String, Object> reportData) {
        EntityManager entityManager = GuiceConfigSingleton.inject(EntityManager.class);
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            Connection connection = entityManager.unwrap(Connection.class);

            byte[] template = mlReport.getTemplate();
            if(template==null) throw new RuntimeException("Шаблон для репорта не задан");

            JasperReport jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(template));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, reportData, connection);

            ByteArrayOutputStream result = new ByteArrayOutputStream();

            JRDocxExporter jrDocxExporter = new JRDocxExporter();
            jrDocxExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            jrDocxExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(result));
            jrDocxExporter.exportReport();
            return result.toByteArray();
        }catch (Exception e){
            throw new MlServerException("Ошибка при генерации отчета ",e);
        }finally {
            if(transaction.isActive()) transaction.commit();
        }
    }
}
