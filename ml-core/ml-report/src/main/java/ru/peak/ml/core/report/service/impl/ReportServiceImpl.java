package ru.peak.ml.core.report.service.impl;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.ml.core.report.model.MlGeneratedReport;
import ru.peak.ml.core.report.model.MlReport;
import ru.peak.ml.core.report.model.MlReportParameter;
import ru.peak.ml.core.report.service.ReportGenerator;
import ru.peak.ml.core.report.service.ReportService;
import ru.peak.ml.core.report.service.dto.ReportResult;
import ru.peak.ml.template.TemplateEngine;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ReportServiceImpl implements ReportService {
    private static final Logger log = LoggerFactory.getLogger(ReportServiceImpl.class);
    @Inject
    Map<Class,ReportGenerator> generatorMap;
    @Inject
    CommonDao commonDao;
    @Inject
    TemplateEngine templateEngine;

    @Override
    @Transactional
    public ReportResult generateReport(MlReport mlReport, Map<String, Object> reportData) {
        validateParams(mlReport, reportData);
        ReportGenerator reportGenerator;
        if(mlReport.hasController()){
            try {
                reportGenerator = (ReportGenerator) GuiceConfigSingleton.inject(mlReport.getController());
            } catch (ClassNotFoundException e) {
                throw new MlServerException("Ошибка получения генератора отчета ",e);
            }
        }else{
            reportGenerator = generatorMap.get(mlReport.getClass());
        }
        ReportResult result = new ReportResult();
        try {
            result.setReport(reportGenerator.generateReport(mlReport, reportData));
            result.setReportName(templateEngine.renderStringTemplate(mlReport.getReportFileName(),reportData));
            MlGeneratedReport generatedReport = (MlGeneratedReport) commonDao.createNewEntity(MlGeneratedReport.class);
            generatedReport.setGeneratedDate(new Date());
            generatedReport.setReport(mlReport);
            generatedReport.setReportFile(result.getReport());
            generatedReport.setReportFileFileName(result.getReportName());
            if(reportData.get("user")!=null){
                generatedReport.setUser((MlUser) reportData.get("user"));
            }
            commonDao.persistTransactionalWithoutSecurityCheck(generatedReport);
        } catch (InvalidFormatException e) {
            throw new MlServerException("Ошибка при генерации отчета ",e);
        }
        return result;
    }

    private void validateParams(MlReport mlReport, Map<String, Object> reportData) {
        for (MlReportParameter reportParameter : mlReport.getParameters()) {
            validateMandatoryExist(reportParameter, reportData);
            if(reportData.get(reportParameter.getCode())!=null){
                validateType(reportParameter, reportData);
            }

        }
    }

    private void validateType(MlReportParameter reportParameter, Map<String, Object> reportData) {
        try {
            switch (reportParameter.getParameterType()) {
                case STRING:
                    String svalue = (String) reportData.get(reportParameter.getCode());
                    reportData.remove(reportParameter.getCode());
                    reportData.put(reportParameter.getCode(), svalue);
                    break;
                case LONG:
                    Long lvalue = Long.valueOf(String.valueOf(reportData.get(reportParameter.getCode())));
                    reportData.remove(reportParameter.getCode());
                    reportData.put(reportParameter.getCode(), lvalue);
                    break;
                case BOOLEAN:
                    Boolean bvalue = Boolean.valueOf(String.valueOf(reportData.get(reportParameter.getCode())));
                    reportData.remove(reportParameter.getCode());
                    reportData.put(reportParameter.getCode(), bvalue);
                    break;
                case DATE:
                    Object dvalue = reportData.get(reportParameter.getCode());
                    if(dvalue instanceof Double){
                        Date date = new Date(((Double) dvalue).longValue());
                        reportData.remove(reportParameter.getCode());
                        reportData.put(reportParameter.getCode(), dvalue);
                    }else{
                        Date dateValue = (Date) reportData.get(reportParameter.getCode());
                    }
                    break;
                case LINK:
                    if(reportParameter.getSingleChoice()){
                        Object value = reportData.get(reportParameter.getCode());
                        if(!(value instanceof MlDynamicEntityImpl)){
                            if(value instanceof String){
                                value = Long.valueOf((String) value);
                            }
                            if(value instanceof Long){
                                value = commonDao.findById(value,reportParameter.getLinkMlClass().getEntityName());
                                reportData.remove(reportParameter.getCode());
                                reportData.put(reportParameter.getCode(), value);
                            }else{
                                throw new MlServerException(String.format("Параметр %s должен быть либо Long либо наследником MlDynamicEntityimpl",reportParameter.getName()));
                            }
                        }
                    }else{
                        List list = (List) reportData.get(reportParameter.getCode());
                        List<MlDynamicEntityImpl> res = new ArrayList<>();
                        for(Object value: list){
                            if(!(value instanceof MlDynamicEntityImpl)){
                                if(value instanceof String){
                                    value = Long.valueOf((String) value);
                                }
                                if(value instanceof Long){
                                    value = commonDao.findById(value,reportParameter.getLinkMlClass().getEntityName());

                                    res.add((MlDynamicEntityImpl) value);
                                }else{
                                    throw new MlServerException(String.format("Параметр %s должен быть либо Long либо наследником MlDynamicEntityimpl",reportParameter.getName()));
                                }
                            }else{
                                res.add((MlDynamicEntityImpl) value);
                            }
                        }
                        reportData.remove(reportParameter.getCode());
                        reportData.put(reportParameter.getCode(), res);
                    }
                    break;
            }
        } catch (ClassCastException e) {
            log.error("cast exception");
            throw new MlServerException("",e);//todo
        }
    }

    private void validateMandatoryExist(MlReportParameter reportParameter, Map<String, Object> reportData) {
        if (reportParameter.getMandatory()) {
            if (reportData.get(reportParameter.getCode()) == null) {
                log.error(String.format("Report parameter %s not found. Report parameter id = %s", reportParameter.getCode(), reportParameter.getId()));
                throw new MlServerException(String.format("Отсутствует обязательный параметр отчета %s ", reportParameter.getName()));
            }
        }
    }
}
