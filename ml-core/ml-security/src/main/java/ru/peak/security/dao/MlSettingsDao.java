package ru.peak.security.dao;

import com.google.inject.ImplementedBy;
import ru.peak.ml.core.model.security.MlSecuritySettings;
import ru.peak.security.dao.impl.MlSettingsDaoImpl;

/**
 */
@ImplementedBy(MlSettingsDaoImpl.class)
public interface MlSettingsDao {
    MlSecuritySettings getSettings(boolean isAdmin);
}
