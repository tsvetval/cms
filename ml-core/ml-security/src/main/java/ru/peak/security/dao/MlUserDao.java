package ru.peak.security.dao;

import com.google.inject.ImplementedBy;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.security.dao.impl.MlUserDaoImpl;

/**
 *
 */
@ImplementedBy(MlUserDaoImpl.class)
public interface MlUserDao {
    MlUser getUserById(Long id);
    MlUser getUserByLogin(String login);

}
