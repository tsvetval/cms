package ru.peak.security.dao.impl;

import com.google.inject.Inject;
import ru.ml.core.common.exceptions.MlServerException;
import ru.peak.ml.core.model.security.MlSecuritySettings;
import ru.peak.security.dao.MlSettingsDao;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 */
public class MlSettingsDaoImpl implements MlSettingsDao {
    @Inject
    private EntityManager entityManager;

    @Override
    public MlSecuritySettings getSettings(boolean isAdmin) {
        Query query = entityManager.createQuery("select o from MlSecuritySettings o where o.forAdmin = :forAdmin");
        query.setParameter("forAdmin",isAdmin);
        List<MlSecuritySettings> mlSecuritySettingses = query.getResultList();
        if(mlSecuritySettingses.isEmpty()){
            throw new MlServerException("Нет настроек безопасноти!!!");
        }
        return mlSecuritySettingses.get(0);
    }
}
