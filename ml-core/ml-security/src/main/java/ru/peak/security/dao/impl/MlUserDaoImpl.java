package ru.peak.security.dao.impl;

import com.google.inject.Inject;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.security.dao.MlUserDao;

import javax.persistence.EntityManager;
import java.util.List;

/**
 */
public class MlUserDaoImpl extends CommonDao implements MlUserDao{
    @Inject
    private EntityManager entityManager;

    @Override
    public MlUser getUserById(Long id) {
        List<MlUser> mlUsers = getTypedQueryWithoutSecurityCheck("select o from MlUser o where o.id = :id", "MlUser").
                setParameter("id", id).
                getResultList();
        if(mlUsers.size()>0){
            return mlUsers.get(0);
        }
        return null ;
    }

    @Override
    public MlUser getUserByLogin(String login) {
        List<MlUser> mlUsers = getTypedQueryWithoutSecurityCheck("select o from MlUser o where o.login = :login", "MlUser").
                setParameter("login", login).
                getResultList();
        if(mlUsers.size()>0){
            return mlUsers.get(0);
        }
        return null ;
    }
}
