package ru.peak.security.filter;

import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.security.services.impl.SecurityService;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/*


* Так как SecurityFilter синглтон, то при переинициализации у него остается "старый" EntityManager.
* Поэтому получаем сервисы из джуса каждый раз.
* */

@Deprecated
@Singleton
public class SecurityFilter implements Filter {
    private static final Logger log = LoggerFactory.getLogger(SecurityFilter.class);
    public static final String REDIRECT_URL = "/auth";
    public static final String REQUEST_PARAMETER_USER = "user";
    public static final String USER_KEY_SESSION = "user-id";
    public static final String URL_FOR_BACK_AFTER_AUTH = "need-redirect-url";
    public static final String USER_NEED_CHANGE_PASSWORD = "need-change-password";

    public void destroy() {

    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        String url = request.getRequestURI();
        if (!url.endsWith(REDIRECT_URL)) {
            HttpSession session = request.getSession();
            if (session.isNew() || session.getAttribute(USER_KEY_SESSION) == null) {
                if (session.getAttribute(URL_FOR_BACK_AFTER_AUTH) == null) {
                    session.setAttribute(URL_FOR_BACK_AFTER_AUTH, url);
                    log.info(String.format("IP : %s, after auth will redirect to: %s ", req.getRemoteAddr(), url));
                }
                response.sendRedirect(request.getContextPath() + REDIRECT_URL);

            } else {
                SecurityService securityService = GuiceConfigSingleton.inject(SecurityService.class);
                Long id = (Long) session.getAttribute(USER_KEY_SESSION);
                MlUser mlUser = securityService.getUserById(id);
                securityService.detach(mlUser);
                request.setAttribute(REQUEST_PARAMETER_USER, mlUser);
                chain.doFilter(req, resp);
            }
        } else {
            chain.doFilter(req, resp);
        }
    }

    public void init(FilterConfig config) throws ServletException {
    }

}
