package ru.peak.security.listener;

import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.security.services.impl.SecurityService;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.Calendar;
import java.util.Date;

/**
 * Лисенер по инициализации безопасности
 *
 */
public class SessionListener implements HttpSessionListener {
    @Override
    public void sessionCreated(HttpSessionEvent se) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        SecurityService securityService = GuiceConfigSingleton.inject(SecurityService.class);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(se.getSession().getLastAccessedTime());
        calendar.set(Calendar.SECOND,calendar.get(Calendar.SECOND) + se.getSession().getMaxInactiveInterval());
        if(calendar.getTime().before(new Date())){
            securityService.logLogoutSessionLifeTime(se.getSession());
        }
    }
}
