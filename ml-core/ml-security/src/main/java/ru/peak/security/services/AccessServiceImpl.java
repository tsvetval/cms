package ru.peak.security.services;

import com.google.inject.Inject;
import org.eclipse.persistence.dynamic.DynamicEntity;
import ru.ml.core.common.RoleType;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.holders.SecurityHolder;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.folder.MlFolder;
import ru.peak.ml.core.model.page.MlPage;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.security.*;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.model.util.MlUtil;
import ru.peak.ml.core.services.AccessService;

import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 */
public class AccessServiceImpl implements AccessService {

    public static String MAIN_ADMIN_ROLE_TYPE = "MAIN_ADMIN_ROLE";

    @Inject
    private CommonDao commonDao;
    @Inject
    SecurityHolder securityHolder;

    @Override
    public boolean isAnonymous() {
        HttpServletRequest request = GuiceConfigSingleton.inject(HttpServletRequest.class);
        return request.getSession().getAttribute("user") == null;
    }

    public MlUser getCurrentUser(){
        HttpServletRequest request = GuiceConfigSingleton.inject(HttpServletRequest.class);
        if (request.getSession().getAttribute("user") != null){
            return (MlUser)request.getSession().getAttribute("user");
        }
        return null;
    }

    public boolean hasAdminRole(){
        if(isAnonymous()){
            return false;
        }
        return hasAdminRole(getCurrentUser());
    }


    public boolean hasRole(MlUser mlUser, String roleType){
        for (MlRole role : mlUser.getRoles()) {
            if (roleType.equals(role.getRoleType())) {
                return true;
            }
        }
        return false;
    }

    public boolean hasAdminRole(MlUser mlUser) {
        return hasRole(mlUser, MAIN_ADMIN_ROLE_TYPE);
    }

    public List<MlRole> getUserRoles() {
        MlUser user = getCurrentUser();
        List<MlRole> mlRoles;
        if(user == null){
            mlRoles = new ArrayList<>();
            mlRoles.add(getAnonymousRole());
        }else{
            mlRoles = user.getRoles();
        } return mlRoles;
    }

    public MlRole getAnonymousRole(){
        List<MlRole> mlRoles =  commonDao.getTypedQueryWithoutSecurityCheck("select o from MlRole o where o.roleType = :roleType", MlRole.class)
                .setParameter("roleType", RoleType.ANONYMOUS_ROLE).getResultList();
        if(mlRoles.isEmpty()){
            throw new MlServerException("В системе отсутствует роль \"Анонимный пользователь\"");
        }
        return  mlRoles.get(0);
    }

    public MlRole getRole(String roleType){
        TypedQuery<MlRole> query = commonDao.getTypedQueryWithoutSecurityCheck("select o from MlRole o where o.roleType = :roleType", MlRole.class);
        query.setParameter("roleType", roleType);
        return query.getSingleResult();
    }

    public List<MlFolder> getUserRootFolders() {
        Set result = new HashSet();
        return new ArrayList<>(securityHolder.getRootFolders(getUserRoles()));
    }


    public List<? extends MlDynamicEntityImpl> checkAccessFolder(List<? extends MlDynamicEntityImpl> folders) {
        List<MlRole> mlRoles = getUserRoles();
        List<MlFolder> result = new ArrayList<>();
        for(DynamicEntity folder: folders){
            if(securityHolder.checkAccessFolder(mlRoles, (MlFolder) folder)){
                result.add((MlFolder) folder);
            }
        }
        return result;
    }

    @Override
    public Boolean checkAccessFolder(MlFolder folder) {
        List<MlRole> mlRoles = getUserRoles();
        return  securityHolder.checkAccessFolder(mlRoles, (MlFolder) folder);
    }

    public Boolean checkAccessPage(MlPage mlPage){
        List<MlRole> mlRoles = getUserRoles();
        return securityHolder.checkAccessPage(mlRoles, mlPage);
    }

    @Override
    public Boolean checkAccessPageBlock(MlPageBlockBase pageBlock) {
        List<MlRole> mlRoles = getUserRoles();
        if(pageBlock.getMlPage() !=null ){
            return checkAccessPage(pageBlock.getMlPage());
        }else{
            return securityHolder.checkAccessPageBlock(mlRoles,pageBlock);
        }

    }

    public List<MlUtil> checkAccessUtil(List<MlUtil> utils) {
        List<MlRole> mlRoles = getUserRoles();
        List<MlUtil> result = new ArrayList<>();
        for(MlUtil util: utils){
            if(securityHolder.checkAccessUtil(mlRoles, util)){
                result.add(util);
            }
        }
        return result;
    }

    public Boolean checkAccessUtil(MlUtil util) {
        if (hasAdminRole()){
            return true;
        }
        List<MlRole> mlRoles = getUserRoles();
        return securityHolder.checkAccessUtil(mlRoles, util);
    }

    @Override
    public Boolean checkAccessClass(MlClass mlClass, MlClassAccessType accessType) {
        if (hasAdminRole()){
            return true;
        }
        List<MlRole> mlRoles = getUserRoles();
        switch (accessType){
            case CREATE:
                return securityHolder.checkAccessClassCreate(mlRoles, mlClass);
            case READ:
                return securityHolder.checkAccessClassRead(mlRoles, mlClass);
            case UPDATE:
                return securityHolder.checkAccessClassUpdate(mlRoles, mlClass);
            case DELETE:
                return securityHolder.checkAccessClassDelete(mlRoles, mlClass);
            default:
                return false;
        }
    }

    @Override
    public Boolean checkAccessAttr(MlAttr mlAttr, MlAttrAccessType attrAccessType) {
        if (hasAdminRole()){
            return true;
        }
        List<MlRole> mlRoles = getUserRoles();
        switch (attrAccessType){
            case SHOW:
                return securityHolder.checkAccessAttrShow(mlRoles, mlAttr);
            case EDIT:
                return securityHolder.checkAccessAttrEdit(mlRoles, mlAttr);
            default:
                return false;
        }
    }

    @Override
    public List<MlAttr> checkAccessAttrs(List<MlAttr> mlAttrs, MlAttrAccessType attrAccessType) {
        List<MlAttr> result = new ArrayList<>();
        for(MlAttr attr: mlAttrs){
            if(checkAccessAttr(attr,attrAccessType)){
                result.add(attr);
            }
        }
        return result;
    }

    @Override
    public List<String> getAdditionalQueryForClass(MlClass mlClass) {
        List<MlRole> mlRoles = getUserRoles();
        return securityHolder.getAdditionalQueryForClass(mlRoles,mlClass);
    }
}
