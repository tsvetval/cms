package ru.peak.security.services;

import com.google.inject.ImplementedBy;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.security.services.impl.AuthenticateServiceImpl;

/**
 *
 */
@ImplementedBy(AuthenticateServiceImpl.class)
public interface AuthenticateService {
    public MlUser authenticate(String login, String password) throws MlApplicationException;
}
