package ru.peak.security.services.impl;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.model.security.MlSecuritySettings;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.security.dao.MlUserDao;
import ru.peak.security.services.AuthenticateService;

import java.util.Calendar;
import java.util.Date;

/**
 *
 */

public class AuthenticateServiceImpl implements AuthenticateService {

    private static final Logger log = LoggerFactory.getLogger(AuthenticateServiceImpl.class);
    @Inject
    SecuritySettingProvider securitySettingProvider;
    @Inject
    CommonDao commonDao;

    private static final String WRONG_LOGIN_PASSWORD = "Неверное значение пары логин/пароль";
    private static final String USER_BLOCKED = "Пользователь заблокирован";
    private static final String USER_TEMP_BLOCKED = "Пользователь временно заблокирован";
    @Inject
    protected MlUserDao mlUserDao;

    @Override
    @Transactional
    public MlUser authenticate(String login, String password) throws MlApplicationException {
        MlUser result = null;
        MlUser mlUser = mlUserDao.getUserByLogin(login);
        if (mlUser == null) {
            log.info(String.format("Auth FAIL login = [%s], user not found", login));
            throw new MlApplicationException(WRONG_LOGIN_PASSWORD);
        } else {
            MlSecuritySettings securitySettings = securitySettingProvider.getSettingsForUser(mlUser);
            if (mlUser.getLastLogin() != null) {
                // Если дата блокировки больше текущей то блокируем пользователя
                Calendar blockDate = Calendar.getInstance();
                blockDate.setTime(mlUser.getLastLogin());
                blockDate.set(Calendar.DAY_OF_YEAR, (int) (blockDate.get(Calendar.DAY_OF_YEAR) + securitySettings.getBlockAfterNotUse()));
                if (blockDate.getTime().before(new Date())) {
                    mlUser.setBlocked(true);
                }
            }

            if (mlUser.getLastPasswordChange() == null) {
                //
                mlUser.setNeedChangePassword(true);
            } else {
                Calendar changePasswordDate = Calendar.getInstance();
                changePasswordDate.setTime(mlUser.getLastPasswordChange());
                changePasswordDate.set(Calendar.DAY_OF_YEAR, (int) (changePasswordDate.get(Calendar.DAY_OF_YEAR) + securitySettings.getPasswordLifeTime()));
                if (changePasswordDate.getTime().before(new Date())) {
                    mlUser.setNeedChangePassword(true);
                }
            }

            if (mlUser.isTemporalBlocked()) {
                if (mlUser.getTemporalBlockedBefore().before(new Date())) {
                    mlUser.setTemporalBlocked(false);
                    mlUser.setTemporalBlockedBefore(null);
                }
            }

            if (mlUser.getHash().equals(DigestUtils.sha512Hex(password))) {
                if (mlUser.isBlocked() || mlUser.isTemporalBlocked()) {
                    if (mlUser.isBlocked()) {
                        log.info(String.format("Auth - FAIL login = %s, login is blocked", login));
                        commonDao.mergeWithoutSecurityCheck(mlUser, false, false);
                        throw new MlApplicationException(USER_BLOCKED);
                    } else if (mlUser.isTemporalBlocked()) {
                        log.info(String.format("Auth - FAIL login = %s, login is temporal blocked", login));
                        commonDao.mergeWithoutSecurityCheck(mlUser, false, false);
                        throw new MlApplicationException(USER_TEMP_BLOCKED);
                    }
                    mlUser = null;
                } else {
                    mlUser.setFailAuthCount(0L);
                    mlUser.setLastLogin(new Date());
                    result = mlUser;
                    log.info(String.format("Auth - SUCCESS login = %s", mlUser.getLogin()));
                }
            } else {
                log.info(String.format("Auth - FAIL login = %s, wrong password", mlUser.getLogin()));
                Long failCount = mlUser.getFailAuthCount();
                if (failCount == null) {
                    failCount = 0L;
                }
                failCount++;
                mlUser.setFailAuthCount(failCount);
                if (securitySettings.getFailAuthCount().compareTo(failCount) < 0) {
                    mlUser.setTemporalBlocked(true);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(new Date());
                    calendar.set(Calendar.MINUTE, (int) (calendar.get(Calendar.MINUTE) + securitySettings.getBlockTimeAfterFailAuth()));
                    mlUser.setTemporalBlockedBefore(calendar.getTime());
                }
            }

            if (mlUser != null) {
                commonDao.mergeWithoutSecurityCheck(mlUser, false, false);
            }
        }
        return result;

    }
}
