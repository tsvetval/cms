package ru.peak.security.services.impl;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.model.security.MlSecuritySettings;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.security.dao.MlSettingsDao;
import ru.peak.security.dao.MlUserDao;
import ru.peak.security.filter.SecurityFilter;
import ru.peak.security.services.AccessServiceImpl;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpSession;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 */
//TODO
public class SecurityService {
    private static final Logger log = LoggerFactory.getLogger(SecurityService.class);
    @Inject
    protected MlUserDao mlUserDao;
    @Inject
    protected CommonDao commonDao;
    @Inject
    protected MlSettingsDao mlSettingsDao;
    @Inject
    protected AccessServiceImpl accessService;

//    @Inject
//    protected HttpServletRequest request;

    private static final String CYKILLIC_LC_PATTERN = "((.*[а-я])|(.*[А-Я])).{0,255}";
    private static final String CYKILLIC_LC_UC_PATTERN = "((?=.*[а-я])(?=.*[А-Я])).{0,255}";
    private static final String CYKILLIC_LC_UC_NUMBER_PATTERN = "((?=.*\\d)(?=.*[а-я])(?=.*[А-Я])).{0,255}";




    public MlUser getUserById(Long id) {
        return mlUserDao.getUserById(id);
    }


    public MlSecuritySettings getSettingsByUser(MlUser mlUser) {
        if (accessService.hasAdminRole(mlUser)) {
            return getSettingsForAdmin();
        }
        return getSettingsForAllUsers();
    }

    private MlSecuritySettings getSettingsForAllUsers() {
        return mlSettingsDao.getSettings(false);
    }

    private MlSecuritySettings getSettingsForAdmin() {
        return mlSettingsDao.getSettings(true);
    }

    public void detach(MlUser entity) {
        commonDao.detach(entity);
    }


    public void logLogoutSessionLifeTime(HttpSession session) {
        Long idUser = (Long) session.getAttribute(SecurityFilter.USER_KEY_SESSION);
        MlUser mlUser = mlUserDao.getUserById(idUser);
        log.info(String.format("LOGOUT login = %s, session expired ", mlUser.getLogin()));
    }


    public boolean needToChangePassword(MlUser user) {
        return true;
    }

    public Boolean oldPasswordIsOk(MlUser user, String oldPassword) {
        return user.getHash().equals(DigestUtils.sha512Hex(oldPassword));
    }

    public Boolean confirmationIsOk(String password, String confirmation) {
        return password.equals(confirmation);
    }

    public Boolean passwordLengthIsOk(MlUser user, String password) {
        MlSecuritySettings securitySettings = getSettingsByUser(user);
        return password.length() >= securitySettings.getPasswordStrengthLength();
    }

    public Boolean passwordAlphabetIsOk(MlUser user, String password) {
        MlSecuritySettings securitySettings = getSettingsByUser(user);
        String regex = null;
        switch (securitySettings.getPasswordStrengthAlphabet()) {
            case NO_CONTROL:
                return true;
            case CYKILLIC_LC:
                regex = CYKILLIC_LC_PATTERN;
                break;
            case CYKILLIC_LC_UC:
                regex = CYKILLIC_LC_UC_PATTERN;
                break;
            case CYKILLIC_LC_UC_NUMBER:
                regex = CYKILLIC_LC_UC_NUMBER_PATTERN;
                break;
        }

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public Boolean passwordUniqueIsOk(MlUser user, String password) {
        EntityManager entityManager = GuiceConfigSingleton.inject(EntityManager.class);
        long count = (long) entityManager.createNativeQuery("SELECT COUNT(1) " +
                "  FROM \"MlUserHistory\" " +
                "  WHERE \"login\" = ? " +
                "  AND \"password\" = ?;")
                .setParameter(1, user.getLogin())
                .setParameter(2, password)
                .getSingleResult();
        return count == 0;
    }

    @Transactional
    public void changePassword(MlUser user, String password) {
        user.setPassword(password);
        user.set("needChangePassword", false);
        user.setLastPasswordChange(new Date());
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        commonDao.mergeWithoutSecurityCheck(user);
    }

    public Boolean validatePassword(MlUser user, String oldPassword, String password, String confirmation) {
        Boolean oldPasswordIsOk = oldPasswordIsOk(user, oldPassword);
        Boolean confirmationIsOk = confirmationIsOk(password, confirmation);
        Boolean passwordLengthIsOk = passwordLengthIsOk(user, password);
        Boolean passwordAlphabetIsOk = passwordAlphabetIsOk(user, password);
        Boolean passwordUniqueIsOk = passwordUniqueIsOk(user, password);
        return oldPasswordIsOk && confirmationIsOk && passwordLengthIsOk && passwordAlphabetIsOk && passwordUniqueIsOk;
    }

}
