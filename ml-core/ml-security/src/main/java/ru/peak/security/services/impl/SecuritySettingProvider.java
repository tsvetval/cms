package ru.peak.security.services.impl;

import com.google.inject.Inject;
import ru.peak.ml.core.model.security.MlSecuritySettings;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.security.dao.MlSettingsDao;
import ru.peak.security.services.AccessServiceImpl;

/**
 *
 */
public class SecuritySettingProvider {
    @Inject
    protected MlSettingsDao mlSettingsDao;
    @Inject
    protected AccessServiceImpl accessService;


    public MlSecuritySettings getSettingsForUser(MlUser mlUser){
        if (accessService.hasAdminRole(mlUser)) {
            return getSettingsForAdmin();
        }
        return getSettingsForAllUsers();
    }


    private MlSecuritySettings getSettingsForAdmin() {
        return mlSettingsDao.getSettings(true);
    }

    private MlSecuritySettings getSettingsForAllUsers() {
        return mlSettingsDao.getSettings(false);
    }
}
