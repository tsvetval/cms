package ru.ml.jmail.api;

import java.io.Serializable;
import java.util.Set;

/**
 * Базовый интерфейс для модели почтового сообщения
 */
public interface Email extends Serializable {
    public enum EmailType {HTML, TextMultiPart, SIMPLE}

    public EmailType getEmailType();
    /**
     * От кого отправлено сообщение
     *
     * @return эл адрес
     */
    public String getFromAddress();

    /**
     * Кому отправляется сообщение
     *
     * @return список получателей
     */
    public Set<String> getToAddresses();

    /**
     * Кому в копию отправится сообщение
     *
     * @return список получателей в копии письма
     */
    public Set<String> getCcAddresses();

    /**
     * Список получателей письма в скрытой копии
     *
     * @return список получателей в скрытой копии
     */
    public Set<String> getBccAddresses();

    /**
     * Список приложений к письму
     *
     * @return список приложенных файлов к письму
     */
    public Set<EmailAttachment> getAttachments();

    /**
     * Возвращает аттачмент который будет встроен в письмо
     *
     * @return встроенный аттачмент
     */
    public EmailAttachment getInlineAttachment();

    /**
     * Тема сообщения
     *
     * @return тема сообщзения
     */
    public String getSubject();

    /**
     * Тело письма
     *
     * @return тело письма
     */
    public String getBody();
}