package ru.ml.jmail.api;

import javax.activation.DataSource;

/**
 * Интерфейс приложения к почте
 */
public interface EmailAttachment {

    /**
     * Имя приложения
     *
     * @return имя
     */
    public String name();

    /**
     * Дата сорс для приложения
     *
     * @return дата сорс для включения в письмо
     */
    public DataSource buildDataSource();

    /**
     * Тип сообщения
     *
     * @return тип
     */
    public String getMimeType();
}
