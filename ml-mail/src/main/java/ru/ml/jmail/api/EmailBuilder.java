package ru.ml.jmail.api;

import java.util.Collection;

/**
 * Интерфейс билдера сообщения
 */
public interface EmailBuilder {

    public EmailBuilder setEmailType(Email.EmailType emailType);

    /**
     * От кого задается
     *
     * @param address адрес для отправки вида John Smith < j.smith@me.com >
     * @return сам себя для продолжения цепочки
     */
    public EmailBuilder from(String address);

    /**
     * Кому отправляется
     *
     * @param addresses список адресов
     * @return сам себя для продолжения цепочки
     */
    public EmailBuilder to(String... addresses);

    public EmailBuilder to(Collection<String> addresses);
    /**
     * Кому в копию
     *
     * @param addresses список адресов
     * @return сам себя для продолжения цепочки
     */
    public EmailBuilder cc(String... addresses);

    /**
     * Скрытая копия
     *
     * @param addresses список адресов
     * @return сам себя для продолжения цепочки
     */
    public EmailBuilder bcc(String... addresses);

    /**
     * Тема письма
     *
     * @param subject тема письма
     * @return сам себя для продолжения цепочки
     */
    public EmailBuilder withSubject(String subject);

    /**
     * Тело письма
     *
     * @param body тело письма
     * @return сам себя для продолжения цепочки
     */
    public EmailBuilder withBody(String body);

    /**
     * Приложения к письму
     *
     * @param attachments приложения
     * @return сам себя для продолжения цепочки
     */
    public EmailBuilder withAttachment(EmailAttachment... attachments);

    /**
     * Встроенное приложение в сообщение
     *
     * @param attachment приложение
     * @return сам себя
     */
    public EmailBuilder withInlineAttachment(EmailAttachment attachment);

    /**
     * Возвращает сформированное сообщение
     *
     * @return сообщение
     */
    public Email build();
}