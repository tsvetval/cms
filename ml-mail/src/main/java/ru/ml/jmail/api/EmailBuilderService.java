package ru.ml.jmail.api;

import com.google.inject.ImplementedBy;
import ru.ml.jmail.api.impl.EmailBuilderServiceImpl;

/**
 * Сервис получения создателя сообщений
 */
@ImplementedBy(EmailBuilderServiceImpl.class)
public interface EmailBuilderService {

    /**
     * Получение билдера сообщений
     *
     * @return билдер
     */
    public EmailBuilder createBuilder();
}
