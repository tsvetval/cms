package ru.ml.jmail.api;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Дата трансфер объект для сериализации и десериализации
 */
public class EmailDTO implements Email {

    private String fromAddress;
    private Set<String> toAddresses = new HashSet<String>();
    private Set<String> ccAddresses = new HashSet<String>();
    private Set<String> bccAddresses = new HashSet<String>();
    private Set<EmailAttachment> attachments = new HashSet<EmailAttachment>();
    private EmailAttachment inlineAttachment;
    private String subject;
    private String body;
    private EmailType emailType;

    public EmailDTO(Email email) {
        fromAddress = email.getFromAddress();
        toAddresses = email.getToAddresses();
        ccAddresses = email.getCcAddresses();
        bccAddresses = email.getBccAddresses();
        attachments = email.getAttachments();
        subject = email.getSubject();
        body = email.getBody();
        inlineAttachment = email.getInlineAttachment();
    }

    @Override
    public EmailType getEmailType() {
        return emailType;
    }

    /**
     * От кого отправлено сообщение
     *
     * @return эл адрес
     */
    @Override
    public String getFromAddress() {
        return fromAddress;
    }

    /**
     * Кому отправляется сообщение
     *
     * @return список получателей
     */
    @Override
    public Set<String> getToAddresses() {
        return Collections.unmodifiableSet(toAddresses);
    }

    /**
     * Кому в копию отправится сообщение
     *
     * @return список получателей в копии письма
     */
    @Override
    public Set<String> getCcAddresses() {
        return Collections.unmodifiableSet(ccAddresses);
    }

    /**
     * Список получателей письма в скрытой копии
     *
     * @return список получателей в скрытой копии
     */
    @Override
    public Set<String> getBccAddresses() {
        return Collections.unmodifiableSet(bccAddresses);
    }

    /**
     * Список приложений к письму
     *
     * @return список приложенных файлов к письму
     */
    @Override
    public Set<EmailAttachment> getAttachments() {
        return Collections.unmodifiableSet(attachments);
    }

    /**
     * Возвращает аттачмент который будет встроен в письмо
     *
     * @return встроенный аттачмент
     */
    @Override
    public EmailAttachment getInlineAttachment() {
        return inlineAttachment;
    }

    /**
     * Тема сообщения
     *
     * @return тема сообщзения
     */
    @Override
    public String getSubject() {
        return subject;
    }

    /**
     * Тело письма
     *
     * @return тело письма
     */
    @Override
    public String getBody() {
        return body;
    }
}
