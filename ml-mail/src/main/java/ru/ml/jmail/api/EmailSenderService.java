package ru.ml.jmail.api;

import com.google.inject.ImplementedBy;
import ru.ml.jmail.api.impl.EmailSenderServiceImpl;

import java.util.Collection;

/**
 * Интерфейс сервиса для отправки сообщений на почту
 */
@ImplementedBy(EmailSenderServiceImpl.class)
public interface EmailSenderService {

    /**
     * Отправляет сообщение на почту
     *
     * @param emails сообщение
     */
    public void sendMessages(Collection<Email> emails);
}
