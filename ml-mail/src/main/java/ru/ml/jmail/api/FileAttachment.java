package ru.ml.jmail.api;

import javax.activation.DataSource;
import javax.activation.FileDataSource;
import java.io.File;
import java.io.Serializable;

/**
 * Приложение типа файл
 */
public class FileAttachment implements EmailAttachment, Serializable {

    /**
     * Аттачмент
     */
    private String filePath;

    /**
     * Имя приложения
     */
    private String attachmentName;

    /**
     * Конструктор
     *
     * @param attachmentName имя приложения
     * @param attachmentFile файл приложения
     */
    public FileAttachment(String attachmentName, File attachmentFile) {
        // сорс
        this.filePath = attachmentFile.getAbsolutePath();
        // имя приложения
        this.attachmentName = attachmentName;
    }

    /**
     * Имя приложения
     *
     * @return имя
     */
    @Override
    public String name() {
        return attachmentName;
    }

    /**
     * Дата сорс для приложения
     *
     * @return дата сорс для включения в письмо
     */
    @Override
    public DataSource buildDataSource() {
        return new FileDataSource(new File(filePath));
    }

    @Override
    public String getMimeType() {
        return null;
    }
}
