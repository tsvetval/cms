package ru.ml.jmail.api;

/**
 *
 */
public class MailProperties {
    /**
     * smtp.gmail.com
     */
    public final static String HOST = "jmail.server";
    public final static String PORT = "jmail.port";
    /**
     * использовать идентификацию
     * true/false
     */
    public final static String USE_AUTH = "jmail.auth";
    /**
     * username
     */
    public final static String USER = "jmail.username";
    public final static String PASSWORD = "jmail.password";
    public final static String USE_SSL = "jmail.useSSL";


    /**
     * Ссылка на логотип   "http://somesite.ru/blablabla/logo.png"
     */
    public final static String LOGO = "mail.logo";

    public final static String TITLE = "mail.title";


}
