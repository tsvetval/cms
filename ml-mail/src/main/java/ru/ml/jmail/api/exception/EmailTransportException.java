package ru.ml.jmail.api.exception;

/**
 * Класс ошибки транспорта при отправке
 */
public class EmailTransportException extends RuntimeException {

    public EmailTransportException(String message, Exception cause) {
        super(message, cause);
    }
}