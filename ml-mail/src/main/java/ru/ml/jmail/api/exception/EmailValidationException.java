package ru.ml.jmail.api.exception;

/**
 * Базовый класс ошибки валидации сообщения
 */
public class EmailValidationException extends Exception {

    public EmailValidationException() {
    }

    public EmailValidationException(String message) {
        super(message);
    }

    public EmailValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
