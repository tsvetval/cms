package ru.ml.jmail.api.impl;

import ru.ml.jmail.api.Email;
import ru.ml.jmail.api.EmailAttachment;
import ru.ml.jmail.api.EmailBuilder;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Имплементация сервиса создания сообщений
 */
public class EmailBuilderImpl implements EmailBuilder {
    
    private EmailImpl email = new EmailImpl();

    @Override
    public EmailBuilder setEmailType(Email.EmailType emailType) {
        email.setEmailType(emailType);
        return this;
    }

    /**
     * От кого задается
     *
     * @param address адрес для отправки вида John Smith < j.smith@me.com >
     * @return сам себя для продолжения цепочки
     */
    @Override
    public EmailBuilder from(String address) {
        email.setFromAddress(address);
        return this;
    }

    /**
     * Кому отправляется
     *
     * @param addresses список адресов
     * @return сам себя для продолжения цепочки
     */
    @Override
    public EmailBuilder to(String... addresses) {
        return to(Arrays.asList(addresses));
    }

    @Override
    public EmailBuilder to(Collection<String> addresses) {
        Set<String> to = new HashSet<String>(addresses);
        email.setToAddresses(to);
        return this;
    }

    /**
     * Кому в копию
     *
     * @param addresses список адресов
     * @return сам себя для продолжения цепочки
     */
    @Override
    public EmailBuilder cc(String... addresses) {
        Set<String> cc = new HashSet<String>(Arrays.asList(addresses));
        email.setCcAddresses(cc);

        return this;
    }

    /**
     * Скрытая копия
     *
     * @param addresses список адресов
     * @return сам себя для продолжения цепочки
     */
    @Override
    public EmailBuilder bcc(String... addresses) {
        Set<String> bcc = new HashSet<String>(Arrays.asList(addresses));
        email.setBccAddresses(bcc);

        return this;
    }

    /**
     * Тема письма
     *
     * @param subject тема письма
     * @return сам себя для продолжения цепочки
     */
    @Override
    public EmailBuilder withSubject(String subject) {
        email.setSubject(subject);

        return this;
    }

    /**
     * Тело письма
     *
     * @param body тело письма
     * @return сам себя для продолжения цепочки
     */
    @Override
    public EmailBuilder withBody(String body) {
        email.setBody(body);

        return this;
    }

    /**
     * Приложения к письму
     *
     * @param attachments приложения
     * @return сам себя для продолжения цепочки
     */
    @Override
    public EmailBuilder withAttachment(EmailAttachment... attachments) {
        Set<EmailAttachment> att = new HashSet<EmailAttachment>(Arrays.asList(attachments));
        email.setAttachments(att);

        return this;
    }

    @Override
    public EmailBuilder withInlineAttachment(EmailAttachment attachment) {
        email.setInlineAttachment(attachment);

        return this;
    }

    /**
     * Возвращает сформированное сообщение
     *
     * @return сообщение
     */
    @Override
    public Email build() {
        return email;
    }
}
