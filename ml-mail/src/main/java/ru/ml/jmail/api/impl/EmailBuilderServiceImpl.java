package ru.ml.jmail.api.impl;

import com.google.inject.ImplementedBy;
import ru.ml.jmail.api.EmailBuilder;
import ru.ml.jmail.api.EmailBuilderService;

/**
 * Сервис получения билдера
 */
//@Component(immediate = true, name = "EmailBuilderService ")
//@Service

public class EmailBuilderServiceImpl implements EmailBuilderService {

    /**
     * Получение билдера сообщений
     *
     * @return билдер
     */
    @Override
    public EmailBuilder createBuilder() {
        return new EmailBuilderImpl();
    }
}
