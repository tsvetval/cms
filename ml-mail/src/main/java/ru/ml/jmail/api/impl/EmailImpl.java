package ru.ml.jmail.api.impl;

import ru.ml.jmail.api.Email;
import ru.ml.jmail.api.EmailAttachment;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Имплементация бина сообщений
 */
public class EmailImpl implements Email {

    private String fromAddress;
    private Set<String> toAddresses = new HashSet<String>();
    private Set<String> ccAddresses = new HashSet<String>();
    private Set<String> bccAddresses = new HashSet<String>();
    private Set<EmailAttachment> attachments = new HashSet<EmailAttachment>();
    private EmailAttachment inlineAttachment;
    private String subject;
    private String body;
    private EmailType emailType = EmailType.SIMPLE;

    @Override
    public EmailType getEmailType() {
        return emailType;
    }

    /**
     * От кого отправлено сообщение
     *
     * @return эл адрес
     */
    @Override
    public String getFromAddress() {
        return fromAddress;
    }

    /**
     * Кому отправляется сообщение
     *
     * @return список получателей
     */
    @Override
    public Set<String> getToAddresses() {
        return Collections.unmodifiableSet(toAddresses);
    }

    /**
     * Кому в копию отправится сообщение
     *
     * @return список получателей в копии письма
     */
    @Override
    public Set<String> getCcAddresses() {
        return Collections.unmodifiableSet(ccAddresses);
    }

    /**
     * Список получателей письма в скрытой копии
     *
     * @return список получателей в скрытой копии
     */
    @Override
    public Set<String> getBccAddresses() {
        return Collections.unmodifiableSet(bccAddresses);
    }

    /**
     * Список приложений к письму
     *
     * @return список приложенных файлов к письму
     */
    @Override
    public Set<EmailAttachment> getAttachments() {
        return Collections.unmodifiableSet(attachments);
    }

    @Override
    public EmailAttachment getInlineAttachment() {
        return inlineAttachment;
    }

    /**
     * Тема сообщения
     *
     * @return тема сообщзения
     */
    @Override
    public String getSubject() {
        return subject;
    }

    /**
     * Тело письма
     *
     * @return тело письма
     */
    @Override
    public String getBody() {
        return body;
    }

    public void setEmailType(EmailType emailType) {
        this.emailType = emailType;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public void setToAddresses(Set<String> toAddresses) {
        this.toAddresses = toAddresses;
    }

    public void setCcAddresses(Set<String> ccAddresses) {
        this.ccAddresses = ccAddresses;
    }

    public void setBccAddresses(Set<String> bccAddresses) {
        this.bccAddresses = bccAddresses;
    }

    public void setAttachments(Set<EmailAttachment> attachments) {
        this.attachments = attachments;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setInlineAttachment(EmailAttachment inlineAttachment) {
        this.inlineAttachment = inlineAttachment;
    }
}
