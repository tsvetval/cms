package ru.ml.jmail.api.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.*;
import java.util.Arrays;

public class EmailMockTransport extends Transport {

    private static final Logger log = LoggerFactory.getLogger(EmailMockTransport.class);

    public EmailMockTransport(Session session, URLName urlname) {
        super(session, urlname);
    }

    @Override
    public boolean isConnected() {
        return true;
    }

    @Override
    public void connect() throws MessagingException {
        // noop
    }

    @Override
    public void connect(String host, String user, String password) throws MessagingException {
        // noop
    }

    @Override
    public void connect(String user, String password) throws MessagingException {
        // noop
    }

    @Override
    public void connect(String host, int port, String user, String password) throws MessagingException {
        // noop
    }

    @Override
    public void sendMessage(Message msg, Address[] addresses) throws MessagingException {
        log.info("Send Message {} to {}", msg, Arrays.toString(addresses));
    }
}
