package ru.ml.jmail.api.impl;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.jmail.api.Email;
import ru.ml.jmail.api.EmailAttachment;
import ru.ml.jmail.api.EmailSenderService;
import ru.ml.jmail.api.MailProperties;
import ru.ml.utils.collections.Closure;
import ru.peak.ml.prop.MlProperties;

import javax.activation.DataSource;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeUtility;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import static ru.ml.utils.collections.Collections.convert;

/**
 * Имплементация сервиса отправки почты
 */
//@Component(immediate = true, name = "Email Sender Service ")
//@Service

public class EmailSenderServiceImpl implements EmailSenderService {

    private static final Logger log = LoggerFactory.getLogger(EmailSenderService.class);

    @Inject
    MlProperties properties;

    /**
     * Отправляет сообщение на почту
     *
     * @param emails сообщение
     */
    @Override
    public void sendMessages(Collection<Email> emails) {
        try {
            Collection<org.apache.commons.mail.Email> messages = convert(emails, new Closure<org.apache.commons.mail.Email, Email>() {
                @Override
                public org.apache.commons.mail.Email exec(Email arg) {
                    return createMessage(arg);
                }
            });

            send(messages);
        } catch (MessagingException e) {
            log.warn("Send email failed with exception {}", e.getLocalizedMessage(), e);
        }
    }

    /**
     * Возвращает ошибки отправки сообщений
     *
     * @param mimeMessages коллекция сообщений
     * @return ошибки, возникшие при отправке
     * @throws MessagingException при исключительной ситтуации
     */
    protected Map<org.apache.commons.mail.Email, String> send(Collection<org.apache.commons.mail.Email> mimeMessages) throws MessagingException {

        Map<org.apache.commons.mail.Email, String> failedMessages = new LinkedHashMap<>();

        for (org.apache.commons.mail.Email mimeMessage : mimeMessages) {
            try {
                if (mimeMessage.getSentDate() == null) {
                    mimeMessage.setSentDate(new Date());
                }


                if (useAuth()) {
                    mimeMessage.setAuthenticator(getAuthentication());
                }

                mimeMessage.setHostName(properties.getProperty(MailProperties.HOST, "smtp.gmail.com"));

                mimeMessage.send();
            } catch (EmailException e) {
                log.warn("Error while sending message to {}", mimeMessage.getToAddresses(), e);
                failedMessages.put(mimeMessage, e.getLocalizedMessage());
            }
        }

        return failedMessages;
    }

    /**
     * Создает сообщение
     *
     * @param email бин сообщения
     * @return сообщение
     */
    private org.apache.commons.mail.Email createHtmlMessage(Email email) {
        HtmlEmail message = new HtmlEmail();
        try {
            String logoImage = properties.getProperty(MailProperties.LOGO, "");
            String title = email.getSubject();
            if (title == null || title.isEmpty()) {
                title = properties.getProperty(MailProperties.TITLE, "title");
            }

            String htmlTemplate = "<table width=\"100%\" cellspacing=\"10\" cellpadding=\"20\">\n" +
                    "    <tr>\n" +
                    "        <td valign=\"top\" align=\"center\" width=\"160\"><img src=\"cid:{0}\" /></td>\n" +
                    "        <td>\n" +
                    "            <font face=\"Arial, Helvetica, Verdana, sans-serif\">                \n" +
                    "                <table cellpadding=\"5\" cellspacing=\"0\" width=\"100%\">\n" +
                    "                    <tr>\n" +
                    "                        <td bgcolor=\"#FEFCF3\" width=\"100%\" valign=\"middle\" align=\"center\">\n" +
                    "                            <font size=\"1\">\n" +
                    "                                <nobr><h2>{1}</h2></nobr>\n" +
                    "                            </font>\n" +
                    "                        </td>\n" +
                    "                    <tr>\n" +
                    "                    <tr>\n" +
                    "                         <td>\n" +
                    "                            <br /><br />\n" +
                    "                            {2}\n" +
                    "                         </td>\n" +
                    "                    </tr>\n" +
                    "                 </table>\n" +
                    "            </font>\n" +
                    "        </td>\n" +
                    "    </tr>\n" +
                    "</table>";

            String inlineAttachment = email.getBody();
            if (email.getInlineAttachment() != null) {
                try {
                    DataSource attachmentDS = email.getInlineAttachment().buildDataSource();
                    String cid = message.embed(attachmentDS, attachmentDS.getName());
                    inlineAttachment = MessageFormat.format(inlineAttachment, cid);
                } catch (EmailException e) {
                    log.warn("Error while attach inline image");
                    inlineAttachment = "";
                }
            } else {
                inlineAttachment = email.getBody();
            }

            String cid = "";
            if (logoImage != null && !logoImage.isEmpty()) {
                cid = message.embed(new URL(logoImage), title);
            }
            message.setSubject(MimeUtility.encodeText(title, "UTF-8", "B"));
            message.setCharset("UTF-8");
            message.setHtmlMsg(MessageFormat.format(htmlTemplate, cid, email.getSubject(), inlineAttachment));
            message.setTextMsg("Your email client does not support HTML messages, sorry");

            message.setFrom(email.getFromAddress());
            if (message.getFromAddress() == null) {
                message.setFrom(properties.getProperty(MailProperties.USER, "noreply@yandex.ru"));
            }

            message.setTo(convert(email.getToAddresses(), new Closure<InternetAddress, String>() {
                @Override
                public InternetAddress exec(String arg) {
                    try {
                        return new InternetAddress(arg);
                    } catch (Exception e) {
                        return null;
                    }
                }
            }));
            if (!email.getCcAddresses().isEmpty()) {
                message.setCc(convert(email.getCcAddresses(), new Closure<InternetAddress, String>() {
                    @Override
                    public InternetAddress exec(String arg) {
                        try {
                            return new InternetAddress(arg);
                        } catch (Exception e) {
                            return null;
                        }
                    }
                }));
            }

            if (!email.getBccAddresses().isEmpty()) {

                message.setBcc(convert(email.getBccAddresses(), new Closure<InternetAddress, String>() {
                    @Override
                    public InternetAddress exec(String arg) {
                        try {
                            return new InternetAddress(arg);
                        } catch (Exception e) {
                            return null;
                        }
                    }
                }));
            }
            //message.setSSL(useTLS());
            message.setSSLOnConnect(useTLS());
            message.setSmtpPort(getPort());
            message.setSocketConnectionTimeout(5000);
            message.setSocketTimeout(25000);

            message.addHeader("X-Mailer", "Mail API");

            for (EmailAttachment attachment : email.getAttachments()) {
                message.attach(attachment.buildDataSource(), MimeUtility.encodeText(attachment.name(), "UTF-8", "B"), "");
            }
        } catch (EmailException e) {
            log.error("Error while building mail to send {}", e.getLocalizedMessage(), e);
        } catch (MalformedURLException e) {
            log.error("Error while creating url {}", e.getLocalizedMessage(), e);
        } catch (UnsupportedEncodingException e) {
            log.error("Unsupported encoding {}", e.getLocalizedMessage(), e);
        }

        return message;
    }

    /**
     * Создает сообщение
     *
     * @param email бин сообщения
     * @return сообщение
     */
    private org.apache.commons.mail.Email createSimpleMessage(Email email) {
        SimpleEmail message = new SimpleEmail();
        try {

            message.setSubject(MimeUtility.encodeText(email.getSubject(), "UTF-8", "B"));
            message.setCharset("UTF-8");
            message.setMsg(email.getBody());

            if (email.getFromAddress() == null) {
                message.setFrom(properties.getProperty(MailProperties.USER, "noreply@yandex.ru"));
            } else {
                message.setFrom(email.getFromAddress());
            }

            message.setTo(convert(email.getToAddresses(), new Closure<InternetAddress, String>() {
                @Override
                public InternetAddress exec(String arg) {
                    try {
                        return new InternetAddress(arg);
                    } catch (Exception e) {
                        return null;
                    }
                }
            }));
            if (!email.getCcAddresses().isEmpty()) {
                message.setCc(convert(email.getCcAddresses(), new Closure<InternetAddress, String>() {
                    @Override
                    public InternetAddress exec(String arg) {
                        try {
                            return new InternetAddress(arg);
                        } catch (Exception e) {
                            return null;
                        }
                    }
                }));
            }

            if (!email.getBccAddresses().isEmpty()) {

                message.setBcc(convert(email.getBccAddresses(), new Closure<InternetAddress, String>() {
                    @Override
                    public InternetAddress exec(String arg) {
                        try {
                            return new InternetAddress(arg);
                        } catch (Exception e) {
                            return null;
                        }
                    }
                }));
            }
            //message.setSSL(useTLS());
            message.setSSLOnConnect(useTLS());
            message.setSmtpPort(getPort());
            message.setSocketConnectionTimeout(5000);
            message.setSocketTimeout(25000);

            message.addHeader("X-Mailer", "Mail API");

        } catch (EmailException e) {
            log.error("Error while building mail to send {}", e.getLocalizedMessage(), e);
        } catch (UnsupportedEncodingException e) {
            log.error("Unsupported encoding {}", e.getLocalizedMessage(), e);
        }

        return message;
    }


    private org.apache.commons.mail.Email createMessage(Email email) {
        org.apache.commons.mail.Email result = null;
        switch (email.getEmailType()) {
            case HTML:
                result = createHtmlMessage(email);
                break;
            case TextMultiPart:
                break;
            case SIMPLE:
                result = createSimpleMessage(email);
                break;
            default:
                return null;
        }
        return result;
    }


    private DefaultAuthenticator getAuthentication() {
        if (useAuth()) {
            return new DefaultAuthenticator(properties.getProperty(MailProperties.USER, "yoho@gmail.com"),
                    properties.getProperty(MailProperties.PASSWORD, "secretno"));
        }

        return null;
    }

    private boolean useTLS() {
        return "true".equals(properties.getProperty(MailProperties.USE_SSL, "true"));
    }

    private int getPort() {
        return Integer.parseInt(properties.getProperty(MailProperties.PORT, "true"));
        //return useTLS() ? 465 : 25;
    }

    private boolean useAuth() {
        return "true".equals(properties.getProperty(MailProperties.USE_AUTH, "true"));
    }
}
