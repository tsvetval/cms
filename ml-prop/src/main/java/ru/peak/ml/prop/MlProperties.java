package ru.peak.ml.prop;

import com.google.inject.ImplementedBy;
import ru.peak.ml.prop.impl.MlPropertiesImpl;

@ImplementedBy(MlPropertiesImpl.class)
public interface MlProperties {
    String getProperty(Property property);
    String getProperty(String property, String defaultValue);
    String getProperty(String property);
    void setProperty(Property property, String value);
    public void init();
}
