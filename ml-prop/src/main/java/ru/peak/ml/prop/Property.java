package ru.peak.ml.prop;

public enum Property {

    USE_HOME_PAGE_URL("useHomePageUrl","useHomePageUrl", "false"),

    AUTH_PAGE("auth.page","authentification page url", "/console/auth"),

    TEMPLATE_PATH("templatePath",
            "Path to templates",
            System.getProperty("catalina.base") + "/webapps/ml-arm/WEB-INF/templates"),

    STATIC_PATH("staticPath",
            "Path to static resources",
            System.getProperty("catalina.base") + "/webapps/ml-arm/public"),

    ICON_PATH("iconPath",
            "Path to icons",
            System.getProperty("catalina.base") + "/webapps/ml-arm/public/icons"),

    WEBAPPS_PATH("iconShortPath",
            "Path to icons applications scope",
            System.getProperty("catalina.base") + "/webapps"),

    ICON_PATH_RELATIVE("iconPath",
            "Path to icons",
            "/ml-arm/public/icons"),


    /*
        UPLOAD_PATH("uploadPath",
                "Path for upload files",
                "../../../../temp"),

        FILE_PATH("filePath",
                "Path for permanent storage of uploaded files",
                null),
    */
    MODE("mode",
            "Working mode(production/development)",
            "development");


    private String name;
    private String description;
    private String defaultValue;

    private <V> Property(String name, String description, String defaultValue) {
        this.name = name;
        this.description = description;
        this.defaultValue = defaultValue;
    }

    public static String getTempDir() {
        return System.getProperty("java.io.tmpdir");
    }


    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getDefaultValue() {
        return defaultValue;
    }
}
