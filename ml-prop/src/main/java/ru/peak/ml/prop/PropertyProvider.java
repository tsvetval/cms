package ru.peak.ml.prop;

import java.util.Properties;

/**
 * Created by d_litovchenko on 26.03.15.
 */
public interface PropertyProvider {
    Properties getProperties();
}
