package ru.peak.ml.prop.impl;

import ru.peak.ml.prop.PropertyProvider;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by d_litovchenko on 26.03.15.
 */
public abstract class FilePropertiesProvider implements PropertyProvider {
    public abstract String getPropertiesFilePath() ;

    @Override
    public Properties getProperties() {
        Properties properties = new Properties();
        InputStream inputStream = getClass().getResourceAsStream(getPropertiesFilePath());
        if (inputStream != null) {
            try {
                properties.load(inputStream);
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return properties;
    }
}
