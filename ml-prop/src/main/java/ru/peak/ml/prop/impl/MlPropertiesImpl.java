package ru.peak.ml.prop.impl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import ru.peak.ml.prop.MlProperties;
import ru.peak.ml.prop.Property;
import ru.peak.ml.prop.PropertyProvider;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

@Singleton
public class MlPropertiesImpl implements MlProperties {
    Set<PropertyProvider> propertyProviders;
    private Map<String, String> map;

    @Inject
    public MlPropertiesImpl(Set<PropertyProvider> propertyProviders) {
        this.propertyProviders = propertyProviders;
        init();
    }

    public void init() {
        map = new HashMap<>();
        for(PropertyProvider propertyProvider: propertyProviders){
            fillFromPP(map, propertyProvider);
        }
        checkForProduction();
    }

    private void fillFromPP(Map<String, String> map, PropertyProvider propertyProvider) {
        Properties properties = propertyProvider.getProperties();
        for (Object o : properties.keySet()) {
            map.put(o.toString(), properties.getProperty(o.toString()));
        }
    }

    private void checkForProduction() {
        if(!map.containsKey(Property.MODE.getName()) ||
                !map.get(Property.MODE.getName()).equals("development")){
            map.remove(Property.TEMPLATE_PATH.getName());
            map.remove(Property.STATIC_PATH.getName());
        }
    }

    @Override
    public String getProperty(Property property) {
        String value = map.get(property.getName());
        return value != null ? value : property.getDefaultValue();
    }

    @Override
    public String getProperty(String property, String defaultValue) {
        String value = map.get(property);
        return value != null ? value : defaultValue;
    }

    @Override
    public String getProperty(String property) {
        String value = map.get(property);
        return value;
    }

    public void setProperty(Property property, String value) {
        map.put(property.getName(), value);
    }
}
