package ru.peak.ml.prop.impl;

/**
 * Created by d_litovchenko on 26.03.15.
 */
public class ProjectPropertiesFileProvider extends FilePropertiesProvider {
    @Override
    public String getPropertiesFilePath() {
        return "/project.properties";
    }
}
