package ru.peak.ml.template;

import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.prop.MlProperties;
import ru.peak.ml.prop.Property;
import ru.peak.ml.prop.impl.MlPropertiesImpl;
import ru.peak.ml.template.engine.GTTemplateRepo;
import ru.peak.ml.template.engineImpl.GTTemplateRepoBuilder;

import java.io.File;

public class RepoSingleton {
    private static volatile RepoSingleton instance;
    private GTTemplateRepo templateRepo;
    private String templatePath;

    private RepoSingleton() {
        MlProperties properties = GuiceConfigSingleton.inject(MlProperties.class);
        templatePath = properties.getProperty(Property.TEMPLATE_PATH);
        templateRepo = new GTTemplateRepoBuilder()
                .withTemplateRootFolder(new File(templatePath))
                .build();
    }

    public static RepoSingleton getInstance(){
        RepoSingleton localInstance = instance;
        if(localInstance == null){
            synchronized (RepoSingleton.class){
                localInstance = instance;
                if(localInstance == null){
                    instance = new RepoSingleton();
                    localInstance = instance;
                }
            }
        }
        return localInstance;
    }

    public GTTemplateRepo getTemplateRepo() {
        return templateRepo;
    }

    public String getTemplateDirPath() {
        return templatePath;
    }
}
