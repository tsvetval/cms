package ru.peak.ml.template;

import java.util.Map;

public interface TemplateEngine {
    public String renderTemplate(String name, Map<String, Object> data);
    public String renderStringTemplate(String template, Map<String, Object> data);
}
