package ru.peak.ml.template.engine;

public class GTTemplateLocationString extends GTTemplateLocation {
    String template;
    public GTTemplateLocationString(String relativePath) {
        super("test");
        template = relativePath;
    }

    @Override
    public String readSource() {
        return template;
    }

    @Override
    public String toString() {
        return "GTTemplateLocation{" +
                "relativePath='" + relativePath + '\'' +
                '}';
    }
}
