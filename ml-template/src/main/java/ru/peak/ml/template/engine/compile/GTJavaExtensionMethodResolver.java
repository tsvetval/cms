package ru.peak.ml.template.engine.compile;

public interface GTJavaExtensionMethodResolver {

    public Class findClassWithMethod(String methodName);

}
