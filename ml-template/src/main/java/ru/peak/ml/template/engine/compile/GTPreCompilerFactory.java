package ru.peak.ml.template.engine.compile;

import ru.peak.ml.template.engine.GTTemplateRepo;

// Must be implemented by the framework to return the correct ru.peak.ml.prop.impl of the GTPreCompiler
public interface GTPreCompilerFactory {
    public GTPreCompiler createCompiler(GTTemplateRepo templateRepo);
}
