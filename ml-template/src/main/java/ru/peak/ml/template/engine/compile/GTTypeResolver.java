package ru.peak.ml.template.engine.compile;

public interface GTTypeResolver {

    public byte[] getTypeBytes(String name);
    
    public boolean isApplicationClass(String className);
}
