package ru.peak.ml.template.engine.exceptions;

public class GTTemplateNotFound extends GTException {

    public final String queryPath;

    public GTTemplateNotFound(String queryPath) {
        super("Cannot find template file " + queryPath);
        this.queryPath = queryPath;
    }
}
