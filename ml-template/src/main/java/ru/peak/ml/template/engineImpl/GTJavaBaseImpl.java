package ru.peak.ml.template.engineImpl;

import ru.peak.ml.template.engine.GTGroovyBase;
import ru.peak.ml.template.engine.GTJavaBase;
import ru.peak.ml.template.engine.GTTemplateLocation;

public abstract class GTJavaBaseImpl extends GTJavaBase {

    public GTJavaBaseImpl(Class<? extends GTGroovyBase> groovyClass, GTTemplateLocation templateLocation) {
        super(groovyClass, templateLocation);
    }

    @Override
    protected void _renderTemplate() {

    }

    @Override
    public Class getRawDataClass() {
        return null;
    }

    @Override
    public String convertRawDataToString(Object rawData) {
        return null;
    }

    @Override
    public String escapeHTML(String s) {
        return org.apache.commons.lang.StringEscapeUtils.escapeHtml(s).replaceAll("'","&#39;");
    }

    @Override
    public String escapeXML(String s) {
        return s;
    }

    @Override
    public String escapeCsv(String s) {
        return s;
    }

    @Override
    public boolean validationHasErrors() {
        return false;
    }

    @Override
    public boolean validationHasError(String key) {
        return false;
    }

    @Override
    protected String resolveMessage(Object key, Object[] args) {
        return null;
    }

    @Override
    public Object cacheGet(String key) {
        return null;
    }

    @Override
    public void cacheSet(String key, Object data, String duration) {

    }
}
