package ru.peak.ml.template.impl;

import groovy.text.Template;
import ru.peak.ml.template.RepoSingleton;
import ru.peak.ml.template.TemplateEngine;
import ru.peak.ml.template.engine.*;
import ru.peak.ml.template.engineImpl.GTJavaBaseImpl;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

public class TemplateEngineImpl implements TemplateEngine{
    @Override
    public String renderTemplate(String name, Map<String, Object> data) {
        RepoSingleton repoSingleton = RepoSingleton.getInstance();
        GTTemplateRepo templateRepo = repoSingleton.getTemplateRepo();
        GTTemplateLocation templateLocation;
        try {
            // Try to create location real instance (for make isModified check work for templates)
            URL templateUrl = new File(new File(repoSingleton.getTemplateDirPath()), name).toURI().toURL();
            templateLocation = new GTTemplateLocationReal(name, templateUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            templateLocation = new GTTemplateLocation(name);
        }
        GTJavaBase template = templateRepo.getTemplateInstance(templateLocation);
        template.renderTemplate(data);
        return template.getAsString();
    }

    @Override
    public String renderStringTemplate(String template, Map<String, Object> data) {
        RepoSingleton repoSingleton = RepoSingleton.getInstance();
        GTTemplateRepo templateRepo = repoSingleton.getTemplateRepo();
        GTTemplateLocationString templateLocation = new GTTemplateLocationString(template);
        /*try {
            // Try to create location real instance (for make isModified check work for templates)
            //URL templateUrl = new File(new File(repoSingleton.getTemplateDirPath()), name).toURI().toURL();
            templateLocation = new GTTemplateLocationString(template);
        } catch (Exception e) {
            e.printStackTrace();
            //templateLocation = new GTTemplateLocation(name);
        }*/
        GTJavaBase templatse = templateRepo.getTemplateInstance(templateLocation);
        templatse.renderTemplate(data);
        return templatse.getAsString();
    }
}
