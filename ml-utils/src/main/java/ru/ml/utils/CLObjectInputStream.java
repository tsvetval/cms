package ru.ml.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;


public class CLObjectInputStream extends ObjectInputStream {
    private ClassLoader classLoader;

    public CLObjectInputStream(InputStream in, ClassLoader classLoader) throws IOException {
        super(in);
        this.classLoader = classLoader;
    }


    protected CLObjectInputStream(ClassLoader classLoader) throws IOException, SecurityException {
        super();
        this.classLoader = classLoader;
    }


    @Override
    protected Class<?> resolveClass(ObjectStreamClass desc) throws IOException, ClassNotFoundException {
        try {
            return super.resolveClass(desc);
        } catch (ClassNotFoundException cnfe) {
            return classLoader.loadClass(desc.getName());
        }
    }
}
