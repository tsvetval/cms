package ru.ml.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.utils.collections.CompositeEnumeration;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;

/**
 * Данный класс лоадер используется для объекдинения
 * нескольких класслоадеров в один
 */
public class CompositeClassLoader extends ClassLoader {

    private static final Logger log = LoggerFactory.getLogger(CompositeClassLoader.class);

    private List<ClassLoader> classLoaders = new ArrayList<ClassLoader>();

    /**
     * Конструктор класслоадера для двух и более класслоадеров
     *
     * @param loaders - список класслоадеров
     */
    public CompositeClassLoader(ClassLoader... loaders) {
        classLoaders.addAll(Arrays.asList(loaders));
    }

    /**
     * Конструктор для класслоадера по колекции классов
     *
     * @param loaders коллекция класслоадеров
     */
    public CompositeClassLoader(Collection<ClassLoader> loaders) {
        classLoaders.addAll(loaders);
    }

    /**
     * Получение списка класслоадеров которые данный класслоадер инкапсулирует
     *
     * @return коллекцию класслоадеров
     */
    public List<ClassLoader> getClassLoaders() {
        return classLoaders;
    }

    /**
     * @see ClassLoader#clearAssertionStatus()
     */
    @Override
    public synchronized void clearAssertionStatus() {
        for (ClassLoader classLoader : getClassLoaders()) {
            classLoader.clearAssertionStatus();
        }
    }

    /**
     * @see ClassLoader#getResource(String)
     */
    @Override
    public URL getResource(String name) {
        for (ClassLoader classLoader : getClassLoaders()) {
            log.debug("Attempting getResource({}) on {}", name, classLoader.toString());
            URL resource = classLoader.getResource(name);
            if (resource != null) {
                log.debug("Found resource({}) on {}", name, classLoader.toString());
                return resource;
            }
        }
        return null;
    }

    @Override
    public InputStream getResourceAsStream(String name) {
        for (ClassLoader classLoader : getClassLoaders()) {
            log.debug("Attempting getResource({}) on {}", name, classLoader.toString());
            InputStream stream = classLoader.getResourceAsStream(name);
            if (stream != null) {
                log.debug("Found resource({}) on {}", name, classLoader.toString());
                return stream;
            }
        }
        return null;
    }

    @Override
    public Enumeration<URL> getResources(String name) throws IOException {
        List<Enumeration<URL>> enumerations = new ArrayList<Enumeration<URL>>(getClassLoaders().size());
        for (ClassLoader classLoader : getClassLoaders()) {
            log.debug("Attempting getResource({}) on {}", name, classLoader.toString());
            Enumeration<URL> resources = classLoader.getResources(name);
            if (resources != null) {
                log.debug("Found resource({}) on {}", name, classLoader.toString());
                enumerations.add(resources);
            }
        }
        return new CompositeEnumeration<URL>(enumerations);
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        for (ClassLoader classLoader : getClassLoaders()) {
            log.debug("Attempting loadClass({}) on {}", name, classLoader.toString());
            try {
                return classLoader.loadClass(name);
            } catch (ClassNotFoundException e) {
                log.debug("ClassNotFound {} by {}", name, classLoader.toString());
            }
        }
        throw new ClassNotFoundException(name);
    }

    @Override
    public synchronized void setClassAssertionStatus(String className, boolean enabled) {
        for (ClassLoader classLoader : getClassLoaders()) {
            classLoader.setClassAssertionStatus(className, enabled);
        }
    }

    @Override
    public synchronized void setDefaultAssertionStatus(boolean enabled) {
        for (ClassLoader classLoader : getClassLoaders()) {
            classLoader.setDefaultAssertionStatus(enabled);
        }
    }

    @Override
    public synchronized void setPackageAssertionStatus(String packageName, boolean enabled) {
        for (ClassLoader classLoader : getClassLoaders()) {
            classLoader.setPackageAssertionStatus(packageName, enabled);
        }
    }


}
