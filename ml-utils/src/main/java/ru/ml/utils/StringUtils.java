package ru.ml.utils;

import java.io.StreamTokenizer;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Utiltity for numerous and unrelated String escaping operations.
 */
public final class StringUtils {
    private StringUtils() {
        //utility class
    }

    private static final Map<Character, Character> _smartQuotes = new HashMap<Character, Character>(4);

    static {
        _smartQuotes.put((char) 0x91, '\'');
        _smartQuotes.put((char) 0x92, '\'');
        _smartQuotes.put((char) 0x93, '"');
        _smartQuotes.put((char) 0x94, '"');
        _smartQuotes.put((char) 0x96, '-');
    }

    /**
     * The following table is useful for converting characters into their HTML
     * string equivalents. NOTE: Not all values are viewable in all browsers.
     */
    static final char[][] C_HTML_STRING_FROM_CHAR =
            {"&#000;".toCharArray(), "&#001;".toCharArray(), "&#002;".toCharArray(), "&#003;".toCharArray(),
                    "&#004;".toCharArray(), "&#005;".toCharArray(), "&#006;".toCharArray(), "&#007;".toCharArray(),
                    "&#008;".toCharArray(), "&#009;".toCharArray(), "<BR>".toCharArray(), "&#011;".toCharArray(),
                    "&#012;".toCharArray(), "".toCharArray(), "&#014;".toCharArray(), "&#015;".toCharArray(),
                    "&#016;".toCharArray(), "&#017;".toCharArray(), "&#018;".toCharArray(), "&#019;".toCharArray(),
                    "&#020;".toCharArray(), "&#021;".toCharArray(), "&#022;".toCharArray(), "&#023;".toCharArray(),
                    "&#024;".toCharArray(), "&#025;".toCharArray(), "&#026;".toCharArray(), "&#027;".toCharArray(),
                    "&#028;".toCharArray(), "&#029;".toCharArray(), "&#030;".toCharArray(), "&#031;".toCharArray(),
                    " ".toCharArray(), "!".toCharArray(), "&quot;".toCharArray(), "#".toCharArray(), "$".toCharArray(),
                    "%".toCharArray(), "&amp;".toCharArray(), "&#039;".toCharArray(), "(".toCharArray(), ")".toCharArray(),
                    "*".toCharArray(), "+".toCharArray(), ",".toCharArray(), "-".toCharArray(), ".".toCharArray(),
                    "/".toCharArray(), "0".toCharArray(), "1".toCharArray(), "2".toCharArray(), "3".toCharArray(),
                    "4".toCharArray(), "5".toCharArray(), "6".toCharArray(), "7".toCharArray(), "8".toCharArray(),
                    "9".toCharArray(), ":".toCharArray(), ";".toCharArray(), "&#060;".toCharArray(), "=".toCharArray(),
                    "&#062;".toCharArray(), "?".toCharArray(), "@".toCharArray(), "A".toCharArray(), "B".toCharArray(),
                    "C".toCharArray(), "D".toCharArray(), "E".toCharArray(), "F".toCharArray(), "G".toCharArray(),
                    "H".toCharArray(), "I".toCharArray(), "J".toCharArray(), "K".toCharArray(), "L".toCharArray(),
                    "M".toCharArray(), "N".toCharArray(), "O".toCharArray(), "P".toCharArray(), "Q".toCharArray(),
                    "R".toCharArray(), "S".toCharArray(), "T".toCharArray(), "U".toCharArray(), "V".toCharArray(),
                    "W".toCharArray(), "X".toCharArray(), "Y".toCharArray(), "Z".toCharArray(), "[".toCharArray(),
                    "\\".toCharArray(), "]".toCharArray(), "^".toCharArray(), "_".toCharArray(), "`".toCharArray(),
                    "a".toCharArray(), "b".toCharArray(), "c".toCharArray(), "d".toCharArray(), "e".toCharArray(),
                    "f".toCharArray(), "g".toCharArray(), "h".toCharArray(), "i".toCharArray(), "j".toCharArray(),
                    "k".toCharArray(), "l".toCharArray(), "m".toCharArray(), "n".toCharArray(), "o".toCharArray(),
                    "p".toCharArray(), "q".toCharArray(), "r".toCharArray(), "s".toCharArray(), "t".toCharArray(),
                    "u".toCharArray(), "v".toCharArray(), "w".toCharArray(), "x".toCharArray(), "y".toCharArray(),
                    "z".toCharArray(), "{".toCharArray(), "|".toCharArray(), "}".toCharArray(), "~".toCharArray(),
                    "&#127;".toCharArray(), "&#128;".toCharArray(), "&#129;".toCharArray(), "&#130;".toCharArray(),
                    "&#131;".toCharArray(), "&#132;".toCharArray(), "&#133;".toCharArray(), "&#134;".toCharArray(),
                    "&#135;".toCharArray(), "&#136;".toCharArray(), "&#137;".toCharArray(), "&#138;".toCharArray(),
                    "&#139;".toCharArray(), "&#140;".toCharArray(), "&#141;".toCharArray(), "&#142;".toCharArray(),
                    "&#143;".toCharArray(), "&#144;".toCharArray(), "&#145;".toCharArray(), "&#146;".toCharArray(),
                    "&#147;".toCharArray(), "&#148;".toCharArray(), "&#149;".toCharArray(), "&#150;".toCharArray(),
                    "&#151;".toCharArray(), "&#152;".toCharArray(), "&#153;".toCharArray(), "&#154;".toCharArray(),
                    "&#155;".toCharArray(), "&#156;".toCharArray(), "&#157;".toCharArray(), "&#158;".toCharArray(),
                    "&#159;".toCharArray(), "&#160;".toCharArray(), "&#161;".toCharArray(), "&#162;".toCharArray(),
                    "&#163;".toCharArray(), "&#164;".toCharArray(), "&#165;".toCharArray(), "&#166;".toCharArray(),
                    "&#167;".toCharArray(), "&#168;".toCharArray(), "&#169;".toCharArray(), "&#170;".toCharArray(),
                    "&#171;".toCharArray(), "&#172;".toCharArray(), "&#173;".toCharArray(), "&#174;".toCharArray(),
                    "&#175;".toCharArray(), "&#176;".toCharArray(), "&#177;".toCharArray(), "&#178;".toCharArray(),
                    "&#179;".toCharArray(), "&#180;".toCharArray(), "&#181;".toCharArray(), "&#182;".toCharArray(),
                    "&#183;".toCharArray(), "&#184;".toCharArray(), "&#185;".toCharArray(), "&#186;".toCharArray(),
                    "&#187;".toCharArray(), "&#188;".toCharArray(), "&#189;".toCharArray(), "&#190;".toCharArray(),
                    "&#191;".toCharArray(), "&#192;".toCharArray(), "&#193;".toCharArray(), "&#194;".toCharArray(),
                    "&#195;".toCharArray(), "&#196;".toCharArray(), "&#197;".toCharArray(), "&#198;".toCharArray(),
                    "&#199;".toCharArray(), "&#200;".toCharArray(), "&#201;".toCharArray(), "&#202;".toCharArray(),
                    "&#203;".toCharArray(), "&#204;".toCharArray(), "&#205;".toCharArray(), "&#206;".toCharArray(),
                    "&#207;".toCharArray(), "&#208;".toCharArray(), "&#209;".toCharArray(), "&#210;".toCharArray(),
                    "&#211;".toCharArray(), "&#212;".toCharArray(), "&#213;".toCharArray(), "&#214;".toCharArray(),
                    "&#215;".toCharArray(), "&#216;".toCharArray(), "&#217;".toCharArray(), "&#218;".toCharArray(),
                    "&#219;".toCharArray(), "&#220;".toCharArray(), "&#221;".toCharArray(), "&#222;".toCharArray(),
                    "&#223;".toCharArray(), "&#224;".toCharArray(), "&#225;".toCharArray(), "&#226;".toCharArray(),
                    "&#227;".toCharArray(), "&#228;".toCharArray(), "&#229;".toCharArray(), "&#230;".toCharArray(),
                    "&#231;".toCharArray(), "&#232;".toCharArray(), "&#233;".toCharArray(), "&#234;".toCharArray(),
                    "&#235;".toCharArray(), "&#236;".toCharArray(), "&#237;".toCharArray(), "&#238;".toCharArray(),
                    "&#239;".toCharArray(), "&#240;".toCharArray(), "&#241;".toCharArray(), "&#242;".toCharArray(),
                    "&#243;".toCharArray(), "&#244;".toCharArray(), "&#245;".toCharArray(), "&#246;".toCharArray(),
                    "&#247;".toCharArray(), "&#248;".toCharArray(), "&#249;".toCharArray(), "&#250;".toCharArray(),
                    "&#251;".toCharArray(), "&#252;".toCharArray(), "&#253;".toCharArray(), "&#254;".toCharArray(),
                    "&#255;".toCharArray()};

    /**
     * The following table is useful for preparing a string that needs to be
     * converted to HTML and be made ready for a database insert.
     */
    static final char[][] C_DBHTML_STRING_FROM_CHAR =
            {"&#000;".toCharArray(), "&#001;".toCharArray(), "&#002;".toCharArray(), "&#003;".toCharArray(),
                    "&#004;".toCharArray(), "&#005;".toCharArray(), "&#006;".toCharArray(), "&#007;".toCharArray(),
                    "&#008;".toCharArray(), "&#009;".toCharArray(), "<BR>".toCharArray(), "&#011;".toCharArray(),
                    "&#012;".toCharArray(), "".toCharArray(), "&#014;".toCharArray(), "&#015;".toCharArray(),
                    "&#016;".toCharArray(), "&#017;".toCharArray(), "&#018;".toCharArray(), "&#019;".toCharArray(),
                    "&#020;".toCharArray(), "&#021;".toCharArray(), "&#022;".toCharArray(), "&#023;".toCharArray(),
                    "&#024;".toCharArray(), "&#025;".toCharArray(), "&#026;".toCharArray(), "&#027;".toCharArray(),
                    "&#028;".toCharArray(), "&#029;".toCharArray(), "&#030;".toCharArray(), "&#031;".toCharArray(),
                    " ".toCharArray(), "!".toCharArray(), "\"".toCharArray(), "#".toCharArray(), "$".toCharArray(),
                    "%".toCharArray(), "&".toCharArray(), "''".toCharArray(), "(".toCharArray(), ")".toCharArray(),
                    "*".toCharArray(), "+".toCharArray(), ",".toCharArray(), "-".toCharArray(), ".".toCharArray(),
                    "/".toCharArray(), "0".toCharArray(), "1".toCharArray(), "2".toCharArray(), "3".toCharArray(),
                    "4".toCharArray(), "5".toCharArray(), "6".toCharArray(), "7".toCharArray(), "8".toCharArray(),
                    "9".toCharArray(), ":".toCharArray(), ";".toCharArray(), "&#060;".toCharArray(), "=".toCharArray(),
                    "&#062;".toCharArray(), "?".toCharArray(), "@".toCharArray(), "A".toCharArray(), "B".toCharArray(),
                    "C".toCharArray(), "D".toCharArray(), "E".toCharArray(), "F".toCharArray(), "G".toCharArray(),
                    "H".toCharArray(), "I".toCharArray(), "J".toCharArray(), "K".toCharArray(), "L".toCharArray(),
                    "M".toCharArray(), "N".toCharArray(), "O".toCharArray(), "P".toCharArray(), "Q".toCharArray(),
                    "R".toCharArray(), "S".toCharArray(), "T".toCharArray(), "U".toCharArray(), "V".toCharArray(),
                    "W".toCharArray(), "X".toCharArray(), "Y".toCharArray(), "Z".toCharArray(), "[".toCharArray(),
                    "\\".toCharArray(), "]".toCharArray(), "^".toCharArray(), "_".toCharArray(), "`".toCharArray(),
                    "a".toCharArray(), "b".toCharArray(), "c".toCharArray(), "d".toCharArray(), "e".toCharArray(),
                    "f".toCharArray(), "g".toCharArray(), "h".toCharArray(), "i".toCharArray(), "j".toCharArray(),
                    "k".toCharArray(), "l".toCharArray(), "m".toCharArray(), "n".toCharArray(), "o".toCharArray(),
                    "p".toCharArray(), "q".toCharArray(), "r".toCharArray(), "s".toCharArray(), "t".toCharArray(),
                    "u".toCharArray(), "v".toCharArray(), "w".toCharArray(), "x".toCharArray(), "y".toCharArray(),
                    "z".toCharArray(), "{".toCharArray(), "|".toCharArray(), "}".toCharArray(), "~".toCharArray(),
                    "&#127;".toCharArray(), "&#128;".toCharArray(), "&#129;".toCharArray(), "&#130;".toCharArray(),
                    "&#131;".toCharArray(), "&#132;".toCharArray(), "&#133;".toCharArray(), "&#134;".toCharArray(),
                    "&#135;".toCharArray(), "&#136;".toCharArray(), "&#137;".toCharArray(), "&#138;".toCharArray(),
                    "&#139;".toCharArray(), "&#140;".toCharArray(), "&#141;".toCharArray(), "&#142;".toCharArray(),
                    "&#143;".toCharArray(), "&#144;".toCharArray(), "&#145;".toCharArray(), "&#146;".toCharArray(),
                    "&#147;".toCharArray(), "&#148;".toCharArray(), "&#149;".toCharArray(), "&#150;".toCharArray(),
                    "&#151;".toCharArray(), "&#152;".toCharArray(), "&#153;".toCharArray(), "&#154;".toCharArray(),
                    "&#155;".toCharArray(), "&#156;".toCharArray(), "&#157;".toCharArray(), "&#158;".toCharArray(),
                    "&#159;".toCharArray(), "&#160;".toCharArray(), "&#161;".toCharArray(), "&#162;".toCharArray(),
                    "&#163;".toCharArray(), "&#164;".toCharArray(), "&#165;".toCharArray(), "&#166;".toCharArray(),
                    "&#167;".toCharArray(), "&#168;".toCharArray(), "&#169;".toCharArray(), "&#170;".toCharArray(),
                    "&#171;".toCharArray(), "&#172;".toCharArray(), "&#173;".toCharArray(), "&#174;".toCharArray(),
                    "&#175;".toCharArray(), "&#176;".toCharArray(), "&#177;".toCharArray(), "&#178;".toCharArray(),
                    "&#179;".toCharArray(), "&#180;".toCharArray(), "&#181;".toCharArray(), "&#182;".toCharArray(),
                    "&#183;".toCharArray(), "&#184;".toCharArray(), "&#185;".toCharArray(), "&#186;".toCharArray(),
                    "&#187;".toCharArray(), "&#188;".toCharArray(), "&#189;".toCharArray(), "&#190;".toCharArray(),
                    "&#191;".toCharArray(), "&#192;".toCharArray(), "&#193;".toCharArray(), "&#194;".toCharArray(),
                    "&#195;".toCharArray(), "&#196;".toCharArray(), "&#197;".toCharArray(), "&#198;".toCharArray(),
                    "&#199;".toCharArray(), "&#200;".toCharArray(), "&#201;".toCharArray(), "&#202;".toCharArray(),
                    "&#203;".toCharArray(), "&#204;".toCharArray(), "&#205;".toCharArray(), "&#206;".toCharArray(),
                    "&#207;".toCharArray(), "&#208;".toCharArray(), "&#209;".toCharArray(), "&#210;".toCharArray(),
                    "&#211;".toCharArray(), "&#212;".toCharArray(), "&#213;".toCharArray(), "&#214;".toCharArray(),
                    "&#215;".toCharArray(), "&#216;".toCharArray(), "&#217;".toCharArray(), "&#218;".toCharArray(),
                    "&#219;".toCharArray(), "&#220;".toCharArray(), "&#221;".toCharArray(), "&#222;".toCharArray(),
                    "&#223;".toCharArray(), "&#224;".toCharArray(), "&#225;".toCharArray(), "&#226;".toCharArray(),
                    "&#227;".toCharArray(), "&#228;".toCharArray(), "&#229;".toCharArray(), "&#230;".toCharArray(),
                    "&#231;".toCharArray(), "&#232;".toCharArray(), "&#233;".toCharArray(), "&#234;".toCharArray(),
                    "&#235;".toCharArray(), "&#236;".toCharArray(), "&#237;".toCharArray(), "&#238;".toCharArray(),
                    "&#239;".toCharArray(), "&#240;".toCharArray(), "&#241;".toCharArray(), "&#242;".toCharArray(),
                    "&#243;".toCharArray(), "&#244;".toCharArray(), "&#245;".toCharArray(), "&#246;".toCharArray(),
                    "&#247;".toCharArray(), "&#248;".toCharArray(), "&#249;".toCharArray(), "&#250;".toCharArray(),
                    "&#251;".toCharArray(), "&#252;".toCharArray(), "&#253;".toCharArray(), "&#254;".toCharArray(),
                    "&#255;".toCharArray()};

    /**
     * The following is statc data used by the current implementation of
     * fromTextToHTML. The URL_PROTOCOL table contains protocol prefix strings
     * that are recognized by the parser. The corresponding values are the
     * minimum requisite values to constitute an URL.
     */
    private static final Map<String, String> URL_PROTOCOLS = new HashMap<String, String>();

    static {
        URL_PROTOCOLS.put("http", "http:/");
        URL_PROTOCOLS.put("https", "https:/");
        URL_PROTOCOLS.put("ftp", "ftp:/");
        URL_PROTOCOLS.put("gopher", "gopher:/");
        URL_PROTOCOLS.put("telnet", "telnet:/");
        URL_PROTOCOLS.put("file", "file:/");
    }

    public static final Map<String, String> MIME_MAP = new HashMap<String, String>();

    static {
        MIME_MAP.put("application/msword", "WORD DOC");
        MIME_MAP.put("application/vnd.ms-word", "WORD DOC");
        MIME_MAP.put("application/excel", "EXCEL XLS");
        MIME_MAP.put("application/vnd.ms-excel", "EXCEL XLS");
        MIME_MAP.put("application/ppt", "PPT");
        MIME_MAP.put("application/vnd.ms-powerpoint", "PPT");
        MIME_MAP.put("application/vnd.ms-project", "PROJECT MPP");
        MIME_MAP.put("application/msaccess", "ACCESS MDB");
        MIME_MAP.put("application/vnd.ms-access", "ACCESS MDB");
        MIME_MAP.put("application/pdf", "ACROBAT PDF");
        MIME_MAP.put("application/x-shockwave-flash", "FLASH SWF");
        MIME_MAP.put("application/x-photoshop", "PHOTOSHOP PSD");
        MIME_MAP.put("application/rtf", "RTF DOC");
        MIME_MAP.put("text/plain", "TXT DOC");
        MIME_MAP.put("text/html", "HTML DOC");
        MIME_MAP.put("text/rtf", "RTF DOC");
        MIME_MAP.put("image/gif", "IMAGE GIF");
        MIME_MAP.put("image/jpeg", "IMAGE JPG");
    }

    public static final String TEXT_TOKENS = " \n\r\t()[]{}!-;:'\",.<>?";

    private static final String WHITE_SPACE = " \n\r\t";

    /**
     * The following table is useful for URL-encoding strings
     */
    private static final String[] URL_ENCODINGS =
            {"%00", "%01", "%02", "%03", "%04", "%05", "%06", "%07", "%08", "%09", "%0A", "%0B", "%0C", "%0D", "%0E", "%0F",
                    "%10", "%11", "%12", "%13", "%14", "%15", "%16", "%17", "%18", "%19", "%1A", "%1B", "%1C", "%1D", "%1E",
                    "%1F", "+", "%21", "%22", "%23", "%24", "%25", "%26", "%27", "%28", "%29", "%2A", "%2B", "%2C", "%2D",
                    "%2E", "%2F", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "%3A", "%3B", "%3C", "%3D", "%3E", "%3F",
                    "%40", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
                    "U", "V", "W", "X", "Y", "Z", "%5B", "%5C", "%5D", "%5E", "%5F", "%60", "a", "b", "c", "d", "e", "f", "g",
                    "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "%7B",
                    "%7C", "%7D", "%7E", "%7F", "%80", "%81", "%82", "%83", "%84", "%85", "%86", "%87", "%88", "%89", "%8A",
                    "%8B", "%8C", "%8D", "%8E", "%8F", "%90", "%91", "%92", "%93", "%94", "%95", "%96", "%97", "%98", "%99",
                    "%9A", "%9B", "%9C", "%9D", "%9E", "%9F", "%A0", "%A1", "%A2", "%A3", "%A4", "%A5", "%A6", "%A7", "%A8",
                    "%A9", "%AA", "%AB", "%AC", "%AD", "%AE", "%AF", "%B0", "%B1", "%B2", "%B3", "%B4", "%B5", "%B6", "%B7",
                    "%B8", "%B9", "%BA", "%BB", "%BC", "%BD", "%BE", "%BF", "%C0", "%C1", "%C2", "%C3", "%C4", "%C5", "%C6",
                    "%C7", "%C8", "%C9", "%CA", "%CB", "%CC", "%CD", "%CE", "%CF", "%D0", "%D1", "%D2", "%D3", "%D4", "%D5",
                    "%D6", "%D7", "%D8", "%D9", "%DA", "%DB", "%DC", "%DD", "%DE", "%DF", "%E0", "%E1", "%E2", "%E3", "%E4",
                    "%E5", "%E6", "%E7", "%E8", "%E9", "%EA", "%EB", "%EC", "%ED", "%EE", "%EF", "%F0", "%F1", "%F2", "%F3",
                    "%F4", "%F5", "%F6", "%F7", "%F8", "%F9", "%FA", "%FB", "%FC", "%FD", "%FE", "%FF"};

    /**
     * Base64 (uuencode) encoding table
     */
    static final char[] BASE64_CHAR_FROM_INT =
            {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
                    'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
                    'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

    /**
     * Base64 (uuencode) decoding table
     */
    static final byte[] BYTE_FROM_BASE64_CHAR =
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58,
                    59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
                    19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
                    40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};

    /**
     * URL-safe version of BASE64_CHAR_FROM_INT '+' replaced with '_' '/'
     * replaced with '.'
     */
    static final char[] URL_SAFE_BASE64_CHAR_FROM_INT =
            {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
                    'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
                    'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '_', '.'};

    /**
     * URL-safe version of BYTE_FROM_BASE64_CHAR '+' replaced with '_' '/'
     * replaced with '.'
     */
    static final byte[] BYTE_FROM_URL_SAFE_BASE64_CHAR =
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 63, -1, 52, 53, 54, 55, 56, 57, 58,
                    59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
                    19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 62, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
                    40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};

    /**
     * No guarantee for minimum value of Character.MAX_RADIX :(
     */
    static final char[] BASE32_CHAR_FROM_INT =
            {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                    'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v'};

    static final byte[] BYTE_FROM_BASE32_CHAR =
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8,
                    9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
                    24, 25, 26, 27, 28, 29, 30, 31, -1, -1, -1, -1, -1, -1, -1, -1, -1};

    /**
     * Abbreviates an http(s) url string to max characters by: - dropping the
     * protocol - dropping the query string - dropping the anchor in order until
     * max is reached.
     *
     * @param url NOT NULL
     * @param max maximum length in characters; if less than 1 returns url
     * @return NOT NULL
     */
    private static String abbreviateHttpUrl(String url, int max) {
        if (1 > max || url.length() <= max)
            return url;

        String abbrev = url.toLowerCase();
        String proto;
        // protocol
        if (abbrev.startsWith("http://")) {
            proto = "http://";
            abbrev = url.substring("http://".length());
            if (abbrev.length() <= max)
                return abbrev;
        } else if (abbrev.startsWith("https://")) {
            proto = "https://";
            abbrev = url.substring("https://".length());
            if (abbrev.length() <= max)
                return abbrev;
        } else
            return url.substring(0, max) + "...";
        // query string
        int indexQ = abbrev.indexOf('?');
        indexQ = -1 != indexQ ? indexQ : Integer.MAX_VALUE;
        // anchor
        int indexD = abbrev.indexOf('#');
        indexD = -1 != indexD ? indexD : Integer.MAX_VALUE;

        int index = Math.min(indexQ, indexD);
        if (index <= max)
            return abbrev.substring(0, index + 1) + "...";
        int end = Integer.MAX_VALUE == index ? abbrev.length() : index + 1;
        return proto + "..." + abbrev.substring(end - max, end) + (Integer.MAX_VALUE != index ? "..." : "");
    }

    public static byte byteFromBase32Char(char c) {
        return BYTE_FROM_BASE32_CHAR[c];
    }

    /**
     * Performs a lexicographic comparison of two Strings, where null is
     * considered to be lexicographically lower than any non-null String. <p/>
     * Otherwise equivalent to operand.compareTo( target ).
     */
    public static int compareTo(String first, String second) {
        if (null == first) {
            if (null == second)
                return 0;
            return -1;
        } else if (null == second)
            return 1;
        else
            return first.compareTo(second);
    }

    /**
     * Performs a case-insensitive lexicographic comparison of two Strings,
     * where null is considered to be lexicographically lower than any non-null
     * String. <p/> Otherwise equivalent to operand.compareTo( target ).
     */
    public static int compareToIgnoreCase(String first, String second) {
        if (null == first) {
            if (null == second)
                return 0;
            return -1;
        } else if (null == second)
            return 1;
        else
            return first.compareToIgnoreCase(second);
    }

    /**
     * This is an easy method of concatenating a bunch of strings, and it should
     * be faster than using str1 + str2 + str3...
     */
    public static String concatAll(String[] strs) {
        StringBuilder sb = new StringBuilder();

        for (String s : strs)
            if (s != null)
                sb.append(s);

        return sb.toString();
    }

    /**
     * Creates a static SQL IN clause from a Collection of objects; All objects
     * except for Numbers are treated as strings literals and escaped according
     * to the DBMS-specific string escape syntax.
     *
     * @param c Collection Members of the IN clause, NOT NULL
     */
    public static String createInClause(Collection c) {
        if (null == c || c.isEmpty())
            return "";

        StringBuilder buffer = new StringBuilder("IN ( ");

        boolean appendComma = false;
        for (Object item : c) {
            if (appendComma) {
                buffer.append(", ");
            } else {
                appendComma = true;
            }

            buffer.append(item instanceof Number ? item.toString() : escapeString(item.toString(), true));
        }
        buffer.append(" ) ");

        return buffer.toString();
    }

    public static String createNotInClause(Collection c) {
        if (null == c || c.isEmpty())
            return "";
        return "NOT " + createInClause(c);
    }

    /**
     * Returns a String representation of the argument Float. TODO - Use
     * NumberFormat
     *
     * @param f         Float NOT NULL
     * @param places    Number of decimal places to display
     * @param NaNsymbol Returned if the Float is Float.NaN
     */
    public static String displayFloat(Float f, int places, String NaNsymbol) {
        if (f.isNaN())
            return NaNsymbol;

        int multiplier = (int) Math.pow(10.0, places);

        int total = Math.round(f.floatValue() * multiplier);
        int left = total / multiplier;
        int right = (int) Math.abs((long) total - left * multiplier);

        return left + (0 == places ? "" : "." + right);
    }

    /**
     * Returns a Money representation of the argument Float.
     *
     * @param f         Float NOT NULL
     * @param NaNsymbol Returned if the Float is Float.NaN
     */
    public static String displayFloatAsMoney(Float f, String NaNsymbol) {
        if (f.isNaN())
            return NaNsymbol;

        double fValue = f.doubleValue();
        int multiplier = (int) Math.pow(10.0, 2);

        long total = Math.round(fValue * multiplier);
        long left = total / multiplier;
        long right = (int) Math.abs(total - left * multiplier);

        String strLeft = String.valueOf(left);
        String res = "";

        for (int i = 0; i < strLeft.length(); i++)
            if (0 == (strLeft.length() - i - 1) % 3 && (i + 1) != strLeft.length())
                res += ((i + 1) == strLeft.length() ? strLeft.substring(i) : strLeft.substring(i, i + 1)) + ",";

            else
                res += ((i + 1) == strLeft.length() ? strLeft.substring(i) : strLeft.substring(i, i + 1));

        return res + (0 != right ? ("." + right) : "");
    }

    /**
     * Returns a String escaped to be used in a SQL LIKE clause. TODO - Put the
     * search string escape sequence into SqlUtil.
     *
     * @param s                  String literal to be escaped
     * @param searchStringEscape Escape sequence for search wildcards, NOT NULL
     * @param quote              Specifies whether or not the returned String should be quoted
     *                           with single quotes
     * @return The escaped String or null if the argument String is null
     */
    public static String escapeLikeString(String s, String searchStringEscape, boolean quote) {
        if (null == s)
            return null;

        StringBuilder buffer = new StringBuilder();
        if (quote)
            buffer.append('\'');

        for (int i = 0; i < s.length(); i++) {
            final char c = s.charAt(i);

            if ('%' == c || '_' == c)
                buffer.append(searchStringEscape);
            else if ('\'' == c)
                buffer.append('\'');

            buffer.append(c);
        }
        if (quote)
            buffer.append('\'');

        return buffer.toString();
    }

    /**
     * Returns a properly-escaped SQL string literal.
     *
     * @param s     String to be escaped
     * @param quote Specifies whether or not the returned String should be quoted
     *              with single quotes
     * @return the SQL string literal or null if the argument String was null
     */
    public static String escapeString(String s, boolean quote) {
        if (null == s)
            return null;

        StringBuilder buffer = new StringBuilder();
        if (quote)
            buffer.append('\'');

        for (int i = 0; i < s.length(); i++) {
            final char c = s.charAt(i);

            if ('\'' == c)
                buffer.append('\'');
            buffer.append(c);
        }
        if (quote)
            buffer.append('\'');

        return buffer.toString();
    }

    /**
     * Returns formatted number string. Convenience method to handle null
     * number.
     *
     * @param nf NumberFormat
     * @param x  number to be formated
     * @return a String formated according to the specified NumberFormat, or 0
     *         formatted, if i == null
     */
    public static String formatNumber(NumberFormat nf, Number x) {
        if (null != x)
            return nf.format(x); // we'll let Java cast x to a double for us
        return nf.format(0d);
    }

    /**
     * Converts a hexadecimal formatted string (see toHexString() above) into a
     * normal ASCII representation of that string. For example,
     * "0066007200650064" will be converted to "fred". You should note that the
     * hi-bytes in the input string will be ignored. You should also note that
     * an empty string will be returned if the input string is incorrectly
     * formatted.
     */
    public static String fromHexString(String in) {
        int len = in.length();

        if (0 < (len & 0x03))
            return ""; // String 'in' wasn't in hexed-unicode
        // format

        byte[] bytes = in.getBytes(); // new byte[ len ];
        // in.getBytes( 0, len, bytes, 0 ); // were only concerned with the
        // lo-byte
        char[] out = new char[len >>>= 2];
        char tmp;
        char accumulator;
        int i;
        int j;

        for (i = 0; i < len; ++i) {
            accumulator = 0;
            for (j = 0; 4 > j; ++j) {
                accumulator <<= 4;
                tmp = (char) bytes[(i << 2) + j];
                if ('0' <= tmp && '9' >= tmp)
                    accumulator |= (tmp - '0');
                else if ('a' <= tmp && 'f' >= tmp)
                    accumulator |= (tmp - 'a' + 10);
                else if ('A' <= tmp && 'F' >= tmp)
                    accumulator |= (tmp - 'A' + 10);
                else
                    return ""; // String 'in' had stray characters
            }
            out[i] = accumulator;
        }

        return new String(out);
    }

    public static int fromSignedHexString(String hex) {
        return (int) Long.parseLong(hex, 16);
    }

    /**
     * <PRE>
     * <p/>
     * fromTextToHTML does four things: 1) Converts all occurrences of URLs to
     * active HREFs. 2) Replaces all new line feeds '\n' with "\n
     * <P>" 3) Escapes all special HTML characters like <> 4) It optionally
     * will prepare a string for DB insertion as well <p/> For example,
     * "checkout my site at http://mysite.com" would be transformed to "checkout
     * my site at <A HREF="http://mysite.com" TARGET="_top">http://mysite.com</A>".
     * The useDBformat tag tells the processor to prepare the text for database
     * insertion as well (this is done primarily by escaping for single-quotes
     * and enclosing the entire string in single-quotes.
     * <p/>
     * </PRE>
     *
     * @param inString    NOT NULL
     * @param useDBformat If true escape the String in database format
     */
    public static String fromTextToHTML(String inString, boolean useDBformat) {
        return fromTextToHTML(inString, useDBformat, "_top");
    }

    /**
     * <PRE>
     * <p/>
     * fromTextToHTML does four things: 1) Converts all occurrences of URLs to
     * active HREFs. 2) Replaces all new line feeds '\n' with "\n
     * <P>" 3) Escapes all special HTML characters like <> 4) It optionally
     * will prepare a string for DB insertion as well <p/> For example,
     * "checkout my site at http://mysite.com" would be transformed to "checkout
     * my site at <A HREF="http://mysite.com" TARGET="_top">http://mysite.com</A>".
     * The useDBformat tag tells the processor to prepare the text for database
     * insertion as well (this is done primarily by escaping for single-quotes
     * and enclosing the entire string in single-quotes. <p/> TODO: Either make
     * this function a state-driven processor or move to regular expressions.
     * <p/>
     * </PRE>
     *
     * @param inString    NOT NULL
     * @param useDBformat If true escape the String in database format
     * @param frameTarget Target window name for links, NOT NULL
     */
    public static String fromTextToHTML(String inString, boolean useDBformat, String frameTarget) {
        return fromTextToHTML(inString, useDBformat, frameTarget, "<P>");
    }

    /**
     * <PRE>
     * <p/>
     * fromTextToHTML does four things: 1) Converts all occurrences of URLs to
     * active HREFs. 2) Replaces all new line feeds '\n' with '\n<code>newLineRpl</code>'
     * 3) Escapes all special HTML characters like <> 4) It optionally
     * will prepare a string for DB insertion as well <p/> For example,
     * "checkout my site at http://mysite.com" would be transformed to "checkout
     * my site at <A HREF="http://mysite.com" TARGET="_top">http://mysite.com</A>".
     * The useDBformat tag tells the processor to prepare the text for database
     * insertion as well (this is done primarily by escaping for single-quotes
     * and enclosing the entire string in single-quotes. <p/> TODO: Either make
     * this function a state-driven processor or move to regular expressions.
     * <p/>
     * </PRE>
     *
     * @param inString    NOT NULL
     * @param useDBformat If true escape the String in database format
     * @param frameTarget Target window name for links, NOT NULL
     * @param newLineRpl  Replace '\n' with '\n<code>newLineRpl</code>'
     */

    public static String fromTextToHTML(String inString, boolean useDBformat, String frameTarget, String newLineRpl) {
        return fromTextToHTML(inString, useDBformat, frameTarget, newLineRpl, 0);
    }

    /**
     * <PRE>
     * <p/>
     * fromTextToHTML does four things: 1) Converts all occurrences of URLs to
     * active HREFs. 2) Replaces all new line feeds '\n' with '\n<code>newLineRpl</code>'
     * 3) Escapes all special HTML characters like <> 4) It optionally
     * will prepare a string for DB insertion as well <p/> For example,
     * "checkout my site at http://mysite.com" would be transformed to "checkout
     * my site at <A HREF="http://mysite.com" TARGET="_top">http://mysite.com</A>".
     * The useDBformat tag tells the processor to prepare the text for database
     * insertion as well (this is done primarily by escaping for single-quotes
     * and enclosing the entire string in single-quotes. <p/> TODO: Either make
     * this function a state-driven processor or move to regular expressions.
     * <p/>
     * </PRE>
     *
     * @param inString    NOT NULL
     * @param useDBformat If true escape the String in database format
     * @param frameTarget Target window name for links, NOT NULL
     * @param newLineRpl  Replace '\n' with '\n<code>newLineRpl</code>'
     * @param maxWordLen  If breakeWords>0 then break long words
     */
    public static String fromTextToHTML(String inString, boolean useDBformat, String frameTarget, String newLineRpl,
                                        int maxWordLen) {
        StringBuilder HTMLBuffer = new StringBuilder(inString.length() * 3 / 2);
        StringTokenizer toker = new StringTokenizer(inString, TEXT_TOKENS, true);
        String token;
        String url;

        if (useDBformat)
            HTMLBuffer.append('\'');

        while (toker.hasMoreTokens()) {
            token = toker.nextToken(TEXT_TOKENS);
            if ((null != (url = URL_PROTOCOLS.get(token))) && (toker.hasMoreTokens())) {
                StringBuilder URLBuffer = new StringBuilder(256);
                URLBuffer.append(token);
                URLBuffer.append(toker.nextToken(WHITE_SPACE));
                token = URLBuffer.toString();
                if (token.startsWith(url)) {
                    int len = token.length();
                    int last = len - 1;
                    while (0 <= TEXT_TOKENS.indexOf(token.charAt(last)))
                        last--;
                    last++;
                    url = token.substring(0, last);
                    if (last == len) {
                        if (toker.hasMoreTokens())
                            token = toker.nextToken(TEXT_TOKENS);
                        else
                            token = " ";
                    } else
                        token = token.substring(last);

                    StringBuilder breakedURL = new StringBuilder(256);
                    if (maxWordLen > 0 && url.length() > maxWordLen) {
                        breakedURL.append(url.substring(0, maxWordLen - 3)).append("...");
                    } else {
                        breakedURL.append(url);
                    }
                    HTMLBuffer.append("<A HREF=\"").append(toDBStringNoQuotes(url)).append("\" TARGET=\"");
                    HTMLBuffer.append(frameTarget).append("\">").append(
                            toHTMLString(breakedURL.toString(), useDBformat)).append("</A>");
                }
                URLBuffer.setLength(0);
            }

            if ('\n' == token.charAt(0))
                HTMLBuffer.append("\n").append(newLineRpl);
            else if ('\r' == token.charAt(0)) {
                // Skip it
            } else if (maxWordLen > 0) {
                HTMLBuffer.append(toHTMLString(breakSingleLongWord(token, maxWordLen), useDBformat));

            } else {
                HTMLBuffer.append(toHTMLString(token, useDBformat));
            }
        }

        if (useDBformat)
            HTMLBuffer.append('\'');

        return HTMLBuffer.toString();
    }

    /**
     * Converts hexadecimal String into a Base64 String.
     *
     * @return null if the argument is null
     */
    public static String hexToBase64(String hex) {
        if (null == hex)
            return null;

        StringBuilder buffer = new StringBuilder();

        int start = hex.length() % 3;
        if (1 < start) {
            int val = Character.digit(hex.charAt(0), 16);
            buffer.append(BASE64_CHAR_FROM_INT[val >> 2]);
            buffer.append(BASE64_CHAR_FROM_INT[((val & 3) << 4) | Character.digit(hex.charAt(1), 16)]);
        } else if (0 < start)
            buffer.append(BASE64_CHAR_FROM_INT[Character.digit(hex.charAt(0), 16)]);

        for (int i = start; i < hex.length(); i += 3) {
            int val1 = Character.digit(hex.charAt(i), 16);
            int val2 = Character.digit(hex.charAt(i + 1), 16);
            int val3 = Character.digit(hex.charAt(i + 2), 16);

            buffer.append(BASE64_CHAR_FROM_INT[(val1 << 2) | (val2 >> 2)]);
            buffer.append(BASE64_CHAR_FROM_INT[((val2 & 3) << 4) | val3]);
        }

        return buffer.toString();
    }

    public static final Pattern PATTERN_MAC = Pattern.compile("^(?:[0-9a-fA-F]{2}\\-){5}[0-9a-fA-F]{2}$");

    public static boolean isValidMACAddress(String mac) {
        Matcher matcher = PATTERN_MAC.matcher(mac);
        return matcher.find();
    }

    public static final Pattern PATTERN_IP4 = Pattern.compile("^(?:\\d{1,3}\\.){3}\\d{1,3}$");

    public static boolean isValidIPv4Address(String ip) {
        Matcher matcher = PATTERN_IP4.matcher(ip);
        if (!matcher.find()) {
            return false;
        }
        for (String strNum : splitByDelimiters(ip, ".")) {
            if (Integer.valueOf(strNum) > 255) {
                return false;
            }
        }
        return true;
    }

    public static boolean isValidClassName(String className) {
        if (isEmpty(className))
            return false;

        if (!isLatinLetter(className.charAt(0)))
            return false;

        char[] chars = className.toCharArray();

        for (int i = 1; i < chars.length; i++) {
            if (!(isDigit(chars[i]) || isLatinLetter(chars[i]) || chars[i] == '_'))
                return false;
        }
        return true;
    }

    /**
     * notRFC email validating with '@' symbol checkin and dot in domain name checking
     */
    private static final Pattern PATTERN_EMAIL = Pattern
            .compile("^[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]*@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$");

    public static boolean isValidEmailAdditionalCheck(String email) {
        if (email == null)
            return false;
        email = org.apache.commons.lang.StringUtils.trim(email);
        Matcher matcher = PATTERN_EMAIL.matcher(email);
        return matcher.find();
    }

    public static String getPhoneNumberByString(String phoneNumber) {
        StringBuilder sb = new StringBuilder();

        char[] chars = phoneNumber.toCharArray();

        for (char c : chars) {
            if (isDigit(c))
                sb.append(c);
        }

        return sb.toString();
    }

    public static List phraseList(String text) {
        final List<String> lst = new ArrayList<String>();
        try {
            StreamTokenizer tokenizer = new StreamTokenizer(new StringReader(text));
            tokenizer.ordinaryChar('/');
            tokenizer.ordinaryChars('0', '9');
            tokenizer.wordChars('0', '9');
            tokenizer.wordChars('*', '*');
            tokenizer.wordChars('%', '%');
            tokenizer.wordChars('_', '_');
            while (true) {
                int type = tokenizer.nextToken();
                if (StreamTokenizer.TT_EOL == type || StreamTokenizer.TT_EOF == type)
                    break;
                String s = tokenizer.sval;
                if (null == s || 0 == s.length())
                    continue;
                if ('"' == type || '\'' == type) {
                    final List subitems = phraseList(removeSubstring(s, ('"' == type) ? "'" : "\""));
                    if (subitems.isEmpty())
                        continue;
                    if (1 == subitems.size())
                        s = (String) subitems.iterator().next();
                    else {
                        final StringBuilder sb = new StringBuilder();
                        for (Object subitem : subitems) {
                            if (0 < sb.length()) {
                                sb.append(' ');
                            }
                            sb.append((String) subitem);
                        }
                        s = sb.toString();
                    }
                }
                lst.add(s);
            }
        } catch (Exception e) {
            // pass
        }
        return lst;
    }

    public static String removeSubstring(String original, String[] substrings) {
        if (null == original || null == substrings)
            return "";
        String searchIn = original;

        for (String sub : substrings) {
            StringBuilder result = new StringBuilder();
            int prevPosition = 0;

            while (true) {
                int index = searchIn.indexOf(sub, prevPosition);
                if (0 > index) {
                    result.append(searchIn.substring(prevPosition));
                    break;
                }
                result.append(searchIn.substring(prevPosition, index));
                prevPosition = index + sub.length();
            }
            searchIn = result.toString();
        }

        return searchIn;

    }

    /**
     * Generate a reply to the message
     *
     * @param msgBody Body of the original message
     * @return msgBody prefixed with "> "s
     */
    public static String reply(String msgBody) {
        msgBody = replaceSubstring(msgBody, "\n\r", "\n\r> ");
        msgBody = replaceSubstring(msgBody, "\n", "\n> ");
        return "> " + msgBody;
    }

    /**
     * Insert the method's description here. Creation date: (5/2/00 2:35:51 PM)
     *
     * @param original  java.lang.String
     * @param substring java.lang.String
     * @return java.lang.String
     */
    public static String removeSubstring(String original, String substring) {
        if (null == original || null == substring)
            return "";

        StringBuilder result = new StringBuilder();
        int prevPosition = 0;

        while (true) {
            int index = original.indexOf(substring, prevPosition);
            if (0 > index) {
                result.append(original.substring(prevPosition));
                break;
            }
            result.append(original.substring(prevPosition, index));
            prevPosition = index + substring.length();
        }

        return result.toString();
    }

    public static String removeSubstringCaseInsensitive(String original, String substring) {
        if (null == original || null == substring)
            return "";

        substring = substring.toUpperCase();
        String originalUpper = original.toUpperCase();
        StringBuilder result = new StringBuilder();
        int prevPosition = 0;

        while (true) {
            int index = originalUpper.indexOf(substring, prevPosition);
            if (0 > index) {
                result.append(original.substring(prevPosition));
                break;
            }
            result.append(original.substring(prevPosition, index));
            prevPosition = index + substring.length();
        }

        return result.toString();
    }

    /**
     * Replace substring with another substring. Creation date: (6/13/00)
     *
     * @param original java.lang.String
     * @param subFrom  java.lang.String
     * @return java.lang.String
     */
    public static String replaceSubstring(String original, String subFrom, String subTo) {
        if (null == original || null == subFrom)
            return "";

        StringBuilder result = new StringBuilder();
        int prevPosition = 0;

        while (true) {
            int index = original.indexOf(subFrom, prevPosition);
            if (0 > index) {
                result.append(original.substring(prevPosition));
                break;
            }
            result.append(original.substring(prevPosition, index));
            result.append(subTo);
            prevPosition = index + subFrom.length();
        }

        return result.toString();
    }

    public static String textToHTML(String inString) {
        return textToHTML(inString, 40, "_top");
    }

    /**
     * <PRE>
     * <p/>
     * Same as "fromTextToHTML", except uses a "<BR/>" instead of "
     * <P>" and doesn't do database quote conversion. <p/> TextToHTML does
     * three things: 1) Converts all occurrences of URLs to active HREFs. 2)
     * Replaces all new line feeds '\n' with "\n
     * <P>" 3) Escapes all special HTML characters like <> <p/> For example,
     * "checkout my site at http://mysite.com" would be transformed to "checkout
     * my site at <A HREF="http://mysite.com" TARGET="_top">http://mysite.com</A>".
     * <p/> TODO: Either make this function a state-driven processor or move to
     * regular expressions.
     * <p/>
     * </PRE>
     *
     * @param inString NOT NULL
     */
    public static String textToHTML(String inString, int displayUrlMax, String urlTarget) {
        if (null == inString || 0 == inString.length())
            return "";

        StringBuilder HTMLBuffer = new StringBuilder(inString.length() * 3 / 2);
        StringTokenizer toker = new StringTokenizer(inString, TEXT_TOKENS, true);
        String token;
        String url;
        while (toker.hasMoreTokens()) {
            token = toker.nextToken(TEXT_TOKENS);
            if ((null != (url = URL_PROTOCOLS.get(token))) && (toker.hasMoreTokens())) {
                StringBuilder URLBuffer = new StringBuilder(256);
                URLBuffer.append(token);
                URLBuffer.append(toker.nextToken(WHITE_SPACE));
                token = URLBuffer.toString();
                if (token.startsWith(url)) {
                    int len = token.length();
                    int last = len - 1;
                    while (0 <= TEXT_TOKENS.indexOf(token.charAt(last)))
                        last--;
                    last++;
                    url = token.substring(0, last);
                    if (last == len) {
                        if (toker.hasMoreTokens())
                            token = toker.nextToken(TEXT_TOKENS);
                        else
                            token = " ";
                    } else
                        token = token.substring(last);

                    HTMLBuffer.append("<a href=\"");
                    HTMLBuffer.append(toNonMarkupString(url));
                    HTMLBuffer.append('"');
                    if (null != urlTarget) {
                        HTMLBuffer.append(" target=\"");
                        HTMLBuffer.append(urlTarget);
                        HTMLBuffer.append('"');
                    }
                    HTMLBuffer.append('>');
                    HTMLBuffer.append(toNonMarkupString(abbreviateHttpUrl(url, displayUrlMax)));
                    HTMLBuffer.append("</a>");
                }
                URLBuffer.setLength(0);
            }

            if ('\n' == token.charAt(0))
                HTMLBuffer.append("\n<BR/>");
            else if ('\r' == token.charAt(0)) {
                // Skip it
            } else
                HTMLBuffer.append(toHTMLString(token));
        }

        return HTMLBuffer.toString();
    }

    /**
     * @param i int 0 <= i < 32
     * @throws ArrayIndexOutOfBoundsException
     */
    public static char toBase32Char(int i) {
        return BASE32_CHAR_FROM_INT[i];
    }

    /**
     * @param i int 0 <= i < 32
     * @throws ArrayIndexOutOfBoundsException
     */
    public static char toBase32Char(long i) {
        return BASE32_CHAR_FROM_INT[(int) i];
    }

    /**
     * This converts a noun phrase like 'Business Unit' to a database column
     * name, 'business_unit'. The characters are switched to lower case and
     * spaces separating words are switched to underbars. <p/> This mmethod
     * doesn't check for maximum length that a column name is allowed to be for
     * a given database platform. It doesn't handle tabs... It doesn't handle
     * punctuation or other symbols. Thus, use with caution...
     *
     * @param inString NOT NULL
     */
    public static String toDBColumnName(String inString) {
        return (inString.toLowerCase()).replace(' ', '_');
    }

    /**
     * This converts database column names such as ('business_unit',
     * 'department') into column titles such as ('Business Unit', 'Department').
     * <p/> This method doesn't handle columns names using the format
     * ('businessUnit', 'isParticipant'), as the results would be
     * ('Businessunit', 'Isparticipant').
     *
     * @param inString NOT NULL
     */
    public static String toDBColumnTitle(String inString) {
        return toTitleCase(inString.replace('_', ' '));
    }

    /*
     * Converts normal strings into database-ready strings by replacing
     * single-apostophe's with double-apostrophe's. Null string's will be
     * replaced by an empty string literal (i.e., '').
     */
    public static String toDBString(String normalString) {
        if (null == normalString || 0 == normalString.length())
            return "''";

        StringBuilder buffer = new StringBuilder(normalString.length() + 20);

        char ch;
        buffer.append('\'');
        int len = normalString.length();
        for (int i = 0; i < len; i++) {
            buffer.append(ch = normalString.charAt(i));
            if ('\'' == ch)
                buffer.append('\'');
        }
        buffer.append('\'');

        return buffer.toString();
    }

    /**
     * Converts normal strings into database-ready strings by replacing
     * single-apostophe's with double-apostrophe's.
     *
     * @param inString String NOT NULL
     */
    public static String toDBStringNoQuotes(String inString) {
        StringBuilder buffer = new StringBuilder(inString.length() + 20);
        char ch;
        int len = inString.length();
        for (int i = 0; i < len; i++) {
            buffer.append(ch = inString.charAt(i));
            if ('\'' == ch)
                buffer.append('\'');
        }
        return buffer.toString();
    }

    /**
     * format file size
     */
    public static String toFileSize(int size) {
        int kb = size / 1024;
        if (0 == kb) {
            return size + " bytes";
        }
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(1);
        int mb = kb / 1024;
        return (0 < mb) ? (df.format(kb / 1024F) + " MB") : (10 <= kb) ? (kb + " KB")
                : (df.format(size / 1024F) + " KB");
    }

    /**
     * Returns an uppercase hex String from the argument byte array
     *
     * @return null if the argument is null, the empty string if the argument is
     *         of length 0
     */
    public static String toHexString(byte[] in) {
        if (null == in) {
            return null;
        } else if (in.length == 0) {
            return "";
        }

        final StringBuilder buffer = new StringBuilder(2 * in.length);
        for (byte inByte : in) {
            int nibble = (inByte & 0xF0) >> 4;
            buffer.append((10 > nibble) ? (char) ('0' + nibble) : (char) ('A' + nibble - 10));
            nibble = inByte & 0x0F;
            buffer.append((10 > nibble) ? (char) ('0' + nibble) : (char) ('A' + nibble - 10));
        }
        return buffer.toString();
    }

    /**
     * <PRE>
     * <p/>
     * Converts a uni-code string into a hexadecimal representation of each
     * 16-bit char in the string. For example, "fred" will be converted to
     * "0066007200650064" in hex. You should note that the hi-bytes will get
     * included as low-bytes (thus, expanding the string).
     * <p/>
     * </PRE>
     *
     * @param in String NOT NULL
     */
    public static String toHexString(String in) {
        int len = in.length();
        char[] chars = new char[len];
        StringBuilder out = new StringBuilder();

        int i;
        int j;
        int tempint;
        char tempchar;
        char[] ch = new char[4];

        in.getChars(0, len, chars, 0);
        for (i = 0; i < len; ++i) {
            tempchar = chars[i];
            for (j = 3; 0 <= j; --j) {
                tempint = tempchar & 0x0F;
                ch[j] = (10 > tempint) ? (char) ('0' + tempint) : (char) ('a' + tempint - 10);
                tempchar >>>= 4;
            }
            out.append(ch);
        }

        return out.toString();
    }

    /**
     * This is the same as the toHTMLString, except that it defaults to not
     * using the database format substitutions.
     *
     * @see #toHTMLString(String)
     */
    public static String toHTMLString(String inString) {
        return toHTMLString(inString, false);
    }

    /**
     * <PRE>
     * <p/>
     * Escapes all occurrences of special HTML characters (e.g., <>). It will
     * optionally replace occurrences of single-quotes with double occurences.
     * <p/>
     * </PRE>
     *
     * @param inString    To be converted
     * @param useDBformat Whether to escape sinlge quotes with single quotes
     * @return HTML String or the empty String if the argument is null or empty
     */
    public static String toHTMLString(String inString, boolean useDBformat) {
        if (null == inString || 0 == inString.length())
            return "";

        char[][] subTable = useDBformat ? C_DBHTML_STRING_FROM_CHAR : C_HTML_STRING_FROM_CHAR;
        char[] charArray = inString.toCharArray();
        int inStringLength = charArray.length;
        StringBuilder HTMLBuffer = new StringBuilder(inStringLength * 2);
        for (int i = 0; i < inStringLength; ++i) {
            char ch = charArray[i];
            if (-1 < ch && 128 > ch)
                HTMLBuffer.append(subTable[ch]);
            else
                HTMLBuffer.append(ch);
        }
        return HTMLBuffer.toString();
    }

    public static String toLines(String original, int count, String def) {
        if (null == original)
            return def;
        if (0 > count)
            return original;
        int br = -1;
        for (int i = 0; i < count; i++) {
            br = original.indexOf("\n", br + 1);
            if (-1 == br)
                return original;
            if (i >= count - 1)
                return original.substring(0, br) + ((i >= count - 1) ? "..." : "");
        }
        return original;

    }

    /**
     * Formats date with the default U.S. time zone
     *
     * @param inDate Date NOT NULL
     */
    public static String toMMDDYYYY(Date inDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("M/d/yyyy", Locale.US);
        formatter.setTimeZone(TimeZone.getDefault());

        return formatter.format(inDate);
    }

    /**
     * Formats date
     *
     * @param inDate Date NOT NULL
     */
    public static String toDefaultDateFormat(Date inDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.setTimeZone(TimeZone.getDefault());

        return formatter.format(inDate);
    }

    /**
     * Formats date for mail
     *
     * @param inDate Date NOT NULL
     */
    public static String toMailDateFormat(Date inDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        formatter.setTimeZone(TimeZone.getDefault());
        return formatter.format(inDate);
    }

    /**
     * Formats date for FAQ table
     *
     * @param inDate Date NOT NULL
     */
    public static String toFAQDateFormat(Date inDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        formatter.setTimeZone(TimeZone.getDefault());

        return formatter.format(inDate);
    }

    /**
     * Formats date in reverse order for Tender table
     *
     * @param inDate Date NOT NULL
     */
    public static String toReverseDateFormat(Date inDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        formatter.setTimeZone(TimeZone.getDefault());

        return formatter.format(inDate);
    }

    /**
     * <PRE>
     * <p/>
     * Quotes the following characters with their HTML entity equivalents: < -
     * &lt; " - &quot; & - &amp; Returns the empty string if the argument is
     * null.
     * <p/>
     * </PRE>
     */
    public static String toNonMarkupString(String in) {
        return toNonMarkupString(in, false);
    }

    private final static char[] C_NBSP = "&nbsp;".toCharArray();

    private final static char[] C_SPACE = " ".toCharArray();

    private final static char[] C_LT = "&lt;".toCharArray();

    private final static char[] C_QUOT = "&quot;".toCharArray();

    private final static char[] C_AMP = "&amp;".toCharArray();

    private final static char[] C_QT = "&gt;".toCharArray();

    /**
     * <PRE>
     * <p/>
     * Quotes the following characters with their HTML entity equivalents: < -
     * &lt; " - &quot; & - &amp; Returns the empty string if the argument is
     * null.
     * <p/>
     * </PRE>
     */
    public static String toNonMarkupString(String in, boolean escapeSpace) {
        if (null == in)
            return "";
        int l = in.length();
        if (0 == l)
            return "";
        char[] charArr = in.toCharArray();

        final StringBuilder buffer = new StringBuilder(2 * l);
        for (int i = 0; i < l; i++) {
            char c = charArr[i];
            switch (c) {
                case ' ':
                    buffer.append(escapeSpace ? C_NBSP : C_SPACE);
                    break;
                case '<':
                    buffer.append(C_LT);
                    break;
                case '\"':
                    buffer.append(C_QUOT);
                    break;
                case '&':
                    buffer.append(C_AMP);
                    break;
                case '>':
                    buffer.append(C_QT);
                    break;
                default:
                    buffer.append(c);
            }
        }

        return buffer.toString();
    }

    /**
     * reverce method for toNonMarkupString(String, boolean)
     *
     * @param in
     * @param escapeSpace
     * @return
     * @see #toNonMarkupString(String, boolean)
     */
    public static String toMarkupString(final String in, boolean escapeSpace) {
        if (null == in || 0 == in.length())
            return "";

        String out = in;
        if (escapeSpace)
            out = replaceSubstring(out, "&nbsp;", " ");
        out = replaceSubstring(out, "&lt;", "<");
        out = replaceSubstring(out, "&quot;", "\"");
        out = replaceSubstring(out, "&amp;", "&");
        out = replaceSubstring(out, "&gt;", ">");

        return out;
    }

    public static String toQuotedPrintableString(String in) {
        if (null == in || 1 > in.length())
            return in;

        byte[] bytes = in.getBytes();
        final StringBuilder buffer = new StringBuilder(bytes.length);

        for (int i = 0; i < bytes.length; i++) {
            byte b = bytes[i];
            if ((b > 32 && b < 61) || (b > 61 && b < 127))
                buffer.append((char) b);
            else if (13 == b) // CR
            {
                if (i < bytes.length && 10 == bytes[i + 1]) // CRLF
                {
                    buffer.append("=0D=0A");
                    i++;
                } else
                    buffer.append("=0D");
            } else if (10 == b) // LF
                buffer.append("=0D=0A");
            else {
                buffer.append('=');
                String hex = Integer.toHexString(b).toUpperCase();
                if (1 == hex.length())
                    buffer.append('0');
                buffer.append(hex);
            }
        }

        int back = 0;
        int i = 75;
        while (i < buffer.length()) {
            char c = buffer.charAt(i);
            if ('=' == c) {
                buffer.insert(i, "=\r\n");
                i += 79;
                back = 0;
            } else if ((47 < c && 58 > c) || (64 < c && 71 > c)) {
                if (2 == back) {
                    buffer.insert(i + 2, "=\r\n");
                    i += 81;
                    back = 0;
                } else {
                    i--;
                    back++;
                }
            } else {
                buffer.insert(i + back, "=\r\n");
                i += 79 + back;
                back = 0;
            }
        }
        return buffer.toString();
    }

    public static String toSignedHexString(int i) {
        if (0 > i) {
            return "-" + Long.toHexString(Math.abs(i));
        }
        return Integer.toHexString(i);
    }

    /**
     * <PRE>
     * <p/>
     * This converts a string into title case format. (First character of every
     * word in the string is set to upper case. Every character of every word,
     * following the first one, is set to lower case.) <p/> It doesn't handle
     * words starting off with a punctuation character such as: (red), "red",
     * 'Y' It doesn't handle words delimited by characters other than a space.
     * <p/>
     * </PRE>
     *
     * @param inString NOT NULL
     */
    public static String toTitleCase(String inString) {
        boolean isFirst = true;
        StringBuilder outString = new StringBuilder();
        StringTokenizer st = new StringTokenizer(inString, " ");

        while (st.hasMoreTokens()) {
            if (isFirst) {
                outString.append(toTitleCaseToken(st.nextToken()));
                isFirst = false;
            } else {
                outString.append(" ");
                outString.append(toTitleCaseToken(st.nextToken()));
            }
        }

        return outString.toString();
    }

    /**
     * <PRE>
     * <p/>
     * This converts a single word (token) to title case format. (First
     * character in upper case, rest of the characters of the token in lower
     * case.) <p/> It doesn't handle words starting off with some punctuation
     * character such as: (red), "red", 'Y' It doesn't trim off preceeding or
     * trailing spaces. Thus, use with caution.
     * <p/>
     * </PRE>
     *
     * @param inToken NOT NULL
     */
    private static String toTitleCaseToken(String inToken) {
        return Character.toUpperCase(inToken.charAt(0)) + (inToken.substring(1)).toLowerCase();
    }

    /**
     * Insert the method's description here. Creation date: (18.01.2001
     * 11:38:47)
     */
    public static String toTypedMimeType(String contentType) {
        String result = MIME_MAP.get(contentType);
        return null == result ? "UNRECOGNIZED" : result;
    }

    /**
     * Table-base URL encoder.
     */
    public static String urlEncode(String s) {
        if (null == s)
            return s;
        int anal = s.length();
        if (0 == anal)
            return s;
        StringBuilder buffer = new StringBuilder(2 * anal);
        for (int i = 0; i < anal; i++)
            buffer.append(URL_ENCODINGS[s.charAt(i)]);
        return buffer.toString();
    }

    /**
     * Table-base URL UTF-8 encoder.
     */
    public static String urlEncodeUTF8(String string) {
        try {
            StringBuffer buffer = new StringBuffer();
            for (byte b : string.getBytes("UTF-8"))
                buffer.append(URL_ENCODINGS[(b + 0x100) & 0xFF]);
            return buffer.toString();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static List wordList(String text) {
        String strToRemove[] =
                {"\"", "'", ".", ",", ":", ";", "?", ":", "!", "(", ")" /* ,"*" */};
        return phraseList(removeSubstring(text, strToRemove));
    }

    /**
     * This is a toy string-cipher. Its only purpose is to keep kid sisters and
     * zit-faced-hacker-wannabees from easy-reading. As a word of caution, this
     * can be cracked in seconds -- even on an Atari 400. You should note, that
     * this will cipher the hi-bytes. So, if plan on streaming this string --
     * set the hi-bytes accordingly, or just stream the hi-bytes as well.
     *
     * @param plain Cleartext, NOT NULL
     * @param keys  Sliding cipher, NOT NULL
     */
    public static String xorString(String plain, char[] keys) {
        int len = plain.length();
        char[] cipher = new char[len];
        plain.getChars(0, len, cipher, 0);
        int i;
        for (i = 0; i < len; ++i)
            cipher[i] ^= keys[i % keys.length];
        return new String(cipher);
    }

    /**
     * Escapes ['], [\], ["] simbols in source string for using in javascript
     * expressions. e.g. alert("source string");
     */
    public static String toJavaScriptString(String source) {
        final StringBuilder buf = new StringBuilder();
        for (int i = 0; i < source.length(); i++) {
            char c = source.charAt(i);
            switch (c) {
                case '\'':
                case '\\':
                case '"':
                    buf.append("\\");
            }
            buf.append(c);
        }
        return buf.toString();
    }

    public static String toJavaScriptHTMLString(String source) {
        return toHTMLString(toJavaScriptString(source));
    }

    /**
     * [2] Char ::= #x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] |
     * [#x10000-#x10FFFF]
     *
     * @throws IllegalArgumentException if the character isn't a valid XML Char see
     *                                  http://www.w3.org/TR/REC-xml#charsets
     */
    public static String toXmlChar(int c) throws IllegalArgumentException {
        if (0 > c)
            throw new IllegalArgumentException();
        else if (0x20 > c) {
            if (0x9 == c || 0xA == c || 0xD == c)
                return "&#" + String.valueOf(c) + ";";
            throw new IllegalArgumentException();
        } else if (0x7F >= c)
            switch ((char) c) {
                case '"':
                    return "&quot;";
                case '\'':
                    return "&apos;";
                case '&':
                    return "&amp;";
                case '<':
                    return "&lt;";
                case '>':
                    return "&gt;";
                default:
                    return String.valueOf((char) c);
            }
        else if ((0xD7FF < c && 0xE000 > c) || (0xFFFD < c && 0x10000 > c) || 0x10FFF < c)
            throw new IllegalArgumentException();
        else
            return "&#" + String.valueOf(c) + ";";
    }

    public static String toXmlChars(String str, boolean skipNonChars) {
        if (null == str || 1 > str.length())
            return str;

        int strlen = str.length();

        StringBuilder sResult = new StringBuilder(2 * strlen); // assumes most
        // are ascii

        char[] charr = new char[strlen];
        str.getChars(0, strlen, charr, 0);

        for (int i = 0; i < strlen; i++) {
            int c = charr[i];
            try {
                sResult.append(toXmlChar(c));
            } catch (IllegalArgumentException e) {
                if (!skipNonChars)
                    throw new IllegalArgumentException("Character '" + String.valueOf(c) + "' at postion " + i
                            + " in '" + str + "' violates well-formedness.");
            }
        }

        return sResult.toString();
    }

    /**
     * @return String of <code>count</code> copies of <code>text</code>
     */
    public static String duplicate(String text, int count) {
        if (null == text)
            return null;

        if (0 == count)
            return "";

        char[] tc = text.toCharArray();
        char[] res = new char[tc.length * count];

        for (int i = 0; i < count; ++i)
            System.arraycopy(tc, 0, res, tc.length * i, tc.length);
        return new String(res);
    }

    public static String duplicate(char ch, int count) {
        char[] buffer = new char[count];
        for (int i = 0; i < count; ++i)
            buffer[i] = ch;
        return new String(buffer);
    }

    private static final String TAB_SPACE = "\t";

    public static final String[] EMPTY_STRING_ARRAY = new String[0];

    /**
     * Prepends <code>text</code> by <code>count</code> tab symbols
     */
    public static String tabLeft(String text, int count) {
        return duplicate(TAB_SPACE, count) + text;
    }

    /**
     * breaks <code>inString</code> on the List of strings. Each string is no
     * longer than <code>length</code>.
     */
    public static List breakString(String inString, int length, String delimiters) {
        List<String> result = new ArrayList<String>();
        StringBuilder sb = new StringBuilder(inString);
        while (sb.length() > length) {
            int si = sb.length() < length ? sb.length() : length;
            int i = si - 1;
            int fle = sb.toString().indexOf("\n");
            if (-1 != fle && fle < i) {
                if (0 < fle)
                    result.add(sb.substring(0, fle));
                sb.delete(0, fle + 1);
                continue;
            }
            while ((0 <= i) && (-1 == delimiters.indexOf(sb.charAt(i))))
                i--;

            if (0 > i)
                i = si - 1;

            result.add(sb.substring(0, i + 1));
            sb.delete(0, i + 1);
        }
        result.add(sb.toString());
        return result;
    }

    /**
     * Replace defined characters with another characters.
     *
     * @param original java.lang.String
     * @param chars    java.util.Map [ Character fromChar, Character toChar]
     * @return java.lang.String
     */
    @SuppressWarnings("boxing")
    public static String replaceChars(String original, Map<Character, Character> chars) {
        if (isEmpty(original))
            return original;
        final StringBuilder sb = new StringBuilder(original);
        for (int i = 0; i < sb.length(); i++) {
            final Character toC = chars.get(sb.charAt(i));
            if (null != toC)
                sb.setCharAt(i, toC);
        }
        return sb.toString();
    }

    public static String smartQuotes(String original) {
        return replaceChars(original, _smartQuotes);
    }

    /**
     * Limit string to maxLen characters
     *
     * @param str
     * @param maxLen
     * @return String with no more than maxLen total chars (and ... at the end
     *         if the string is trimmed)
     */
    public static String limit(String str, int maxLen) {
        if (maxLen < 4) {
            return "...";
        }
        if (str.length() <= maxLen) {
            return str;
        }
        return str.substring(0, maxLen - 3) + "...";
    }

    public static String stringEnd(String s, int len) {
        if (!isEmpty(s)) {
            int length = s.length();
            int realLen = Math.min(len, length);
            if (0 != realLen) {
                return s.substring(length - realLen, length);
            }
        }
        return "";
    }

    public static String stringBegin(String s, int len) {
        if (!isEmpty(s)) {
            int length = s.length();
            int realLen = Math.min(len, length);
            if (0 != realLen)
                return s.substring(0, realLen);
        }
        return "";
    }

    public static boolean isLatinLetter(char ch) {
        return ((ch >= 'a') && (ch <= 'z')) || ((ch >= 'A') && (ch <= 'Z'));
    }

    public static boolean isRussianLetter(char ch) {
        return (ch >= 'а' && ch <= 'я') || (ch >= 'А' && ch <= 'Я') || ch == 'ё' || ch == 'Ё';
    }

    public static boolean isDigit(char ch) {
        return ((ch >= '0') && (ch <= '9'));
    }

    public static boolean isNaturalNumber(String s) {
        if (isEmpty(s))
            return false;
        for (char ch : s.toCharArray())
            if (!isDigit(ch))
                return false;
        return true;
    }

    public static boolean isNaturalNumber(String s, int len) {
        return isNaturalNumber(s) && s.length() <= len;
    }

    public static boolean isXMLIdentifier(String s) {
        for (int i = 0; i < s.length(); ++i)
            if (!isLatinLetter(s.charAt(i)) && !isDigit(s.charAt(i)) && (s.charAt(i) != '-') && (s.charAt(i) != '_'))
                return false;
        return true;
    }

    public static String appendToLen(String s, int len) {
        String Result = s;
        while (Result.length() < len)
            Result = "0" + Result;
        return Result;
    }

    public static String appendToLen(int num, int len) {
        return appendToLen(Integer.toString(num), len);
    }

    public static String removeBlanksBetweenWords(String s) {
        StringBuilder result = new StringBuilder(s.length());
        for (int i = 0; i < s.length(); ++i) {
            char charAt = s.charAt(i);
            if ((charAt != ' ') || (i == s.length() - 1) || (s.charAt(i + 1) != ' '))
                result.append(charAt);
        }
        return result.toString();
    }

    public static String capitalize(String s) {
        return capitalize(s, 0);
    }

    public static String capitalize(String s, int i) {
        return changeRegister(s, i, true);
    }

    public static String changeRegister(String s, int i, boolean toUpper) {
        if (s == null)
            return "";
        if (i < 0 || i >= s.length())
            return s;
        char chars[] = s.toCharArray();
        chars[i] = toUpper ? Character.toUpperCase(chars[i]) : Character.toLowerCase(chars[i]);
        return new String(chars);
    }

    public static String[] getterName(String property) {
        String capitalized = capitalize(property);
        return new String[]
                {"get" + capitalized, "is" + capitalized, "list" + capitalized};
    }

    public static String setterName(String property) {
        return "set" + capitalize(property);
    }

    public static boolean isEmpty(String s) {
        return (null == s) || (0 == s.length());
    }

    public static boolean isEmptyTrim(String s) {
        return (null == s) || (0 == s.trim().length());
    }

    public static String stripPrecedingSlash(String s) {
        if (isEmptyTrim(s) || !s.startsWith("/")) return s;
        return (s.length() == 1) ? "" : s.substring(1, s.length());
    }

    private static void joinInOrderWithPrefixSuffix(StringBuilder Result, Object[] objects, String separator,
                                                    boolean forward, String prefix, String suffix) {
        if ((null == objects) || (objects.length == 0))
            return;

        Result.append(prefix);
        if (forward) {
            Result.append(objects[0]);
            Result.append(suffix);
            for (int i = 1; i < objects.length; ++i) {
                Result.append(separator);
                Result.append(prefix);
                Result.append(objects[i]);
                Result.append(suffix);
            }
        } else {
            Result.append(objects[objects.length - 1]);
            Result.append(suffix);
            for (int i = objects.length - 2; i >= 0; --i) {
                Result.append(separator);
                Result.append(prefix);
                Result.append(objects[i]);
                Result.append(suffix);
            }
        }
    }

    private static String joinInOrderWithPrefixSuffix(Object[] objects, String separator, boolean forward,
                                                      String prefix, String suffix) {
        StringBuilder buffer = new StringBuilder();
        joinInOrderWithPrefixSuffix(buffer, objects, separator, forward, prefix, suffix);
        return buffer.toString();
    }

    private static String joinInOrder(Collection objects, String separator, boolean forward) {
        StringBuilder buffer = new StringBuilder();
        joinInOrder(buffer, objects, separator, forward);
        return buffer.toString();
    }

    private static void joinInOrder(StringBuilder buffer, Collection objects, String separator, boolean forward) {
        if (null != objects)
            joinInOrderWithPrefixSuffix(buffer, objects.toArray(), separator, forward, "", "");
    }

    public static String joinReversed(Collection names, String separator) {
        return joinInOrder(names, separator, false);
    }

    public static String join(Collection names, String separator) {
        return joinInOrder(names, separator, true);
    }

    public static void join(StringBuilder buffer, Collection names, String separator) {
        joinInOrder(buffer, names, separator, true);
    }

    public static String join(Object[] objects, String separator) {
        return joinInOrderWithPrefixSuffix(objects, separator, true, "", "");
    }

    public static void join(StringBuilder buffer, Object[] objects, String separator) {
        joinInOrderWithPrefixSuffix(buffer, objects, separator, true, "", "");
    }

    public static String joinReversed(Object[] objects, String separator) {
        return joinInOrderWithPrefixSuffix(objects, separator, false, "", "");
    }

    public static String joinSingleQuoted(Object[] objects, String separator) {
        return joinInOrderWithPrefixSuffix(objects, separator, true, "'", "'");
    }

    public static String transliterate(String strRU) {
        StringBuilder strEN = new StringBuilder();
        char liter;
        for (int i = 0; i < strRU.length(); ++i) {
            liter = strRU.charAt(i);
            strEN.append(transliterateChar(liter));
        }

        return strEN.toString();
    }

    private static char[] transliterateChar(char charRU) {
        if (charRU >= 'А' && charRU <= 'ё' || charRU == 'Ё') {
            char[] charEN;
            boolean upperCase = Character.isUpperCase(charRU);

            switch (Character.toLowerCase(charRU)) {
                case 'а':
                    charEN = new char[]
                            {'a'};
                    break;
                case 'б':
                    charEN = new char[]
                            {'b'};
                    break;
                case 'в':
                    charEN = new char[]
                            {'v'};
                    break;
                case 'г':
                    charEN = new char[]
                            {'g'};
                    break;
                case 'д':
                    charEN = new char[]
                            {'d'};
                    break;
                case 'е':
                    charEN = new char[]
                            {'e'};
                    break;
                case 'ё':
                    charEN = new char[]
                            {'y', 'o'};
                    break;
                case 'ж':
                    charEN = new char[]
                            {'j'};
                    break;
                case 'з':
                    charEN = new char[]
                            {'z'};
                    break;
                case 'и':
                    charEN = new char[]
                            {'i'};
                    break;
                case 'й':
                    charEN = new char[]
                            {'y'};
                    break;
                case 'к':
                    charEN = new char[]
                            {'k'};
                    break;
                case 'л':
                    charEN = new char[]
                            {'l'};
                    break;
                case 'м':
                    charEN = new char[]
                            {'m'};
                    break;
                case 'н':
                    charEN = new char[]
                            {'n'};
                    break;
                case 'о':
                    charEN = new char[]
                            {'o'};
                    break;
                case 'п':
                    charEN = new char[]
                            {'p'};
                    break;
                case 'р':
                    charEN = new char[]
                            {'r'};
                    break;
                case 'с':
                    charEN = new char[]
                            {'s'};
                    break;
                case 'т':
                    charEN = new char[]
                            {'t'};
                    break;
                case 'у':
                    charEN = new char[]
                            {'u'};
                    break;
                case 'ф':
                    charEN = new char[]
                            {'f'};
                    break;
                case 'х':
                    charEN = new char[]
                            {'h'};
                    break;
                case 'ц':
                    charEN = new char[]
                            {'t', 's'};
                    break;
                case 'ч':
                    charEN = new char[]
                            {'c', 'h'};
                    break;
                case 'ш':
                    charEN = new char[]
                            {'s', 'h'};
                    break;
                case 'щ':
                    charEN = new char[]
                            {'t', 's', 'h'};
                    break;
                case 'ъ':
                    charEN = new char[]
                            {'\''};
                    break;
                case 'ы':
                    charEN = new char[]
                            {'y'};
                    break;
                case 'ь':
                    charEN = new char[]
                            {'\''};
                    break;
                case 'э':
                    charEN = new char[]
                            {'e'};
                    break;
                case 'ю':
                    charEN = new char[]
                            {'y', 'u'};
                    break;
                case 'я':
                    charEN = new char[]
                            {'y', 'a'};
                    break;
                default:
                    charEN = new char[]
                            {charRU};
                    break;
            }

            if (upperCase) {
                charEN[0] = Character.toUpperCase(charEN[0]);
            }

            return charEN;
        }
        return new char[]
                {charRU};
    }

    public static String toFileName(String filename) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < filename.length(); ++i) {
            char liter = filename.charAt(i);
            if (liter == '/' || liter == '\\' || liter == ':' || liter == '|')
                result.append('-');
            else if (liter == '"')
                result.append('\'');
            else if (liter == '*' || liter == '?' || liter == '>' || liter == '<')
                result.append('_');
            else
                result.append(liter);
        }
        return result.toString();
    }

    public static boolean toBoolean(String s) {
        if (null != s) {
            String upperCased = s.toUpperCase();
            return upperCased.startsWith("YES") || upperCased.startsWith("ДА") || upperCased.startsWith("TRUE");
        }
        return false;
    }

    public static boolean equals(String s1, String s2) {
        if (s1 == s2)
            return true;
        else if ((null == s1) || (null == s2))
            return false;
        else
            return s1.equals(s2);
    }

    public static String[] concatenate(String[] a, String[] b) {
        String[] result = new String[a.length + b.length];
        int index = 0;
        for (int i = 0; i < a.length; ++i)
            result[index++] = a[i];
        for (int i = 0; i < b.length; ++i)
            result[index++] = b[i];
        return result;
    }

    public static String[] concatenate(String[] a, String b) {
        String[] result = new String[a.length + 1];

        /* i think it's not a good practice ;)
         for ( int i = 0; i < a.length; ++i )
         result[i] = a[i];
         */
        System.arraycopy(a, 0, result, 0, a.length);
        result[a.length] = b;
        return result;
    }

    public static String[] splitByDelimiters(String str, String delimiters) {
        List<String> result = new ArrayList<String>();
        splitByDelimiters(str, delimiters, result);
        return result.toArray(new String[result.size()]);
    }

    public static void splitByDelimiters(String str, String delimiters, List<String> result) {
        StringTokenizer tknStr = new StringTokenizer(str, delimiters);
        while (tknStr.hasMoreTokens())
            result.add(tknStr.nextToken());
    }

    public static String normText(String text) {
        text = text.replaceAll("<([^>]+)>", " ");
        text = text.replaceAll("([^ёа-яЁА-Я\\w]+)", " ");
        return text;
    }

    //removes HTML tags( yes, "removes" not "replaces with spaces", feel the difference! )
    // but keeps HTML Character Entities such as &quot; etc. and punctuation
    public static String normTextKeepHtmlCharEntities(String text) {
        text = text.replaceAll("<([^>]+)>", "");
        text = text.replaceAll("([^ёа-яЁА-Я\\w\\p{Punct}]+)", " ");
        return text;
    }

    public static String[] toStringArray(Object object) {
        if (object == null) {
            return new String[0];
        } else if (object instanceof String[]) {
            return (String[]) object;
        } else if (object instanceof Collection<?>) {
            Collection<?> col = (Collection<?>) object;
            return col.toArray(new String[col.size()]);
        } else
            return new String[]
                    {(String) object};
    }

    /**
     * Trims unicode leading\trailing whitespace symbols
     *
     * @param str
     * @return String without leading\trailing space symbols or empty("") String
     *         when there are only space symbols in <param>str</param>
     */
    public static String trimWhitespaces(String str) {
        char[] chars = str.toCharArray();
        int last = str.length();

        int first = 0;
        while ((first < last) && (Character.isWhitespace(chars[first])))
            ++first;

        while ((first < last) && (Character.isWhitespace(chars[last - 1])))
            --last;

        return ((first > 0) || (last < str.length())) ? str.substring(first, last) : str;
    }

    public static String toUpperCase(String s) {
        return (null != s) ? s.toUpperCase() : null;
    }

    public static String toString(Object object) {
        if (null != object)
            return object.toString();
        return null;
    }

    public static String[] removeEmptyStrings(String[] strings) {
        List<String> result = new ArrayList<String>();
        for (int i = 0; i < strings.length; ++i)
            if (!StringUtils.isEmpty(strings[i]))
                result.add(strings[i]);
        return toStringArray(result);
    }

    public static String extractBounded(String value, final String beg, final String end) {
        int i = value.indexOf(beg) + beg.length();
        if (i < 0)
            return null;
        int j = value.indexOf(end, i);
        if (j < 0)
            return null;
        return value.substring(i, j);
    }

    public static String handleNull(String str) {
        if (null == str)
            return "";
        return str;
    }

    /**
     * @param src
     * @return result
     * @see StringUtils#breakLongWords(String, int)
     */
    public static String breakLongWords(String src) {
        return breakLongWords(src, 39);
    }

    /**
     * Преобразует исходную строку так, чтобы в ней не было слишком длинных слов,
     * разрезая пробелом такие слова на несколько частей.
     * <p/>
     * Modify src-string so that it contains no words with length greater than specified(maxWordLength).
     * Inserts spaces in too long words, splitting them by two or more short words.
     * <p/>
     * Method can be used for ... (чтобы колонки по ширине не разъезжались)
     *
     * @param src           sorce string
     * @param maxWordLength maximum word' length
     * @return result
     */
    public static String breakLongWords(String src, int maxWordLength) {
        int len = src.length();
        int i;
        int seqStart = 0;
        boolean inWord = false;
        String delimiters = " \n\t\r";
        StringBuilder result = new StringBuilder(len + 10);

        for (i = 0; i <= len; ++i) {
            char c = (i < len) ? src.charAt(i) : ' ';
            boolean isDelim = (delimiters.indexOf(c) >= 0);

            if (inWord && isDelim) {
                String word = src.substring(seqStart, i);
                if (word.length() > maxWordLength) {
                    result.append(breakSingleLongWord(word, maxWordLength));
                } else {
                    result.append(word);
                }
                seqStart = i;
                inWord = false;
            } else if (!inWord && !isDelim) {
                result.append(src.substring(seqStart, i));
                seqStart = i;
                inWord = true;
            } else if (!inWord && i == len) {
                result.append(src.substring(seqStart));
            }
        }

        return result.toString();
    }

    private static String breakSingleLongWord(String word, int maxWordLength) {
        int len = word.length();
        if (len == 0) {
            return word;
        }

        int parts = len / maxWordLength;
        if (parts * maxWordLength < len) {
            ++parts;
        }

        int partlen = len / parts;
        if (partlen * parts < len) {
            ++partlen;
        }

        StringBuilder result = new StringBuilder(len + parts);

        int pos = 0;
        while (pos < len) {
            if (pos > 0) {
                result.append(' ');
            }
            result.append(word.substring(pos, pos + partlen < len ? pos + partlen : len));
            pos += partlen;
        }

        return result.toString();
    }

    public static boolean containsRussianLetter(String s) {
        if (s == null)
            return false;
        for (char ch : s.toCharArray())
            if (isRussianLetter(ch))
                return true;
        return false;
    }

    public static String reverse(String s) {
        return new StringBuilder(s).reverse().toString();
    }

    public static String carryOver(String s, int t) {
        String res = "";
        int len = 0;
        for (char ch : s.toCharArray())
            if (Character.isWhitespace(ch) && len >= t) {
                res += "\n";
                len = 0;
            } else {
                ++len;
                res += ch;
            }
        return res;
    }

    public static String joinWrap(Collection<String> list, String prefix, String suffix, String separator) {
        list = wrap(list, prefix, suffix);
        return join(list, separator);
    }

    private static List<String> wrap(Collection<String> list, String prefix, String suffix) {
        List<String> result = new ArrayList<String>();
        for (String s : list)
            result.add(prefix + s + suffix);
        return result;
    }
}
