package ru.ml.utils.classprocessor;

import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.utils.collections.Closure;
import ru.ml.utils.reflection.ReflectionUtils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static ru.ml.utils.collections.Collections.each;

/**
 * Класс для прохлода по файлдам другого класса с поддержкой иерархии
 */
public class AnnotationClassReader {
    
    private static final Logger log = LoggerFactory.getLogger(AnnotationClassReader.class);
    
    private Collection<FieldDescriptor> fDescriptors = new ArrayList<FieldDescriptor>();
    
    public static final class FieldDescriptor {
        private Field field;
        
        private PropertyDescriptor propertyDescriptor;

        public FieldDescriptor(Field field, PropertyDescriptor propertyDescriptor) {
            this.field = field;
            this.propertyDescriptor = propertyDescriptor;
        }

        public Field getField() {
            return field;
        }

        public PropertyDescriptor getPropertyDescriptor() {
            return propertyDescriptor;
        }
        
        public Annotation[] listAnnotation() {
            Annotation[] fieldAnnotation = field.getDeclaredAnnotations();

            try {
                Annotation[] getterAnnotation = propertyDescriptor.getReadMethod().getDeclaredAnnotations();

                return (Annotation[]) ArrayUtils.addAll(fieldAnnotation, getterAnnotation);
            } catch (Exception e) {
                log.warn("Error while getting getter annotations {}", e.getLocalizedMessage());
            }

            return fieldAnnotation;
        }
    }
    
    private Class<?> targetClass;
    
    private ClassLoader targetClassLoader;

    public AnnotationClassReader(Class<?> targetClass, ClassLoader targetClassLoader) {
        this.targetClass = targetClass;
        this.targetClassLoader = targetClassLoader;

        createFieldCache();
    }
    
    private void createFieldCache() {  
        if(fDescriptors.isEmpty())  {
            try {
                targetClass = targetClassLoader.loadClass(targetClass.getName());

                Field[] fields = ReflectionUtils.getDeclaredAndInheritedFields(targetClass, false);
                BeanInfo beanInfo = Introspector.getBeanInfo(targetClass);

                Map<String, PropertyDescriptor> descriptorMap = new HashMap<String, PropertyDescriptor>();

                PropertyDescriptor[] descriptors = beanInfo.getPropertyDescriptors();
                for(PropertyDescriptor descriptor : descriptors) {
                    descriptorMap.put(descriptor.getName(), descriptor);
                }


                for (Field field : fields) {
                    FieldDescriptor fieldDescriptor = new FieldDescriptor(field, descriptorMap.get(field.getName()));
        
                    fDescriptors.add(fieldDescriptor);
                }
            } catch (ClassNotFoundException e) {
                log.warn("Class not found {}", e.getLocalizedMessage());
            } catch (IntrospectionException e) {
                log.warn("Bean currupted {}", e.getLocalizedMessage());
            }
        }
    }


    public void eachField(Closure<Object, FieldDescriptor> closure) {
        each(fDescriptors, closure);
    }
}
