package ru.ml.utils.collections;

public interface Closure<R, A> {
    public R exec(A arg);
}
