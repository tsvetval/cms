package ru.ml.utils.collections;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Collections {

    private static final DateFormat df = new SimpleDateFormat("dd.MM.yyyy");

    public static <S extends Comparable<S>> Range<S> Range(S from, S to) {
        return new Range<S>(from, to);
    }

    public static Date Date(String date) {
        try {
            return df.parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> List<T> List(T... elems) {
        return new ArrayList<T>(Arrays.asList(elems));
    }

    public static <T> T[] Array(T... elems) {
        return elems;
    }

    public static <T> Set<T> Set(T... elems) {
        return new HashSet<T>(List(elems));
    }


    public static <T> Collection<T> filter(Collection<T> source, Closure<Boolean, T> filter) {
        Collection<T> filteredElements = new ArrayList<T>();

        for (T item : source) {
            if (filter.exec(item)) {
                filteredElements.add(item);
            }
        }
        return filteredElements;
    }

    public static <T, S> Collection<T> convert(Collection<S> source, Closure<T, S> converter) {
        Collection<T> convertedElements = new ArrayList<T>();

        for (S item : source) {
            T el = converter.exec(item);
            if (el != null)
                convertedElements.add(el);
        }
        return convertedElements;
    }

    public static <T> void each(Collection<T> source, Closure<?, T> visitor) {

        for (T item : source) {
            visitor.exec(item);
        }
    }
}
