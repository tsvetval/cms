package ru.ml.utils.collections;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Данный класс используется для преобразования итерайбл объектов на основе преобразователя.
 */
public class IterableWrapper<T, K> implements Iterable<T> {

    private final Iterable<K> sourceIterable;

    private final Closure<T, K> transformer;

    public IterableWrapper(Iterable<K> sourceIterable, Closure<T, K> transformer) {
        this.sourceIterable = sourceIterable;
        this.transformer = transformer;
    }

    /**
     * Returns an iterator over a set of elements of type T.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator<T> iterator() {
        final Iterator<K> sourceIterator = sourceIterable.iterator();

        return new Iterator<T>() {

            private T nextElement = null;
            private Boolean hasNext = null;


            @Override
            public boolean hasNext() {
                if (hasNext == null){
                    hasNext = sourceIterator.hasNext();
                    if(hasNext) {
                        try {
                            nextElement = internalNext();
                        } catch (NoSuchElementException e) {
                            hasNext = false;
                            return false;
                        }
                    }
                }
                return hasNext && nextElement != null;
            }


            @Override
            public T next() {
                hasNext = null;
                return nextElement;
            }

            public T internalNext() {
                try {
                    K nextElement = sourceIterator.next();
                    T result = transformer.exec(nextElement);
                    if(result != null){
                        return result;
                    } else if (sourceIterator.hasNext()) {
                        return internalNext();
                    }
                } catch (Exception e) {
                    if(sourceIterator.hasNext()) {
                        return internalNext();
                    }
                }

                throw new NoSuchElementException();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }
}
