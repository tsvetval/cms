package ru.ml.utils.collections;

//import ru.ml.utils.annotation.Priority;

import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: dmitry
 * Date: 19.10.11
 * Time: 8:39
 *
 * compare two objects according to priority annotation
 */
public class PriorityComparator<T> implements Comparator<T>{

    /**
     * returns new PriorityComparator for class
     * @return PriorityComparator
     */
    public static  <D> PriorityComparator<D> getComparator(){
        return new PriorityComparator<D>();
    }

    @Override
    public int compare(T o, T o1) {
        int oPriority = 100;
        int o1Priority = 100;

//        if (o.getClass().isAnnotationPresent(Priority.class)){
//            oPriority = o.getClass().getAnnotation(Priority.class).value();
//        }
//
//        if (o1.getClass().isAnnotationPresent(Priority.class)){
//            o1Priority = o1.getClass().getAnnotation(Priority.class).value();
//        }
        if (oPriority == o1Priority){
            return 0;
        } else if (oPriority > o1Priority){
            return -1;
        } else {
            return 1;
        }
    }
}
