package ru.ml.utils.collections;

import ru.ml.utils.collections.seq.Sequence;

import java.util.Iterator;

public class Range<T extends Comparable<T>> implements Iterable<T> {

    private T from = null;
    private T to = null;

    public Range(T start, T end) {
        this.from = start;
        this.to = end;
    }

    public boolean contains(T value) {
        return from.compareTo(value) <= 0 && to.compareTo(value) >= 0;
    }

    @Override
    public Iterator<T> iterator() {
        Sequence<T> sequence = getSequence(from);

        return new RangeIterator<T>(sequence, to);
    }

    @SuppressWarnings({"unchecked"})
    private Sequence<T> getSequence(T from) {
        Sequence<T> sequence;

        String className = "ru.daydev.core.utils.collections.seq." + from.getClass().getSimpleName() + "Sequence";
        try {
            Class clazz = Class.forName(className);

            sequence = (Sequence<T>) clazz.getDeclaredConstructor(from.getClass()).newInstance(from);

        } catch (Exception e) {
            throw new RuntimeException("No Sequence found for type " + from.getClass());
        }

        return sequence;
    }
}