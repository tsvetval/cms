package ru.ml.utils.collections.seq;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateSequence implements Sequence<Date> {
    private Date value = null;

    public DateSequence(Date value) {
        this.value = value;
    }

    public Date value() {
        return value;
    }

    public Sequence<Date> next() {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(value);
        cal.add(Calendar.DATE, 1);

        return new DateSequence(cal.getTime());
    }

    public Sequence<Date> previous() {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(value);
        cal.roll(Calendar.DATE, 1);

        return new DateSequence(cal.getTime());
    }
}