package ru.ml.utils.collections.seq;

public interface Sequence<T> {

    public T value();

    public Sequence<T> next();

    public Sequence<T> previous();
}
