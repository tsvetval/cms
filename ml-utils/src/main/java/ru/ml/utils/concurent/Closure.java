package ru.ml.utils.concurent;

public interface Closure<T> {
    void invoke(T result);
}