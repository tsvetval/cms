package ru.ml.utils.concurent;

import java.util.Iterator;

public class None<T> extends Option<T> {

    @Override
    public boolean isDefined() {
        return false;
    }

    @Override
    public T get() {
        throw new IllegalStateException("No value");
    }

    public Iterator<T> iterator() {
        return java.util.Collections.<T>emptyList().iterator();
    }

    @Override
    public String toString() {
        return "None";
    }
}
