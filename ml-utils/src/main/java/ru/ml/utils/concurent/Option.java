package ru.ml.utils.concurent;

public abstract class Option<T> implements Iterable<T> {

    public abstract boolean isDefined();

    public abstract T get();

    public static <T> Some<T> Some(T value) {
        return new Some<T>(value);
    }
}
