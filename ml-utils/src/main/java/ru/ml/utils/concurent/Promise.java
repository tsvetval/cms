package ru.ml.utils.concurent;

import ru.ml.utils.collections.Collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;

public class Promise<V> implements Future<V>, Closure<V> {

    private final CountDownLatch taskLock = new CountDownLatch(1);

    public boolean cancel(boolean mayInterruptIfRunning) {
        return false;
    }

    public boolean isCancelled() {
        return false;
    }

    public boolean isDone() {
        return invoked;
    }

    public V getOrNull() {
        return result;
    }

    public V get() throws InterruptedException, ExecutionException {
        taskLock.await();
        return result;
    }

    public V get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        taskLock.await(timeout, unit);
        return result;
    }

    private List<Closure<Promise<V>>> callbacks = new ArrayList<Closure<Promise<V>>>();
    private boolean invoked = false;
    private V result = null;

    public void invoke(V result) {
        synchronized (this) {
            if (!invoked) {
                invoked = true;
                this.result = result;
                taskLock.countDown();
            } else {
                return;
            }
        }
        for (Closure<Promise<V>> callback : callbacks) {
            callback.invoke(this);
        }
    }

    public void onRedeem(Closure<Promise<V>> callback) {
        synchronized (this) {
            if (!invoked) {
                callbacks.add(callback);
            }
        }
        if (invoked) {
            callback.invoke(this);
        }
    }

    public static <T> Promise<List<T>> waitAll(final Promise<T>... promises) {
        return waitAll(Collections.List(promises));
    }

    public static <T> Promise<List<T>> waitAll(final Collection<Promise<T>> promises) {
        final CountDownLatch waitAllLock = new CountDownLatch(promises.size());
        final Promise<List<T>> result = new Promise<List<T>>() {

            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                boolean r = true;
                for (Promise<T> f : promises) {
                    r = r & f.cancel(mayInterruptIfRunning);
                }
                return r;
            }

            @Override
            public boolean isCancelled() {
                boolean r = true;
                for (Promise<T> f : promises) {
                    r = r & f.isCancelled();
                }
                return r;
            }

            @Override
            public boolean isDone() {
                boolean r = true;
                for (Promise<T> f : promises) {
                    r = r & f.isDone();
                }
                return r;
            }

            @Override
            public List<T> get() throws InterruptedException, ExecutionException {
                waitAllLock.await();
                List<T> r = new ArrayList<T>();
                for (Promise<T> f : promises) {
                    r.add(f.get());
                }
                return r;
            }

            @Override
            public List<T> get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
                waitAllLock.await(timeout, unit);
                return get();
            }
        };
        final Closure<Promise<T>> action = new Closure<Promise<T>>() {

            public void invoke(Promise<T> completed) {
                waitAllLock.countDown();
                if (waitAllLock.getCount() == 0) {
                    try {
                        result.invoke(result.get());
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        };
        for (Promise<T> f : promises) {
            f.onRedeem(action);
        }
        return result;
    }



    public static <T> Promise<T> waitAny(final Promise<T>... futures) {
        final Promise<T> result = new Promise<T>();

        final Closure<Promise<T>> action = new Closure<Promise<T>>() {

            public void invoke(Promise<T> completed) {
                synchronized (this) {
                    if (result.isDone()) {
                        return;
                    }
                }
                result.invoke(completed.getOrNull());
            }
        };

        for (Promise<T> f : futures) {
            f.onRedeem(action);
        }

        return result;
    }
}