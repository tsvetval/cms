package ru.ml.utils.concurent;

import java.util.Iterator;

public class Some<T> extends Option<T> {

    final T value;

    public Some(T value) {
        this.value = value;
    }

    @Override
    public boolean isDefined() {
        return true;
    }

    @Override
    public T get() {
        return value;
    }

    public Iterator<T> iterator() {
        return java.util.Collections.singletonList(value).iterator();
    }

    @Override
    public String toString() {
        return "Some(" + value + ")";
    }
}