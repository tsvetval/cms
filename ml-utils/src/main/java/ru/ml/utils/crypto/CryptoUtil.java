package ru.ml.utils.crypto;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CryptoUtil {

    private static final String BLOWFISH_ECB_PKCS5_PADDING = "Blowfish/ECB/PKCS5Padding";
    private static final String UTF_8 = "UTF-8";
    private static final String BLOW_FISH = "Blowfish";

    public static final String CRYPT_KEY = "GGFJNNNDKBBBJHDBJJJDBBBJ";

    static final char[] HEX_CHARS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};


    /**
     * Sign a message with a key
     *
     * @param message The message to sign
     * @param key     The key to use
     * @return The signed message (in hexadecimal)
     */
    public static String sign(String message, String key) {
        return sign(message, hexStringToByte(key));
    }

    /**
     * Sign a message with a key
     *
     * @param message The message to sign
     * @param key     The key to use
     * @return The signed message (in hexadecimal)
     */
    public static String sign(String message, byte[] key) {

        if (key.length == 0) {
            return message;
        }

        try {
            Mac mac = Mac.getInstance("HmacSHA1");
            SecretKeySpec signingKey = new SecretKeySpec(key, "HmacSHA1");
            mac.init(signingKey);
            byte[] messageBytes = message.getBytes("utf-8");
            byte[] result = mac.doFinal(messageBytes);
            int len = result.length;
            char[] hexChars = new char[len * 2];


            for (int charIndex = 0, startIndex = 0; charIndex < hexChars.length; ) {
                int bite = result[startIndex++] & 0xff;
                hexChars[charIndex++] = HEX_CHARS[bite >> 4];
                hexChars[charIndex++] = HEX_CHARS[bite & 0xf];
            }
            return new String(hexChars);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

    }

    /**
     * Create an MD5 password hash
     *
     * @param input The password
     * @return The password hash
     */
    public static String passwordHash(String input) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            byte[] out = m.digest(input.getBytes());
            return new String(Base64.encodeBase64(out));
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    /**
     * Encrypt a String with the AES encryption standard. Private key must have a length of 16 bytes
     *
     * @param value      The String to encrypt
     * @param privateKey The key used to encrypt
     * @return An hexadecimal encrypted string
     */
    public static String encryptAES(String value, String privateKey) {
        try {
            byte[] raw = privateKey.getBytes();
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            return byteToHexString(cipher.doFinal(value.getBytes()));
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Decrypt a String with the AES encryption standard. Private key must have a length of 16 bytes
     *
     * @param value      An hexadecimal encrypted string
     * @param privateKey The key used to encrypt
     * @return The decrypted String
     */
    public static String decryptAES(String value, String privateKey) {
        try {
            byte[] raw = privateKey.getBytes();
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            return new String(cipher.doFinal(hexStringToByte(value)));
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public static String cryptText(String text, String key) throws Exception {
        Key cipherKey = getKeyFromString(key);

        Cipher cipher = Cipher.getInstance(BLOWFISH_ECB_PKCS5_PADDING);
        cipher.init(Cipher.ENCRYPT_MODE, cipherKey);

        byte[] cryptedText = cipher.doFinal(text.getBytes(UTF_8));

        return byteToHexString(cryptedText);
    }

    public static String cryptText(String text) throws Exception {
        KeyGenerator keyGenerator = KeyGenerator.getInstance(BLOW_FISH);
        keyGenerator.init(128);

        Key key = keyGenerator.generateKey();

        Cipher cipher = Cipher.getInstance(BLOWFISH_ECB_PKCS5_PADDING);
        cipher.init(Cipher.ENCRYPT_MODE, key);

        byte[] cryptedText = cipher.doFinal(text.getBytes(UTF_8));

        return byteToHexString(cryptedText);
    }

    public static String decryptText(String encryptedText, String key) throws Exception {

        byte[] encryptedBytes = hexStringToByte(encryptedText);

        Key cipherKey = getKeyFromString(key);
        Cipher cipher = Cipher.getInstance(BLOWFISH_ECB_PKCS5_PADDING);

        cipher.init(Cipher.DECRYPT_MODE, cipherKey);
        byte[] decryptedText = cipher.doFinal(encryptedBytes);

        return new String(decryptedText, UTF_8);
    }

    private static Key getKeyFromString(String key) throws UnsupportedEncodingException {
        byte[] rawKeyBytes = key.getBytes(UTF_8);
        // On some systems blowfish support only 16byte key (128bit) on my mac work fine with 56byte key, WTF?
        byte[] rawKey = new byte[16];  // max blowfish key is 448 bits
        for (int i = 0, j = 0; i < 16; i++) {
            if (j < rawKeyBytes.length) {
                rawKey[i] = rawKeyBytes[j];
            } else {
                j = 0;
                rawKey[i] = rawKeyBytes[j];
            }
            j++;
        }

        return new SecretKeySpec(rawKey, BLOW_FISH);
    }

    /**
     * Write a byte array as hexadecimal String.
     *
     * @param bytes bytes
     * @return hex string
     */
    private static String byteToHexString(byte[] bytes) {
        return String.valueOf(Hex.encodeHex(bytes));
    }

    /**
     * Transform an hexadecimal String to a byte array.
     *
     * @param hexString hex string AFFBBDD234FF
     * @return bytes
     */
    private static byte[] hexStringToByte(String hexString) {
        try {
            return Hex.decodeHex(hexString.toCharArray());
        } catch (DecoderException e) {
            throw new RuntimeException(e);
        }
    }
}
