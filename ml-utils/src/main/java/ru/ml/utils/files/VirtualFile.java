package ru.ml.utils.files;

import org.apache.commons.io.FileUtils;

import java.io.File;

public class VirtualFile {

    private File file;

    public VirtualFile(File file) {
        this.file = file;
    }

    public String getFileContent() {
        try {
            return FileUtils.readFileToString(file, "UTF-8");
        } catch (Exception e) {
            return "";
        }
    }

    public String getRelativePath(File parent) {
        return file.getAbsolutePath().substring(parent.getAbsolutePath().length());
    }
}
