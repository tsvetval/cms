package ru.ml.utils.http;

/**
 */
public interface HTTPClient {
   public HTTPClientUtils.HTTPRequest newRequest(String url);
   public void stop();
}
