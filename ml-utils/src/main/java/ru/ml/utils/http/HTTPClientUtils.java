package ru.ml.utils.http;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.NotImplementedException;
import ru.ml.utils.concurent.Promise;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Simple HTTP client to make requests..
 */
public class HTTPClientUtils {

    private static HTTPClient client = new HTTPFectchClient();

    public static HTTPClient getClient() {
        return client;
    }

    public enum Scheme {
        BASIC, DIGEST, NTLM, KERBEROS, SPNEGO
    }

    /**
     * URL-encode an UTF-8 string to be used as a query string parameter.
     *
     * @param part string to encode
     * @return url-encoded string
     */
    public static String encode(String part) {
        try {
            return URLEncoder.encode(part, "utf-8");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static abstract class HTTPRequest {
        public String url;
        public String username;
        public String password;
        public Scheme scheme;
        public Object body;
        public FileParam[] fileParams;
        public Map<String, String> headers = new HashMap<String, String>();
        public Map<String, Object> parameters = new HashMap<String, Object>();
        public String mimeType;
        public boolean followRedirects = true;
        /**
         * timeout: value in seconds
         */
        public Long timeout = 60l;

        public HTTPRequest() {
        }

        public HTTPRequest(String url) {
            this.url = url;
        }

        /**
         * Add a MimeType to the web service request.
         *
         * @param mimeType
         * @return the HTTPRequest for chaining.
         */
        public HTTPRequest mimeType(String mimeType) {
            this.mimeType = mimeType;
            return this;
        }

        /**
         * define client authentication for a server host
         * provided credentials will be used during the request
         *
         * @param username
         * @param password
         * @return the HTTPRequest for chaining.
         */
        public HTTPRequest authenticate(String username, String password, Scheme scheme) {
            this.username = username;
            this.password = password;
            this.scheme = scheme;
            return this;
        }

        /**
         * define client authentication for a server host
         * provided credentials will be used during the request
         * the basic scheme will be used
         *
         * @param username
         * @param password
         * @return the HTTPRequest for chaining.
         */
        public HTTPRequest authenticate(String username, String password) {
            return authenticate(username, password, Scheme.BASIC);
        }

        /**
         * Indicate if the WS should continue when hitting a 301 or 302
         *
         * @return the HTTPRequest for chaining.
         */
        public HTTPRequest followRedirects(boolean value) {
            this.followRedirects = value;
            return this;
        }

        /**
         * Set the value of the request timeout, i.e. the number of seconds before cutting the
         * connection - default to 60 seconds
         *
         * @param timeout the timeout value, e.g. "30s", "1min"
         * @return the HTTPRequest for chaining
         */
        public HTTPRequest timeout(String timeout) /*throws InvalidDurationException*/ {
            throw new RuntimeException("Not implemented");
            //this.timeout = DurationUtil.getDuration(timeout);
            // return this;
        }

        /**
         * Add files to request. This will only work with POST or PUT.
         *
         * @param files
         * @return the HTTPRequest for chaining.
         */
        public HTTPRequest files(File... files) {
            this.fileParams = FileParam.getFileParams(files);
            return this;
        }

        /**
         * Add fileParams aka File and Name parameter to the request. This will only work with POST or PUT.
         *
         * @param fileParams
         * @return the HTTPRequest for chaining.
         */
        public HTTPRequest files(FileParam... fileParams) {
            this.fileParams = fileParams;
            return this;
        }

        /**
         * Add the given body to the request.
         *
         * @param body
         * @return the HTTPRequest for chaining.
         */
        public HTTPRequest body(Object body) {
            this.body = body;
            return this;
        }

        /**
         * Add a header to the request
         *
         * @param name  header name
         * @param value header value
         * @return the HTTPRequest for chaining.
         */
        public HTTPRequest setHeader(String name, String value) {
            this.headers.put(name, value);
            return this;
        }

        /**
         * Add a parameter to the request
         *
         * @param name  parameter name
         * @param value parameter value
         * @return the HTTPRequest for chaining.
         */
        public HTTPRequest setParameter(String name, String value) {
            this.parameters.put(name, value);
            return this;
        }

        public HTTPRequest setParameter(String name, Object value) {
            this.parameters.put(name, value);
            return this;
        }

        /**
         * Use the provided headers when executing request.
         *
         * @param headers
         * @return the HTTPRequest for chaining.
         */
        public HTTPRequest headers(Map<String, String> headers) {
            this.headers = headers;
            return this;
        }

        /**
         * Add parameters to request.
         * If POST or PUT, parameters are passed in body using x-www-form-urlencoded if alone, or form-data if there is files too.
         * For any other method, those params are appended to the queryString.
         *
         * @return the HTTPRequest for chaining.
         */
        public HTTPRequest params(Map<String, Object> parameters) {
            this.parameters = parameters;
            return this;
        }

        /**
         * Add parameters to request.
         * If POST or PUT, parameters are passed in body using x-www-form-urlencoded if alone, or form-data if there is files too.
         * For any other method, those params are appended to the queryString.
         *
         * @return the HTTPRequest for chaining.
         */
        public HTTPRequest setParameters(Map<String, String> parameters) {
            this.parameters.putAll(parameters);
            return this;
        }

        /**
         * Execute a GET request synchronously.
         */
        public abstract HttpResponse get();

        /**
         * Execute a GET request asynchronously.
         */
        public Promise<HttpResponse> getAsync() {
            throw new NotImplementedException();
        }

        /**
         * Execute a POST request.
         */
        public abstract HttpResponse post();

        /**
         * Execute a POST request asynchronously.
         */
        public Promise<HttpResponse> postAsync() {
            throw new NotImplementedException();
        }

        /**
         * Execute a PUT request.
         */
        public abstract HttpResponse put();

        /**
         * Execute a PUT request asynchronously.
         */
        public Promise<HttpResponse> putAsync() {
            throw new NotImplementedException();
        }

        /**
         * Execute a DELETE request.
         */
        public abstract HttpResponse delete();

        /**
         * Execute a DELETE request asynchronously.
         */
        public Promise<HttpResponse> deleteAsync() {
            throw new NotImplementedException();
        }

        /**
         * Execute a OPTIONS request.
         */
        public abstract HttpResponse options();

        /**
         * Execute a OPTIONS request asynchronously.
         */
        public Promise<HttpResponse> optionsAsync() {
            throw new NotImplementedException();
        }

        /**
         * Execute a HEAD request.
         */
        public abstract HttpResponse head();

        /**
         * Execute a HEAD request asynchronously.
         */
        public Promise<HttpResponse> headAsync() {
            throw new NotImplementedException();
        }

        /**
         * Execute a TRACE request.
         */
        public abstract HttpResponse trace();

        /**
         * Execute a TRACE request asynchronously.
         */
        public Promise<HttpResponse> traceAsync() {
            throw new NotImplementedException();
        }

        protected String basicAuthHeader() {
            return "Basic " + new String(Base64.encodeBase64((this.username + ":" + this.password).getBytes()));
        }

        protected String createQueryString() {
            StringBuilder sb = new StringBuilder();
            for (String key : this.parameters.keySet()) {
                if (sb.length() > 0) {
                    sb.append("&");
                }
                Object value = this.parameters.get(key);

                if (value != null) {
                    if (value instanceof Collection<?> || value.getClass().isArray()) {
                        Collection<?> values = value.getClass().isArray() ? Arrays.asList((Object[]) value) : (Collection<?>) value;
                        boolean first = true;
                        for (Object v : values) {
                            if (!first) {
                                sb.append("&");
                            }
                            first = false;
                            sb.append(encode(key)).append("=").append(encode(v.toString()));
                        }
                    } else {
                        sb.append(encode(key)).append("=").append(encode(this.parameters.get(key).toString()));
                    }
                }
            }
            return sb.toString();
        }

    }

    /**
     * An HTTP response wrapper
     */
    public static abstract class HttpResponse {

        /**
         * the HTTP status code
         *
         * @return the status code of the http response
         */
        public abstract Integer getStatus();

        /**
         * The http response content type
         *
         * @return the content type of the http response
         */
        public String getContentType() {
            return getHeader("content-type");
        }

        public abstract String getHeader(String key);

        /**
         * get the response body as a string
         *
         * @return the body of the http response
         */
        public String getString() throws IOException {
            return IOUtils.toString(getStream());
        }

        /**
         * Parse the response string as a query string.
         *
         * @return The parameters as a Map. Return an empty map if the response
         * is not formed as a query string.
         */
        public Map<String, String> getQueryString() throws IOException {
            Map<String, String> result = new HashMap<String, String>();
            String body = getString();
            for (String entry : body.split("&")) {
                if (entry.indexOf("=") > 0) {
                    result.put(entry.split("=")[0], entry.split("=")[1]);
                }
            }
            return result;
        }

        /**
         * get the response as a stream
         *
         * @return an inputstream
         */
        public abstract InputStream getStream();
    }


    public static class FileParam {
        public File file;
        public String paramName;

        public FileParam(File file, String name) {
            this.file = file;
            this.paramName = name;
        }

        public static FileParam[] getFileParams(File[] files) {
            FileParam[] filesp = new FileParam[files.length];
            for (int i = 0; i < files.length; i++) {
                filesp[i] = new FileParam(files[i], files[i].getName());
            }
            return filesp;
        }
    }

}
