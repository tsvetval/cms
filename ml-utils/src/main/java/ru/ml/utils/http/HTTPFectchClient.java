package ru.ml.utils.http;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 */
public class HTTPFectchClient implements  HTTPClient{


    private static final Logger log = LoggerFactory.getLogger(HTTPFectchClient.class);

    public HTTPFectchClient() {}

    public void stop() {}

    public HTTPClientUtils.HTTPRequest newRequest(String url) {
        return new WSUrlfetchRequest(url);
    }

    public class WSUrlfetchRequest extends HTTPClientUtils.HTTPRequest {

        protected WSUrlfetchRequest(String url) {
            this.url = url;
        }

        /** Execute a GET request synchronously. */
        public HTTPClientUtils.HttpResponse get() {
            try {
                return new HttpUrlfetchResponse(prepare(new URL(url), "GET"));
            } catch (Exception e) {
                log.error(e.toString());
                throw new RuntimeException(e);
            }
        }

        /** Execute a POST request.*/
        public HTTPClientUtils.HttpResponse post() {
            try {
                HttpURLConnection conn = prepare(new URL(url), "POST");
                return new HttpUrlfetchResponse(conn);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        /** Execute a PUT request.*/
        public HTTPClientUtils.HttpResponse put() {
            try {
                return new HttpUrlfetchResponse(prepare(new URL(url), "PUT"));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

       /** Execute a DELETE request.*/
        public HTTPClientUtils.HttpResponse delete() {
            try {
                return new HttpUrlfetchResponse(prepare(new URL(url), "DELETE"));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        /** Execute a OPTIONS request.*/
        public HTTPClientUtils.HttpResponse options() {
            try {
                return new HttpUrlfetchResponse(prepare(new URL(url), "OPTIONS"));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        /** Execute a HEAD request.*/
        public HTTPClientUtils.HttpResponse head() {
            try {
                return new HttpUrlfetchResponse(prepare(new URL(url), "HEAD"));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        /** Execute a TRACE request.*/
        public HTTPClientUtils.HttpResponse trace() {
            try {
                return new HttpUrlfetchResponse(prepare(new URL(url), "TRACE"));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        private HttpURLConnection prepare(URL url, String method) {
            if (this.username != null && this.password != null && this.scheme != null) {
                String authString = null;
                switch (this.scheme) {
                case BASIC: authString = basicAuthHeader(); break;
                default: throw new RuntimeException("Scheme " + this.scheme + " not supported by the UrlFetch WS backend.");
                }
                this.headers.put("Authorization", authString);
            }
            try {
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod(method);
                connection.setDoInput(true);
                connection.setInstanceFollowRedirects(this.followRedirects);
                connection.setReadTimeout((int) (this.timeout * 1000l));
                for (String key: this.headers.keySet()) {
                    connection.setRequestProperty(key, headers.get(key));
                }
                checkFileBody(connection);
                return connection;
            } catch(Exception e) {
                throw new RuntimeException(e);
            }
        }

        private void checkFileBody(HttpURLConnection connection) throws IOException {
            if (this.parameters != null && !this.parameters.isEmpty()) {
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setDoOutput(true);
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                writer.write(createQueryString());
                writer.close();
            }
            if (this.body != null) {
                if (this.parameters != null && !this.parameters.isEmpty()) {
                    throw new RuntimeException("POST or PUT method with parameters AND body are not supported.");
                }
                connection.setDoOutput(true);
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                writer.write(this.body.toString());
                writer.close();
                if(this.mimeType != null) {
                    connection.setRequestProperty("Content-Type", this.mimeType);
                }
            }
        }

    }

    /**
     * An HTTP response wrapper
     */
    public static class HttpUrlfetchResponse extends HTTPClientUtils.HttpResponse {

        private String body;
        private Integer status;
        private Map<String, List<String>> headersMap;

        /**
         * you shouldnt have to create an HttpResponse yourself
         * @param connection
         */
        public HttpUrlfetchResponse(HttpURLConnection connection) {
            try {
                this.status = connection.getResponseCode();
                this.headersMap = connection.getHeaderFields();
                this.body = IOUtils.toString(connection.getInputStream());
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            } finally {
                connection.disconnect();
            }
        }

        /**
         * the HTTP status code
         * @return the status code of the http response
         */
        @Override
        public Integer getStatus() {
            return status;
        }

        @Override
        public String getHeader(String key) {
            return headersMap.containsKey(key) ? headersMap.get(key).get(0) : null;
        }

        public Map<String, List<String>>  getHeaders() {
            return  headersMap;
        }

        /**
         * get the response body as a string
         * @return the body of the http response
         */
        @Override
        public String getString() {
            return body;
        }

        /**
         * get the response as a stream
         * @return an inputstream
         */
        @Override
        public InputStream getStream() {
            try {
                return new ByteArrayInputStream(body.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
