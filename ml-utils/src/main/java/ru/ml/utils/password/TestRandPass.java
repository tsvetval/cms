package ru.ml.utils.password;

import java.util.regex.Pattern;

/**
 *
 */
public class TestRandPass {
    public static void main(String[] args) {

        Pattern EMAIL_PATTERN =
                Pattern.compile("^[_A-Za-zа-яА-Я0-9-\\+]+(\\.[_A-Za-zа-яА-Я0-9-]+)*@"
                        + "[A-Za-zа-яА-Я0-9-]+(\\.[A-Za-zа-яА-Я0-9]+)*(\\.[A-Za-zа-яА-Я]{2,})$");
        System.out.println(EMAIL_PATTERN.matcher("tsval@yandex.ru").matches());

        RandPass randPass = new RandPass();
        randPass.addRequirement(RandPass.UPPERCASE_LETTERS_ALPHABET, 1);
        randPass.addRequirement(RandPass.LOWERCASE_LETTERS_ALPHABET, 1);
        randPass.addRequirement(RandPass.DIGITS_ALPHABET, 1);
        String pass = randPass.getPass(8);
        System.out.println(pass);

        /**
         ^                 # start-of-string
         (?=.*[0-9])       # a digit must occur at least once
         (?=.*[a-z])       # a lower case letter must occur at least once
         (?=.*[A-Z])       # an upper case letter must occur at least once
         (?=.*[@#$%^&+=])  # a special character must occur at least once
         (?=\S+$)          # no whitespace allowed in the entire string
         .{8,}             # anything, at least eight places though
         $                 # end-of-string
         */
        Pattern PASSWORD_PATTERN = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$");
        System.out.println(PASSWORD_PATTERN.matcher(pass).matches());
        System.out.println(PASSWORD_PATTERN.matcher("Tn6dmKZGa").matches()); //9 вшпшеы
        System.out.println(PASSWORD_PATTERN.matcher("Tn6dmKZ").matches()); //7 digits
        System.out.println(PASSWORD_PATTERN.matcher("an6dmcca").matches()); //no upper

    }
}
