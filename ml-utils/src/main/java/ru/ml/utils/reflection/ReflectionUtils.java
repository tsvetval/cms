package ru.ml.utils.reflection;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.*;
import java.net.URL;
import java.sql.Timestamp;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

public class ReflectionUtils {

    /**
     * Возвращает список всех полей класса и его родителей исключая java.lang.Object
     *
     * @param type              класс для получения полей
     * @param returnFinalFields настрока возвращать ли файнал поля
     * @return массив всех полей класса
     */
    public static Field[] getDeclaredAndInheritedFields(Class type, boolean returnFinalFields) {
        List<Field> allFields = new ArrayList<Field>();
        allFields.addAll(getValidFields(type.getDeclaredFields(), returnFinalFields));
        Class parent = type.getSuperclass();
        while (parent != null && parent != Object.class) {
            allFields.addAll(getValidFields(parent.getDeclaredFields(), returnFinalFields));
            parent = parent.getSuperclass();
        }
        return allFields.toArray(new Field[allFields.size()]);
    }

    /**
     * возвращает описание поля класса
     * @param type класс
     * @param name имя поля
     * @param returnFinalFields  настрока возвращать ли файнал поля
     * @return Field/ null
     */
    public static Field getDeclaredAndInheritedField(Class type, String name, boolean returnFinalFields){
        Field[] fields = getDeclaredAndInheritedFields(type, returnFinalFields);
        for (Field field : fields){
            if (field.getName().equals(name)){
                return field;
            }
        }
        return null;
    }

    /**
     * Созвращает переработанный список полей исходя из настроек
     * По имолчанию исключаются все статические и файнал поля
     *
     * @param fields            список полей для проверки
     * @param returnFinalFields настрока возвращать ли файнал поля
     * @return массив всех полей класса
     */
    public static List<Field> getValidFields(Field[] fields, boolean returnFinalFields) {
        List<Field> validFields = new ArrayList<Field>();
        // we ignore static and final fields
        for (Field field : fields) {
            if (!Modifier.isStatic(field.getModifiers()) && (returnFinalFields || !Modifier.isFinal(field.getModifiers()))) {
                validFields.add(field);
            }
        }
        return validFields;
    }

    /**
     * Проверяет на соответствие основным типа для мапинга на любые БД
     *
     * @param type тип который мы проверяем
     * @return true если данный класс мы можем замапить
     */
    public static boolean isPropertyType(Class type) {
        return isValidMapValueType(type) || type == InputStream.class || isArrayOfType(type, byte.class);
    }

    public static boolean isValidMapValueType(Class type) {
        return type == String.class || isArrayOfType(type, String.class) || type == Date.class || isArrayOfType(type, Date.class) || type == Calendar.class || isArrayOfType(type, Calendar.class) || type == Timestamp.class || isArrayOfType(type, Timestamp.class) || type == Integer.class || isArrayOfType(type, Integer.class) || type == int.class || isArrayOfType(type, int.class) || type == Long.class || isArrayOfType(type, Long.class) || type == long.class || isArrayOfType(type, long.class) || type == Double.class || isArrayOfType(type, Double.class) || type == double.class || isArrayOfType(type, double.class) || type == Boolean.class || isArrayOfType(type, Boolean.class) || type == boolean.class || isArrayOfType(type, boolean.class) || type == Locale.class || isArrayOfType(type, Locale.class) || type.isEnum() || (type.isArray() && type.getComponentType().isEnum());
    }

    private static boolean isArrayOfType(Class c, Class type) {
        return c.isArray() && c.getComponentType() == type;
    }

    public static boolean isDateType(Class type) {
        return type == Date.class || type == Calendar.class || type == Timestamp.class;
    }

    /**
     * Получаем первый генерик класс
     *
     * @param field поле класса
     * @return первый класс который идет как генерик класс для класса поля
     */
    public static Class<?> getParametrizedClass(Field field) {
        return getParametrizedClass(field, 0);
    }

    /**
     * Возвращает генерик класс по индексу
     *
     * @param field поле класса
     * @param index индекс генерика класса
     * @return класс который указан как генерик тип
     */
    public static Class getParametrizedClass(Field field, int index) {
        if (field.getGenericType() instanceof ParameterizedType) {
            ParameterizedType pType = (ParameterizedType) field.getGenericType();
            Type paramType = pType.getActualTypeArguments()[index];
            if (paramType instanceof GenericArrayType) {
                Class arrayType = (Class) ((GenericArrayType) paramType).getGenericComponentType();
                return Array.newInstance(arrayType, 0).getClass();
            } else {
                if (paramType instanceof ParameterizedType) {
                    ParameterizedType paramPType = (ParameterizedType) paramType;
                    return (Class) paramPType.getRawType();
                } else {
                    return (Class) paramType;
                }
            }
        }
        return null;
    }

    public static Class getTypeArgumentOfParametrizedClass(Field field, int index, int typeIndex) {
        if (field.getGenericType() instanceof ParameterizedType) {
            ParameterizedType pType = (ParameterizedType) field.getGenericType();
            Type paramType = pType.getActualTypeArguments()[index];
            if (!(paramType instanceof GenericArrayType)) {
                if (paramType instanceof ParameterizedType) {
                    ParameterizedType paramPType = (ParameterizedType) paramType;
                    Type paramParamType = paramPType.getActualTypeArguments()[typeIndex];
                    if (!(paramParamType instanceof ParameterizedType)) {
                        return (Class) paramParamType;
                    }
                }
            }
        }
        return null;
    }

    public static Class getParametrizedClass(Class c) {
        return getParametrizedClass(c, 0);
    }

    public static Class getParametrizedClass(Class c, int index) {
        TypeVariable[] typeVars = c.getTypeParameters();
        if (typeVars.length > 0) {
            return (Class) typeVars[index].getBounds()[0];
        } else {
            return null;
        }
    }

    /**
     * Проверяем что данное поле параметризованно указанным классом
     *
     * @param field поле класса
     * @param c     класс котрый мы проверяем на наличие в генерике
     * @return true если данный класс указан в генерике
     */
    public static boolean isFieldParametrizedWithClass(Field field, Class c) {
        if (field.getGenericType() instanceof ParameterizedType) {
            ParameterizedType pType = (ParameterizedType) field.getGenericType();
            for (Type type : pType.getActualTypeArguments()) {
                if (type == c) {
                    return true;
                }
                if (c.isInterface() && c.isAssignableFrom((Class) type)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Проверяем что данное поле параметризованно поддерживаемым в мапинге классом
     *
     * @param field поле класса
     * @return true если данное поле поддерживается
     */
    public static boolean isFieldParametrizedWithPropertyType(Field field) {
        if (field.getGenericType() instanceof ParameterizedType) {
            ParameterizedType pType = (ParameterizedType) field.getGenericType();
            for (Type type : pType.getActualTypeArguments()) {
                if (isPropertyType((Class) type)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static String stripFilenameExtension(String filename) {
        if (filename.indexOf('.') != -1) {
            return filename.substring(0, filename.lastIndexOf('.'));
        } else {
            return filename;
        }
    }

    public static Set<Class<?>> getFromDirectory(File directory, String packageName) throws ClassNotFoundException {
        Set<Class<?>> classes = new HashSet<Class<?>>();
        if (directory.exists()) {
            for (String file : directory.list()) {
                if (file.endsWith(".class")) {
                    String name = packageName + '.' + stripFilenameExtension(file);
                    Class<?> clazz = Class.forName(name);
                    classes.add(clazz);
                }
            }
        }
        return classes;
    }

    public static Set<Class<?>> getFromJARFile(String jar, String packageName) throws IOException, ClassNotFoundException {
        Set<Class<?>> classes = new HashSet<Class<?>>();
        JarInputStream jarFile = new JarInputStream(new FileInputStream(jar));
        JarEntry jarEntry;
        do {
            jarEntry = jarFile.getNextJarEntry();
            if (jarEntry != null) {
                String className = jarEntry.getName();
                if (className.endsWith(".class")) {
                    className = stripFilenameExtension(className);
                    if (className.startsWith(packageName)) {
                        classes.add(Class.forName(className.replace('/', '.')));
                    }
                }
            }
        } while (jarEntry != null);
        return classes;
    }

    public static Set<Class<?>> getClasses(ClassLoader loader, String packageName) throws IOException, ClassNotFoundException {
        Set<Class<?>> classes = new HashSet<Class<?>>();
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = loader.getResources(path);
        if (resources != null) {
            while (resources.hasMoreElements()) {
                String filePath = resources.nextElement().getFile();
                // WINDOWS HACK
                if (filePath.contains("%20")) {
                    filePath = filePath.replaceAll("%20", " ");
                }
                if (filePath != null) {
                    if ((filePath.contains("!")) & (filePath.contains(".jar"))) {
                        String jarPath = filePath.substring(0, filePath.indexOf("!")).substring(filePath.indexOf(":") + 1);
                        // WINDOWS HACK
                        if (jarPath.contains(":")) {
                            jarPath = jarPath.substring(1);
                        }
                        classes.addAll(getFromJARFile(jarPath, path));
                    } else {
                        classes.addAll(getFromDirectory(new File(filePath), packageName));
                    }
                }
            }
        }
        return classes;
    }

    @SuppressWarnings("unchecked")
    public static  <T> T getFieldValue(Object bean, String fieldName) {
        Field field = getDeclaredAndInheritedField(bean.getClass(), fieldName, true);

        try {
            if(field != null) {
                field.setAccessible(true);
                return (T) field.get(bean);
            }
        } catch (IllegalAccessException e) {
            // do nothing
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static void setFieldValue(Object bean, String fieldName, Object value) {
        Field field = getDeclaredAndInheritedField(bean.getClass(), fieldName, true);

        try {
            if(field != null) {
                field.setAccessible(true);
                field.set(bean, value);
            }
        } catch (IllegalAccessException e) {
            // do nothing
        }
    }

    public static boolean isAbstractClass(Class clazz) {
        return Modifier.isAbstract(clazz.getModifiers());
    }

    public static Class getAncestorClass(Class clazz) {
        Class ancestorClass = clazz.getSuperclass();
        if (ancestorClass == Object.class) {
            return null;
        } else {
            return ancestorClass;
        }
    }


    public static Class[] getInterfaces(Class clazz) {
        return clazz.getInterfaces();
    }
}