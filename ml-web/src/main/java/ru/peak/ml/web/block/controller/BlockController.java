package ru.peak.ml.web.block.controller;

import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.peak.ml.core.model.page.MlPageBlockBase;

public interface BlockController {
    //путь к файлам шаблонов
    static final String TEMPLATE_PREFIX = "blocks/content/";
    //расширение шаблонов
    static final String TEMPLATE_POSTFIX = ".hml";


    void serve(MlHttpServletRequest params, MlPageBlockBase mlInstance, MlHttpServletResponse resp) throws MlApplicationException, MlServerException;
}
