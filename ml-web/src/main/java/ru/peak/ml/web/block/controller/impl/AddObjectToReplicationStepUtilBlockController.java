package ru.peak.ml.web.block.controller.impl;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.MLUID;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.template.TemplateEngine;
import ru.peak.ml.web.block.controller.BlockController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.model.MlHeteroLink;
import ru.peak.ml.web.model.MlReplication;
import ru.peak.ml.web.model.MlReplicationStep;

import java.util.HashMap;
import java.util.List;

/**
 * Контроллер добавления объекта к шагу репликации
 */
public class AddObjectToReplicationStepUtilBlockController implements BlockController {
    private static final Logger log = LoggerFactory.getLogger(AddObjectToReplicationStepUtilBlockController.class);
    private static final String SELECT_TEMPLATE = "blocks/utils/replication/selectReplicationStep.hml";
    @Inject
    CommonDao commonDao;
    @Inject
    MetaDataHolder metaDataHolder;

    @Override
    @Transactional
    public void serve(MlHttpServletRequest request, MlPageBlockBase mlInstance, MlHttpServletResponse resp) {
        String action = request.getString("action", "show");
        // отображение шаблона блока добавления объекта к шагу репликации
        if (action.equals("show")) {
            JsonObject result = new JsonObject();
            result.add("showType", new JsonPrimitive("modal"));
            result.add("idModal", new JsonPrimitive((Long) mlInstance.get("id")));
            result.add("title", new JsonPrimitive("Добавить объект в блок обновлений"));

            TemplateEngine templateEngine = GuiceConfigSingleton.inject(TemplateEngine.class);
            List<MlReplication> replications = commonDao.getResultList("SELECT o FROM MlReplication o", MlReplication.class);
            HashMap<String, Object> data = new HashMap<>();
            data.put("replications", replications);
            data.put("widgetMlInstance", mlInstance);

            String html = templateEngine.renderTemplate(SELECT_TEMPLATE, data);
            result.add("html", new JsonPrimitive(html));


            resp.setJsonData(result);

            // добавления объекта к шагу репликации
        } else if (action.equals("addToReplication")) {
            String mlClass = request.getString("className");
            Long objectId = request.getLong("objectId");
            if (checkTypicalAttrExist(metaDataHolder.getMlClassByName(mlClass))) {
                MlDynamicEntityImpl entity = commonDao.findById(objectId, mlClass);
                MlReplicationStep mlReplicationStep = (MlReplicationStep) commonDao.findById(request.getLong("idReplicationStep"), MlReplicationStep.class);
                MlHeteroLink heteroLink = (MlHeteroLink) commonDao.createNewEntity(MlHeteroLink.class);
                heteroLink.setObject(entity);
                mlReplicationStep.getObjects().add(heteroLink);
                commonDao.persistWithSecurityCheck(heteroLink);
                commonDao.persistWithSecurityCheck(mlReplicationStep);

                JsonObject result = new JsonObject();
                result.add("showType", new JsonPrimitive("modal"));
                result.add("idModal", new JsonPrimitive((Long) mlInstance.get("id")));
                result.add("title", new JsonPrimitive("Результат операции"));
                result.add("content", new JsonPrimitive("Обьект успешно добавлен!"));
                resp.setJsonData(result);
            } else {
                JsonObject result = new JsonObject();
                result.add("showType", new JsonPrimitive("modal"));
                result.add("idModal", new JsonPrimitive((Long) mlInstance.get("id")));
                result.add("title", new JsonPrimitive("Результат операции"));
                result.add("content", new JsonPrimitive("Объект невозможно добавить в шаг обновлнеий! " +
                        "У объекта отсутствуют необходимые поля!"));
                resp.setJsonData(result);

            }
        }
    }

    /**
     * Проверка наличия у класса необходимых для репликации атрибутоа (guid и lastChange)
     *
     * @param IMlClass -   класс для проверки
     * @return -   результат проверки
     */
    private boolean checkTypicalAttrExist(MlClass IMlClass) {
        for (MlAttr attr : IMlClass.getAttrSet()) {
            if (attr.getEntityFieldName().equals(MlAttr.GUID) || attr.getEntityFieldName().equals(MlAttr.LAST_CHANGE)) {
                return true;
            }
        }
        return false;
    }

}
