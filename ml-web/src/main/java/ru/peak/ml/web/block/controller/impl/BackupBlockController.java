package ru.peak.ml.web.block.controller.impl;

import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.web.block.controller.BlockController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.model.MlHeteroLink;
import ru.peak.ml.web.model.MlReplication;
import ru.peak.ml.web.model.MlReplicationStep;

import javax.persistence.TypedQuery;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Контроллер блока создания резервной копии (репликации) базы данных
 *
 */
public class BackupBlockController implements BlockController {
    private static final Logger log = LoggerFactory.getLogger(BackupBlockController.class);

    private static final String ACTION_SHOW = "show";
    private static final String ACTION_BACKUP = "backup";

    private static final String DATE_FORMAT_STRING = "dd.MM.yyyy";

    private static final String DATE_FORMAT_STRING_FOR_NAME = "yyyyMMdd";
    private static final String DATETIME_FORMAT_STRING_FOR_NAME = "yyyyMMdd_HHmm";

    static final String TEMPLATE_SHOW = "blocks/backup/backup.hml";

    @Inject
    CommonDao commonDao;

    @Override
    public void serve(MlHttpServletRequest params, MlPageBlockBase mlInstance, MlHttpServletResponse resp) throws MlApplicationException, MlServerException {

        String action = params.getString("action");
        MlUser user = (MlUser) params.getRequest().getSession().getAttribute("user");

        switch (action) {
            // отображение блока
            case ACTION_SHOW:
                Map<String, Object> data = new HashMap<>();
                data.put("currentPageBlock", mlInstance);
                resp.setDataType(MlHttpServletResponse.DataType.JSON);
                resp.renderTemplateToJson(TEMPLATE_SHOW, data);
                break;
            /**
             * создание резервной копии БД - в параметрах указываются:
             * dateFrom         -   начало периода изменения объектов для репликации
             * dateTo           -   конец периода изменения объектов для репликации
             * includeInstances -   включать ли в репликацию инстансы классов (объекты)
             *
             */
            case ACTION_BACKUP:
                String dateFromStr = params.getString("dateFrom");
                if (dateFromStr.isEmpty()) {
                    throw new MlApplicationException("Не задана дата начала периода репликации");
                }

                String dateToStr = params.getString("dateTo");
                if (dateToStr.isEmpty()) {
                    throw new MlApplicationException("Не задана дата окончания периода репликации");
                }

                SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_STRING);
                Date dateFrom = null;

                try {
                    dateFrom = dateFormat.parse(dateFromStr);
                } catch (ParseException e) {
                    throw new MlApplicationException(String.format("Ошибка обработки даты начала периода репликации. Строка [%s] не может быть приведена к дате.", dateFromStr), e);
                }
                Date dateTo = null;
                try {
                    dateTo = dateFormat.parse(dateToStr);
                } catch (ParseException e) {
                    throw new MlApplicationException(String.format("Ошибка обработки даты окончания периода репликации. Строка [%s] не может быть приведена к дате.", dateToStr), e);
                }
                if (dateTo.before(dateFrom)) {
                    throw new MlApplicationException("Неверно задан период репликации: дата начала должна быть меньше даты окончания.");
                }

                Boolean includeInstances = params.getBoolean("includeInstances");

                Date currentDate = new Date();

                MlReplication modelReplication = null;
                try {
                    modelReplication = createModelBackupReplication(dateFrom, dateTo, currentDate);
                } catch (Exception e) {
                    throw new MlApplicationException("Ошибка при создании блока репликации структуры БД", e);
                }

                MlReplication instanceReplication = null;
                if (includeInstances) {
                    try {
                        instanceReplication = createInstanceBackupReplication(dateFrom, dateTo, currentDate);
                    } catch (Exception e) {
                        throw new MlApplicationException("Ошибка при создании блока репликации данных БД", e);
                    }
                }

                resp.setDataType(MlHttpServletResponse.DataType.JSON);
                resp.addDataToJson("dateFrom", new JsonPrimitive(dateFromStr));
                resp.addDataToJson("dateTo", new JsonPrimitive(dateToStr));

                if (modelReplication != null) {
                    int replicationModelCount = 0;
                    for (MlReplicationStep step : modelReplication.getReplicationSteps()) {
                        replicationModelCount += step.getObjects().size();
                    }
                    String name = modelReplication.getName();
                    resp.addDataToJson("replicationModelCount", new JsonPrimitive(replicationModelCount));
                    resp.addDataToJson("replicationModelName", new JsonPrimitive(name));
                }

                if (instanceReplication != null) {
                    int replicationInstanceCount = 0;
                    for (MlReplicationStep step : instanceReplication.getReplicationSteps()) {
                        replicationInstanceCount += step.getObjects().size();
                    }
                    String name = instanceReplication.getName();
                    resp.addDataToJson("replicationInstanceCount", new JsonPrimitive(replicationInstanceCount));
                    resp.addDataToJson("replicationInstanceName", new JsonPrimitive(name));
                }
                break;
            default:
                log.error("Unknown action: " + action);
                throw new MlApplicationException("Данный функционал (" + action + ") пока не реализован");
        }
    }

    /**
     * Создание блока репликации для моделей (метаданных)
     *
     * @param dateFrom      -   начало периода изменения данных моделей подлежащих репликации
     * @param dateTo        -   конец периода изменения данных моделей подлежащих репликации
     * @param currentDate   -   текущая дата
     * @return              -   блок репликации
     */
    @Transactional
    public MlReplication createModelBackupReplication(Date dateFrom, Date dateTo, Date currentDate) {
        List<MlClass> backupClassList = getClassListToBackupInDateInterval(dateFrom, dateTo);
        List<MlAttr> backupAttrList = getAttrListToBackupInDateInterval(dateFrom, dateTo);

        if (backupClassList.size() + backupAttrList.size() == 0) {
            return null;
        }

        MlReplication modelReplicationBlock = new MlReplication();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_STRING_FOR_NAME);
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat(DATETIME_FORMAT_STRING_FOR_NAME);
        String name = String.format("DatabaseModelBackup[%s-%s]createdAt[%s]", dateFormat.format(dateFrom), dateFormat.format(dateTo), dateTimeFormat.format(currentDate));
        modelReplicationBlock.setName(name);

        MlReplicationStep modelReplicationStep = new MlReplicationStep();
        modelReplicationStep.setName("DatabaseModelBackupStep");
        modelReplicationStep.setStepNumber(1L);
        modelReplicationStep.setOffHandlers(true);
        modelReplicationStep.setReinitialization(false);

        for (MlDynamicEntityImpl backupClass : backupClassList) {
            MlHeteroLink heteroLink = (MlHeteroLink) commonDao.createNewEntity(MlHeteroLink.class);
            heteroLink.setObject(backupClass);
            modelReplicationStep.getObjects().add(heteroLink);
        }
        for (MlDynamicEntityImpl backupAttr : backupAttrList) {
            MlHeteroLink heteroLink = (MlHeteroLink) commonDao.createNewEntity(MlHeteroLink.class);
            heteroLink.setObject(backupAttr);
            modelReplicationStep.getObjects().add(heteroLink);
        }
        modelReplicationBlock.getReplicationSteps().add(modelReplicationStep);

        int replicationModelCount = modelReplicationStep.getObjects().size();

        if (replicationModelCount == 0) {
            return null;
        }

        commonDao.persistWithSecurityCheck(modelReplicationBlock);
        modelReplicationStep.setReplication(modelReplicationBlock);
        commonDao.persistWithSecurityCheck(modelReplicationStep);

        for (MlHeteroLink heteroLink : modelReplicationStep.getObjects()) {
            commonDao.persistWithSecurityCheck(heteroLink);
        }

        return modelReplicationBlock;
    }

    /**
     * Создание блока репликации для объектов БД (инстансов классов)
     *
     * @param dateFrom      -   начало периода изменения данных объектов подлежащих репликации
     * @param dateTo        -   конец периода изменения данных объектов подлежащих репликации
     * @param currentDate   -   текущая дата
     * @return              -   блок репликации
     */
    @Transactional
    public MlReplication createInstanceBackupReplication(Date dateFrom, Date dateTo, Date currentDate) {
        List<MlClass> backupDataClassList = getBackupDataClassList();

        if (backupDataClassList.isEmpty()) {
            return null;
        }

        MlReplication instanceReplicationBlock = new MlReplication();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_STRING_FOR_NAME);
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat(DATETIME_FORMAT_STRING_FOR_NAME);
        String name = String.format("DatabaseDataBackup[%s-%s]createdAt[%s]", dateFormat.format(dateFrom), dateFormat.format(dateTo), dateTimeFormat.format(currentDate));
        instanceReplicationBlock.setName(name);

        MlReplicationStep instanceReplicationStep = new MlReplicationStep();
        instanceReplicationStep.setName("DatabaseDataBackupStep");
        instanceReplicationStep.setStepNumber(1L);
        instanceReplicationStep.setOffHandlers(false);
        instanceReplicationStep.setReinitialization(false);

        for (MlClass mlClass : backupDataClassList) {
            List<MlDynamicEntityImpl> instanceList = getInstanceListToBackupInDateInterval(mlClass.getEntityName(), dateFrom, dateTo);
            for (MlDynamicEntityImpl instance : instanceList) {
                MlHeteroLink heteroLink = (MlHeteroLink) commonDao.createNewEntity(MlHeteroLink.class);
                heteroLink.setObject(instance);
                instanceReplicationStep.getObjects().add(heteroLink);
            }
        }

        instanceReplicationBlock.getReplicationSteps().add(instanceReplicationStep);

        int replicationInstanceCount = instanceReplicationStep.getObjects().size();

        if (replicationInstanceCount == 0) {
            return null;
        }

        commonDao.persistWithSecurityCheck(instanceReplicationBlock);
        instanceReplicationStep.setReplication(instanceReplicationBlock);
        commonDao.persistWithSecurityCheck(instanceReplicationStep);

        for (MlHeteroLink heteroLink : instanceReplicationStep.getObjects()) {
            commonDao.persistWithSecurityCheck(heteroLink);
        }

        return instanceReplicationBlock;
    }

    /**
     * получение списка атрибутов для репликации (измененных в указанный период)
     *
     * @param dateFrom  -   начало периода
     * @param dateTo    -   конец периода
     * @return
     */
    private List<MlAttr> getAttrListToBackupInDateInterval(Date dateFrom, Date dateTo) {
        String jpql = "select o from MlAttr o where o.lastChange between :dateFrom and :dateTo";
        TypedQuery<MlAttr> query = commonDao.getTypedQueryWithoutSecurityCheck(jpql, "MlAttr");
        query.setParameter("dateFrom", dateFrom);
        query.setParameter("dateTo", dateTo);
        return query.getResultList();
    }

    /**
     * получение списка классов для репликации (измененных в указанный период)
     *
     * @param dateFrom  -   начало периода
     * @param dateTo    -   конец периода
     * @return
     */
    private List<MlClass> getClassListToBackupInDateInterval(Date dateFrom, Date dateTo) {
        String jpql = "select o from MlClass o where o.lastChange between :dateFrom and :dateTo";
        TypedQuery<MlClass> query = commonDao.getTypedQueryWithoutSecurityCheck(jpql, "MlClass");
        query.setParameter("dateFrom", dateFrom);
        query.setParameter("dateTo", dateTo);
        return query.getResultList();
    }

    /**
     * получение списка классов для репликации (все классы кроме MlClass и MlAttr, которые могут быть реплицированны)
     */
    private List<MlClass> getBackupDataClassList() {
        String jpql = "select o from MlClass o where o.entityName not in ('MlClass', 'MlAttr') " +
                "and o.id in (select a.mlClass.id from MlAttr a where a.entityFieldName = 'lastChange') " +
                "and o.id in (select a.mlClass.id from MlAttr a where a.entityFieldName = 'guid')";
        TypedQuery<MlClass> query = commonDao.getTypedQueryWithoutSecurityCheck(jpql, "MlClass");
        return query.getResultList();
    }

    /**
     * получение списка объектов указанного класса для репликации (измененных в указанный период)
     *
     * @param className     -   имя класса для репликации
     * @param dateFrom      -   начало периода
     * @param dateTo        -   конец периода
     * @return              -   список объектов для репликации
     */
    private List<MlDynamicEntityImpl> getInstanceListToBackupInDateInterval(String className, Date dateFrom, Date dateTo) {
        String jpql = String.format("select o from %s o where o.lastChange between :dateFrom and :dateTo", className);
        TypedQuery<MlDynamicEntityImpl> query = commonDao.getTypedQueryWithoutSecurityCheck(jpql, "MlDynamicEntityImpl");
        query.setParameter("dateFrom", dateFrom);
        query.setParameter("dateTo", dateTo);
        return query.getResultList();
    }

}

