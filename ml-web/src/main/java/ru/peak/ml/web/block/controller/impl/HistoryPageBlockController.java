package ru.peak.ml.web.block.controller.impl;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.page.MlPage;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.template.TemplateEngine;
import ru.peak.ml.web.block.controller.BlockController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.helper.PageHelper;
import ru.peak.ml.web.history.HistoryDao;
import ru.peak.ml.web.model.MlHistoryUtil;

import javax.persistence.Query;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: d_litovchenko
 * Date: 04.06.14
 * Time: 13:17
 * To change this template use File | Settings | File Templates.
 */
public class HistoryPageBlockController implements BlockController {      //     ru.peak.ml.web.block.controller.impl.HistoryPageBlockController
    private static final Logger log = LoggerFactory.getLogger(HistoryPageBlockController.class);

    //private static final String BLOCK_TEMPLATE = "blocks/utils/history/history.hml";
    private static final String SHOW_HISTORY_TEMPLATE = "blocks/utils/history/showHistory.hml";
    private static final String HISTORY_UTIL_PARAM_NAME = "action";

    @Inject
    CommonDao commonDao;
    @Inject
    MetaDataHolder metaDataHolder;

    private static class Actions {
        public static final String SHOW = "show";
        public static final String SHOW_HISTORY = "showHistory";
    }

    @Override
    public void serve(MlHttpServletRequest request, MlPageBlockBase pageBlock, MlHttpServletResponse resp) {
        String action = request.getString(HISTORY_UTIL_PARAM_NAME, Actions.SHOW);
        if (Actions.SHOW.equalsIgnoreCase(action)) {
            Query query = commonDao.getQueryWithoutSecurityCheck("select o from MlHistoryUtil o left join o.mlClasses c where c.id = :classId");
            query.setParameter("classId", metaDataHolder.getMlClassByName(request.getString("className")).getId());
            List<MlHistoryUtil> mlHistoryUtils = query.getResultList();
            if (mlHistoryUtils.size() > 1) {
                throw new MlApplicationException("Ошибка конфигурации!");
            }
            MlHistoryUtil mlHistoryUtil = mlHistoryUtils.get(0);
            TemplateEngine templateEngine = GuiceConfigSingleton.inject(TemplateEngine.class);
            Long linkedObjectId = request.getLong("objectId");
            MlClass linkedClass = metaDataHolder.getMlClassByName(request.getString("className"));
            HistoryDao historyDao = GuiceConfigSingleton.inject(HistoryDao.class);
            Map<String, Object> data = historyDao.getHistoryItems(linkedObjectId, linkedClass, mlHistoryUtil);
            data.put("objectId", request.getLong("objectId"));
            data.put("className", request.getString("className"));
            data.put("listClass", linkedClass);
            data.put("widgetMlInstance", pageBlock);

            MlDynamicEntityImpl entity = commonDao.findById(linkedObjectId, linkedClass.getEntityName());
            String title = PageHelper.getPageTitle(((MlPage) pageBlock.get("page")).getUrl(), entity.getTitle());
            String html = templateEngine.renderTemplate(SHOW_HISTORY_TEMPLATE, data);
            JsonObject dataJson = new JsonObject();
            dataJson.add("html", new JsonPrimitive(html));
            dataJson.add("title", new JsonPrimitive(title));
            resp.setJsonData(dataJson);
        } else if (Actions.SHOW_HISTORY.equals(action)) {


        }
    }


}
