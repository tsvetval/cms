package ru.peak.ml.web.block.controller.impl;

import com.google.gson.Gson;
import com.google.gson.JsonPrimitive;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.eclipse.persistence.dynamic.DynamicEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.AttrType;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.holders.EnumHolder;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlAttrView;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.model.system.MlEnum;
import ru.peak.ml.prop.Property;
import ru.peak.ml.web.block.controller.BlockController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.dao.impl.FolderDao;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * контроллер блока импорта данных
 */
@Deprecated
public class ImportObjectsBlockController implements BlockController {
    private static final Logger log = LoggerFactory.getLogger(ImportObjectsBlockController.class);

    //просмотр объекта
    private static final String ACTION_SHOW = "show";
    private static final String ACTION_IMPORT = "import";
    private static final String ACTION_DOWNLOAD_TEMPLATE = "download";

    private static final String DATETIME_FORMAT_STRING = "yyyy.MM.dd HH:mm";
    private static final String DATE_FORMAT_STRING = "yyyy.MM.dd";
    private static final String TIME_FORMAT_STRING = "HH:mm";


    static final String TEMPLATE_SHOW = "blocks/import/import.hml";
    public static final int SYMBOL_LENGTH = 256;
    private static final int MIN_WIDTH = SYMBOL_LENGTH * 10;
    AttrType[] supportedAttrTypes = {
            AttrType.STRING,
            AttrType.BOOLEAN,
            AttrType.DOUBLE,
            AttrType.LONG,
            AttrType.DATE,
            AttrType.ENUM,
            AttrType.MANY_TO_ONE,
            AttrType.TEXT};
    @Inject
    CommonDao commonDao;
    @Inject
    MetaDataHolder metaDataHolder;
    @Inject
    EnumHolder enumHolder;
    private int importUpdateCount = 0;
    private int importCreateCount = 0;


    @Override
    public void serve(MlHttpServletRequest params, MlPageBlockBase currentPageBlock, MlHttpServletResponse resp) {

        String action = params.getString("action");
        MlUser user = (MlUser) params.getRequest().getSession().getAttribute("user");

        switch (action) {
            /**
             * отображение блока
             */
            case ACTION_SHOW:
                if (params.hasString("folderId")) {
                    Long folderId = params.getLong("folderId");
                    FolderDao folderDao = GuiceConfigSingleton.inject(FolderDao.class);
                    MlClass mlClass = (MlClass) folderDao.getChildClassByFolderId(folderId);
                    Map<String, Object> data = new HashMap<>();
                    data.put("mlClass", mlClass);
                    data.put("currentPageBlock", currentPageBlock);
                    resp.addDataToJson("className", new JsonPrimitive(mlClass.getEntityName()));
                    resp.setDataType(MlHttpServletResponse.DataType.JSON);
                    resp.renderTemplateToJson(TEMPLATE_SHOW, data);
                } else if (params.hasString("className")) {
                    String className = params.getString("className");
                    Map<String, Object> data = new HashMap<>();
                    MlClass mlClass = metaDataHolder.getMlClassByName(className);
                    data.put("mlClass", mlClass);
                    data.put("currentPageBlock", currentPageBlock);
                    resp.addDataToJson("className", new JsonPrimitive(mlClass.getEntityName()));
                    resp.setDataType(MlHttpServletResponse.DataType.JSON);
                    resp.renderTemplateToJson(TEMPLATE_SHOW, data);
                } else {
                    throw new MlApplicationException("Некорректные параметры запроса, в запросе должен присутствовать [folderId] или [className]");
                }
                break;
            /**
             * импорт данных
             */
            case ACTION_IMPORT:
                String keyAttrListStr = params.getString("keyAttrListStr");
                String className = params.getString("className");
                Gson gson = new Gson();
                List<String> keyAttrNameList = gson.fromJson(keyAttrListStr, new TypeToken<List<String>>() {
                }.getType());
                Map<String, Integer> keyAttrIndexMap = new HashMap<>();
                for (String entityFieldName : keyAttrNameList) {
                    MlAttr attr = metaDataHolder.getAttr(className, entityFieldName);
                    if (attr != null) {
                        keyAttrIndexMap.put(attr.getEntityFieldName(), -1);
                    }
                }
                MlClass mlClass = metaDataHolder.getMlClassByName(className);
                String filename = params.getString("filename");
                importUpdateCount = 0;
                importCreateCount = 0;
                Path filepath = Paths.get(String.format("%s/user_%d/%s", Property.getTempDir(), user.getId(), filename));
                try {
                    Map<String, Object> data = getImportData(filepath, mlClass, keyAttrIndexMap);
                } catch (IOException e) {
                    throw new MlApplicationException("Ошибка импорта", e);
                }
                resp.setDataType(MlHttpServletResponse.DataType.JSON);
                resp.addDataToJson("importUpdateCount", new JsonPrimitive(importUpdateCount));
                resp.addDataToJson("importCreateCount", new JsonPrimitive(importCreateCount));
                break;
            /**
             * выгрузка шаблона
             */
            case ACTION_DOWNLOAD_TEMPLATE:
                if (params.hasString("className")) {
                    String name = params.getString("className");
                    Boolean fillData = false;
                    if (params.hasString("fillData")) {
                        fillData = params.getBoolean("fillData");
                    }
                    byte[] template = new byte[0];
                    try {
                        template = generateXLSXTemlpateForClass(name, fillData);
                    } catch (IOException e) {
                        throw new MlApplicationException(String.format("Ошибка генерации шаблона для класса %s", name), e);
                    }
                    resp.setDownloadData(template);
                    resp.setDownloadFileName(String.format("%s.xlsx", name));
                } else {
                    throw new MlApplicationException("Некорректные параметры запроса, в запросе должен присутствовать [className]");
                }
                break;
            default:
                log.error("Unknown action: " + action);
                throw new MlApplicationException("Данный функционал (" + action + ") пока не реализован");
        }
    }

    /**
     * генерация шаблона для импорта данных класса
     *
     * @param className -   имя класса для генерации шаблона
     * @param fillData  -   заполнять и шаблон текущими данными
     * @return -   файл шаблона в бинарном виде
     * @throws IOException
     */
    private byte[] generateXLSXTemlpateForClass(String className, Boolean fillData) throws IOException {
        MlClass mlClass = metaDataHolder.getMlClassByName(className);
        FileOutputStream file = new FileOutputStream(String.format("%s.xlsx", className));
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet worksheet = workbook.createSheet(className);
        XSSFRow headerRow = worksheet.createRow(0);

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setBorderBottom(CellStyle.BORDER_MEDIUM);
        headerCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        headerCellStyle.setBorderLeft(CellStyle.BORDER_MEDIUM);
        headerCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        headerCellStyle.setBorderRight(CellStyle.BORDER_MEDIUM);
        headerCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
        headerCellStyle.setBorderTop(CellStyle.BORDER_MEDIUM);
        headerCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

        int index = 0;
        for (MlAttr mlAttr : mlClass.getAttrSet()) {
            AttrType fieldType = mlAttr.getFieldType();
            String header;
            if (Arrays.asList(supportedAttrTypes).contains(fieldType)) {
                XSSFCell headerCell = headerRow.createCell(index++);
                header = String.format("%s [%s]", mlAttr.getTitle(), mlAttr.getTableFieldName());
                headerCell.setCellValue(header);
                headerCell.setCellStyle(headerCellStyle);
            }
        }

        for (int i = 0; i < index; i++) {
            worksheet.autoSizeColumn(i);
            if (worksheet.getColumnWidth(i) < SYMBOL_LENGTH) {
                worksheet.setColumnWidth(i, MIN_WIDTH);
            }
        }

        CellStyle dataCellStyle = workbook.createCellStyle();
        dataCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
        dataCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        dataCellStyle.setBorderLeft(CellStyle.BORDER_THIN);
        dataCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        dataCellStyle.setBorderRight(CellStyle.BORDER_THIN);
        dataCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
        dataCellStyle.setBorderTop(CellStyle.BORDER_THIN);
        dataCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

        int dataCellCount = 10;
        List<DynamicEntity> objectList = null;
        if (fillData) {
            Query query = commonDao.getQueryWithoutSecurityCheck(String.format("select o from %s o", className));
            objectList = query.getResultList();
            dataCellCount = objectList.size();
        }

        for (int i = 1; i <= dataCellCount; i++) {
            XSSFRow dataRow = worksheet.createRow(i);
            index = 0;
            for (MlAttr mlAttr : mlClass.getAttrSet()) {
                MlAttr attr = (MlAttr) mlAttr;
                AttrType fieldType = attr.getFieldType();
                if (!Arrays.asList(supportedAttrTypes).contains(fieldType)) {
                    continue;
                }
                XSSFCell dataCell = dataRow.createCell(index++);
                dataCell.setCellStyle(dataCellStyle);

                Object object = null;
                if (fillData && objectList != null) {
                    object = objectList.get(i - 1).get(attr.getEntityFieldName());
                }

                switch (fieldType) {
                    case STRING:
                        dataCell.setCellType(Cell.CELL_TYPE_STRING);
                        if (object != null) {
                            dataCell.setCellValue((String) object);
                        }
                        break;
                    case LONG:
                        dataCell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        if (object != null) {
                            dataCell.setCellValue((Long) object);
                        }
                        break;
                    case BOOLEAN:
                        dataCell.setCellType(Cell.CELL_TYPE_BOOLEAN);
                        if (object != null) {
                            dataCell.setCellValue((Boolean) object);
                        }
                        break;
                    case DATE:
                        dataCell.setCellType(Cell.CELL_TYPE_STRING);
                        if (object != null) {
                            MlAttrView view = attr.get("view");
                            String formatString = null;
                            if (view != null) {
                                String viewCode = view.getCode();
                                switch (viewCode) {
                                    case "DATETIME":
                                        formatString = DATETIME_FORMAT_STRING;
                                        break;
                                    case "DATE":
                                        formatString = DATE_FORMAT_STRING;
                                        break;
                                    case "TIME":
                                        formatString = TIME_FORMAT_STRING;
                                        break;
                                }
                            } else {
                                formatString = DATETIME_FORMAT_STRING;
                            }
                            if (formatString != null) {
                                Date date = (Date) object;
                                SimpleDateFormat dateFormat = new SimpleDateFormat(formatString);
                                dataCell.setCellValue(dateFormat.format(date));
                            }
                        }
                        break;
                    case ENUM:
                        dataCell.setCellType(Cell.CELL_TYPE_STRING);
                        if (object != null) {
                            dataCell.setCellValue(object.toString());
                        }
                        break;
                    case MANY_TO_ONE:
                        dataCell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        if (object != null) {
                            dataCell.setCellValue((Long) ((DynamicEntity) object).get("id"));
                        }
                        break;
                    case DOUBLE:
                        dataCell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        if (object != null) {
                            dataCell.setCellValue((Double) object);
                        }
                        break;
                    case TEXT:
                        dataCell.setCellType(Cell.CELL_TYPE_STRING);
                        if (object != null) {
                            dataCell.setCellValue((String) object);
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        ByteArrayOutputStream excelOutput = new ByteArrayOutputStream();
        workbook.write(excelOutput);
        excelOutput.close();
        return excelOutput.toByteArray();
    }

    /**
     * получение табличного имени поля из заголовка
     *
     * @param header -   строка заголовка
     * @return
     */
    private String extractTableFieldNameFromHeader(String header) {
        Integer from = header.indexOf("[");
        Integer to = header.indexOf("]");
        if (from == -1 || to == -1) {
            return null;
        } else {
            return header.substring(from + 1, to);
        }
    }

    /**
     * поиск атрибута класса по табличному имени
     *
     * @param mlClass        -   класс
     * @param tableFieldName -   табличное имя атрибута
     * @return
     */
    private MlAttr findMlAttrInClassByTableFieldName(MlClass mlClass, String tableFieldName) {
        List<MlAttr> attrList = mlClass.getAttrSet();
        for (MlAttr attr : attrList) {
            if (attr.getTableFieldName().equals(tableFieldName)) {
                return attr;
            }
        }
        return null;
    }

    /**
     * получения данных для импорта
     *
     * @param filepath        -   путь к XLSX-файлу с данными
     * @param mlClass         -   класс импортируемых данных
     * @param keyAttrIndexMap -   список атрибутов являющихся составным первичным ключом
     * @return -   список импортируемых данных
     * @throws IOException
     */
    @Transactional
    protected Map<String, Object> getImportData(Path filepath, MlClass mlClass, Map<String, Integer> keyAttrIndexMap) throws IOException {
        InputStream file = Files.newInputStream(filepath, StandardOpenOption.READ);
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);

        //строим мапу названий атрибутов - индексов колонок
        Row titleRow = sheet.getRow(0);
        int importAttrCount = titleRow.getLastCellNum();
        Map<Integer, MlAttr> indexImportAttrMap = new HashMap<>();
        for (int i = 0; i < importAttrCount; i++) {
            Cell cell = titleRow.getCell(i);
            String header = cell.getStringCellValue();
            String tableFieldName = extractTableFieldNameFromHeader(header);
            if (tableFieldName == null) {
                throw new MlApplicationException(String.format("Ошибка в ячейке [%s:%d], содержимое ячейки [%s]. Не найдено табличное имя в заголовке атрибута.", CellReference.convertNumToColString(cell.getColumnIndex()), cell.getRowIndex() + 1, header));
            }
            MlAttr attr = findMlAttrInClassByTableFieldName(mlClass, tableFieldName);
            if (attr == null) {
                throw new MlApplicationException(String.format("Ошибка в ячейке [%s:%d], содержимое ячейки [%s]. Не найден атрибут с указанным табличным именем [%s].", CellReference.convertNumToColString(cell.getColumnIndex()), cell.getRowIndex() + 1, header, tableFieldName));
            }
            if (indexImportAttrMap.containsValue(attr)) {
                throw new MlApplicationException(String.format("Ошибка в ячейке [%s:%d], содержимое ячейки [%s]. Атрибут с указанным табличным именем [%s] дублируется в таблице.", CellReference.convertNumToColString(cell.getColumnIndex()), cell.getRowIndex() + 1, header, tableFieldName));
            }
            indexImportAttrMap.put(i, attr);
            if (keyAttrIndexMap.keySet().contains(attr.getEntityFieldName())) {
                keyAttrIndexMap.put(attr.getEntityFieldName(), i);
            }
        }

        //подготавливаем шаблон для поиска по ключевым атрибутам
        StringBuilder stringBuilder = new StringBuilder("select o from ");
        stringBuilder.append(mlClass.getEntityName()).append(" o");
        String prefix = " where ";
        for (String entityFieldName : keyAttrIndexMap.keySet()) {
            if (keyAttrIndexMap.get(entityFieldName) == -1) {
                continue;
            }
            stringBuilder.append(prefix)
                    .append("o.")
                    .append(entityFieldName)
                    .append(" = :")
                    .append(entityFieldName);
            prefix = " and ";
        }
        String selectByKeyAttrsQuery = stringBuilder.toString();
        Query query = commonDao.getQueryWithoutSecurityCheck(selectByKeyAttrsQuery);

        for (int i = 1; i <= sheet.getLastRowNum(); i++) {
            Row row = sheet.getRow(i);
            MlDynamicEntityImpl object = findOrCreateObjectByKeys(mlClass, keyAttrIndexMap, query, row);
            for (int j = 0; j < importAttrCount; j++) {
                Cell cell = row.getCell(j);
                if (cell == null) {
                    continue;
                }
                MlAttr attr = indexImportAttrMap.get(j);
                updateAttrWithCell(object, attr, cell);
            }
            commonDao.persistWithSecurityCheck(object);
        }
        fixSequence(mlClass);
        return null;
    }

    /**
     * коррекция сиквенса для таблицы в БД
     *
     * @param mlClass -   класс для коррекции сиквенса
     */
    private void fixSequence(MlClass mlClass) {
        EntityManager entityManager = GuiceConfigSingleton.inject(EntityManager.class);
        Query fixSequenceQuery = entityManager.createNativeQuery(String.format("SELECT setval('\"%s_id_SEQ\"', (SELECT MAX(\"id\") FROM \"%s\"));",
                mlClass.getTableName(), mlClass.getTableName()));
        fixSequenceQuery.getSingleResult();
    }

    /**
     * получить значение ячейки XLSX-файла для атрибута
     *
     * @param cell -   ячейка
     * @param attr -   атрибут
     * @return -   объект-значение ячейки
     */
    private Object getCellValueForAttr(Cell cell, MlAttr attr) {
        Object value = null;
        int row = cell.getRowIndex() + 1;
        int col = cell.getColumnIndex();
        String colName = CellReference.convertNumToColString(col);
        AttrType fieldType = attr.getFieldType();
        int cellType = cell.getCellType();

        DataFormatter dataFormatter = new DataFormatter();

        switch (fieldType) {
            case LONG: {
                String valueStr;
                switch (cellType) {
                    case Cell.CELL_TYPE_NUMERIC: {
                        valueStr = dataFormatter.formatCellValue(cell);
                    }
                    break;
                    case Cell.CELL_TYPE_STRING: {
                        valueStr = cell.getStringCellValue();
                    }
                    break;
                    case Cell.CELL_TYPE_BLANK:
                        return null;
                    default:
                        throw new MlApplicationException(String.format("Неверный тип данных в ячейке [%s:%d]. Ожидается целое значение", colName, row));
                }
                if (valueStr.isEmpty()) {
                    return null;
                }
                try {
                    value = Long.valueOf(valueStr);
                } catch (NumberFormatException e) {
                    throw new MlApplicationException(String.format("Неверный тип данных в ячейке [%s:%d]. Ожидается целое значение", colName, row));
                }
            }
            break;
            case DOUBLE: {
                switch (cellType) {
                    case Cell.CELL_TYPE_NUMERIC: {
                        value = cell.getNumericCellValue();
                    }
                    break;
                    case Cell.CELL_TYPE_STRING: {
                        String valueStr = cell.getStringCellValue();
                        if (valueStr.isEmpty()) {
                            return null;
                        }
                        try {
                            value = Long.valueOf(valueStr);
                        } catch (NumberFormatException e) {
                            throw new MlApplicationException(String.format("Неверный тип данных в ячейке [%s:%d]. Ожидается вещественное значение", colName, row));
                        }
                    }
                    break;
                    case Cell.CELL_TYPE_BLANK:
                        return null;
                    default:
                        throw new MlApplicationException(String.format("Неверный тип данных в ячейке [%s:%d]. Ожидается вещественное значение", colName, row));
                }
            }
            break;
            case MANY_TO_ONE: {
                String valueStr;
                switch (cellType) {
                    case Cell.CELL_TYPE_NUMERIC: {
                        valueStr = dataFormatter.formatCellValue(cell);
                    }
                    break;
                    case Cell.CELL_TYPE_STRING: {
                        valueStr = cell.getStringCellValue();
                    }
                    break;
                    case Cell.CELL_TYPE_BLANK:
                        return null;
                    default:
                        throw new MlApplicationException(String.format("Неверный тип данных в ячейке [%s:%d]. Ожидается целое значение", colName, row));
                }
                Long id;
                try {
                    id = Long.valueOf(valueStr);
                } catch (NumberFormatException e) {
                    throw new MlApplicationException(String.format("Неверный тип данных в ячейке [%s:%d]. Ожидается целое значение", colName, row));
                }
                MlClass linkClass = attr.getLinkClass();
                value = commonDao.findById(id, linkClass.getEntityName());
                if (value == null) {
                    throw new MlApplicationException(String.format("Ошибка в ячейке [%s:%d], содержимое ячейки [%d]. Не найдено значение ссылочного класса с указанным идентификатором.", colName, row, id));
                }
            }
            break;
            case STRING:
            case TEXT: {
                switch (cellType) {
                    /*case Cell.CELL_TYPE_NUMERIC: {
                        value = dataFormatter.formatCellValue(cell);
                    }
                    break;*/
                    case Cell.CELL_TYPE_STRING: {
                        value = cell.getStringCellValue();
                    }
                    break;
                    case Cell.CELL_TYPE_BLANK: {
                        return null;
                    }
                    default:
                        throw new MlApplicationException(String.format("Неверный тип данных в ячейке [%s:%d]. Ожидается текстовое значение", colName, row));
                }
            }
            break;
            case ENUM: {
                String valueStr;
                switch (cellType) {
                    case Cell.CELL_TYPE_BLANK:
                        return null;
                    case Cell.CELL_TYPE_NUMERIC: {
                        valueStr = dataFormatter.formatCellValue(cell);
                    }
                    break;
                    case Cell.CELL_TYPE_STRING: {
                        valueStr = cell.getStringCellValue();
                    }
                    break;
                    default:
                        throw new MlApplicationException(String.format("Неверный тип данных в ячейке [%s:%d]. Ожидается текстовое значение", colName, row));
                }

                if (valueStr.isEmpty()) {
                    return null;
                }
                MlEnum anEnum = enumHolder.getEnum(attr, valueStr);
                if (anEnum == null) {
                    List<MlEnum> enumList = (List<MlEnum>) enumHolder.getEnumList(attr);
                    StringBuilder stringBuilder = new StringBuilder("[");
                    String prefix = "";
                    for (MlEnum en : enumList) {
                        stringBuilder.append(prefix);
                        stringBuilder.append(en.getCode());
                        prefix = ",";
                    }
                    stringBuilder.append("]");
                    throw new MlApplicationException(String.format("Ошибка в ячейке [%s:%d], содержимое ячейки [%s]. Не найдено значение перечислимого. " +
                            "Ожидается одно из следующих значений: %s", colName, row, valueStr, stringBuilder.toString()));

                } else {
                    value = anEnum.getCode();
                }
            }
            break;
            case DATE:
                switch (cellType) {
                    case Cell.CELL_TYPE_BLANK:
                        return null;
                    case Cell.CELL_TYPE_NUMERIC: {
                        Date date = cell.getDateCellValue();
                        value = new Timestamp(date.getTime());
                    }
                    break;
                    case Cell.CELL_TYPE_STRING: {
                        String dateString = cell.getStringCellValue();
                        if (dateString.isEmpty()) {
                            return null;
                        }
                        String formatString;
                        MlAttrView view = attr.get("view");
                        if (view != null) {
                            String viewCode = view.getCode();
                            switch (viewCode) {
                                case "DATETIME":
                                    formatString = DATETIME_FORMAT_STRING;
                                    break;
                                case "DATE":
                                    formatString = DATE_FORMAT_STRING;
                                    break;
                                case "TIME":
                                    formatString = TIME_FORMAT_STRING;
                                    break;
                                default:
                                    formatString = DATETIME_FORMAT_STRING;
                            }
                        } else {
                            formatString = DATETIME_FORMAT_STRING;
                        }
                        Date date;
                        try {
                            SimpleDateFormat dateFormat = new SimpleDateFormat(formatString);
                            date = dateFormat.parse(dateString);
                        } catch (ParseException e) {
                            throw new MlApplicationException(String.format("Ошибка в ячейке [%s:%d], содержимое ячейки [%s]. Дата указана в неверном формате. Ожидается ввод в виде %s",
                                    colName, row, dateString, formatString));
                        }
                        value = new Timestamp(date.getTime());
                    }
                    break;
                    default:
                        throw new MlApplicationException(String.format("Неверный тип данных в ячейке [%s:%d]. Ожидается дата.", colName, row));
                }
                break;
            case BOOLEAN:
                if (cellType == Cell.CELL_TYPE_BLANK)
                    return null;
                if (cellType == Cell.CELL_TYPE_STRING) {
                    String booleanString = cell.getStringCellValue();
                    if (booleanString.isEmpty()) {
                        return null;
                    }
                    try {
                        value = Boolean.valueOf(booleanString);
                    } catch (Exception e) {
                        throw new MlApplicationException(String.format("Неверный тип данных в ячейке [%s:%d]. Ожидается булевое значение", colName, row));
                    }
                } else if (cellType == Cell.CELL_TYPE_BOOLEAN) {
                    value = cell.getBooleanCellValue();
                } else {
                    throw new MlApplicationException(String.format("Неверный тип данных в ячейке [%s:%d]. Ожидается булевое значение", colName, row));
                }
                break;
        }
        return value;
    }

    /**
     * Обновить значение аттрибута по занчению в ячейке
     *
     * @param object -   объект-владелец атрибута
     * @param attr   -   атрибут для обновления
     * @param cell   -   ячейка XLSX-файла с данными
     */
    private void updateAttrWithCell(MlDynamicEntityImpl object, MlAttr attr, Cell cell) {
        Object value = getCellValueForAttr(cell, attr);
        if (value != null) {
            if (attr.getPrimaryKey()) {
                if (object.get(attr.getEntityFieldName()) == null) {
                    if (commonDao.findById(value, object.getInstanceMlClass().getEntityName()) != null) {
                        throw new MlApplicationException(String.format("Ошибка в ячейке [%s:%d]. Объект с таким первичным ключем уже существует.", CellReference.convertNumToColString(cell.getColumnIndex()), cell.getRowIndex() + 1));
                    }
                }
            }
            object.set(attr.getEntityFieldName(), value);
        } else if (!attr.getPrimaryKey()) {
            object.set(attr.getEntityFieldName(), value);
        }
    }

    /**
     * найти объект по заданным ключевым атрибутам или создать новый объект
     *
     * @param mlClass         -   класс объекта
     * @param keyAttrIndexMap -   список ключевых атрибутов (имя/позиция в строке)
     * @param query           -   JPQL-запрос
     * @param row             -   строка XSLX-файла
     * @return -   новый или найденный объект
     */
    private MlDynamicEntityImpl findOrCreateObjectByKeys(MlClass mlClass, Map<String, Integer> keyAttrIndexMap, Query query, Row row) {
        //подсчёт ключевых атрибутов и создание
        //временной мапы для вывода ошибки
        int keyAttrCount = 0;
        Map<String, Object> tempAttrMap = new HashMap<>();
        for (String entityFieldName : keyAttrIndexMap.keySet()) {
            Integer index = keyAttrIndexMap.get(entityFieldName);
            if (index == -1) {
                continue;
            }
            Cell cell = row.getCell(index);
            MlAttr attr = metaDataHolder.getAttr(mlClass.getEntityName(), entityFieldName);
            keyAttrCount++;
            Object value = getCellValueForAttr(cell, attr);
            tempAttrMap.put(attr.getTitle(), value);
            query.setParameter(entityFieldName, value);
        }

        List<DynamicEntity> objectList = query.getResultList();
        if (objectList.size() > 1 && keyAttrCount > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            String prefix = "";
            for (String title : tempAttrMap.keySet()) {
                stringBuilder.append(prefix)
                        .append(title)
                        .append(": ")
                        .append(tempAttrMap.get(title));
                prefix = ", ";
            }
            throw new MlApplicationException(String.format("Ошибка в строке %d. С выбранным набором ключевых полей в БД обнаружены неуникальные объекты. " +
                    "Набору [%s] соответствует объектов: %d", row.getRowNum(), stringBuilder.toString(), objectList.size()));
        } else if (objectList.size() == 1 && keyAttrCount > 0) {
            importUpdateCount++;
            return (MlDynamicEntityImpl) objectList.get(0);
        } else {
            importCreateCount++;
            MlDynamicEntityImpl entity = commonDao.createNewEntity(metaDataHolder.getEntityClassByName(mlClass.getEntityName()));
            EntityManager entityManager = GuiceConfigSingleton.inject(EntityManager.class);

            return entity;
        }
    }
}

