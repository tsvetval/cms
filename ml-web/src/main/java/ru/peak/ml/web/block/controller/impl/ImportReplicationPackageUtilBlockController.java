package ru.peak.ml.web.block.controller.impl;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.prop.Property;
import ru.peak.ml.template.TemplateEngine;
import ru.peak.ml.web.block.controller.BlockController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.model.MlImportReplicationFile;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.web.replication.helper.ImportReplicationOption;
import ru.peak.ml.web.replication.helper.Replication;
import ru.peak.ml.web.service.MlReplicationService;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;

/**
 * Контроллер блока импорта файла репликации
 */
public class ImportReplicationPackageUtilBlockController implements BlockController {
    private static final Logger log = LoggerFactory.getLogger(ImportReplicationPackageUtilBlockController.class);
    private static final String SELECT_TEMPLATE = "blocks/utils/replication/import/selectImportFile.hml";
    private static final String SHOW_INFO = "blocks/utils/replication/import/showInfoFile.hml";
    private static final String SHOW_RESULT = "blocks/utils/replication/import/showResultImport.hml";
    @Inject
    CommonDao commonDao;
    @Inject
    MlReplicationService replicationService;

    @Override
    @Transactional
    public void serve(MlHttpServletRequest request, MlPageBlockBase mlPageBlockBase, MlHttpServletResponse resp){
        String action = request.getString("action", "show");
        // отображение шаблона блока импорта файла репликации
        if (action.equals("show")) {
            JsonObject result = new JsonObject();
            result.add("idModal", new JsonPrimitive((Long) mlPageBlockBase.get("id")));
            result.add("title", new JsonPrimitive("Выбрать файл блока обновлений"));
            TemplateEngine templateEngine = GuiceConfigSingleton.inject(TemplateEngine.class);
            HashMap<String, Object> data = new HashMap<>();
            data.put("widgetMlInstance", mlPageBlockBase);
            String html = templateEngine.renderTemplate(SELECT_TEMPLATE, data);
            result.add("html", new JsonPrimitive(html));
            resp.setJsonData(result);
        // отображение информации о файле репликации
        } else if (action.equals("showFileInfo")) {
            String fileName = request.getString("fileName");
            ImportReplicationOption.ImportType importType = ImportReplicationOption.ImportType.valueOf(request.getString("importType"));
            JsonObject result = new JsonObject();
            result.add("showType", new JsonPrimitive("modal"));
            result.add("idModal", new JsonPrimitive((Long) mlPageBlockBase.get("id")));
            byte[] data = getZipFileBytes(fileName);

            Replication replication = null;
            try {
                replication = replicationService.importReplication(data, true, new ImportReplicationOption(importType));
            } catch (Exception e) {
                log.error("",e);
                throw new ru.ml.core.common.exceptions.MlApplicationException("Ошибка разбора пакета обновлений "+e.getMessage());
            }
            replication.setFileName(fileName);
            TemplateEngine templateEngine = GuiceConfigSingleton.inject(TemplateEngine.class);
            HashMap<String, Object> dataMap = new HashMap<>();
            dataMap.put("block", replication);
            dataMap.put("importType", importType);
            dataMap.put("widgetMlInstance", mlPageBlockBase);
            String html = templateEngine.renderTemplate(SHOW_INFO, dataMap);
            result.add("html", new JsonPrimitive(html));
            result.add("title", new JsonPrimitive("Информация по пакету обновлений"));
            resp.setJsonData(result);
        // импорт файла репликации
        } else if (action.equals("doReplication")) {
            String fileName = request.getString("fileName");
            byte[] data = getZipFileBytes(fileName);
            ImportReplicationOption.ImportType importType = ImportReplicationOption.ImportType.valueOf(request.getString("importType"));

            Replication replication = null;
            try {
                replication = replicationService.importReplication(data, false, new ImportReplicationOption(importType));
            } catch (Exception e) {
                log.error("",e);
                throw new MlApplicationException("Ошибка разбора пакета обновлений "+e.getMessage());
            }
            replication.setFileName(fileName);
            JsonObject result = new JsonObject();
            result.add("title", new JsonPrimitive("Результат импорта блока обновлений"));
            TemplateEngine templateEngine = GuiceConfigSingleton.inject(TemplateEngine.class);
            HashMap<String, Object> dataMap = new HashMap<>();
            dataMap.put("widgetMlInstance", mlPageBlockBase);
            dataMap.put("block", replication);
            String html = templateEngine.renderTemplate(SHOW_RESULT, dataMap);
            result.add("html", new JsonPrimitive(html));

            MlImportReplicationFile mlImportReplication = (MlImportReplicationFile) commonDao.createNewEntity(MlImportReplicationFile.class);
            mlImportReplication.setDateLoad(new Date());
            mlImportReplication.setReplicationFile(data);
            mlImportReplication.setName(replication.getName());
            mlImportReplication.setReplicationFileName(fileName);
            mlImportReplication.setReportFile(html.getBytes());
            mlImportReplication.setReportFileName("report_" + fileName.replace(".zip", ".html"));
            commonDao.persistWithSecurityCheck(mlImportReplication);

            resp.setJsonData(result);
        }
    }

    /**
     * Возвращает бинарные данные файла
     *
     * @param fileName              -   имя файла репликации
     * @return                      -   бинарные данные (содержимое файла)
     * @throws MlServerException
     */
    private byte[] getZipFileBytes(String fileName) throws MlServerException {
        try{
        Long userId = 1L;//user.getId();
        byte[] data;
        Path tempPath = Paths.get(String.format("%s/user_%d/%s", Property.getTempDir(), userId, fileName));
        data = Files.readAllBytes(tempPath);
        return data;
        }catch (Exception e){
            log.error("",e);
            throw new MlServerException(e.getMessage(),e);
        }
    }

}
