package ru.peak.ml.web.block.controller.impl;

import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.peak.ml.web.block.controller.BlockController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.core.model.page.MlPageBlockBase;

import java.util.HashMap;

/**
 *
 */
public class MlStaticPageBlockController implements BlockController {
    @Override
    public void serve(MlHttpServletRequest params, MlPageBlockBase mlInstance, MlHttpServletResponse resp) throws MlApplicationException, MlServerException {
        //This controller do nothing
        //String templatePath = ((MlStaticPageBlock)mlInstance).getTemplatePath();
        // override main template
        String action = params.getString("action", "show");
        String templatePath = (mlInstance.getTemplate().isEmpty()) ?
                "/templates/hidden.hml" : mlInstance.getTemplate();
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        resp.renderTemplateToJson(templatePath, new HashMap<String, Object>());
    }
}
