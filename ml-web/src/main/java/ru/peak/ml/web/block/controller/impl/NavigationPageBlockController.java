package ru.peak.ml.web.block.controller.impl;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.model.folder.MlFolder;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.web.block.controller.BlockController;
import ru.peak.ml.web.navigation.FolderRequest;
import ru.peak.ml.web.navigation.FolderService;
import ru.peak.ml.web.navigation.classifier.ClassifierService;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.dao.impl.FolderDao;
import ru.peak.ml.web.helper.IconHelper;
import ru.peak.ml.web.service.annotations.PageBlockAction;
import ru.peak.security.services.AccessServiceImpl;

import java.util.List;

/**
 *
 */
public class NavigationPageBlockController implements BlockController {
    private static final Logger log = LoggerFactory.getLogger(NavigationPageBlockController.class);
    @Inject
    CommonDao commonDao;
    private static final String BLOCK_TEMPLATE = "blocks/navigation/init.hml";
    @Inject
    AccessServiceImpl accessService;
    @Inject
    FolderDao folderDao;
    @Inject
    FolderService folderService;

    private static class Actions {
        public static final String CREATE_NODE = "createNode";
        public static final String MODIFY_NODE = "modifyNode";
        public static final String REMOVE_NODE = "removeNode";
    }

    @PageBlockAction(action = "getFolderChildren")
    public void getFolderChildren(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase pageBlock) {
        FolderRequest folderRequest = new FolderRequest(params.getLong("id", null),params.getString("classifierId", null),params.getString("value", null));
        resp.setJsonData(folderService.serializeFolders(folderRequest));
    }




    @PageBlockAction(action = "findFolderPath", dataType = MlHttpServletResponse.DataType.JSON)
    public void findFolderPath(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase pageBlock) {
        if (!params.hasLong("folderId")) {
            throw new MlApplicationException("folderId not found in request");
        }
        Long folderId = params.getLong("folderId");
        List<Long> path = folderDao.findPath(folderId);
        JsonArray jsonArray = new JsonArray();
        for (Long id : path) {
            jsonArray.add(new JsonPrimitive(id));
        }
        resp.setJsonData(jsonArray);
    }

    public void serve(MlHttpServletRequest request, MlPageBlockBase pageBlock, MlHttpServletResponse resp) {
        //TODO переделать через вызов методов
        String action = request.getString("action", "show");
        if (Actions.MODIFY_NODE.equals(action)) {
            FolderDao folderDao = GuiceConfigSingleton.inject(FolderDao.class);

            JsonArray result = new JsonArray();
            JsonObject dataJson = new JsonObject();

            // Вытаскиваем редактируемый объект из базы
            long objectId = request.getLong("objectId");
            MlFolder mlFolder = folderDao.findById(objectId);

            // Изменяем необходимые параметры
            mlFolder.setTitle(request.getString("text"));

            // Сохраняем
            commonDao.persistTransactionalWithSecurityCheck(mlFolder);

            dataJson.add("objectId", new JsonPrimitive(mlFolder.getId()));
            result.add(dataJson);
            resp.setJsonData(result);
        } else if (Actions.CREATE_NODE.equals(action)) {
            FolderDao folderDao = GuiceConfigSingleton.inject(FolderDao.class);

            JsonArray result = new JsonArray();
            JsonObject dataJson = new JsonObject();

            // Получаем родительский объект (если он задан и это не корень)
            MlFolder parent = null;
            if (request.hasLong("parentId")) {
                parent = folderDao.findById(request.getLong("parentId"));
            }

            // Создаем новый объект и настраиваем для него родителя

            MlFolder mlFolder = (MlFolder) commonDao.createNewEntity(MlFolder.class);
            mlFolder.setParent(parent);

            // Изменяем необходимые параметры
            mlFolder.setTitle(request.getString("text"));
            commonDao.persistTransactionalWithSecurityCheck(mlFolder);

            dataJson.add("objectId", new JsonPrimitive(mlFolder.getId()));
            result.add(dataJson);
            resp.setJsonData(result);
        } else if (Actions.REMOVE_NODE.equals(action)) {
            long objectId = request.getLong("objectId");
            FolderDao folderDao = GuiceConfigSingleton.inject(FolderDao.class);
            MlFolder mlFolder = folderDao.findById(objectId);
            MlFolder parent = mlFolder.getParent();
            commonDao.removeTransactional(mlFolder);
            if (parent != null) {
                JsonArray result = new JsonArray();
                JsonObject dataJson = new JsonObject();
                dataJson.add("parentId", new JsonPrimitive(parent.getId()));
                result.add(dataJson);
                resp.setJsonData(result);
            }
        } else {
            log.error("Unknown action: " + action);
            throw new MlApplicationException("Данный функционал (" + action + ") пока не реализован");
        }
    }

}
