package ru.peak.ml.web.block.controller.impl;

import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.web.block.controller.BlockController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.service.annotations.PageBlockAction;

/**
 * Контроллер блока мастера создания страниц
 */
public class PageWizardBlockController implements BlockController {
    private static final Logger log = LoggerFactory.getLogger(PageWizardBlockController.class);

    @Inject
    CommonDao commonDao;

    @Inject
    MetaDataHolder metaDataHolder;

    public void serve(MlHttpServletRequest params, MlPageBlockBase mlInstance, MlHttpServletResponse resp) throws MlApplicationException, MlServerException {
        log.error(String.format("Unknown action: [%s] in class [%s] ", params.getString("action"), this.getClass()));
        throw new MlApplicationException(String.format("В контроллере %s Данный функционал (%s) пока не реализован", this.getClass(), params.getString("action")));
    }

    @PageBlockAction(action = "getPageBlockInfo")
    public void getMetaData(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        MlPageBlockBase pageBlock;
        if (params.hasString("className") && params.hasLong("objectId")) {
            pageBlock = (MlPageBlockBase) commonDao.findById(params.getLong("objectId"), params.getString("className"));
        } else {
            throw new MlApplicationException("Некорректные параметры запроса, в запросе должны присутствовать [className] и [objectId]");
        }
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        resp.addDataToJson("jsBootFilename", new JsonPrimitive(pageBlock.getBootJs().concat(".js")));
        resp.addDataToJson("javaControllerFilename", new JsonPrimitive(pageBlock.getControllerJavaClass().replace(".", "/").concat(".java")));
    }

}

