package ru.peak.ml.web.block.controller.impl;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.AttrType;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.services.AccessService;
import ru.peak.ml.web.block.controller.BlockController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.dao.impl.FolderDao;
import ru.peak.ml.web.gson.adapter.serializers.MlDynamicEntitySerializerImpl;
import ru.peak.ml.web.gson.adapter.serializers.bean.AttrSerializerOption;
import ru.peak.ml.web.service.annotations.PageBlockAction;

import java.util.ArrayList;
import java.util.List;

public class SearchBlockController implements BlockController {
    private static final Logger log = LoggerFactory.getLogger(SearchBlockController.class);
    @Inject
    MetaDataHolder metaDataHolder;
    @Inject
    AccessService accessService;
    @Inject
    CommonDao commonDao;
    @Inject
    MlDynamicEntitySerializerImpl serializer;

    @Override
    public void serve(MlHttpServletRequest params, MlPageBlockBase mlInstance, MlHttpServletResponse resp) throws MlApplicationException, MlServerException {
        log.error(String.format("Unknown action: [%s] in class [%s] ", params.getString("action"), this.getClass()));
        throw new MlApplicationException(String.format("В контроллере %s Данный функционал (%s) пока не реализован", this.getClass(), params.getString("action")));
    }

    @PageBlockAction(action = "getSearchMetaData")
    public void getSearchMetaData(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        FolderDao folderDao = GuiceConfigSingleton.inject(FolderDao.class);
        MlClass listClass = null;

        String refClassName = "";
        if (params.hasLong("folderId")) {
            Long folderId = params.getLong("folderId");
            listClass = folderDao.getChildClassByFolderId(folderId);
        } else if (params.hasString("className")) {
            String className = params.getString("className");
            if (className.contains("@")) {
                Long id = Long.parseLong(className.split("@")[0]);
                className = className.split("@")[1];
                listClass = (MlClass) commonDao.findById(id, className);
            } else if(params.hasLong("objectId") && "MlClass".equals(className)){
                listClass = (MlClass) commonDao.findById(params.getLong("objectId"),className);
            }else {
                listClass = metaDataHolder.getMlClassByName(className);
            }
        } else if (params.hasLong("refAttrId")) {
            MlAttr refAttr = metaDataHolder.getAttrById(params.getLong("refAttrId"));
            // В качестве ссылочного атрибута может прийти геторегенный(при первом открыитии страницы)
            if (!refAttr.getFieldType().equals(AttrType.HETERO_LINK)) {
                listClass = refAttr.getAttrLinkClass();
            }
        } else {
            log.error("Wrong arguments missing [folderId] or [className] params");
        }

        String searchType = "simple";
        if (params.hasString("type")) {
            searchType = params.getString("type");
        }

        JsonObject dataJson = new JsonObject();
        if (listClass != null) {
            List<MlAttr> attrListToSearch = new ArrayList<>();
            if ("simple".equals(searchType)) {
                attrListToSearch = metaDataHolder.getSimpleSearchAttrList(listClass);
            } else {
                attrListToSearch = metaDataHolder.getExtendedSearchAttrList(listClass);
            }
            List<AttrSerializerOption> serializerOption = new ArrayList<>();
            serializerOption.add(new AttrSerializerOption(metaDataHolder.getAttr("MlAttr", "entityFieldName")));
            serializerOption.add(new AttrSerializerOption(metaDataHolder.getAttr("MlAttr", "description")));
            serializerOption.add(new AttrSerializerOption(metaDataHolder.getAttr("MlAttr", "fieldType")));
            serializerOption.add(new AttrSerializerOption(metaDataHolder.getAttr("MlAttr", "linkClass")));
            serializerOption.add(
                    new AttrSerializerOption(metaDataHolder.getAttr("MlAttr", "enumList"))
                        .addAttrOption(metaDataHolder.getAttr("MlEnum", "code"))
                        .addAttrOption(metaDataHolder.getAttr("MlEnum", "title"))
            );
            serializerOption.add(new AttrSerializerOption(metaDataHolder.getAttr("MlAttr", "fieldFormat")));

            JsonObject attrListJson = serializer.serializeObjectListWithAttrOptions(attrListToSearch, serializerOption);
            dataJson.add("attrList", attrListJson);
            dataJson.add("entityName", new JsonPrimitive(listClass.getEntityName()));
            dataJson.add("ok", new JsonPrimitive(true));
        } else {
            dataJson.add("ok", new JsonPrimitive(false));
        }
        resp.setJsonData(dataJson);
    }
}
