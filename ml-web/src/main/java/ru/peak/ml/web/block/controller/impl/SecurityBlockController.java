package ru.peak.ml.web.block.controller.impl;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.security.MlSecuritySettings;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.ml.prop.MlProperties;
import ru.peak.ml.prop.Property;
import ru.peak.ml.web.block.controller.BlockController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.service.annotations.PageBlockAction;
import ru.peak.security.dao.MlUserDao;
import ru.peak.security.filter.SecurityFilter;
import ru.peak.security.services.AuthenticateService;
import ru.peak.security.services.impl.SecuritySettingProvider;

import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;

public class SecurityBlockController implements BlockController {
    private static final Logger log = LoggerFactory.getLogger(SecurityBlockController.class);
    @Inject
    MlProperties mlProperties;
    @Inject
    AuthenticateService authenticateService;
    @Inject
    MlUserDao userDao;
    @Inject
    SecuritySettingProvider securitySettingProvider;


    @PageBlockAction(action = "getUserData")
    public void getUserData(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        HttpSession session = params.getRequest().getSession();
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        if (session.getAttribute(SecurityFilter.USER_KEY_SESSION) != null) {
            Long userId = (Long) session.getAttribute(SecurityFilter.USER_KEY_SESSION);
            MlUser user = userDao.getUserById(userId);
            resp.addDataToJson("loggedIn", new JsonPrimitive(true));
            resp.addDataToJson("login", new JsonPrimitive(user.get("login").toString()));
            if (user.get("helpUrl") != null) {
                resp.addDataToJson("helpUrl", new JsonPrimitive(user.get("helpUrl").toString()));
            }
        } else {
            resp.addDataToJson("loggedIn", new JsonPrimitive(false));
        }
    }

    @PageBlockAction(action = "logout")
    public void logout(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        HttpSession session = params.getRequest().getSession();
        Long userId = (Long) session.getAttribute(SecurityFilter.USER_KEY_SESSION);
        MlUser user = userDao.getUserById(userId);
        if (user != null) {
            log.info(String.format("LOGOUT login = %s, logout", user.getLogin()));
        } else {
            log.info("LOGOUT user");
        }
        session.invalidate();
        String url = params.getRequest().getContextPath() + "/" + mlProperties.getProperty(Property.AUTH_PAGE);
        resp.addDataToJson("logoutUrl", new JsonPrimitive(url));
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
    }

    @PageBlockAction(action = "login")
    public void login(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        // parse params
        String login = params.getString("login", null);
        String password = params.getString("password", null);
        if (login == null && password == null) {
            throw new MlApplicationException("Некорректные параметры запроса, в запросе должны присутствовать [login, password]");
        }

        //Boolean authorized = false;
        Boolean needToChangePassword = false;
        String url = params.getRequest().getContextPath();
        log.info(String.format("Try auth width login: %s", login));
        MlUser mlUser = authenticateService.authenticate(login, password);

        if (mlUser != null) {
            log.info("Auth success!");
            MlSecuritySettings securitySettings = securitySettingProvider.getSettingsForUser(mlUser);
            HttpSession session = params.getRequest().getSession();
            session.setMaxInactiveInterval((int) (securitySettings.getSessionLifeTime() * 60));
            session.setAttribute(SecurityFilter.USER_KEY_SESSION, mlUser.getId());
            session.setAttribute(SecurityFilter.REQUEST_PARAMETER_USER, mlUser);

            needToChangePassword = mlUser.get("needChangePassword");
            if (needToChangePassword != null && needToChangePassword) {
                session.setAttribute(SecurityFilter.USER_NEED_CHANGE_PASSWORD, true);
            }
            //url = params.getRequest().getContextPath();
            if (mlUser.getHomePage() != null) {
                url = url + "/" + mlUser.getHomePage();
            }
            //authorized = true;
            // После ваторизации добавляем в куку домашнюю страницу и логин(потом будем выводить при логине)
            int loginCookieAge = 24 * 60 * 60;
            Cookie userLoginCookie = new Cookie("userLogin", mlUser.getLogin());
            userLoginCookie.setMaxAge(loginCookieAge);
            resp.addCookie(userLoginCookie);

            Cookie homePage = new Cookie("homePage", mlUser.getHomePage());
            homePage.setPath(params.getRequest().getContextPath() + "/");
            homePage.setMaxAge(loginCookieAge);
            resp.addCookie(homePage);

        } else {
            log.info("Auth fail! for login " + login);
            throw new MlApplicationException("Неверное значение пары логин/пароль");
        }

        JsonObject dataJson = new JsonObject();
        dataJson.add("authorized", new JsonPrimitive(true));
        dataJson.add("useHomePageUrl",
                new JsonPrimitive(mlProperties.getProperty(Property.USE_HOME_PAGE_URL).toUpperCase().equals("TRUE")));
        if (url != null) {
            dataJson.add("url", new JsonPrimitive(url));
        }
        resp.setJsonData(dataJson);
    }


    @Override
    public void serve(MlHttpServletRequest params, MlPageBlockBase mlInstance, MlHttpServletResponse resp) throws MlApplicationException, MlServerException {
        log.error(String.format("Unknown action: [%s] in class [%s] ", params.getString("action"), this.getClass()));
        throw new MlApplicationException(String.format("В контроллере %s Данный функционал (%s) пока не реализован", this.getClass(), params.getString("action")));
    }
}
