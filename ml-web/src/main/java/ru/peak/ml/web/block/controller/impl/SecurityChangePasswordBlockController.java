package ru.peak.ml.web.block.controller.impl;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.model.security.MlSecuritySettings;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.ml.template.TemplateEngine;
import ru.peak.ml.web.block.controller.BlockController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.security.filter.SecurityFilter;
import ru.peak.security.services.impl.SecurityService;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * контроллер блока смены пароля
 *
 */
public class SecurityChangePasswordBlockController implements BlockController {
    private static final Logger log = LoggerFactory.getLogger(SecurityChangePasswordBlockController.class);
    //путь к файлам шаблонов
    private static final String TEMPLATE_PREFIX = "blocks/security/";
    //расширение шаблонов
    private static final String TEMPLATE_POSTFIX = ".hml";
    private static final String ACTION_SHOW = "show";
    private static final String ACTION_CHANGE = "change";
    private static final String ACTION_VALIDATE = "validate";
    private static final String URL_CHANGE_PASSWORD = "changepassword";

    @Override
    public void serve(MlHttpServletRequest params, MlPageBlockBase currentPageBlock, MlHttpServletResponse resp) {
        StringBuilder templatePath = new StringBuilder();
        templatePath.append(TEMPLATE_PREFIX);
        Map<String, Object> data = new HashMap<>();
        String action = params.getString("action", ACTION_SHOW);
        SecurityService securityService = GuiceConfigSingleton.inject(SecurityService.class);
        switch (action) {
            // отображение блока смены пароля
            case ACTION_SHOW: {
                HttpSession session = params.getRequest().getSession();
                Long userId = (Long) session.getAttribute(SecurityFilter.USER_KEY_SESSION);
                MlUser user = securityService.getUserById(userId);
                data.put("login", user.get("login"));
                MlSecuritySettings mlSecuritySettings = securityService.getSettingsByUser(user);
                data.put("minLength", mlSecuritySettings.getPasswordStrengthLength());
                data.put("alphabet", mlSecuritySettings.getPasswordStrengthAlphabet().ordinal());
                templatePath.append(URL_CHANGE_PASSWORD);
                break;
            }
            // смена пароля, в параметрах должны содержаться
            // oldPassword (string) -   старый пароль
            // password (string)    -   новый пароль
            // confirmation (string)-   подтверждение пароля
            case ACTION_CHANGE: {
                HttpSession session = params.getRequest().getSession();
                Long userId = (Long) session.getAttribute(SecurityFilter.USER_KEY_SESSION);
                MlUser user = securityService.getUserById(userId);
                String oldPassword = params.getString("oldPassword");
                String password = params.getString("password");
                String confirmation = params.getString("confirmation");
                Boolean passwordIsValid = securityService.validatePassword(user, oldPassword, password, confirmation);
                if(passwordIsValid){
                    securityService.changePassword(user, password);
                    session.invalidate();
                    resp.addDataToJson("passwordChanged", new JsonPrimitive(true));
                    resp.addDataToJson("url", new JsonPrimitive(params.getRequest().getContextPath()));
                } else {
                    resp.addDataToJson("passwordChanged", new JsonPrimitive(false));
                }
                resp.setDataType(MlHttpServletResponse.DataType.JSON);
                return;
            }
            // валидация пароля, в параметрах должны содержаться
            // oldPassword (string) -   старый пароль
            // password (string)    -   новый пароль
            // confirmation (string)-   подтверждение пароля
            case ACTION_VALIDATE: {
                HttpSession session = params.getRequest().getSession();
                Long userId = (Long) session.getAttribute(SecurityFilter.USER_KEY_SESSION);
                MlUser user = securityService.getUserById(userId);
                String oldPassword = params.getString("oldPassword");
                String password = params.getString("password");
                String confirmation = params.getString("confirmation");
                Boolean oldPasswordIsOk = securityService.oldPasswordIsOk(user, oldPassword);
                Boolean confirmationIsOk = securityService.confirmationIsOk(password, confirmation);
                Boolean passwordLengthIsOk = securityService.passwordLengthIsOk(user, password);
                Boolean passwordAlphabetIsOk = securityService.passwordAlphabetIsOk(user, password);
                Boolean passwordUniqueIsOk = securityService.passwordUniqueIsOk(user, password);
                resp.addDataToJson("oldPasswordIsOk", new JsonPrimitive(oldPasswordIsOk));
                resp.addDataToJson("confirmationIsOk", new JsonPrimitive(confirmationIsOk));
                resp.addDataToJson("passwordLengthIsOk", new JsonPrimitive(passwordLengthIsOk));
                resp.addDataToJson("passwordAlphabetIsOk", new JsonPrimitive(passwordAlphabetIsOk));
                resp.addDataToJson("passwordUniqueIsOk", new JsonPrimitive(passwordUniqueIsOk));
                resp.setDataType(MlHttpServletResponse.DataType.JSON);
                return;
            }
        }

        templatePath.append(TEMPLATE_POSTFIX);
        data.put("currentPageBlock", currentPageBlock);
        TemplateEngine templateEngine = GuiceConfigSingleton.inject(TemplateEngine.class);
        String html = templateEngine.renderTemplate(templatePath.toString(), data);
        JsonObject dataJson = new JsonObject();
        dataJson.add("html", new JsonPrimitive(html));
        String title = (String) data.get("title");
        if (title != null) {
            dataJson.add("title", new JsonPrimitive(title));
        }
        resp.setJsonData(dataJson);
    }

}
