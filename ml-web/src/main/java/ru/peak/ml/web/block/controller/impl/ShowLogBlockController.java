package ru.peak.ml.web.block.controller.impl;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.web.block.controller.BlockController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.service.annotations.PageBlockAction;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 */
public class ShowLogBlockController implements BlockController {
    private static final Logger log = LoggerFactory.getLogger(ShowLogBlockController.class);

    @Override
    public void serve(MlHttpServletRequest params, MlPageBlockBase mlInstance, MlHttpServletResponse resp) throws MlApplicationException, MlServerException {
        log.error(String.format("Unknown action: [%s] in class [%s] ", params.getString("action"), this.getClass()));
        throw new MlApplicationException(String.format("В контроллере %s Данный функционал (%s) пока не реализован", this.getClass(), params.getString("action")));
    }

    @PageBlockAction(action = "download")
    private void downloadFile(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        String fileName = params.getString("fileName");
        resp.setDataType(MlHttpServletResponse.DataType.NONE);
        try {
            resp.downloadFile(fileName,
                    getLogFolder(params).getAbsolutePath() + "/" + fileName,
                    URLConnection.guessContentTypeFromName(fileName));
        } catch (IOException e) {
            e.printStackTrace();
            throw new MlServerException("Ошибка чтения файла " + fileName, e);
        }
    }

    @PageBlockAction(action = "showFile")
    private void getFile(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();
        String fileName = params.getString("fileName");
        Long lineNumber = params.getLong("position");
        result.add("idModal", new JsonPrimitive((Long) mlInstance.get("id")));
        result.add("title", new JsonPrimitive("Просмотреть логи"));
        try {
            LogFileLines logFileLines = getLogFile(fileName, getLogFolder(params), lineNumber);
            List<String> lines = logFileLines.getLines();
            result.add("lines", new JsonPrimitive(gson.toJson(lines)));
            result.add("position", new JsonPrimitive(logFileLines.getPosition()));
        } catch (IOException e) {
            throw new MlServerException("Ошибка чтения файла " + fileName, e);
        }
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        resp.setJsonData(result);
    }

    @PageBlockAction(action = "clearFile")
    private void clearFile(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();
        String fileName = params.getString("fileName");
        try {
            PrintWriter writer = new PrintWriter(new File(getLogFolder(params).getAbsoluteFile() + "/" + fileName));
            writer.print("");
            writer.close();
        } catch (IOException e) {
            throw new MlServerException("Ошибка чтения файла " + fileName, e);
        }
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        result.add("status", new JsonPrimitive(true));
        resp.setJsonData(result);
    }

    @PageBlockAction(action = "show")
    private void showTemplate(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();
        result.add("idModal", new JsonPrimitive((Long) mlInstance.get("id")));
        result.add("title", new JsonPrimitive("Просмотреть логи"));
        List<String> fileList = getLogFiles(params);
        result.add("fileList", new JsonPrimitive(gson.toJson(fileList)));
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        resp.setJsonData(result);
    }

    private List<String> getLogFiles(MlHttpServletRequest request) {
        File logFolder = getLogFolder(request);
        return Arrays.asList(logFolder.list());
    }

    private LogFileLines getLogFile(String fileName, File logFolder, Long position) throws IOException {
        LogFileLines logFileLines = new LogFileLines();
        RandomAccessFile randomAccessFile = new RandomAccessFile(logFolder.getAbsoluteFile() + "/" + fileName, "r");
        List<String> lines = new ArrayList<>();
        long pos = position > 0 ? position : randomAccessFile.length();
        pos = pos -2;
        if (randomAccessFile.length() != 0) {
            randomAccessFile.seek(pos);
            for (int i = 0; i < 300; i++) {
                for (; pos > 0; pos--) {
                    randomAccessFile.seek(pos);
                    if (((char) randomAccessFile.read()) == '\n') {
                        position = randomAccessFile.getChannel().position();
                        String line = new String(randomAccessFile.readLine().getBytes("ISO-8859-1"), "UTF-8");
                        lines.add(line);
                        pos--;
                        break;
                    }
                }
                if (pos == 0) {
                    break;
                }
            }
        }else {
           lines.add("<Файл пуст>");
        }
        Collections.reverse(lines);
        logFileLines.setLines(lines);
        logFileLines.setPosition(position);
        return logFileLines;
    }

    private File getLogFolder(MlHttpServletRequest request) {
        String logFolderPath = request.getRequest().getServletContext().getRealPath("/") + "/../../logs";
        File logFolder = new File(logFolderPath);
        if (!logFolder.exists() || !logFolder.isDirectory()) {
            throw new MlServerException(String.format("Bad path to log folder! path:%s", logFolder));
        }
        return logFolder;
    }

    public class LogFileLines {
        private List<String> lines = new ArrayList<>();
        private Long position;

        public List<String> getLines() {
            return lines;
        }

        public void setLines(List<String> lines) {
            this.lines = lines;
        }

        public Long getPosition() {
            return position;
        }

        public void setPosition(Long position) {
            this.position = position;
        }
    }

}
