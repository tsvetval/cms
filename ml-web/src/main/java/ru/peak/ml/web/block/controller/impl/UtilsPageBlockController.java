package ru.peak.ml.web.block.controller.impl;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import org.eclipse.persistence.dynamic.DynamicEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.model.util.MlUtil;
import ru.peak.ml.template.TemplateEngine;
import ru.peak.ml.web.block.controller.BlockController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.gson.MlGsonBuilder;
import ru.peak.ml.web.service.annotations.PageBlockAction;
import ru.peak.ml.web.utils.controller.UtilController;
import ru.peak.security.services.AccessServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 */
public class UtilsPageBlockController implements BlockController {
    private static final Logger log = LoggerFactory.getLogger(UtilsPageBlockController.class);
    @Inject
    AccessServiceImpl accessService;
    @Inject
    CommonDao commonDao;
    @Inject
    MetaDataHolder metaDataHolder;
    private static final String BLOCK_TEMPLATE = "blocks/utils/init.hml";


    @PageBlockAction(action = "show",dataType = MlHttpServletResponse.DataType.JSON)
    public void showUtilTemplate(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance){
        TemplateEngine templateEngine = GuiceConfigSingleton.inject(TemplateEngine.class);
        HashMap<String, Object> data = new HashMap<>();
        data.put("widgetMlInstance", mlInstance);
        String html = templateEngine.renderTemplate(BLOCK_TEMPLATE, data);
        JsonObject dataJson = new JsonObject();
        dataJson.add("html", new JsonPrimitive(html));
        resp.setJsonData(dataJson);
    }

    @PageBlockAction(action = "getFolderUtils",dataType = MlHttpServletResponse.DataType.JSON)
    public void getFolderUtils(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance){
        if (!params.hasString("folderId")) {
            resp.addDataToJson("EmptyUtilList", new JsonPrimitive(true));
            return;
        }
        Long folderId = params.getLong("folderId");
        List<MlUtil> utils = commonDao.getResultList("select o from MlUtil o left join o.folders f where " +
                "f.id = " + folderId + " order by o.orderNumber", "MlUtil");
        putUtilsToResponse(resp,utils);
    }

    @PageBlockAction(action = "getObjectUtils",dataType = MlHttpServletResponse.DataType.JSON)
    public void getObjectUtils(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance){
        if (!params.hasString("className")) {
            return;
        }
        MlClass linkedClass = metaDataHolder.getMlClassByName(params.getString("className"));

        List<MlUtil> utils = commonDao.getResultList("select o from MlUtil o left join o.mlClasses c " +
                " where o.showInAllObjects = true  or c.id = " + linkedClass.getId() + " order by o.orderNumber", "MlUtil");

        List<DynamicEntity> currentObjectList = new ArrayList<>();

        Long objectId = null;
        if (params.hasLong("objectId")) {
            objectId = params.getLong("objectId");
        }

        if (objectId != null) {
            DynamicEntity currentObject = commonDao.findById(objectId, linkedClass.getEntityName());
            currentObjectList.add(currentObject);
        }
        List<MlUtil> resultUtils = new ArrayList<>();
        for (MlUtil util : utils) {
            // Провеярем должна ли отображаться утилита для текущего объекта
            if (util.getControllerJavaClass() != null) {
                try {
                    Class<?> aClass = Class.forName(util.getControllerJavaClass());
                    UtilController controller = (UtilController) GuiceConfigSingleton.inject(aClass);
                    if (controller.isShownForObjectList(currentObjectList, util)) {
                        resultUtils.add(util);
                    }
                } catch (Exception e) {
                    log.error("Check show util error for util with id = " + util.getOUID(), e);
                }
            }else {
                resultUtils.add(util);
            }
        }

        putUtilsToResponse(resp, resultUtils);
    }

    private void putUtilsToResponse(MlHttpServletResponse resp, List<MlUtil> utils) {
        utils = accessService.checkAccessUtil(utils);
        JsonArray result = (JsonArray) MlGsonBuilder.getDynamicEntityNameValueSerializer().toJsonTree(utils);
        /*for (MlUtil util : utils) {
            JsonObject utilJson = new JsonObject();
*//*            MlGsonBuilder.getViewFormSerializer()*//*

                                utilJson.add("id", new JsonPrimitive(util.getId()));
            utilJson.add("buttonLabel", new JsonPrimitive(util.getButtonLabel()));
            if (util.getButtonImgUrl() != null)
                utilJson.add("buttonImgUrl", new JsonPrimitive(util.getButtonImgUrl()));
            if (util.getButtonIcon() != null) utilJson.add("buttonIcon", new JsonPrimitive(util.getButtonIcon()));
            utilJson.add("url", new JsonPrimitive(util.getUrl()));
            utilJson.add("method", new JsonPrimitive(util.getMethod()));
            if (util.getMethod().equalsIgnoreCase("get")) {
                utilJson.add("openType", new JsonPrimitive(util.getOpenType()));
            }
            utilJson.add("orderNumber", new JsonPrimitive(util.getOrderNumber()));
            utilJson.add("confirmExec", new JsonPrimitive(util.getConfirmExec()));
            utilJson.add("confirmExecMsg", new JsonPrimitive(util.getConfirmExecMsg()));
            utilJson.add("utilBootJs",new JsonPrimitive(util.getUtilBootJs()));
            result.add(utilJson);
        }*/
        resp.setJsonData(result);
    }

    @Override
    public void serve(MlHttpServletRequest params, MlPageBlockBase mlInstance, MlHttpServletResponse resp) throws MlApplicationException, MlServerException {
        log.error(String.format("Unknown action: [%s] in class [%s] ", params.getString("action"), this.getClass()));
        throw new MlApplicationException(String.format("В контроллере %s Данный функционал (%s) пока не реализован", this.getClass(), params.getString("action")));
    }
}
