package ru.peak.ml.web.block.controller.impl.form;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import org.eclipse.persistence.dynamic.DynamicEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlAttrGroup;
import ru.peak.ml.core.model.system.MlAttrView;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.web.block.controller.BlockController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.gson.MlGsonBuilder;
import ru.peak.ml.web.gson.adapter.serializers.MlDynamicEntitySerializerImpl;
import ru.peak.ml.web.helper.IconHelper;
import ru.peak.ml.web.service.annotations.PageBlockAction;

import java.util.List;

/**
 * Базовый контроллер блока формы (представление объекта в форме)
 */
public abstract class FormBaseController implements BlockController {
    private static final Logger log = LoggerFactory.getLogger(FormBaseController.class);
    @Inject
    CommonDao commonDao;
    @Inject
    MetaDataHolder metaDataHolder;

    public void serve(MlHttpServletRequest params, MlPageBlockBase mlInstance, MlHttpServletResponse resp) throws MlApplicationException, MlServerException {
        log.error(String.format("Unknown action: [%s] in class [%s] ", params.getString("action"), this.getClass()));
        throw new MlApplicationException(String.format("В контроллере %s Данный функционал (%s) пока не реализован", this.getClass(), params.getString("action")));
    }

    /**
     * Метод для получения бинарных данных атрибута типа FILE
     * TODO при рефакторинге необходимо вынести в отельный сервис
     *
     * @param params     параметры запроса, должны содержать
     *                   objectId (long)    -   id объекта-владельца атрибута
     *                   className (string) -   имя класса объекта-владельца атрибута
     *                   attrName (string)  -   имя (entityFieldName) аттрибута типа FILE
     * @param resp
     * @param mlInstance
     */
    @PageBlockAction(action = "downloadFile")
    public void downloadFile(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        Long objectId = params.getLong("objectId");
        String attrName = params.getString("attrName");
        String className = params.getString("className");

        DynamicEntity file = commonDao.findById(objectId, className);
        resp.setDownloadData((byte[]) file.get(attrName));
        resp.setDownloadFileName((String) file.get(attrName + "_filename"));

    }

    /**
     * Метод для получения метаданных класса объекта
     *
     * @param params     параметры запроса, должны содержать
     *                   className (string)    -   имя класса объекта
     *                   refAttrId (long)      -   идентификатор ссылочного атрибута
     * @param resp
     * @param mlInstance
     */
    @PageBlockAction(action = "getMetaData")
    public void getMetaData(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        // Check Params
        MlClass mlClass;
        if (params.hasString("className")) {
            mlClass = metaDataHolder.getMlClassByName(params.getString("className"));
        } else if (params.hasString("refAttrId")) {
            // В случае если создается объект из другого
            MlAttr mlAttr = metaDataHolder.getAttrById(params.getLong("refAttrId"));
            if (mlAttr == null) {
                throw new MlApplicationException("В базе отсутствукт атрибут с идентификатором " + params.getLong("refAttrId"));
            }
            mlClass = mlAttr.getAttrLinkClass();
        } else {
            throw new MlApplicationException("Некорректные параметры запроса, в запросе должны присутствовать [className] [refAttrId]");
        }
        if (mlClass == null) {
            throw new MlApplicationException("Не удалось получить mlClass по данным запроса");
        }

        // Серифлизуем мета данные по классу
        JsonElement metaDataJson = MlGsonBuilder.getViewFormSerializer().toJsonTree(mlClass);
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        resp.addDataToJson("MetaData", metaDataJson);
    }

    /**
     * Метод для получения URL-иконки по имени файла иконки (для отображения иконок папок в списке)
     * TODO при рефакторинге необходимо вынести в отельный сервис
     *
     * @param params     параметры запроса, должны содержать
     *                   filename (string)  -   имя файла иконки (берется из атрибута iconURL объекта MlFolder)
     * @param resp
     * @param mlInstance
     */
    @PageBlockAction(action = "getIconURL")
    public void getIconURL(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        String filename = params.getString("filename", null);
        if (filename != null) {
            String iconURL = IconHelper.getIconURL(filename);
            resp.setDataType(MlHttpServletResponse.DataType.JSON);
            resp.addDataToJson("iconURL", iconURL == null ? JsonNull.INSTANCE : new JsonPrimitive(iconURL));
        }
    }

    /**
     * Получение списка иконок
     *
     * @param params     параметры запроса, могут содержать
     *                   "currentIcon"      -   имя текущей иконки
     * @param resp
     * @param mlInstance
     */
    @PageBlockAction(action = "getIconList")
    public void getIconList(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        List<String> iconList = IconHelper.getIconList();
        String iconFolder = IconHelper.getIconFolder();
        if (params.hasString("currentIcon")) {
            int iconIndex = IconHelper.getIconIndex(params.getString("currentIcon"));
            resp.addDataToJson("iconIndex", new JsonPrimitive(iconIndex));
        }
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        resp.addDataToJson("iconList", new JsonPrimitive(new Gson().toJson(iconList)));
        resp.addDataToJson("iconFolder", iconFolder == null ? JsonNull.INSTANCE : new JsonPrimitive(iconFolder));
    }

    /**
     * Получить список возможных представлений (AttrView) для указанного типа атрибута
     *
     * @param params     параметры запроса, должны содержать
     *                   attrType (string)  -   тип атрибута
     * @param resp
     * @param mlInstance
     */
    @PageBlockAction(action = "getAttrViewListForType")
    public void getAttrViewListForType(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        if (params.hasString("attrType")) {
            String jpql = String.format("select o from MlAttrView o where o.attrType = '%s'", params.getString("attrType"));
            List resultList = commonDao.getResultList(jpql, MlAttrView.class);
            MlDynamicEntitySerializerImpl serializer = GuiceConfigSingleton.inject(MlDynamicEntitySerializerImpl.class);
            //TODO think about this
            List<MlAttr> attrViewAttrList = metaDataHolder.getMlClassByName("MlAttrView").getAttrSet();
            JsonElement attrViewListDataJson = serializer.serializeObjectList(resultList, attrViewAttrList, true);
            resp.setDataType(MlHttpServletResponse.DataType.JSON);
            resp.addDataToJson("AttrViewListData", attrViewListDataJson);
        }
    }

    /**
     * Получить спиок групп атрибутов класса
     *
     * @param params     параметры запроса, должны содержать
     *                   mlClass (long)  -   идентификатор класса
     * @param resp
     * @param mlInstance
     */
    @PageBlockAction(action = "getAttrGroupListForMlClass")
    public void getAttrGroupListForMlClass(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        if (params.hasLong("mlClass")) {
            String jpql = String.format("select o from MlAttrGroup o where o.linkedClass.id = '%s'", params.getLong("mlClass"));
            List resultList = commonDao.getResultList(jpql, MlAttrGroup.class);
            MlDynamicEntitySerializerImpl serializer = GuiceConfigSingleton.inject(MlDynamicEntitySerializerImpl.class);
            //TODO
            List<MlAttr> attrGroupAttrList = metaDataHolder.getMlClassByName("MlAttrGroup").getAttrSet();
            JsonElement attrGroupListDataJson = serializer.serializeObjectList(resultList, attrGroupAttrList, true);
            resp.setDataType(MlHttpServletResponse.DataType.JSON);
            resp.addDataToJson("AttrGroupListData", attrGroupListDataJson);
        }
    }

    /**
     * Получить список объектов класса
     *
     * @param params     параметры запроса, должны содержать
     *                   mlClass     -   имя класса (entityName)
     * @param resp
     * @param mlInstance
     */
    @PageBlockAction(action = "getMlClassInstanceList")
    public void getMlClassInstanceList(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        if (params.hasString("mlClass")) {
            String className = params.getString("mlClass");
            MlClass mlClass = metaDataHolder.getMlClassByName(className);
            String jpql = String.format("select o from %s o", className);
            List resultList = commonDao.getResultList(jpql, className);
            MlDynamicEntitySerializerImpl serializer = GuiceConfigSingleton.inject(MlDynamicEntitySerializerImpl.class);
            //TODO
            List<MlAttr> attrViewAttrList = mlClass.getAttrSet();
            JsonElement mlClassInstanceListDataJson = serializer.serializeObjectList(resultList, attrViewAttrList, true);
            resp.setDataType(MlHttpServletResponse.DataType.JSON);
            resp.addDataToJson("MlClassInstanceListData", mlClassInstanceListDataJson);
        }
    }

    /**
     * Сериализация объекта
     *
     * @param resp
     * @param object -   объект для сериализации
     */
    protected void serializeObjectDataToResponse(MlHttpServletResponse resp, MlDynamicEntityImpl object) {
        JsonElement objectDataJson = MlGsonBuilder.getFormViewObjectSerializer()
                .toJsonTree(object);
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        resp.addDataToJson("ObjectData", objectDataJson);

    }

}
