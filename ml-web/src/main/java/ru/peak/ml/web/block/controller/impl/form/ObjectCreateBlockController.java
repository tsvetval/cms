package ru.peak.ml.web.block.controller.impl.form;

import com.google.gson.Gson;
import com.google.gson.JsonPrimitive;
import com.google.gson.reflect.TypeToken;
import org.eclipse.persistence.dynamic.DynamicEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.AttrType;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.helper.ObjectHelper;
import ru.peak.ml.web.service.annotations.PageBlockAction;

import java.util.Map;

/**
 * Контроллер блока создания объектов
 */
public class ObjectCreateBlockController extends ObjectEditBlockController {
    private static final Logger log = LoggerFactory.getLogger(ObjectCreateBlockController.class);

    /**
     * Получить данные объекта (список значений атрибутов для отображения в форме)
     *
     * @param params     -   параметры запроса, должны содержать
     *                   className   (string)    -   имя класса (entityName) или
     *                   refAttrId   (long)      -   идентификатор ссылочного атрибута
     *                   могут содержать
     *                   refObjectId (long)      -   идентификатор объекта-владельца связи
     * @param resp
     * @param mlInstance
     */
    @PageBlockAction(action = "getObjectData")
    public void getObjectData(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        //check params
        String className;
        Long refObjectId = null;
        MlAttr refAttr = null;
        if (params.hasString("className")) {
            className = params.getString("className");
        } else if (params.hasLong("refAttrId")) {
            // Создание идет из атрибута другого объекта
            refAttr = metaDataHolder.getAttrById(params.getLong("refAttrId"));
            if (refAttr == null) {
                throw new MlApplicationException("В базе отсутствукт атрибут с идентификатором " + params.getLong("refAttrId"));
            }
            className = refAttr.getAttrLinkClass().getEntityName();
        } else {
            throw new MlApplicationException("Некорректные параметры запроса, в запросе должны присутствовать [className] [refAttrId]");
        }

        // если есть идентификатор объекта из которого создаем другой
        if (params.hasLong("refObjectId")) {
            refObjectId = params.getLong("refObjectId");
        }


        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        MlDynamicEntityImpl object = commonDao.createNewEntity(metaDataHolder.getEntityClassByName(className));
        if (refObjectId != null && refAttr != null) {
            // Если создание идет из другого объекта то проставляем создаваямому объекту значение ссылочного атрибута
            if (refAttr.getFieldType().equals(AttrType.ONE_TO_MANY)) {
                // Если атрибут из которого идет создания ONE_TO_MANY о проставляем значение в создаваемомо объекте
                DynamicEntity refObject = commonDao.findById(refObjectId, refAttr.getMlClass().getEntityName());
                object.set(refAttr.getLinkAttr().getEntityFieldName(), refObject);
            }
        }
        serializeObjectDataToResponse(resp, object);
    }

    /**
     * Сохранение (создание) объекта
     *
     * @param params     -   параметры запроса, должны содержать
     *                   className   (string)    -   имя класса (entityName) или
     *                   refAttrId   (long)      -   идентификатор ссылочного атрибута
     *                   data        (string)    -   сереализованные данные о полях объекта (мапа имя атрибута - значение)
     * @param resp
     * @param mlInstance
     */
    @PageBlockAction(action = "saveObject")
    public void saveObject(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        MlUser user = (MlUser) params.getRequest().getSession().getAttribute("user");
        // Check params
        String className;
        MlAttr refAttr;
        if (params.hasString("className")) {
            className = params.getString("className");
        } else if (params.hasLong("refAttrId")) {
            refAttr = metaDataHolder.getAttrById(params.getLong("refAttrId"));
            if (refAttr == null) {
                throw new MlApplicationException("В базе отсутствукт атрибут с идентификатором " + params.getLong("refAttrId"));
            }
            className = refAttr.getAttrLinkClass().getEntityName();
        } else {
            throw new MlApplicationException("Некорректные параметры запроса, в запросе должны присутствовать [className] [refAttrId]");
        }

        // Десериализуем параметры создаваемого объекта
        Gson gson = new Gson();
        Map<String, Object> objectMap = gson.fromJson(params.getString("data"),
                new TypeToken<Map<String, Object>>() {
                }.getType());

        // создаем новый объект
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        MlDynamicEntityImpl newObject = commonDao.createNewEntity(metaDataHolder.getEntityClassByName(className));
        newObject = ObjectHelper.updateObjectByMap(newObject, objectMap, user);
        commonDao.persistTransactionalWithSecurityCheck(newObject);
        //хендлеры могли изменить состояние обьекта после его сохранения. Надо почистить кеш
        commonDao.evictL2Cache(newObject);
        // отдаем результат идентификатор и заголовок нового объекта
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        resp.addDataToJson("objectId", new JsonPrimitive(newObject.getOUID()));
        resp.addDataToJson("title", new JsonPrimitive(newObject.getTitle()));
    }


}
