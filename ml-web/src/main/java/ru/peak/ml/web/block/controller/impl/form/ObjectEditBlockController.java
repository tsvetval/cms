package ru.peak.ml.web.block.controller.impl.form;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.eclipse.persistence.dynamic.DynamicEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.AttrType;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.metadata.api.MetaDataHelper;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.gson.adapter.serializers.MlDynamicEntitySerializerImpl;
import ru.peak.ml.web.helper.ObjectHelper;
import ru.peak.ml.web.service.annotations.PageBlockAction;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Контроллер блока редактирования объекта
 */
public class ObjectEditBlockController extends ObjectViewBlockController {
    private static final Logger log = LoggerFactory.getLogger(ObjectEditBlockController.class);

    /**
     * Сохранение объекта
     *
     * @param params     -   параметры запроса, должны содержать
     *                   objectId    (long)      -   идентификатор объекта
     *                   className   (string)    -   имя класса (entityName) или
     *                   refAttrId   (long)      -   идентификатор ссылочного атрибута
     *                   data        (string)    -   сереализованные данные о полях объекта (мапа имя атрибута - значение)
     * @param resp
     * @param mlInstance
     */
    @PageBlockAction(action = "saveObject")
    public void saveObject(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        MlUser user = (MlUser) params.getRequest().getSession().getAttribute("user");
        String className;
        Long objectId;

        if (params.hasLong("objectId")) {
            objectId = params.getLong("objectId");
        } else {
            throw new MlApplicationException("Некорректные параметры запроса, в запросе должен присутствовать [objectId:Long]");
        }

        if (params.hasString("className")) {
            className = params.getString("className");
        } else if (params.hasString("refAttrId")) {
            MlAttr mlAttr = MetaDataHelper.getMlAttrById(params.getLong("refAttrId"));
            if (mlAttr == null) {
                throw new MlApplicationException("В базе отсутствукт атрибут с идентификатором " + params.getLong("refAttrId"));
            }
            className = mlAttr.getAttrLinkClass().getEntityName();
        } else {
            throw new MlApplicationException("Некорректные параметры запроса, в запросе должны присутствовать [className:String] или [refAttrId:Long]");
        }

        Gson gson = new Gson();
        Map<String, Object> objectMap = gson.fromJson(params.getString("data"),
                new TypeToken<Map<String, Object>>() {
                }.getType());

        if (!updateObject(objectId, className, objectMap, user)) {
            throw new MlApplicationException("Ошибка при сохранении объекта");
        }
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        resp.addDataToJson("result", new JsonPrimitive("ok"));
        //log.debug("Object saved");
    }

    /**
     * Удаление объекта
     *
     * @param params     -   параметры запроса, должны содержать
     *                   objectId    (long)      -   идентификатор объекта
     *                   className   (string)    -   имя класса (entityName)
     * @param resp
     * @param mlInstance
     */
    @PageBlockAction(action = "deleteObject")
    public void deleteObject(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        //check params
        String className = params.getString("className");
        Long objectId = params.getLong("objectId");
        if (params.hasLong("objectId") && params.hasString("className")) {
            className = params.getString("className");
            objectId = params.getLong("objectId");
        } else {
            throw new MlApplicationException("Некорректные параметры запроса, в запросе должны присутствовать [objectId,className]");
        }

        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        MlDynamicEntityImpl object = commonDao.findById(objectId, MetaDataHelper.getEntityClassByName(className));
        if (object == null) {
            throw new MlApplicationException(String.format("Невозможно удалить объект (Данный объект отсутствует в базе) objectId=[%s] className=[%s]", objectId, className));
        }

        commonDao.removeTransactional(object);

        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        resp.addDataToJson("result", new JsonPrimitive("ok"));

    }

    /**
     * Получить значение ссылочного атрибута
     *
     * @param params     -   параметры запроса, должны содержать
     *                   className   (string)    -   имя класса (entityName)
     *                   attrId      (long)      -   идентификатор ссылочного атрибута
     *                   idList      (string)    -   список идентификаторов связанных объектов
     * @param resp
     * @param mlInstance
     */
    @PageBlockAction(action = "getRefAttrValues")
    //TODO refactor this (переписать нормально)
    public void getRefAttrValues(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        JsonElement result = null;
        if (params.hasString("className") && params.hasString("idList") && params.hasLong("attrId") && !params.getString("idList").equals("[]")) {
            CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
            Long attrId = params.getLong("attrId");
            MlAttr attr = MetaDataHelper.getMlAttrById(attrId);
            String idListStr = params.getString("idList");
            Gson gson = new Gson();
            if (attr.getFieldType() == AttrType.HETERO_LINK) {
                Type type = new TypeToken<List<String>>() {}.getType();
                List<String> ids = gson.fromJson(idListStr,type);
                result = new JsonObject();
                JsonArray jsonElements = new JsonArray();
                for(String id : ids){
                    String className = id.split("@")[1];
                    String objectId = id.split("@")[0];
                    JsonObject jsonObject = new JsonObject();
                    MlDynamicEntityImpl entity = commonDao.findById(Long.parseLong(objectId), className);
                    jsonObject.add("title",new JsonPrimitive(entity.getTitle()));
                    jsonObject.add("objectId",new JsonPrimitive(objectId+"@"+className));
                    jsonObject.add("mlClass",new JsonPrimitive(className));
                    jsonElements.add(jsonObject);
                }
                ((JsonObject)result).add("objectList",jsonElements);
            } else {
                Type type = new TypeToken<List<Long>>() {
                }.getType();
                List<Long> idList = gson.fromJson(idListStr, type);
                if (attr.getFieldType() == AttrType.MANY_TO_ONE && !idList.isEmpty()) {
                    String className = attr.getLinkClass().getEntityName();
                    DynamicEntity instance = commonDao.findById(idList.get(0), className);
                    result = new JsonObject();
                    ((JsonObject) result).addProperty("title", ((MlDynamicEntityImpl) instance).getTitle());
                    ((JsonObject) result).addProperty("id", idList.get(0));
                } else if (attr.getFieldType() == AttrType.MANY_TO_MANY || attr.getFieldType() == AttrType.ONE_TO_MANY) {
                    //TODO
                    MlDynamicEntitySerializerImpl serializer = GuiceConfigSingleton.inject(MlDynamicEntitySerializerImpl.class);
                    String inArray = idList.toString().replace("[", "(").replace("]", ")");
                    MlClass linkClass;
                    if (attr.getFieldType().equals(AttrType.MANY_TO_ONE) || attr.getFieldType().equals(AttrType.MANY_TO_MANY)) {
                        linkClass = attr.getLinkClass();
                    } else {
                        linkClass = attr.getLinkAttr().getMlClass();
                    }
                    String jpql = String.format("select o from %s o where o.id in %s", linkClass.getEntityName(), inArray);
                    List<MlDynamicEntityImpl> selectedObjectList = commonDao.getResultList(jpql, linkClass.getEntityName());
                    selectedObjectList = sortEntityListByIdList(selectedObjectList, idList);
                    result = serializer.serializeObjectList(selectedObjectList, linkClass.getInListAttrList(), true);
                }
            }
        }
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        resp.addDataToJson("result", result);

    }

    /**
     * Сортировка списка объектов по идетификаторам
     *
     * @param objectList -   список объектов
     * @param idList     -   упорядоченный список идентификаторов
     * @return -   отсортированный список объектов
     */
    private List<MlDynamicEntityImpl> sortEntityListByIdList(List<MlDynamicEntityImpl> objectList, List<Long> idList) {
        List<MlDynamicEntityImpl> result = new ArrayList<>();
        for (Long id : idList) {
            for (MlDynamicEntityImpl obj : objectList) {
                if (obj.getOUID().equals(id.toString())) {
                    result.add(obj);
                }
            }
        }
        return result;
    }

    /**
     * обновление значений атрибутов объекта
     *
     * @param objectId  -   идентификатор объекта для обновления
     * @param className -   класс объекта
     * @param objectMap -   мапа атрибутов и их значений
     * @param user      -   пользователь CMS
     * @return -   результат выполнения обновления данных
     */
    protected boolean updateObject(long objectId, String className, Map<String, Object> objectMap, MlUser user) {
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        String jpql = String.format("select o from %s o where o.id = %d", className, objectId);
        List<MlDynamicEntityImpl> instanceList = (List<MlDynamicEntityImpl>) commonDao.getResultList(jpql, className);
        if (instanceList != null && !instanceList.isEmpty()) {
            MlDynamicEntityImpl object = instanceList.get(0);
            object = ObjectHelper.updateObjectByMap(object, objectMap, user);
            commonDao.mergeTransactional(object);
            commonDao.evictL2Cache(object);
            return true;
        } else {
            return false;
        }
    }
}
