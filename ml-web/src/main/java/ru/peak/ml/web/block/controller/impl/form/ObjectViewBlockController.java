package ru.peak.ml.web.block.controller.impl.form;

import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlSecurityException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.security.MlClassAccessType;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.service.annotations.PageBlockAction;
import ru.peak.security.services.AccessServiceImpl;

/**
 * Контроллер блока просмотра объекта в форме
 */
public class ObjectViewBlockController extends FormBaseController {
    private static final Logger log = LoggerFactory.getLogger(ObjectViewBlockController.class);

    @Inject
    AccessServiceImpl accessService;
    @Inject
    MetaDataHolder metaDataHolder;
    /**
     * Получение данных объекта (списка атрибутов и их значений для отображения в форме)
     *
     * @param params     -   параметры запроса, должны содержать
     *                   objectId    (long)      -   идентификатор объекта
     *                   className   (string)    -   имя класса (entityName) или
     *                   refAttrId   (long)      -   идентификатор ссылочного атрибута
     * @param resp
     * @param mlInstance
     */
    @PageBlockAction(action = "getObjectData")
    public void getObjectData(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        // check params
        Long objectId;
        String className;

        if (params.hasString("className")) {
            className = params.getString("className");
        } else if (params.hasString("refAttrId")) {
            MlAttr mlAttr = metaDataHolder.getAttrById(params.getLong("refAttrId"));
            if (mlAttr == null) {
                throw new MlApplicationException("В базе отсутствукт атрибут с идентификатором " + params.getLong("refAttrId"));
            }
            className = mlAttr.getAttrLinkClass().getEntityName();
        } else {
            throw new MlApplicationException("Некорректные параметры запроса, в запросе должны присутствовать [className:String] или [refAttrId:Long]");
        }

        if (params.hasLong("objectId")) {
            objectId = params.getLong("objectId");
        } else if (params.hasString("objectId")) {
            String objectMLUID = params.getString("objectId");
            String[] data = objectMLUID.split("@");
            if (data[0] != null && !data[0].isEmpty() && data[1] != null && !data[1].isEmpty()) {
                objectId = Long.parseLong(data[0]);
                className = data[1];
            } else {
                throw new MlApplicationException("Некорректные параметры запроса, в запросе должен присутствовать [objectId:Long]");
            }
        } else {
            throw new MlApplicationException("Некорректные параметры запроса, в запросе должен присутствовать [objectId:Long]");
        }

        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        MlClass mlClass = metaDataHolder.getMlClassByName(className);
        if(!accessService.checkAccessClass(mlClass, MlClassAccessType.READ)){
            String message = String.format("Отсутствуют права доступа на %s объектов класса \"%s\" [%s].", MlClassAccessType.READ.getOperationName(), mlClass.getTitle(), mlClass.getEntityName());
            throw new MlSecurityException(message);
        }

        MlDynamicEntityImpl object = commonDao.findById(objectId, className);
        if (object == null) {
            throw new MlApplicationException(String.format("В базе отсутствует объект класса [%s] с идентификатором [%s]"
                    , className, objectId));
        }
        serializeObjectDataToResponse(resp, object);
        //log.debug("Object show");
        addSecurityInfo(mlClass,resp);
    }

    private void addSecurityInfo(MlClass mlClass, MlHttpServletResponse resp) {
        resp.addDataToJson("canCreate", new JsonPrimitive(accessService.checkAccessClass(mlClass, MlClassAccessType.CREATE)));
        resp.addDataToJson("canDelete", new JsonPrimitive(accessService.checkAccessClass(mlClass, MlClassAccessType.DELETE)));
        resp.addDataToJson("canEdit", new JsonPrimitive(accessService.checkAccessClass(mlClass, MlClassAccessType.UPDATE)));
    }
}
