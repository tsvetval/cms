package ru.peak.ml.web.block.controller.impl.list;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.filter.result.FilterResult;
import ru.peak.ml.core.filter.FilterBuilder;
import ru.peak.ml.core.metadata.api.MetaDataHelper;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.folder.MlFolder;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.security.MlAttrAccessType;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.gson.adapter.serializers.MlDynamicEntitySerializerImpl;
import ru.peak.ml.web.service.annotations.PageBlockAction;

import java.util.*;

/**
 *  Контроллер для получения списка объектов связанного класса
 *
 */
public class HeteroObjectListSelectBlockController extends ObjectListBlockController {

    private static final Logger log = LoggerFactory.getLogger(HeteroObjectListSelectBlockController.class);

    @PageBlockAction(action = "getObjectData",dataType = MlHttpServletResponse.DataType.JSON)
    public void getObjectListData(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase currentPageBlock) {
        // check params
        Long folderId = params.getLong("folderId", null);
        String className = params.getString("className", null);

        if (folderId != null) {
            //просмотр папки

            MlFolder mlFolder = (MlFolder) commonDao.findById(folderId, MlFolder.class);
            MlClass mlClass = mlFolder.getChildClass();

            if (accessService.checkAccessFolder(Arrays.asList(mlFolder)).isEmpty()) {
                responseWithRootFolders(resp);
                //throw new MlServerException(String.format("Отказано в доступе к каталогу [%s]", mlFolder.getTitle()));
            }

            List<MlDynamicEntityImpl> objectList = null;
            List<MlAttr> objectAttrList = null;
            MlDynamicEntitySerializerImpl serializer = GuiceConfigSingleton.inject(MlDynamicEntitySerializerImpl.class);

            if (mlClass != null) {

                FilterBuilder filterBuilder = GuiceConfigSingleton.inject(FilterBuilder.class);
                filterBuilder.setQueryMlClass(mlFolder.getChildClass());
                fillServiceFromRequest(filterBuilder,params, mlClass);
                filterBuilder.addAdditionalCondition(mlFolder.getChildClassCondition());

                FilterResult filterResult = filterBuilder.buildFilterResult();
                objectList = filterResult.getResultList();
                resp.addDataToJson("RecordsCount", new JsonPrimitive(filterResult.getRecordsCount()));
                resp.addDataToJson("PagesCount", new JsonPrimitive(filterResult.getPagesCount()));
                List<MlAttr> attrListToShow = mlFolder.getAttrListToShow();
                if (attrListToShow == null || attrListToShow.isEmpty()) {
                    objectAttrList = mlClass.getInListAttrList();
                } else {
                    objectAttrList = new ArrayList<>();
                    for (MlAttr attr : attrListToShow) {
                        objectAttrList.add(attr);
                    }
                }

                objectAttrList = accessService.checkAccessAttrs(objectAttrList, MlAttrAccessType.SHOW);

                resp.addDataToJson("ObjectClassName", new JsonPrimitive(mlClass.getEntityName()));
                JsonElement objectListDataJson = serializer.serializeObjectList(objectList, objectAttrList, true);
                resp.addDataToJson("ObjectListData", objectListDataJson);

            }

            List<? extends MlDynamicEntityImpl> folderList = null;
            List<MlAttr> folderAttrList = MetaDataHelper.getMlClassAttributes(MetaDataHelper.getMlClassByName("MlFolder"));
            if (mlFolder.getChildFolders() != null && !mlFolder.getChildFolders().isEmpty()) {
                folderList = accessService.checkAccessFolder(mlFolder.getChildFolders());// getFolderListForFolder(mlFolder);
            }


            JsonObject currentFolderJson = serializer.serializeObject(mlFolder, folderAttrList, true);
            resp.addDataToJson("CurrentFolder", currentFolderJson);

            if (folderList != null && !folderList.isEmpty()) {
                JsonElement folderListDataJson = serializer.serializeObjectList(folderList, folderAttrList, true);
                resp.addDataToJson("FolderListData", folderListDataJson);
            }
        } else if (className != null) {
            //просмотр списка объектов класса
            MlClass mlClass = MetaDataHelper.getMlClassByName(className);

            FilterBuilder filterBuilder = GuiceConfigSingleton.inject(FilterBuilder.class);
            filterBuilder.setQueryMlClass(mlClass);
            fillServiceFromRequest(filterBuilder,params, mlClass);

            FilterResult filterResult = filterBuilder.buildFilterResult();

            List<MlDynamicEntityImpl> objectList = filterResult.getResultList();
            resp.addDataToJson("RecordsCount", new JsonPrimitive(filterResult.getRecordsCount()));
            resp.addDataToJson("PagesCount", new JsonPrimitive(filterResult.getPagesCount()));
            List<MlAttr> objectAttrList = mlClass.getInListAttrList();
            resp.addDataToJson("ObjectClassName", new JsonPrimitive(mlClass.getEntityName()));

            objectAttrList = accessService.checkAccessAttrs(objectAttrList, MlAttrAccessType.SHOW);

            MlDynamicEntitySerializerImpl serializer = GuiceConfigSingleton.inject(MlDynamicEntitySerializerImpl.class);
            JsonElement objectListDataJson = serializer.serializeObjectList(objectList, objectAttrList, true);
            resp.addDataToJson("ObjectListData", objectListDataJson);
        } else {
            //просмотр корневой папки
            responseWithRootFolders(resp);
        }

    }
}
