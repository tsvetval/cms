package ru.peak.ml.web.block.controller.impl.list;

import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import org.eclipse.persistence.dynamic.DynamicEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.web.block.controller.BlockController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.helper.IconHelper;
import ru.peak.ml.web.service.annotations.PageBlockAction;

/**
 *  Базовый контроллер для работы со списком объектов
 *
 */
public abstract class ListBaseController implements BlockController {
    private static final Logger log = LoggerFactory.getLogger(ListBaseController.class);

    @Inject
    CommonDao commonDao;

    public void serve(MlHttpServletRequest params, MlPageBlockBase mlInstance, MlHttpServletResponse resp) throws MlApplicationException, MlServerException {
        log.error(String.format("Unknown action: [%s] in class [%s] ", params.getString("action"), this.getClass()));
        throw new MlApplicationException(String.format("В контроллере %s Данный функционал (%s) пока не реализован", this.getClass(), params.getString("action")));
    }

    /**
     * Метод для получения бинарных данных атрибута типа FILE
     * TODO при рефакторинге необходимо вынести в отельный сервис
     *
     * @param params параметры запроса, должны содержать
     *               objectId (long)    -   id объекта-владельца атрибута
     *               className (string) -   имя класса объекта-владельца атрибута
     *               attrName (string)  -   имя (entityFieldName) аттрибута типа FILE
     *
     * @param resp
     * @param mlInstance
     */
    @PageBlockAction(action = "downloadFile")
    public void downloadFile(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        Long objectId = params.getLong("objectId");
        String attrName = params.getString("attrName");
        String className = params.getString("className");

        DynamicEntity file = commonDao.findById(objectId, className);
        resp.setDownloadData((byte[]) file.get(attrName));
        resp.setDownloadFileName((String) file.get(attrName + "_filename"));
    }

    /**
     * Метод для получения URL-иконки по имени файла иконки (для отображения иконок папок в списке)
     * TODO при рефакторинге необходимо вынести в отельный сервис
     *
     * @param params параметры запроса, должны содержать
     *               filename (string)  -   имя файла иконки (берется из атрибута iconURL объекта MlFolder)
     * @param resp
     * @param mlInstance
     */
    @PageBlockAction(action = "getIconURL")
    public void getIconURL(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        String filename = params.getString("filename", null);
        if (filename != null) {
            String iconURL = IconHelper.getIconURL(filename);
            resp.setDataType(MlHttpServletResponse.DataType.JSON);
            resp.addDataToJson("iconURL", new JsonPrimitive(iconURL));
        }
    }
}
