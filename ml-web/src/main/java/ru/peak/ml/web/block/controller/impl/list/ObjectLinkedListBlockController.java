package ru.peak.ml.web.block.controller.impl.list;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.eclipse.persistence.dynamic.DynamicEntity;
import ru.ml.core.common.AttrType;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.filter.result.FilterResult;
import ru.peak.ml.core.filter.FilterBuilder;
import ru.peak.ml.core.metadata.api.MetaDataHelper;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.security.MlAttrAccessType;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.gson.adapter.serializers.MlDynamicEntitySerializerImpl;
import ru.peak.ml.web.helper.ObjectHelper;
import ru.peak.ml.web.service.annotations.PageBlockAction;

import java.lang.reflect.Type;
import java.util.*;

/**
 * Контроллер для получения списка объектов связанного класса
 */
public class ObjectLinkedListBlockController extends ObjectListBlockController {

    /**
     * Получение списа объектов связанного класса
     *
     * @param params           -   параметры запроса могут содержать
     *                         refAttrId (long)    -   id атрибута связи
     *                         linkFilter (string) -   условия фильтрации связанных объектов
     *                         objectId (long)     -   id объекта-владельца связи
     * @param resp
     * @param currentPageBlock
     */
    @PageBlockAction(action = "getObjectData")
    public void getObjectListData(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase currentPageBlock) {
        resp.setDataType(MlHttpServletResponse.DataType.JSON);

        Long id = Long.parseLong(params.getString("refAttrId", null));
        MlAttr mlAttr = (MlAttr) commonDao.findById(id, "MlAttr");

        MlClass refClass = null;
        if (mlAttr.getFieldType() == AttrType.ONE_TO_MANY) {
            refClass = mlAttr.getLinkAttr().getMlClass();
        } else if (mlAttr.getFieldType() == AttrType.MANY_TO_MANY) {
            refClass = mlAttr.getLinkClass();
        }

        MlClass mlClass = mlAttr.getMlClass();
        String ownerClassName = mlClass.get("entityName");
        Long ownerId = params.getLong("objectId");
        DynamicEntity ownerEntity = commonDao.findById(ownerId, ownerClassName);
        List<MlDynamicEntityImpl> linkedList = ownerEntity.get(mlAttr.getEntityFieldName());
        String linkCondition = null;
        if (linkedList.size() > 0) {
            StringBuilder linkConditionBuilder = new StringBuilder();
            long[] idList = new long[linkedList.size()];
            for (int i = 0; i < idList.length; i++) {
                idList[i] = linkedList.get(i).get("id");
            }
            linkConditionBuilder.append("o.id in ");
            linkConditionBuilder.append(Arrays.toString(idList).replace("[", "(").replace("]", ")"));
            linkCondition = linkConditionBuilder.toString();
        }


        FilterBuilder filterBuilder = GuiceConfigSingleton.inject(FilterBuilder.class);
        filterBuilder.setQueryMlClass(refClass);
        fillServiceFromRequest(filterBuilder,params, refClass);
        filterBuilder.addAdditionalCondition(linkCondition);

        FilterResult filterResult = filterBuilder.buildFilterResult();
        List<MlDynamicEntityImpl> objectList = filterResult.getResultList();
        resp.addDataToJson("RecordsCount", new JsonPrimitive(filterResult.getRecordsCount()));
        resp.addDataToJson("PagesCount", new JsonPrimitive(filterResult.getPagesCount()));


        List<MlAttr> objectAttrList = refClass.getInListAttrList();

        objectAttrList = accessService.checkAccessAttrs(objectAttrList, MlAttrAccessType.SHOW);

        resp.addDataToJson("ObjectClassName", new JsonPrimitive(refClass.getEntityName()));
        resp.addDataToJson("ClassDescription", new JsonPrimitive(refClass.getDescription()));

        if (objectList != null) {
            MlDynamicEntitySerializerImpl serializer = GuiceConfigSingleton.inject(MlDynamicEntitySerializerImpl.class);
            JsonElement objectListDataJson = serializer.serializeObjectList(objectList, objectAttrList, true);
            resp.addDataToJson("ObjectListData", objectListDataJson);
        }

    }

    /**
     * Получить значение ссылочного атрибута
     *
     * @param params     -   параметры запроса, должны содержать
     *                   className   (string)    -   имя класса (entityName)
     *                   attrId      (long)      -   идентификатор ссылочного атрибута
     *                   idList      (string)    -   список идентификаторов связанных объектов
     * @param resp
     * @param mlInstance
     */
    @PageBlockAction(action = "getRefAttrValues")
    public void getRefAttrValues(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        JsonElement result = null;
        if (params.hasString("className") && params.hasString("idList") && params.hasLong("attrId")) {
            CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
            Long attrId = params.getLong("attrId");
            MlAttr attr = (MlAttr) MetaDataHelper.getMlAttrById(attrId);
            String idListStr = params.getString("idList");
            Gson gson = new Gson();
            if (attr.getFieldType() == AttrType.HETERO_LINK) {
                Type type = new TypeToken<List<String>>() {
                }.getType();
                List<String> ids = gson.fromJson(idListStr, type);
                result = new JsonObject();
                JsonArray jsonElements = new JsonArray();
                for (String id : ids) {
                    String className = id.split("@")[1];
                    String objectId = id.split("@")[0];
                    JsonObject jsonObject = new JsonObject();
                    MlDynamicEntityImpl entity = commonDao.findById(Long.parseLong(objectId), className);
                    jsonObject.add("title", new JsonPrimitive(entity.getTitle()));
                    jsonObject.add("objectId", new JsonPrimitive(objectId + "@" + className));
                    jsonObject.add("mlClass", new JsonPrimitive(className));
                    jsonElements.add(jsonObject);
                }
                ((JsonObject) result).add("objectList", jsonElements);
            } else {
                Type type = new TypeToken<List<Long>>() {
                }.getType();
                List<Long> idList = gson.fromJson(idListStr, type);
                if (attr.getFieldType() == AttrType.MANY_TO_ONE && !idList.isEmpty()) {
                    String className = attr.getLinkClass().getEntityName();
                    DynamicEntity instance = commonDao.findById(idList.get(0), className);
                    result = new JsonObject();
                    ((JsonObject) result).addProperty("title", ((MlDynamicEntityImpl) instance).getTitle());
                    ((JsonObject) result).addProperty("id", idList.get(0));
                } else if (attr.getFieldType() == AttrType.MANY_TO_MANY || attr.getFieldType() == AttrType.ONE_TO_MANY) {
                    //TODO
                    MlDynamicEntitySerializerImpl serializer = GuiceConfigSingleton.inject(MlDynamicEntitySerializerImpl.class);
                    String inArray = idList.toString().replace("[", "(").replace("]", ")");
                    MlClass linkClass;
                    if (attr.getFieldType().equals(AttrType.MANY_TO_ONE) || attr.getFieldType().equals(AttrType.MANY_TO_MANY)) {
                        linkClass = attr.getLinkClass();
                    } else {
                        linkClass = attr.getLinkAttr().getMlClass();
                    }
                    String jpql = String.format("select o from %s o where o.id in %s", linkClass.getEntityName(), inArray);
                    List<MlDynamicEntityImpl> selectedObjectList = commonDao.getResultList(jpql, linkClass.getEntityName());
                    selectedObjectList = sortEntityListByIdList(selectedObjectList, idList);

                    List<MlAttr> objectAttrList = linkClass.getInListAttrList();
                    objectAttrList = accessService.checkAccessAttrs(objectAttrList, MlAttrAccessType.SHOW);

                    result = serializer.serializeObjectList(selectedObjectList, objectAttrList, true);
                }
            }
        }
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        resp.addDataToJson("result", result);

    }

    /**
     * Сортировка списка объектов по идетификаторам
     *
     * @param objectList -   список объектов
     * @param idList     -   упорядоченный список идентификаторов
     * @return -   отсортированный список объектов
     */
    private List<MlDynamicEntityImpl> sortEntityListByIdList(List<MlDynamicEntityImpl> objectList, List<Long> idList) {
        List<MlDynamicEntityImpl> result = new ArrayList<>();
        for (Long id : idList) {
            for (MlDynamicEntityImpl obj : objectList) {
                if (obj.getOUID().equals(id.toString())) {
                    result.add(obj);
                }
            }
        }
        return result;
    }

    /**
     * Сохранение объекта
     *
     * @param params     -   параметры запроса, должны содержать
     *                   objectId    (long)      -   идентификатор объекта
     *                   className   (string)    -   имя класса (entityName) или
     *                   refAttrId   (long)      -   идентификатор ссылочного атрибута
     *                   data        (string)    -   сереализованные данные о полях объекта (мапа имя атрибута - значение)
     * @param resp
     * @param mlInstance
     */
    @PageBlockAction(action = "saveObject")
    public void saveObject(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        MlUser user = (MlUser) params.getRequest().getSession().getAttribute("user");
        String className;
        Long objectId;

        if (params.hasLong("objectId")) {
            objectId = params.getLong("objectId");
        } else {
            throw new MlApplicationException("Некорректные параметры запроса, в запросе должен присутствовать [objectId:Long]");
        }

        MlAttr refAttr;
        if (params.hasString("refAttrId")) {
            refAttr = MetaDataHelper.getMlAttrById(params.getLong("refAttrId"));
            if (refAttr == null) {
                throw new MlApplicationException("В базе отсутствукт атрибут с идентификатором " + params.getLong("refAttrId"));
            }
            className = refAttr.getMlClass().getEntityName();
        } else {
            throw new MlApplicationException("Некорректные параметры запроса, в запросе должен присутствовать [refAttrId:Long]");
        }

        String idListStr = params.getString("idList");

        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put(refAttr.getEntityFieldName(), idListStr);

        if (!updateObject(objectId, className, objectMap, user)) {
            throw new MlApplicationException("Ошибка при сохранении объекта");
        }
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        resp.addDataToJson("result", new JsonPrimitive("ok"));
    }

    /**
     * обновление значений атрибутов объекта
     *
     * @param objectId  -   идентификатор объекта для обновления
     * @param className -   класс объекта
     * @param objectMap -   мапа атрибутов и их значений
     * @param user      -   пользователь CMS
     * @return -   результат выполнения обновления данных
     */
    protected boolean updateObject(long objectId, String className, Map<String, Object> objectMap, MlUser user) {
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        String jpql = String.format("select o from %s o where o.id = %d", className, objectId);
        List<MlDynamicEntityImpl> instanceList = (List<MlDynamicEntityImpl>) commonDao.getResultList(jpql, className);
        if (instanceList != null && !instanceList.isEmpty()) {
            MlDynamicEntityImpl object = instanceList.get(0);
            object = ObjectHelper.updateObjectByMap(object, objectMap, user);
            commonDao.mergeTransactional(object);
            return true;
        } else {
            return false;
        }
    }

}
