package ru.peak.ml.web.block.controller.impl.list;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlSecurityException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.filter.result.FilterResult;
import ru.peak.ml.core.filter.FilterBuilder;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.folder.MlFolder;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.security.MlAttrAccessType;
import ru.peak.ml.core.model.security.MlClassAccessType;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.web.navigation.FolderRequest;
import ru.peak.ml.web.navigation.FolderService;
import ru.peak.ml.web.navigation.classifier.ClassifierService;
import ru.peak.ml.web.common.CommonQueryParameterService;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.dao.impl.FolderDao;
import ru.peak.ml.web.gson.adapter.serializers.MlDynamicEntitySerializerImpl;
import ru.peak.ml.web.navigation.model.FolderCondition;
import ru.peak.ml.web.service.annotations.PageBlockAction;
import ru.peak.security.services.AccessServiceImpl;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Контроллер для работы со списком объектов
 */
public class ObjectListBlockController extends ListBaseController {
    private static final Logger log = LoggerFactory.getLogger(ObjectListBlockController.class);

    @Inject
    CommonDao commonDao;


    @Inject
    AccessServiceImpl accessService;

    @Inject
    FolderDao folderDao;

    @Inject
    MetaDataHolder metaDataHolder;
    @Inject
    FolderService folderService;
    @Inject
    ClassifierService classifierService;


    /**
     * Метод для получения списка объектов для заданной папки, корневой папки, списка объектов заданного класса
     *
     * @param params     -   параметры запроса
     *                   если параметры запроса содержат folderId (long) - id папки, то возвращается список объектов,
     *                   содержащихся в папке
     *                   иначе, если параметры запроса содержат className (string) - имя класса, то возвращается список
     *                   объектов данного класса
     *                   иначе возвращается список объектов (папок) корневой папки
     * @param resp
     * @param mlInstance
     */
    @PageBlockAction(action = "getObjectData",dataType = MlHttpServletResponse.DataType.JSON)
    public void getObjectListData(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        // check params
        Long folderId = params.getLong("folderId", null);
        String className = params.getString("className", null);

        if (folderId != null) {
            //просмотр папки
            MlFolder mlFolder = (MlFolder) commonDao.findById(folderId, MlFolder.class);
            MlClass mlClass = mlFolder.getChildClass();


            if (accessService.checkAccessFolder(Arrays.asList(mlFolder)).isEmpty()) {
                responseWithRootFolders(resp);
                //throw new MlServerException(String.format("Отказано в доступе к каталогу [%s]", mlFolder.getTitle()));
            }

            List<MlDynamicEntityImpl> objectList;
            List<MlAttr> objectAttrList;
            MlDynamicEntitySerializerImpl serializer = GuiceConfigSingleton.inject(MlDynamicEntitySerializerImpl.class);

            FolderRequest folderRequest = new FolderRequest(folderId,params.getString("classifierId",null),params.getString("classifierValue",null));

            if (mlClass != null) {
                FilterBuilder filterBuilder = GuiceConfigSingleton.inject(FilterBuilder.class);
                filterBuilder.setQueryMlClass(mlClass);
                fillServiceFromRequest(filterBuilder, params, mlClass);
                if(params.hasString("classifierId")){
                    FolderCondition additionalCondition = folderService.createCondition(folderRequest);
                    filterBuilder.addAdditionalCondition(additionalCondition.getCondition());
                    filterBuilder.getAdditionalParameters().putAll(additionalCondition.getParameters());
                }
                filterBuilder.addAdditionalCondition(mlFolder.getChildClassCondition());
                FilterResult filterResult = filterBuilder.buildFilterResult();
                objectList = filterResult.getResultList();
                resp.addDataToJson("RecordsCount", new JsonPrimitive(filterResult.getRecordsCount()));
                resp.addDataToJson("PagesCount", new JsonPrimitive(filterResult.getPagesCount()));
                List<MlAttr> attrListToShow = mlFolder.getAttrListToShow();
                if (attrListToShow == null || attrListToShow.isEmpty()) {
                    objectAttrList = mlClass.getInListAttrList();
                } else {
                    objectAttrList = new ArrayList<>();
                    for (MlAttr attr : attrListToShow) {
                        objectAttrList.add(attr);
                    }
                }

                objectAttrList = accessService.checkAccessAttrs(objectAttrList, MlAttrAccessType.SHOW);

                resp.addDataToJson("ObjectClassName", new JsonPrimitive(mlClass.getEntityName()));
                JsonElement objectListDataJson = serializer.serializeObjectList(objectList, objectAttrList, true);
                resp.addDataToJson("ObjectListData", objectListDataJson);
                addSecurityInfo(mlClass, resp);
                addCreationData(mlClass, resp);
            }
            resp.addDataToJson("CurrentFolder", folderService.serializeCurrentFolder(folderRequest));
            resp.addDataToJson("folders",folderService.serializeFolders(folderRequest));

        } else if (className != null) {
            //просмотр списка объектов класса
            MlClass mlClass = metaDataHolder.getMlClassByName(className);

            FilterBuilder filterBuilder = GuiceConfigSingleton.inject(FilterBuilder.class);
            filterBuilder.setQueryMlClass(mlClass);
            fillServiceFromRequest(filterBuilder, params,mlClass);
            FilterResult filterResult = filterBuilder.buildFilterResult();
            List<MlDynamicEntityImpl> objectList = filterResult.getResultList();
            resp.addDataToJson("RecordsCount", new JsonPrimitive(filterResult.getRecordsCount()));
            resp.addDataToJson("PagesCount", new JsonPrimitive(filterResult.getPagesCount()));
            List<MlAttr> objectAttrList = mlClass.getInListAttrList();
            resp.addDataToJson("ObjectClassName", new JsonPrimitive(mlClass.getEntityName()));

            MlDynamicEntitySerializerImpl serializer = GuiceConfigSingleton.inject(MlDynamicEntitySerializerImpl.class);

            objectAttrList = accessService.checkAccessAttrs(objectAttrList, MlAttrAccessType.SHOW);

            JsonElement objectListDataJson = serializer.serializeObjectList(objectList, objectAttrList, true);
            resp.setDataType(MlHttpServletResponse.DataType.JSON);
            resp.addDataToJson("ObjectListData", objectListDataJson);
            addCreationData(mlClass, resp);
        } else {
            //просмотр корневой папки
            FolderRequest folderRequest = new FolderRequest(null);
            resp.addDataToJson("folders",folderService.serializeFolders(folderRequest));
            //responseWithRootFolders(resp);
        }
    }

    protected void fillServiceFromRequest(FilterBuilder filterBuilder, MlHttpServletRequest params, MlClass mlClass) {
        filterBuilder.setOrderAttr(params.getString("orderAttr", null));
        filterBuilder.setOrderType(params.getString("orderType", null));
        filterBuilder.setPageNumber(params.getLong("currentPage", 1L));
        filterBuilder.setRowPerPage(params.getLong("objectsPerPage", 10L));

        List<String> additionalQueries = accessService.getAdditionalQueryForClass(mlClass);
        filterBuilder.addAdditionalCondition(additionalQueries);
        CommonQueryParameterService parameterService = GuiceConfigSingleton.inject(CommonQueryParameterService.class);

        filterBuilder.setAdditionalParameters(parameterService.getCommonParameters(params));

        if (params.hasString("useFilter")) {
            if (params.getString("useFilter").equals("extended")) {
                filterBuilder.useExtendedSearch();
                String jsonSettings = params.getString("extendedFilterSettings", "[]");
                Gson gson = new Gson();
                JsonArray jsonArray = gson.fromJson(jsonSettings, JsonArray.class);
                for (int i = 0; i < jsonArray.size(); i++) {
                    JsonObject object = (JsonObject) jsonArray.get(i);
                    String attrName = object.get("entityName").getAsString();
                    while (object.has("children")) {
                        object = (JsonObject) object.get("children").getAsJsonArray().get(0);
                        attrName += "." + object.get("entityName").getAsString();
                    }
                    if (object.get("op") != null && !object.get("op").isJsonNull()) {
                        filterBuilder.addExtendedParameter(attrName, object.get("op").getAsString(), object.get("value").getAsString());
                    }
                }
            } else {
                if (!params.getString("simpleFilter","").isEmpty()) {
                    filterBuilder.useSimpleSearch().setSimpleSearchValue(params.getString("simpleFilter"));
                    if (params.hasString("searchField")) {
                        if (!params.getString("searchField").isEmpty()) {
                            filterBuilder.setSimpleSearchAttr(params.getString("searchField"));
                        }
                    }
                }
            }
        }
    }

    private void addCreationData(MlClass mlClass, MlHttpServletResponse resp) {
        if (mlClass.getAbstract()) {
            JsonArray elements = new JsonArray();
            for (MlClass childClass : metaDataHolder.getChildrenByClassName(mlClass.getEntityName())) {
                JsonObject cls = new JsonObject();
                cls.add("className", new JsonPrimitive(childClass.getEntityName()));
                cls.add("title", new JsonPrimitive(childClass.getDescription()));
                elements.add(cls);
            }
            resp.addDataToJson("createdClasses", elements);
        }
    }

    @PageBlockAction(action = "deleteObjects")
    public void deleteObjects(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        String className = params.getString("className");
        String idListStr = params.getString("objectIds");
        Gson gson = new Gson();
        Type type = new TypeToken<List<Long>>() {
        }.getType();
        List<Long> ids = gson.fromJson(idListStr, type);
        MlClass mlClass = metaDataHolder.getMlClassByName(className);
        if (!accessService.checkAccessClass(mlClass, MlClassAccessType.DELETE)) {
            String message = String.format("Отсутствуют права доступа на %s объектов класса \"%s\" [%s].", MlClassAccessType.DELETE.getOperationName(), mlClass.getTitle(), mlClass.getEntityName());
            throw new MlSecurityException(message);
        }
        for (Long id : ids) {
            MlDynamicEntityImpl entity = commonDao.findById(id, className);
            commonDao.removeTransactional(entity);
        }
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        resp.addDataToJson("result", new JsonPrimitive("ok"));
    }

    private void addSecurityInfo(MlClass mlClass, MlHttpServletResponse resp) {
        resp.addDataToJson("canCreate", new JsonPrimitive(accessService.checkAccessClass(mlClass, MlClassAccessType.CREATE)));
        resp.addDataToJson("canDelete", new JsonPrimitive(accessService.checkAccessClass(mlClass, MlClassAccessType.DELETE)));
        resp.addDataToJson("canEdit", new JsonPrimitive(accessService.checkAccessClass(mlClass, MlClassAccessType.UPDATE)));
    }

    /**
     * Получение списка папок в кормевой папке
     *
     * @param resp
     */
    protected void responseWithRootFolders(MlHttpServletResponse resp) {
        List<MlFolder> folderList = folderDao.getChildFolders((Long) null);// getFolderListForRoot();
        List<MlAttr> folderAttrList = metaDataHolder.getMlClassByName("MlFolder").getAttrSet();

        MlDynamicEntitySerializerImpl serializer = GuiceConfigSingleton.inject(MlDynamicEntitySerializerImpl.class);
        resp.setDataType(MlHttpServletResponse.DataType.JSON);
        List<MlFolder> resultFolderList = new ArrayList<>();
        for (MlFolder folder : folderList) {
            MlFolder tmpFolder = (MlFolder) folderDao.cloneObject(folder);
            tmpFolder.setParent(null);
            resultFolderList.add(folder);
        }
        JsonElement folderListDataJson = serializer.serializeObjectList(resultFolderList, folderAttrList, true);
        resp.addDataToJson("FolderListData", folderListDataJson);
    }

    /**
     * Получение списка папок в указанной папке
     *
     * @param mlFolder - папка для которой необходимо получить список дочерних папок
     * @return
     */
    private List<MlDynamicEntityImpl> getFolderListForFolder(MlFolder mlFolder) {
        List<MlDynamicEntityImpl> folderList = new ArrayList<>();
        List<MlFolder> childFolders = mlFolder.getChildFolders();
        for (MlFolder folder : childFolders) {
            folderList.add(folder);
        }
        return folderList;
    }

}
