package ru.peak.ml.web.block.controller.impl.list;

import com.google.gson.JsonPrimitive;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.service.annotations.PageBlockAction;

public class ObjectListByClassBlockController extends ObjectListBlockController {

    @PageBlockAction(action = "getClass",dataType = MlHttpServletResponse.DataType.JSON)
    public void getClass(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase mlInstance) {
        Long objectId = params.getLong("objectId", null);
        MlClass mlClass = (MlClass) commonDao.findById(objectId,"MlClass");
        resp.addDataToJson("class",new JsonPrimitive(mlClass.getEntityName()));
    }
}
