package ru.peak.ml.web.block.controller.impl.list;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import org.eclipse.persistence.dynamic.DynamicEntity;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.ml.core.common.AttrType;
import ru.peak.ml.core.filter.result.FilterResult;
import ru.peak.ml.core.filter.FilterBuilder;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.security.MlAttrAccessType;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.gson.adapter.serializers.MlDynamicEntitySerializerImpl;
import ru.peak.ml.web.helper.AttributeHelper;
import ru.peak.ml.web.service.annotations.PageBlockAction;

import java.util.HashMap;
import java.util.List;

/**
 *  Контроллер для получения списка объектов связанного класса
 *
 */
public class ObjectListSelectBlockController extends ObjectListBlockController {

    /**
     * Получение списа объектов связанного класса
     *
     * @param params    -   параметры запроса могут содержать
     *                  refAttrId (long)    -   id атрибута связи
     *                  linkFilter (string) -   условия фильтрации связанных объектов
     *                  objectId (long)     -   id объекта-владельца связи
     *
     * @param resp
     * @param currentPageBlock
     */
    @PageBlockAction(action = "getObjectData")
    public void getObjectListData(MlHttpServletRequest params, MlHttpServletResponse resp, MlPageBlockBase currentPageBlock) {
        resp.setDataType(MlHttpServletResponse.DataType.JSON);

        Long id = Long.parseLong(params.getString("refAttrId", null));
        MlAttr mlAttr = (MlAttr)commonDao.findById(id, "MlAttr");

        MlClass refClass = null;
        if (mlAttr.getFieldType() == AttrType.ONE_TO_MANY){
            refClass = (MlClass)mlAttr.getLinkAttr().getMlClass();
        } else if (mlAttr.getFieldType() == AttrType.MANY_TO_MANY){
            refClass = (MlClass)mlAttr.getLinkClass();
        } else if (mlAttr.getFieldType() == AttrType.MANY_TO_ONE){
            refClass = (MlClass)mlAttr.getLinkClass();
        }

        String linkFilter = mlAttr.get("linkFilter");
        String linkCondition = null;
        if (linkFilter != null && !linkFilter.isEmpty() && params.hasLong("objectId")) {
            MlClass entity = (MlClass)mlAttr.getMlClass();
            String ownerClassName = entity.get("entityName");
            Long ownerId = params.getLong("objectId");
            DynamicEntity ownerEntity = commonDao.findById(ownerId, ownerClassName);
            HashMap<String, Object> context = new HashMap<>();
            context.put("this", ownerEntity);
            linkCondition = AttributeHelper.prepareTemplateString(context, linkFilter);
        } else if (linkFilter != null && !linkFilter.isEmpty()) {
            linkCondition = linkFilter;
        }

        FilterBuilder filterBuilder = GuiceConfigSingleton.inject(FilterBuilder.class);
        filterBuilder.setQueryMlClass(refClass);
        fillServiceFromRequest(filterBuilder,params, refClass);
        filterBuilder.addAdditionalCondition(linkCondition);
        FilterResult filterResult = filterBuilder.buildFilterResult();
        List<MlDynamicEntityImpl> objectList = filterResult.getResultList();
        resp.addDataToJson("RecordsCount", new JsonPrimitive(filterResult.getRecordsCount()));
        resp.addDataToJson("PagesCount", new JsonPrimitive(filterResult.getPagesCount()));

        List<MlAttr> objectAttrList = refClass.getInListAttrList();

        objectAttrList = accessService.checkAccessAttrs(objectAttrList, MlAttrAccessType.SHOW);

        resp.addDataToJson("ObjectClassName", new JsonPrimitive(refClass.getEntityName()));
        resp.addDataToJson("ClassDescription", new JsonPrimitive(refClass.getDescription()));

        if (objectList != null) {
            MlDynamicEntitySerializerImpl serializer = GuiceConfigSingleton.inject(MlDynamicEntitySerializerImpl.class);
            JsonElement objectListDataJson = serializer.serializeObjectList(objectList, objectAttrList, true);
            resp.addDataToJson("ObjectListData", objectListDataJson);
        }

    }
}
