package ru.peak.ml.web.common;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by d_litovchenko on 16.04.15.
 */
public class CommonQueryParameterService {

    public Map<String,Object> getCommonParameters(MlHttpServletRequest request){
        Map<String,Object> result = new HashMap<>();
        result.put("userId",request.getRequest().getSession().getAttribute("user-id"));
        result.put("user",request.getRequest().getSession().getAttribute("user"));
        return result;
    }
}
