package ru.peak.ml.web.common;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 *
 */
public class MlHttpServletRequest {

    private HttpServletRequest request;


    public MlHttpServletRequest(HttpServletRequest request) {
        this.request = request;
//        objectMap = new HashMap<String, Object>();
//        for (Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
//            if (entry.getValue().length > 0) {
//                objectMap.put(entry.getKey(), entry.getValue()[0]);
//            }
//        }
    }

    public HttpServletRequest getRequest() {
        return request;
    }

/*
    public Integer getInt(String name, Integer def) {
        if (!request.getParameterMap().containsKey(name)) return def;
        String objValue = request.getParameterMap().get(name)[0];
        if (objValue == null) return def;
        else {
            try {
                return Double.valueOf(objValue.toString()).intValue();
            } catch (NumberFormatException ignore) {
                return def;
            }
        }
    }
*/

    public Long getLong(String name) {
        if (!request.getParameterMap().containsKey(name))
            throw new RuntimeException("Parameter '" + name + "' doesn't exists!");
        String objValue = request.getParameterMap().get(name)[0];
        if (objValue == null)
            throw new RuntimeException("Parameter '" + name + "' is null!");
        else {
            try {
                return Double.valueOf(objValue.toString()).longValue();
            } catch (NumberFormatException e) {
                throw new RuntimeException("Parameter '" + name + "' couldn't be parsed as integer!", e);
            }
        }
    }

    public Long getLong(String name, Long def) {
        if (!request.getParameterMap().containsKey(name))
            return def;
        String objValue = request.getParameterMap().get(name)[0];
        if (objValue == null) return def;
        else {
            try {
                return Double.valueOf(objValue.toString()).longValue();
            } catch (NumberFormatException e) {
                return def;
            }
        }
    }

    public String getString(String name) {
        if (!request.getParameterMap().containsKey(name))
            throw new RuntimeException("Parameter '" + name + " doesn't exists!");
        Object objValue = request.getParameterMap().get(name)[0];
        if (objValue == null) {
            throw new RuntimeException("Parameter '" + name + "' is null!");
        } else {
            return objValue.toString();
        }
    }

    public String getString(String name, String def) {
        if (!request.getParameterMap().containsKey(name)) return def;
        Object objValue = request.getParameterMap().get(name)[0];
        if (objValue == null) return def;
        else {
            return objValue.toString();
        }
    }

    public boolean hasLong(String name) {
        if (request.getParameterMap().containsKey(name)) {
            try {
                Double.valueOf(request.getParameterMap().get(name)[0]).longValue();
                return true;
            } catch (NumberFormatException ignore) {
            }

        }
        return false;
    }

    public boolean hasString(String name) {
        return request.getParameterMap().containsKey(name);
    }

    public Boolean getBoolean(String name, Boolean def) {
        if (!request.getParameterMap().containsKey(name)) return def;
        Object objValue = request.getParameterMap().get(name)[0];
        if (objValue == null) return def;
        else {
            return Boolean.parseBoolean(objValue.toString());
        }
    }

    public Boolean getBoolean(String name) {
        if (!request.getParameterMap().containsKey(name))
            throw new RuntimeException("Parameter '" + name + " doesn't exists!");
        Object objValue = request.getParameterMap().get(name)[0];
        if (objValue == null) {
            throw new RuntimeException("Parameter '" + name + "' is null!");
        } else {
            return Boolean.parseBoolean(objValue.toString());
        }
    }

    public Map<String, ? extends Object> getParameters() {
        return request.getParameterMap();
    }

}
