package ru.peak.ml.web.common;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.template.TemplateEngine;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class MlHttpServletResponse {
    private HttpServletResponse response;

    public static enum DataType {HTML, JSON, DOWNLOAD, BINARY, NONE, REDIRECT}

    private DataType dataType = DataType.HTML;

    private String htmlTemplate;
    private Map<String, Object> htmlTemplateData = new HashMap<>();

    private JsonElement jsonData;

    private String downloadFileName;
    private byte[] downloadData;
    private byte[] binaryData;


    public MlHttpServletResponse(HttpServletResponse response) {
        this.response = response;
    }

    public void addCookie(Cookie cookie){
        response.addCookie(cookie);
    }

    public String getHtmlTemplate() {
        return htmlTemplate;
    }

    public void setHtmlTemplate(String htmlTemplate) {
        this.dataType = DataType.HTML;
        this.htmlTemplate = htmlTemplate;
    }

    public Map<String, Object> getHtmlTemplateData() {
        return htmlTemplateData;
    }

    public JsonElement getJsonData() {
        if (jsonData == null){
            jsonData = new JsonObject();
        }
        return jsonData;
    }

    public void setJsonData(JsonElement jsonData) {
        this.dataType = DataType.JSON;
        this.jsonData = jsonData;
    }

    public void downloadFile(String fileName,String filePath,String contentType) throws IOException {
        response.setContentType(URLConnection.guessContentTypeFromName(fileName));
        response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        File file = new File(filePath);
        response.setHeader("Content-Length", String.valueOf(file.length()));
        response.setHeader("Content-Type", "application/zip");
        FileInputStream fis = new FileInputStream(file);
        OutputStream sos = response.getOutputStream();
        byte[] buf = new byte[1024];
        int length = 0;
        while ((length = fis.read(buf))>0){
            sos.write(buf,0,length);
        }
        response.getOutputStream().flush();
        response.getOutputStream().close();
    }

    public void renderTemplateToJson(String templatePath, Map<String, Object> data){
        TemplateEngine templateEngine = GuiceConfigSingleton.inject(TemplateEngine.class);
        String html = templateEngine.renderTemplate(templatePath.toString(), data);
        ((JsonObject)getJsonData()).add("html", new JsonPrimitive(html));
    }


    public void addDataToJson(String key, JsonElement value){
       ((JsonObject)getJsonData()).add(key, value);
    }

    public void addDataToJson(Map<String, JsonElement> data){
        for (String key: data.keySet()) {
            ((JsonObject) getJsonData()).add(key, data.get(key));
        }
    }

    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public byte[] getDownloadData() {
        return downloadData;
    }

    public void setDownloadData(byte[] downloadData) {
        this.dataType = DataType.DOWNLOAD;
        this.downloadData = downloadData;
    }

    public byte[] getBinaryData() {
        return binaryData;
    }

    public void setBinaryData(byte[] binaryData) {
        this.dataType = DataType.BINARY;
        this.binaryData = binaryData;
    }

    public String getDownloadFileName() {
        return downloadFileName;
    }

    public void setDownloadFileName(String downloadFileName) {
        this.downloadFileName = downloadFileName;
    }

    public void sendRedirect(String url) throws IOException {
        this.response.sendRedirect(url);
    }

}
