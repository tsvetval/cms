package ru.peak.ml.web.dao;

import com.google.inject.ImplementedBy;
import ru.peak.ml.web.dao.impl.MlPageDaoImpl;
import ru.peak.ml.core.model.page.MlPage;

/**
 *
 */
@ImplementedBy(MlPageDaoImpl.class)
public interface MlPageDao {
    public MlPage getPageByUrl(String url);
}
