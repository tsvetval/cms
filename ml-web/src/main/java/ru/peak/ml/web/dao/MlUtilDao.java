package ru.peak.ml.web.dao;

import com.google.inject.ImplementedBy;
import ru.peak.ml.web.dao.impl.MlUtilDaoImpl;
import ru.peak.ml.core.model.util.MlUtil;

/**
 *
 */
@ImplementedBy(MlUtilDaoImpl.class)
public interface MlUtilDao {
    public MlUtil getUtilByUrl(String url);
}
