package ru.peak.ml.web.dao.impl;

import com.google.inject.Inject;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.folder.MlFolder;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.security.services.AccessServiceImpl;

import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FolderDao extends CommonDao<MlFolder> {

    @Inject
    AccessServiceImpl accessService;

    public MlClass getChildClassByFolderId(long instanceId) {
        MlFolder folder = findById(instanceId, MlFolder.class);
        return folder.getChildClass();
    }

    public List<MlFolder> getChildFolders(Long parentId) {
        List<? extends MlDynamicEntityImpl> result;
        if (parentId == null) {
            if (accessService.hasAdminRole()) {
                result = getResultList("select o from MlFolder o where o.parent is null order by o.order");
            } else {
                return accessService.getUserRootFolders();
            }
        } else {
            result = getChildFolders(findById(parentId));
        }
        result = accessService.checkAccessFolder(result);
        return (List<MlFolder>) result;
    }

    public List<MlFolder> getChildFolders(MlFolder parent) {
        String que = "select o from MlFolder o where o.parent = :parent order by o.order";
        TypedQuery<MlFolder> typedQuery = entityManager.createQuery(que, MlFolder.class);
        typedQuery.setParameter("parent", parent);
        return typedQuery.getResultList();
    }

    public Long getChildFoldersCount(Long parentId) {
        return getChildFoldersCount(findById(parentId));
    }

    public Long getChildFoldersCount(MlFolder parent) {
        String que = "select count(o) from MlFolder o where o.parent = :parent";
        TypedQuery<Long> typedQuery = entityManager.createQuery(que, Long.class);
        typedQuery.setParameter("parent", parent);
        return typedQuery.getResultList().get(0);
    }

    public MlFolder findById(Object id) {
        return findById(id, MlFolder.class);
    }

    public List<MlFolder> getResultList(String query) {
        return getResultList(query, MlFolder.class);
    }

    public List<Long> findPath(Long folderId) {
        List<Long> result = new ArrayList<>();
        MlFolder mlFolder = findById(folderId);
        result.add(mlFolder.getId());
        while (mlFolder.getParent()!=null){
            mlFolder = mlFolder.getParent();
            result.add(mlFolder.getId());
        }
        Collections.reverse(result);
        return result;
    }

    public boolean hasPageBlocks(MlFolder mlFolder, List<MlPageBlockBase> pageBlocks) {
        for(MlPageBlockBase pageBlock: pageBlocks){
            if(!mlFolder.getPageBlocks().contains(pageBlock)){
                return false;
            }
        }
        return true;
    }
}
