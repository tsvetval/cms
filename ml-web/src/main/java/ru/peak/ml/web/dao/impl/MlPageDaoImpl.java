package ru.peak.ml.web.dao.impl;

import ru.peak.ml.web.dao.MlPageDao;
import ru.peak.ml.core.model.page.MlPage;
import ru.peak.ml.core.dao.CommonDao;

import javax.persistence.Query;
import java.util.List;

/**
 *
 */

public class MlPageDaoImpl extends CommonDao<MlPage> implements MlPageDao {

    public MlPage getPageByUrl(String url){
        //TODo check for one value
        Query query =  entityManager.createQuery("select o from MlPage o where o.url = :url");
        query.setParameter("url", url);
        List resultList = query.getResultList();
        if (!resultList.isEmpty()){
            return  (MlPage)resultList.get(0);
        } else {
            return null;
        }
    }

}
