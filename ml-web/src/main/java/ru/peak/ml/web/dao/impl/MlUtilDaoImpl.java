package ru.peak.ml.web.dao.impl;

import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.web.dao.MlUtilDao;
import ru.peak.ml.core.model.util.MlUtil;

import javax.persistence.Query;
import java.util.List;

/**
 *
 */

public class MlUtilDaoImpl extends CommonDao<MlUtil> implements MlUtilDao {

    @Override
    public MlUtil getUtilByUrl(String url) {
        Query query =  entityManager.createQuery("select o from MlUtil o where o.url = :url");
        query.setParameter("url", url);
        List resultList = query.getResultList();
        if (!resultList.isEmpty()){
            return  (MlUtil)resultList.get(0);
        } else {
            return null;
        }
    }
}
