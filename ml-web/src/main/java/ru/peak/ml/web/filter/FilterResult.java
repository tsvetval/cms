package ru.peak.ml.web.filter;


import ru.peak.ml.core.model.MlDynamicEntityImpl;

import java.util.List;

/**
 *
 */
public class FilterResult {
    private List<MlDynamicEntityImpl> resultList;
    private Long recordsCount;
    private Long pagesCount;
    private Long pageCurrent;

    public List<MlDynamicEntityImpl> getResultList() {
        return resultList;
    }

    public void setResultList(List<MlDynamicEntityImpl> resultList) {
        this.resultList = resultList;
    }

    public Long getRecordsCount() {
        return recordsCount;
    }

    public void setRecordsCount(Long recordsCount) {
        this.recordsCount = recordsCount;
    }

    public Long getPagesCount() {
        return pagesCount;
    }

    public void setPagesCount(Long pagesCount) {
        this.pagesCount = pagesCount;
    }

    public Long getPageCurrent() {
        return pageCurrent;
    }

    public void setPageCurrent(Long pageCurrent) {
        this.pageCurrent = pageCurrent;
    }
}
