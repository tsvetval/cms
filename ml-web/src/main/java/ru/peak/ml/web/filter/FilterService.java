package ru.peak.ml.web.filter;

import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.AttrType;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.web.filter.exception.FilterException;
import ru.peak.ml.web.filter.params.FilterParams;
import ru.peak.ml.web.filter.params.SimpleFilterParams;
import ru.peak.ml.web.filter.params.extendedFilter.Filter;
import ru.peak.ml.web.filter.params.extendedFilter.FilterLinked;
import ru.peak.ml.web.filter.params.extendedFilter.FilterSimple;
import ru.peak.ml.web.helper.AttributeHelper;

import javax.persistence.TypedQuery;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Сервис формирования запроса для получения списка
 */
public class FilterService {
    private static final Logger log = LoggerFactory.getLogger(FilterService.class);

    private final static String SIMPLE_FILTER = "simpleFilter";
    private final static String ENTITIES_ALIAS = "o";

    @Inject
    CommonDao commonDao;
    @Inject
    MetaDataHolder metaDataHolder;

    public FilterResult getFilteredData(FilterParams filterParams, MlClass MlClass, String rawCondition) throws FilterException {
        FilterResult result = new FilterResult();
        List<MlDynamicEntityImpl> instanceList;
        TypedQuery query;
        TypedQuery queryCount;
        long recordCount;
        long pageCount;

        /*
            Create order by jpql part
          */
        StringBuilder jpqlOrderBy = new StringBuilder();
        if (filterParams.getOrderAttr() != null && !filterParams.getOrderAttr().isEmpty()) {
            MlAttr orderAttr = metaDataHolder.getAttr(MlClass, filterParams.getOrderAttr());
            if (orderAttr == null) {
                throw new FilterException(String.format("Can't find order attr with name [%s]", filterParams.getOrderAttr()));
            }
            jpqlOrderBy.append(" order by " + ENTITIES_ALIAS + ".").append(orderAttr.getEntityFieldName());
            if (filterParams.getOrder() != null) {
                jpqlOrderBy.append(" ").append(filterParams.getOrder().name());
            } else {
                jpqlOrderBy.append(" ").append(FilterParams.Order.ASC);
            }
        }

        //todo: if rawCondition is not null, need to generate query with intersection


        /*
            Depend on filter settings
         */
        if (filterParams.useExtendedFilter()) {

            FilterLinked filterLinked = filterParams.getExtendedFilterParams().getFilterLinked();

            // Remove empty filters
            validateLinkedFilter(MlClass, filterLinked);

            // Fetch tables for join and create jpql expression for it (need to make possible filters on linked entities)
            List<String> joinTables = fetchJoinTables(ENTITIES_ALIAS, MlClass, filterLinked, new Counter());
            String jpqlJoins = " " + StringUtils.join(joinTables, " ") + " ";

            // Generate WHERE expression with filters set
            String jpqlCondition = generateCondition(ENTITIES_ALIAS, MlClass, filterLinked, new Counter(), new Counter());
            if (!jpqlCondition.equals("")) jpqlCondition = " where " + jpqlCondition;

            // Generate query
            String jpqlSelectPrefix = String.format("select " + ENTITIES_ALIAS + " from %s " + ENTITIES_ALIAS, MlClass.getEntityName());
            String jpqlSelectCountPrefix = String.format("select count(distinct " + ENTITIES_ALIAS + ") from %s " + ENTITIES_ALIAS, MlClass.getEntityName());

            // Generate groupBy
            StringBuilder jpqlGroupBy = new StringBuilder();
            jpqlGroupBy.append(" group by " + ENTITIES_ALIAS);

            // Generate raw condition jpql postfix
            String jpqlRawCondition = "";
            if (rawCondition != null && !rawCondition.equals("")) {
                HashMap<String, Object> context = new HashMap<>();
                context.put("this", ENTITIES_ALIAS);
                jpqlRawCondition = AttributeHelper.prepareTemplateString(context, rawCondition); //todo: need 'where' when no filters
                jpqlRawCondition = !jpqlCondition.equals("")
                        ? " and (" + jpqlRawCondition + ")"
                        : " where " + jpqlRawCondition;
            }


            query = commonDao.getTypedQueryWithSecurityCheck(jpqlSelectPrefix + jpqlJoins + jpqlCondition + jpqlRawCondition + jpqlGroupBy + jpqlOrderBy, MlClass.getEntityName());
            queryCount = commonDao.getTypedQueryWithSecurityCheck(jpqlSelectCountPrefix + jpqlJoins + jpqlCondition + jpqlRawCondition, MlClass.getEntityName());

            // Set query parameters with special recursive function
            try {
                setQueryParams(MlClass, filterLinked, query, new Counter());
                setQueryParams(MlClass, filterLinked, queryCount, new Counter());
            } catch (ParseException e) {
                throw new FilterException("Could not set parameters to query", e);
            }

        } else if (filterParams.useSimpleFilter()) {

            String jpqlSelectPrefix = String.format("select " + ENTITIES_ALIAS + " from %s " + ENTITIES_ALIAS, MlClass.getEntityName());
            String jpqlSelectCountPrefix = String.format("select count(" + ENTITIES_ALIAS + ") from %s " + ENTITIES_ALIAS, MlClass.getEntityName());

            String jpqlCondition = generateSimpleSearchJPQLCondition(MlClass, filterParams.getSimpleFilterParams());

            // Generate raw condition jpql postfix
            String jpqlRawCondition = "";
            if (rawCondition != null && !rawCondition.equals("")) {
                HashMap<String, Object> context = new HashMap<>();
                context.put("this", ENTITIES_ALIAS);
                jpqlRawCondition = " and (" + AttributeHelper.prepareTemplateString(context, rawCondition) + ")"; //todo: need 'where' when no filters
            }


            query = commonDao.getTypedQueryWithSecurityCheck(jpqlSelectPrefix + jpqlCondition + jpqlRawCondition + jpqlOrderBy, MlClass.getEntityName());
            queryCount = commonDao.getTypedQueryWithSecurityCheck(jpqlSelectCountPrefix + jpqlCondition + jpqlRawCondition, MlClass.getEntityName());

            query.setParameter(SIMPLE_FILTER, filterParams.getSimpleFilterParams().getTerm().toUpperCase() + "%");
            queryCount.setParameter(SIMPLE_FILTER, filterParams.getSimpleFilterParams().getTerm().toUpperCase() + "%");
        } else {
            //Select entities without filtration
            String jpqlSelectPrefix = String.format("select " + ENTITIES_ALIAS + " from %s " + ENTITIES_ALIAS, MlClass.getEntityName());
            String jpqlSelectCountPrefix = String.format("select count(" + ENTITIES_ALIAS + ") from %s " + ENTITIES_ALIAS, MlClass.getEntityName());

            /*
                Generate raw condition jpql postfix
            */
            String jpqlRawCondition = "";
            if (rawCondition != null && !rawCondition.equals("")) {
                HashMap<String, Object> context = new HashMap<>();
                context.put("this", ENTITIES_ALIAS);
                jpqlRawCondition = " where " + AttributeHelper.prepareTemplateString(context, rawCondition);
            }

            query = commonDao.getTypedQueryWithSecurityCheck(jpqlSelectPrefix + jpqlRawCondition + jpqlOrderBy, MlClass.getEntityName());
            queryCount = commonDao.getTypedQueryWithSecurityCheck(jpqlSelectCountPrefix + jpqlRawCondition, MlClass.getEntityName());
        }

        // Count entities and pages
        recordCount = Integer.valueOf(queryCount.getSingleResult().toString());
        pageCount = recordCount / filterParams.getObjectsPerPage();
        long addOnePage = recordCount % filterParams.getObjectsPerPage();
        if (addOnePage > 0) {
            pageCount++;
        }

        // Select entities and create result
        query.setFirstResult((int) ((filterParams.getCurrentPage() - 1) * filterParams.getObjectsPerPage()));
        query.setMaxResults(filterParams.getObjectsPerPage().intValue());
        instanceList = query.getResultList();

        result.setResultList(instanceList);
        result.setPagesCount(pageCount);
        result.setRecordsCount(recordCount);
        result.setPageCurrent(filterParams.getCurrentPage());

        return result;
    }

    private void validateLinkedFilter(MlClass MlClass, FilterLinked filterLinked) {
        Iterator<Filter> it = filterLinked.children.iterator();
        while (it.hasNext()) {
            Filter filter = it.next();
            if (filter instanceof FilterLinked) {
                FilterLinked nested = (FilterLinked) filter;
                MlAttr attr = MlClass.getAttr(nested.entityName);
                MlClass linkClass = attr.getFieldType().equals(AttrType.ONE_TO_MANY)
                        ? attr.getLinkAttr().getMlClass()
                        : attr.getLinkClass();


                validateLinkedFilter(linkClass, (FilterLinked) filter);
            } else {
                FilterSimple filterSimple = (FilterSimple) filter;
                MlAttr attr = MlClass.getAttr(filterSimple.entityName);

                try {
                    // Try to cast value
                    switch (attr.getFieldType()) {
                        case LONG:
                            Long.valueOf(filterSimple.value);
                            break;
                        case DATE:
                            new SimpleDateFormat("dd.MM.yyyy").parse(filterSimple.value);
                            break;
                    }
                } catch (NumberFormatException | ParseException e) {
                    // If value has wrong format - remove filter
                    it.remove();
                }
            }
        }
    }

    private List<String> fetchJoinTables(String prefix, MlClass MlClass, FilterLinked filterLinked, Counter tableCounter) {
        List<String> tables = new ArrayList<>();
        for (Filter filter : filterLinked.children) {
            if (filter instanceof FilterLinked) {
                FilterLinked nestedFilter = (FilterLinked) filter;
                MlAttr attr = MlClass.getAttr(filter.entityName);
                String joinAlias = "table_" + (tableCounter.value++);
                String newPrefix = prefix + "." + nestedFilter.entityName;
                tables.add("join " + newPrefix + "" + " " + joinAlias);

                MlClass linkClass = (MlClass) (attr.getFieldType().equals(AttrType.ONE_TO_MANY)
                        ? attr.getLinkAttr().getMlClass()
                        : attr.getLinkClass());

                tables.addAll(fetchJoinTables(joinAlias, linkClass, nestedFilter, tableCounter));
            }
        }

        return tables;
    }

    private String generateCondition(String prefix, MlClass MlClass, FilterLinked filterLinked, Counter paramCounter, Counter tableCounter) {
        List<String> conditions = new ArrayList<>();
        for (Filter filter : filterLinked.children) {
            if (filter instanceof FilterSimple) {
                FilterSimple filterSimple = (FilterSimple) filter;

                String path = prefix + "." + filterSimple.entityName;
                String paramName = ":p" + (paramCounter.value++);

                conditions.add(generateCompare(
                        MlClass.getAttr(filterSimple.entityName),
                        path,
                        filterSimple.op,
                        paramName));
            } else {
                FilterLinked nested = (FilterLinked) filter;
                String joinAlias = "table_" + (tableCounter.value++);
                MlAttr attr = MlClass.getAttr(nested.entityName);
                MlClass linkClass = (MlClass) (attr.getFieldType().equals(AttrType.ONE_TO_MANY)
                        ? attr.getLinkAttr().getMlClass()
                        : attr.getLinkClass());
                conditions.add(generateCondition(joinAlias, linkClass, nested, paramCounter, tableCounter));
            }
        }
        return conditions.isEmpty() ? "" : StringUtils.join(conditions, " and ");
    }


    private void setQueryParams(MlClass MlClass, FilterLinked filterLinked, TypedQuery typedQuery, Counter paramCounter) throws ParseException {

        for (Filter filter : filterLinked.children) {
            if (filter instanceof FilterSimple) {
                FilterSimple filterSimple = (FilterSimple) filter;
                MlAttr attr = MlClass.getAttr(filterSimple.entityName);

                Object value;
                switch (attr.getFieldType()) {
                    case LONG:
                        value = Long.valueOf(filterSimple.value);
                        break;
                    case ENUM:
                        value = filterSimple.value.trim();
                        break;
                    case STRING:
                        value = filterSimple.value.trim();
                        break;
                    case DATE:
                        value = new SimpleDateFormat("dd.MM.yyyy").parse(filterSimple.value.trim());
                        break;
                    default:
                        throw new RuntimeException("Behaviour doesn't defined for type: " + attr.getFieldType());
                }

                String paramName = "p" + (paramCounter.value++);
                typedQuery.setParameter(paramName, value);
            } else {
                FilterLinked nested = (FilterLinked) filter;
                MlAttr attr = MlClass.getAttr(nested.entityName);

                MlClass linkClass = attr.getFieldType().equals(AttrType.ONE_TO_MANY)
                        ? attr.getLinkAttr().getMlClass()
                        : attr.getLinkClass();

                setQueryParams(linkClass, nested, typedQuery, paramCounter);
            }
        }
    }


    private String generateSimpleSearchJPQLCondition(MlClass MlClass, SimpleFilterParams params) {
        List<String> conditions = new ArrayList<>();
        for (MlAttr mlAttr : metaDataHolder.getSimpleSearchAttrList(MlClass)) {
            if (params.getSearchField() != null && !params.getSearchField().equals(mlAttr.getEntityFieldName())) {
                continue;
            }
            switch (mlAttr.getFieldType()) {
                case TEXT:
                case STRING:
                    conditions.add(" UPPER(o." + mlAttr.getEntityFieldName() + ") like UPPER(concat('%',:" + SIMPLE_FILTER + ",'%'))");
                    break;
                case DOUBLE:
                case LONG:
                    conditions.add("cast( o." + mlAttr.getEntityFieldName() + " as text) like :" + SIMPLE_FILTER);
                    break;
                case DATE:
                    conditions.add("FUNC('to_char',o." + mlAttr.getEntityFieldName() + ",'dd.mm.yyyy hh24:mi:ss' ) like concat('%',:" + SIMPLE_FILTER + ",'%')");
                    break;
                case ENUM:
                    conditions.add(" (select upper(e.title) from MlEnum e where e.mlAttr.id = " + mlAttr.getId() + " and e.code = o." + mlAttr.getEntityFieldName() + ") like concat('%',:" + SIMPLE_FILTER + ",'%')");
                    break;
            }
        }
        if (conditions.size() > 0) {
            return " where (" + StringUtils.join(conditions, " or ") + ")";
        }
        return " where (:" + SIMPLE_FILTER + " = :" + SIMPLE_FILTER + ")";
    }


    private String generateCompare(MlAttr attr, String path, String op, String value) {
        switch (attr.getFieldType()) {
            case LONG:
                switch (op) {
                    case "equals":
                        return path + " = " + value;
                    case "not_equals":
                        return path + " != " + value;
                    case "less":
                        return path + " < " + value;
                    case "greater":
                        return path + " > " + value;
                    case "less_or_equals":
                        return path + " <= " + value;
                    case "greater_or_equals":
                        return path + " >= " + value;
                }
            case STRING:
                switch (op) {
                    case "equals":
                        return "UPPER(" + path + ") = UPPER(" + value + ")";
                    case "not_equals":
                        return "UPPER(" + path + ") != UPPER(" + value + ")";
                    case "contains":
                        return "UPPER(" + path + ") like UPPER(concat('%', " + value + ", '%'))";
                    case "not_contains":
                        return "UPPER(" + path + ") not like UPPER(concat('%', " + value + ", '%'))";
                    case "startsWith":
                        return "UPPER(" + path + ") like UPPER(concat(" + value + ", '%'))";
                    case "endsWith":
                        return "UPPER(" + path + ") like UPPER(concat('%', " + value + "))";
                }
            case ENUM:
                switch (op) {
                    case "equals":
                        return "UPPER(" + path + ") = UPPER(" + value + ")";
                    case "not_equals":
                        return "UPPER(" + path + ") != UPPER(" + value + ")";
                }
            case DATE:
                switch (op) {
                    case "equals":
                        return path + " = " + value;
                    case "not_equals":
                        return path + " != " + value;
                    case "from":
                        return path + " >= " + value;
                    case "to":
                        return path + " <= " + value;
                }
        }


        throw new RuntimeException("Operation '" + op + "' doesn't supported for filed type '" + attr.getFieldType() + "'");
    }


    private static class Counter {
        public int value;

        private Counter() {
            this.value = 0;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
    }

}
