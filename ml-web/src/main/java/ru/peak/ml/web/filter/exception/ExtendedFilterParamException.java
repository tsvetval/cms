package ru.peak.ml.web.filter.exception;

/**
 *
 */
public class ExtendedFilterParamException extends Exception{

    public ExtendedFilterParamException(String message) {
        super(message);
    }
}
