package ru.peak.ml.web.filter.exception;

/**
 *
 */
public class FilterException  extends Exception{

    public FilterException(String message) {
        super(message);
    }

    public FilterException() {
        super();    //To change body of overridden methods use File | Settings | File Templates.
    }

    public FilterException(String message, Throwable cause) {
        super(message, cause);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public FilterException(Throwable cause) {
        super(cause);    //To change body of overridden methods use File | Settings | File Templates.
    }

    protected FilterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);    //To change body of overridden methods use File | Settings | File Templates.
    }
}
