package ru.peak.ml.web.filter.params;

import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.web.filter.types.SearchCondition;

/**
 *
 */
public class ExtendedFilterCriterion {

    private MlAttr attr;
    private SearchCondition condition;
    private String value;
    private Long index;

    public ExtendedFilterCriterion(MlAttr attr, SearchCondition condition, String value, Long index) {
        this.attr = attr;
        this.condition = condition;
        this.value = value;
        this.index = index;

    }

    public ExtendedFilterCriterion(MlAttr attr, SearchCondition condition, String value) {
        this(attr, condition, value, null);
    }

    public MlAttr getAttr() {
        return attr;
    }

    public SearchCondition getCondition() {
        return condition;
    }

    public String getValue() {
        return value;
    }
}
