package ru.peak.ml.web.filter.params;

import com.google.gson.*;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.filter.exception.ExtendedFilterParamException;
import ru.peak.ml.web.filter.params.extendedFilter.Filter;
import ru.peak.ml.web.filter.params.extendedFilter.FilterLinked;
import ru.peak.ml.web.filter.params.extendedFilter.FilterSimple;

import java.util.ArrayList;
import java.util.List;

public class ExtendedFilterParams {

    /*
        Root filter settings
     */
    FilterLinked filterLinked;

    public FilterLinked getFilterLinked() {
        return filterLinked;
    }

    public void parseRequest(MlHttpServletRequest request) throws ExtendedFilterParamException {
        try {
            String jsonSettings = request.getString("extendedFilterSettings", "[]");
            Gson gson = new Gson();
            JsonArray jsonArray = gson.fromJson(jsonSettings, JsonArray.class);
            filterLinked = new FilterLinked("root", parse(jsonArray));
        } catch (JsonSyntaxException e) {
            throw new RuntimeException("Erorr while parse extended search parameters");
        }
    }

    private static List<Filter> parse(JsonArray context) {
        try {
            List<Filter> resultChildren = new ArrayList<>();
            for (JsonElement jsonElement : context) {
                JsonObject jsObject = jsonElement.getAsJsonObject();

                String id = jsObject.get("entityName").getAsString();

                Filter filter;
                if(jsObject.has("children"))
                {
                    List<Filter> children = parse(jsObject.getAsJsonArray("children"));
                    filter = new FilterLinked(id, children);
                }
                else {
                    String op = jsObject.get("op").getAsString();
                    String value = jsObject.get("value").getAsString();
                    //String searchField = jsObject.get("searchField").getAsString();

                    filter = new FilterSimple(id, op, value);
                }
                resultChildren.add(filter);
            }
            return resultChildren;
        } catch (JsonSyntaxException e) {
            throw new RuntimeException("Erorr while parse extended search parameters");
        }
    }


}
