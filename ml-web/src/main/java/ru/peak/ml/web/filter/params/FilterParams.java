package ru.peak.ml.web.filter.params;

import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.filter.exception.ExtendedFilterParamException;

/**
 *
 */
public class FilterParams {
    private static final String REQUEST_CURRENT_PAGE = "currentPage";
    private static final String REQUEST_OBJECTS_PER_PAGE = "objectsPerPage";
    private static final String REQUEST_USE_FILTER = "useFilter";// возможны значения simple, extended
    private static final String USE_FILTER_SIMPLE = "simple";
    private static final String USE_FILTER_EXTENDED = "extended";

    private static final String REQUEST_ORDER_ATTR = "orderAttr";
    private static final String REQUEST_ORDER_TYPE = "orderType";

    private static final Long DEFAULT_CURRENT_PAGE = 1L;
    private static final Long DEFAULT_OBJECTS_PER_PAGE = 10L;

    public enum Order{ASC, DESC}

    private String orderAttr;
    private Order order;

    private String useFilter;
    private Long currentPage;
    private Long objectsPerPage;
    private SimpleFilterParams simpleFilterParams;
    private ExtendedFilterParams extendedFilterParams;

    public Long getCurrentPage() {
        return currentPage != null ? currentPage : DEFAULT_CURRENT_PAGE;
    }

    public SimpleFilterParams getSimpleFilterParams() {
        return simpleFilterParams;
    }

    public ExtendedFilterParams getExtendedFilterParams() {
        return extendedFilterParams;
    }

    public boolean useSimpleFilter(){
        return useFilter != null && useFilter.equals(USE_FILTER_SIMPLE);
    }

    public boolean useExtendedFilter(){
        return useFilter != null && useFilter.equals(USE_FILTER_EXTENDED);
    }

    public String getOrderAttr() {
        return orderAttr;
    }

    public void setOrderAttr(String orderAttr) {
        this.orderAttr = orderAttr;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public void setCurrentPage(Long currentPage) {
        this.currentPage = currentPage;
    }

    public Long getObjectsPerPage() {
        return objectsPerPage != null ? objectsPerPage : DEFAULT_OBJECTS_PER_PAGE;
    }

    public void setObjectsPerPage(Long objectsPerPage) {
        this.objectsPerPage = objectsPerPage;
    }

    public void parseRequest(MlHttpServletRequest request) throws ExtendedFilterParamException {
        currentPage = request.getLong(REQUEST_CURRENT_PAGE, null);
        objectsPerPage = request.getLong(REQUEST_OBJECTS_PER_PAGE, null);

        useFilter = request.getString(REQUEST_USE_FILTER, null);

        orderAttr = request.getString(REQUEST_ORDER_ATTR, null);
        if (orderAttr != null){
            String orderType = request.getString(REQUEST_ORDER_TYPE, null);
            if (orderType != null){
                this.order = Order.valueOf(orderType.toUpperCase());
            }
        }

        simpleFilterParams = new SimpleFilterParams();
        simpleFilterParams.parseRequest(request);

        extendedFilterParams = new ExtendedFilterParams();
        extendedFilterParams.parseRequest(request);
    }

}
