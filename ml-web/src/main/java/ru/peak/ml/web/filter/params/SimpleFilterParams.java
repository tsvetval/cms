package ru.peak.ml.web.filter.params;

import ru.peak.ml.web.common.MlHttpServletRequest;

/**
 *
 */
public class SimpleFilterParams {
    public static String REQUEST_SIMPLE_FILTER = "simpleFilter";
    public static String REQUEST_SEARCH_FIELD = "searchField";

    private String term;
    private String searchField;

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public void parseRequest(MlHttpServletRequest request){
        this.term = request.getString(REQUEST_SIMPLE_FILTER, null);
        String searchField = request.getString(REQUEST_SEARCH_FIELD, null);
        if (searchField != null && !searchField.isEmpty()) {
            this.searchField = searchField;
        }

    }
}
