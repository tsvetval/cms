package ru.peak.ml.web.filter.params.extendedFilter;

import java.util.List;

/**
 * Describes filter for attributes in linked entity
 */
public class FilterLinked extends Filter {

    public final List<Filter> children;

    public FilterLinked(String entityName, List<Filter> children) {
        super(entityName);
        this.children = children;
    }
}
