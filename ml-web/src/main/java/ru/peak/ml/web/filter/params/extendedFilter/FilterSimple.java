package ru.peak.ml.web.filter.params.extendedFilter;

public class FilterSimple extends Filter {

    public final String op;
    public final String value;

    public FilterSimple(String entityName, String op, String value) {
        super(entityName);
        this.op = op;
        this.value = value;
    }
}
