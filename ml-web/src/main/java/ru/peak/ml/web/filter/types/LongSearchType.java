package ru.peak.ml.web.filter.types;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public  class LongSearchType {
    private static final List<SearchCondition> conditionList = new ArrayList<SearchCondition>()
    {{add(SearchCondition.GRATER_THAN);add(SearchCondition.LESS_THAN);add(SearchCondition.EQUAL);}};

    public static List<SearchCondition> getConditionList() {
        return conditionList;
    }
}
