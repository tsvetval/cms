package ru.peak.ml.web.filter.types;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public enum  SearchCondition {
    LIKE_RIGHT("Начинается с"),
    LIKE_LEFT("Заканчивается на"),
    LIKE_ALL("Содержит"),
    //LIKE_NO("Равно"),

    GRATER_THAN("Больше чем"),
    GRATER_EQUAL_THAN("Больше равно чем"),
    LESS_THAN("Меньше  чем"),
    LESS_EQUAL_THAN("Меньше равно чем"),
    EQUAL("Равно");



    private SearchCondition(String label) {
        this.label = label;
    }

    private String label;

    public String getLabel() {
        return label;
    }

}
