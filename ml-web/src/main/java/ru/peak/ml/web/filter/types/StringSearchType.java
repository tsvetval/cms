package ru.peak.ml.web.filter.types;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class StringSearchType {
    private static final List<SearchCondition> conditionList = new ArrayList<SearchCondition>()
    {{add(SearchCondition.LIKE_RIGHT);add(SearchCondition.LIKE_LEFT);add(SearchCondition.LIKE_ALL);add(SearchCondition.EQUAL);}};


    public static List<SearchCondition> getConditionList() {
        return conditionList;
    }
}
