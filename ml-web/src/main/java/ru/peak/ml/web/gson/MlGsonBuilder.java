package ru.peak.ml.web.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlAttrGroup;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.web.gson.adapter.*;

/**
 *
 */
public class MlGsonBuilder {
    public static Gson getViewFormSerializer() {
        GsonBuilder gson = new GsonBuilder();
        gson.registerTypeAdapter(MlAttr.class, new MlAttrAdapter());
        gson.registerTypeAdapter(MlAttrGroup.class, new MlAttrGroupAdapter());
        gson.registerTypeAdapter(MlClass.class, new MlClassAdapter());
        return gson.create();
    }

    public static Gson getDynamicEntityNameValueSerializer(){
        GsonBuilder gson = new GsonBuilder();
        gson.registerTypeHierarchyAdapter(MlDynamicEntityImpl.class, new NameValueMlDynamicEntityAdapter());
                return gson.create();
    }

    public static Gson getFormViewObjectSerializer() {
        GsonBuilder gson = new GsonBuilder();
        gson.registerTypeHierarchyAdapter(MlDynamicEntityImpl.class, new ViewMlDynamicEntityAdapter());
        return gson.create();
    }
}
