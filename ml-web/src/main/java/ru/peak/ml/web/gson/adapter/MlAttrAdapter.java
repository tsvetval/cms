package ru.peak.ml.web.gson.adapter;

import com.google.gson.*;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.web.gson.adapter.serializers.AttrMetaSerializerImpl;

import java.lang.reflect.Type;

/**
 *
 */
public class MlAttrAdapter implements JsonSerializer<MlAttr> {

    @Override
    public JsonElement serialize(MlAttr mlAttr, Type type, JsonSerializationContext jsonSerializationContext) {

        AttrMetaSerializerImpl dynamicEntitySerializer = GuiceConfigSingleton.inject(AttrMetaSerializerImpl.class);
        return dynamicEntitySerializer.serializeInFormAttrMeta(mlAttr);
    }

}
