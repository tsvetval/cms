package ru.peak.ml.web.gson.adapter;

import com.google.gson.*;
import ru.peak.ml.core.model.system.MlAttrGroup;

import java.lang.reflect.Type;

/**
 *
 */
public class MlAttrGroupAdapter implements JsonSerializer<MlAttrGroup> {
    @Override
    public JsonElement serialize(MlAttrGroup mlAttrGroup, Type type, JsonSerializationContext jsonSerializationContext) {
        // serialize common attrs
        JsonObject result = new JsonObject();
        result.add(MlAttrGroup.TITLE, new JsonPrimitive(mlAttrGroup.getTitle()));
        if (mlAttrGroup.getParent() != null){
            result.add(MlAttrGroup.PARENT, new JsonPrimitive(mlAttrGroup.getParent().getId()));
        }
        result.add("id", new JsonPrimitive(mlAttrGroup.getId()));
        return result;

    }
}
