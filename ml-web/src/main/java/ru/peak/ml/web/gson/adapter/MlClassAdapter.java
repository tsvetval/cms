package ru.peak.ml.web.gson.adapter;

import com.google.gson.*;
import com.google.inject.Inject;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.model.security.MlAttrAccessType;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlAttrGroup;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.services.AccessService;

import java.lang.reflect.Type;
import java.util.List;

/**
 *
 */
public class MlClassAdapter implements JsonSerializer<MlClass> {

    @Override
    public JsonElement serialize(MlClass mlClass, Type type, JsonSerializationContext jsonSerializationContext) {
        // serialize common attrs
        JsonObject result = new JsonObject();
        result.add(MlClass.ENTITY_NAME, new JsonPrimitive(mlClass.getEntityName()));
        result.add("entityId", new JsonPrimitive(mlClass.getId()));
        result.add(MlClass.DESCRIPTION, new JsonPrimitive(mlClass.getDescription()));

        // serialize inForm attrs
        JsonArray attrList = new JsonArray();
        result.add("attrList", attrList);
        List<MlAttr> attrsList = mlClass.getInFormAttrList();
        AccessService accessService = GuiceConfigSingleton.inject(AccessService.class);
        attrsList = accessService.checkAccessAttrs(attrsList, MlAttrAccessType.SHOW);
        for (MlAttr attr : attrsList){
            attrList.add(jsonSerializationContext.serialize(attr));
        }
        // serialize Groups
        JsonArray groupList = new JsonArray();
        result.add("groupList", groupList);
        for (MlAttrGroup group : mlClass.getGroupList()) {
            groupList.add(jsonSerializationContext.serialize(group));
        }

        return result;
    }

}
