package ru.peak.ml.web.gson.adapter;

import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.web.gson.adapter.serializers.MlDynamicEntityNameValueSerializerImpl;
import ru.peak.ml.web.gson.adapter.serializers.MlDynamicEntitySerializerImpl;

import java.lang.reflect.Type;

/**
 * Created by d_litovchenko on 08.04.15.
 */
public class NameValueMlDynamicEntityAdapter implements JsonSerializer<MlDynamicEntityImpl> {
    @Override
    public JsonElement serialize(MlDynamicEntityImpl mlDynamicEntity, Type type, JsonSerializationContext jsonSerializationContext) {
        MlDynamicEntityNameValueSerializerImpl dynamicEntitySerializer = GuiceConfigSingleton.inject(MlDynamicEntityNameValueSerializerImpl.class);
        return dynamicEntitySerializer.serializeInFormObject(mlDynamicEntity);
    }
}
