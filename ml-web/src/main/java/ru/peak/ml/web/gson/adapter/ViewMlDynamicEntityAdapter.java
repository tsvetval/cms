package ru.peak.ml.web.gson.adapter;

import com.google.gson.*;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.web.gson.adapter.serializers.MlDynamicEntitySerializerImpl;

import java.lang.reflect.Type;

/**
 *
 */
public class ViewMlDynamicEntityAdapter implements JsonSerializer<MlDynamicEntityImpl> {
    @Override
    public JsonElement serialize(MlDynamicEntityImpl mlDynamicEntity, Type type, JsonSerializationContext jsonSerializationContext) {
        MlDynamicEntitySerializerImpl dynamicEntitySerializer = GuiceConfigSingleton.inject(MlDynamicEntitySerializerImpl.class);
        return dynamicEntitySerializer.serializeInFormObject(mlDynamicEntity);
    }
}
