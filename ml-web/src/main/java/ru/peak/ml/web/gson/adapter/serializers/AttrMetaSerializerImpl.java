package ru.peak.ml.web.gson.adapter.serializers;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import ru.ml.core.common.AttrType;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.holders.EnumHolder;
import ru.peak.ml.core.model.security.MlAttrAccessType;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlAttrView;
import ru.peak.ml.core.model.system.MlEnum;
import ru.peak.ml.core.services.AccessService;

import java.util.Collection;

/**
 *
 */
public class AttrMetaSerializerImpl {

    public JsonObject serializeInFormAttrMeta(MlAttr mlAttr) {
        JsonObject result = new JsonObject();
        serializeCommonMetaData(result, mlAttr);
        result.add(MlAttr.NEW_LINE, new JsonPrimitive(mlAttr.isNewLine()));
        result.add(MlAttr.VIEW_POS, new JsonPrimitive(mlAttr.getViewPos()));
        result.add(MlAttr.OFFSET, new JsonPrimitive(mlAttr.getOffset()));
        result.add(MlAttr.TOTAL_LENGTH, new JsonPrimitive(mlAttr.getTotalLength()));
        result.add(MlAttr.TITLE_LENGTH, new JsonPrimitive(mlAttr.getTitleLength()));
        result.add(MlAttr.TABLE_HEIGHT, new JsonPrimitive(mlAttr.getTableHeight()));

        result.add(MlAttr.MANDATORY, new JsonPrimitive(mlAttr.isMandatory()));
        result.add(MlAttr.READONLY, new JsonPrimitive(mlAttr.isReadOnly()));
        result.add(MlAttr.ORDERED, new JsonPrimitive(mlAttr.isOrdered()));

        String format = mlAttr.getFormat();
        if (format != null) {
            result.add(MlAttr.FORMAT, new JsonPrimitive(format));
        }

        String mask = mlAttr.getMask();
        if (mask != null) {
            result.add(MlAttr.MASK, new JsonPrimitive(mask));
        }


        String templateView = mlAttr.getTemplateView();
        if (templateView != null && !templateView.isEmpty()) {
            result.add(MlAttr.TEMPLATE_VIEW, new JsonPrimitive(templateView));
        }

        String templateEdit = mlAttr.getTemplateEdit();
        if (templateEdit != null && !templateEdit.isEmpty()) {
            result.add(MlAttr.TEMPLATE_EDIT, new JsonPrimitive(templateEdit));
        }

        String templateCreate = mlAttr.getTemplateCreate();
        if (templateCreate != null && !templateCreate.isEmpty()) {
            result.add(MlAttr.TEMPLATE_CREATE, new JsonPrimitive(templateCreate));
        }

        MlAttrView view = mlAttr.getView();
        if (view != null) {
            result.add(MlAttr.VIEW, new JsonPrimitive(view.getTemplateName()));
        }

        if (mlAttr.getGroup() != null) {
            result.add("groupId", new JsonPrimitive(mlAttr.getGroup().getId()));
        }
        return result;
    }

    public JsonObject serializeListAttrMeta(MlAttr mlAttr) {
        JsonObject result = new JsonObject();
        serializeCommonMetaData(result, mlAttr);
        return result;
    }

    private void serializeCommonMetaData(JsonObject result, MlAttr mlAttr) {
        AccessService accessService = GuiceConfigSingleton.inject(AccessService.class);
        result.add(MlAttr.ID, new JsonPrimitive(mlAttr.getId()));
        result.add(MlAttr.ENTITY_FIELD_NAME, new JsonPrimitive(mlAttr.getEntityFieldName()));
        result.add(MlAttr.FILED_TYPE, new JsonPrimitive(mlAttr.getFieldType().name()));
        result.add(MlAttr.DESCRIPTION, new JsonPrimitive(mlAttr.getDescription()));
        if (mlAttr.getFormat() != null && !mlAttr.getFormat().isEmpty()) {
            result.add(MlAttr.FORMAT, new JsonPrimitive(mlAttr.getFormat()));
        }
        result.add("canEdit",new JsonPrimitive(accessService.checkAccessAttr(mlAttr, MlAttrAccessType.EDIT)));
        if (mlAttr.getFieldType().equals(AttrType.MANY_TO_ONE)) {
            result.add(MlAttr.LINK_CLASS, new JsonPrimitive(mlAttr.getLinkClass().getEntityName()));
        } else if (mlAttr.getFieldType().equals(AttrType.MANY_TO_MANY)) {
            result.add(MlAttr.LINK_CLASS, new JsonPrimitive(mlAttr.getLinkClass().getEntityName()));
        } else if (mlAttr.getFieldType().equals(AttrType.ONE_TO_MANY)) {
            result.add(MlAttr.LINK_ATTR, new JsonPrimitive(mlAttr.getLinkAttr().getEntityFieldName()));
        } else if (mlAttr.getFieldType().equals(AttrType.ENUM)) {
            result.add("enumList", serializeEnumValues(mlAttr));
        }
        result.add(MlAttr.USE_IN_SIMPLE_SEARCH, new JsonPrimitive(mlAttr.isUseInSimpleSearch()));
        result.add(MlAttr.USE_IN_EXTENDED_SEARCH, new JsonPrimitive(mlAttr.isUseInExtendedSearch()));
        if (mlAttr.getLinkClass() != null) {
            result.add(MlAttr.LINK_CLASS, new JsonPrimitive(mlAttr.getLinkClass().getEntityName()));
        }
//        if (mlAttr.getLinkAttr() != null){
//            result.add(MlAttr.LINK_CLASS, new JsonPrimitive(mlAttr.getLinkAttr().getLinkClass().getEntityName()));
//            result.add(MlAttr.LINK_ATTR, new JsonPrimitive(mlAttr.getLinkAttr().getEntityFieldName()));
//        }

    }

    private JsonElement serializeEnumValues(MlAttr mlAttr) {
        JsonArray resultArray = new JsonArray();
        Collection<MlEnum> enumList = GuiceConfigSingleton.inject(EnumHolder.class).getEnumList(mlAttr);
        for (MlEnum tmpEnum : enumList) {
            JsonObject jsonEnum = new JsonObject();
            jsonEnum.add("code", new JsonPrimitive(tmpEnum.getCode()));
            jsonEnum.add("title", new JsonPrimitive(tmpEnum.getTitle()));
            resultArray.add(jsonEnum);
        }
        return resultArray;
    }


}
