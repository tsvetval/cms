package ru.peak.ml.web.gson.adapter.serializers;

import com.google.gson.*;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.holders.EnumHolder;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlAttrView;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.model.system.MlEnum;
import ru.peak.ml.web.model.MlHeteroLink;
import ru.peak.ml.web.service.MlHeteroLinkService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 *
 */
public class MlDynamicEntityNameValueSerializerImpl {
    @Inject
    AttrMetaSerializerImpl attrMetaSerializer;
    @Inject
    MetaDataHolder metaDataHolder;
    @Inject
    EnumHolder enumHolder;

    public JsonObject serializeInFormObject(MlDynamicEntityImpl mlDynamicEntity) {
        MlClass mlClass = metaDataHolder.getMlClassByEntityDynamicClass(mlDynamicEntity.getClass());
        return serializeObject(mlDynamicEntity, mlClass.getAttrSet(), true);
    }

    public JsonObject serializeObject(MlDynamicEntityImpl mlDynamicEntity, Collection<MlAttr> attrCollection, boolean serializeFullLinkObjects) {
        JsonObject result = new JsonObject();

        for (MlAttr mlAttr : attrCollection) {
            switch (mlAttr.getFieldType()) {
                case TEXT:
                case STRING:
                    result.add(mlAttr.getEntityFieldName(), serializeStringAttr(mlDynamicEntity, mlAttr));
                    break;
                case LONG:
                    result.add(mlAttr.getEntityFieldName(), serializeLongAttr(mlDynamicEntity, mlAttr));
                    break;
                case BOOLEAN:
                    result.add(mlAttr.getEntityFieldName(), serializeBooleanAttr(mlDynamicEntity, mlAttr));
                    break;
                case DATE:
                    result.add(mlAttr.getEntityFieldName(), serializeDateAttr(mlDynamicEntity, mlAttr));
                    break;
                case ENUM:
                    result.add(mlAttr.getEntityFieldName(), serializeEnumAttr(mlDynamicEntity, mlAttr));
                    break;
                case ONE_TO_MANY:
                    if(serializeFullLinkObjects){
                        result.add(mlAttr.getEntityFieldName(), serializeOneToManyAttr(mlDynamicEntity, mlAttr, false));
                    }
                    break;
                case MANY_TO_ONE:
                    if(serializeFullLinkObjects){
                        result.add(mlAttr.getEntityFieldName(), serializeObject((MlDynamicEntityImpl) mlDynamicEntity.get(mlAttr.getEntityFieldName()), mlAttr.getLinkClass().getAttrSet(),false));
                    }
                    break;
                case MANY_TO_MANY:
                    if(serializeFullLinkObjects){
                        result.add(mlAttr.getEntityFieldName(), serializeManyToManyAttr(mlDynamicEntity, mlAttr, false));
                    }
                    break;
                case LONG_LINK:
                    //attrValues.add(mlAttr.getEntityFieldName(), serializeLongLinkAttr(mlDynamicEntity, mlAttr));
                    break;
                case HETERO_LINK:
                    result.add(mlAttr.getEntityFieldName(), serializeHeteroLinkAttr(mlDynamicEntity, mlAttr));
                    break;
                case DOUBLE:
                    result.add(mlAttr.getEntityFieldName(), serializeDoubleAttr(mlDynamicEntity, mlAttr));
                    break;
                case FILE:
                    result.add(mlAttr.getEntityFieldName(), serializeFileAttr(mlDynamicEntity, mlAttr));
                    break;
                case ONE_TO_ONE:
                    break;
            }
        }
        return result;
    }

    public JsonElement serializeObjectList(Collection<? extends MlDynamicEntityImpl> objectList, Collection<MlAttr> attrList, boolean withLinkAttrValues) {

        JsonArray objectArray = new JsonArray();
        for (MlDynamicEntityImpl listObject : objectList) {
            /*if (!withLinkAttrValues) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.add("title", new JsonPrimitive(listObject.getTitle()));
                objectArray.add(jsonObject);
            } else {*/
                JsonObject jsonListObject = serializeObject(listObject, attrList, false);
                objectArray.add(jsonListObject);
            //}
        }
        return objectArray;
    }


    private JsonElement serializeStringAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        if (mlDynamicEntity.get(mlAttr.getEntityFieldName()) == null) {
            return JsonNull.INSTANCE;
        }
        return new JsonPrimitive((String) mlDynamicEntity.get(mlAttr.getEntityFieldName()));
    }

    private JsonElement serializeLongAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        if (mlDynamicEntity.get(mlAttr.getEntityFieldName()) == null) {
            return JsonNull.INSTANCE;
        }
        return new JsonPrimitive((Long) mlDynamicEntity.get(mlAttr.getEntityFieldName()));
    }

    private JsonElement serializeDoubleAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        if (mlDynamicEntity.get(mlAttr.getEntityFieldName()) == null) {
            return JsonNull.INSTANCE;
        }
        return new JsonPrimitive((Double) mlDynamicEntity.get(mlAttr.getEntityFieldName()));
    }

    private JsonElement serializeBooleanAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        if (mlDynamicEntity.get(mlAttr.getEntityFieldName()) == null) {
            return JsonNull.INSTANCE;
        }
        return new JsonPrimitive((Boolean) mlDynamicEntity.get(mlAttr.getEntityFieldName()));
    }

    private JsonElement serializeLongLinkAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        String longLinkValue = mlAttr.getLongLinkValue();
        String[] longLinkPath = longLinkValue.split("->");
        Object o = mlDynamicEntity.get(longLinkPath[0]);
        //TODO check
        for (int i = 1; i < longLinkPath.length; i++) {
            if (o != null) {
                o = ((MlDynamicEntityImpl) o).get(longLinkPath[i]);
            }
        }
        if (o != null){
            if (o instanceof MlDynamicEntityImpl){
                return new JsonPrimitive(((MlDynamicEntityImpl)o).getTitle());
            } else {
                return new JsonPrimitive((o.toString()));
            }
        } else {
            return  JsonNull.INSTANCE;
        }
    }

    private JsonElement serializeDateAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        if (mlDynamicEntity.get(mlAttr.getEntityFieldName()) == null) {
            return JsonNull.INSTANCE;
        }
        Date dateValue = mlDynamicEntity.get(mlAttr.getEntityFieldName());
        return new JsonPrimitive(dateValue.getTime());
    }

    private JsonElement serializeManyToOneAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        if (mlDynamicEntity.get(mlAttr.getEntityFieldName()) == null) {
            return JsonNull.INSTANCE;
        }
        JsonObject result = new JsonObject();
        MlDynamicEntityImpl linkObject = mlDynamicEntity.get(mlAttr.getEntityFieldName());
        result.add("title", new JsonPrimitive(linkObject.getTitle()));
        result.add("objectId", new JsonPrimitive(linkObject.getOUID()));
        result.add("mlClass", new JsonPrimitive(linkObject.getInstanceMlClass().getEntityName()));
        return result;
    }

    private JsonElement serializeEnumAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        if (mlDynamicEntity.get(mlAttr.getEntityFieldName()) == null) {
            return JsonNull.INSTANCE;
        }
        MlAttr enumAttr = mlAttr;
        if(mlAttr.getMlClass().getParent()!=null){
            enumAttr = findParentAttr(mlAttr.getMlClass(),mlAttr);
        }
        MlEnum mlEnum = enumHolder.getEnum(enumAttr, (String) mlDynamicEntity.get(mlAttr.getEntityFieldName()));
        JsonObject result = new JsonObject();
        result.add("code", new JsonPrimitive(mlEnum.getCode()));
        result.add("title", new JsonPrimitive(mlEnum.getTitle()));
        return result;
    }

    private MlAttr findParentAttr(MlClass mlClass, MlAttr mlAttr) {
        MlAttr result = mlClass.getParent().getAttr(mlAttr.getEntityFieldName());
        if(result == null){
            result = mlAttr;
        }
        if(mlClass.getParent().getParent()!=null){
            result = findParentAttr(mlClass.getParent(), result);
        }
        return result;
    }

    private JsonElement serializeOneToManyAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr, boolean withLinkAttrValues) {
        MlClass linkClass = metaDataHolder.getMlClassByName(mlAttr.getLinkAttr().getMlClass().getEntityName());
        Collection<MlDynamicEntityImpl> attrValueObjectList = mlDynamicEntity.get(mlAttr.getEntityFieldName());
        if (attrValueObjectList == null) {
            attrValueObjectList = new ArrayList<>();
        }
        return serializeObjectList(attrValueObjectList, linkClass.getAttrSet(), withLinkAttrValues);
    }

    private JsonElement serializeManyToManyAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr, boolean withLinkAttrValues) {
        MlClass linkClass = metaDataHolder.getMlClassByName(mlAttr.getLinkClass().getEntityName());
        Collection<MlDynamicEntityImpl> attrValueObjectList = mlDynamicEntity.get(mlAttr.getEntityFieldName());
        //атрибуть родителя возвращаюся в виде null, поэтому создаем пустой список
        if (attrValueObjectList == null) {
            attrValueObjectList = new ArrayList<>();
        }

        return serializeObjectList(attrValueObjectList, linkClass.getAttrSet(), withLinkAttrValues);
    }

    private JsonElement serializeHeteroLinkAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        //  MlClass linkClass = (MlClass) MetaDataHelper.getMlClassByName(mlAttr.getLinkClass().getEntityName());
        List<MlHeteroLink> attrValueObjectList = mlDynamicEntity.get(mlAttr.getEntityFieldName());
        MlHeteroLinkService heteroLinkService = GuiceConfigSingleton.inject(MlHeteroLinkService.class);
        //атрибуть родителя возвращаюся в виде null, поэтому создаем пустой список
        List<MlAttr> mlAttrs = new ArrayList<MlAttr>();
        JsonObject result = new JsonObject();
        JsonArray jsonElements = new JsonArray();
        if (attrValueObjectList == null) {
            attrValueObjectList = new ArrayList<>();
        } else if (attrValueObjectList.size() > 0) {
            for(MlHeteroLink link: attrValueObjectList){
                JsonObject jsonObject = new JsonObject();
                MlDynamicEntityImpl entity = heteroLinkService.getObjectHeteroLink(link);
                jsonObject.add("title",new JsonPrimitive(entity.getTitle()));
                jsonObject.add("objectId",new JsonPrimitive(link.getStrId()));
                jsonObject.add("mlClass",new JsonPrimitive(link.getClassName()));
                jsonElements.add(jsonObject);
            }
        }

        result.add("objectList",jsonElements);
        return result;
    }

    private JsonElement serializeFileAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        String filenameField = mlAttr.getEntityFieldName() + "_filename";
        if (mlDynamicEntity.get(filenameField) == null) {
            return JsonNull.INSTANCE;
        }
        JsonObject result = new JsonObject();
        result.add("title", new JsonPrimitive((String) mlDynamicEntity.get(filenameField)));
        result.add("objectId", new JsonPrimitive(mlDynamicEntity.getOUID()));
        result.add("entityFieldName", new JsonPrimitive(mlAttr.getEntityFieldName()));
        return result;
    }


}
