package ru.peak.ml.web.gson.adapter.serializers;

import com.google.gson.*;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.holders.EnumHolder;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.security.MlAttrAccessType;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlAttrView;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.model.system.MlEnum;
import ru.peak.ml.core.services.AccessService;
import ru.peak.ml.web.gson.adapter.serializers.bean.AttrSerializerOption;
import ru.peak.ml.web.model.MlHeteroLink;
import ru.peak.ml.web.service.MlHeteroLinkService;

import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 *
 */
public class MlDynamicEntitySerializerImpl {
    @Inject
    AttrMetaSerializerImpl attrMetaSerializer;
    @Inject
    MetaDataHolder metaDataHolder;
    @Inject
    EnumHolder enumHolder;
    @Inject
    AccessService accessService;

    public JsonObject serializeInFormObject(MlDynamicEntityImpl mlDynamicEntity) {
        MlClass mlClass = metaDataHolder.getMlClassByEntityDynamicClass(mlDynamicEntity.getClass());
        List<MlAttr> objectAttrList = mlClass.getInFormAttrList();
        objectAttrList = accessService.checkAccessAttrs(objectAttrList, MlAttrAccessType.SHOW);
        return serializeObject(mlDynamicEntity, objectAttrList, true);
    }

    public JsonObject serializeObject(MlDynamicEntityImpl mlDynamicEntity, Collection<MlAttr> attrCollection, boolean serializeFullLinkObjects) {
        JsonObject result = new JsonObject();

        String title = mlDynamicEntity.getTitle();
        if (title != null) {
            result.add("title", new JsonPrimitive(mlDynamicEntity.getTitle()));
        } else {
            result.add("title", JsonNull.INSTANCE);
        }
        if (mlDynamicEntity.getOUID() != null) {
            result.add("objectId", new JsonPrimitive(mlDynamicEntity.getOUID()));
        } else {
            result.add("objectId", JsonNull.INSTANCE);
        }
        result.add("mlClass", new JsonPrimitive(mlDynamicEntity.getInstanceMlClass().getEntityName()));
        result.add("description", new JsonPrimitive(mlDynamicEntity.getInstanceMlClass().getDescription()));
        // serialize inForm attrs
        JsonObject attrValues = new JsonObject();
        result.add("attrValues", attrValues);
        for (MlAttr mlAttr : attrCollection) {
            switch (mlAttr.getFieldType()) {
                case TEXT:
                case STRING:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeStringAttr(mlDynamicEntity, mlAttr));
                    break;
                case LONG:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeLongAttr(mlDynamicEntity, mlAttr));
                    break;
                case BOOLEAN:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeBooleanAttr(mlDynamicEntity, mlAttr));
                    break;
                case DATE:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeDateAttr(mlDynamicEntity, mlAttr));
                    break;
                case ENUM:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeEnumAttr(mlDynamicEntity, mlAttr));
                    break;
                case ONE_TO_MANY:
                    //если атрибуту задано представление "Открыть в новон окне", то такой атрибут не сериализуем
                    if (mlAttr.getView() == null || !MlAttrView.LIST_1_N_EXTERNAL.equals(mlAttr.getView().getCode())) {
                        attrValues.add(mlAttr.getEntityFieldName(), serializeOneToManyAttr(mlDynamicEntity, mlAttr, serializeFullLinkObjects));
                    }
                    break;
                case MANY_TO_ONE:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeManyToOneAttr(mlDynamicEntity, mlAttr));
                    break;
                case MANY_TO_MANY:
                    //если атрибуту задано представление "Открыть в новон окне", то такой атрибут не сериализуем
                    if (mlAttr.getView() == null || !MlAttrView.LIST_M_N_EXTERNAL.equals(mlAttr.getView().getCode())) {
                        attrValues.add(mlAttr.getEntityFieldName(), serializeManyToManyAttr(mlDynamicEntity, mlAttr, serializeFullLinkObjects));
                    }
                    break;
                case LONG_LINK:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeLongLinkAttr(mlDynamicEntity, mlAttr));
                    break;
                case HETERO_LINK:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeHeteroLinkAttr(mlDynamicEntity, mlAttr));
                    break;
                case DOUBLE:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeDoubleAttr(mlDynamicEntity, mlAttr));
                    break;
                case FILE:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeFileAttr(mlDynamicEntity, mlAttr));
                    break;
                case ONE_TO_ONE:
                    break;
            }
        }
        return result;
    }
    //TODO подкмать как переписать без дублирования
    /**
     * Копия метода serializeObject где изменена сериализация serializeOneToManyAttr и serializeManyToManyAttr
     * @param mlDynamicEntity
     * @param attrCollection
     * @return
     */
    public JsonObject serializeObjectWithAttrOptions(MlDynamicEntityImpl mlDynamicEntity, Collection<AttrSerializerOption> attrCollection) {
        JsonObject result = new JsonObject();

        String title = mlDynamicEntity.getTitle();
        if (title != null) {
            result.add("title", new JsonPrimitive(mlDynamicEntity.getTitle()));
        } else {
            result.add("title", JsonNull.INSTANCE);
        }
        if (mlDynamicEntity.getOUID() != null) {
            result.add("objectId", new JsonPrimitive(mlDynamicEntity.getOUID()));
        } else {
            result.add("objectId", JsonNull.INSTANCE);
        }
        result.add("mlClass", new JsonPrimitive(mlDynamicEntity.getInstanceMlClass().getEntityName()));
        result.add("description", new JsonPrimitive(mlDynamicEntity.getInstanceMlClass().getDescription()));
        // serialize inForm attrs
        JsonObject attrValues = new JsonObject();
        result.add("attrValues", attrValues);
        for (AttrSerializerOption mlAttrOption : attrCollection) {
            MlAttr mlAttr = mlAttrOption.getAttr();
            switch (mlAttrOption.getAttr().getFieldType()) {
                case TEXT:
                case STRING:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeStringAttr(mlDynamicEntity, mlAttr));
                    break;
                case LONG:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeLongAttr(mlDynamicEntity, mlAttr));
                    break;
                case BOOLEAN:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeBooleanAttr(mlDynamicEntity, mlAttr));
                    break;
                case DATE:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeDateAttr(mlDynamicEntity, mlAttr));
                    break;
                case ENUM:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeEnumAttr(mlDynamicEntity, mlAttr));
                    break;
                case ONE_TO_MANY:
                    //если атрибуту задано представление "Открыть в новон окне", то такой атрибут не сериализуем
                    if (mlAttr.getView() == null || !MlAttrView.LIST_1_N_EXTERNAL.equals(mlAttr.getView().getCode())) {
                        attrValues.add(mlAttr.getEntityFieldName(), serializeOneToManyAttrWithAttrOptionsInsecure(mlDynamicEntity, mlAttrOption));
                    }
                    break;
                case MANY_TO_ONE:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeManyToOneAttr(mlDynamicEntity, mlAttr));
                    break;
                case MANY_TO_MANY:
                    //если атрибуту задано представление "Открыть в новон окне", то такой атрибут не сериализуем
                    if (mlAttr.getView() == null || !MlAttrView.LIST_M_N_EXTERNAL.equals(mlAttr.getView().getCode())) {
                        attrValues.add(mlAttr.getEntityFieldName(), serializeManyToManyAttrWithAttrOptions(mlDynamicEntity, mlAttrOption));
                    }
                    break;
                case LONG_LINK:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeLongLinkAttr(mlDynamicEntity, mlAttr));
                    break;
                case HETERO_LINK:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeHeteroLinkAttr(mlDynamicEntity, mlAttr));
                    break;
                case DOUBLE:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeDoubleAttr(mlDynamicEntity, mlAttr));
                    break;
                case FILE:
                    attrValues.add(mlAttr.getEntityFieldName(), serializeFileAttr(mlDynamicEntity, mlAttr));
                    break;
                case ONE_TO_ONE:
                    break;
            }
        }
        return result;
    }


    public JsonObject serializeObjectListWithAttrOptions(Collection<? extends MlDynamicEntityImpl> objectList, Collection<AttrSerializerOption> attrOptionList) {
        JsonObject result = new JsonObject();
        JsonArray attrArray = new JsonArray();
        result.add("attrList", attrArray);
        for (AttrSerializerOption attr : attrOptionList) {
            attrArray.add(attrMetaSerializer.serializeListAttrMeta(attr.getAttr()));
        }

        JsonArray objectArray = new JsonArray();
        result.add("objectList", objectArray);
        for (MlDynamicEntityImpl listObject : objectList) {
            JsonObject jsonListObject = serializeObjectWithAttrOptions(listObject, attrOptionList);
            objectArray.add(jsonListObject);
        }
        return result;
    }


    public JsonObject serializeObjectList(Collection<? extends MlDynamicEntityImpl> objectList, Collection<MlAttr> attrList, boolean withLinkAttrValues) {
        JsonObject result = new JsonObject();
        JsonArray attrArray = new JsonArray();
        result.add("attrList", attrArray);
        for (MlAttr attr : attrList) {
            attrArray.add(attrMetaSerializer.serializeListAttrMeta(attr));
        }

        JsonArray objectArray = new JsonArray();
        result.add("objectList", objectArray);
        for (MlDynamicEntityImpl listObject : objectList) {
            if (!withLinkAttrValues) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.add("title", new JsonPrimitive(listObject.getTitle()));
                objectArray.add(jsonObject);
            } else {
                JsonObject jsonListObject = serializeObject(listObject, attrList, false);
                objectArray.add(jsonListObject);
            }
        }
        return result;
    }


    private JsonElement serializeStringAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        if (mlDynamicEntity.get(mlAttr.getEntityFieldName()) == null) {
            return JsonNull.INSTANCE;
        }
        return new JsonPrimitive((String) mlDynamicEntity.get(mlAttr.getEntityFieldName()));
    }

    private JsonElement serializeLongAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        if (mlDynamicEntity.get(mlAttr.getEntityFieldName()) == null) {
            return JsonNull.INSTANCE;
        }
        return new JsonPrimitive((Long) mlDynamicEntity.get(mlAttr.getEntityFieldName()));
    }

    private JsonElement serializeDoubleAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        if (mlDynamicEntity.get(mlAttr.getEntityFieldName()) == null) {
            return JsonNull.INSTANCE;
        }
        return new JsonPrimitive((Double) mlDynamicEntity.get(mlAttr.getEntityFieldName()));
    }

    private JsonElement serializeBooleanAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        if (mlDynamicEntity.get(mlAttr.getEntityFieldName()) == null) {
            return JsonNull.INSTANCE;
        }
        return new JsonPrimitive((Boolean) mlDynamicEntity.get(mlAttr.getEntityFieldName()));
    }

    private JsonElement serializeLongLinkAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        String longLinkValue = mlAttr.getLongLinkValue();
        String[] longLinkPath = longLinkValue.split("->");
        Object o = mlDynamicEntity.get(longLinkPath[0]);
        //TODO check
        for (int i = 1; i < longLinkPath.length; i++) {
            if (o != null) {
                o = ((MlDynamicEntityImpl) o).get(longLinkPath[i]);
            }
        }
        if (o != null) {
            if (o instanceof MlDynamicEntityImpl) {
                return new JsonPrimitive(((MlDynamicEntityImpl) o).getTitle());
            } else {
                return new JsonPrimitive((o.toString()));
            }
        } else {
            return JsonNull.INSTANCE;
        }
    }

    private JsonElement serializeDateAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        if (mlDynamicEntity.get(mlAttr.getEntityFieldName()) == null) {
            return JsonNull.INSTANCE;
        }
        Date dateValue = mlDynamicEntity.get(mlAttr.getEntityFieldName());
        String dateFormatString = "dd.MM.yyyy";
        if (mlAttr.getFormat() != null && !mlAttr.getFormat().trim().isEmpty()) {
            dateFormatString = mlAttr.getFormat();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatString);

        return new JsonPrimitive(dateFormat.format(dateValue));
    }

    private JsonElement serializeManyToOneAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        if (mlDynamicEntity.get(mlAttr.getEntityFieldName()) == null) {
            return JsonNull.INSTANCE;
        }
        JsonObject result = new JsonObject();
        MlDynamicEntityImpl linkObject = mlDynamicEntity.get(mlAttr.getEntityFieldName());
        result.add("title", new JsonPrimitive(linkObject.getTitle()));
        result.add("objectId", new JsonPrimitive(linkObject.getOUID()));
        result.add("mlClass", new JsonPrimitive(linkObject.getInstanceMlClass().getEntityName()));
        return result;
    }

    private JsonElement serializeEnumAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        if (mlDynamicEntity.get(mlAttr.getEntityFieldName()) == null) {
            return JsonNull.INSTANCE;
        }
        MlAttr enumAttr = mlAttr;
        if (mlAttr.getMlClass().getParent() != null) {
            enumAttr = findParentAttr(mlAttr.getMlClass(), mlAttr);
        }
        MlEnum mlEnum = enumHolder.getEnum(enumAttr, mlDynamicEntity.get(mlAttr.getEntityFieldName()));
        JsonObject result = new JsonObject();
        result.add("code", new JsonPrimitive(mlEnum.getCode()));
        result.add("title", new JsonPrimitive(mlEnum.getTitle()));
        return result;
    }

    private MlAttr findParentAttr(MlClass mlClass, MlAttr mlAttr) {
        MlAttr result = mlClass.getParent().getAttr(mlAttr.getEntityFieldName());
        if (result == null) {
            result = mlAttr;
        }
        if (mlClass.getParent().getParent() != null) {
            result = findParentAttr(mlClass.getParent(), result);
        }
        return result;
    }

    private JsonElement serializeOneToManyAttrWithAttrOptionsInsecure(MlDynamicEntityImpl mlDynamicEntity, AttrSerializerOption attrSerializerOption) {
        if (attrSerializerOption.isFullSerializeLinkAttr()) {
            return serializeOneToManyAttr(mlDynamicEntity, attrSerializerOption.getAttr(), true);
        } else if (attrSerializerOption.getSerializeLinkAttrsOption() == null) {
            return serializeOneToManyAttr(mlDynamicEntity, attrSerializerOption.getAttr(), false);
        } else {
           // MlClass linkClass = metaDataHolder.getMlClassByName(attrSerializerOption.getAttr().getLinkAttr().getMlClass().getEntityName());
            Collection<MlDynamicEntityImpl> attrValueObjectList = mlDynamicEntity.get(attrSerializerOption.getAttr().getEntityFieldName());
            if (attrValueObjectList == null) {
                attrValueObjectList = new ArrayList<>();
            }
           // List<MlAttr> mlAttrs = linkClass.getInListAttrList();
           // mlAttrs = accessService.checkAccessAttrs(mlAttrs, MlAttrAccessType.SHOW);
            return serializeObjectListWithAttrOptions(attrValueObjectList, attrSerializerOption.getSerializeLinkAttrsOption());
        }
    }

    private JsonElement serializeOneToManyAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr, boolean withLinkAttrValues) {
        MlClass linkClass = metaDataHolder.getMlClassByName(mlAttr.getLinkAttr().getMlClass().getEntityName());
        Collection<MlDynamicEntityImpl> attrValueObjectList = mlDynamicEntity.get(mlAttr.getEntityFieldName());
        if (attrValueObjectList == null) {
            attrValueObjectList = new ArrayList<>();
        }
        List<MlAttr> mlAttrs = linkClass.getInListAttrList();
        mlAttrs = accessService.checkAccessAttrs(mlAttrs, MlAttrAccessType.SHOW);
        return serializeObjectList(attrValueObjectList, mlAttrs, withLinkAttrValues);
    }

    private JsonElement serializeManyToManyAttrWithAttrOptions(MlDynamicEntityImpl mlDynamicEntity, AttrSerializerOption attrSerializerOption) {
        if (attrSerializerOption.isFullSerializeLinkAttr()) {
            return serializeOneToManyAttr(mlDynamicEntity, attrSerializerOption.getAttr(), true);
        } else if (attrSerializerOption.getSerializeLinkAttrsOption() == null) {
            return serializeOneToManyAttr(mlDynamicEntity, attrSerializerOption.getAttr(), false);
        } else {
            //MlClass linkClass = metaDataHolder.getMlClassByName(attrSerializerOption.getAttr().getLinkClass().getEntityName());
            Collection<MlDynamicEntityImpl> attrValueObjectList = mlDynamicEntity.get(attrSerializerOption.getAttr().getEntityFieldName());
            //атрибуть родителя возвращаюся в виде null, поэтому создаем пустой список
            if (attrValueObjectList == null) {
                attrValueObjectList = new ArrayList<>();
            }
//            List<MlAttr> mlAttrs = linkClass.getInListAttrList();
//            mlAttrs = accessService.checkAccessAttrs(mlAttrs, MlAttrAccessType.SHOW);

            return serializeObjectListWithAttrOptions(attrValueObjectList, attrSerializerOption.getSerializeLinkAttrsOption());
        }
    }

    private JsonElement serializeManyToManyAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr, boolean withLinkAttrValues) {
        MlClass linkClass = metaDataHolder.getMlClassByName(mlAttr.getLinkClass().getEntityName());
        Collection<MlDynamicEntityImpl> attrValueObjectList = mlDynamicEntity.get(mlAttr.getEntityFieldName());
        //атрибуть родителя возвращаюся в виде null, поэтому создаем пустой список
        if (attrValueObjectList == null) {
            attrValueObjectList = new ArrayList<>();
        }
        List<MlAttr> mlAttrs = linkClass.getInListAttrList();
        mlAttrs = accessService.checkAccessAttrs(mlAttrs, MlAttrAccessType.SHOW);
        return serializeObjectList(attrValueObjectList, mlAttrs, withLinkAttrValues);
    }

    private JsonElement serializeHeteroLinkAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        //  MlClass linkClass = (MlClass) MetaDataHelper.getMlClassByName(mlAttr.getLinkClass().getEntityName());
        List<MlHeteroLink> attrValueObjectList = mlDynamicEntity.get(mlAttr.getEntityFieldName());
        MlHeteroLinkService heteroLinkService = GuiceConfigSingleton.inject(MlHeteroLinkService.class);
        //атрибуть родителя возвращаюся в виде null, поэтому создаем пустой список
        List<MlAttr> mlAttrs = new ArrayList<MlAttr>();
        JsonObject result = new JsonObject();
        JsonArray jsonElements = new JsonArray();
        if (attrValueObjectList == null) {
            attrValueObjectList = new ArrayList<>();
        } else if (attrValueObjectList.size() > 0) {
            for (MlHeteroLink link : attrValueObjectList) {
                JsonObject jsonObject = new JsonObject();
                MlDynamicEntityImpl entity = heteroLinkService.getObjectHeteroLink(link);
                jsonObject.add("title", new JsonPrimitive(entity.getTitle()));
                jsonObject.add("objectId", new JsonPrimitive(link.getStrId()));
                jsonObject.add("mlClass", new JsonPrimitive(link.getClassName()));
                jsonElements.add(jsonObject);
            }
        }

        result.add("objectList", jsonElements);
        return result;
    }

    private JsonElement serializeFileAttr(MlDynamicEntityImpl mlDynamicEntity, MlAttr mlAttr) {
        String filenameField = mlAttr.getEntityFieldName() + "_filename";
        if (mlDynamicEntity.get(filenameField) == null) {
            return JsonNull.INSTANCE;
        }
        JsonObject result = new JsonObject();
        result.add("title", new JsonPrimitive((String) mlDynamicEntity.get(filenameField)));
        result.add("objectId", new JsonPrimitive(mlDynamicEntity.getOUID()));
        result.add("entityFieldName", new JsonPrimitive(mlAttr.getEntityFieldName()));
        return result;
    }


}
