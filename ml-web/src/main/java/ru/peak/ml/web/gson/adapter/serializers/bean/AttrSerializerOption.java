package ru.peak.ml.web.gson.adapter.serializers.bean;

import ru.peak.ml.core.model.system.MlAttr;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class AttrSerializerOption {
    MlAttr attr;
    List<AttrSerializerOption> serializeLinkAttrsOption = null;
    boolean fullSerializeLinkAttr = false;

    public AttrSerializerOption(MlAttr attr) {
        this.attr = attr;
    }

    public MlAttr getAttr() {
        return attr;
    }

    public void setAttr(MlAttr attr) {
        this.attr = attr;
    }

    public List<AttrSerializerOption> getSerializeLinkAttrsOption() {
        return serializeLinkAttrsOption;
    }

    public void setSerializeLinkAttrsOption(List<AttrSerializerOption> serializeLinkAttrsOption) {
        this.serializeLinkAttrsOption = serializeLinkAttrsOption;
    }

    public boolean isFullSerializeLinkAttr() {
        return fullSerializeLinkAttr;
    }

    public void setFullSerializeLinkAttr(boolean fullSerializeLinkAttr) {
        this.fullSerializeLinkAttr = fullSerializeLinkAttr;
    }

    public AttrSerializerOption addAttrOption(MlAttr attr){

        if (serializeLinkAttrsOption == null){
            serializeLinkAttrsOption = new ArrayList<>();
        }

        AttrSerializerOption newOption = new AttrSerializerOption(attr);
        serializeLinkAttrsOption.add(newOption);
        return this;
    }


}
