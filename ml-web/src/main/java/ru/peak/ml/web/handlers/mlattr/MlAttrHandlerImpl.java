package ru.peak.ml.web.handlers.mlattr;

import ddl.creator.DDLGenerator;
import org.eclipse.persistence.internal.descriptors.changetracking.AttributeChangeListener;
import org.eclipse.persistence.internal.sessions.ObjectChangeSet;
import org.eclipse.persistence.sessions.changesets.ChangeRecord;
import org.eclipse.persistence.sessions.changesets.DirectToFieldChangeRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.AttrType;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.handler.DefaultMlClassHandler;
import ru.peak.ml.core.handler.validator.AbstractValidator;
import ru.peak.ml.core.initializer.MlMetaDataInitializeService;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;

import javax.inject.Inject;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Хендлер на атрибуты
 */
public class MlAttrHandlerImpl extends DefaultMlClassHandler<MlAttr> {
    private static final Logger log = LoggerFactory.getLogger(MlAttrHandlerImpl.class);
    private static final String ORDER_COLUMN_SUFFIX = "_order";

    @Inject
    DDLGenerator ddlGenerator;

    @Inject
    MlMetaDataInitializeService metaDataInitializeService;
    //JPADynamicClassInitializerService jpaClassInitializer;

    @Inject
    CommonDao commonDao;

    private Boolean reinitFlag = false;

    @Override
    public void beforeCreate(MlAttr mlAttr) {
        AbstractValidator<MlAttr> validator = GuiceConfigSingleton.inject(MlAttrValidator.class);
        validator.validateBeforeCreate(mlAttr);
        MlClass MlClass = mlAttr.getMlClass();
        if (!mlAttr.isVirtual()) {
            switch (mlAttr.getFieldType()) {
                case HETERO_LINK:
                    String heteroTableName = MlAttr.HETERO_LINK_TABLE_PREFIX+"_"+mlAttr.getMlClass().getTableName()+"_"+mlAttr.getEntityFieldName();
                    String idFiledName = mlAttr.getMlClass().getTableName()+"_id";
                    String heteroFiledName = MlAttr.HETERO_LINK_FIELD_NAME;
                    ddlGenerator.createTable(heteroTableName);
                    ddlGenerator.addColumn(heteroTableName, idFiledName, Long.class);
                    ddlGenerator.addColumn(heteroTableName,heteroFiledName, Long.class);
                    if (mlAttr.isOrdered()) {
                        String orderField = heteroTableName + ORDER_COLUMN_SUFFIX;
                        ddlGenerator.addColumn(heteroTableName, orderField, Long.class);
                    }
                    break;
                case ONE_TO_MANY:
                    if (mlAttr.isOrdered()) {
                        String linkedTable = mlAttr.getLinkAttr().getMlClass().getTableName();
                        String orderField = mlAttr.getLinkAttr().getEntityFieldName() + ORDER_COLUMN_SUFFIX;
                        ddlGenerator.addColumn(linkedTable, orderField, Long.class);
                    }
                    break;
                case MANY_TO_MANY: // TODOЦентрализовать генерацию имени таблицы, т.к. оно используется при инициализации метаинформации о сущностях
                    String tableName = getMtoMTableName(mlAttr);

                    MlAttr mlId = mlAttr.getMlClass().getPrimaryKeyAttr();
                    String fieldM = getMtoMFieldM(mlAttr);

                    MlAttr mlLinkId = mlAttr.getLinkClass().getPrimaryKeyAttr();
                    String fieldN = getMtoMFieldN(mlAttr);

                    if (!ddlGenerator.tableExist(tableName)) {
                        ddlGenerator.createTable(tableName);
                        ddlGenerator.addColumn(tableName, fieldM, mlId.getFieldType().getaClass());
                        ddlGenerator.addColumn(tableName, fieldN, mlLinkId.getFieldType().getaClass());
                    }

                    addMtMOrder(mlAttr);
                    break;
                case FILE: {
                    ddlGenerator.addColumn(MlClass.getTableName(), mlAttr.getTableFieldName(), mlAttr.getFieldType().getaClass());
                    ddlGenerator.addColumn(MlClass.getTableName(), mlAttr.getTableFieldName() + "_filename", String.class);
                    if (MlClass.hasHistory()) {
                        addFieldInHistoryTable(mlAttr);
                    }
                }
                break;
                default:

                    String sequenceName = MlClass.getTableName() + "_" + mlAttr.getTableFieldName() + DDLGenerator.SEQUENCE_POSTFIX;
                    if (mlAttr.getAutoIncrement() && !ddlGenerator.sequenceExists(sequenceName)) {
                        ddlGenerator.createSequence(sequenceName);
                    }
                    ddlGenerator.addColumn(MlClass.getTableName(), mlAttr.getTableFieldName(), mlAttr.getFieldType().getaClass(), mlAttr.getDefaultSqlValue());
                    if (mlAttr.getPrimaryKey()) {
                        ddlGenerator.createPrimaryKey(MlClass.getTableName(), mlAttr.getTableFieldName());
                    }
            }
        }
        if (MlClass.hasHistory()) {
            addFieldInHistoryTable(mlAttr);
        }
        setViewPos(mlAttr);
    }

    private void setViewPos(MlAttr mlAttr) {
        if(mlAttr.getViewPos() == null || mlAttr.getViewPos()==0){
            List<Long> maxViewPos = commonDao.getResultList("select max(o.viewPos)+1 from MlAttr o where o.mlClass.id = "+mlAttr.getMlClass().getId(),Long.class);
            mlAttr.set(MlAttr.VIEW_POS,maxViewPos.get(0)==null?0:maxViewPos.get(0));
        }
    }

    private void addFieldInHistoryTable(MlAttr entity) {
        String historyTableName = entity.getMlClass().getTableName() + MlClass.HISTORY_TABLE_POSTFIX;
        switch (entity.getFieldType()) {
            case ONE_TO_MANY:
            case MANY_TO_MANY:
                ddlGenerator.addColumn(historyTableName, entity.getTableFieldName(), AttrType.STRING.getaClass());
                break;
            case FILE:
                ddlGenerator.addColumn(historyTableName, entity.getTableFieldName(), AttrType.FILE.getaClass());
                ddlGenerator.addColumn(historyTableName, entity.getTableFieldName() + "_filename", AttrType.STRING.getaClass());
                break;
            default:
                ddlGenerator.addColumn(historyTableName, entity.getTableFieldName(), entity.getFieldType().getaClass());
        }
    }

    private void validateNewAttr(MlAttr mlAttr) {
        if (mlAttr.getTableFieldName() == null || mlAttr.getTableFieldName().trim().isEmpty()) {
            throw new MlApplicationException("Необходимо заполнить Табличное имя атрибута");
        }
        if (mlAttr.getEntityFieldName() == null || mlAttr.getEntityFieldName().trim().isEmpty()) {
            throw new MlApplicationException("Необходимо заполнить Кодовое имя атрибута");
        }
    }

    @Override
    public void beforeUpdate(MlAttr mlAttr) {
        AttributeChangeListener changeListener = ((AttributeChangeListener) mlAttr._persistence_getPropertyChangeListener());
        ObjectChangeSet objectChangeSet = changeListener.getObjectChangeSet();

        if (objectChangeSet == null) {
            return;
        }
        AbstractValidator<MlAttr> validator = GuiceConfigSingleton.inject(MlAttrValidator.class);
        validator.validateBeforeUpdate(mlAttr, objectChangeSet);

        switch (mlAttr.getFieldType()) {
            case MANY_TO_MANY:
                addMtMOrder(mlAttr);
                break;
            default:

                for (ChangeRecord changeRecord : objectChangeSet.getChanges()) {
                    switch (changeRecord.getAttribute()) {
                        case MlAttr.PRIMARY_KEY:
                            if (mlAttr.getPrimaryKey()) {
                                String sequenceName = mlAttr.getMlClass().getTableName() + "_" + mlAttr.getTableFieldName() + DDLGenerator.SEQUENCE_POSTFIX;
                                if (mlAttr.getAutoIncrement() && !ddlGenerator.sequenceExists(sequenceName)) {
                                    ddlGenerator.createSequence(sequenceName);
                                }
                                ddlGenerator.createPrimaryKey(mlAttr.getMlClass().getTableName(), mlAttr.getTableFieldName());
                            }
                            reinitFlag = true;
                            break;
                        case MlAttr.ENTITY_FIELD_NAME:
                             if(mlAttr.getFieldType() == AttrType.HETERO_LINK){
                                 DirectToFieldChangeRecord record = (DirectToFieldChangeRecord) changeRecord;
                                 String oldHeteroTableName = MlAttr.HETERO_LINK_TABLE_PREFIX+"_"+mlAttr.getMlClass().getTableName()+"_"+record.getOldValue().toString();
                                 String newHeteroTableName = MlAttr.HETERO_LINK_TABLE_PREFIX+"_"+mlAttr.getMlClass().getTableName()+"_"+record.getNewValue().toString();
                                 ddlGenerator.renameTable(oldHeteroTableName,newHeteroTableName);
                             }
                        case MlAttr.LINK_ATTR:
                        case MlAttr.LINK_CLASS:
                        case MlAttr.AUTO_INCREMENT:
                            reinitFlag = true;
                            break;
                        case MlAttr.ML_CLASS:

                            break;
                        case MlAttr.DEFAULT_SQL_VALUE:
                            updateDefaultSqlValue(mlAttr);
                            break;
                        case MlAttr.TABLE_FIELD_NAME:
                            if (!mlAttr.isVirtual()) {
                                DirectToFieldChangeRecord record = (DirectToFieldChangeRecord) changeRecord;
                                ddlGenerator.renameColumn(mlAttr.getMlClass().getTableName(), record.getOldValue().toString(), record.getNewValue().toString());
                                if (mlAttr.getMlClass().hasHistory()) {
                                    renameFieldInHistoryTable(mlAttr, record.getOldValue().toString(), record.getNewValue().toString());
                                }
                                reinitFlag = true;
                            }
                            break;
                        case MlAttr.FILED_TYPE:
                            ddlGenerator.changeType(mlAttr.getMlClass().getTableName(), mlAttr.getTableFieldName(), mlAttr.getFieldType().getaClass());
                            if (mlAttr.getMlClass().hasHistory()) {
                                changeTypeFieldInHistoryTable(mlAttr);
                            }
                            reinitFlag = true;
                            break;
                        case MlAttr.ORDERED:
                            switch (mlAttr.getFieldType()) {
                                case HETERO_LINK:
                                    String heteroTableName = MlAttr.HETERO_LINK_TABLE_PREFIX+"_"+mlAttr.getMlClass().getTableName()+"_"+mlAttr.getEntityFieldName();
                                    String heteroOrderField = heteroTableName + ORDER_COLUMN_SUFFIX;
                                    ddlGenerator.addColumn(heteroTableName, heteroOrderField, Long.class);
                                    break;
                                case ONE_TO_MANY:
                                    DirectToFieldChangeRecord record = (DirectToFieldChangeRecord) changeRecord;
                                    String linkedTable = mlAttr.getLinkAttr().getMlClass().getTableName();
                                    String orderField = mlAttr.getLinkAttr().getEntityFieldName() + ORDER_COLUMN_SUFFIX;
                                    if ((Boolean) record.getNewValue()) {
                                        ddlGenerator.addColumn(linkedTable, orderField, Long.class);
                                    } else {
                                        ddlGenerator.dropColumn(linkedTable, orderField);
                                    }
                                    reinitFlag = true;
                                    break;
                                case MANY_TO_MANY:
                                    DirectToFieldChangeRecord recordMN = (DirectToFieldChangeRecord) changeRecord;
                                    if ((Boolean) recordMN.getNewValue()) {
                                        addMtMOrder(mlAttr);
                                    }
                                    break;
                            }
                            break;
                    }
                }
        }
    }

    private void updateDefaultSqlValue(MlAttr entity) {
        ddlGenerator.changeDefaultSQLValue(entity.getMlClass().getTableName(), entity.getTableFieldName(), entity.getDefaultSqlValue(), true);
    }

    private void changeTypeFieldInHistoryTable(MlAttr entity) {
        String historyTableName = entity.getMlClass().getTableName() + MlClass.HISTORY_TABLE_POSTFIX;
        ddlGenerator.changeType(historyTableName, entity.getTableFieldName(), entity.getFieldType().getaClass());
    }

    private void renameFieldInHistoryTable(MlAttr entity, String oldFieldName, String newFieldName) {
        String historyTableName = entity.getMlClass().getTableName() + MlClass.HISTORY_TABLE_POSTFIX;
        ddlGenerator.renameColumn(historyTableName, oldFieldName, newFieldName);
    }

    @Override
    public void beforeDelete(MlAttr entity) {
        AbstractValidator<MlAttr> validator = GuiceConfigSingleton.inject(MlAttrValidator.class);
        validator.validateBeforeDelete(entity);

        switch (entity.getFieldType()) {
            case ONE_TO_MANY:
                break;
            case MANY_TO_MANY:
                dropRelationTable(entity);
                break;
            case FILE:
                dropFile(entity);
                break;
            case HETERO_LINK:
                dropHeteroTable(entity);
                break;
            default:
                if (!entity.isVirtual()) {
                    ddlGenerator.dropColumn(entity.getMlClass().getTableName(), entity.getTableFieldName());
                    if (entity.getMlClass().hasHistory()) {
                        String historyTableName = entity.getMlClass().getTableName() + MlClass.HISTORY_TABLE_POSTFIX;
                        ddlGenerator.renameColumn(historyTableName, entity.getTableFieldName(), entity.getTableFieldName() + "_" + System.currentTimeMillis());
                    }
                }
        }
    }

    private void dropHeteroTable(MlAttr mlAttr) {
        String tableName = MlAttr.HETERO_LINK_TABLE_PREFIX+"_"+mlAttr.getMlClass().getTableName()+"_"+mlAttr.getEntityFieldName();
        ddlGenerator.dropTable(tableName);
    }

    private void dropFile(MlAttr entity) {
        ddlGenerator.dropColumn(entity.getMlClass().getTableName(), entity.getTableFieldName());
        ddlGenerator.dropColumn(entity.getMlClass().getTableName(), entity.getTableFieldName() + "_filename");
        if (entity.getMlClass().hasHistory()) {
            String historyTableName = entity.getMlClass().getTableName() + MlClass.HISTORY_TABLE_POSTFIX;
            ddlGenerator.renameColumn(historyTableName, entity.getTableFieldName(), entity.getTableFieldName() + "_" + System.currentTimeMillis());
            ddlGenerator.renameColumn(historyTableName, entity.getTableFieldName() + "_filename", entity.getTableFieldName() + "_filename" + "_" + System.currentTimeMillis());
        }
    }

    private void dropRelationTable(MlAttr mlAttr) {
        TypedQuery<MlAttr> query = commonDao.getTypedQueryWithoutSecurityCheck("select o from MlAttr o where o.linkClass.id = :thisClass and o.fieldType = 'MANY_TO_MANY' and o.mlClass.id = :relClass", MlAttr.class)
                .setParameter("thisClass", mlAttr.getMlClass().getId())
                .setParameter("relClass", mlAttr.getLinkClass().getId());
        if (query.getResultList().isEmpty()) {
            String tableName = "MN_";
            if (Collator.getInstance().compare(mlAttr.getMlClass().getTableName(), mlAttr.getLinkClass().getTableName()) < 0) {
                tableName += mlAttr.getMlClass().getTableName() + "_" + mlAttr.getLinkClass().getTableName();
            } else {
                tableName += mlAttr.getLinkClass().getTableName() + "_" + mlAttr.getMlClass().getTableName();
            }
            ddlGenerator.dropTable(tableName);
        }
    }

    @Override
    public void afterDelete(MlAttr entity) {
        MlClass mlClass = entity.getMlClass();
        mlClass.getAttrSet().remove(entity);
        reInitializeClass(entity);
    }


    @Override
    public void afterCreate(MlAttr entity) {
        MlClass mlClass = entity.getMlClass();
        mlClass.getAttrSet().add(entity);
        reInitializeClass(entity);


    }

    @Override
    public void afterUpdate(MlAttr entity, boolean runBeforeHandlers) {
        if (reinitFlag || !runBeforeHandlers) {
            reInitializeClass(entity);
        } else {
            // В jpa нет изменений просто обновляем мето описание
            try {
                MlMetaDataInitializeService mlMetaDataInitializeService = GuiceConfigSingleton.inject(MlMetaDataInitializeService.class);
                mlMetaDataInitializeService.updateMlClass(entity.getMlClass());
                for(MlClass child: getAllChild(entity.getMlClass())){
                    mlMetaDataInitializeService.updateMlClass(child);
                }
            } catch (ClassNotFoundException e) {
                throw new MlServerException("Ошибка обновления мета данных", e);
            }
        }
    }

    private void reInitializeClass(MlAttr mlAttr) {
        MlClass mlClass = mlAttr.getMlClass();
        try {
            initializeClass(mlClass);
        } catch (Exception e) {
            log.warn("Bad init. Try recovery state");
            mlClass.getAttrSet().remove(mlAttr);
            initializeClass(mlClass);
            log.warn("Recovery done");
            throw e;
        }

    }

    private void initializeClass(MlClass entity) {
        List<MlClass> mlClassList = new ArrayList<>();
        mlClassList.add(entity);
        mlClassList.addAll(getAllChild(entity));
        try {
            for (MlClass mlClass : mlClassList) {
                commonDao.detach(mlClass);
            }
            metaDataInitializeService.initializeClasses(mlClassList, false);
        } catch (ClassNotFoundException e) {
            log.debug("", e);
        }
    }

    private String getMNTableName(String sourсeTable, String destTable) {
        if (sourсeTable.compareTo(destTable) > 0) {
            return "MN_" + destTable + "_" + sourсeTable;
        } else {
            return "MN_" + sourсeTable + "_" + destTable;
        }
    }


    /**
     * Операция добавления колонок упорядочивания N to M
     */
    private void addMtMOrder(MlAttr mlAttr) {
        String tableName = getMtoMTableName(mlAttr);
        //Упорядочивание элементов списка, в независимости от того, будет ли использоваться, добавляем колонку
        MlAttr mlId = mlAttr.getMlClass().getPrimaryKeyAttr();
        String fieldM = getMtoMFieldM(mlAttr);
        String orderFieldM = fieldM.replace("_" + mlId.getEntityFieldName(), ORDER_COLUMN_SUFFIX);
        ddlGenerator.addColumnIfNotExists(tableName, orderFieldM, Long.class);

        MlAttr mlLinkId = mlAttr.getLinkClass().getPrimaryKeyAttr();
        String fieldN = getMtoMFieldN(mlAttr);
        String orderFieldN = fieldN.replace("_" + mlLinkId.getEntityFieldName(), ORDER_COLUMN_SUFFIX);
        ddlGenerator.addColumnIfNotExists(tableName, orderFieldN, Long.class);
        reinitFlag = true;

    }

    private String getMtoMTableName(MlAttr mlAttr) {
        String tableName;
        String tableM = mlAttr.getMlClass().getTableName();
        String tableN = mlAttr.getLinkClass().getTableName();
        if (mlAttr.getManyToManyTableName() != null && !mlAttr.getManyToManyTableName().equals("")) {
            tableName = mlAttr.getManyToManyTableName();
        } else {
            tableName = getMNTableName(tableM, tableN);
        }
        return tableName;
    }

    /**
     * Получить наименование поля M в таблице связи
     */
    private String getMtoMFieldM(MlAttr mlAttr) {
        MlAttr mlId = mlAttr.getMlClass().getPrimaryKeyAttr();
        String tableM = mlAttr.getMlClass().getTableName();
        String fieldM;
        if (mlAttr.getManyToManyFieldNameM() != null && !mlAttr.getManyToManyFieldNameM().equals("")) {
            fieldM = mlAttr.getManyToManyFieldNameM();
        } else {
            fieldM = tableM + "_" + mlId.getTableFieldName();
        }
        return fieldM;
    }

    /**
     * Получить наименование поля N в таблице связи
     */
    private String getMtoMFieldN(MlAttr mlAttr) {
        String tableN = mlAttr.getLinkClass().getTableName();
        MlAttr mlLinkId = mlAttr.getLinkClass().getPrimaryKeyAttr();
        String fieldN;
        if (mlAttr.getManyToManyFieldNameN() != null && !mlAttr.getManyToManyFieldNameN().equals("")) {
            fieldN = mlAttr.getManyToManyFieldNameN();
        } else {
            fieldN = tableN + "_" + mlLinkId.getTableFieldName();
        }
        return fieldN;
    }

    /**
     * Получить всех наследников класса
     *
     * @param entity класс
     * @return наследники
     */
    private Collection<MlClass> getAllChild(MlClass entity) {
        TypedQuery<MlClass> query = commonDao.getTypedQueryWithoutSecurityCheck("select o from MlClass o where o.parent.id = :classId ", MlClass.class)
                .setParameter("classId", entity.getId());
        return query.getResultList();
    }
}
