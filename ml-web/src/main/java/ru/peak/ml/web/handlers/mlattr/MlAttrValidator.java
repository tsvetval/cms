package ru.peak.ml.web.handlers.mlattr;

import com.google.inject.Inject;
import org.eclipse.persistence.internal.sessions.ObjectChangeSet;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.handler.validator.AbstractValidator;
import ru.peak.ml.web.handlers.mlattr.validator.MlAttrInheritanceValidator;
import ru.peak.ml.web.handlers.mlattr.validator.MlAttrNameValidator;
import ru.peak.ml.web.handlers.mlattr.validator.MlAttrTypeValidator;
import ru.peak.ml.web.handlers.mlattr.validator.MlAttrValueValidator;

/**
 */
public class MlAttrValidator extends AbstractValidator<MlAttr> {
    @Inject
    MlAttrNameValidator mlAttrNameValidator;
    @Inject
    MlAttrValueValidator mlAttrValueValidator;
    @Inject
    MlAttrTypeValidator mlAttrTypeValidator;
    @Inject
    MlAttrInheritanceValidator mlAttrInheritanceValidator;

    @Override
    public void validateBeforeCreate(MlAttr entity) {
        mlAttrNameValidator.validate(entity);
        mlAttrValueValidator.validate(entity);
    }

    @Override
    public void validateBeforeUpdate(MlAttr entity, ObjectChangeSet objectChangeSet) {
        mlAttrNameValidator.validate(entity);
        mlAttrTypeValidator.validate(entity, objectChangeSet);
        mlAttrInheritanceValidator.validateOnChange(entity, objectChangeSet);
        mlAttrValueValidator.validate(entity);
    }

    @Override
    public void validateBeforeDelete(MlAttr entity) {
        mlAttrInheritanceValidator.validateOnDelete(entity);
    }
}
