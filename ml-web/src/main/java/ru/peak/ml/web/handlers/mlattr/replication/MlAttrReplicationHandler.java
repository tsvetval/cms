package ru.peak.ml.web.handlers.mlattr.replication;

import ddl.creator.DDLGenerator;
import org.eclipse.persistence.dynamic.DynamicEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.AttrType;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.handler.MlReplicationHandler;
import ru.peak.ml.core.initializer.MlMetaDataInitializeService;
import ru.peak.ml.core.model.MLUID;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.web.handlers.mlclass.replication.MlClassMlReplicationHandler;

import javax.inject.Inject;
import java.text.Collator;
import java.util.ArrayList;
import java.util.List;

/**
 */
public class MlAttrReplicationHandler implements MlReplicationHandler {
    private static final Logger log = LoggerFactory.getLogger(MlClassMlReplicationHandler.class);
    @Inject
    DDLGenerator ddlGenerator;

    @Inject
    CommonDao commonDao;

    @Inject
    MlMetaDataInitializeService metaDataInitializeService;
    //JPADynamicClassInitializerService jpaClassInitializer;

    @Override
    public void beforeReplication(MLUID mluid) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    private MlAttr findPrimaryKeyAttr(MlClass clazz) {
        MlAttr MlAttr = clazz.getPrimaryKeyAttr();
        while (MlAttr == null) {
            MlClass parent = clazz.getParent();
            MlAttr = parent.getPrimaryKeyAttr();
        }
        return MlAttr;
    }

    @Override
    public void afterReplication(DynamicEntity dynamicEntity) {
        MlAttr entity = (MlAttr) dynamicEntity;
        log.debug(String.format("Handler MlAttrReplicationHandler start mlAttr.entityFieldName = %s", entity.getEntityFieldName()));
        MlClass mlClass = entity.getMlClass();
        switch (entity.getFieldType()) {
            case ONE_TO_MANY:
                break;
            case MANY_TO_MANY:
                String tableName = "MN_";
                if (Collator.getInstance().compare(entity.getMlClass().getTableName(), entity.getLinkClass().getTableName()) < 0) {
                    tableName += entity.getMlClass().getTableName() + "_" + entity.getLinkClass().getTableName();
                } else {
                    tableName += entity.getLinkClass().getTableName() + "_" + entity.getMlClass().getTableName();
                }

                if (!ddlGenerator.tableExist(tableName)) {
                    ddlGenerator.createTable(tableName);

                    MlClass clazz = entity.getMlClass();
                    commonDao.refresh(clazz);
                    MlAttr MlAttr = findPrimaryKeyAttr(clazz);
                    ddlGenerator.addColumn(tableName, clazz.getTableName() + "_" + MlAttr.getTableFieldName(), MlAttr.getFieldType().getaClass());
                    //commonDao.refresh((MlClass)entity.getLinkClass());
                    MlClass relation = entity.getLinkClass();
                    commonDao.refresh(relation);
                    MlAttr MlAttrLink = findPrimaryKeyAttr(relation);
                    ddlGenerator.addColumn(tableName, entity.getLinkClass().getTableName() + "_" + MlAttrLink.getTableFieldName(), MlAttrLink.getFieldType().getaClass());
                }

                break;
            case FILE: {
                if (!ddlGenerator.columnExist(mlClass.getTableName(), entity.getTableFieldName())) {
                    ddlGenerator.addColumn(mlClass.getTableName(), entity.getTableFieldName(), entity.getFieldType().getaClass());
                }
                if (!ddlGenerator.columnExist(mlClass.getTableName(), entity.getTableFieldName() + "_filename")) {
                    ddlGenerator.addColumn(mlClass.getTableName(), entity.getTableFieldName() + "_filename", String.class);
                }
                if (mlClass.hasHistory()) {
                    addFieldInHistoryTable(entity);
                }
            }
            break;
            default:

                if (entity.getAutoIncrement()) {
                    String sequenceName = mlClass.getTableName() + "_" + entity.getTableFieldName() + DDLGenerator.SEQUENCE_POSTFIX;
                    if (!ddlGenerator.sequenceExists(sequenceName)) {
                        ddlGenerator.createSequence(sequenceName);
                    }
                }
                if (!ddlGenerator.columnExist(mlClass.getTableName(), entity.getTableFieldName())) {
                    ddlGenerator.addColumn(mlClass.getTableName(), entity.getTableFieldName(), entity.getFieldType().getaClass());
                    if (entity.getPrimaryKey()) {
                        ddlGenerator.createPrimaryKey(mlClass.getTableName(), entity.getTableFieldName());
                    }
                }
        }
        if (mlClass.hasHistory()) {
            addFieldInHistoryTable(entity);
        }
        //reInitializeClass((MlClass) entity.getMlClass());
    }

    private void addFieldInHistoryTable(MlAttr entity) {
        String historyTableName = entity.getMlClass().getTableName() + MlClass.HISTORY_TABLE_POSTFIX;
        switch (entity.getFieldType()) {
            case ONE_TO_MANY:
            case MANY_TO_MANY:
                if (!ddlGenerator.columnExist(historyTableName, entity.getTableFieldName())) {
                    ddlGenerator.addColumn(historyTableName, entity.getTableFieldName(), AttrType.STRING.getaClass());
                }
                break;
            case FILE:
                if (!ddlGenerator.columnExist(historyTableName, entity.getTableFieldName())) {
                    ddlGenerator.addColumn(historyTableName, entity.getTableFieldName(), AttrType.FILE.getaClass());
                }
                if (!ddlGenerator.columnExist(historyTableName, entity.getTableFieldName() + "_filename")) {
                    ddlGenerator.addColumn(historyTableName, entity.getTableFieldName() + "_filename", AttrType.STRING.getaClass());
                }
                break;
            default:
                if (!ddlGenerator.columnExist(historyTableName, entity.getTableFieldName())) {
                    ddlGenerator.addColumn(historyTableName, entity.getTableFieldName(), entity.getFieldType().getaClass());
                }
        }
    }

    private void reInitializeClass(MlClass entity) {
        commonDao.refresh(entity);
        boolean doInitalize = false;
        for (MlAttr attr : entity.getAttrSet()) {
            if (attr.getPrimaryKey()) {
                doInitalize = true;
            }
        }
        if (doInitalize) {
            List<MlClass> mlClassList = new ArrayList<>();
            mlClassList.add(entity);
            try {
                metaDataInitializeService.initializeClasses(mlClassList, false);
            } catch (ClassNotFoundException e) {
                log.error("Meta initialization Error", e);
            }
        }
    }


}
