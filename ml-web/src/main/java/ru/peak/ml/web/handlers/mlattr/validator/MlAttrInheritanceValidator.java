package ru.peak.ml.web.handlers.mlattr.validator;

import org.eclipse.persistence.internal.sessions.ObjectChangeSet;
import org.eclipse.persistence.sessions.changesets.ChangeRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.dao.CommonDao;

/**
 */
public class MlAttrInheritanceValidator {
    private static final Logger log = LoggerFactory.getLogger(MlAttrInheritanceValidator.class);
    private static String notRenameRealDynamicEntity = "Атрибут %s не может быть изменен, он участвует в наследовании. Для его переименования удалите все классы наследники класса %s.";
    private static String notRemoveRealDynamicEntity = "Атрибут %s не может быть удален, он участвует в наследовании. Для его удаления удалите все классы наследники класса %s.";


    public void validateOnChange(MlAttr mlAttr, ObjectChangeSet changes) {
        if (changes == null) {
            return;
        }
        for (ChangeRecord changeRecord : changes.getChanges()) {
            switch (changeRecord.getAttribute()) {
                case MlAttr.ENTITY_FIELD_NAME:
                case MlAttr.TABLE_FIELD_NAME:
                    if ("realDynamicEntity".equals(changeRecord.getOldValue())) {
                        if (hasChildren(mlAttr.getMlClass())) {
                            log.error("Class " + mlAttr.getMlClass().getEntityName() + " has children. Field realDynamicEntity can not be renamed.");
                            throw new MlApplicationException(String.format(notRenameRealDynamicEntity,
                                    mlAttr.getDescription(), ((MlClass) mlAttr.getMlClass()).getDescription()));
                        }
                    }
                    break;
            }
        }
    }

    public void validateOnDelete(MlAttr mlAttr) {
        if ("realDynamicEntity".equals(mlAttr.getEntityFieldName()) ||
                "realDynamicEntity".equals(mlAttr.getTableFieldName())) {
            if (hasChildren(mlAttr.getMlClass())) {
                log.error("Class " + mlAttr.getMlClass().getEntityName() + " has children. Field realDynamicEntity can not be remove.");
                throw new MlApplicationException(String.format(notRemoveRealDynamicEntity,
                        mlAttr.getDescription(), ((MlClass) mlAttr.getMlClass()).getDescription()));
            }
        }
    }

    private boolean hasChildren(MlClass mlClass) {
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        Long childrenCount = (Long) commonDao.getQueryWithoutSecurityCheck("select count(1) from MlClass o where o.parent.id = :classId").
                setParameter("classId", mlClass.getId()).getSingleResult();
        if (childrenCount > 0) {
            return true;
        }
        return false;
    }
}
