package ru.peak.ml.web.handlers.mlattr.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.handler.validator.Validator;
import ru.peak.ml.core.model.system.MlAttr;

import javax.persistence.Query;
import java.util.List;
import java.util.regex.Pattern;

/**
 */
public class MlAttrNameValidator implements Validator<MlAttr> {
    private static final Logger log = LoggerFactory.getLogger(MlAttrNameValidator.class);
    private static String emptyMlClass = "Атрибут %s должен быть привзан к классу.";
    private static String badEntityName = "Кодовое имя может содержать буквы латинского алфавита, цифры и символ нижнего подчеркивания. Должно начинаться с буквы и содержать не менее двух символов.";
    private static String badTableName = "Табличное имя может содержать буквы латинского алфавита, цифры и символ нижнего подчеркивания. Должно начинаться с буквы и содержать не менее двух символов.";
    private static String notUniqueEntityFieldName = "Атрибут с таким кодовым именем уже есть в классе %s";
    private static String notUniqueTableFieldName = "Атрибут с таким табличным именем уже есть в классе %s";

    @Override
    public void validate(MlAttr mlAttr) {
        emptyMlClassCheck(mlAttr);
        entityNameCheck(mlAttr);
        tableNameCheck(mlAttr);
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        uniqueEntityNameCheck(mlAttr, commonDao);
        uniqueTableNameCheck(mlAttr, commonDao);
    }

    private void emptyMlClassCheck(MlAttr mlAttr) {
        if (mlAttr.getMlClass() == null) {
            log.error("Empty mlClass " + mlAttr.getEntityFieldName());
            throw new MlApplicationException(String.format(emptyMlClass, mlAttr.getDescription()));
        }
    }

    private void uniqueTableNameCheck(MlAttr mlAttr, CommonDao commonDao) {
        Query query = commonDao.getQueryWithoutSecurityCheck("select o from MlAttr o where o.tableFieldName = :tableFieldName and o.mlClass.id = :classId").
                setParameter("tableFieldName", mlAttr.getTableFieldName()).setParameter("classId", mlAttr.getMlClass().getId());
        List<MlAttr> mlAttrs = query.getResultList();
        if (mlAttrs.size() > 0) {
            for (MlAttr attr : mlAttrs) {
                if (!attr.getId().equals(mlAttr.getId())) {
                    log.error("tableFieldName not unique");
                    throw new MlApplicationException(String.format(notUniqueTableFieldName,  mlAttr.getMlClass().getDescription()));
                }
            }
        }
    }

    private void uniqueEntityNameCheck(MlAttr mlAttr, CommonDao commonDao) {
        Query query = commonDao.getQueryWithoutSecurityCheck("select o from MlAttr o where o.entityFieldName = :entityFieldName and o.mlClass.id = :classId").
                setParameter("entityFieldName", mlAttr.getEntityFieldName()).setParameter("classId", mlAttr.getMlClass().getId());
        List<MlAttr> mlAttrs = query.getResultList();
        if (mlAttrs.size() > 0) {
            for (MlAttr attr : mlAttrs) {
                if (!attr.getId().equals(mlAttr.getId())) {
                    log.error("entityFieldName not unique");
                    throw new MlApplicationException(String.format(notUniqueEntityFieldName,  mlAttr.getMlClass().getDescription()));
                }
            }
        }
    }

    private void tableNameCheck(MlAttr mlAttr) {
        if (mlAttr.getTableFieldName() != null && !mlAttr.getTableFieldName().equals("")) {
            latinSumbolCheck(mlAttr.getTableFieldName(), badTableName);
        }
    }

    private void entityNameCheck(MlAttr mlAttr) {
        latinSumbolCheck(mlAttr.getEntityFieldName(), badEntityName);
    }

    private void latinSumbolCheck(String name, String message) {
        String regexp = "^[a-zA-Z][a-zA-Z0-9_]+$";
        if (!Pattern.matches(regexp, name)) {
            log.error("Bad entity field name");
            throw new MlApplicationException(message);
        }
    }
}
