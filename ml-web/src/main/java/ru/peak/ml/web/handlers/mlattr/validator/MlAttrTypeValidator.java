package ru.peak.ml.web.handlers.mlattr.validator;

import org.eclipse.persistence.internal.sessions.DirectToFieldChangeRecord;
import org.eclipse.persistence.internal.sessions.ObjectChangeSet;
import org.eclipse.persistence.sessions.changesets.ChangeRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.AttrType;
import ru.peak.ml.core.model.system.MlAttr;

/**
 */
public class MlAttrTypeValidator {

    private static final Logger log = LoggerFactory.getLogger(MlAttrTypeValidator.class);
    private static final String wrongChangeType = "Невозможно сменить тип с %s на %s. Для смены типа надо удалить атрибут и создать новый с нужным типом.";
    private static final String changeClass = "Невозможно сменить класс у атрибута. Удалите атрибут и создайте новый атрибут у другого класса.";
    private static final String changeManyToManySettings = "Невозможно изменять поля \"Имя таблицы связи M-N(многие ко многим)\", \"Имя поля в таблице связи M-N(со стороны M)\",\"Имя поля в таблице связи M-N(со стороны N)\". Удалите атрибут и создайте новую связь.";

    public void validate(MlAttr mlAttr, ObjectChangeSet objectChangeSet) {
        changeTypeCheck(mlAttr, objectChangeSet);

    }

    private void changeTypeCheck(MlAttr mlAttr, ObjectChangeSet objectChangeSet) {
        if (objectChangeSet == null) {
            return;
        }
        for (ChangeRecord changeRecord : objectChangeSet.getChanges()) {
            switch (changeRecord.getAttribute()) {
                case MlAttr.FILED_TYPE:
                    validateChangeType(AttrType.valueOf(changeRecord.getOldValue().toString()),
                            AttrType.valueOf(((DirectToFieldChangeRecord) changeRecord).getNewValue().toString()));
                    break;
                case MlAttr.ML_CLASS:
                    log.error("Change mlClass");
                    throw new MlApplicationException(changeClass);
                case MlAttr.MANY_TO_MANY_TABLE_NAME:
                case MlAttr.MANY_TO_MANY_FIELD_NAME_M:
                case MlAttr.MANY_TO_MANY_FIELD_NAME_N:
                    log.error("Change manyToMany settings");
                    throw new MlApplicationException(changeManyToManySettings);
            }
        }
    }

    private void validateChangeType(AttrType oldType, AttrType newType) {
        switch (oldType) {
            case DOUBLE:
            case LONG:
                switch (newType) {
                    case TEXT:
                    case STRING:
                        return;
                    default:
                        log.error("Wrong change type " + oldType + " to " + newType);
                        throw new MlApplicationException(String.format(wrongChangeType, oldType.name(),newType.name()));
                }
            case STRING:
                switch (newType){
                    case TEXT:
                        return;
                }
            default:
                log.error("Wrong change type " + oldType + " to " + newType);
                throw new MlApplicationException(String.format(wrongChangeType, oldType.name(),newType.name()));
        }
    }
}
