package ru.peak.ml.web.handlers.mlattr.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;

/**
 */
public class MlAttrValueValidator {
    private static final Logger log = LoggerFactory.getLogger(MlAttrValueValidator.class);
    private static final String wrongLongLink = "Дальняя ссылка %s имеет неверный формат. Атрибут с кодовым именем %s отсутствует в классе %s";
    private static final String wrongLongLinkNotEnd = "Дальняя ссылка %s имеет неверный формат. Ссылка должна оканчиваться именем атрибута";


    public void validate(MlAttr mlAttr) {
        switch (mlAttr.getFieldType()) {
            case LONG_LINK:
                longLinkCheck(mlAttr);
                break;
        }
    }

    private void longLinkCheck(MlAttr mlAttr) {
        String link = mlAttr.getLongLinkValue();
        if (link.endsWith("->")) {
            log.error("Wrong longLink " + link);
            throw new MlApplicationException(String.format(wrongLongLinkNotEnd, link));
        }
        String[] fields = link.split("->");
        MlClass mlClass = mlAttr.getMlClass();
        for (int i = 0; i < fields.length; i++) {
            String field = fields[i];

            MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
            MlAttr attr = metaDataHolder.getAttr(mlClass, field);
            if (attr == null) {
                log.error("Wrong longLink " + link + " attribute " + field + " not exist in class " + mlClass.getDescription());
                throw new MlApplicationException(String.format(wrongLongLink, link, field, mlClass.getDescription()));
            } else {
                if ((i + 1) != fields.length) {
                    mlClass = attr.getLinkClass();
                }
            }
        }
    }
}
