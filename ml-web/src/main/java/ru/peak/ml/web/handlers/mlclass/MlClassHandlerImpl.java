package ru.peak.ml.web.handlers.mlclass;

import ddl.creator.DDLGenerator;
import org.eclipse.persistence.internal.descriptors.changetracking.AttributeChangeListener;
import org.eclipse.persistence.internal.sessions.ObjectChangeSet;
import org.eclipse.persistence.sessions.changesets.ChangeRecord;
import org.eclipse.persistence.sessions.changesets.DirectToFieldChangeRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.AttrType;
import ru.ml.core.common.ReplicationType;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.handler.DefaultMlClassHandler;
import ru.peak.ml.core.handler.validator.AbstractValidator;
import ru.peak.ml.core.initializer.MlMetaDataInitializeService;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;

import javax.inject.Inject;
import javax.persistence.Query;
import java.sql.Timestamp;
import java.text.Collator;
import java.util.*;

/**
 * Хендлер на класс
 */
public class MlClassHandlerImpl extends DefaultMlClassHandler<MlClass> {
    private static final Logger log = LoggerFactory.getLogger(MlClassHandlerImpl.class);

    @Inject
    DDLGenerator ddlGenerator;

    @Inject
    CommonDao commonDao;

    @Inject
    MlMetaDataInitializeService metaDataInitializeService;
    //JPADynamicClassInitializerService jpaClassInitializer;

    private boolean reinitFlag = false;
    private ObjectChangeSet objectChangeSet;

    /**
     * Хендлер перед созданием создает идентификатор и если нужно историю
     *
     * @param entity
     */
    @Override
    public void beforeCreate(MlClass entity) {
        AbstractValidator validator = GuiceConfigSingleton.inject(MlClassValidator.class);
        validator.validateBeforeCreate(entity);
        ddlGenerator.createTable(entity.getTableName());
        if (entity.hasHistory()) {
            createHistoryTable(entity.getTableName());
        }
        MlAttr attr = (MlAttr) commonDao.createNewEntity(MlAttr.class);
        attr.setEntityFieldName("id");
        attr.setMlClass(entity);
        attr.setTableFieldName("id");
        attr.setDescription("Идентификатор");
        attr.setSystemField(true);
        attr.setPrimaryKey(true);
        attr.setInList(false);
        attr.setInForm(false);
        attr.setVirtual(false);
        attr.setAutoIncrement(true);
        attr.setFieldType(AttrType.LONG);
        attr.setReplicationType(ReplicationType.NOT_REPLICATE);
        attr.setGUID(UUID.randomUUID().toString());
        attr.setLastChange(new Date());
        commonDao.persistWithSecurityCheck(attr, true, false);
        entity.getAttrSet().add(attr);
    }

    private void createHistoryTable(String tableName) {
        String historyTableName = tableName + MlClass.HISTORY_TABLE_POSTFIX;
        ddlGenerator.createTable(historyTableName);
        ddlGenerator.addColumn(historyTableName, MlAttr.HISTORY_START_DATE_FIELD_NAME, Timestamp.class);
        ddlGenerator.addColumn(historyTableName, MlAttr.HISTORY_END_DATE_FIELD_NAME, Timestamp.class);
        ddlGenerator.addColumn(historyTableName, MlAttr.HISTORY_USER_LOGIN_FIELD_NAME, String.class);
        ddlGenerator.addColumn(historyTableName, MlAttr.HISTORY_ACTION_TYPE_FIELD_NAME, String.class);
    }

    /**
     * Хендлер перед обновлением, меняет таблицу истории
     *
     * @param entity
     */
    @Override
    public void beforeUpdate(MlClass entity) {
        AttributeChangeListener changeListener = ((AttributeChangeListener) entity._persistence_getPropertyChangeListener());
        objectChangeSet = changeListener.getObjectChangeSet();
        if (objectChangeSet == null) {
            return;
        }
        AbstractValidator validator = GuiceConfigSingleton.inject(MlClassValidator.class);
        validator.validateBeforeUpdate(entity, objectChangeSet);
        for (ChangeRecord changeRecord : objectChangeSet.getChanges()) {
            switch (changeRecord.getAttribute()) {
                case MlClass.ENTITY_NAME:
                case MlClass.IS_CACHEABLE:
                case MlClass.JAVA_CLASS:
                case MlClass.PARENT:
                    reinitFlag = true;
                    break;
                case MlClass.TABLE_NAME:
                    DirectToFieldChangeRecord record = (DirectToFieldChangeRecord) changeRecord;
                    dropConstraintPK(entity, record.getOldValue().toString(), record.getNewValue().toString());
                    ddlGenerator.renameTable(record.getOldValue().toString(), record.getNewValue().toString());
                    renameSequence(entity, record.getOldValue().toString(), record.getNewValue().toString());
                    createConstraintPK(entity, record.getNewValue().toString());
                    renameManyToMany(entity, record.getOldValue().toString(), record.getNewValue().toString());
                    if (entity.hasHistory()) {
                        renameHstoryTable(record.getOldValue().toString(), record.getNewValue().toString());
                    }
                    renameHeteroTables(entity, record.getOldValue().toString(), record.getNewValue().toString());
                    reinitFlag = true;
                    break;
                case MlClass.HAS_HISTORY:
                    DirectToFieldChangeRecord historyRecord = (DirectToFieldChangeRecord) changeRecord;
                    if ((Boolean) historyRecord.getNewValue()) {
                        String tableName = entity.getTableName() + MlClass.HISTORY_TABLE_POSTFIX;         //todo работу с историей вынести в отдельный класс
                        if (!ddlGenerator.tableExist(tableName)) {
                            ddlGenerator.createTable(tableName);
                            reinitFlag = true;
                        }
                        for (MlAttr mlAttr : entity.getAttrSet()) {
                            String fieldName = mlAttr.getTableFieldName();
                            if (!ddlGenerator.columnExist(tableName, fieldName)) {
                                switch (mlAttr.getFieldType()) {
                                    case ONE_TO_MANY:
                                    case MANY_TO_MANY:
                                        ddlGenerator.addColumn(tableName, fieldName, AttrType.STRING.getaClass());
                                        reinitFlag = true;
                                        break;
                                    case FILE:
                                        ddlGenerator.addColumn(tableName, fieldName, AttrType.FILE.getaClass());
                                        ddlGenerator.addColumn(tableName, fieldName + "_filename", AttrType.STRING.getaClass());
                                        reinitFlag = true;
                                        break;
                                    default:
                                        ddlGenerator.addColumn(tableName, fieldName, mlAttr.getFieldType().getaClass());
                                        reinitFlag = true;
                                }
                            }
                        }
                        if (!ddlGenerator.columnExist(tableName, MlAttr.HISTORY_START_DATE_FIELD_NAME)) {
                            ddlGenerator.addColumn(tableName, MlAttr.HISTORY_START_DATE_FIELD_NAME, Timestamp.class);
                            reinitFlag = true;
                        }
                        if (!ddlGenerator.columnExist(tableName, MlAttr.HISTORY_END_DATE_FIELD_NAME)) {
                            ddlGenerator.addColumn(tableName, MlAttr.HISTORY_END_DATE_FIELD_NAME, Timestamp.class);
                            reinitFlag = true;
                        }
                        if (!ddlGenerator.columnExist(tableName, MlAttr.HISTORY_USER_LOGIN_FIELD_NAME)) {
                            ddlGenerator.addColumn(tableName, MlAttr.HISTORY_USER_LOGIN_FIELD_NAME, String.class);
                            reinitFlag = true;
                        }
                        if (!ddlGenerator.columnExist(tableName, MlAttr.HISTORY_ACTION_TYPE_FIELD_NAME)) {
                            ddlGenerator.addColumn(tableName, MlAttr.HISTORY_ACTION_TYPE_FIELD_NAME, String.class);
                            reinitFlag = true;
                        }
                    } else {
                        if (ddlGenerator.tableExist(entity.getTableName() + MlClass.HISTORY_TABLE_POSTFIX)) {
                            ddlGenerator.dropTable(entity.getTableName() + MlClass.HISTORY_TABLE_POSTFIX);
                            reinitFlag = true;
                        }
                    }
                    break;
            }
        }
    }

    private void renameHeteroTables(MlClass entity, String oldTableName, String newTableName) {
        for (MlAttr mlAttr : entity.getAttrSet()) {
            if (mlAttr.getFieldType() == AttrType.HETERO_LINK) {
                String oldHeteroTableName = MlAttr.HETERO_LINK_TABLE_PREFIX + "_" + oldTableName + "_" + mlAttr.getEntityFieldName();
                String newHeteroTableName = MlAttr.HETERO_LINK_TABLE_PREFIX + "_" + newTableName + "_" + mlAttr.getEntityFieldName();
                ddlGenerator.renameTable(oldHeteroTableName, newHeteroTableName);
                String oldIdFiledName = oldTableName+"_id";
                String newIdFiledName = newTableName+"_id";
                ddlGenerator.renameColumn(newHeteroTableName,oldIdFiledName,newIdFiledName);
            }
        }
    }

    private void renameManyToMany(MlClass entity, String oldTableName, String newTableName) {
        Query query = commonDao.getQueryWithoutSecurityCheck("select o from MlAttr o where o.linkClass.id = :idClass and o.fieldType = 'MANY_TO_MANY'")
                .setParameter("idClass", entity.getId());
        List<MlAttr> mlAttrs = query.getResultList();
        for (MlAttr attr : mlAttrs) {
            renameMN(attr, oldTableName, newTableName, entity);
        }
        for (MlAttr attr : entity.getAttrSet()) {
            if (attr.getFieldType() == AttrType.MANY_TO_MANY) {
                renameMN(attr, oldTableName, newTableName, entity);
            }
        }
    }

    private void renameMN(MlAttr attr, String oldTableName, String newTableName, MlClass mlClass) {
        String oldMNTableName = "MN_";
        if (Collator.getInstance().compare(attr.getMlClass().getTableName(), oldTableName) < 0) {
            oldMNTableName += attr.getMlClass().getTableName() + "_" + oldTableName;
        } else {
            oldMNTableName += oldTableName + "_" + attr.getMlClass().getTableName();
        }
        if (attr.getManyToManyTableName() != null && !attr.getManyToManyTableName().equals("")) {
            return;
        }
        String newMNTableName = "MN_";
        if (Collator.getInstance().compare(attr.getMlClass().getTableName(), newTableName) < 0) {
            newMNTableName += attr.getMlClass().getTableName() + "_" + newTableName;
        } else {
            newMNTableName += newTableName + "_" + attr.getMlClass().getTableName();
        }

        if (ddlGenerator.tableExist(oldMNTableName)) {
            ddlGenerator.renameTable(oldMNTableName, newMNTableName);
            String oldFieldName = oldTableName + "_" + mlClass.getPrimaryKeyAttr().getTableFieldName();
            String newFieldName = newTableName + "_" + mlClass.getPrimaryKeyAttr().getTableFieldName();
            ddlGenerator.renameColumn(newMNTableName, oldFieldName, newFieldName);

        }
    }

    private void dropConstraintPK(MlClass mlClass, String oldTableName, String newTableName) {
        ddlGenerator.dropPrimaryKey(oldTableName);
    }

    private void createConstraintPK(MlClass mlClass, String newTableName) {
        ddlGenerator.createPrimaryKey(newTableName, mlClass.getPrimaryKeyAttr().getTableFieldName());
    }

    private void renameSequence(MlClass entity, String oldTableName, String newTableName) {
        for (MlAttr attr : entity.getAttrSet()) {
            if (attr.getPrimaryKey()) {
                String oldSeqName = oldTableName + "_" + attr.getTableFieldName() + DDLGenerator.SEQUENCE_POSTFIX;
                String newSeqName = newTableName + "_" + attr.getTableFieldName() + DDLGenerator.SEQUENCE_POSTFIX;
                ddlGenerator.renameSequence(oldSeqName, newSeqName);
                return;
            }
        }
        throw new RuntimeException("Entity not have primary key");
    }

    private void renameHstoryTable(String oldTableName, String newTableName) {
        String oldHistoryTableName = oldTableName + MlClass.HISTORY_TABLE_POSTFIX;
        String newHistoryTableName = newTableName + MlClass.HISTORY_TABLE_POSTFIX;
        ddlGenerator.renameTable(oldHistoryTableName, newHistoryTableName);
    }

    @Override
    public void beforeDelete(MlClass entity) {
        AbstractValidator validator = GuiceConfigSingleton.inject(MlClassValidator.class);
        validator.validateBeforeDelete(entity);
        if (entity.hasHistory()) {
            ddlGenerator.renameTable(entity.getTableName() + MlClass.HISTORY_TABLE_POSTFIX, entity.getTableName() + MlClass.HISTORY_TABLE_POSTFIX + "_" + System.currentTimeMillis());
        }
        dropHeteroTables(entity);
        ddlGenerator.dropTable(entity.getTableName());
    }

    private void dropHeteroTables(MlClass entity) {
        for (MlAttr mlAttr : entity.getAttrSet()) {
            if (mlAttr.getFieldType() == AttrType.HETERO_LINK) {
                String heteroTableName = MlAttr.HETERO_LINK_TABLE_PREFIX + "_" + entity.getTableName() + "_" + mlAttr.getEntityFieldName();
                ddlGenerator.dropTable(heteroTableName);
            }
        }
    }

    @Override
    public void afterDelete(MlClass entity) {
        for (MlAttr attr : entity.getAttrSet()) {
            if(attr.getMlClass().getId().equals(entity.getId())){
                commonDao.removeWithSecurityCheck(attr, false, false);
            }
        }
        reInitializeClass(entity);
    }

    @Override
    public void afterCreate(MlClass entity) {
        reInitializeClass(entity);
/*
        try {
            MlMetaDataInitializeService mlMetaDataInitializeService = GuiceConfigSingleton.inject(MlMetaDataInitializeService.class);
            mlMetaDataInitializeService.updateMlClass(entity);
        } catch (ClassNotFoundException e) {
            throw new MlServerException("Ошибка обновления мета данных", e);
        }
*/
    }

    @Override
    public void afterUpdate(MlClass entity, boolean runBeforeHandlers) {
        if (objectChangeSet == null) {
            return;
        }
        for (ChangeRecord changeRecord : objectChangeSet.getChanges()) {
            switch (changeRecord.getAttribute()) {
                case MlClass.PARENT:
                    //jpaClassInitializer.deleteClassWriter(entity);
                    List<MlClass> mlClasses = new ArrayList<>();
                    if (entity.getParent() == null) {
                        mlClasses = getAllParent((MlClass) changeRecord.getOldValue());
                    } /*else {
                        mlClasses = getAllParent((MlClass) entity.getParent());
                    }*/
                    mlClasses.add(entity);
                    reInitializeClass(mlClasses);
                    return;
            }
        }

        if (reinitFlag || !runBeforeHandlers) {
            reInitializeClass(entity);
        } else {
            // В jpa нет изменений просто обновляем мето описание
            try {
                MlMetaDataInitializeService mlMetaDataInitializeService = GuiceConfigSingleton.inject(MlMetaDataInitializeService.class);
                mlMetaDataInitializeService.updateMlClass(entity);
            } catch (ClassNotFoundException e) {
                throw new MlServerException("Ошибка обновления мета данных", e);
            }
        }
    }

    private void reInitializeClass(MlClass entity) {
        List<MlClass> mlClassList = getAllRelationClassess(entity);
        reInitializeClass(mlClassList);
    }

    private List<MlClass> getAllRelationClassess(MlClass entity) {
        List<MlClass> mlClassList = new ArrayList<>();
        mlClassList.addAll(getAllParent(entity));
        mlClassList.addAll(getAllChild(mlClassList));
        mlClassList.addAll(getAllManyTo(mlClassList));
        mlClassList.addAll(getAllOneTo(mlClassList));
        return mlClassList;
    }


    private List<MlClass> getAllParent(MlClass entity) {
        List<MlClass> result = new ArrayList<>();
        result.add(entity);
        MlClass parent = entity;
        do {
            parent = parent.getParent();
            if (parent != null) {
                result.add(parent);
            }
        } while (parent != null);
        return result;
    }

    private Collection<? extends MlClass> getAllChild(List<MlClass> entities) {
        List<MlClass> results = new ArrayList<>();
        for(MlClass entity: entities){
            Query query = commonDao.getQueryWithoutSecurityCheck("select o from MlClass o where o.parent.id = :classId ").setParameter("classId", entity.getId());
            results.addAll(query.getResultList());
        }
        return results;
    }

    private Collection<? extends MlClass> getAllOneTo(List<MlClass> entities) {
        List<MlClass> results = new ArrayList<>();
        for(MlClass entity: entities){
            Query query = commonDao.getQueryWithoutSecurityCheck("select distinct o from MlClass o where o.id in (select a.mlClass.id from MlAttr a where a.linkAttr.id in (select c.id from MlAttr c where c.mlClass.id = :classId ) )  ").setParameter("classId", entity.getId());
            results.addAll(query.getResultList());
        }
        return results;
    }

    private Collection<? extends MlClass> getAllManyTo(List<MlClass> entities) {
        Set<MlClass> results = new HashSet<>();
        for(MlClass entity: entities){
            Query query = commonDao.getQueryWithoutSecurityCheck("select distinct o from MlAttr o where o.linkClass.id = :classId ").setParameter("classId", entity.getId());
            List<MlAttr> mlAttrs = query.getResultList();
            for(MlAttr attr: mlAttrs){
                results.add(attr.getMlClass());
            }

        }
        return results;
    }


    private void reInitializeClass(List<MlClass> entities) {
        try {
            for (MlClass mlClass : entities) {
                commonDao.detach(mlClass);
            }
            metaDataInitializeService.initializeClasses(entities, false);
        } catch (ClassNotFoundException e) {
            log.error("", e);
        }
    }


}
