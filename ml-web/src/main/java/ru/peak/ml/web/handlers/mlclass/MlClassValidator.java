package ru.peak.ml.web.handlers.mlclass;

import com.google.inject.Inject;
import org.eclipse.persistence.internal.sessions.ObjectChangeSet;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.handler.validator.AbstractValidator;
import ru.peak.ml.web.handlers.mlclass.validator.*;

/**
 */
public class MlClassValidator extends AbstractValidator<MlClass> {
    @Inject
    MlClassClassesValidator mlClassClassesValidator;
    @Inject
    MlClassInheritanceValidator mlClassInheritanceValidator;
    @Inject
    MlClassAttrValidator mlClassAttrValidator;
    @Inject
    MlClassNameValidator mlClassNameValidator;
    @Inject
    MlClassDeleteValidator mlClassDeleteValidator;

    @Override
    public void validateBeforeCreate(MlClass entity) {
        mlClassClassesValidator.validate(entity);
        mlClassInheritanceValidator.validate(entity);
        mlClassAttrValidator.validate(entity);
        mlClassNameValidator.validate(entity);
    }

    @Override
    public void validateBeforeUpdate(MlClass entity, ObjectChangeSet objectChangeSet) {
        mlClassClassesValidator.validate(entity);
        mlClassInheritanceValidator.validate(entity);
        mlClassInheritanceValidator.validateOnUpdate(entity,objectChangeSet);
        mlClassAttrValidator.validate(entity);
        mlClassNameValidator.validate(entity);
        mlClassAttrValidator.validateUpdate(entity,objectChangeSet);
    }

    @Override
    public void validateBeforeDelete(MlClass entity) {
        mlClassDeleteValidator.validate(entity);
        mlClassInheritanceValidator.validateOnDelete(entity);
    }
}
