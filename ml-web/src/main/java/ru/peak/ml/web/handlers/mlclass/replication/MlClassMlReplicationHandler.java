package ru.peak.ml.web.handlers.mlclass.replication;

import ddl.creator.DDLGenerator;
import org.eclipse.persistence.dynamic.DynamicEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.model.MLUID;
import ru.peak.ml.core.handler.MlReplicationHandler;

import javax.inject.Inject;
import java.sql.Timestamp;

/**
 */
public class MlClassMlReplicationHandler implements MlReplicationHandler {
    private static final Logger log = LoggerFactory.getLogger(MlClassMlReplicationHandler.class);
    @Inject
    DDLGenerator ddlGenerator;

    @Inject
    CommonDao commonDao;

    @Override
    public void beforeReplication(MLUID mluid) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void afterReplication(DynamicEntity dynamicEntity) {
        MlClass mlClass = (MlClass) dynamicEntity;
        log.debug(String.format("Handler MlClassMlReplicationHandler start mlClass.entityName = %s", mlClass.getEntityName()));
        if (!ddlGenerator.tableExist(mlClass.getTableName())) {
            ddlGenerator.createTable(mlClass.getTableName());
        }
        if (mlClass.hasHistory()) {
            createHistoryTable(mlClass.getTableName());
        }
    }

    private void createHistoryTable(String tableName) {
        String historyTableName = tableName + MlClass.HISTORY_TABLE_POSTFIX;
        if (!ddlGenerator.tableExist(historyTableName)) {
            ddlGenerator.createTable(historyTableName);
            ddlGenerator.addColumn(historyTableName, MlAttr.HISTORY_START_DATE_FIELD_NAME, Timestamp.class);
            ddlGenerator.addColumn(historyTableName, MlAttr.HISTORY_END_DATE_FIELD_NAME, Timestamp.class);
            ddlGenerator.addColumn(historyTableName, MlAttr.HISTORY_USER_LOGIN_FIELD_NAME, String.class);
            ddlGenerator.addColumn(historyTableName, MlAttr.HISTORY_ACTION_TYPE_FIELD_NAME, String.class);
        }
    }

}
