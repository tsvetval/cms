package ru.peak.ml.web.handlers.mlclass.validator;

import org.eclipse.persistence.internal.sessions.ObjectChangeSet;
import org.eclipse.persistence.sessions.changesets.ChangeRecord;
import org.eclipse.persistence.sessions.changesets.CollectionChangeRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.AttrType;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.handler.validator.Validator;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 */
public class MlClassAttrValidator implements Validator<MlClass> {
    private static final Logger log = LoggerFactory.getLogger(MlClassAttrValidator.class);
    private static final String haveNotAttributeInDescription = "В поле \"Формат заголовка\" указан атрибут %s которого нет в классе.";
    private static final String deleteAttribute = "Невозможно отвязать атрибут от класса.";
    private static final String oneToNAttribute = "Невозможно указать атрибут типа 1-N (один ко многим) в поле \"Формат заголовка\"";

    @Override
    public void validate(MlClass mlClass) {
        if (mlClass.getTitleFormat() != null && !mlClass.getTitleFormat().equals("")) {
            String pattern = "\\$\\{(.+?)\\}";
            List<String> attrNameList = new ArrayList<>();
            Matcher m = Pattern.compile(pattern).matcher(mlClass.getTitleFormat());
            while (m.find()) {
                String attr = m.group().substring(2, m.group().length() - 1);
                attrNameList.add(attr);
            }
            for (String attrName : attrNameList) {
                if (attrName.contains(",")) {
                    attrName = attrName.substring(0, attrName.indexOf(","));
                }
                MetaDataHolder  metaDatHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);

                MlAttr mlAttr = metaDatHolder.getAttr(mlClass, attrName);

                if (mlAttr == null) {
                    log.error("Have not attribute " + attrName + " in " + mlClass.getEntityName() + " title format: " + mlClass.getTitleFormat());
                    throw new MlApplicationException(String.format(haveNotAttributeInDescription, attrName));
                } else if (mlAttr != null && mlAttr.getFieldType().equals(AttrType.ONE_TO_MANY)) {
                    log.error("Try to add 1-N attribute " + attrName + " in " + mlClass.getEntityName() + " title format: " + mlClass.getTitleFormat());
                    throw new MlApplicationException(String.format(oneToNAttribute));
                }
            }
        }
    }

    public void validateUpdate(MlClass mlClass, ObjectChangeSet objectChangeSet) {
        if (objectChangeSet == null) {
            return;
        }
        for (ChangeRecord changeRecord : objectChangeSet.getChanges()) {
            switch (changeRecord.getAttribute()) {
                case MlClass.ATTR_SET:
                    if (((CollectionChangeRecord) changeRecord).getRemoveObjectList().size() > 0) {
                        log.error("Try unbound attr");
                        throw new MlApplicationException(deleteAttribute);
                    }
            }
        }
    }
}
