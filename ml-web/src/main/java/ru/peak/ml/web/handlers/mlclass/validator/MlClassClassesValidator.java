package ru.peak.ml.web.handlers.mlclass.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.handler.MlClassHandler;
import ru.peak.ml.core.handler.MlReplicationHandler;
import ru.peak.ml.core.handler.validator.Validator;

import javax.persistence.Query;
import java.util.List;

/**
 */
public class MlClassClassesValidator implements Validator<MlClass> {
    private static final Logger log = LoggerFactory.getLogger(MlClassClassesValidator.class);
    private static String badJavaClassName = "Java Класс с именем %s не существует.";
    private static String javaClassAlreadyUse = "Java Класс с именем %s уже используется в классе %s.";
    private static String javaClassNotExtendMlDynamicEntityImpl = "Java Класс с именем %s не наследует MlDynamicEntityImpl.";
    private static String badHandlerClassName = "Пре\\пост обработчик с именем %s не существует.";
    private static String badHandlerClassNotImplementMlClassHandler = "Пре\\пост обработчик с именем %s не имплеменирует нужный интерфейс.";

    private static String badReplicationHandlerClassName = "Пре\\пост обработчик при импорте пакета обновлений с именем %s не существует.";
    private static String badReplicationHandlerClassNotImplementMlClassHandler = "Пре\\пост обработчик при импорте пакета обновлений с именем %s не имплеменирует нужный интерфейс.";

    @Override
    public void validate(MlClass mlClass) {
        javaClassCheck(mlClass);
        handlerClassCheck(mlClass);
        replicationHandlerClassCheck(mlClass);
    }

    private void replicationHandlerClassCheck(MlClass mlClass) {
        if (mlClass.getReplicationHandlerName() != null && !mlClass.getReplicationHandlerName().equals("")) {
            try {
                Class clazz = Class.forName(mlClass.getReplicationHandlerName());
                if (!(clazz.newInstance() instanceof MlReplicationHandler)) {
                    log.error("Bad HandlerClass class " + mlClass.getReplicationHandlerName() + " not implement MlClassHandler");
                    throw new MlApplicationException(String.format(badReplicationHandlerClassNotImplementMlClassHandler, mlClass.getReplicationHandlerName()));
                }
            } catch (ClassNotFoundException cnfe) {
                log.error("Bad HandlerClassName class " + mlClass.getReplicationHandlerName());
                throw new MlApplicationException(String.format(badReplicationHandlerClassName, mlClass.getReplicationHandlerName()));
            } catch (InstantiationException e) {
                log.error("Bad HandlerClass class " + mlClass.getReplicationHandlerName() + " not implement MlClassHandler");
                log.error("", e);
                throw new MlApplicationException(String.format(badReplicationHandlerClassNotImplementMlClassHandler, mlClass.getReplicationHandlerName()));
            } catch (IllegalAccessException e) {
                log.error("Bad HandlerClass class " + mlClass.getReplicationHandlerName() + " not implement MlClassHandler");
                log.error("", e);
                throw new MlApplicationException(String.format(badReplicationHandlerClassNotImplementMlClassHandler, mlClass.getReplicationHandlerName()));
            }
        }
    }

    private void handlerClassCheck(MlClass mlClass) {
        if (mlClass.getHandlerName() != null && !mlClass.getHandlerName().equals("")) {
            try {
                Class clazz = Class.forName(mlClass.getHandlerName());
                if (!(clazz.newInstance() instanceof MlClassHandler)) {
                    log.error("Bad HandlerClass class " + mlClass.getHandlerName() + " not implement MlClassHandler");
                    throw new MlApplicationException(String.format(badHandlerClassNotImplementMlClassHandler, mlClass.getHandlerName()));
                }
            } catch (ClassNotFoundException cnfe) {
                log.error("Bad HandlerClassName class " + mlClass.getHandlerName());
                throw new MlApplicationException(String.format(badHandlerClassName, mlClass.getHandlerName()));
            } catch (InstantiationException e) {
                log.error("Bad HandlerClass class " + mlClass.getHandlerName() + " not implement MlClassHandler");
                log.error("", e);
                throw new MlApplicationException(String.format(badHandlerClassNotImplementMlClassHandler, mlClass.getHandlerName()));
            } catch (IllegalAccessException e) {
                log.error("Bad HandlerClass class " + mlClass.getHandlerName() + " not implement MlClassHandler");
                log.error("", e);
                throw new MlApplicationException(String.format(badHandlerClassNotImplementMlClassHandler, mlClass.getHandlerName()));
            }
        }
    }

    private void javaClassCheck(MlClass mlClass) {
        if (mlClass.getJavaClass() != null && !mlClass.getJavaClass().equals("")) {
            classExistCheck(mlClass);
            classExtendsMlDynamicEntityImpl(mlClass);
            classUniqueCheck(mlClass);
        }
    }

    private void classUniqueCheck(MlClass mlClass) {
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        Query query = commonDao.getQueryWithoutSecurityCheck("select o from MlClass o where o.javaClass = :javaClass and o.id != :classId")
                .setParameter("javaClass", mlClass.getJavaClass())
                .setParameter("classId", mlClass.getId() == null? 0:mlClass.getId());
        List<MlClass> mlClasses = query.getResultList();
        if (mlClasses.size() > 0) {
            log.error("Java class " + mlClass.getJavaClass() + " already use in " + mlClasses.get(0).getEntityName());
            throw new MlApplicationException(String.format(javaClassAlreadyUse, mlClass.getJavaClass(), mlClasses.get(0).getDescription()));
        }
    }

    private void classExtendsMlDynamicEntityImpl(MlClass mlClass) {
        Class clazz = null;
        try {
            clazz = Class.forName(mlClass.getJavaClass());
        } catch (ClassNotFoundException e) {
            log.error("", e);
        }
        if (!MlDynamicEntityImpl.class.isAssignableFrom(clazz)) {
            log.error("Java class " + mlClass.getJavaClass() + " not extend MlDynamicEntityImpl");
            throw new MlApplicationException(String.format(javaClassNotExtendMlDynamicEntityImpl, mlClass.getJavaClass()));
        }
    }

    private void classExistCheck(MlClass mlClass) {
        try {
            Class clazz = Class.forName(mlClass.getJavaClass());
        } catch (ClassNotFoundException cnfe) {
            log.error("Bad Java class " + mlClass.getJavaClass());
            throw new MlApplicationException(String.format(badJavaClassName, mlClass.getJavaClass()));
        }
    }
}


