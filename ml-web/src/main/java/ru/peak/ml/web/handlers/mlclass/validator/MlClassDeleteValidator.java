package ru.peak.ml.web.handlers.mlclass.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.handler.validator.Validator;

import javax.persistence.Query;
import java.util.List;

/**
 */
public class MlClassDeleteValidator implements Validator<MlClass> {
    private static final Logger log = LoggerFactory.getLogger(MlClassNameValidator.class);
    private static final String classNotEmpty = "Для удаления класса %s необходимо удалить все объекты этого класса";
    private static final String classUseInRelations = "Класс %s ссылается на класс %s по атрибуту %s. ";
    private static final String attrUseInRelations = "Артибут %s класса %s ссылается на атрибут %s класса %s. ";
    private static final String classUseInFolder = "Класс %s отображается в каталоге %s. ";

    @Override
    public void validate(MlClass mlClass) {
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        emptyTableCheck(mlClass, commonDao);
        manyToOneCheck(mlClass, commonDao);
        manyToManyCheck(mlClass, commonDao);
        folderCheck(mlClass, commonDao);
       // utilCheck(mlClass, commonDao);
    }

    /*private void utilCheck(MlClass mlClass, CommonDao commonDao) {
        Query query = commonDao.getQuery("select o from MlFolder o where o.childClass.id = :classId").setParameter("classId", mlClass.getId());
        List<MlDynamicEntityImpl> folders = query.getResultList();
        if (folders.size() > 0) {
            for (MlDynamicEntityImpl folder : folders) {
                log.error("Class " + mlClass.getEntityName() + " use is folder "+folder.get("title"));
                throw new MlApplicationException(String.format(classUseInFolder, folder.get("title")));
            }
        }
    }
*/
    private void folderCheck(MlClass mlClass, CommonDao commonDao) {
        Query query = commonDao.getQueryWithoutSecurityCheck("select o from MlFolder o where o.childClass.id = :classId").setParameter("classId", mlClass.getId());
        List<MlDynamicEntityImpl> folders = query.getResultList();
        if (folders.size() > 0) {
            for (MlDynamicEntityImpl folder : folders) {
                log.error("Class " + mlClass.getEntityName() + " use is folder "+folder.get("title"));
                throw new MlApplicationException(String.format(classUseInFolder, mlClass.getDescription(), folder.get("title")));
            }
        }
    }

    private void manyToManyCheck(MlClass mlClass, CommonDao commonDao) {
        Query query;
        for (MlAttr mlAttr : mlClass.getAttrSet()) {
            query = commonDao.getQueryWithoutSecurityCheck("select o from MlAttr o where o.linkAttr.id = :attrId").setParameter("attrId", mlAttr.getId());
            List<MlAttr> mlAttrList = query.getResultList();
            if (mlAttrList.size() > 0) {
                String message = "Класс имеет атрибуты использующиеся в связях: ";
                for (MlAttr attr : mlAttrList) {
                    message += String.format(attrUseInRelations, attr.getDescription(),
                            ((MlClass) attr.getMlClass()).getDescription(),
                            ((MlAttr) mlAttr).getDescription(),
                            mlClass.getDescription());
                }
                message += "Уделение невозможно.";
                log.error("Class " + mlClass.getEntityName() + " have attr in relations");
                throw new MlApplicationException(message);
            }
        }
    }

    private void manyToOneCheck(MlClass mlClass, CommonDao commonDao) {
        Query query = commonDao.getQueryWithoutSecurityCheck("select o from MlAttr o where o.linkClass.id = :classId and o.mlClass.id != :classId").setParameter("classId", mlClass.getId());
        List<MlAttr> mlAttrs = query.getResultList();
        if (mlAttrs.size() > 0) {
            String message = "Класс имеет внешние связи: ";
            for (MlAttr mlAttr : mlAttrs) {
                message += String.format(classUseInRelations, ((MlClass) mlAttr.getMlClass()).getDescription(), mlClass.getDescription(), mlAttr.getDescription());
            }
            message += "Пока на класс есть связи уделение невозможно.";
            log.error("Class " + mlClass.getEntityName() + " have relations");
            throw new MlApplicationException(message);
        }
    }

    private void emptyTableCheck(MlClass mlClass, CommonDao commonDao) {
        Query query = commonDao.getQueryWithoutSecurityCheck("select count(1) from " + mlClass.getEntityName() + " o ");
        Long rowCount = (Long) query.getSingleResult();
        if (rowCount > 0) {
            log.error("Class " + mlClass.getEntityName() + " is not empty");
            throw new MlApplicationException(String.format(classNotEmpty, mlClass.getDescription()));
        }
    }
}
