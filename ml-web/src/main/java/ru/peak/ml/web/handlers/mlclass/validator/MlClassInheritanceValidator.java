package ru.peak.ml.web.handlers.mlclass.validator;

import com.google.inject.Inject;
import org.eclipse.persistence.internal.sessions.ObjectChangeSet;
import org.eclipse.persistence.sessions.changesets.ChangeRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.AttrType;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.handler.validator.Validator;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;

import javax.persistence.Query;

/**
 */
public class MlClassInheritanceValidator implements Validator<MlClass> {
    private static final Logger log = LoggerFactory.getLogger(MlClassInheritanceValidator.class);
    private static final String haveNotRealDynamicEntity = "В родительском классе %s отсутствует необходимое для наследования поле realDynamicEntity.";
    private static final String notStringRealDynamicEntity = "В родительском классе %s поле realDynamicEntity должно быть строкового типа.";
    private static final String parentClassHasWrongRealDynamicEntity = "В родительском классе %s поле realDynamicEntity содержит недопустимые значения.";
    private static final String classHasObjects = "Для изменения атрибута Родитель необходимо удалить все объекты класса %s.";
    private static final String haveChildren = "Класс %s не может быть удален, у него есть классы наследники. Для его удаления удалите все классы наследники.";
    @Inject
    CommonDao commonDao;

    @Override
    public void validate(MlClass mlClass) {
        if (mlClass.getParent() != null) {
            MlClass parent = mlClass.getParent();
            MlAttr attr = GuiceConfigSingleton.inject(MetaDataHolder.class).getAttr(parent, "realDynamicEntity");
            if (attr == null) {
                log.error("Have not realDynamicEntity in " + parent.getEntityName());
                throw new MlApplicationException(String.format(haveNotRealDynamicEntity, parent.getDescription()));
            }
            if (attr.getFieldType() != AttrType.STRING && attr.getFieldType() != AttrType.TEXT) {
                log.error("realDynamicEntity is not string! type = " + attr.getFieldType().name());
                throw new MlApplicationException(String.format(notStringRealDynamicEntity, parent.getDescription()));
            }
            if (checkRealDynamicEntity(parent)) {
                log.error("realDynamicEntity contains values not exist in MlClass. class " + mlClass.getEntityName());
                throw new MlApplicationException(String.format(parentClassHasWrongRealDynamicEntity, parent.getDescription()));
            }


        }
    }

    public void validateOnUpdate(MlClass mlClass, ObjectChangeSet objectChangeSet) {
        if (objectChangeSet == null) {
            return;
        }
        for (ChangeRecord changeRecord : objectChangeSet.getChanges()) {
            switch (changeRecord.getAttribute()) {
                case MlClass.PARENT:
                    if (hasObjects(mlClass)) {
                        log.error("Class " + mlClass.getEntityName() + " is not empty");
                        throw new MlApplicationException(String.format(classHasObjects, mlClass.getDescription()));
                    }
            }
        }
    }

    public Boolean checkRealDynamicEntity(MlClass mlClass) {
        Query query = commonDao.getQueryWithoutSecurityCheck("select count(1) from " + mlClass.getEntityName() + " o where o.realDynamicEntity not in (select c.entityName from MlClass c )");
        Long count = (Long) query.getSingleResult();
        return count > 0;
    }

    public Boolean hasObjects(MlClass mlClass) {
        Query query = commonDao.getQueryWithoutSecurityCheck("select count(1) from " + mlClass.getEntityName() + " o ");
        Long rowCount = (Long) query.getSingleResult();
        return rowCount > 0;
    }

    public void validateOnDelete(MlClass mlClass) {
        if (hasChildren(mlClass)) {
            log.error("Have children " + mlClass.getEntityName() + ". Can not be deleted.");
            throw new MlApplicationException(String.format(haveChildren, mlClass.getDescription()));
        }
    }

    private boolean hasChildren(MlClass mlClass) {
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        Long childrenCount = (Long) commonDao.getQueryWithoutSecurityCheck("select count(1) from MlClass o where o.parent.id = :classId").
                setParameter("classId", mlClass.getId()).getSingleResult();
        return childrenCount > 0;
    }
}
