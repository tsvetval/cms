package ru.peak.ml.web.handlers.mlenum;

import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.handler.DefaultMlClassHandler;
import ru.peak.ml.core.initializer.MlMetaDataInitializeService;
import ru.peak.ml.core.model.system.MlEnum;

public class MlEnumHandlerImpl extends DefaultMlClassHandler<MlEnum> {
    @Override
    public void beforeUpdate(MlEnum entity) {
        if (entity.get("title") == null) {
            entity.set("title", "");
        }
    }

    @Override
    public void beforeCreate(MlEnum entity) {
        if (entity.get("title") == null) {
            entity.set("title", "");
        }
    }

    @Override
    public void afterUpdate(MlEnum entity, boolean runBeforeHandlers) {
        MlMetaDataInitializeService mlMetaDataInitializeService = GuiceConfigSingleton.inject(MlMetaDataInitializeService.class);
        mlMetaDataInitializeService.addEnum(entity);
    }

    @Override
    public void afterCreate(MlEnum entity) {
        MlMetaDataInitializeService mlMetaDataInitializeService = GuiceConfigSingleton.inject(MlMetaDataInitializeService.class);
        mlMetaDataInitializeService.addEnum(entity);
    }
}
