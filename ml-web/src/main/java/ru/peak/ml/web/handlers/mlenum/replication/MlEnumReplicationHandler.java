package ru.peak.ml.web.handlers.mlenum.replication;

import org.eclipse.persistence.dynamic.DynamicEntity;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.model.system.MlEnum;
import ru.peak.ml.core.model.MLUID;
import ru.peak.ml.core.initializer.MlMetaDataInitializeService;
import ru.peak.ml.core.handler.MlReplicationHandler;

/**
 */
public class MlEnumReplicationHandler implements MlReplicationHandler {
    @Override
    public void beforeReplication(MLUID mluid) {

    }

    @Override
    public void afterReplication(DynamicEntity dynamicEntity) {
        MlMetaDataInitializeService mlMetaDataInitializeService = GuiceConfigSingleton.inject(MlMetaDataInitializeService.class);
        mlMetaDataInitializeService.addEnum((MlEnum) dynamicEntity);
    }
}
