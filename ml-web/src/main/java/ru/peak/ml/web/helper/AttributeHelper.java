package ru.peak.ml.web.helper;


import ru.peak.ml.core.model.MlDynamicEntityImpl;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AttributeHelper {

    /**
     * Replace substitutions in linkFilter property (it could look like "where o.linkedClass.id = ${this.mlClass.id}",
     * need to replace all content between {} by real values)
     *
     * @param context
     * @param original
     * @return
     */
    public static String prepareTemplateString(Map<String, Object> context, String original)
    {
        StringBuffer resultString = new StringBuffer();
        Pattern regexp = Pattern.compile("\\$\\{(.*)\\}");
        Matcher matcher = regexp.matcher(original);
        while (matcher.find()) {
            String id = matcher.group(1);
            Object value;
            String[] parts = id.split("\\.");
            if(parts.length==0)
            {
                value = null;
            }
            else {
                value = context;
                for(String next: parts) {
                    if(value instanceof MlDynamicEntityImpl)
                    {
                        MlDynamicEntityImpl dynamicEntity = (MlDynamicEntityImpl) value;
                        value = dynamicEntity.get(next);
                    }
                    else if(value instanceof Map)
                    {
                        Map map = (Map) value;
                        value = map.get(next);
                    }
                    else {
                        break;
                    }
                }
            }
            //todo: need to escape
            matcher.appendReplacement(resultString, value != null ? value.toString() : "null");
        }
        matcher.appendTail(resultString);
        return resultString.toString();
    }

    public static void main(String[] args) {
        HashMap<String, Object> context = new HashMap<>();
        context.put("this", "o");
        String result = AttributeHelper.prepareTemplateString(context, "${this}.childClass.id = 42");
        System.out.println(result);
    }

}
