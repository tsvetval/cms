package ru.peak.ml.web.helper;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DownloadHelper {

    public final static int MAX_FILES = 3;
    public static final String DIR_NAME = "download";

    public static String generateDownloadLink(byte[] data, String name, ServletContext servletContext) throws IOException {


        File dir = new File(servletContext.getRealPath("/" + DIR_NAME + "/"));
        dir.mkdir();

        if (dir.list().length >= MAX_FILES) {
            List<File> files = new ArrayList<>();
            for (String s : dir.list())
                files.add(new File(dir, s));

            Collections.sort(files, new Comparator<File>() {
                @Override
                public int compare(File o1, File o2) {
                    return (int) (o1.lastModified() - o2.lastModified());
                }
            });

            int toRemove = dir.list().length - MAX_FILES + 1;
            for (int i = 0; i < toRemove; i++) {
                files.get(i).delete();
            }

        }

        File file = new File(dir, name);

        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(data);
        fileOutputStream.close();

        return servletContext.getContextPath() + "/" + DIR_NAME + "/" + name + "?ml_request=true";

    }

    public static boolean isDownloadLink(String url, ServletContext servletContext) {
        return url.startsWith("/" + DIR_NAME + "/");
    }

    public static void feedDownloadFile(String url, HttpServletResponse resp, ServletContext servletContext) throws IOException {
        String[] split = url.split("/");
        String name = split[split.length - 1];

        File file = new File(servletContext.getRealPath("/" + DIR_NAME + "/" + name));

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        {
            FileInputStream inputStream = new FileInputStream(file);
            int read;
            byte[] buf = new byte[1024];
            while ((read = inputStream.read(buf)) > 0) {
                outputStream.write(buf, 0, read);
            }
            outputStream.close();
            inputStream.close();
        }

        byte[] bytes = outputStream.toByteArray();


        String contentType = URLConnection.guessContentTypeFromName(file.getName());
        resp.setContentType(contentType != null ? contentType : "application/octet-stream");
        resp.addHeader("Content-Disposition", "attachment;");
        resp.setHeader("Content-Length", String.valueOf(bytes.length));
        resp.getOutputStream().write(bytes);
    }
}
