package ru.peak.ml.web.helper;

import org.eclipse.persistence.internal.dynamic.DynamicEntityImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;

import javax.xml.parsers.DocumentBuilderFactory;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/*
    Helper for export current DB state into liquibase config file
    //todo: need to find appropriate place for it and make GUI to use it
 */
public class ExportToLiquibase {


    public ExportToLiquibase() {
    }

    public Document doit() throws ExportException {
        try {

            CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
            List<MlClass> allMlClasses = commonDao.getResultList("select c from MlClass c", MlClass.class);

            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

            Element root = document.createElementNS("http://www.liquibase.org/xml/ns/dbchangelog", "databaseChangeLog");
            root.setAttributeNS("http://www.w3.org/2001/XMLSchema-instance", "xsi:schemaLocation", "http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.1.xsd");
            document.appendChild(root);

            // Create change set
            Element changeSet = document.createElement("changeSet");
            changeSet.setAttribute("id", "ts_" + String.valueOf(new Date().getTime())); //todo: generate
            changeSet.setAttribute("author", "robot"); //todo: generate
            root.appendChild(changeSet);


            // Export all classes
            changeSet.appendChild(document.createComment("Add table creations"));
            for (MlClass MlClass : allMlClasses) {
                exportClass(MlClass, document, changeSet);
            }

            // Export all instances
            changeSet.appendChild(document.createComment("Add instance insert instructions"));
            for (MlClass MlClass : allMlClasses) {
                List<DynamicEntityImpl> instanceList = commonDao.getResultList("select o from " + MlClass.getEntityName() + " o", DynamicEntityImpl.class);
                for (DynamicEntityImpl instance : instanceList) {
                    exportInstance(MlClass, instance, document, changeSet);
                }
            }

            return document;

/*            // Write
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File("C:\\mavrenkov\\cms\\rep\\src\\ml-web\\src\\main\\java\\ru\\peak\\ml\\web\\service\\impl\\out.xml"));
            transformer.transform(source, result);*/

        } catch (Exception e) {
            throw new ExportException("Couldn't export DB to liquibase file", e);
        }

    }

    private void exportClass(MlClass MlClass, Document document, Element root) {
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);

        List<MlAttr> attrSet = MlClass.getAttrSet();

        // Create sequence
        Element createSequence = document.createElement("createSequence");
        createSequence.setAttribute("sequenceName", MlClass.getEntityName() + "_id_SEQ");
        createSequence.setAttribute("maxValue", "9223372036854775807");
        // Found max id for class and use as current sequence value
        Long maxId = (Long) commonDao.getResultList("select COALESCE(max(o.id),0) from " + MlClass.getEntityName() + " o", Long.class).get(0);
        createSequence.setAttribute("startValue", String.valueOf(maxId+1));

        root.appendChild(createSequence);

        // Create table
        Element createTable = document.createElement("createTable");
        createTable.setAttribute("tableName", MlClass.getTableName());

        // Insert mandatory id attribute
        {
            Element column = document.createElement("column");
            column.setAttribute("name", "id");
            column.setAttribute("type", "bigint");
            column.setAttribute("defaultValue", "nextval('\""+ MlClass.getTableName()+"_id_SEQ\"'::regclass)");
            createTable.appendChild(column);
        }
        for (MlAttr attr : attrSet) {
            if (attr.isVirtual()){
                continue;
            }
            if(attr.getTableFieldName()!=null && !attr.getTableFieldName().equals("id"))
            {
                Element column = document.createElement("column");
                column.setAttribute("name",attr.getTableFieldName());
                String type;
                switch (attr.getFieldType()) {
                    case STRING: type = "character varying"; break;
                    case BOOLEAN: type = "boolean"; break;
                    case LONG: type = "bigint"; break;
                    case TEXT: type = "character varying"; break;
                    case DATE: type = "date"; break;
                    case DOUBLE: type = "numeric(10,2)"; break;
                    case MANY_TO_MANY: type = "bigint"; break;
                    case MANY_TO_ONE: type = "bigint"; break;
                    case ONE_TO_MANY: type = "bigint"; break;
                    case ONE_TO_ONE: type = "bigint"; break;
                    case LONG_LINK: type = "character varying"; break;
                    case FILE: type = "character varying"; break; //todo: implement
                    case ENUM: type = "character varying"; break;
                    default:throw new RuntimeException("SQL equivalent for attr type "+attr.getFieldType()+" is not defined!");
                }
                column.setAttribute("type",type); //todo: check
                createTable.appendChild(column);
            }
        }
        root.appendChild(createTable);

        // Add primary key
        Element primaryKey = document.createElement("addPrimaryKey");
        primaryKey.setAttribute("tableName", MlClass.getTableName());
        primaryKey.setAttribute("columnNames", "id");
        primaryKey.setAttribute("constraintName", MlClass.getEntityName() + "_PK");
        root.appendChild(primaryKey);

    }



    private void exportInstance(MlClass MlClass, DynamicEntityImpl instance, Document document, Element root)
    {

        Element insert = document.createElement("insert");

        insert.setAttribute("tableName", MlClass.getTableName());

        {
            Element column = document.createElement("column");
            column.setAttribute("name", "id");
            column.setAttribute("value", String.valueOf(instance.get("id")));
            insert.appendChild(column);
        }

        for (MlAttr attr : MlClass.getAttrSet()) {

            if (attr.getTableFieldName()!=null && !attr.getTableFieldName().equals("id")
                    && !attr.isVirtual()) {
                switch (attr.getFieldType()) {
                    case ONE_TO_MANY:break;
                    case MANY_TO_MANY:break;

                    case ONE_TO_ONE:break;
                    case MANY_TO_ONE:  {
                        //todo: in case of link attributes need to insert only id's (or nothing, if it's many_to_one)
                        Element column = document.createElement("column");

                        column.setAttribute("name", attr.getTableFieldName());
                        DynamicEntityImpl linkedEntity = instance.get(attr.getEntityFieldName());
                        column.setAttribute("value", String.valueOf(linkedEntity != null ? linkedEntity.get("id") : null));
                        insert.appendChild(column);
                        break;
                    }

                    case TEXT:
                    case STRING:
                    case LONG:
                    case LONG_LINK:
                    case ENUM:
                    case BOOLEAN:{
                        Element column = document.createElement("column");
                        column.setAttribute("name", attr.getTableFieldName());
                        column.setAttribute("value", String.valueOf(instance.get(attr.getEntityFieldName())));
                        insert.appendChild(column);
                        break;
                    }

                    case DATE: {
                        Element column = document.createElement("column");
                        column.setAttribute("name", attr.getTableFieldName());
                        column.setAttribute("value", new SimpleDateFormat("yyyy-MM-dd").format(instance.get(attr.getEntityFieldName())));
                        insert.appendChild(column);
                        break;
                    }

                    default:
                        throw new RuntimeException("Not implemented: " + attr.getFieldType());

                }
            }
        }

        root.appendChild(insert);
    }


    public static class ExportException extends Exception {
        public ExportException() {
        }

        public ExportException(String message) {
            super(message);
        }

        public ExportException(String message, Throwable cause) {
            super(message, cause);
        }

        public ExportException(Throwable cause) {
            super(cause);
        }

        public ExportException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
        }
    }

}
