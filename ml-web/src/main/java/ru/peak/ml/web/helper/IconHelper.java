package ru.peak.ml.web.helper;

import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.prop.MlProperties;
import ru.peak.ml.prop.Property;
import ru.peak.ml.prop.impl.MlPropertiesImpl;
import ru.peak.ml.web.common.MlHttpServletRequest;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class IconHelper {



    public static List<String> getIconList() {
        MlProperties properties = GuiceConfigSingleton.inject(MlProperties.class);
        String iconPath = properties.getProperty(Property.ICON_PATH);
        File folder = new File(iconPath);

        File[] listOfIconFiles = folder.listFiles();

        List<String> iconList = new ArrayList<>();

        if (listOfIconFiles != null) {
            for (File file : listOfIconFiles) {
                if (file.isFile()) {
                    iconList.add(file.getName());
                }
            }
        }

        return iconList;
    }

    public static String getNavigationIconURL(String filename) {
        String filepath = GuiceConfigSingleton.inject(MlProperties.class).getProperty(Property.WEBAPPS_PATH) + "/" + filename;
        File file = new File(filepath);
        if (!file.exists()) {
            return null;
        }
        return filename;
    }

    public static String getIconURL(String filename) {
        MlProperties properties = GuiceConfigSingleton.inject(MlProperties.class);
        String filepath = properties.getProperty(Property.ICON_PATH) + "/" + filename;
        File file = new File(filepath);
        if (!file.exists()) {
            return null;
        }
        return getIconFolder() + "/" + filename;
    }

    public static String getIconFolder() {
        MlProperties properties = GuiceConfigSingleton.inject(MlProperties.class);
        return properties.getProperty(Property.ICON_PATH_RELATIVE);
    }

    public static int getIconIndex(String filename) {
        List<String> iconList = getIconList();
        return iconList.indexOf(filename);
    }

    private static String getIconFolderPath(String contextPath){
        return GuiceConfigSingleton.inject(MlProperties.class).getProperty(Property.WEBAPPS_PATH);
    }
}
