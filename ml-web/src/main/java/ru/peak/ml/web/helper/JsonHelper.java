package ru.peak.ml.web.helper;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.eclipse.persistence.internal.dynamic.DynamicEntityImpl;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;

import java.util.List;

/**
 *
 */
public class JsonHelper {

    public static JsonElement dynamicObjectToJson(DynamicEntityImpl object, String[] fields) {
        JsonObject result = new JsonObject();
        for (String field : fields) {
            //TODO add DynamicSerializer

            MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
            MlClass IMlClass = metaDataHolder.getMlClassByEntityDynamicClass(object.getClass());
            MlAttr attr = metaDataHolder.getAttr(IMlClass, field);
            switch (attr.getFieldType()) {
                //TODO add
                case STRING:
                    result.add(field, new JsonPrimitive((String) object.get(field)));
                    break;
                case LONG:
                    result.add(field, new JsonPrimitive((Long) object.get(field)));
                    break;
                case BOOLEAN:
                    result.add(field, new JsonPrimitive((Boolean) object.get(field)));
                    break;
            }
        }
        return result;
    }

    public static JsonElement dynamicObjectListToJson(List<DynamicEntityImpl> objectList, String[] fields) {
        JsonArray result = new JsonArray();
        for (DynamicEntityImpl dynamicEntity : objectList) {
            result.add(dynamicObjectToJson(dynamicEntity, fields));
        }
        return result;
    }


}
