package ru.peak.ml.web.helper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.eclipse.persistence.dynamic.DynamicEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.AttrType;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.filter.FilterBuilder;
import ru.peak.ml.core.filter.result.FilterResult;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.folder.MlFolder;
import ru.peak.ml.core.model.security.MlAttrAccessType;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.services.AccessService;
import ru.peak.ml.prop.Property;
import ru.peak.ml.web.model.MlHeteroLink;

import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ObjectHelper {
    private static final Logger log = LoggerFactory.getLogger(ObjectHelper.class);

    public static MlDynamicEntityImpl updateObjectByMap(MlDynamicEntityImpl object, Map<String, Object> objectMap, MlUser user) {
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        AccessService accessService = GuiceConfigSingleton.inject(AccessService.class);
        for (String key : objectMap.keySet()) {

            MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
            MlAttr attr = metaDataHolder.getAttr(metaDataHolder.getMlClassByEntityDynamicClass(object.getClass()), key);
            attr = attr.getOverridedAttrIfExist();
            if (attr.isVirtual()) {
                continue;
            }
            if (!accessService.checkAccessAttr(attr, MlAttrAccessType.EDIT)) {
                continue;
            }
            Object value = objectMap.get(key);
            if (value != null && !value.toString().isEmpty()) {
                switch (attr.getFieldType()) {
                    case HETERO_LINK:
                        List<String> ids = (List<String>) value;
                        List<MlHeteroLink> mlHeteroLinks = new ArrayList<>();
                        for (String id : ids) {
                            String className = id.split("@")[1];
                            String objectId = id.split("@")[0];
                            MlHeteroLink mlHeteroLink = (MlHeteroLink) commonDao.createNewEntity(MlHeteroLink.class);
                            MlDynamicEntityImpl entity = commonDao.findById(Long.parseLong(objectId), className);
                            mlHeteroLink.setObject(entity);//;ClassName(className);
                            //mlHeteroLink.setObjectId(Long.parseLong(objectId));
                            commonDao.persistWithSecurityCheck(mlHeteroLink);
                            mlHeteroLinks.add(mlHeteroLink);
                        }
                        object.set(key, mlHeteroLinks);
                        break;
                    case MANY_TO_ONE:
                        String linkObjectClassName = attr.getLinkClass().getEntityName();
                        Long linkObjectId = Long.parseLong(value.toString());
                        MlDynamicEntityImpl linkObject = commonDao.findById(linkObjectId, linkObjectClassName);
                        object.set(key, linkObject);
                        break;
                    case ONE_TO_MANY: {
                        Gson gson = new Gson();
                        Type listType = new TypeToken<ArrayList<Long>>() {
                        }.getType();
                        List<Long> selectedList = gson.fromJson(value.toString(), listType);
                        String className = attr.getLinkAttr().getMlClass().getEntityName();
                        List<MlDynamicEntityImpl> instanceList;
                        if (selectedList != null && !selectedList.isEmpty()) {
                            String idList = selectedList.toString();
                            idList = idList.substring(1, idList.length() - 1);
                            String jpql = String.format("select o from %s o where o.id in (%s)", className, idList);
                            instanceList = commonDao.getResultList(jpql, className);
                            instanceList = sortArrayByIdList(instanceList, selectedList);
                        } else {
                            instanceList = Collections.emptyList();
                        }
                        for(MlDynamicEntityImpl entity: instanceList){
                            entity.set(attr.getLinkAttr().getEntityFieldName(),object);
                        }
                        Collection<MlDynamicEntityImpl> relations = object.get(key);
                        if (relations == null) {
                            relations = new ArrayList();
                        }
                        for(MlDynamicEntityImpl entity : relations){
                            if(!instanceList.contains(entity)){
                                entity.set(attr.getLinkAttr().getEntityFieldName(),null);
                            }
                        }
                        relations.clear();
                        relations.addAll(instanceList);
//                        MlClass MlClass = object.getInstanceMlClass();
//                        object.setOneToManyListValues(MlClass, key, instanceList);
                    }
                    break;
                    case MANY_TO_MANY: {
                        Gson gson = new Gson();
                        Type listType = new TypeToken<ArrayList<Long>>() {
                        }.getType();
                        List<Long> selectedList = gson.fromJson(value.toString(), listType);
                        String className = attr.getLinkClass().getEntityName();
                        List<MlDynamicEntityImpl> instanceList;
                        if (selectedList != null && !selectedList.isEmpty()) {
                            String idList = selectedList.toString();
                            idList = idList.substring(1, idList.length() - 1);
                            String jpql = String.format("select o from %s o where o.id in (%s)", className, idList);
                            instanceList = commonDao.getResultList(jpql, className);
                            instanceList = sortArrayByIdList(instanceList, selectedList);
                        } else {
                            instanceList = Collections.emptyList();
                        }
                        Collection<MlDynamicEntityImpl> relations = object.get(key);
                        if (relations == null) {
                            relations = new ArrayList<>();
                        }
                        relations.clear();
                        relations.addAll(instanceList);
                    }
                    break;
                    case DOUBLE:
                        try {
                            object.set(key, Double.parseDouble(value.toString()));
                        } catch (NumberFormatException e) {
                            log.error(e.getMessage(), e);
                            throw new MlApplicationException(String.format("Неверное значение вещественного аттрибута[%s] : %s", attr.getDescription(), value.toString()));
                        }
                        break;

                    case LONG:
                        try {
                            object.set(key, Long.parseLong(value.toString()));
                        } catch (NumberFormatException e) {
                            log.error(e.getMessage(), e);
                            throw new MlApplicationException(String.format("Неверное значение целочисленного атрибута[%s] : %s", attr.getDescription(), value.toString()));
                        }
                        break;
                    case DATE:
                        String dateFormatString = "dd.MM.yyyy";
                        if (attr.getFormat() != null && !attr.getFormat().trim().isEmpty()) {
                            dateFormatString = attr.getFormat();
                        }
                        SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatString);
                        try {
                            Date date = dateFormat.parse(value.toString());
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(date);
                            if (!dateFormatString.contains("H")) {
                                cal.set(Calendar.HOUR_OF_DAY, 0);
                            }
                            if (!dateFormatString.contains("m")) {
                                cal.set(Calendar.MINUTE, 0);
                            }
                            if (!dateFormatString.contains("s")) {
                                cal.set(Calendar.SECOND, 0);
                            }
                            if (!dateFormatString.contains("S")) {
                                cal.set(Calendar.MILLISECOND, 0);
                            }
                            object.set(key, date);
                        } catch (ParseException e) {
                            log.error(e.getMessage(), e);
                            throw new MlApplicationException(String.format("Неверное значение аттрибута [%s] тип дата: %s", attr.getDescription(), value.toString()));
                        }
                        break;
                    case FILE:
                        Long userId = user.getId();
                        String filename = value.toString();
                        Path tempPath = Paths.get(String.format("%s/user_%d/%s", Property.getTempDir(), userId, filename));
                        try {
                            byte[] data = Files.readAllBytes(tempPath);
                            object.set(key, data);
                        } catch (Exception e) {
                            log.error(e.getMessage(), e);
                            throw new MlServerException("Ошибка загрузки файла", e.getCause());
                        }
                        object.set(key + "_filename", filename);
                        break;
                    case BOOLEAN:
                        object.set(key, Boolean.parseBoolean(value.toString()));
                        break;
                    case LONG_LINK:
                        object.set(key, value.toString());
                        break;
                    case STRING:
                        object.set(key, value.toString());
                        break;
                    case TEXT:
                        object.set(key, value.toString());
                        break;
                    default:
                        object.set(key, value);
                }
            } else {
                object.set(key, null);
                if (attr.getFieldType().equals(AttrType.FILE)) {
                    object.set(key + "_filename", null);
                }
            }
        }
        return object;
    }

    private static List<MlDynamicEntityImpl> sortArrayByIdList
            (List<MlDynamicEntityImpl> instanceList, List<Long> idList) {
        List<MlDynamicEntityImpl> orderedArray = new ArrayList<>();
        for (Long id : idList) {
            MlDynamicEntityImpl entity = null;
            for (MlDynamicEntityImpl instance : instanceList) {
                if (instance.get("id").equals(id)) {
                    entity = instance;
                    break;
                }
            }
            orderedArray.add(entity);
        }
        return orderedArray;
    }

    public static List<MlDynamicEntityImpl> getObjectsForAttr(DynamicEntity owner, MlAttr attr) {
        MlClass linkClass = attr.getLinkClass();

        HashMap<String, Object> context = new HashMap<>();
        String linkFilter = ((DynamicEntity) attr).get("linkFilter");
        String linkCondition = null;
        if (linkFilter != null && !linkFilter.isEmpty()) {
            context.put("this", owner);
            linkCondition = AttributeHelper.prepareTemplateString(context, linkFilter);
        }

        FilterBuilder filterBuilder = GuiceConfigSingleton.inject(FilterBuilder.class);

        FilterResult filterResult = filterBuilder.setQueryMlClass(linkClass).addAdditionalCondition(linkCondition).buildFilterResult();

        return filterResult != null ? filterResult.getResultList() : null;
    }


    public static DynamicEntity getObjectInFolder(long objectId, long folderId) {
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        MlFolder folder = (MlFolder) commonDao.findById(folderId, "MlFolder");
        if (folder != null) {
            MlClass MlClass = folder.getChildClass();
            if (MlClass != null) {
                return commonDao.findById(objectId, MlClass.getEntityName());
            }
        }
        return null;
    }

    public static DynamicEntity getObjectByMlId(String mlId) {
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        Long id = Long.parseLong(mlId.split("@")[0]);
        String className = mlId.split("@")[1];
        return commonDao.findById(id, className);
    }
}
