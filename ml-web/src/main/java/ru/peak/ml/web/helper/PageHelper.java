package ru.peak.ml.web.helper;

import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.web.dao.MlPageDao;
import ru.peak.ml.core.model.page.MlPage;

public class PageHelper {

    public static String getPageTitle(String url, String ... params) {
        MlPageDao mlPageDao = GuiceConfigSingleton.inject(MlPageDao.class);
        MlPage mlPage = mlPageDao.getPageByUrl(url);
        String title = mlPage.get("title");
        title = String.format(title, params);
        return title;
    }
}
