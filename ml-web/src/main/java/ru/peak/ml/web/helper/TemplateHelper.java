package ru.peak.ml.web.helper;

import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.prop.MlProperties;
import ru.peak.ml.prop.Property;
import ru.peak.ml.prop.impl.MlPropertiesImpl;

import java.io.File;

public class TemplateHelper {
    public static boolean templateExists(String path){
        MlProperties properties = GuiceConfigSingleton.inject(MlProperties.class);
        String templatePath = properties.getProperty(Property.TEMPLATE_PATH) + '/' + path;
        File file = new File(templatePath);
        return file.exists();
    }
}

