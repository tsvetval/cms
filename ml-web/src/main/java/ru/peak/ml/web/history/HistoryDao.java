package ru.peak.ml.web.history;

import com.google.inject.Inject;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.ml.core.common.AttrType;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.web.model.MlHistoryUtil;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 */
public class HistoryDao {

    private final Integer ITEMS_PER_PAGE = 10;
    private static final String REQUEST_CURRENT_PAGE = "currentPage";

    @Inject
    EntityManager entityManager;
    @Inject
    CommonDao commonDao;

    public Map<String, Object> getHistoryItems(Long linkedObjectId, MlClass MlClass, MlHistoryUtil mlHistoryUtil) {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("headers", getHeaders(MlClass, mlHistoryUtil));
        String queryString = HistoryQueryBuilder.build().getQueryString(MlClass, mlHistoryUtil);
        String queryStringCount = HistoryQueryBuilder.build().getCountQueryString(MlClass, mlHistoryUtil);


        Query query = entityManager.createNativeQuery(queryString);
        Query queryCount = entityManager.createNativeQuery(queryStringCount);

        query.setParameter(1, linkedObjectId);
        queryCount.setParameter(1, linkedObjectId);

        long recordCount = Integer.valueOf(queryCount.getSingleResult().toString());
        long pageCount = recordCount / ITEMS_PER_PAGE;
        long addOnePage = recordCount % ITEMS_PER_PAGE;
        if (addOnePage > 0) {
            pageCount++;
        }

        HttpServletRequest request = GuiceConfigSingleton.inject(HttpServletRequest.class);

        Long currentPage = request.getParameter(REQUEST_CURRENT_PAGE) == null ? 1 : Long.parseLong(request.getParameter(REQUEST_CURRENT_PAGE));

        query.setFirstResult((int) (currentPage - 1) * ITEMS_PER_PAGE);
        query.setMaxResults(ITEMS_PER_PAGE);

        List<Object[]> values = query.getResultList();
        List<EntityStub> queryResult = new ArrayList<EntityStub>();
        for (Object[] val : values) {
            EntityStub item;
            if (mlHistoryUtil.getFieldsForShowInHistory().size() > 0) {
                item = fillEntityStub(val, mlHistoryUtil.getFieldsForShowInHistory());
            } else {
                item = fillEntityStub(val, MlClass.getAttrSet());
            }
            queryResult.add(item);
        }
        result.put("dataList", queryResult);

        result.put("pageCount", pageCount);
        result.put("pageCurrent", currentPage);

        return result;
    }

    private EntityStub fillEntityStub(Object[] val, List<? extends MlAttr> mlAttrs) {
        EntityStub result = new EntityStub();
        int ind = 0;
        for (MlAttr attr : mlAttrs) {
            if (!attr.isVirtual()) {
                switch (attr.getFieldType()) {
                    case MANY_TO_ONE:
                        if (val[ind] != null) {
                            MlDynamicEntityImpl entity = (MlDynamicEntityImpl) commonDao.findById(val[ind], attr.getLinkClass().getEntityName());
                            result.put(attr.getEntityFieldName(), entity);
                        } else {
                            result.put(attr.getEntityFieldName(), null);
                        }
                        break;

                    default:
                        if (attr.getPrimaryKey()) {
                            result.setId(val[ind].toString());
                        }
                        result.put(attr.getEntityFieldName(), val[ind]);
                }
                ind++;
            }
        }
        result.put(MlAttr.HISTORY_START_DATE_FIELD_NAME, val[ind++]);
        result.put(MlAttr.HISTORY_ACTION_TYPE_FIELD_NAME, val[ind++]);
        //  item.put(MlAttr.HISTORY_END_DATE_FIELD_NAME,val[ind++]);
        result.put(MlAttr.HISTORY_USER_LOGIN_FIELD_NAME, val[ind++]);
        return result;
    }

    private List<MlAttrStubForView> getHeaders(MlClass MlClass, MlHistoryUtil mlHistoryUtil) {
        List<MlAttrStubForView> result = new ArrayList<MlAttrStubForView>();
        List<? extends MlAttr> attrs;
        if (mlHistoryUtil.getFieldsForShowInHistory().size() > 0) {
            attrs = mlHistoryUtil.getFieldsForShowInHistory();
        } else {
            attrs = MlClass.getAttrSet();
        }
        for (MlAttr attr : attrs) {
            switch (attr.getFieldType()) {
                case MANY_TO_MANY:
                    result.add(new MlAttrStubForView(AttrType.STRING, ((MlAttr) attr).getDescription(), attr.getEntityFieldName()));
                    break;
                default:
                    result.add(new MlAttrStubForView(attr.getFieldType(), ((MlAttr) attr).getDescription(), attr.getEntityFieldName()));
            }

        }
        result.add(new MlAttrStubForView(AttrType.DATE, "Дата", MlAttr.HISTORY_START_DATE_FIELD_NAME));
        result.add(new MlAttrStubForView(AttrType.STRING, "Действие", MlAttr.HISTORY_ACTION_TYPE_FIELD_NAME));
        //result.add(new MlAttrStubForView(AttrType.DATE,"По",MlAttr.HISTORY_END_DATE_FIELD_NAME));
        result.add(new MlAttrStubForView(AttrType.STRING, "Пользователь", MlAttr.HISTORY_USER_LOGIN_FIELD_NAME));

        return result;
    }
}
