package ru.peak.ml.web.history;

import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.web.model.MlHistoryUtil;

/**
 */
public interface HistoryQuery {

    public String getQueryString(MlClass linkedClass, MlHistoryUtil mlHistoryUtil);

    String getCountQueryString(MlClass IMlClass, MlHistoryUtil mlHistoryUtil);
}
