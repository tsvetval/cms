package ru.peak.ml.web.history;

import org.eclipse.persistence.jpa.JpaEntityManager;
import ru.ml.core.common.guice.GuiceConfigSingleton;

import javax.persistence.EntityManager;

/**
 * Created with IntelliJ IDEA.
 * User: d_litovchenko
 * Date: 05.06.14
 * Time: 15:49
 * To change this template use File | Settings | File Templates.
 */
public class HistoryQueryBuilder {



    public static HistoryQuery build(){
        EntityManager entityManager = GuiceConfigSingleton.inject(EntityManager.class);
        if(entityManager.unwrap(JpaEntityManager.class).getSession().getPlatform().toString().equalsIgnoreCase("PostgreSQLPlatform")){
            return new PostgresHistoryQuery();
        }else{
            throw new RuntimeException("Unsupported platform "+entityManager.unwrap(JpaEntityManager.class).getSession().getPlatform().toString());
        }
    }
}
