package ru.peak.ml.web.history;

import ru.ml.core.common.AttrType;

/**
 * Created with IntelliJ IDEA.
 * User: d_litovchenko
 * Date: 05.06.14
 * Time: 17:45
 * To change this template use File | Settings | File Templates.
 */
public class MlAttrStubForView {
    AttrType fieldType;
    String description;
    String entityFieldName;

    public MlAttrStubForView(AttrType fieldType, String description, String entityFieldName) {
        this.fieldType = fieldType;
        this.description = description;
        this.entityFieldName = entityFieldName;
    }

    public AttrType getFieldType() {
        return fieldType;
    }

    public void setFieldType(AttrType fieldType) {
        this.fieldType = fieldType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEntityFieldName() {
        return entityFieldName;
    }

    public void setEntityFieldName(String entityFieldName) {
        this.entityFieldName = entityFieldName;
    }

    public Boolean getReadOnly(){
        return true;
    }

    public String getView(){
        return null;
    }
}
