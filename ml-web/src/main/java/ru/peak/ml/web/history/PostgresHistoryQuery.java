package ru.peak.ml.web.history;

import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.web.model.MlHistoryUtil;

/**
 */
public class PostgresHistoryQuery implements HistoryQuery {
    private static final String DELIMETER = "\"";

    @Override
    public String getQueryString(MlClass MlClass, MlHistoryUtil mlHistoryUtil) {
        StringBuilder query = new StringBuilder("select ");
        if (mlHistoryUtil.getFieldsForShowInHistory().size() > 0) {
            for (MlAttr attr : mlHistoryUtil.getFieldsForShowInHistory()) {
                if (!attr.isVirtual()) {
                    query.append(DELIMETER).append(attr.getTableFieldName()).append(DELIMETER).append(", ");
                }
            }
        } else {
            for (MlAttr attr : MlClass.getAttrSet()) {
                if (!attr.isVirtual()) {
                    query.append(DELIMETER).append(attr.getTableFieldName()).append(DELIMETER).append(", ");
                }
            }
        }

        query.append(DELIMETER).append(MlAttr.HISTORY_START_DATE_FIELD_NAME).append(DELIMETER).append(", ");
        query.append(DELIMETER).append(MlAttr.HISTORY_ACTION_TYPE_FIELD_NAME).append(DELIMETER).append(", ");
        //query.append(DELIMETER).append(MlAttr.HISTORY_END_DATE_FIELD_NAME).append(DELIMETER).append(", ");
        query.append(DELIMETER).append(MlAttr.HISTORY_USER_LOGIN_FIELD_NAME).append(DELIMETER);

        query.append(" from ");
        query.append(DELIMETER).append(MlClass.getTableName()).append(MlClass.HISTORY_TABLE_POSTFIX).
                append(DELIMETER).append(" where ").append(DELIMETER).
                append(MlClass.getPrimaryKeyAttr().getTableFieldName()).
                append(DELIMETER).append(" = ").append("?");
        query.append(" order by ").append(DELIMETER).append(MlAttr.HISTORY_START_DATE_FIELD_NAME).append(DELIMETER).append(" desc");
        return query.toString();
    }

    @Override
    public String getCountQueryString(MlClass MlClass, MlHistoryUtil mlHistoryUtil) {
        StringBuilder query = new StringBuilder("select count(1) ");
        query.append(" from ");
        query.append(DELIMETER).append(MlClass.getTableName()).append(MlClass.HISTORY_TABLE_POSTFIX).
                append(DELIMETER).append(" where ").append(DELIMETER).
                append(MlClass.getPrimaryKeyAttr().getTableFieldName()).
                append(DELIMETER).append(" = ").append("?");
        return query.toString();
    }
}
