package ru.peak.ml.web.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.model.MlDynamicEntityImpl;

/**
 * Created by d_litovchenko on 28.01.15.
 */
public class MlHeteroLink extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }

    /*public MlDynamicEntityImpl getObject() {

        return get(Properties.OBJECT);
    }


*/

    protected static class Properties{
        //public static final String OBJECT = "object";
        public static final String OBJECT_ID = "objectId";
        public static final String CLASS_NAME = "class";
    }

    public <T extends MlDynamicEntityImpl> void setObject(T entity) {
        set(Properties.CLASS_NAME, entity.getInstanceMlClass().getEntityName());
        set(Properties.OBJECT_ID, entity.get(entity.getInstanceMlClass().getPrimaryKeyAttr().getEntityFieldName()));
    }

    public String getStrId(){
        return getObjectId()+"@"+getClassName();
    }

    public String getClassName(){
        return get(Properties.CLASS_NAME);
    }

    public Long getObjectId() {
        return get(Properties.OBJECT_ID);
    }

}
