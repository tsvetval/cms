package ru.peak.ml.web.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.util.MlUtil;

import java.util.List;

/**
 *
 */
public class MlHistoryUtil extends MlUtil {

    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public static final String FIELDS_FOR_SHOW_IN_HISTORY = "attrsForShowInHistory";


    public MlHistoryUtil() {
    }

    public List<MlAttr> getFieldsForShowInHistory() {
        return get(FIELDS_FOR_SHOW_IN_HISTORY);
    }


}