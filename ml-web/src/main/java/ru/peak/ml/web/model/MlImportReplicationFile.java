package ru.peak.ml.web.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.system.MlAttr;

import java.util.Date;

/**
 *
 */
public class MlImportReplicationFile extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    public Long getId(){
        return get("id");
    }

    public String getName(){
        return get("name");
    }

    public void setName(String name){
        set("name",name);
    }

    public void setReplicationFile(byte[] file){
        set("replicationFile",file);
    }
    public void setReplicationFileName(String fileName){
        set("replicationFile"+ MlAttr.FILE_NAME_POSTFIX,fileName);
    }

    public void setReportFile(byte[] file){
        set("reportFile",file);
    }
    public void setReportFileName(String fileName){
        set("reportFile"+ MlAttr.FILE_NAME_POSTFIX,fileName);
    }

    public void setDateLoad(Date date){
        set("dateLoad",date);
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
