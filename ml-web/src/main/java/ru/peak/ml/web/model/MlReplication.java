package ru.peak.ml.web.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.system.MlAttr;

import java.util.List;

/**
 *
 */
public class MlReplication extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    public Long getId(){
        return get("id");
    }

    public String getName(){
        return get("name");
    }

    public void setName(String name) {
        set("name", name);
    }

    public MlReplication getMlReplication(){
        return get("replication");
    }

    public List<MlReplicationStep> getReplicationSteps(){
        return get("replicationSteps");
    }

    public void setFile(byte[] file){
        set("replicationFile",file);
    }
    public void setFileName(String fileName){
        set("replicationFile"+ MlAttr.FILE_NAME_POSTFIX,fileName);
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
