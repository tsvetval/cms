package ru.peak.ml.web.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;

import java.util.List;

/**
 *
 */
public class MlReplicationStep extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    public Long getId() {
        return get("id");
    }

    public Long getStepNumber() {
        return get("stepNumber");
    }

    public String getName() {
        return get("name");
    }

    public void setName(String name) {
        set("name", name);
    }

    public MlReplication getMlReplication() {
        return get("replication");
    }

    public Boolean getOffHandlers() {
        return get("offHandlers");
    }

    public void setOffHandlers(Boolean offHandlers) {
        set("offHandlers", offHandlers);
    }

    public List<MlHeteroLink> getObjects(){
        return get("objects");
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String toString() {
        return (new StringBuffer()).append("MlReplicationStep(Id = ").append(getId()).append(", name = ").append(getName()).
                append(", offHandlers = ").append(getOffHandlers()).
                append(", stepNumber = ").append(getStepNumber()).toString();
    }

    public void setReplication(MlReplication replication) {
        set("replication", replication);
    }

    public void setStepNumber(Long stepNumber) {
        set("stepNumber", stepNumber);
    }

    public void setReinitialization(boolean reinitialization) {
        set("reinitialization", reinitialization);
    }
}
