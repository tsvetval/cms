package ru.peak.ml.web.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.MLUID;

/**
 *
 */
public class MlReplicationStepObject extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    public Long getId() {
        return get("id");
    }

    public MLUID getMlUID() {
        return new MLUID(get("mluid").toString());
    }

    public MlReplicationStep getMlReplicationStep() {
        return get("replicationStep");
    }

    public void setMlUID(String mlUID) {
        set("mluid", mlUID);
    }

    public void setMlReplicationStep(MlReplicationStep mlReplicationStep) {
        set("replicationStep", mlReplicationStep);
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String toString() {
        return (new StringBuffer()).append("MlReplicationObject(Id = ").append(getId()).append(", MLUID = ").append(get("mluid").toString()).toString();
    }
}
