package ru.peak.ml.web.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;

import java.util.Date;

/**
 * Периодическое задание
 */
public class MlScheduled extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    protected static class Properties{
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String CRON = "cron";
        public static final String JOB_CLASS = "jobClass";
        public static final String LAST_RESULT = "lastResult";
        public static final String LAST_START = "lastStart";
        public static final String IS_ACTIVE = "isActive";
    }

    public Long getId(){
        return get(Properties.ID);
    }

    public String getName(){
        return get(Properties.NAME);
    }

    public void setName(String name){
        set(Properties.NAME, name);
    }

    public String getCron(){
        return get(Properties.CRON);
    }

    public String getJobClass(){
        return get(Properties.JOB_CLASS);
    }

    public String getLastResult(){
        return get(Properties.LAST_RESULT);
    }

    public Date getLastStart(){
        return get(Properties.LAST_START);
    }

    public String isActive(){
        return get(Properties.IS_ACTIVE);
    }

    public void setActive(boolean active) {
        set(Properties.IS_ACTIVE, active);
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }
}
