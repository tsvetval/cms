package ru.peak.ml.web.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;

/**
 * Created by d_litovchenko on 28.01.15.
 */
public class MlScheduledSQL extends MlScheduled {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }

    protected static class Properties{
        public static final String SQL = "sql";
    }

    public String getSql(){
        return get(Properties.SQL);
    }
}
