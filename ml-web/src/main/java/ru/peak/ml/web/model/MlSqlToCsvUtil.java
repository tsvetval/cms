package ru.peak.ml.web.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.util.MlUtil;

/**
 *
 */
public class MlSqlToCsvUtil extends MlUtil {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getSql(){
        return get("sql");
    }
    public String getFileEncoding(){
        return get("fileEncoding");
    }
    public String getColumnSeparator(){
        return get("columnSeparator");
    }
    public String getFileName(){
        return get("fileName");
    }
}
