package ru.peak.ml.web.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.page.MlPageBlockBase;

/**
 *
 */
public class MlStaticPageBlock extends MlPageBlockBase {

    public static final String TEMPLATE = "template";
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String  getTemplatePath(){
        return this.get(TEMPLATE);
    }
}