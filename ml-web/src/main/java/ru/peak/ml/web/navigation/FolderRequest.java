package ru.peak.ml.web.navigation;


public class FolderRequest {

    private Long folderId;
    private String classifierId;
    private String classifierValue;

    public FolderRequest() {
    }

    public FolderRequest(Long folderId) {
        this.folderId = folderId;
    }

    public FolderRequest(Long folderId, String classifierId, String classifierValue) {
        this.folderId = folderId;
        this.classifierId = classifierId;
        this.classifierValue = classifierValue;
    }

    public Long getFolderId() {
        return folderId;
    }

    public void setFolderId(Long folderId) {
        this.folderId = folderId;
    }

    public String getClassifierId() {
        return classifierId;
    }

    public void setClassifierId(String classifierId) {
        this.classifierId = classifierId;
    }

    public String getClassifierValue() {
        return classifierValue;
    }

    public void setClassifierValue(String classifierValue) {
        this.classifierValue = classifierValue;
    }
}
