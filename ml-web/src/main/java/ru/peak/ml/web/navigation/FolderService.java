package ru.peak.ml.web.navigation;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.ImplementedBy;
import ru.peak.ml.web.navigation.impl.FolderServiceImpl;
import ru.peak.ml.web.navigation.model.FolderCondition;

@ImplementedBy(FolderServiceImpl.class)
public interface FolderService {
    public JsonElement serializeFolders(FolderRequest folderRequest);

    public FolderCondition createCondition(FolderRequest folderRequest);

    public JsonElement serializeCurrentFolder(FolderRequest folderRequest);
}
