package ru.peak.ml.web.navigation.classifier;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.ImplementedBy;
import ru.peak.ml.core.model.folder.MlFolder;
import ru.peak.ml.web.navigation.FolderRequest;
import ru.peak.ml.web.navigation.classifier.impl.ClassifierServiceImpl;
import ru.peak.ml.web.navigation.model.FolderCondition;
import ru.peak.ml.web.navigation.model.FolderDTO;

import java.util.List;

@ImplementedBy(ClassifierServiceImpl.class)
public interface ClassifierService {

    public FolderCondition createCondition(MlFolder mlFolder, String classifierId, String value);

    public List<FolderDTO> getFolderDTOs(FolderRequest folderRequest);

    public FolderDTO getCurrentFolderDTO(FolderRequest folderRequest);

}
