package ru.peak.ml.web.navigation.classifier.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.model.folder.MlFolder;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.web.dao.impl.FolderDao;
import ru.peak.ml.web.helper.IconHelper;
import ru.peak.ml.web.navigation.FolderRequest;
import ru.peak.ml.web.navigation.classifier.Classifier;
import ru.peak.ml.web.navigation.classifier.ClassifierService;
import ru.peak.ml.web.navigation.model.FolderCondition;
import ru.peak.ml.web.navigation.model.FolderDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClassifierServiceImpl implements ClassifierService {
    @Inject
    CommonDao commonDao;
    @Inject
    FolderDao folderDao;

    Classifier classifier;
    List<Classifier> classifiersBefore = new ArrayList<>();
    Gson gson = new Gson();


    @Override
    public FolderDTO getCurrentFolderDTO(FolderRequest folderRequest) {
        MlFolder mlFolder = folderDao.findById(folderRequest.getFolderId());
        List<Classifier> classifiers = gson.fromJson(mlFolder.getClassifierJSON(), new TypeToken<List<Classifier>>() {
        }.getType());
        findClassifier(classifiers, folderRequest.getClassifierId());
        fillValuesClassifierBefore(folderRequest.getClassifierValue());
        FolderDTO folderDTO = new FolderDTO();
        JsonObject id = new JsonObject();
        Classifier currentClassifier = classifiersBefore.get(classifiersBefore.size()-1);
        id.add("folderId", new JsonPrimitive(mlFolder.getId()));
        id.add("classifierId", new JsonPrimitive(currentClassifier.getId()));
        id.add("value", new JsonPrimitive(folderRequest.getClassifierValue()));
        List<Long> blockIds = new ArrayList<>();
        for (MlPageBlockBase blockBase : mlFolder.getPageBlocks()) {
            blockIds.add(blockBase.getId());
        }

        folderDTO.id = gson.toJson(id);
        folderDTO.text = currentClassifier.getValue();
        folderDTO.children = currentClassifier.hasNext();
        folderDTO.icon = generateIcon(mlFolder);
        folderDTO.pageBlocks = blockIds;

        Classifier classifierBefore = findClassifierBefore(currentClassifier);
        if(classifierBefore != null){
            JsonObject parentId = new JsonObject();
            parentId.add("folderId", new JsonPrimitive(mlFolder.getId()));
            parentId.add("classifierId", new JsonPrimitive(classifierBefore.getId()));
            parentId.add("value", new JsonPrimitive(classifierBefore.getValue()));
            folderDTO.parent = gson.toJson(parentId);
        }else{
            folderDTO.parent = folderRequest.getFolderId().toString();
        }
        return folderDTO;
    }

    private Classifier findClassifierBefore(Classifier currentClassifier) {
        Classifier result = null;
        for(Classifier classifier: classifiersBefore){
            if(classifier.getId().equals(currentClassifier.getId())){
                break;
            }
            result = classifier;
        }
        return  result;
    }

    @Override
    public List<FolderDTO> getFolderDTOs(FolderRequest folderRequest) {
        List<FolderDTO> folderDTOs = new ArrayList<>();
        if (folderRequest.getFolderId() != null) {
            MlFolder mlFolder = folderDao.findById(folderRequest.getFolderId());
            if (mlFolder.isClassifier()) {
                List<String> folderNames = getFolderNames(mlFolder, folderRequest.getClassifierId(), folderRequest.getClassifierValue());
                List<Long> blockIds = new ArrayList<>();
                for (MlPageBlockBase blockBase : mlFolder.getPageBlocks()) {
                    blockIds.add(blockBase.getId());
                }
                folderNames.forEach(folderName -> {
                    FolderDTO folderDTO = new FolderDTO();
                    JsonObject id = new JsonObject();
                    id.add("folderId", new JsonPrimitive(mlFolder.getId()));
                    id.add("classifierId", new JsonPrimitive(classifier.getId()));
                    id.add("value", new JsonPrimitive((StringUtils.isEmpty(folderRequest.getClassifierValue()) ? "" : folderRequest.getClassifierValue() + "//") + folderName));
                    folderDTO.id = gson.toJson(id);
                    folderDTO.text = folderName;
                    folderDTO.children = classifier.hasNext();
                    folderDTO.icon = generateIcon(mlFolder);
                    folderDTO.pageBlocks = blockIds;

                    if(!StringUtils.isEmpty(folderRequest.getClassifierId())){
                        JsonObject parentId = new JsonObject();
                        parentId.add("folderId", new JsonPrimitive(mlFolder.getId()));
                        parentId.add("classifierId", new JsonPrimitive(folderRequest.getClassifierId()));
                        parentId.add("value", new JsonPrimitive(folderRequest.getClassifierValue()));
                        folderDTO.parent = gson.toJson(parentId);
                    }else{
                        folderDTO.parent = folderRequest.getFolderId().toString();
                    }

                    folderDTOs.add(folderDTO);
                });
            }
        }
        return folderDTOs;
    }

    private List<String> getFolderNames(MlFolder mlFolder, String classifierId, String value) {
        List<Classifier> classifiers = gson.fromJson(mlFolder.getClassifierJSON(), new TypeToken<List<Classifier>>() {
        }.getType());
        findClassifier(classifiers, classifierId);
        List<String> folderNames = new ArrayList<>();
        if (classifier != null) {
            fillValuesClassifierBefore(value);
            folderNames = findFolderNames(mlFolder.getChildClass());
        }
        return folderNames;
    }


    private void fillValuesClassifierBefore(String value) {
        if (!StringUtils.isEmpty(value)) {
            String[] values = value.split("//");
            for (int i = 0; i < values.length; i++) {
                classifiersBefore.get(i).setValue(values[i]);
            }
        }
    }

    @Override
    public FolderCondition createCondition(MlFolder mlFolder, String classifierId, String value) {
        FolderCondition condition = new FolderCondition();
        List<Classifier> classifiers = gson.fromJson(mlFolder.getClassifierJSON(), new TypeToken<List<Classifier>>() {
        }.getType());
        findClassifier(classifiers, classifierId);
        fillValuesClassifierBefore(value);

        StringBuilder builder = new StringBuilder("");

        for (Classifier parentClassifier : classifiersBefore) {
            if (builder.length() > 0) {
                builder.append(" and ");
            }
            if (StringUtils.isEmpty(parentClassifier.getSelectValue())) {
                builder.append("o.").append(parentClassifier.getAttrName());
            } else {
                builder.append(parentClassifier.getSelectValue());
            }
            String paramName = "condition_"+parentClassifier.getId();
            builder.append(" = :").append(paramName).append(" ");
            condition.getParameters().put(paramName,parentClassifier.getValue());
        }
        condition.setCondition(builder.toString());
        return condition;
    }

    private JsonArray createResult(List<String> folderNames, MlFolder mlFolder, String value) {
        JsonArray result = new JsonArray();
        JsonArray pageBlocks = new JsonArray();


        folderNames.forEach(name -> {
            JsonObject jsonObject = new JsonObject();
            JsonObject dataJson = new JsonObject();
            dataJson.add("populated", new JsonPrimitive(false));
            JsonObject id = new JsonObject();
            id.add("folderId", new JsonPrimitive(mlFolder.getId()));
            id.add("classifierId", new JsonPrimitive(classifier.getId()));
            id.add("value", new JsonPrimitive((StringUtils.isEmpty(value) ? "" : value + "//") + name));


            jsonObject.add("id", new JsonPrimitive(gson.toJson(id)));

            jsonObject.add("text", new JsonPrimitive(name));

            jsonObject.add("children", new JsonPrimitive(classifier.hasNext()));

            jsonObject.add("data", dataJson);
            jsonObject.add("pageBlocks", pageBlocks);
            result.add(jsonObject);
        });
        return result;
    }


    private List<String> findFolderNames(MlClass fromClass) {
        String distinctJPQL = createQuery(classifier, fromClass);
        List<String> result = commonDao.getQueryWithoutSecurityCheck(distinctJPQL).getResultList();
        return result;
    }

    private String createQuery(Classifier classifier, MlClass fromClass) {
        StringBuilder builder = new StringBuilder("select distinct ");
        if (classifier.getSelectValue() == null || "".equals(classifier.getSelectValue())) {
            MlAttr attr = fromClass.getAttr(classifier.getAttrName());
            builder.append(" o.").append(attr.getEntityFieldName()).append(" ");
        } else {
            builder.append(classifier.getSelectValue()).append(" ");
        }
        builder.append("from ").append(fromClass.getEntityName()).append(" o ");
        builder.append("where 1=1 ");
        if (classifier.getWhereCondition() != null && !"".equals(classifier.getWhereCondition())) {
            builder.append(" and ").append(classifier.getWhereCondition());
        }
        for (Classifier parentClassifier : classifiersBefore) {
            builder.append(" and ");
            if (StringUtils.isEmpty(parentClassifier.getSelectValue())) {
                builder.append("o.").append(parentClassifier.getAttrName());
            } else {
                builder.append(parentClassifier.getSelectValue());
            }
            builder.append(" = ").append("'").append(parentClassifier.getValue()).append("' ");
        }

        if (classifier.getOrderCondition() != null && !"".equals(classifier.getOrderCondition())) {
            builder.append(" order ").append(classifier.getWhereCondition());
        }
        return builder.toString();
    }

    private void findClassifier(List<Classifier> classifiers, String classifierId) {
        classifier = null;
        classifiersBefore.clear();
        if (StringUtils.isEmpty(classifierId)) {
            classifier = classifiers.get(0);
            classifier.setHasNext(1 < classifiers.size());
        } else {
            for (int i = 0; i < classifiers.size(); i++) {
                if (classifiers.get(i).getId().equals(classifierId)) {
                    classifiersBefore.add(classifiers.get(i));
                    i++;
                    if (i < classifiers.size()) {
                        classifier = classifiers.get(i);
                        i++;
                        classifier.setHasNext(i < classifiers.size());
                    }
                    break;
                } else {
                    classifiersBefore.add(classifiers.get(i));
                }
            }
        }
    }

    private String generateIcon(MlFolder folder){
        String icon = null;
        if (folder.getIconURL() != null) {
            icon = IconHelper.getNavigationIconURL( "public/icons/" + folder.getIconURL());
        }
        if (StringUtils.isEmpty(icon)) {
            icon = folder.getIcon() != null? folder.getIcon(): "glyphicon glyphicon-folder-open";
        }
        return icon;
    }
}
