package ru.peak.ml.web.navigation.impl;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.model.folder.MlFolder;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.services.AccessService;
import ru.peak.ml.web.dao.impl.FolderDao;
import ru.peak.ml.web.helper.IconHelper;
import ru.peak.ml.web.navigation.FolderRequest;
import ru.peak.ml.web.navigation.FolderService;
import ru.peak.ml.web.navigation.classifier.ClassifierService;
import ru.peak.ml.web.navigation.model.FolderCondition;
import ru.peak.ml.web.navigation.model.FolderDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class FolderServiceImpl implements FolderService {

    @Inject
    ClassifierService classifierService;
    @Inject
    FolderDao folderDao;
    @Inject
    AccessService accessService;


    @Override
    public JsonElement serializeFolders(FolderRequest folderRequest) {
        List<MlFolder> folderList = folderDao.getChildFolders(folderRequest.getFolderId());
        List<FolderDTO> folderDTOs = new ArrayList<>();
        for (MlFolder folder : folderList) {
            FolderDTO folderDTO = new FolderDTO();
            folderDTO.id = folder.getId().toString();
            folderDTO.text = folder.getTitle();
            folderDTO.children = hasChildren(folder);
            folderDTO.icon = generateIcon(folder);
            folderDTO.url = folder.getUrl();
            if (folder.getParent() == null){
                folderDTO.parent = null;
            } else {
            // Проверяем доступ к парентовой папке если его нет то передаем null
                if (!accessService.checkAccessFolder(folder.getParent())){
                    folderDTO.parent = null;
                } else {
                    folderDTO.parent = folder.getParent().getId().toString();
                }
            }
            //folderDTO.parent = folder.getParent() == null ? null : folder.getParent().getId().toString();
            for (MlPageBlockBase blockBase : folder.getPageBlocks()) {
                folderDTO.pageBlocks.add(blockBase.getId());
            }
            folderDTOs.add(folderDTO);
        }
        folderDTOs.addAll(classifierService.getFolderDTOs(folderRequest));
        Gson gson = new Gson();
        return gson.toJsonTree(folderDTOs);
    }

    @Override
    public FolderCondition createCondition(FolderRequest folderRequest) {
        MlFolder mlFolder = folderDao.findById(folderRequest.getFolderId());
        return classifierService.createCondition(mlFolder, folderRequest.getClassifierId(), folderRequest.getClassifierValue());
    }

    @Override
    public JsonElement serializeCurrentFolder(FolderRequest folderRequest) {
        MlFolder mlFolder = folderDao.findById(folderRequest.getFolderId());
        FolderDTO folderDTO;
        if (StringUtils.isEmpty(folderRequest.getClassifierId())) {
            folderDTO = new FolderDTO();
            folderDTO.id = mlFolder.getId().toString();
            folderDTO.text = mlFolder.getTitle();
            folderDTO.children = hasChildren(mlFolder);
            folderDTO.icon = generateIcon(mlFolder);
            folderDTO.url = mlFolder.getUrl();
            folderDTO.parent = mlFolder.getParent() == null ? null : mlFolder.getParent().getId().toString();
            for (MlPageBlockBase blockBase : mlFolder.getPageBlocks()) {
                folderDTO.pageBlocks.add(blockBase.getId());
            }
        } else {
            folderDTO = classifierService.getCurrentFolderDTO(folderRequest);
        }
        Gson gson = new Gson();
        return gson.toJsonTree(folderDTO);
    }

    private boolean hasChildren(MlFolder folder) {
        return (folder.getChildFolders() != null &&
                !accessService.checkAccessFolder(folder.getChildFolders()).isEmpty()) || (
                folder.isClassifier() && accessService.checkAccessFolder(folder));
    }

    private String generateIcon(MlFolder folder) {
        String icon = null;
        if (folder.getIconURL() != null) {
            //TODO брать контекст не из реквеста
            HttpServletRequest request = GuiceConfigSingleton.inject(HttpServletRequest.class);
            icon = IconHelper.getNavigationIconURL(request.getContextPath() + "/public/icons/" + folder.getIconURL());
        }
        if (StringUtils.isEmpty(icon)) {
            icon = folder.getIcon() != null ? folder.getIcon() : "glyphicon glyphicon-folder-open";
        }
        return icon;
    }
}
