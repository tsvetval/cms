package ru.peak.ml.web.navigation.model;

import java.util.HashMap;
import java.util.Map;

public class FolderCondition {
    private String condition;
    private Map<String,Object> parameters = new HashMap<>();

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }
}
