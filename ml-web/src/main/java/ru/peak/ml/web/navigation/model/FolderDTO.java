package ru.peak.ml.web.navigation.model;

import java.util.ArrayList;
import java.util.List;

public class FolderDTO {
    public String id;
    public String text;
    public Boolean children;
    public List<Long> pageBlocks = new ArrayList<>();
    public String icon;
    public String parent;
    public String url;
}
