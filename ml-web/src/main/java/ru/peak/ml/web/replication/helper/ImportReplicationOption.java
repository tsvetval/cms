package ru.peak.ml.web.replication.helper;

/**
 */
public class ImportReplicationOption {
    private ImportType importType;

    public ImportReplicationOption(ImportType importType) {
        this.importType = importType;
    }

    public ImportType getImportType() {
        return importType;
    }

    public void setImportType(ImportType importType) {
        this.importType = importType;
    }

    public enum ImportType {
        ONLY_CREATE("Создавать"),
        CREATE_UPDATE("Создавать и обновлять"),
        REWRITE("Обновлять связи");

        private String label;

        private ImportType(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }
}
