package ru.peak.ml.web.replication.helper;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class Replication {
    private String name;
    private String fileName;
    private List<ReplicationStep> replicationSteps = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public List<ReplicationStep> getReplicationSteps() {
        return replicationSteps;
    }

    public void setReplicationSteps(List<ReplicationStep> replicationSteps) {
        this.replicationSteps = replicationSteps;
    }

    @Override
    public String toString() {
        return (new StringBuffer()).append(" Replication(name = ").append(getName()).append(", fileName = ").append(getFileName()).append(")").toString();
    }
}
