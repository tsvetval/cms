package ru.peak.ml.web.replication.helper;

import java.util.Date;

/**
 */
public class ReplicationObject {
    private String mluid;
    private String note;
    private Date lastChangeDate;
    private Type type;

    public String getMluid() {
        return mluid;
    }

    public void setMluid(String mluid) {
        this.mluid = mluid;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getLastChangeDate() {
        return lastChangeDate;
    }

    public void setLastChangeDate(Date lastChangeDate) {
        this.lastChangeDate = lastChangeDate;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return (new StringBuffer()).append(" ReplicationObject(mluid = ").append(getMluid()).append(", lastChangeDate = ").append(getLastChangeDate())
                .append(")").toString();
    }

    public enum Type{
        NO_REFRESH,REFRESH
    }
}
