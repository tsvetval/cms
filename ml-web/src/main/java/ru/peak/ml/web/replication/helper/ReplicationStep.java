package ru.peak.ml.web.replication.helper;

import ru.ml.core.common.exceptions.MlServerException;
import ru.peak.ml.core.model.MLUID;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class ReplicationStep {
    private String name;
    private Long number;
    private Boolean reinitiaslize;
    private Boolean offHandler;
    private List<ReplicationObject> replicationObjects = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getReinitiaslize() {
        return reinitiaslize;
    }

    public void setReinitiaslize(Boolean reinitiaslize) {
        this.reinitiaslize = reinitiaslize;
    }

    public Boolean getOffHandler() {
        return offHandler;
    }

    public void setOffHandler(Boolean offHandler) {
        this.offHandler = offHandler;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public List<ReplicationObject> getReplicationObjects() {
        return replicationObjects;
    }

    public void setReplicationObjects(List<ReplicationObject> replicationObjects) {
        this.replicationObjects = replicationObjects;
    }


    public ReplicationObject getReplicationObjectByMLUID(MLUID mluid){
        for(ReplicationObject replicationObject: getReplicationObjects()){
            if(replicationObject.getMluid().equals(mluid.toString())){
                return replicationObject;
            }
        }
        throw new MlServerException("Ошибка репликации");
    }

    @Override
    public String toString() {
        return (new StringBuffer()).append(" ReplicationStep(name = ").append(getName()).append(", number = ").append(getNumber())
                .append(", reinit = ").append(getReinitiaslize()).append(", offHandler = ").append(getOffHandler()).append(")").toString();
    }
}
