
package ru.peak.ml.web.replication.stub;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.peak.ml.web.replication.stub package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.peak.ml.web.replication.stub
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ReplicationBlock }
     * 
     */
    public ReplicationBlock createReplicationBlock() {
        return new ReplicationBlock();
    }

    /**
     * Create an instance of {@link ReplicationBlock.ReplicationStep }
     * 
     */
    public ReplicationBlock.ReplicationStep createReplicationBlockReplicationStep() {
        return new ReplicationBlock.ReplicationStep();
    }

    /**
     * Create an instance of {@link ReplicationBlock.ReplicationStep.ReplicationObject }
     * 
     */
    public ReplicationBlock.ReplicationStep.ReplicationObject createReplicationBlockReplicationStepReplicationObject() {
        return new ReplicationBlock.ReplicationStep.ReplicationObject();
    }

    /**
     * Create an instance of {@link ReplicationBlock.ReplicationStep.ReplicationObject.Property }
     * 
     */
    public ReplicationBlock.ReplicationStep.ReplicationObject.Property createReplicationBlockReplicationStepReplicationObjectProperty() {
        return new ReplicationBlock.ReplicationStep.ReplicationObject.Property();
    }

}
