package ru.peak.ml.web.replication.stub;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.ByteArrayInputStream;
import java.io.StringWriter;

/**
 * Created with IntelliJ IDEA.
 * User: d_litovchenko
 * Date: 07.07.14
 * Time: 13:07
 * To change this template use File | Settings | File Templates.
 */
public class SchemeConverter {

    protected JAXBContext context;
    protected Schema schema;
    private static final String CONTEXT_PATH = "ru.peak.ml.web.replication.stub";

    public SchemeConverter() throws JAXBException, SAXException {
        context = JAXBContext.newInstance(CONTEXT_PATH);
        schema = SchemaFactory.newInstance(
                XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(
                getClass().getClassLoader().getResource("replication.xsd"));
    }

    public String ToXml(Object message) throws JAXBException {
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
        marshaller.setSchema(schema);
        StringWriter sw = new StringWriter();
        marshaller.marshal(message, sw);
        String result = sw.toString();
        return result;
    }

    @SuppressWarnings("unchecked")
    public <T> T fromXml(byte[] content) throws JAXBException {
        Unmarshaller unmarshaller = context.createUnmarshaller();
        unmarshaller.setSchema(schema);
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(content);
        return (T) unmarshaller.unmarshal(byteArrayInputStream);
    }

}
