package ru.peak.ml.web.schedule;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import ru.peak.ml.web.model.MlScheduled;

/**
 * Абстрактный класс-исполнителя периодического задания, с методами выполняющими подготовку окружения
 */
public abstract class AbstractScheduleJob implements ScheduleJob {

    private JobExecutionContext context;
    @Override
    public void doBefore(){

    }

    @Override
    public void doAfter(){

    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        this.context = context;
        doBefore();
        executeJob(context);
        doAfter();
    }

    protected  <T extends MlScheduled> T getScheduledMeta(){
        return (T) context.getJobDetail().getJobDataMap().get(ScheduleJob.MlScheduled);
    }

    protected abstract void executeJob(JobExecutionContext context);

}
