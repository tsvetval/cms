package ru.peak.ml.web.schedule;

import org.quartz.JobExecutionContext;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.web.model.MlScheduled;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Пример периодического задания. Подлежит удалению.
 */
public class ExampleScheduleJob extends AbstractScheduleJob {

    private static final SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");

    @Override
    protected void executeJob(JobExecutionContext context){
        createUnactiveSchedule();
    }

    private void createUnactiveSchedule(){
        CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        MlScheduled tmpScheduled = (MlScheduled) commonDao.createNewEntity(MlScheduled.class);
        String name = format.format(new Date());
        tmpScheduled.setName(name);
        tmpScheduled.setActive(false);
        commonDao.persistTransactionalWithoutSecurityCheck(tmpScheduled);
    }

}
