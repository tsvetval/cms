package ru.peak.ml.web.schedule;

import org.quartz.JobExecutionContext;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.web.model.MlScheduled;
import ru.peak.ml.web.model.MlScheduledSQL;

import javax.persistence.EntityManager;

/**
 * Created by d_litovchenko on 28.01.15.
 */
public class SQLScheduleJob extends AbstractScheduleJob {

    @Override
    protected void executeJob(JobExecutionContext context) {
        MlScheduledSQL mlScheduled = getScheduledMeta();
        String sql = mlScheduled.getSql();
        String operation = sql.substring(0,sql.indexOf(" "));
        EntityManager entityManager = GuiceConfigSingleton.inject(EntityManager.class);
        entityManager.getTransaction().begin();
        if(operation.equalsIgnoreCase("select")){
            //todo непонятно что с селектами делать.
        }else{
            entityManager.createNativeQuery(mlScheduled.getSql()).executeUpdate();
        }
        entityManager.getTransaction().commit();
    }
}
