package ru.peak.ml.web.schedule;

import org.quartz.Job;

/**
 * Класс-исполнитель периодического задания.
 */
public interface ScheduleJob extends Job {
    public static final String MlScheduled = "MlScheduled";
    public void doBefore();
    public void doAfter();
}
