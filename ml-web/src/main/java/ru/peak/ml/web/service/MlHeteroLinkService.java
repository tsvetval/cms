package ru.peak.ml.web.service;

import com.google.inject.ImplementedBy;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.web.model.MlHeteroLink;
import ru.peak.ml.web.service.impl.MlHeteroLinkServiceImpl;

/**
 * Created by d_litovchenko on 23.03.15.
 */
@ImplementedBy(MlHeteroLinkServiceImpl.class)
public interface MlHeteroLinkService {
    public MlDynamicEntityImpl getObjectHeteroLink(MlHeteroLink link);

}
