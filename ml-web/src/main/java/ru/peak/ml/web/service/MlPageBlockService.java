package ru.peak.ml.web.service;

import com.google.inject.ImplementedBy;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.peak.ml.web.service.impl.MlPageBlockServiceImpl;

/**
 *
 */
@ImplementedBy(MlPageBlockServiceImpl.class)
public interface MlPageBlockService {
    public void processRequest(MlHttpServletRequest req, MlHttpServletResponse resp) throws MlApplicationException, MlServerException;

}
