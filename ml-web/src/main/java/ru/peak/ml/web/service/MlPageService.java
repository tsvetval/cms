package ru.peak.ml.web.service;

import com.google.inject.ImplementedBy;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.service.impl.MlPageServiceImpl;

import java.io.IOException;

/**
 *
 */
@ImplementedBy(MlPageServiceImpl.class)
public interface MlPageService {

    public void processRequest(MlHttpServletRequest req, MlHttpServletResponse resp) throws IOException;

}
