package ru.peak.ml.web.service;

import com.google.inject.ImplementedBy;
import org.apache.commons.compress.archivers.ArchiveException;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.peak.ml.web.model.MlReplication;
import ru.peak.ml.web.replication.helper.ImportReplicationOption;
import ru.peak.ml.web.replication.helper.Replication;
import ru.peak.ml.web.replication.stub.ReplicationBlock;
import ru.peak.ml.web.service.impl.MlReplicationServiceImpl;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: d_litovchenko
 * Date: 04.07.14
 * Time: 17:24
 * To change this template use File | Settings | File Templates.
 */
@ImplementedBy(MlReplicationServiceImpl.class)
public interface MlReplicationService {

    public byte[] doReplication(MlReplication mlReplication) throws JAXBException, DatatypeConfigurationException;
    public ReplicationBlock getReplicationInfo(byte[] file) throws JAXBException;

    public Replication importReplication(byte[] zipFile, Boolean onlyGenerateResultFile, ImportReplicationOption option) throws IOException, JAXBException, MlApplicationException, ArchiveException;
}
