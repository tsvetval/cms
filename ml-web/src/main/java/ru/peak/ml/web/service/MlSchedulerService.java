package ru.peak.ml.web.service;

import com.google.inject.ImplementedBy;
import ru.peak.ml.web.service.impl.MlSchedulerServiceImpl;

/**
 * Created with IntelliJ IDEA.
 * User: a_gumenyuk
 * Date: 23.07.14
 * Time: 15:30
 * Сервис выполнения периодических задач
 */
@ImplementedBy(MlSchedulerServiceImpl.class)
public interface MlSchedulerService {

    public void start();

    public void shutdown();

    public void startScheduledJob(Long objectId);

    public void stopScheduledJob(Long objectId);
}
