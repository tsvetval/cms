package ru.peak.ml.web.service.annotations;

import ru.peak.ml.web.common.MlHttpServletResponse;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;

/**
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({METHOD})
public @interface PageBlockAction {
    String action() default "";
    MlHttpServletResponse.DataType dataType() default MlHttpServletResponse.DataType.JSON;
}
