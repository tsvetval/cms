package ru.peak.ml.web.service.impl;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlServerException;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.initializer.MlMetaDataInitializeService;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.web.model.MlHeteroLink;
import ru.peak.ml.web.service.MlHeteroLinkService;

/**
 * Created by d_litovchenko on 23.03.15.
 */
public class MlHeteroLinkServiceImpl implements MlHeteroLinkService {
    private static final Logger log = LoggerFactory.getLogger(MlHeteroLinkServiceImpl.class);

    @Inject
    CommonDao commonDao;
    @Inject
    MetaDataHolder metaDataHolder;

    @Override
    public MlDynamicEntityImpl getObjectHeteroLink(MlHeteroLink link) {
        MlDynamicEntityImpl entity = commonDao.findById(link.getObjectId(),metaDataHolder.getEntityClassByName(link.getClassName()));
        if(entity == null){
            log.error("MlHeteroLink not found "+link.getStrId());
            throw new MlServerException("Невозможно найти гетерогенную ссылку "+link.getStrId());
        }
        return entity;
    }
}
