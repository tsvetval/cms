package ru.peak.ml.web.service.impl;

import com.google.inject.persist.Transactional;
import org.eclipse.persistence.dynamic.DynamicEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.handler.MlReplicationHandler;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.initializer.MlMetaDataInitializeService;
import ru.peak.ml.core.model.MLUID;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.web.model.MlHeteroLink;
import ru.peak.ml.web.replication.helper.ImportReplicationOption;
import ru.peak.ml.web.replication.helper.Replication;
import ru.peak.ml.web.replication.helper.ReplicationObject;
import ru.peak.ml.web.replication.helper.ReplicationStep;
import ru.peak.ml.web.replication.stub.ReplicationBlock;
import ru.peak.ml.web.service.MlHeteroLinkService;
import sun.misc.BASE64Decoder;

import java.io.IOException;
import java.util.*;

/**
 *
 */
public class MlImportReplicationImpl {
    private static final Logger log = LoggerFactory.getLogger(MlImportReplicationImpl.class);
    private CommonDao commonDao;

    Map<MLUID, Map<String, List<MLUID>>> manyToMany = new HashMap<>();
    Set<MlClass> mlClassForInitialization = new HashSet<>();

    @Transactional
    public Replication doImport(ReplicationBlock block, Boolean onlyGenerateResultFile, ImportReplicationOption option) throws MlApplicationException {
        Replication replication = new Replication();
        replication.setName(block.getName());
        setReplicationSteps(replication, block, option, onlyGenerateResultFile);
        log.debug(String.format("Import Replication %s", replication.toString()));
        return replication;
    }

    private void setReplicationSteps(Replication replication, ReplicationBlock block, ImportReplicationOption option, Boolean onlyGenerateResultFile) throws MlApplicationException {
        commonDao = GuiceConfigSingleton.inject(CommonDao.class);
        for (ReplicationBlock.ReplicationStep step : block.getReplicationStep()) {
            ReplicationStep replicationStep = new ReplicationStep();
            replicationStep.setNumber(step.getStepNumber());
            replicationStep.setName(step.getName());
            replicationStep.setReinitiaslize(step.isReinitialization());
            replicationStep.setOffHandler(step.isOffHandler());
            log.debug(String.format("Import step %s", replicationStep.toString()));
            setReplicationObjects(replicationStep, step, option, onlyGenerateResultFile);
            replication.getReplicationSteps().add(replicationStep);
            if (!onlyGenerateResultFile) {
                insertManyToMany();
                insertManyToOne(step, replicationStep);
                initHandlers(step);
                initializeMlClasses();
            }
        }

    }

    private void initializeMlClasses() {
        MlMetaDataInitializeService jpaClassInitializer = GuiceConfigSingleton.inject(MlMetaDataInitializeService.class);
        if (!mlClassForInitialization.isEmpty()) {
            try {
                for (MlClass mlClass : mlClassForInitialization) {
                    commonDao.refresh(mlClass);
                }
                jpaClassInitializer.initializeClasses(mlClassForInitialization, false);
            } catch (ClassNotFoundException e) {
                log.error("", e);
            }
        }
    }

    private void initHandlers(ReplicationBlock.ReplicationStep step) {

        for (ReplicationBlock.ReplicationStep.ReplicationObject object : step.getReplicationObject()) {
            MlClass mlClass = GuiceConfigSingleton.inject(MetaDataHolder.class).getMlClassByName(new MLUID(object.getMluid()).getClassName());
            DynamicEntity dynamicEntity = commonDao.getByMlUID(new MLUID(object.getMluid()));
            if (dynamicEntity instanceof MlClass) {
                mlClassForInitialization.add((MlClass) dynamicEntity);
            }
            if (dynamicEntity instanceof MlAttr) {
                mlClassForInitialization.add(((MlAttr) dynamicEntity).getMlClass());
            }
            invokeHandler(mlClass, dynamicEntity);
        }

    }

    private void insertManyToOne(ReplicationBlock.ReplicationStep step, ReplicationStep replicationStep) {
        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);

        for (ReplicationBlock.ReplicationStep.ReplicationObject object : step.getReplicationObject()) {
            MlClass mlClass = metaDataHolder.getMlClassByName(new MLUID(object.getMluid()).getClassName());
            MlDynamicEntityImpl dynamicEntity = commonDao.getByMlUID(new MLUID(object.getMluid()));
            ReplicationObject replicationObject = replicationStep.getReplicationObjectByMLUID(new MLUID(object.getMluid()));
            switch (replicationObject.getType()) {
                case NO_REFRESH:
                    break;
                case REFRESH:
                    for (ReplicationBlock.ReplicationStep.ReplicationObject.Property property : object.getProperty()) {
                        MlAttr attr = metaDataHolder.getAttr(mlClass, property.getEntityFielName());
                        switch (attr.getFieldType()) {
                            case MANY_TO_ONE:
                                DynamicEntity dynamic = commonDao.getByMlUID(new MLUID(property.getValue()));
                                if (dynamic == null) {
                                    throw new MlApplicationException(String.format("Не найден объект по MLUID :%s", property.getValue()));
                                }
                                dynamicEntity.set(property.getEntityFielName(), dynamic);

                                break;
                        }
                    }
                    if (step.isOffHandler()) {
                        commonDao.persistWithoutSecurityCheck(dynamicEntity, false, false);
                    } else {
                        commonDao.persistWithoutSecurityCheck(dynamicEntity);
                    }
            }

        }
    }


    private void setReplicationObjects(ReplicationStep replicationStep, ReplicationBlock.ReplicationStep step, ImportReplicationOption option, Boolean onlyGenerateResultFile) throws MlApplicationException {
        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);

        for (ReplicationBlock.ReplicationStep.ReplicationObject object : step.getReplicationObject()) {
            ReplicationObject replicationObject = new ReplicationObject();
            replicationObject.setMluid(object.getMluid());
            replicationObject.setLastChangeDate(object.getLasChangeDate().toGregorianCalendar().getTime());
            log.debug(String.format("Import object %s", replicationObject.toString()));
            MlClass mlClass = metaDataHolder.getMlClassByName(new MLUID(object.getMluid()).getClassName());
            if (!onlyGenerateResultFile) {
                MlAttr mlattr = metaDataHolder.getAttr(mlClass, MlAttr.GUID);
                if (mlattr == null) {
                    throw new MlApplicationException(String.format("У класса %s отсутствует атрибут guid! Репликация невозможна!", mlClass.getEntityName()));
                }
            }
            if (onlyGenerateResultFile && mlClass == null) {
                replicationObject.setNote("Создан");
                replicationObject.setType(ReplicationObject.Type.REFRESH);
            } else {
                if (mlClass.getAttr("guid") == null) {
                    replicationObject.setNote("Создан");
                    replicationObject.setType(ReplicationObject.Type.REFRESH);
                } else {
                    if (commonDao.getByMlUID(new MLUID(object.getMluid())) != null) {
                        switch (option.getImportType()) {
                            case ONLY_CREATE:
                                replicationObject.setType(ReplicationObject.Type.NO_REFRESH);
                                replicationObject.setNote("Не обновлен");
                                break;
                            case REWRITE:
                                replicationObject.setType(ReplicationObject.Type.REFRESH);
                                replicationObject.setNote("Перезаписан");
                                break;
                            case CREATE_UPDATE:
                                replicationObject.setType(ReplicationObject.Type.REFRESH);
                                replicationObject.setNote("Обновлен");
                                break;
                        }
                    } else {
                        replicationObject.setNote("Создан");
                        replicationObject.setType(ReplicationObject.Type.REFRESH);
                    }
                }
            }
            if (!onlyGenerateResultFile) {
                replicateObject(object, option, step.isOffHandler());
            }
            replicationStep.getReplicationObjects().add(replicationObject);
        }
    }

    private void replicateObject(ReplicationBlock.ReplicationStep.ReplicationObject replicationObject, ImportReplicationOption option, Boolean offHandlers) throws MlApplicationException {
        MetaDataHolder metaDataHolder = GuiceConfigSingleton.inject(MetaDataHolder.class);
        MLUID mluid = new MLUID(replicationObject.getMluid());
        MlClass MlClass = metaDataHolder.getMlClassByName(mluid.getClassName());
        MlDynamicEntityImpl dynamicEntity = commonDao.getByMlUID(mluid);
        if (dynamicEntity == null) {
            dynamicEntity = commonDao.createNewEntity(metaDataHolder.getEntityClassByName(mluid.getClassName()), true);
            dynamicEntity.set(MlAttr.GUID, mluid.getGuid());

            Map<String, List<MLUID>> listMany = fillDynamicEntityNewData(dynamicEntity, MlClass, replicationObject.getProperty());
            manyToMany.put(mluid, listMany);
        } else {
            switch (option.getImportType()) {
                case ONLY_CREATE:
                    break;
                case REWRITE:
                    deleteRelationsInObject(dynamicEntity, MlClass);
                case CREATE_UPDATE:
                    Map<String, List<MLUID>> listMany = fillDynamicEntityNewData(dynamicEntity, MlClass, replicationObject.getProperty());
                    manyToMany.put(mluid, listMany);
            }
        }
        commonDao.persistWithoutSecurityCheck(dynamicEntity, false, false);
    }

    private void invokeHandler(MlClass MlClass, DynamicEntity dynamicEntity) {
        String handlerClassName = MlClass.getReplicationHandler();
        MlReplicationHandler handler = null;
        if (handlerClassName != null && !"".equals(handlerClassName)) {
            Class handlerClass = null;
            try {
                handlerClass = Class.forName(handlerClassName);
            } catch (ClassNotFoundException e) {
                log.error("", e);
            }
            handler = (MlReplicationHandler) GuiceConfigSingleton.inject(handlerClass);
        }


        if (handler != null) {
            handler.afterReplication(dynamicEntity);
        }
    }

    private void deleteRelationsInObject(DynamicEntity dynamicEntity, MlClass MlClass) {
        for (MlAttr attr : MlClass.getAttrSet()) {
            switch (attr.getFieldType()) {
                case MANY_TO_MANY:
                case ONE_TO_MANY:
                    dynamicEntity.set(attr.getEntityFieldName(), new ArrayList<DynamicEntity>());
            }
        }
    }

    private Map<String, List<MLUID>> fillDynamicEntityNewData(DynamicEntity dynamicEntity, MlClass mlClass, List<ReplicationBlock.ReplicationStep.ReplicationObject.Property> properties) throws MlApplicationException {
        Map<String, List<MLUID>> mapMany = new HashMap<>();
        for (ReplicationBlock.ReplicationStep.ReplicationObject.Property property : properties) {
            log.debug(String.format("Import property %s = %s", property.getEntityFielName(), property.getValue()));
            MlAttr attr = GuiceConfigSingleton.inject(MetaDataHolder.class).getAttr(mlClass, property.getEntityFielName());
            switch (attr.getFieldType()) {
                case DATE:
                    dynamicEntity.set(property.getEntityFielName(), new Date(Long.parseLong(property.getValue())));
                    break;
                case BOOLEAN:
                    dynamicEntity.set(property.getEntityFielName(), Boolean.parseBoolean(property.getValue()));
                    break;
                case LONG:
                    dynamicEntity.set(property.getEntityFielName(), Long.parseLong(property.getValue()));
                    break;
                case DOUBLE:
                    dynamicEntity.set(property.getEntityFielName(), Double.parseDouble(property.getValue()));
                    break;
                case FILE:
                    try {
                        String file = property.getValue();
                        BASE64Decoder base64Decoder = new BASE64Decoder();
                        String fileName = file.substring(0,file.indexOf("//"));
                        byte[] fileValue = base64Decoder.decodeBuffer(file.substring(file.indexOf("//") + 2));
                        dynamicEntity.set(property.getEntityFielName(), fileValue);
                        dynamicEntity.set(property.getEntityFielName()+"_filename", fileName);
                    } catch (IOException e) {
                        throw  new MlServerException(String.format("Ощибка репликации типа Файл. [%s,%s]",mlClass.getEntityName(),property.getEntityFielName()),e);
                    }
                    break;
                case MANY_TO_ONE:
                    dynamicEntity.set(property.getEntityFielName(), commonDao.getByMlUID(new MLUID(property.getValue())));
                    break;
                case MANY_TO_MANY:
                case ONE_TO_MANY:
                case HETERO_LINK:
                    if (!property.getValue().equals("")) {
                        List<MLUID> mluids = new ArrayList<>();
                        for (String mluidStr : property.getValue().split("#")) {
                            mluids.add(new MLUID(mluidStr));
                        }
                        mapMany.put(property.getEntityFielName(), mluids);
                        System.out.println(property.getValue());
                    }
                    break;
                default:
                    dynamicEntity.set(property.getEntityFielName(), property.getValue());
            }
        }
        return mapMany;
    }

    private void insertManyToMany() throws MlApplicationException {
        MlHeteroLinkService heteroLinkService = GuiceConfigSingleton.inject(MlHeteroLinkService.class);
        for (MLUID mluid : manyToMany.keySet()) {
            if (manyToMany.get(mluid) != null && !manyToMany.get(mluid).isEmpty()) {
                MlDynamicEntityImpl dynamicEntity = commonDao.getByMlUID(mluid);
                Map<String, List<MLUID>> mapMany = manyToMany.get(mluid);
                for (String attrName : mapMany.keySet()) {
                    MlAttr mlAttr = dynamicEntity.getInstanceMlClass().getAttr(attrName);
                    List<MLUID> relationMluidList = mapMany.get(attrName);
                    switch (mlAttr.getFieldType()){
                        case HETERO_LINK:
                            Set<MlHeteroLink> mlHeteroLinks = new HashSet<>();
                            for (MLUID relationMluid : relationMluidList) {
                                MlDynamicEntityImpl dynamicEntityRelation = commonDao.getByMlUID(relationMluid);
                                if (dynamicEntityRelation == null) {
                                    throw new MlApplicationException(String.format("Не найден объект по MLUID :%s", relationMluid.toString()));
                                }

                                boolean needInsert = true;

                                for(MlHeteroLink heteroLink: (List<MlHeteroLink>)dynamicEntity.get(attrName) ){
                                    MlDynamicEntityImpl heteroEntity = heteroLinkService.getObjectHeteroLink(heteroLink);
                                    if (heteroLink.getClassName().equals(dynamicEntityRelation.getInstanceMlClass().getEntityName())
                                            && heteroEntity.get(MlAttr.GUID).equals(dynamicEntityRelation.get(MlAttr.GUID))) {
                                        needInsert = false;
                                        break;
                                    }
                                }
                                if(needInsert){
                                    MlHeteroLink heteroLink = (MlHeteroLink) commonDao.createNewEntity(MlHeteroLink.class);
                                    heteroLink.setObject(dynamicEntityRelation);
                                    commonDao.persistWithoutSecurityCheck(heteroLink);
                                    mlHeteroLinks.add(heteroLink);
                                }
                            }
                            commonDao.refresh(dynamicEntity);
                            mlHeteroLinks.addAll((List<MlHeteroLink>) dynamicEntity.get(attrName));
                            List<MlHeteroLink> links = new ArrayList<>();
                            links.addAll(mlHeteroLinks);
                            dynamicEntity.set(attrName, links);
                            commonDao.persistWithoutSecurityCheck(dynamicEntity, false, false);

                            break;
                        case MANY_TO_MANY:
                        case ONE_TO_MANY:
                            Set<DynamicEntity> entityList = new HashSet<>();
                            for (MLUID relationMluid : relationMluidList) {
                                DynamicEntity dynamicEntityRelation = commonDao.getByMlUID(relationMluid);
                                if (dynamicEntityRelation == null) {
                                    throw new MlApplicationException(String.format("Не найден объект по MLUID :%s", relationMluid.toString()));
                                }
                                entityList.add(dynamicEntityRelation);
                            }
                            commonDao.refresh(dynamicEntity);
                            entityList.addAll((List<DynamicEntity>) dynamicEntity.get(attrName));
                            List<DynamicEntity> entities = new ArrayList<>();
                            entities.addAll(entityList);
                            dynamicEntity.set(attrName, entities);
                            commonDao.persistWithoutSecurityCheck(dynamicEntity, false, false);
                            break;
                    }
                }
            }
        }
        manyToMany = new HashMap<>();
    }
}
