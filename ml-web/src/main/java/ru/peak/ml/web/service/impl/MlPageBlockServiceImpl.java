package ru.peak.ml.web.service.impl;

import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlSecurityException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.ml.prop.MlProperties;
import ru.peak.ml.prop.Property;
import ru.peak.ml.web.block.controller.BlockController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.service.MlPageBlockService;
import ru.peak.ml.web.service.annotations.PageBlockAction;
import ru.peak.security.services.AccessServiceImpl;

import javax.servlet.http.Cookie;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 *
 */
public class MlPageBlockServiceImpl implements MlPageBlockService {
    private static final Logger log = LoggerFactory.getLogger(MlPageBlockServiceImpl.class);

    @Inject
    CommonDao commonDao;
    @Inject
    AccessServiceImpl accessService;
    @Inject
    MlProperties properties;

    @Override
    public void processRequest(MlHttpServletRequest req, MlHttpServletResponse resp) {
        // Find pageBlock
        log.debug(String.format("Process request for PageBlock with id = [%d]", req.getLong("pageBlockId")));
        MlPageBlockBase pageBlock = (MlPageBlockBase) commonDao.findById(req.getLong("pageBlockId"), MlPageBlockBase.MlPageBlockBase);
        if (pageBlock == null) {
            log.error(String.format("Page block with id = [%d] not found", req.getLong("pageBlockId")));
            throw new MlServerException(String.format("Страничный блок с id = [%d] не найден", req.getLong("pageBlockId")));
        }
        //check access
        if (!accessService.checkAccessPageBlock(pageBlock)){
            String authPage = properties.getProperty(Property.AUTH_PAGE);
            for(Cookie cookie: req.getRequest().getCookies()){
                if(cookie.getName().equals("authPage")){
                    authPage = cookie.getValue();
                }
            }
            String authPageUrl = req.getRequest().getContextPath() + authPage;
            if (accessService.isAnonymous()){
                // Делаем редирект на логин если пользователь не авторизован
                resp.setDataType(MlHttpServletResponse.DataType.JSON);
                resp.addDataToJson("needAuth", new JsonPrimitive(true));
                resp.addDataToJson("url", new JsonPrimitive(authPageUrl));
            }  else {
                resp.setDataType(MlHttpServletResponse.DataType.JSON);
                resp.addDataToJson("error", new JsonPrimitive(true));
                resp.addDataToJson("errorMessage", new JsonPrimitive("Ошибка доступа. Обратитесь к администратору."));
            }
            return;
        }
        // Get controller class and render block

        log.debug(String.format("Invoke serve from class = [%s] for PageBlock with id = [%d] ", pageBlock.getControllerJavaClass(), req.getLong("pageBlockId")));
        Class<?> aClass;
        try {
            aClass = Class.forName(pageBlock.getControllerJavaClass());
        } catch (ClassNotFoundException e) {
            log.error(e.getMessage(), e);
            throw new MlServerException("Не найден класс контроллера", e);
        }
        BlockController blockController = (BlockController) GuiceConfigSingleton.inject(aClass);
        // Читаем анатации методов класса и всех его предков и если есть анатации BlockAction у метода
        // и совпадает имя вызываемого метода то делаем вызов
        Method method = null;
        if (req.hasString("action")) {
            Class parent = aClass;
            //TODO add cache
            while (parent != null) {
                BlockAction blockAction = findPageBlockActionMethodInfo(parent, req.getString("action"));
                method = blockAction.method;
                if (method != null) {
                    method.setAccessible(true);
                    try {
                        resp.setDataType(blockAction.dataType);
                        method.invoke(blockController, req, resp, pageBlock);
                        break;
                    } catch (InvocationTargetException e){
                        log.error(String.format("Ошибка вызова метода [%s] класса [%s]", method.getName(), pageBlock.getControllerJavaClass()), e);
                        if (e.getTargetException() instanceof MlSecurityException){
                            throw (MlSecurityException)e.getTargetException();
                        }
                        if (e.getTargetException() instanceof MlApplicationException){
                           throw (MlApplicationException)e.getTargetException();
                        }
                        if ( e.getTargetException() instanceof MlServerException){
                            throw (MlServerException)e.getTargetException();
                        }
                        throw new MlServerException(String.format("Ошибка вызова метода [%s] класса [%s]", method.getName(), pageBlock.getControllerJavaClass()), e.getTargetException());
                    }
                    catch (IllegalAccessException e) {
                        log.error(e.getMessage(), e);
                        throw new MlServerException(String.format("Ошибка вызова метода [%s] класса [%s]", method.getName(), pageBlock.getControllerJavaClass()), e);
                    }
                }
                parent = parent.getSuperclass();
            }
        }
        if (method == null) {
            blockController.serve(req, pageBlock, resp);
        }
    }

    private BlockAction findPageBlockActionMethodInfo(Class<?> clazz,String action) {
        BlockAction blockAction = new BlockAction(MlHttpServletResponse.DataType.JSON);
        for (Method method : clazz.getDeclaredMethods()) {
            if (method.isAnnotationPresent(PageBlockAction.class)) {
                PageBlockAction annotation = method.getAnnotation(PageBlockAction.class);
                if (annotation.action().equals(action)) {
                    blockAction.method = method;
                    blockAction.dataType = annotation.dataType();
                    break;
                }
            }
        }
        return blockAction;
    }

    /**
     * Метод PageBlock'а
     */
    private class BlockAction {
        Method method;
        MlHttpServletResponse.DataType dataType;

        public BlockAction(MlHttpServletResponse.DataType dataType) {
            this.dataType = dataType;
        }
    }

}
