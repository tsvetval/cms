package ru.peak.ml.web.service.impl;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import org.eclipse.persistence.dynamic.DynamicEntity;
import org.eclipse.persistence.internal.dynamic.DynamicEntityImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.folder.MlFolder;
import ru.peak.ml.core.model.page.MlPage;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.ml.prop.MlProperties;
import ru.peak.ml.prop.Property;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.dao.MlPageDao;
import ru.peak.ml.web.dao.impl.FolderDao;
import ru.peak.ml.web.helper.JsonHelper;
import ru.peak.ml.web.service.MlPageService;
import ru.peak.security.services.AccessServiceImpl;

import javax.servlet.http.Cookie;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
  *
 */
public class MlPageServiceImpl implements MlPageService {
    private static final Logger log = LoggerFactory.getLogger(MlPageServiceImpl.class);
    private static final String ML_REQUEST_PARAM = "ml_request";
    @Inject
    MlPageDao mlPageDao;
    @Inject
    AccessServiceImpl accessService;
    @Inject
    MlProperties properties;
    @Inject
    FolderDao folderDao;

    @Override
    public void processRequest(MlHttpServletRequest req, MlHttpServletResponse resp) throws IOException {
        // Парсинг входных параметров
        String pageUrl = req.getRequest().getPathInfo();
        log.debug(String.format("Process page with url = [%s], action = [%s]", pageUrl, req.getString("action", "NULL")));
        // Ищем стринцу по заданному URL
        MlPage mlPage = mlPageDao.getPageByUrl(pageUrl);
        if (mlPage == null) {
        // не найдена страница по данному URL
            log.debug(String.format("Could not find page with url: %s", pageUrl));
            resp.setDataType(MlHttpServletResponse.DataType.JSON);
            resp.addDataToJson("error", new JsonPrimitive(true));
            resp.addDataToJson("errorMessage", new JsonPrimitive("Страница не найдена"));
        } else if (!req.getRequest().getParameterMap().containsKey(ML_REQUEST_PARAM)) {
            // Это чистый запрос CMS еще не загружена отдаем страницу с проектным шаблоном
            log.debug(String.format("This is first request, render template %s", mlPage.getProjectTemplate() == null ? "boot.html" : mlPage.getProjectTemplate()));
            Map<String, Object> data = resp.getHtmlTemplateData();
            data.put("CONTEXT_PATH", req.getRequest().getContextPath());
            if (mlPage.getProjectTemplate() == null){
                resp.setHtmlTemplate("boot.hml");
            } else {
                resp.setHtmlTemplate(mlPage.getProjectTemplate());
            }
        } else if (accessService.checkAccessPage(mlPage) || (accessService.isAnonymous() && properties.getProperty(Property.AUTH_PAGE).equals(pageUrl))) {
        // Есть доступ к странице либо пытаемся зати на страницу авторизации
            log.debug(String.format("For page with url = [%s], found page with id = [%d]", pageUrl, mlPage.getId()));
            if (req.getString("action", "NULL").equals("getPageBlocks")) {
                JsonElement pageBlockCollection = JsonHelper.dynamicObjectListToJson((List) mlPage.getPageBlocks(),
                        new String[]{MlPageBlockBase.ID, MlPageBlockBase.ZONE, MlPageBlockBase.ORDER_NUMBER, MlPageBlockBase.BOOT_JS});
                log.debug(String.format("For page with url = [%s], found [%d] pageBlocks, add them to Json result", pageUrl, mlPage.getPageBlocks().size()));
                JsonObject result = new JsonObject();
                result.add("pageBlocks", pageBlockCollection);
                JsonObject page = new JsonObject();
                page.add("id",new JsonPrimitive(mlPage.getId()));
                page.add("title",new JsonPrimitive(mlPage.getTitle()));
                result.add("page",page);
                resp.setJsonData(result);

            } else if (req.getString("action", "NULL").equals("getPageBlockByIDs")) {
                if(!req.hasString("pageBlockIds")){
                    throw new MlServerException("Parameter \"pageBlockIds\" not found in request");
                }
                if(!req.hasLong("folderId")){
                    throw new MlServerException("Parameter \"folderId\" not found in request");
                }
                Long folderId = req.getLong("folderId");
                MlFolder mlFolder = folderDao.findById(folderId);
                Gson gson = new Gson();
                List<Double> blockIds = gson.fromJson(req.getString("pageBlockIds"),List.class);
                if(accessService.checkAccessFolder(mlFolder)){
                    CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
                    List<MlPageBlockBase> mlPageBlockBases = new ArrayList<>();
                    for(Double blockId:blockIds){
                        MlPageBlockBase mlPageBlockBase = (MlPageBlockBase) commonDao.findById(blockId.longValue(),MlPageBlockBase.class);
                        mlPageBlockBases.add(mlPageBlockBase);

                    }
                    if(folderDao.hasPageBlocks(mlFolder,mlPageBlockBases)){
                    JsonElement pageBlockCollection = JsonHelper.dynamicObjectListToJson((List)mlPageBlockBases,
                            new String[]{MlPageBlockBase.ID, MlPageBlockBase.ZONE, MlPageBlockBase.ORDER_NUMBER, MlPageBlockBase.BOOT_JS});
                    resp.setJsonData(pageBlockCollection);
                    }else{
                        resp.setDataType(MlHttpServletResponse.DataType.JSON);
                        resp.addDataToJson("error", new JsonPrimitive(true));
                        resp.addDataToJson("errorMessage", new JsonPrimitive("Отказано в доступе к страничному блоку. Блок не относится к папке.Обратитесь к администратору."));
                    }
                }else{
                    resp.setDataType(MlHttpServletResponse.DataType.JSON);
                    resp.addDataToJson("error", new JsonPrimitive(true));
                    resp.addDataToJson("errorMessage", new JsonPrimitive("Отказано в доступе к папке. Обратитесь к администратору."));
                }
            } else {
                log.debug(String.format("For page with url = [%s], set render template = [%s]", pageUrl, mlPage.getTemplate()));
                resp.getHtmlTemplateData().put("CONTEXT_PATH", req.getRequest().getContextPath());
                for(String key:req.getParameters().keySet()){
                    resp.getHtmlTemplateData().put(key, req.getRequest().getParameter(key));
                }
                resp.setHtmlTemplate(mlPage.getTemplate());
            }
        } else {
            log.debug(String.format("Access denided. For page with url = [%s], user login = [%s]", pageUrl,
                    req.getRequest().getSession().getAttribute("user") == null? "anonymous":
                            ((MlUser)req.getRequest().getSession().getAttribute("user")).getLogin()));
            String authPage = properties.getProperty(Property.AUTH_PAGE);
            if(req.getRequest().getCookies() != null) {
             for (Cookie cookie : req.getRequest().getCookies()) {
               if (cookie.getName().equals("authPage")) {
                 authPage = cookie.getValue();
               }
             }
            }
            String authPageUrl = req.getRequest().getContextPath() + authPage;
            if (accessService.isAnonymous()){
              // Делаем редирект на логин если пользователь не авторизован
                resp.setDataType(MlHttpServletResponse.DataType.JSON);
                resp.addDataToJson("needAuth", new JsonPrimitive(true));
                resp.addDataToJson("url", new JsonPrimitive(authPageUrl));
            }  else {
                resp.setDataType(MlHttpServletResponse.DataType.JSON);
                resp.addDataToJson("error", new JsonPrimitive(true));
                resp.addDataToJson("errorMessage", new JsonPrimitive("Отказано в доступе к странице. Обратитесь к администратору."));
            }
        }
    }

}
