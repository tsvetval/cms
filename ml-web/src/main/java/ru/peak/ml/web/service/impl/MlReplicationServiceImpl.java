package ru.peak.ml.web.service.impl;

import com.google.inject.Inject;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.eclipse.persistence.dynamic.DynamicEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.holders.MetaDataHolder;
import ru.peak.ml.core.model.MLUID;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.web.model.MlHeteroLink;
import ru.peak.ml.web.model.MlReplication;
import ru.peak.ml.web.model.MlReplicationStep;
import ru.peak.ml.web.replication.helper.ImportReplicationOption;
import ru.peak.ml.web.replication.helper.Replication;
import ru.peak.ml.web.replication.stub.ReplicationBlock;
import ru.peak.ml.web.replication.stub.SchemeConverter;
import ru.peak.ml.web.service.MlHeteroLinkService;
import ru.peak.ml.web.service.MlReplicationService;
import sun.misc.BASE64Encoder;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.jar.JarInputStream;
import java.util.zip.ZipInputStream;

//import ru.peak.ml.web.replication.ReplicationBlock;

/**
 * Created with IntelliJ IDEA.
 * User: d_litovchenko
 * Date: 04.07.14
 * Time: 17:25
 * To change this template use File | Settings | File Templates.
 */
public class MlReplicationServiceImpl implements MlReplicationService {
    private static final Logger log = LoggerFactory.getLogger(MlReplicationServiceImpl.class);
    @Inject
    CommonDao commonDao;
    @Inject
    SchemeConverter schemeConverter;
    @Inject
    MlImportReplicationImpl mlImportReplication;
    @Inject
    MlHeteroLinkService heteroService;

    private Map<String, Boolean> mluids = new HashMap<String, Boolean>();

    @Override
    public byte[] doReplication(MlReplication mlReplication) throws JAXBException, DatatypeConfigurationException {
        log.debug(String.format("Start replication process. MlReplication.id = %s", mlReplication.getId()));
        validateReplication(mlReplication);
        MlHeteroLinkService heteroLinkService = GuiceConfigSingleton.inject(MlHeteroLinkService.class);
        ReplicationBlock replicationBlock = new ReplicationBlock();
        replicationBlock.setName(mlReplication.getName());
        List<MlReplicationStep> mlReplicationSteps = commonDao.getResultList("select o from MlReplicationStep o where o.replication.id =" + mlReplication.getId() + "  order by o.stepNumber", MlReplicationStep.class);
        for (MlReplicationStep mlReplicationStep : mlReplicationSteps) {
            log.debug(String.format("   Replication step. %s", mlReplicationStep.toString()));
            ReplicationBlock.ReplicationStep replicationStep = new ReplicationBlock.ReplicationStep();
            replicationStep.setStepNumber(mlReplicationStep.getId());
            replicationStep.setName(mlReplicationStep.getName());
            replicationStep.setOffHandler(mlReplicationStep.getOffHandlers());
            replicationBlock.getReplicationStep().add(replicationStep);
            for(MlHeteroLink heteroLink: mlReplicationStep.getObjects()){
                MlDynamicEntityImpl heteroEntity = heteroLinkService.getObjectHeteroLink(heteroLink);
                MlClass mlClass = heteroEntity.getInstanceMlClass();
                if(mlClass.getAttr(MlAttr.GUID) != null && mlClass.getAttr(MlAttr.LAST_CHANGE) != null){
                    checkMlClassOrMlAttr(mlClass, mlReplicationStep);
                    addEntity(heteroEntity,mlClass,replicationStep);
                }else{
                    throw new MlApplicationException(String.format("В классе %s отсутсвтвует необходимые для репликации поля!",mlClass.getDescription()));
                }
            }
        }
        System.out.println(schemeConverter.ToXml(replicationBlock));
        return schemeConverter.ToXml(replicationBlock).getBytes();
    }

    private void validateReplication(MlReplication mlReplication) {
        List<MlClass> meta = getAllMetaFromReplication(mlReplication);
        List<MlClass> data = getMlClassesFromData(mlReplication);
        meta.retainAll(data);
        meta.forEach(mlClass ->{
            throw new MlApplicationException(String.format("Ошибка! В одном пакете обновлений содериться мета информация и данные класса %s",mlClass.getEntityName()));
        });
    }

    private List<MlClass> getMlClassesFromData(MlReplication mlReplication) {
        List<MlClass> result = new ArrayList<>();
        for(MlReplicationStep step: mlReplication.getReplicationSteps()){
            for(MlHeteroLink heteroLink: step.getObjects()){
                MlDynamicEntityImpl entity = heteroService.getObjectHeteroLink(heteroLink);
                result.add(entity.getInstanceMlClass());
            }
        }
        return  result;

    }

    private List<MlClass> getAllMetaFromReplication(MlReplication mlReplication) {
        List<MlClass> result = new ArrayList<>();
        for(MlReplicationStep step: mlReplication.getReplicationSteps()){
            for(MlHeteroLink heteroLink: step.getObjects()){
                MlDynamicEntityImpl entity = heteroService.getObjectHeteroLink(heteroLink);
                if(entity instanceof MlClass){
                    result.add((MlClass) entity);
                }else if(entity instanceof MlAttr){
                    result.add(((MlAttr) entity).getMlClass());
                }
            }
        }
        return  result;
    }

    private void checkMlClassOrMlAttr(MlClass mlClass, MlReplicationStep mlReplicationStep) {
        if(mlClass.getEntityName().equals("MlClass") || mlClass.getEntityName().equals("MlAttr")){
            if(!mlReplicationStep.getOffHandlers()){
                throw new MlServerException("В пакете репликации не отключены обработчики но присутствует "+mlClass.getEntityName());
            }
        }
    }

    private void addEntity(DynamicEntity entity, MlClass mlClass, ReplicationBlock.ReplicationStep replicationStep) throws DatatypeConfigurationException {
        MLUID mluid = commonDao.getMlUID((Long) entity.get(MlAttr.ID), mlClass.getEntityName());
        MlHeteroLinkService heteroLinkService = GuiceConfigSingleton.inject(MlHeteroLinkService.class);
        if (mluids.containsKey(mluid.toString())) {
            return;
        }
        mluids.put(mluid.toString(), true);
        addManyToOneRelations(entity, mlClass, replicationStep);
        ReplicationBlock.ReplicationStep.ReplicationObject replicationObject = new ReplicationBlock.ReplicationStep.ReplicationObject();

        replicationObject.setMluid(mluid.toString());
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime((Date) entity.get(MlAttr.LAST_CHANGE));
        XMLGregorianCalendar lastChangeDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        replicationObject.setLasChangeDate(lastChangeDate);

        for (MlAttr attr : mlClass.getAttrSet()) {
            switch (attr.getReplicationType()) {
                case NOT_REPLICATE:
                    break;
                case REPLICATE:
                case CASCADE_REPLICATE:
                    ReplicationBlock.ReplicationStep.ReplicationObject.Property property =
                            new ReplicationBlock.ReplicationStep.ReplicationObject.Property();

                    if (entity.get(attr.getEntityFieldName()) != null) {
                        if (!attr.getPrimaryKey()) {
                            switch (attr.getFieldType()) {
                                case MANY_TO_ONE:
                                    switch (attr.getReplicationType()) {
                                        case CASCADE_REPLICATE:
                                        case REPLICATE:
                                            property.setEntityFielName(attr.getEntityFieldName());
                                            DynamicEntity dynamicEntity = entity.get(attr.getEntityFieldName());
                                            property.setValue(new MLUID(dynamicEntity).toString());
                                            replicationObject.getProperty().add(property);
                                    }
                                    break;
                                case BOOLEAN:
                                case LONG_LINK:
                                case TEXT:
                                case DOUBLE:
                                case ENUM:
                                case STRING:
                                case LONG:
                                    property.setEntityFielName(attr.getEntityFieldName());
                                    property.setValue(entity.get(attr.getEntityFieldName()).toString());
                                    replicationObject.getProperty().add(property);
                                    break;
                                case DATE:
                                    property.setEntityFielName(attr.getEntityFieldName());
                                    property.setValue(Long.toString(((Date) entity.get(attr.getEntityFieldName())).getTime()));
                                    replicationObject.getProperty().add(property);
                                    break;
                                case FILE:
                                    property.setEntityFielName(attr.getEntityFieldName());
                                    BASE64Encoder base64Encoder = new BASE64Encoder();
                                    String value = entity.get(attr.getEntityFieldName()+"_filename")+"//"+base64Encoder.encode((byte[]) entity.get(attr.getEntityFieldName()));
                                    property.setValue(value);
                                    replicationObject.getProperty().add(property);
                                    break;
                                default:
                                    System.out.println(attr.getEntityFieldName());

                            }
                        }
                    }
            }
        }
        replicationStep.getReplicationObject().add(replicationObject);
        for (MlAttr attr : mlClass.getAttrSet()) {
            switch (attr.getFieldType()) {
                case HETERO_LINK:
                    switch (attr.getReplicationType()) {
                        case CASCADE_REPLICATE:
                            List<MlHeteroLink> links = entity.get(attr.getEntityFieldName());
                            for (MlHeteroLink link : links) {
                                MlDynamicEntityImpl childEntity = heteroLinkService.getObjectHeteroLink(link);
                                addEntity(childEntity, childEntity.getInstanceMlClass(), replicationStep);
                            }
                        case REPLICATE:
                            ReplicationBlock.ReplicationStep.ReplicationObject.Property property =
                                    new ReplicationBlock.ReplicationStep.ReplicationObject.Property();
                            String oneToMany = "";
                            List<MlHeteroLink> mlHeteroLinks = entity.get(attr.getEntityFieldName());
                            for (MlHeteroLink heteroLink : mlHeteroLinks) {
                                MlDynamicEntityImpl childEntity = heteroLinkService.getObjectHeteroLink(heteroLink);
                                try{
                                oneToMany += commonDao.getMlUID(childEntity) + "#";
                                }catch (Exception e){
                                    throw new MlApplicationException(String.format("Невозможно получить MLUID у обьекта %s",childEntity.getTitle()));
                                }
                            }
                            property.setEntityFielName(attr.getEntityFieldName());
                            property.setValue(oneToMany);
                            replicationObject.getProperty().add(property);
                            break;
                    }
                    break;
                case ONE_TO_MANY:
                    switch (attr.getReplicationType()) {
                        case CASCADE_REPLICATE:
                            List<DynamicEntity> entities = entity.get(attr.getEntityFieldName());
                            for (DynamicEntity childEntity : entities) {
                                addEntity(childEntity, attr.getLinkAttr().getMlClass(), replicationStep);
                            }
                        case REPLICATE:
                            ReplicationBlock.ReplicationStep.ReplicationObject.Property property =
                                    new ReplicationBlock.ReplicationStep.ReplicationObject.Property();
                            String oneToMany = "";
                            List<DynamicEntity> oneToManyEnityes = entity.get(attr.getEntityFieldName());
                            for (DynamicEntity childEntity : oneToManyEnityes) {
                                oneToMany += commonDao.getMlUID(childEntity) + "#";
                            }
                            property.setEntityFielName(attr.getEntityFieldName());
                            property.setValue(oneToMany);
                            replicationObject.getProperty().add(property);
                            break;
                    }
                    break;
                case MANY_TO_MANY:
                    switch (attr.getReplicationType()) {
                        case CASCADE_REPLICATE:
                            List<DynamicEntity> entities = entity.get(attr.getEntityFieldName());
                            if (entities != null) {
                                for (DynamicEntity childEntity : entities) {
                                    addEntity(childEntity, attr.getLinkClass(), replicationStep);
                                }
                            }
                        case REPLICATE:
                            ReplicationBlock.ReplicationStep.ReplicationObject.Property property =
                                    new ReplicationBlock.ReplicationStep.ReplicationObject.Property();
                            String manyToMany = "";
                            List<DynamicEntity> MNentities = entity.get(attr.getEntityFieldName());
                            for (DynamicEntity childEntity : MNentities) {
                                manyToMany += commonDao.getMlUID(childEntity) + "#";
                            }
                            property.setEntityFielName(attr.getEntityFieldName());
                            property.setValue(manyToMany);
                            replicationObject.getProperty().add(property);
                    }
            }
        }
    }

    private void addManyToOneRelations(DynamicEntity entity, MlClass mlClass, ReplicationBlock.ReplicationStep replicationStep) throws DatatypeConfigurationException {
        for (MlAttr attr : mlClass.getAttrSet()) {
            switch (attr.getFieldType()) {
                case MANY_TO_ONE:
                    switch (attr.getReplicationType()) {
                        case CASCADE_REPLICATE:
                            if (entity.get(attr.getEntityFieldName()) != null) {
                                addEntity((DynamicEntity) entity.get(attr.getEntityFieldName()), attr.getLinkClass(), replicationStep);
                            }
                            break;
                    }
                    break;
            }
        }
    }

    @Override
    public ReplicationBlock getReplicationInfo(byte[] file) throws JAXBException {
        return schemeConverter.fromXml(file);
    }

    @Override
    public Replication importReplication(byte[] zipFile, Boolean onlyGenerateResultFile, ImportReplicationOption option) throws IOException, JAXBException, MlApplicationException, ArchiveException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(zipFile);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        ArchiveInputStream input = new ArchiveStreamFactory().createArchiveInputStream(byteArrayInputStream);
        input.getNextEntry();

        byte[] buffer = new byte[1024];
        int len;
        while ((len = input.read(buffer)) > 0) {
            byteArrayOutputStream.write(buffer, 0, len);
        }
        input.close();

        ReplicationBlock block = getReplicationInfo(byteArrayOutputStream.toByteArray());
        byteArrayOutputStream.close();
        return mlImportReplication.doImport(block, onlyGenerateResultFile, option);
    }
}
