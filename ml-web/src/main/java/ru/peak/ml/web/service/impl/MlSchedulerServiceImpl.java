package ru.peak.ml.web.service.impl;

import com.google.inject.Inject;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.web.model.MlScheduled;
import ru.peak.ml.web.schedule.ScheduleJob;
import ru.peak.ml.web.service.MlSchedulerService;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: a_gumenyuk
 * Date: 23.07.14
 * Time: 15:32
 * Сервис выполнения периодических задач
 */
public class MlSchedulerServiceImpl implements MlSchedulerService {

    private static final Logger log = LoggerFactory.getLogger(MlSchedulerServiceImpl.class);

    private static final String GROUP_NAME = "mlSchedulesGroup";
    private static final String TRIGGER_SUFFIX = "_trigger";

    @Inject
    CommonDao commonDao;

    /**
     * Запуск планировщика
     */
    @Override
    public void start() {
        initJobs();
        try {
            if (!schedulerIsEmpty()) {
                getScheduler().start();
            }
        } catch (SchedulerException e) {
            log.error("Error start Quartz scheduler", e);
        }
    }

    /**
     * Остановка планировщика
     */
    @Override
    public void shutdown() {
        try {
            getScheduler().shutdown();
        } catch (SchedulerException e) {
            log.error("Error shutdown Quartz scheduler", e);
        }
    }

    /**
     * Запустить задачу
     * @param objectId
     */
    @Override
    public void startScheduledJob(Long objectId) {
        initJob((MlScheduled)commonDao.findById(objectId, MlScheduled.class));
    }

    /**
     * Остановить задачу
     * @param objectId
     */
    @Override
    public void stopScheduledJob(Long objectId) {
        MlScheduled scheduled = (MlScheduled) commonDao.findById(objectId, MlScheduled.class);
        try {
            getScheduler().deleteJob(JobKey.jobKey(scheduled.getName(), GROUP_NAME));
        } catch (SchedulerException e) {
            log.error("Error Quartz scheduler stop job: [" + scheduled.getName() + "]", e);
        }
    }

    /**
     * Получить и добавить для выполнения задачи
     */
    private void initJobs() {
        for (MlScheduled scheduled : getMlScheduled()) {
            initJob(scheduled);
        }
    }

    /**
     * Добавить для выполнения периодическую задачу
     * @param scheduled
     */
    private void initJob(MlScheduled scheduled) {
        try {
            JobDetail jobDetail = createJobDetail(scheduled);
            Trigger trigger = createTrigger(scheduled);
            if (jobDetail != null && trigger != null) {
                getScheduler().scheduleJob(jobDetail, trigger);
            }
        } catch (SchedulerException e) {
            log.error("Error creating scheduled job by name [" + scheduled.getName() + "]", e);
        }
    }

    /**
     * Получить планировщик
     *
     * @return
     */
    private Scheduler getScheduler() {
        Scheduler scheduler = null;
        try {
            SchedulerFactory schedulerFactory = new org.quartz.impl.StdSchedulerFactory();
            scheduler = schedulerFactory.getScheduler();
        } catch (SchedulerException e) {
            log.error("Error get default Scheduler, by Quartz scheduler(StdSchedulerFactory)", e);
        }
        return scheduler;
    }

    /**
     * Нет задач для выполнения
     *
     * @return
     */
    private boolean schedulerIsEmpty() {
        boolean result = true;
        try {
            result = getScheduler().getJobGroupNames().isEmpty();
        } catch (SchedulerException e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * Описание работы
     * Непосредственно выполняет действия
     *
     * @param scheduled - Данные по периодическому заданию
     * @return
     */
    private JobDetail createJobDetail(MlScheduled scheduled) {
        String jobClassName = scheduled.getJobClass();
        if (jobClassName == null || jobClassName.isEmpty()) {
            log.error("Error create scheduled job, class is not specified");
            return null;
        }
        JobDetail jobDetail = null;
        Class jobClass;
        try {
            jobClass = Class.forName(jobClassName);
            jobDetail = JobBuilder.newJob(jobClass)
                    .withIdentity(scheduled.getName(), GROUP_NAME)
                    .build();
            jobDetail.getJobDataMap().put(ScheduleJob.MlScheduled,scheduled);
        } catch (ClassNotFoundException e) {
            log.error("Error create scheduled job, not find class [" + jobClassName + "]", e);
        }
        return jobDetail;
    }

    /**
     * Триггер работы
     * Отвечает за периодичность выполнения
     *
     * @param scheduled - Данные по периодическому заданию
     * @return
     */
    private Trigger createTrigger(MlScheduled scheduled) {
        Trigger trigger = null;
        try{
            trigger = TriggerBuilder.newTrigger()
                    .withIdentity(scheduled.getName() + TRIGGER_SUFFIX, GROUP_NAME)
                    .startNow()
                    .withSchedule(CronScheduleBuilder.cronSchedule(scheduled.getCron()))
                    .build();
        } catch (RuntimeException e){
            log.error(e.getMessage(), e);
        }
        return trigger;
    }

    /**
     * Список задач из БД
     *
     * @return
     */
    private List<MlScheduled> getMlScheduled() {
        return commonDao.getResultList("SELECT o FROM MlScheduled o where o.isActive = true", MlScheduled.class);
    }
}
