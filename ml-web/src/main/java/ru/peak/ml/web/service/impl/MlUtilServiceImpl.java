package ru.peak.ml.web.service.impl;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.dao.MlUtilDao;
import ru.peak.ml.core.model.util.MlUtil;
import ru.peak.ml.web.service.MlUtilService;
import ru.peak.ml.web.utils.controller.UtilController;
import ru.peak.security.services.AccessServiceImpl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 */
public class MlUtilServiceImpl implements MlUtilService {
    private static final Logger log = LoggerFactory.getLogger(MlUtilServiceImpl.class);
    private static final String ERROR_TEMPLATE = "blocks/utils/error.hml";

    @Inject
    MlUtilDao mlUtilDao;
    @Inject
    AccessServiceImpl accessService;

    @Override
    public void processRequest(MlHttpServletRequest req, MlHttpServletResponse resp) throws IOException {
        String url = req.getRequest().getRequestURI();
        url = url.replaceAll(req.getRequest().getContextPath(), "");
        MlUtil mlUtil = mlUtilDao.getUtilByUrl(url);
        try {
            if (accessService.checkAccessUtil(mlUtil)) {
                Class<?> aClass = Class.forName(mlUtil.getControllerJavaClass());
                UtilController controller = (UtilController) GuiceConfigSingleton.inject(aClass);
                controller.serve(req, mlUtil, resp);
            }else{
                log.error("Access denided! Util "+mlUtil.getTitle());
                throw new MlApplicationException("У вас нет прав на исполнение утилиты "+mlUtil.getTitle());
            }
        } catch (Exception e) {
            /*//TODO refactor
            log.error("Page Block internal error", e);
            ByteArrayOutputStream stackTrace = new ByteArrayOutputStream();
            e.printStackTrace(new PrintWriter(stackTrace));
            resp.getHtmlTemplateData().put("stackTrace", new String(stackTrace.toByteArray()));
            resp.setHtmlTemplate(ERROR_TEMPLATE);*/
            throw new RuntimeException(e);
        }
    }


}
