package ru.peak.ml.web.utils.controller;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.web.utils.controller.utils.AbstractUtilComboBoxWithButtonController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.core.model.page.MlPageBlockBase;

import java.util.Date;
import java.util.List;

/**
 * User: Nikolay Mavrenkov <n.mavrenkov@peaksystems.ru>
 * Date: 12.03.14
 * Time: 16:30
 */
public class AuthorUtilController extends AbstractUtilComboBoxWithButtonController {
    private static final Logger log = LoggerFactory.getLogger(AuthorUtilController.class);

    @Override
    protected void initActions() {
        actions.put("authorCsv","Экспортировать список авторов в CSV");
        actions.put("showModal","Показать в модальном окне");
    }

    @Override
    protected void execUtil(String action, MlHttpServletRequest request, MlPageBlockBase pageBlock, MlHttpServletResponse resp) {
        try {
            if("authorCsv".equals(action)){
                StringBuilder builder = new StringBuilder();
                CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
                List<MlDynamicEntityImpl> entities = commonDao.getResultList("select o from Author o", "Author")  ;
                for(MlDynamicEntityImpl entity: entities){
                    builder.append(entity.get("name").toString()).append(";").append(entity.get("passport").toString()).append("\r\n");
                }
                resp.setDownloadData(builder.toString().getBytes());
                resp.setDownloadFileName("author_export_" + (new Date().getTime()) + ".xml");
            } else if ("showModal".equals(action)){
                StringBuilder builder = new StringBuilder();
                CommonDao commonDao = GuiceConfigSingleton.inject(CommonDao.class);
                List<MlDynamicEntityImpl> entities = commonDao.getResultList("select o from Author o", "Author")  ;
                for(MlDynamicEntityImpl entity: entities){
                    builder.append("<li>").append(entity.get("name").toString()).append(" - ").append(entity.get("passport").toString()).append("</li>");
                }
                JsonObject result = new JsonObject();
                result.add("showType",new JsonPrimitive("modal"));
                result.add("idModal",new JsonPrimitive((Long)pageBlock.get("id")));
                result.add("title",new JsonPrimitive("Инфа по авторам"));
                result.add("content",new JsonPrimitive(builder.toString()));

                resp.setJsonData(result);

            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

}
