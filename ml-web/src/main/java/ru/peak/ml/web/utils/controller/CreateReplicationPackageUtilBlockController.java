package ru.peak.ml.web.utils.controller;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.compress.compressors.CompressorOutputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.model.util.MlUtil;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.model.MlReplication;
import ru.peak.ml.web.service.MlReplicationService;
import ru.peak.ml.web.utils.controller.utils.AbstractUtilController;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 */
public class CreateReplicationPackageUtilBlockController extends AbstractUtilController implements UtilController {
    private static final Logger log = LoggerFactory.getLogger(CreateReplicationPackageUtilBlockController.class);
    @Inject
    CommonDao commonDao;
    @Inject
    MlReplicationService replicationService;

    @Override
    @Transactional
    public void serve(MlHttpServletRequest request, MlUtil mlUtil, MlHttpServletResponse resp) throws IOException, ParseException, DatatypeConfigurationException {
        Long objectId = request.getLong("objectId");
        MlReplication mlReplication = (MlReplication) commonDao.findById(objectId, "MlReplication");
        try {
            byte[] file = replicationService.doReplication(mlReplication);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            ArchiveOutputStream zipFile = new ArchiveStreamFactory().createArchiveOutputStream(ArchiveStreamFactory.ZIP, byteArrayOutputStream);
            zipFile.putArchiveEntry(new ZipArchiveEntry(mlReplication.getName() + ".xml"));
            zipFile.write(file);
            zipFile.closeArchiveEntry();
            zipFile.finish();
            mlReplication.setFile(byteArrayOutputStream.toByteArray());
            zipFile.close();

            byteArrayOutputStream.close();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYYMMdd_HHmmss");
            mlReplication.setFileName(mlReplication.getName() + "_" + simpleDateFormat.format(new Date()) + ".zip");
        } catch (JAXBException e) {
            throw new MlApplicationException("Ошибка сборки пакета репликации",e);
        } catch (ArchiveException e) {
            throw new MlApplicationException("Ошибка сборки пакета репликации",e);
        }
        JsonObject result = new JsonObject();
        result.add("showType", new JsonPrimitive("modal"));
        result.add("idModal", new JsonPrimitive((Long) mlUtil.get("id")));
        result.add("title", new JsonPrimitive("Результат операции"));
        result.add("content", new JsonPrimitive("Пакет обновлений успешно собран!"));
        resp.setJsonData(result);
    }


}
