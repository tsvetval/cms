package ru.peak.ml.web.utils.controller;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.ReplicationType;
import ru.peak.ml.core.model.system.MlAttr;
import ru.peak.ml.core.model.system.MlClass;
import ru.peak.ml.core.dao.CommonDao;
import ru.ml.core.common.AttrType;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.core.model.util.MlUtil;
import ru.peak.ml.web.utils.controller.utils.AbstractUtilController;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 *
 */
public class CreateTypicalAttrUtilBlockController extends AbstractUtilController implements UtilController {
    private static final Logger log = LoggerFactory.getLogger(CreateTypicalAttrUtilBlockController.class);
    @Inject
    CommonDao commonDao;

    @Override
    @Transactional
    public void serve(MlHttpServletRequest request, MlUtil mlUtil, MlHttpServletResponse resp) throws IOException, ParseException {
        MlClass mlClass = (MlClass) commonDao.findById(request.getLong("objectId"), "MlClass");
        JsonObject result = new JsonObject();
        result.add("showType", new JsonPrimitive("modal"));
        result.add("idModal", new JsonPrimitive((Long) mlUtil.get("id")));
        result.add("title", new JsonPrimitive("Результат операции"));
        if (!checkTypicalAttrExist(mlClass)) {
            log.debug("Creating typical attribute to " + mlClass.getEntityName());
            createGUID(mlClass);
            createLastChange(mlClass);
            //commonDao.refresh(mlClass);
            commonDao.mergeWithSecurityCheck(mlClass, false, true);
            result.add("content", new JsonPrimitive("Типовые артибуты: \"Глобальный идентификатор\", \"Дата последнего изменения\" успешно созданы!"));
        } else {
            log.debug("Typical attribute already exist in " + mlClass.getEntityName());
            result.add("content", new JsonPrimitive("Типовые артибуты уже были созданы либо имеются арртибуты с названиями :  \"Глобальный идентификатор\", \"Дата последнего изменения\""));
        }

        resp.setJsonData(result);
    }

    private boolean checkTypicalAttrExist(MlClass IMlClass) {
        for (MlAttr attr : IMlClass.getAttrSet()) {
            if (attr.getEntityFieldName().equals(MlAttr.GUID) || attr.getEntityFieldName().equals(MlAttr.LAST_CHANGE)) {
                return true;
            }
        }
        return false;
    }

    private void createGUID(MlClass mlClass) {
        createAttr("Глобальный идентификатор", MlAttr.GUID, AttrType.STRING, mlClass, "uuid_generate_v4()");
    }

    private void createLastChange(MlClass mlClass) {
        createAttr("Дата последнего изменения", MlAttr.LAST_CHANGE, AttrType.DATE, mlClass, "current_timestamp");
    }

    private void createAttr(String description, String fieldName, AttrType type, MlClass mlClass, String defaultValue) {
        MlAttr attr = (MlAttr) commonDao.createNewEntity(MlAttr.class);
        attr.setEntityFieldName(fieldName);
        attr.setMlClass(mlClass);
        attr.setTableFieldName(fieldName);
        attr.setDescription(description);
        attr.setSystemField(true);
        attr.setPrimaryKey(false);
        attr.setDefaultSqlValue(defaultValue);
        attr.setInList(false);
        attr.setInForm(false);
        attr.getPropertiesMap().get("useInExtendedSearch").setValue(true);
        attr.setAutoIncrement(false);
        attr.setFieldType(type);
        attr.setReplicationType(ReplicationType.REPLICATE);
        attr.setLastChange(new Date());
        attr.setGUID(UUID.randomUUID().toString());
        attr.setVirtual(false);
        commonDao.persistWithSecurityCheck(attr, true, false);
        List attrs = mlClass.getAttrSet();
        attrs.add(attr);
        mlClass.set(MlClass.ATTR_SET, attrs);
        commonDao.mergeWithSecurityCheck(mlClass, false, false);
    }

}
