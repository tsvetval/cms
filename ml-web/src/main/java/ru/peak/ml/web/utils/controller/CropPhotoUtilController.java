package ru.peak.ml.web.utils.controller;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import com.google.inject.internal.util.$FinalizableReferenceQueue;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import ru.peak.ml.core.model.util.MlUtil;
import ru.peak.ml.core.services.AccessService;
import ru.peak.ml.prop.Property;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.utils.controller.utils.AbstractUtilController;

import javax.imageio.*;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.ParseException;

/**
 * Контроллер для кропинга фотографий
 */
public class CropPhotoUtilController extends AbstractUtilController implements UtilController {

    private static final Logger log = LoggerFactory.getLogger(CropPhotoUtilController.class);
    @Inject
    AccessService accessService;

    @Override
    public void serve(MlHttpServletRequest request, MlUtil mlUtil, MlHttpServletResponse resp) throws IOException, ParseException {
        String action = request.getString("action");
        Long userId = accessService.getCurrentUser().getId();
        switch (action) {
            case "getInfo":
                String fileName = request.getString("fileName");
                resp.setJsonData(minimalizePhoto(fileName, userId));
                break;
            case "savePhoto":
                resp.setJsonData(cutPhoto(request,userId));
                break;
        }
    }

    private JsonElement cutPhoto(MlHttpServletRequest request, Long userId) throws IOException {
        String fileName = request.getString("fileName");
        String resultFileName = fileName+"_result";
         int width = 480;
        int height = 600;
        int dpi = 610;

        String filePath = String.format("%s/user_%d/%s", Property.getTempDir(), userId, fileName);
        FileInputStream inputStream = new FileInputStream(filePath);
        BufferedImage bufferedImage = ImageIO.read(inputStream);
        double koef = 0;
        if (bufferedImage.getWidth() > bufferedImage.getHeight()) {
            koef = ((double) bufferedImage.getWidth()) / 500;
        } else {
            koef = ((double) bufferedImage.getHeight()) / 500;
        }
        int x = (int) (request.getLong("x") * koef);
        int y = (int) (request.getLong("y") * koef);
        int h = (int) (request.getLong("height") * koef);
        int w = (int) (request.getLong("width") * koef);
        w = w > bufferedImage.getWidth() ? bufferedImage.getWidth() : w;
        h = h > bufferedImage.getHeight() ? bufferedImage.getHeight() : h;
        BufferedImage cutImage = bufferedImage;
        if (h > 0 && w > 0) {
            cutImage = bufferedImage.getSubimage(x, y, w, h);
        }

        BufferedImage resImage = new BufferedImage(width, height, cutImage.getType());
        Graphics2D g = resImage.createGraphics();
        g.drawImage(cutImage, 0, 0, width, height, null);
        g.dispose();
        File destinationFile = new File(String.format("%s/user_%d/%s", Property.getTempDir(), userId, resultFileName));

        ImageWriter imageWriter = ImageIO.getImageWritersBySuffix("jpeg").next();
        ImageOutputStream ios = ImageIO.createImageOutputStream(destinationFile);
        imageWriter.setOutput(ios);
        ImageWriteParam jpegParams = imageWriter.getDefaultWriteParam();

        IIOMetadata data = imageWriter.getDefaultImageMetadata(new ImageTypeSpecifier(resImage), jpegParams);
        Element tree = (Element) data.getAsTree("javax_imageio_jpeg_image_1.0");
        Element jfif = (Element) tree.getElementsByTagName("app0JFIF").item(0);
        jfif.setAttribute("Xdensity", Integer.toString(dpi));
        jfif.setAttribute("Ydensity", Integer.toString(dpi));
        jfif.setAttribute("resUnits", "1"); // density is dots per inch
        data.mergeTree("javax_imageio_jpeg_image_1.0", tree);

        // Write and clean up
        imageWriter.write(data, new IIOImage(resImage, null, data), jpegParams);
        ios.close();
        imageWriter.dispose();
        JsonObject result = new JsonObject();
        result.add("fileName",new JsonPrimitive(resultFileName));
        return result;
    }

    private JsonElement minimalizePhoto(String fileName, Long userId) throws IOException {

        String tempPath = String.format("%s/user_%d/%s", Property.getTempDir(), userId, fileName);
        String fname = fileName + "_" + String.valueOf(System.currentTimeMillis());
        String tfname = String.format("%s/user_%d/%s", Property.getTempDir(), userId, fname);

        FileOutputStream outputStream = new FileOutputStream(fname);
        byte[] img = FileUtils.readFileToByteArray(new File(tempPath));


        ByteArrayInputStream bis = new ByteArrayInputStream(img);
        BufferedImage origImage = ImageIO.read(bis);

        ImageIO.write(origImage, "jpeg", outputStream);
        outputStream.close();

        int w = origImage.getWidth();
        int h = origImage.getHeight();
        double koef = 0.0;

        int img_w, img_h;
        if (w > h) {
            koef = ((double) w) / 500;
            img_w = 500;
            img_h = (int) (h / koef);
        } else {
            koef = ((double) h) / 500;
            img_h = 500;
            img_w = (int) (w / koef);

        }
        BufferedImage resizedImage = new BufferedImage(img_w, img_h, origImage.getType());
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(origImage, 0, 0, img_w, img_h, null);
        g.dispose();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(resizedImage, "jpeg", baos);
        baos.flush();
        byte[] imageInByte = baos.toByteArray();
        baos.close();

        FileUtils.writeByteArrayToFile(new File(tfname),imageInByte);
        JsonObject result = new JsonObject();
        result.add("width",new JsonPrimitive(img_w));
        result.add("height",new JsonPrimitive(img_h));
        result.add("fileName",new JsonPrimitive(fname));
        return result;
    }

}