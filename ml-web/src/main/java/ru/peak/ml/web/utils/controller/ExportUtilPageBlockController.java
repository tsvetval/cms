package ru.peak.ml.web.utils.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import ru.peak.ml.core.model.util.MlUtil;
import ru.peak.ml.web.utils.controller.utils.AbstractUtilButtonController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.helper.ExportToLiquibase;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.util.Date;

/**
 *
 */
public class ExportUtilPageBlockController extends AbstractUtilButtonController {
    private static final Logger log = LoggerFactory.getLogger(ExportUtilPageBlockController.class);

    @Override
    protected void execUtil(MlHttpServletRequest request, MlUtil mlUtil, MlHttpServletResponse resp) {
        try {
            Document document = (new ExportToLiquibase()).doit();

            // Write
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            StreamResult result = new StreamResult(outputStream);
            transformer.transform(source, result);

            resp.setDownloadData(outputStream.toByteArray());
            resp.setDownloadFileName("export_" + (new Date().getTime()) + ".xml");

        } catch (ExportToLiquibase.ExportException e) {
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

}
