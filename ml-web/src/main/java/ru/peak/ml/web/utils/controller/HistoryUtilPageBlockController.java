package ru.peak.ml.web.utils.controller;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.peak.ml.core.model.util.MlUtil;
import ru.peak.ml.web.utils.controller.utils.AbstractUtilButtonController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;

/**
 */
public class HistoryUtilPageBlockController extends AbstractUtilButtonController {      //     ru.peak.ml.web.utils.controller.HistoryUtilPageBlockController
    private static final Logger log = LoggerFactory.getLogger(HistoryUtilPageBlockController.class);

    private static final String SHOW_HISTORY_TEMPLATE = "blocks/utils/history/showHistory.hml";

    @Override
    protected void execUtil(MlHttpServletRequest request, MlUtil mlUtil, MlHttpServletResponse resp) {
        JsonObject dataJson = new JsonObject();
        dataJson.add("showType",new JsonPrimitive("execJs"));
        dataJson.add("functionName", new JsonPrimitive("openHistory"));
        JsonObject params = new JsonObject();
        params.add("objectId",new JsonPrimitive(request.getLong("objectId")));
        params.add("className", new JsonPrimitive(request.getString("className")));
        params.add("historyUtilId", new JsonPrimitive((Long)mlUtil.get("id")));
        dataJson.add("params",params);
        resp.setJsonData(dataJson);
    }
}
