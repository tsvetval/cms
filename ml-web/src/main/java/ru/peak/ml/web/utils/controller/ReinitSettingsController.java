package ru.peak.ml.web.utils.controller;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.core.holders.SecurityHolder;
import ru.peak.ml.core.model.util.MlUtil;
import ru.peak.ml.prop.MlProperties;
import ru.peak.ml.prop.Property;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.utils.controller.utils.AbstractUtilController;

import java.io.IOException;
import java.text.ParseException;

/**
 * Контроллер для переинициализации EntityManagerFactory
 */
public class ReinitSettingsController extends AbstractUtilController implements UtilController{

    private static final Logger log = LoggerFactory.getLogger(ReinitSettingsController.class);

    @Override
    public void serve(MlHttpServletRequest request, MlUtil mlUtil, MlHttpServletResponse resp) throws IOException, ParseException {
        System.out.println("11111111111");
        MlProperties mlProperties = GuiceConfigSingleton.inject(MlProperties.class);
        mlProperties.init();
        if(!"development".equals(mlProperties.getProperty(Property.MODE))) {
            String contextPath = request.getRequest().getServletContext().getContextPath();
            mlProperties.setProperty(
                    Property.TEMPLATE_PATH,
                    System.getProperty("catalina.base") + "/webapps" + contextPath + "/WEB-INF/templates"
            );
            mlProperties.setProperty(
                    Property.STATIC_PATH,
                    System.getProperty("catalina.base") + "/webapps/" + contextPath + "/public"
            );
        }
        JsonObject result = new JsonObject();
        result.add("showType", new JsonPrimitive("modal"));
        result.add("idModal", new JsonPrimitive((Long) mlUtil.get("id")));
        result.add("title", new JsonPrimitive("Результат операции"));
        result.add("content", new JsonPrimitive("Настройки системы успешно применены!"));
        resp.setJsonData(result);
    }

}