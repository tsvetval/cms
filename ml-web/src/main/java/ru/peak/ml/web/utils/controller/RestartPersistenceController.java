package ru.peak.ml.web.utils.controller;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.persist.PersistService;
import com.google.inject.persist.UnitOfWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.ml.core.common.guice.IGuiceModules;
import ru.peak.ml.core.holders.SecurityHolder;
import ru.peak.ml.core.initializer.MlMetaDataInitializeService;
import ru.peak.ml.core.holders.impl.SecurityHolderImpl;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.core.model.util.MlUtil;
import ru.peak.ml.web.utils.controller.utils.AbstractUtilController;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/**
 * Контроллер для переинициализации EntityManagerFactory
 */
public class RestartPersistenceController extends AbstractUtilController implements UtilController{

    private static final Logger log = LoggerFactory.getLogger(RestartPersistenceController.class);

    @Override
    public void serve(MlHttpServletRequest request, MlUtil mlUtil, MlHttpServletResponse resp) throws IOException, ParseException {
        //GuiceConfigSingleton.inject(UnitOfWork.class).end();
        //GuiceConfigSingleton.inject(EntityManagerFactory.class).close();
        //killEntityManagerInThreadLocale();
        reinitInjector();
        GuiceConfigSingleton.inject(UnitOfWork.class).begin();
        GuiceConfigSingleton.inject(MlMetaDataInitializeService.class).initializeAllMetaData();
        // Перенинициализируем безопасность
        SecurityHolder securityHolder = GuiceConfigSingleton.inject(SecurityHolder.class);
        securityHolder.initAllRoles();

        JsonObject result = new JsonObject();
        result.add("showType", new JsonPrimitive("modal"));
        result.add("idModal", new JsonPrimitive((Long) mlUtil.get("id")));
        result.add("title", new JsonPrimitive("Результат операции"));
        result.add("content", new JsonPrimitive("Переинициализация завершена!"));
        resp.setJsonData(result);
        GuiceConfigSingleton.inject(UnitOfWork.class).end();
    }

    private void killEntityManagerInThreadLocale() {
        ThreadLocal<EntityManager> managerThreadLocal = new InheritableThreadLocal<>();
        System.out.println(managerThreadLocal.get().isOpen());

    }

    private void reinitInjector(){
        List<Module> modules = GuiceConfigSingleton.inject(IGuiceModules.class).getGuiceModules();
        Injector injector = Guice.createInjector(modules);
        injector.getInstance(PersistService.class).start();
        GuiceConfigSingleton.getInstance().setInjector(injector);
    }
}