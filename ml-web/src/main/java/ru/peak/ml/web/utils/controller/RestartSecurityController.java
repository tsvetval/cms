package ru.peak.ml.web.utils.controller;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.persist.PersistService;
import com.google.inject.persist.UnitOfWork;
import org.codehaus.groovy.runtime.powerassert.SourceText;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.ml.core.common.guice.IGuiceModules;
import ru.peak.ml.core.holders.SecurityHolder;
import ru.peak.ml.core.initializer.MlMetaDataInitializeService;
import ru.peak.ml.core.model.util.MlUtil;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.utils.controller.utils.AbstractUtilController;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/**
 * Контроллер для переинициализации EntityManagerFactory
 */
public class RestartSecurityController extends AbstractUtilController implements UtilController{

    private static final Logger log = LoggerFactory.getLogger(RestartSecurityController.class);

    @Override
    public void serve(MlHttpServletRequest request, MlUtil mlUtil, MlHttpServletResponse resp) throws IOException, ParseException {
        System.out.println("11111111111");
        SecurityHolder securityHolder = GuiceConfigSingleton.inject(SecurityHolder.class);
        securityHolder.initAllRoles();
        JsonObject result = new JsonObject();
        result.add("showType", new JsonPrimitive("modal"));
        result.add("idModal", new JsonPrimitive((Long) mlUtil.get("id")));
        result.add("title", new JsonPrimitive("Результат операции"));
        result.add("content", new JsonPrimitive("Настройки безопасности успешно применены!"));
        resp.setJsonData(result);
    }

}