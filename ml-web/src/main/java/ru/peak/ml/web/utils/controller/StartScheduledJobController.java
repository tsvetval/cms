package ru.peak.ml.web.utils.controller;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.core.model.util.MlUtil;
import ru.peak.ml.web.service.MlSchedulerService;
import ru.peak.ml.web.utils.controller.utils.AbstractUtilController;

import java.io.IOException;
import java.text.ParseException;

/**
 * Контроллер для запуска периодического задания
 */
public class StartScheduledJobController extends AbstractUtilController implements UtilController{

    private static final Logger log = LoggerFactory.getLogger(StartScheduledJobController.class);
    @Inject
    MlSchedulerService scheduledService;

    @Override
    public void serve(MlHttpServletRequest request, MlUtil mlUtil, MlHttpServletResponse resp) throws IOException, ParseException {
        scheduledService.startScheduledJob(request.getLong("objectId"));
    }
}
