package ru.peak.ml.web.utils.controller;

import org.eclipse.persistence.dynamic.DynamicEntity;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.core.model.util.MlUtil;

import javax.xml.datatype.DatatypeConfigurationException;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public interface UtilController {

    void serve(MlHttpServletRequest request, MlUtil mlUtil, MlHttpServletResponse resp) throws IOException, ParseException, DatatypeConfigurationException;

    public boolean isShownForObjectList(List<DynamicEntity> objectList, MlUtil util);
}
