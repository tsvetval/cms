package ru.peak.ml.web.utils.controller.utils;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.template.TemplateEngine;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.core.model.util.MlUtil;
import ru.peak.ml.web.utils.controller.UtilController;

import java.io.IOException;
import java.util.HashMap;

/**
 */
public abstract class AbstractUtilButtonController extends AbstractUtilController implements UtilController {
    private static final Logger log = LoggerFactory.getLogger(AbstractUtilButtonController.class);

    private static final String BLOCK_TEMPLATE = "blocks/utils/util/initButton.hml";

    private static class Actions {
        public static final String SHOW = "show";
        public static final String EXEC_UTIL = "execUtil";
    }

    @Override
    public void serve(MlHttpServletRequest request, MlUtil mlUtil, MlHttpServletResponse resp) throws IOException {
        String action = request.getString("action", "show");
        if (Actions.SHOW.equalsIgnoreCase(action)) {
            JsonObject dataJson = new JsonObject();
            if (isCanRenderUtil(request)) {
                TemplateEngine templateEngine = GuiceConfigSingleton.inject(TemplateEngine.class);

                HashMap<String, Object> data = new HashMap<>();
                data.put("widgetMlInstance", mlUtil);

                String html = templateEngine.renderTemplate(BLOCK_TEMPLATE, data);
                dataJson.add("html", new JsonPrimitive(html));
            }
            resp.setJsonData(dataJson);
        } else if (Actions.EXEC_UTIL.equalsIgnoreCase(action)) {
            execUtil(request, mlUtil, resp);
        }
    }

    protected boolean isCanRenderUtil(MlHttpServletRequest request) {
        return true;
    }


    protected abstract void execUtil(MlHttpServletRequest request, MlUtil pageBlock, MlHttpServletResponse resp);
}