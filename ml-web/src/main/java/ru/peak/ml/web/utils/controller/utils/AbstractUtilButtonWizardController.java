package ru.peak.ml.web.utils.controller.utils;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.template.TemplateEngine;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.core.model.util.MlUtil;
import ru.peak.ml.web.utils.controller.UtilController;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 */
public abstract class AbstractUtilButtonWizardController extends AbstractUtilController implements UtilController {
    private static final Logger log = LoggerFactory.getLogger(AbstractUtilButtonWizardController.class);

    private static final String BLOCK_TEMPLATE = "blocks/utils/util/initButtonWizard.hml";

    protected String nextAction;
    private static class Actions {
        public static final String SHOW = "show";
    }

    protected Map<String,String> actions = new HashMap<String,String>();

    @Override
    public void serve(MlHttpServletRequest request, MlUtil mlUtil, MlHttpServletResponse resp) throws IOException {
        String action = request.getString("action", "show");
        initActions();
        initNextAction();
        if (Actions.SHOW.equalsIgnoreCase(action)) {

            JsonObject dataJson = new JsonObject();
            if (isCanRenderUtil(request)) {
                TemplateEngine templateEngine = GuiceConfigSingleton.inject(TemplateEngine.class);

                HashMap<String, Object> data = new HashMap<>();
                data.put("widgetMlInstance", mlUtil);
                data.put("nextAction",nextAction);
                String html = templateEngine.renderTemplate(BLOCK_TEMPLATE, data);
                dataJson.add("html", new JsonPrimitive(html));
            }
            resp.setJsonData(dataJson);

        }   else {
            if(actions.containsKey(action)){
                execUtil(action, request,mlUtil,resp);
            }else {
                throw new RuntimeException("Действие: "+action+" не определено!");
            }
        }
    }

    protected boolean isCanRenderUtil(MlHttpServletRequest request) {
        return true;
    }

    protected  abstract void initNextAction();
    protected abstract void initActions();

    protected abstract void execUtil(String action, MlHttpServletRequest request, MlUtil mlUtil, MlHttpServletResponse resp);
}