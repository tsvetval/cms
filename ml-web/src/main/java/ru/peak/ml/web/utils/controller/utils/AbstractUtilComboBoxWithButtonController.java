package ru.peak.ml.web.utils.controller.utils;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.peak.ml.template.TemplateEngine;
import ru.peak.ml.web.block.controller.BlockController;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.core.model.page.MlPageBlockBase;

import java.util.HashMap;
import java.util.Map;

/**
 */
public abstract class AbstractUtilComboBoxWithButtonController implements BlockController {
    private static final Logger log = LoggerFactory.getLogger(AbstractUtilComboBoxWithButtonController.class);

    private static final String BLOCK_TEMPLATE = "blocks/utils/util/initComboBoxWithButton.hml";

    private static class Actions {
        public static final String SHOW = "show";
        public static final String EXEC_UTIL = "execUtil";
    }

    protected Map<String,String> actions = new HashMap<String,String>();

    @Override
    public void serve(MlHttpServletRequest request, MlPageBlockBase pageBlock, MlHttpServletResponse resp){
        String action = request.getString("action", "show");
        initActions();
        if (Actions.SHOW.equalsIgnoreCase(action)) {
            TemplateEngine templateEngine = GuiceConfigSingleton.inject(TemplateEngine.class);
            HashMap<String, Object> data = new HashMap<>();
            data.put("widgetMlInstance", pageBlock);
            data.put("actions",actions);

            String html = templateEngine.renderTemplate(BLOCK_TEMPLATE, data);
            JsonObject dataJson = new JsonObject();
            dataJson.add("html", new JsonPrimitive(html));
            resp.setJsonData(dataJson);
        }   else {
            if(actions.containsKey(action)){
                execUtil(action, request,pageBlock,resp);
            }else {
                throw new RuntimeException("Действие: "+action+" не определено!");
            }
        }
    }

    protected abstract void initActions();

    protected abstract void execUtil(String action, MlHttpServletRequest request, MlPageBlockBase pageBlock, MlHttpServletResponse resp);
}