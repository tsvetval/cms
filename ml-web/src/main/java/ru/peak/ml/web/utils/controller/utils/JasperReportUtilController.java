package ru.peak.ml.web.utils.controller.utils;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.peak.ml.core.model.util.MlUtil;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.model.MlJasperReportUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.xml.datatype.DatatypeConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 */
public class JasperReportUtilController extends AbstractUtilController {
    private static final Logger log = LoggerFactory.getLogger(JasperReportUtilController.class);

    @Inject
    protected EntityManager entityManager;

    @Override
    public void serve(MlHttpServletRequest request, MlUtil mlUtil, MlHttpServletResponse resp) throws IOException, ParseException, DatatypeConfigurationException {
        MlJasperReportUtil util = (MlJasperReportUtil) mlUtil;

        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();

            Connection connection = entityManager.unwrap(Connection.class);

            byte[] template = util.getTemplate();
            if(template==null) throw new RuntimeException("Шаблон для репорта не задан");

            JasperReport jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(template));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap<String, Object>(), connection);

            ByteArrayOutputStream result = new ByteArrayOutputStream();

            JRDocxExporter jrDocxExporter = new JRDocxExporter();
            jrDocxExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            jrDocxExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(result));
            jrDocxExporter.exportReport();

            resp.setDownloadData(result.toByteArray());
            resp.setDownloadFileName(prepareFileName(util) + ".docx");

        } catch (Exception e) {

            ByteArrayOutputStream message = new ByteArrayOutputStream();
            PrintWriter writer = new PrintWriter(message);
            e.printStackTrace(writer);
            writer.close();

            JsonObject result = new JsonObject();
            result.add("showType", new JsonPrimitive("modal"));
            result.add("idModal", new JsonPrimitive((Long) mlUtil.get("id")));
            result.add("title", new JsonPrimitive("Результат операции: ошибка"));
            result.add("content", new JsonPrimitive("Возникла ошибка: " + e.getMessage()));
            resp.setJsonData(result);
        } finally {
            if(transaction.isActive()) transaction.commit();
        }
    }

    /**
     * This function implement simple expression language for file name. Not it is possible to use functions
     * in following format: $func(arguments). Supported functions:
     *  * attr - value of tool object attribute; take one argument - name of attribute
     *  * now - current time; could take one argument - date format
     */
    private static String prepareFileName(MlJasperReportUtil util) {
        StringBuffer result = new StringBuffer();

        Pattern pattern = Pattern.compile("\\$([a-z_]+?)\\((.*?)\\)");
        Matcher matcher = pattern.matcher(util.getFileName());

        while(matcher.find()) {
            String fName = matcher.group(1);
            String fArgs = matcher.group(2);

            String replace;
            switch (fName) {
                case "attr": {
                    String attrName = fArgs.replaceAll("^'|'$|^\"|\"$", "");
                    Object value = util.get(attrName);
                    replace = value==null ? "null" : value.toString();
                    break;
                }
                case "now": {
                    String format = fArgs.replaceAll("^'|'$|^\"|\"$", "");
                    if(format==null || format.equals("")) format = "dd.MM.yyyy HH:mm";
                    SimpleDateFormat simpleDateFormat;
                    try {
                        simpleDateFormat = new SimpleDateFormat(format);
                        replace = simpleDateFormat.format(new Date());
                    } catch (Exception e) {
                        replace = "WRONG_DATE_FORMAT_" + format;
                    }
                    break;
                }
                default: {
                    replace = matcher.group();
                }
            }

            matcher.appendReplacement(result, replace);
        }
        matcher.appendTail(result);

        return result.toString();
    }

}
