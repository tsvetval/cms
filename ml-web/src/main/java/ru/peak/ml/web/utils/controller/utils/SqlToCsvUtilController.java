package ru.peak.ml.web.utils.controller.utils;

import au.com.bytecode.opencsv.CSVWriter;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.model.MlSqlToCsvUtil;
import ru.peak.ml.core.model.util.MlUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.xml.datatype.DatatypeConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 */
public class SqlToCsvUtilController extends AbstractUtilController {
    private static final Logger log = LoggerFactory.getLogger(SqlToCsvUtilController.class);

    @Inject
    protected EntityManager entityManager;

    @Override
    public void serve(MlHttpServletRequest request, MlUtil mlUtil, MlHttpServletResponse resp) throws IOException, ParseException, DatatypeConfigurationException {
        MlSqlToCsvUtil util = (MlSqlToCsvUtil) mlUtil;
        String sql = util.getSql();

        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            Connection connection = entityManager.unwrap(Connection.class);

            Statement statement = connection.createStatement();
            statement.execute(sql);
            ResultSet resultSet = statement.getResultSet();

            ByteArrayOutputStream result = new ByteArrayOutputStream();

            char separator;
            separator = (util.getColumnSeparator() != null && util.getColumnSeparator().length()>0)
                    ? util.getColumnSeparator().charAt(0)
                    : ',';

            CSVWriter csvWriter = new CSVWriter(new OutputStreamWriter(result, Charset.forName(util.getFileEncoding())), separator);
            csvWriter.writeAll(resultSet, true);
            csvWriter.close();

            resp.setDownloadData(result.toByteArray());
            resp.setDownloadFileName(prepareFileName(util));

        } catch (SQLException e) {

            ByteArrayOutputStream message = new ByteArrayOutputStream();
            PrintWriter writer = new PrintWriter(message);
            e.printStackTrace(writer);
            writer.close();

            JsonObject result = new JsonObject();
            result.add("showType", new JsonPrimitive("modal"));
            result.add("idModal", new JsonPrimitive((Long) mlUtil.get("id")));
            result.add("title", new JsonPrimitive("Результат операции: ошибка"));
            result.add("content", new JsonPrimitive("Возникла ошибка: " + e.getMessage()));
            resp.setJsonData(result);
        } finally {
            if(transaction.isActive()) transaction.commit();
        }



    }

    /**
     * This function implement simple expression language for file name. Not it is possible to use functions
     * in following format: $func(arguments). Supported functions:
     *  * attr - value of tool object attribute; take one argument - name of attribute
     *  * now - current time; could take one argument - date format
     */
    private static String prepareFileName(MlSqlToCsvUtil util) {
        StringBuffer result = new StringBuffer();

        Pattern pattern = Pattern.compile("\\$([a-z_]+?)\\((.*?)\\)");
        Matcher matcher = pattern.matcher(util.getFileName());

        while(matcher.find()) {
            String fName = matcher.group(1);
            String fArgs = matcher.group(2);

            String replace;
            switch (fName) {
                case "attr": {
                    String attrName = fArgs.replaceAll("^'|'$|^\"|\"$", "");
                    Object value = util.get(attrName);
                    replace = value==null ? "null" : value.toString();
                    break;
                }
                case "now": {
                    String format = fArgs.replaceAll("^'|'$|^\"|\"$", "");
                    if(format==null || format.equals("")) format = "dd.MM.yyyy HH:mm";
                    SimpleDateFormat simpleDateFormat;
                    try {
                        simpleDateFormat = new SimpleDateFormat(format);
                        replace = simpleDateFormat.format(new Date());
                    } catch (Exception e) {
                        replace = "WRONG_DATE_FORMAT_" + format;
                    }
                    break;
                }
                default: {
                    replace = matcher.group();
                }
            }

            matcher.appendReplacement(result, replace);
        }
        matcher.appendTail(result);

        return result.toString();
    }

}
